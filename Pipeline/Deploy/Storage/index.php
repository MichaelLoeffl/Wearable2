<?php
/***************************************************************************
 *
 * Encode Explorer
 *
 * Author : Marek Rei (marek ät marekrei dot com)
 * Version : 6.4.1
 * Homepage : encode-explorer.siineiolekala.net
 *
 *
 * NB!:If you change anything, save with UTF-8! Otherwise you may
 *     encounter problems, especially when displaying images.
 *
 ***************************************************************************/

/***************************************************************************/
/*   HERE ARE THE SETTINGS FOR CONFIGURATION                               */
/***************************************************************************/

//
// Initialising variables. Don't change these.
//

$_CONFIG = array();
$_ERROR = "";
$_START_TIME = microtime(TRUE);

/*
 * GENERAL SETTINGS
 */

//
// Choose a language. See below in the language section for options.
// Default: $_CONFIG['lang'] = "en";
//
$_CONFIG['lang'] = "en";

//
// Display thumbnails when hovering over image entries in the list.
// Common image types are supported (jpeg, png, gif).
// Pdf files are also supported but require ImageMagick to be installed.
// Default: $_CONFIG['thumbnails'] = true;
//
$_CONFIG['thumbnails'] = true;

//
// Maximum sizes of the thumbnails.
// Default: $_CONFIG['thumbnails_width'] = 200;
// Default: $_CONFIG['thumbnails_height'] = 200;
//
$_CONFIG['thumbnails_width'] = 300;
$_CONFIG['thumbnails_height'] = 300;

//
// Mobile interface enabled. true/false
// Default: $_CONFIG['mobile_enabled'] = true;
//
$_CONFIG['mobile_enabled'] = true;

//
// Mobile interface as the default setting. true/false
// Default: $_CONFIG['mobile_default'] = false;
//
$_CONFIG['mobile_default'] = false;

/*
 * USER INTERFACE
 */

//
// Will the files be opened in a new window? true/false
// Default: $_CONFIG['open_in_new_window'] = false;
//
$_CONFIG['open_in_new_window'] = false;

//
// How deep in subfolders will the script search for files?
// Set it larger than 0 to display the total used space.
// Default: $_CONFIG['calculate_space_level'] = 0;
//
$_CONFIG['calculate_space_level'] = 0;

// Will the page header be displayed? 0=no, 1=yes.
// Default: $_CONFIG['show_top'] = true;
//
$_CONFIG['show_top'] = true;

//
// The title for the page
// Default: $_CONFIG['main_title'] = "Encode Explorer";
//
$_CONFIG['main_title'] = "##Title##";

//
// The secondary page titles, randomly selected and displayed under the main header.
// For example: $_CONFIG['secondary_titles'] = array("Secondary title", "&ldquo;Secondary title with quotes&rdquo;");
// Default: $_CONFIG['secondary_titles'] = array();
//
$_CONFIG['secondary_titles'] = array();

//
// Display breadcrumbs (relative path of the location).
// Default: $_CONFIG['show_path'] = true;
//
$_CONFIG['show_path'] = false;

//
// Display the time it took to load the page.
// Default: $_CONFIG['show_load_time'] = true;
//
$_CONFIG['show_load_time'] = false;

//
// The time format for the "last changed" column.
// Default: $_CONFIG['time_format'] = "d.m.y H:i:s";
//
$_CONFIG['time_format'] = "Y-m-d H:i:s";

//
// Charset. Use the one that suits for you.
// Default: $_CONFIG['charset'] = "UTF-8";
//
$_CONFIG['charset'] = "UTF-8";

/*
* PERMISSIONS
*/

//
// The array of folder names that will be hidden from the list.
// Default: $_CONFIG['hidden_dirs'] = array();
//
$_CONFIG['hidden_dirs'] = array(".well-known");

//
// Filenames that will be hidden from the list.
// Default: $_CONFIG['hidden_files'] = array(".ftpquota", "index.php", "index.php~", ".htaccess", ".htpasswd");
//
$_CONFIG['hidden_files'] = array(".ftpquota", "index.php", "index.php~", ".htaccess", ".htpasswd");

//
// Whether authentication is required to see the contents of the page.
// If set to false, the page is public.
// If set to true, you should specify some users as well (see below).
// Important: This only prevents people from seeing the list.
// They will still be able to access the files with a direct link.
// Default: $_CONFIG['require_login'] = false;
//
$_CONFIG['require_login'] = false;

//
// Usernames and passwords for restricting access to the page.
// The format is: array(username, password, status)
// Status can be either "user" or "admin". User can read the page, admin can upload and delete.
// For example: $_CONFIG['users'] = array(array("username1", "password1", "user"), array("username2", "password2", "admin"));
// You can also keep require_login=false and specify an admin.
// That way everyone can see the page but username and password are needed for uploading.
// For example: $_CONFIG['users'] = array(array("username", "password", "admin"));
// Default: $_CONFIG['users'] = array();
//
$_CONFIG['users'] = array();

//
// Permissions for uploading, creating new directories and deleting.
// They only apply to admin accounts, regular users can never perform these operations.
// Default:
// $_CONFIG['upload_enable'] = true;
// $_CONFIG['newdir_enable'] = true;
// $_CONFIG['delete_enable'] = false;
//
$_CONFIG['upload_enable'] = false;
$_CONFIG['newdir_enable'] = false;
$_CONFIG['delete_enable'] = false;

/*
 * UPLOADING
 */

//
// List of directories where users are allowed to upload.
// For example: $_CONFIG['upload_dirs'] = array("./myuploaddir1/", "./mydir/upload2/");
// The path should be relative to the main directory, start with "./" and end with "/".
// All the directories below the marked ones are automatically included as well.
// If the list is empty (default), all directories are open for uploads, given that the password has been set.
// Default: $_CONFIG['upload_dirs'] = array();
//
$_CONFIG['upload_dirs'] = array();

//
// MIME type that are allowed to be uploaded.
// For example, to only allow uploading of common image types, you could use:
// $_CONFIG['upload_allow_type'] = array("image/png", "image/gif", "image/jpeg");
// Default: $_CONFIG['upload_allow_type'] = array();
//
$_CONFIG['upload_allow_type'] = array();

//
// File extensions that are not allowed for uploading.
// For example: $_CONFIG['upload_reject_extension'] = array("php", "html", "htm");
// Default: $_CONFIG['upload_reject_extension'] = array();
//
$_CONFIG['upload_reject_extension'] = array("php", "php2", "php3", "php4", "php5", "phtml");

//
// By default, apply 0755 permissions to new directories
//
// The mode parameter consists of three octal number components specifying
// access restrictions for the owner, the user group in which the owner is
// in, and to everybody else in this order.
//
// See: https://php.net/manual/en/function.chmod.php
//
// Default: $_CONFIG['new_dir_mode'] = 0755;
//
$_CONFIG['new_dir_mode'] = 0755;

//
// By default, apply 0644 permissions to uploaded files
//
// The mode parameter consists of three octal number components specifying
// access restrictions for the owner, the user group in which the owner is
// in, and to everybody else in this order.
//
// See: https://php.net/manual/en/function.chmod.php
//
// Default: $_CONFIG['upload_file_mode'] = 0644;
//
$_CONFIG['upload_file_mode'] = 0644;

/*
 * LOGGING
 */

//
// Upload notification e-mail.
// If set, an e-mail will be sent every time someone uploads a file or creates a new dirctory.
// Default: $_CONFIG['upload_email'] = "";
//
$_CONFIG['upload_email'] = "";

//
// Logfile name. If set, a log line will be written there whenever a directory or file is accessed.
// For example: $_CONFIG['log_file'] = ".log.txt";
// Default: $_CONFIG['log_file'] = "";
//
$_CONFIG['log_file'] = "";

/*
 * SYSTEM
 */


//
// The starting directory. Normally no need to change this.
// Use only relative subdirectories!
// For example: $_CONFIG['starting_dir'] = "./mysubdir/";
// Default: $_CONFIG['starting_dir'] = ".";
//
$_CONFIG['starting_dir'] = ".";

//
// Location in the server. Usually this does not have to be set manually.
// Default: $_CONFIG['basedir'] = "";
//
$_CONFIG['basedir'] = "";

//
// Big files. If you have some very big files (>4GB), enable this for correct
// file size calculation.
// Default: $_CONFIG['large_files'] = false;
//
$_CONFIG['large_files'] = false;

//
// The session name, which is used as a cookie name.
// Change this to something original if you have multiple copies in the same space
// and wish to keep their authentication separate.
// The value can contain only letters and numbers. For example: MYSESSION1
// More info at: http://www.php.net/manual/en/function.session-name.php
// Default: $_CONFIG['session_name'] = "";
//
$_CONFIG['session_name'] = "";

/***************************************************************************/
/*   TÕLKED                                                                */
/*                                                                         */
/*   TRANSLATIONS.                                                         */
/***************************************************************************/

$_TRANSLATIONS = array();

// English
$_TRANSLATIONS["en"] = array(
	"file_name" => "File name",
	"size" => "Size",
	"last_changed" => "Last updated",
	"total_used_space" => "Used space",
	"free_space" => "Free space",
	"password" => "Password",
	"upload" => "Upload",
	"failed_upload" => "Failed to upload the file!",
	"failed_move" => "Failed to move the file into the right directory!",
	"wrong_password" => "Wrong password",
	"make_directory" => "New directory",
	"new_dir_failed" => "Failed to create directory",
	"chmod_dir_failed" => "Failed to change directory rights",
	"unable_to_read_dir" => "Unable to read directory",
	"location" => "Location",
	"root" => "Root",
	"log_file_permission_error" => "The script does not have permissions to write the log file.",
	"upload_not_allowed" => "The script configuration does not allow uploading in this directory.",
	"upload_dir_not_writable" => "This directory does not have write permissions.",
	"mobile_version" => "Mobile view",
	"standard_version" => "Standard view",
	"page_load_time" => "Page loaded in %.2f ms",
	"wrong_pass" => "Wrong username or password",
	"username" => "Username",
	"log_in" => "Log in",
	"upload_type_not_allowed" => "This file type is not allowed for uploading.",
	"del" => "Delete",
	"log_out" => "Log out"
);

/***************************************************************************/
/*   CSS FOR TWEAKING THE DESIGN                                           */
/***************************************************************************/


function css()
{
?>
<style type="text/css">

/* General styles */

body {
	background-color:#FFFFFF;
	font-family: Roboto,Verdana,Arial,sans-serif;
	font-size:small;
}

a {
	color: #000000;
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}

#top {
	width:100%;
	padding-bottom: 20px;
}

#top a span, #top a:hover, #top a span:hover{
	color:#68a9d2;
	font-weight:bold;
	text-align:center;
	font-size:large;
}

#top a {
	display:block;
	padding:20px 0 0 0;
}

#top span {
	display:block;
}

div.subtitle{
	width:80%;
	margin: 0 auto;
	color:#68a9d2;
	text-align:center;
}

#frame {
	border: 1px solid #CDD2D6;
	text-align:left;
	position: relative;
	margin: 0 auto;
	max-width:680px;
	overflow:hidden;
}

#error {
	max-width:450px;
	background-color:#FFE4E1;
	color:#000000;
	padding:7pt;
	position: relative;
	margin: 10pt auto;
	text-align:center;
	border: 1px dotted #CDD2D6;
}

input {
	border: 1px solid #CDD2D6;
}

.bar{
	width:100%;
	clear:both;
	height:1px;
}

/* File list */

table.table {
	width:100%;
	border-collapse: collapse;
}

table.table td{
	padding:3px;
}

table.table tr.row.two {
	background-color:#fcfdfe;
}

table.table tr.row.one {
	background-color:#f8f9fa;
}

table.table tr.row td.icon {
	width:25px;
	padding-top:3px;
	padding-bottom:1px;
}

table.table td.del {
	width:25px;
}

table.table tr.row td.size {
	width: 100px;
	text-align: right;
}

table.table tr.row td.changed {
	width: 150px;
	text-align: center;
}

table.table tr.header img {
	vertical-align:bottom;
}

table img{
	border:0;
	width: 32px;
	height: 32px;
}

/* Info area */

#info {
	color:#000000;
	max-width:680px;
	position: relative;
	margin: 0 auto;
	margin-top: 0.5em;
	text-align:center;
}

/* Thumbnail area */

#thumb {
	position:absolute;
	border: 1px solid #CDD2D6;
	background:#f8f9fa;
	display:none;
	padding:3px;
}

#thumb img {
	display:block;
}

/* Login bar (at the bottom of the page) */
#login_bar {
	margin: 0 auto;
	margin-top:2px;
	max-width:680px;
}

#login_bar input.submit{
	float:right;
}

/* Upload area */

#upload {
	margin: 0 auto;
	margin-top:2px;
	max-width:680px;
}

#upload #password_container {
	margin-right:20px;
}

#upload #newdir_container, #upload #password_container {
	float:left;
}

#upload #upload_container{
	float:right;
}

#upload input.upload_dirname, #upload input.upload_password{
	width:140px;
}

#upload input.upload_file{
	font-size:small;
}

/* Breadcrumbs */

div.breadcrumbs {
	display:block;
	padding:1px 3px;
	color:#cccccc;
	font-size:x-small;
}

div.breadcrumbs a{
	display:inline-block;
	color:#cccccc;
	padding:2px 0;
	font-size:small;
}

/* Login area */

#login {
	max-width:280px;
	text-align:right;
	margin:15px auto 50px auto;
}

#login div {
	display:block;
	width:100%;
	margin-top:5px;
}

#login label{
	width: 120px;
	text-align: right;
}

/* Mobile interface */

body.mobile #frame, body.mobile #info, body.mobile #upload {
	max-width:none;
}

body.mobile {
	font-size:medium;
}

body.mobile a.item {
	display:block;
	padding:10px 0;
}

body.mobile a.item span.size {
	float:right;
	margin-left:10px;
}

body.mobile table.table {
	margin-bottom:30px;
}

body.mobile table.table tr td {
	border-top: 1px solid #CDD2D6;
}

body.mobile table.table tr.last td {
	border-bottom: 1px solid #CDD2D6;
}

body.mobile #top {
	padding-bottom:3px;
}

body.mobile #top a {
	padding-top:3px;
}

body.mobile #upload #password_container, body.mobile #upload #upload_container, body.mobile #upload #newdir_container {
	float:none;
	margin-top:5px;
}

body.mobile #upload input.upload_dirname, body.mobile #upload input.upload_password{
	width:240px;
}

body.mobile #upload {
	margin-bottom:15px;
}

</style>

<?php
}

/***************************************************************************/
/*   IMAGE CODES IN BASE64                                                 */
/*   You can generate your own with a converter                            */
/*   Like here: http://www.motobit.com/util/base64-decoder-encoder.asp     */
/*   Or here: http://www.greywyvern.com/code/php/binary2base64             */
/*   Or just use PHP base64_encode() function                              */
/***************************************************************************/


$_IMAGES = array();

$_IMAGES["arrow_down"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAABbSURBVCjPY/jPgB8yDCkFB/7v+r/5/+r/
i/7P+N/3DYuC7V93/d//fydQ0Zz/9eexKFgtsejLiv8b/8/8X/WtUBGrGyZLdH6f8r/sW64cTkdW
SRS+zpQbgiEJAI4UCqdRg1A6AAAAAElFTkSuQmCC";
$_IMAGES["arrow_up"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAABbSURBVCjPY/jPgB8yDDkFmyVWv14kh1PB
eoll31f/n/ytUw6rgtUSi76s+L/x/8z/Vd8KFbEomPt16f/1/1f+X/S/7X/qeSwK+v63/K/6X/g/
83/S/5hvQywkAdMGCdCoabZeAAAAAElFTkSuQmCC";
$_IMAGES["del"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJdSURBVDjLpZP7S1NhGMf9W7YfogSJboSE
UVCY8zJ31trcps6zTI9bLGJpjp1hmkGNxVz4Q6ildtXKXzJNbJRaRmrXoeWx8tJOTWptnrNryre5
YCYuI3rh+8vL+/m8PA/PkwIg5X+y5mJWrxfOUBXm91QZM6UluUmthntHqplxUml2lciF6wrmdHri
I0Wx3xw2hAediLwZRWRkCPzdDswaSvGqkGCfq8VEUsEyPF1O8Qu3O7A09RbRvjuIttsRbT6HHzeb
sDjcB4/JgFFlNv9MnkmsEszodIIY7Oaut2OJcSF68Qx8dgv8tmqEL1gQaaARtp5A+N4NzB0lMXxo
n/uxbI8gIYjB9HytGYuusfiPIQcN71kjgnW6VeFOkgh3XcHLvAwMSDPohOADdYQJdF1FtLMZPmsl
vhZJk2ahkgRvq4HHUoWHRDqTEDDl2mDkfheiDgt8pw340/EocuClCuFvboQzb0cwIZgki4KhzlaE
6w0InipbVzBfqoK/qRH94i0rgokSFeO11iBkp8EdV8cfJo0yD75aE2ZNRvSJ0lZKcBXLaUYmQrCz
DT6tDN5SyRqYlWeDLZAg0H4JQ+Jt6M3atNLE10VSwQsN4Z6r0CBwqzXesHmV+BeoyAUri8EyMfi2
FowXS5dhd7doo2DVII0V5BAjigP89GEVAtda8b2ehodU4rNaAW+dGfzlFkyo89GTlcrHYCLpKD+V
7yeeHNzLjkp24Uu1Ed6G8/F8qjqGRzlbl2H2dzjpMg1KdwsHxOlmJ7GTeZC/nesXbeZ6c9OYnuxU
c3fmBuFft/Ff8xMd0s65SXIb/gAAAABJRU5ErkJggg==";


$_IMAGES["audio"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIvSURBVDjLjZPLaxNRFIeriP+AO7Gg7nRX
qo1ogoKCK0Fbig8QuxKhPop04SYLNYqlKpEmQlDBRRcFFWlBqqJYLVpbq6ktaRo0aWmamUxmJpN5
ZvKoP++9mmlqWuzAt7jc+X2Hcy6nDkAdhXxbCI2Epv+wlbDeyVUJGm3bzpVKpcVyuYyVIPcIBAL3
qiXVgiYaNgwDpmk6qKoKRVEgCAKT8DyPYDDoSCrhdYHrO9qzkdOQvp+E+O04hC+tED63gBs+QiDn
hQgTWJYFWiQUCv2RUEH/g4YNXwdcT/VEJ6xkF8zEDRixq1CnriD94SikH08gikJNS2wmVLDwybON
H3GbNt8DY+YMrDk/tGkvhOFmKPE+pxVJkpDJZMBx3JJAHN+/MTPq8amxdtj8fWjhwzB+diH5ag9y
8V6QubDhUYmmaWwesiwvCYRRtyv9ca9oc37kk3egTbbBiPowP+iGOHGT0A1h7BrS43ehiXHous5E
joCEx3IzF6FMnYMcPgs95iOCW1DDXqTfnEBqsBnRR9shTvYibyhsiBRHwL13dabe7r797uHOx3Kk
m1T2IDfhhTRyAfMDh5Aauox8Ns5aKRQKDNrSsiHSZ6SHoq1i9nkDuNfHkHi2D9loHwtSisUig4ZX
FaSG2pB8cZBUPY+ila0JV1Mj8F/a3DHbfwDq3Mtlb12R/EuNoKN10ylLmv612h6swKIj+CvZRQZk
0ou1hMm/OtveKkE9laxhnSvQ1a//DV9axd5NSHlCAAAAAElFTkSuQmCC";
$_IMAGES["database"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHVSURBVDjLjZPLaiJBFIZNHmJWCeQdMuT1
Mi/gYlARBRUkao+abHUhmhgU0QHtARVxJ0bxhvfGa07Of5Iu21yYFPyLrqrz1f+f6rIRkQ3icca6
ZF39RxesU1VnAVyuVqvJdrvd73Y7+ky8Tk6n87cVYgVcoXixWNByuVSaTqc0Ho+p1+sJpNvtksvl
UhCb3W7/cf/w+BSLxfapVIqSySRlMhnSdZ2GwyHN53OaTCbU7/cFYBgG4RCPx/MKub27+1ur1Xqj
0YjW6zWxCyloNBqUSCSkYDab0WAw+BBJeqLFtQpvGoFqAlAEaZomuc0ocAQnnU7nALiJ3uh8whgn
ttttarVaVCgUpCAUCgnQhMAJ+gG3CsDZa7xh1mw2ZbFSqYgwgsGgbDQhcIWeAHSIoP1pcGeNarUq
gFKpJMLw+/0q72azkYhmPAWIRmM6AGbXc7kc5fN5AXi9XgWACwAguLEAojrfsVGv1yV/sVikcrks
AIfDIYUQHEAoPgLwT3GdzWYNdBfXh3xwApDP5zsqtkoBwuHwaSAQ+OV2u//F43GKRCLEc5ROpwVo
OngvBXj7jU/wwZPPX72DT7RXgDfIT27QEgvfKea9c3m9FsA5IN94zqbw9M9fAEuW+zzj8uLvAAAA
AElFTkSuQmCC";
$_IMAGES["graphics"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAH8SURBVDjLjZPLaxNRFIfHLrpx10WbghXx
H7DQx6p14cadiCs31Y2LLizYhdBFWyhYaFUaUxLUQFCxL61E+0gofWGLRUqGqoWp2JpGG8g4ybTJ
JJm86897Ls4QJIm98DED9/6+mXNmjiAIwhlGE6P1P5xjVAEQiqHVlMlkYvl8/rhQKKAUbB92u91W
SkKrlcLJZBK6rptomoZoNApFUbhElmU4HA4u8YzU1PsmWryroxYrF9CBdDqNbDbLr0QikUAsFkM4
HOaCVCoFesjzpwMuaeXuthYcw4rtvG4KKGxAAgrE43FEIhGzlJQWxE/RirQ6i8/T7XjXV2szBawM
8yDdU91GKaqqInQgwf9xCNmoB7LYgZn+Oud0T121KfiXYokqf8X+5jAyR3NQvtzEq96z4os7lhqz
ieW6TxJN3UVg8yEPqzu38P7xRVy+cPoay52qKDhUf0HaWsC3xRvstd3Qvt9mTWtEOPAJf/+L8oKA
fwfLnil43z7Bkusqdr2X4Btvg1+c5fsVBZJ/H9aXbix/2EAouAVx4zVmHl2BtOrkPako2DsIwule
xKhnG/cmfbg+uIbukXkooR/I5XKcioLu+8/QNTyGzqE36OidQNeDJayLe7yZBuUEv8t9iRIcU6Z4
FprZ36fTxknC7GyCBrBY0ECSE4yzAY1+gyH4Ay9cw2Ifwv9mAAAAAElFTkSuQmCC";
$_IMAGES["image"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGWSURBVBgZpcE/a1NhGMbh3/OeN56cKq2D
p6AoCOKmk4uCn8DNycEOIojilr2TaBfRzVnESQR3Bz+FFDoWA2IjtkRqmpyc97k9qYl/IQV7XSaJ
w4g0VlZfP0m13dwepPbuiH85fyhyWCx4/ubxjU6kkdxWHt69VC6XpZlFBAhwJgwJJHAmRKorbj94
ewvoRBrbuykvT5R2/+lLTp05Tp45STmEJYJBMAjByILxYeM9jzr3GCczGpHGYAQhRM6fO8uFy1fJ
QoaUwCKYEcwwC4QQaGUBd36KTDmQ523axTGQmEcIEBORKQfG1ZDxcA/MkBxXwj1ggCQyS9TVAMmZ
iUxJ8Ln/kS+9PmOvcSW+jrao0mmMH5bzHfa+9UGBmciUBJ+2Fmh1h+yTQCXSkJkdCrpd8btIwwEJ
QnaEkOXMk7XaiF8CUxL/JdKQOwb0Ntc5SG9zHXQNd/ZFGsaEeLa2ChjzXQcqZiKNxSL0vR4unVww
MENMCATib0ZdV+QtE41I42geXt1Ze3dlMNZFdw6Ut6CIvKBhkjiM79Pyq1YUmtkKAAAAAElFTkSu
QmCC";
$_IMAGES["presentation"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHeSURBVDjLjZO/i1NBEMc/u+/lBYxiLkgU
7vRstLEUDyxtxV68ykIMWlocaGHrD1DxSAqxNf4t115jo6DYhCRCEsk733s7u2PxkuiRoBkYdmGZ
z3xndsaoKgDGmC3gLBDxbxsA31U1AKCqzCBXsywbO+e8iOgqz7JM2+32W+AiYFX1GGDHOeen06mm
abrwyWSio9FI+/2+ioj2ej3tdDoLiJm+bimAhgBeUe9RmbkrT5wgT97RaDQoioIQAt1ud7/Var1h
+uq+/s9+PLilw+FwqSRgJ1YpexHSKenHF4DFf/uC3b7CydsPsafraO5IkoTxeEwIARGh2WwCYNUJ
AOmHZ5y4eY/a7h4hPcIdHvDz/fMSnjviOCZJEiqVCtVqdfEl8RygHkz9DLZWQzOHisd9OizfckcU
RRhjMMbMm14CQlEC/NfPjPd2CSJQCEEEDWYBsNZijFkaCqu5Ky+blwl5geaOUDg0c8TnNssSClkE
R1GEtXYZcOruI6ILl1AJqATirW02Hr8sFThBVZfklyXMFdQbbDzdXzm78z4Bx7KXTcwdgzs3yizu
zxAhHvVh4avqBzAzaQa4JiIHgGE9C3EcX7ezhVIgeO9/AWGdYO/9EeDNX+t8frbOdk0FHhj8BvUs
fP0TH5dOAAAAAElFTkSuQmCC";
$_IMAGES["spreadsheet"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIpSURBVDjLjZNPSFRRFMZ/9707o0SOOshM
0x/JFtUmisKBooVEEUThsgi3KS0CN0G2lagWEYkSUdsRWgSFG9sVFAW1EIwQqRZiiDOZY804b967
954249hUpB98y/PjO5zzKREBQCm1E0gDPv9XHpgTEQeAiFCDHAmCoBhFkTXGyL8cBIGMjo7eA3YD
nog0ALJRFNlSqSTlcrnulZUVWV5elsXFRTHGyMLCgoyNjdUhanCyV9ayOSeIdTgnOCtY43DWYY3j
9ulxkskkYRjinCOXy40MDAzcZXCyVzZS38MeKRQKf60EZPXSXInL9y+wLZMkCMs0RR28mJ2grSWJ
Eo+lH9/IpNPE43GKxSLOOYwxpFIpAPTWjiaOtZ+gLdFKlJlD8u00xWP8lO/M5+e5efEB18b70Vqj
lMJai++vH8qLqoa+nn4+fJmiNNPCvMzQnIjzZuo1V88Ns3/HAcKKwfd9tNZorYnFYuuAMLDMfJ3m
+fQznr7L0Vk9zGpLmezB4zx++YggqhAFEZ7n4ft+HVQHVMoB5++cJNWaRrQwMjHM9qCLTFcnJJq5
9WSIMLAopQDwfR/P8+oAbaqWK2eGSGxpxVrDnvQ+3s++4tPnj4SewYscUdUgIiilcM41/uXZG9kN
z9h9aa+EYdjg+hnDwHDq+iGsaXwcZ6XhsdZW+FOqFk0B3caYt4Bic3Ja66NerVACOGttBXCbGbbW
rgJW/VbnXbU6e5tMYIH8L54Xq0cq018+AAAAAElFTkSuQmCC";
$_IMAGES["unknown"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAC4SURBVCjPdZFbDsIgEEWnrsMm7oGGfZro
hxvU+Iq1TyjU60Bf1pac4Yc5YS4ZAtGWBMk/drQBOVwJlZrWYkLhsB8UV9K0BUrPGy9cWbng2CtE
EUmLGppPjRwpbixUKHBiZRS0p+ZGhvs4irNEvWD8heHpbsyDXznPhYFOyTjJc13olIqzZCHBouE0
FRMUjA+s1gTjaRgVFpqRwC8mfoXPPEVPS7LbRaJL2y7bOifRCTEli3U7BMWgLzKlW/CuebZPAAAA
AElFTkSuQmCC";
$_IMAGES["vectorgraphics"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIWSURBVDjLhZNPbxJRFMWhRrYu3NrExIUb
dzWte6M7d34Eo2Hjxm8gwZUxIYEARUKAWgwbV0BpxAW11bpQFrCoCVEMDplhQMow782/enx3WsiU
0jrJ2bz7zu+9e95cHwAfSXzXhFaEVv+j60JLM58HsGIYxsi27SPHcbBIoo5oNBrxQryAVTJPJhPo
uu6q0+mgVquh0WhAlmUX0uv1EIvFZpCp2U8A2sA5h2maYIyhUChA0zTU63UoiuICaJ0OSSaTx5B5
AJnpqqVSCbmNTWxVt9FsNtHv98+05GYyD7AsC5VKBZvFd/j2k6Etc6gjHfLgELKiujeRJGkxQGSA
YDCIx8+eI/ORIb3Lkf0sWvmio9aaoC2NoQ7+QFUHCwFr5XIZ8bfvhZFhq2XgU9tEb2Tj99DCgcTx
9YeOg64GZTCGPQdYEnpaLBbxZl9HfIejo1rg5nGvti3CMyxouonhIYM8ZG7NBWSz2YepVKobiUR+
UXjrwry+wzBm9qnAqD03YHohbsASUP+ly2u+XC7XzmQyt9LpdJc2xuscr0ULU9NUFC6JDiFRCy4g
n88/EWqFw+EEmfL7HK8+8FOAqdmrWYjC7E8kElcCgcAdWmx2LbzY5mCmc+YWXp33H/w1LQehKhPP
ZuK8mTjR0QxwArktQtKpsLHHEarwC81ir+ZOrwewTBCiXr157/7d0PfqjQcvH10w1jT6y/8A/nHJ
HcAgm2AAAAAASUVORK5CYII=";
$_IMAGES["video"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIfSURBVDjLpZNPaBNBGMXfbrubzBqbg4kL
0lJLgiVKE/AP6Kl6UUFQNAeDIAjVS08aELx59GQPAREV/4BeiqcqROpRD4pUNCJSS21OgloISWME
Z/aPb6ARdNeTCz92mO+9N9/w7RphGOJ/nsH+olqtvg+CYJR8q9VquThxuVz+oJTKeZ63Uq/XC38E
0Jj3ff8+OVupVGLbolkzQw5HOqAxQU4wXWWnZrykmYD0QsgAOJe9hpEUcPr8i0GaJ8n2vs/sL2h8
R66TpVfWTdETHWE6GRGKjGiiKNLii5BSLpN7pBHpgMYhMkm8tPUWz3sL2D1wFaY/jvnWcTTaE5Dy
jMfTT5J0XIAiTRYn3ASwZ1MKbTmN7z+KaHUOYqmb1fcPiNa4kQBuyvWAHYfcHGzDgYcx9NKrwJYH
CAyF21JiPWBnXMAQOea6bmn+4ueYGZi8gtymNVobF7BG5prNpjd+eW6X4BSUD0gOdCpzA8MpA/v2
v15kl4+pK0emwHSbjJGBlz+vYM1fQeDrYOBTdzOGvDf6EFNr+LYjHbBgsaCLxr+moNQjU2vYhRXp
gIUOmSWWnsJRfjlOZhrexgtYDZ/gWbetNRbNs6QT10GJglNk64HMaGgbAkoMo5fiFNy7CKDQUGqE
5r38YktxAfSqW7Zt33l66WtkAkACjuNsaLVaDxlw5HdJ/86aYrG4WCgUZD6fX+jv/U0ymfxoWVZo
muZyf+8XqfGP49CCrBUAAAAASUVORK5CYII=";


$_IMAGES["as"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIqSURBVDjLjZPNi1JRGMan/ooWDbSKNq2s
gZqh0UgqKVoOU7OooEWLgZi+JIaYGolaRAS60JXuxJWoIC6E0KAgAzGbCqpFmua393qv9+PoPJ33
THPHcYy68HDPvee8v/e8zznvFIApEn8Octm4Zv6hQ1z7rbgRgE3X9S5jbDgYDDBJfB5er/flKGQU
MEPBiqJAVVVLkiSh0+mgVqsJSLVahc/nsyDbwfsIQAs0TYNhGNDevIX29BnUxx50u13U63UB6Pf7
oCR+v38LMg6gYCOdhnb1GgaeVajnL0CWZTQajT0lCU/GAea379AWFsHu3kJ/4TLUO/etUprNpthJ
pVL5C4Ax6I/WwVbvoe9+AMazMvrHzSMI7YT8aLVakwHs8xdoS1eguC7CeJUBa3fEwkKhgEwmI+pP
8/Ly+fxkgP78BZj7NgYP3ZDn7FDXPGJhKpVCuVwW/tA7HA7vBawdPrJEmZl7hQc7IJ2YtwCxWEyU
IgzmCgaDuwF157kDlVOnC+bKMmS7E8a79zA3PsEs/0Q8Hkc2m4VpmkLkB5URjUa3AMpZ1+uew/lV
mnMw/cZ1qOtPrGOirKVSCclk0gKQQqGQOFYB6NnPKPKsfdNYvgnJdQnsV23XWRMkkUig3W6LMSkQ
COyUIJ+ch3R8Fj+O2j6YHzc2J/VAsVgUEBpHIhHkcjkaDy0P/hh5jBuk0sQ4gO4AXSIa09b595Cv
7YnuHQFME+Q/2nlb1PrTvwGo2K3gWVH3FgAAAABJRU5ErkJggg==";
$_IMAGES["avi"] = $_IMAGES["video"];
$_IMAGES["c"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHdSURBVDjLjZNLS+NgFIad+R0KwuzcSQdd
unTWXraKA4KCuFKcWYqgVbE4TKJWNyqC2oHKoDBeEBF04UpFUVQqUoemSVOTJr2lrb5+5xsTUy+j
gYdc3yfnnOQrAVBCsK2U4WFUvUE546OTcwk82WxWz+fzt4VCAS/B7kMQhB9uiVtQReFkMolUKuWQ
SCSgaRpkWeYSSZIgiqIjscMfSEAPZDIZWJbF94RpmtB1HYqicEE6nQa9xO/3/5OQoM57/qm2a3PG
tyzDtxzF/FYMe6c6F1DAMAzEYrFnLfGZ1A9devqC8o2wpmL8jwJhRcbw7ygGAxJYS7xvuxVVVXkl
kUjkUdAshgP+DRVfureXbPPcuoKe2b/QDKtIQpXQPOLx+KOgf0nGCCu9smHiu7u8IGuDBHRsS6gd
mgmJHEHfLwn9wSgqagc6Xvt8RC6X48MlCeEI2ibDIS8TVDYGBHfAO3ONowvTOacqSEBQNY6gpvOk
p3cxgq8/Q8ZxyISWsDAwfY32sSscnhk8SFAFBIWLBPQZq1sOvjX5LozOqTBaxSu0jF5iYVV+FnZT
JLB/pN0DDTv7WlHvtuQpLwrYxbv/DfIJt47gQfKZDShFN94TZs+afPW6BGUkecdytqGlX3YPTr7m
omspN0YAAAAASUVORK5CYII=";
$_IMAGES["cpp"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAH/SURBVDjLjZPNaxNRFMWrf4cFwV13JVKX
Luta61apIChIV0rblUqhjYpRcUaNboxIqxFTQgVti4hQQTe1C7FFSUmnmvmM85XJzCSpx3efzmTS
RtqBw7yZ9+5v7rl3bg+AHhK7DjClmAZ20UGm/XFcApAKgsBqNptbrVYL3cT2IQjCnSQkCRig4Fqt
Bs/zYtm2DdM0oaoqh8iyDFEUY0gUvI8AdMD3fYRhyO8k13VhWRY0TeOAer0O+kg2m/0LIcDx9LdD
gxff5jJzKjJzCmbe6fi0anEABTiOA13Xd1jiNTlxfT01UVB/CfMG7r/WILxScaOo4FpeBrPEfUdW
DMPgmVQqlTbgtCjls4sGjl16PxuRny5oGH3yA7oZoPjR4BDbqeHlksLrUa1W24DJWRU3Wer9Qw/G
k+kVmA2lGuDKtMQzsVwfl6c3eE3IUgyYeCFjsqCgb3DqQhJwq/gTY7lyV61Jdhtw7qFUSjNA/8m8
kASkc5tYXnN4BvTs1kO23uAdIksx4OjI19Grzys4c7fkfCm5MO0QU483cf5eGcurNq8BWfD8kK11
HtwBoDYeGV4ZO5X57ow8knBWLGP49jqevVF5IKnRaOxQByD6kT6smFj6bHb0OoJsV1cAe/n7f3PQ
RVsx4B/kMCuQRxt7CWZnXT69CUAvQfYwzpFo9Hv/AD332dKni9XnAAAAAElFTkSuQmCC";
$_IMAGES["cs"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJOSURBVDjLjZPbaxNBFMarf4cFwb9AIgXB
R18Enyw+i1gs4g01kphSlPjQeAtNzNqGNLVpNCGhEvBS21Rr0ZIK6ovFiKbNbXNpdpNsstncUz9n
NiauErEDHwMz8/1mzjlz+gD0UZGxh0hFNPAf7SXa3fUpAKparVZoNpvbrVYLvUT2YbFYTEqIEjBA
zZIkoVwud1UsFiEIAjKZjAxJp9NgGKYL6Zh3UQA9UK1WUa/X5ZmqVCqhUCiA4zgZUKlUQC+xWq1t
CAUM3v6+74hu2cH4eUz6OcwFcvgYEmUANYiiiFF3Aq5XHIJRCeqHLOJbFcg5OW6Mqm495fL2NznY
l7OwveYxsZSF6QUHEpIc9+eQgOvuFL6EMjC6wrg4GZZfIwOGbazX8TaPY/qAr5Ms72oOBt8WknwV
em8KWmcCY0/S0E1HcXYyhjNMBAYH2waYF8izl3I4eGLqmjLjz9by+PRNxCMS0k0C0c+yMDjj0Mwm
MOGJ4+Vqtg0Yn+dwf5HH/sG75/4uWzAiwbfCQ+dMYSGQxdhMHMPmMFY+8MgX623AiDu9+YAADg35
LErzHU8SGkcSI4+T0DoSuGRnoZ5mcdIUwdC9zd85OHpjQzP+nMOVmZj4NSZBKNVh9LbN6xslnGai
8CxmMP+Ol81criwntgugZTysDmovTEXEUVcKV8lt520s5kjJvP4MTpkjyApVXCZmvTWKRqMh6w9A
5yO9Xy9ijUgZCi1lL/UEkMUf/+qDHtruAn5BDpAvXKYbOzGTsyW5exWAfgrZQTt3RFu//yfHVsX/
fi5tjwAAAABJRU5ErkJggg==";
$_IMAGES["exe"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEkSURBVCjPbdE9S0IBGIbhxxobWxP8D8r5
I60RLg0NNTS21VBRQwg1aA4VOAWBpBVCFhKUtkVJtPQx9GFFWh49x3P0bvAjjsWzXrzvcAtpREEZ
fQtoACEkpKBVdpouv7NYi3SJkAynWcXExKTCJ6+4PLPeIZJPhksdmzp1vilTwqVGlWhEgR6wsbGp
U+OLt94rGfJ1gIOLi4OFSYV3Sjx5QXdtkiHFx//gjiwlTshyT5LV3T8gwy3HFLnhkCuWmB3qA0Uu
2WGOZVIUmN/ru5CiwAsLNLCI8cg+i3hAggMeiNOgwQbXRJnwghoX5DkiTow0OcLJ8HAbtLpkkzwJ
CuTY4pQppgeFFLJNtxMrzSRFtlnhvDXO6Fk7ll8hb+wZxpChoPzoB6aiXIYcSLDWAAAAAElFTkSu
QmCC";
$_IMAGES["gif"] = $_IMAGES["image"];
$_IMAGES["h"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLjZNLS9xQFMe138C9A/0OynyB
UjeFQjduROi2MMtCEalS0ToLEdQMdEShoKDWRymKigWxII7PhaB9aBFUJjHJpHlnnvbfe27NJcVI
DfwIyT3nd885cOoA1BHsaWQ0MZL/4SHjgciLCJpKpZJVrVava7Ua4mDnkCRpKCqJCpKU7HkefN8X
2LYN0zShqiqXKIqCTCYjJGFyPQkooFgsolwu8zfhui4sy4KmaVwQBAHokmw2+1cSClpSUmr12MP7
LQunii8klOA4DnRdv9USn0koePRiJDW+aTGBjcOLgAewlnjfYSuFQoFXIsvybQF9jG2avIKFPQtz
OyZmcyZMtywkVAnNwzCMeMG7jV+YyFmQ1g30L2kYWitAWtZFJdQOzYREsYLhzwZGGF+OHez/9PD2
k4aeeYUHVyoVPheSELGCwRUdA+zG/VMPeycu3iyo6J5WxDxIQFA1QtCauUwPrOpIPh/vSC+qSC/q
PHn3u4uu2Su8nsrzZKqAoOR/BO2j+Q+DTPC0/2CdSu79qOLVlIyXk3l0zsjomJYxv6ELQYgQPOk7
a2jpOnmcaG57tvuD3fzNxc5XB9sEm0XuyMb5VcCriBI7A/bz9117EMO1ENxImtmAfDq4TzKLdfn2
RgQJktxjnUNo9RN/AFmTwlP7TY1uAAAAAElFTkSuQmCC";
$_IMAGES["iso"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIsSURBVDjLjZNfa9NQGIdnP4cDv8Nkn8PL
6UfwSgQZOoSBYkUvZLN1lMFArQyHrsIuWkE3ug2t1K3O0LXrZotdlzZp0qZp/qc9P8852qyyigs8
F8nJ7znveZN3DMAYg14XKROUyf9wiRIKckOCCcdxNN/3+71eD6Og64hEInPDkmHBJAsbhgHTNAM6
nQ7a7TYkSeKSer2OaDQaSAbhC7efJGY28gZWPrUQTyt4l2lCKLfR7XahaRpkWeYCy7LANonFYr8l
qzt26PUXIxzf7pCfioeS5EI2fVQkG+GVH0hlRVqFjmazeeZIvCc0PBXf1ohu96GZBEnBQMMmcAjg
eH3cWRKQyTf4URRF4ZWIongqoOFURXZpUEOt1YNm+BzDI6AeFKo6IqsF3g9d13k/VFU9FSytK9V8
zUJiR0WbBh+/2cVich+trodvNQeFEwvTsa/8C7Dzs54wUSBYeN+ofq+ageDZmoBX64dQdRcbByaE
qoGbTzPwPA+u63IJIxDMrR2nDkUTR6oPxSJ8ZxYuNlxsHtnYLal48DIH+om5gMGqCQSP3lam7i+X
SMfp40AFsjWCrbKHdMlGpeng2uxHpHM1XgGDhf8S3Fsuhe4+3w9PL+6RvbKGguhAODaRLSq4OvsB
L5JFvutAMCAQDH6kK9fnZyKJAm4tZHFj/jMexnPYzJ3w0kdxRsBu6EPyrzkYQT8Q/JFcpqWabOE8
Yfpul0/vkGCcSc4xzgPY6I//AmC87eKq4rrzAAAAAElFTkSuQmCC";
$_IMAGES["java"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIRSURBVDjLjZPJa1NRFIera/8ECy7dV7tx
kb2UOoDgzo0R3YuLrFwWIVglWQRtN0GCLkIixJDJQJKGQOYBA4akmec5eSFT/XnPsXlNsWIffOTd
d3O+e+6PezcAbBDiuS7YEmz/hxuCq3LdmmBrOp32F4vFyXK5xEWIeWg0mnfrknXBNhWPx2NIkiQz
GAzQ6/XQaDRYUqvVoNVqZQkXGwyGm2q1+k00GkUkEkE4HEYwGGQCgQDS6TSKxSILJpMJaBGdTvdH
YjKZHvp8vuNsNot6vc7QavRLq1UqFcTjcbhcLrmLFZyJ2+0u9Pt9hC1f8OHpDt4/uoO3928zmscK
HD5/gKPPB8jn8yxpNpuoVqtnAqPRiOFwiPGgB/fhPr7uvcJH5S4Ont3Dp5dP8G3/NX4cfedCi8XC
eXQ6nTOBzWaT5vM5J0yTFFy325WhtmkbhN1ux2g04gVlgcfj+UmDUqkEh8OBcrnM7xRaLpdDIpHg
cSqVYihEYr0DL61O6fv9fhQKBd4vhUrpk6DdbsNsNrN8Nptxt7JApVK9EMW9TCbDEgqI2qUOSELv
JPF6vbw9Kj4nEM81pVJ5V6/XH8diMQ6IaLVaLAmFQnA6nfyNslohC05P4RWFQrFLHVitVoYSF2cE
yWSSgxOn9Bx/CWggPv761z24gBNZcCq5JQKSaOIyxeK/I769a4JNklziOq+gq7/5Gx172kZga+XW
AAAAAElFTkSuQmCC";
$_IMAGES["jpg"] = $_IMAGES["image"];
$_IMAGES["jpeg"] = $_IMAGES["image"];
$_IMAGES["js"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHdSURBVDjLjZNPaxNBGIdrLwURLznWgkcv
IrQhRw9FGgy01IY0TVsQ0q6GFkT0kwjJId9AP4AHP4Q9FO2hJ7El2+yf7OzMbja7Sf0578QdNybF
LjwszLu/Z2femZkDMEfI54FkRVL4Dw8l8zqXEawMBgM2HA6vR6MRZiHraDabH7KSrKBA4SAIEIah
xvd9eJ6HbrerJKZpotVqaUkavkMC+iCKIsRxrN6EEAKMMViWpQT9fh/0k3a7PZZkBUPmqXAKCSjA
OYdt21NLUj1JBYW7C6vi6BC8vKWKQXUXQcNA5Nh6KY7jqJl0Op1JwY/Hi7mLp/lT/uoA/OX2WLC3
C9FoQBwfILKulIRmQv1wXfevwHmyuMPXS5Fv1MHrFSTmhSomnUvw/Spo3C+vg3/+pJZDPSGRFvil
NV+8PUZvoziKvn+d3LZvJ/BelMDevIZXK2EQCiUhtMDM53bY5rOIGXtwjU3EVz/HM5Az8eplqPFK
EfzLR91cOg8TPTgr3MudFx+d9owK7KMNVfQOtyQ1OO9qiHsWkiRRUHhKQLuwfH9+1XpfhVVfU0V3
//k4zFwdzjIlSA/Sv8jTOZObBL9uugczuNaCP5K8bFBIhduE5bdC3d6MYIkkt7jOKXT1l34DkIu9
e0agZjoAAAAASUVORK5CYII=";
$_IMAGES["mov"] = $_IMAGES["video"];
$_IMAGES["mp2"] = $_IMAGES["audio"];
$_IMAGES["mp3"] = $_IMAGES["audio"];
$_IMAGES["mp4"] = $_IMAGES["video"];
$_IMAGES["mp4a"] = $_IMAGES["audio"];
$_IMAGES["ogg"] = $_IMAGES["audio"];
$_IMAGES["flac"] = $_IMAGES["audio"];
$_IMAGES["mpeg"] = $_IMAGES["video"];
$_IMAGES["mpg"] = $_IMAGES["video"];
$_IMAGES["odg"] = $_IMAGES["vectorgraphics"];
$_IMAGES["odp"] = $_IMAGES["presentation"];
$_IMAGES["ods"] = $_IMAGES["spreadsheet"];
$_IMAGES["php"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGsSURBVDjLjZNLSwJRFICtFv2AgggS2vQL
DFvVpn0Pi4iItm1KItvWJqW1pYsRemyyNILARbZpm0WtrJ0kbmbUlHmr4+t0z60Z7oSSAx935txz
vrlPBwA4EPKMEVwE9z+ME/qtOkbgqtVqUqPRaDWbTegE6YdQKBRkJazAjcWapoGu6xayLIMoilAo
FKhEEAQIh8OWxCzuQwEmVKtVMAyDtoiqqiBJEhSLRSqoVCqAP+E47keCAvfU5sDQ8MRs/OYNtr1x
2PXdwuJShLLljcFlNAW5HA9khLYp0TUhSYMLHm7PLEDS7zyw3ybRqyfg+TyBtwl2sDP1nKWFiUSa
zFex3tk45sXjL1Aul20CGTs+syVY37igBbwg03eMsfH9gwSsrZ+Doig2QZsdNiZmMkVrKmwc18az
HKELyQrOMEHTDJp8HXu1hostG8dY8PiRngdWMEq467ZwbDxwlIR8XrQLcBvn5k9Gpmd8fn/gHlZW
T20C/D4k8eTDB3yVFKjX6xSbgD1If8G970Q3QbvbPehAyxL8SibJEdaxo5dikqvS28sInCjp4Tqb
4NV3fgPirZ4pD4KS4wAAAABJRU5ErkJggg==";
$_IMAGES["png"] = $_IMAGES["image"];
$_IMAGES["pps"] = $_IMAGES["presentation"];
$_IMAGES["ppsx"] = $_IMAGES["presentation"];
$_IMAGES["ppt"] = $_IMAGES["presentation"];
$_IMAGES["pptx"] = $_IMAGES["presentation"];
$_IMAGES["psd"] = $_IMAGES["graphics"];
$_IMAGES["rb"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIESURBVDjLjZNPTxNBGIexid9CEr8DBr8C
HEiMVoomJiQkxBIM3dgIiaIESJTGGpVtyXIzHhoM4SIe9KAnEi4clQtJEczWFrbdP93d7s7u/JwZ
7XYJBdnkyRxmfs/MvO9OD4AeDvuuMPoY/f/hKiMR5WKCvlarpRNCwiAI0A02D1mW38QlcUE/Dzeb
Tdi2HWEYBhqNBqrVqpBUKhUUCoVI0g5f4gK+wHVdeJ4nRo5lWdB1HbVaTQgcxwHfRFGUvxIuCKYf
zmqZyZ2wKIO8fQ3/1Uv4Sy/QWliAO/sU9qMZmFMS3HfvT1xJ1ITOZJ9RpQi6+RH0y2fQb19BP23C
VhRo+TysXA71+XkcMIk6fAfHK6tQVfWEoESXngNra0C5DHZJYGMDZiaD35IEi41qOo3vc3MoJ1Oo
j92HpmkdQZiVEsHUAzl88hjY3gYIAdbXYQ0MoDo4CH1kBHssvH8jCf3eGKzDXzBNsyNoF/HH7WSJ
ZLPA7i6wtQVnaAhmKoXjxUX8vDkMY3Qcnm6IInJOCS4nEte9QhF+RhInIRMTcFhYvZWCcXcUPmsl
7w6H/w+nBFEb5SLc8TTo8jLq7M4m25mHfd8X8PC5AtHrXB5NdmwRrnfCcc4VCEnpA8jREasp6cpZ
AnrWO+hCGAn+Sa6xAtl84iJhttYSrzcm6OWSCzznNvzp9/4BgwKvG3Zq1eoAAAAASUVORK5CYII=";
$_IMAGES["sln"] = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJQSURBVDjLjZNvSBNxGMeX9O+FOAkaLbeh
ozdGRGiMQqTIlEqJMIig3oxl0YxcgYt6FUZRryLYwpFWCr2wXgjBIMJMYhFjgZSiEXOg5c5N593u
dne7u+2+3V3tT22SBx/uxe/5fu7uuefRAdCpKJdJoVHB9h9qFSryuSJBYzqdpiRJymYyGZRDOYfH
43lULCkW2NRwKpUCy7J5kskkSJJELBbTJARBwOv15iW58AZVoBbwPA9BELS7CsMwoCgK8XhcE3Ac
B/UhPp/vtyQnGBi03pYXjyAbPQuRD2sSbmUFVN9NLJ5ux9DryZJP0nqiChzjl48Oh9oYRPTAXBVk
sgnS0hRWu7uxXG/EfL0ZZ9yjGHgb1t4kGo0WBO6AvcUVsFP9oTZZjlQCP7ZA/r4JpHM3lup2Im6p
RsRai2PX/GjoDWEk8BWJRKIg6P147mfP+CW63d16RUyOQP5SA6rLAsKyA0TNNizvM4D9/A4Tk2Ec
7nuPE0+vgqbpgqBnzLl6vv8N3+x4eEsS0mAvHAJhMoAw6kHUVUF4rkeWHAKXZtA15kDL6C6tkXmB
ffiZs/P+NE7dC4pBhwsJY6USVjBtBO/bCswrbfq2GS+Ce9DwyooHoRvaPPzVxI67IVfHnQA+2JqQ
MFQgur0anP8J5IVmYEopmdbh5YQO1wMu0BxdKlB/44GLg48/HT8J8uBesH6/ViDxC5DnWiHPWjAz
0wleYCGKokaJIDdI/6JMZ1nWEshr7UEZsnnBH8l+ZfpY9WA9YaWW0ba3SGBWJetY5xzq6pt/AY6/
mKmzshF5AAAAAElFTkSuQmCC";
$_IMAGES["sql"] = $_IMAGES["database"];
$_IMAGES["wav"] = $_IMAGES["audio"];
$_IMAGES["wma"] = $_IMAGES["audio"];
$_IMAGES["wmv"] = $_IMAGES["video"];
$_IMAGES["xcf"] = $_IMAGES["graphics"];
$_IMAGES["xls"] = $_IMAGES["spreadsheet"];
$_IMAGES["xlsx"] = $_IMAGES["spreadsheet"];

$_IMAGES_SVG = array();

$_IMAGES_SVG["empty_copy_source"] = "";
$_IMAGES_SVG["directoryup"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1OCA1
OCI+PHBhdGggZmlsbD0iI2ViYmExNiIgZD0iTTUwLjI2OCAxMi41SDI1bC01LTdIMS43MzJDLjc3
NiA1LjUgMCA2LjI3NSAwIDcuMjMyVjQ5Ljk2Yy4wNjkuMDAyLjEzOC4wMDYuMjA1LjAxTDEwLjIy
IDIyLjY1NmExLjU2MyAxLjU2MyAwIDAgMSAxLjUxLTEuMTU2SDUydi03LjI2OGMwLS45NTctLjc3
Ni0xLjczMi0xLjczMi0xLjczMnoiLz48ZyBmaWxsPSJncmVlbiI+PHBhdGggZD0iTTEzLjc3OCA3
LjA3TDE1Ljc1Ny44MzggMS4zODQgOC4yODMgOC44MyAyMi42NTZsMi4wMjgtNi4zOWM3Ljc5MyAy
LjQ3NCAxMi40NTYgNi43IDE0LjYyIDEzLjM5MS45MTYtOC4yODctMS4yODYtMTcuNTY0LTExLjct
MjIuNTg2eiIvPjwvZz48cGF0aCBmaWxsPSIjZWZjZTRhIiBkPSJNNDYuMzI0IDUyLjVIMS41NjVh
MS41NjQgMS41NjQgMCAwIDEtMS41MS0xLjk3M2wxMC4xNjYtMjcuODcxYTEuNTY0IDEuNTY0IDAg
MCAxIDEuNTEtMS4xNTZINTYuNDljMS4wMyAwIDEuNTEuOTg0IDEuNTEgMS45NzNMNDcuODM0IDUx
LjM0NGExLjU2NCAxLjU2NCAwIDAgMS0xLjUxIDEuMTU2eiIvPjwvc3ZnPg==";
$_IMAGES_SVG["md"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzODQg
Mzg0Ij48cGF0aCBmaWxsPSIjZWZlZWVlIiBkPSJNNjQgMHYzODRoMjI0bDk2LTk2VjB6Ii8+PHBh
dGggZmlsbD0iI2FiYWJhYiIgZD0iTTI4OCAyODh2OTZsOTYtOTZ6Ii8+PHBhdGggZmlsbD0iI2Rl
ZGVkZCIgZD0iTTE5MiAzODRoOTZ2LTk2eiIvPjxwYXRoIGZpbGw9IiNjYjU2NDEiIGQ9Ik0wIDk2
djExMmgyNTZWOTZIMHoiLz48ZyBmaWxsPSIjZmZmIj48cGF0aCBkPSJNNTcuNjQ4IDExNy44NTZs
OS43NzYgNDguMzg0aC4yMDhsOS44ODgtNDguMzg0aDIwLjQzMlYxODYuNEg4NS4yOHYtNTQuNzJo
LS4xOTJMNzIuODk2IDE4Ni40SDYyLjE0NGwtMTIuMTkyLTU0LjcyaC0uMTkydjU0LjcySDM3LjA4
OHYtNjguNTQ0aDIwLjU2ek0xNDAuNzY4IDExNy44NGM0LjgxNiAwIDguNjcyLjggMTEuNjMyIDIu
MzY4IDIuOTQ0IDEuNTY4IDUuMjMyIDMuNzkyIDYuODY0IDYuNzIgMS42NDggMi45MTIgMi43MzYg
Ni40IDMuMzEyIDEwLjUxMi41NzYgNC4wOTYuODY0IDguNjcyLjg2NCAxMy43MTIgMCA2LjAxNi0u
MzUyIDExLjI0OC0xLjA4OCAxNS42OTYtLjczNiA0LjQzMi0yIDguMTEyLTMuNzkyIDEwLjk5Mi0x
Ljc5MiAyLjg5Ni00LjE5MiA1LjAyNC03LjIgNi40MzJzLTYuODE2IDIuMTEyLTExLjQyNCAyLjEx
MmgtMjEuODI0VjExNy44NGgyMi42NTZ6bS0yLjY4OCA1OC40YzIuNDMyIDAgNC4zODQtLjQxNiA1
Ljg1Ni0xLjI0OCAxLjQ3Mi0uODMyIDIuNjQtMi4yMDggMy41MDQtNC4xMjguODgtMS45MiAxLjQ1
Ni00LjQ0OCAxLjcyOC03LjYuMjg4LTMuMTIuNDMyLTcuMDI0LjQzMi0xMS42OTYgMC0zLjkwNC0u
MTQ0LTcuMzI4LS4zODQtMTAuMjg4LS4yNTYtMi45NDQtLjc4NC01LjM5Mi0xLjU4NC03LjM0NC0u
ODE2LTEuOTUyLTEuOTg0LTMuNDI0LTMuNTY4LTQuNC0xLjU2OC0uOTkyLTMuNjY0LTEuNTA0LTYu
Mjg4LTEuNTA0aC01Ljg1NnY0OC4yMDhoNi4xNnoiLz48L2c+PC9zdmc+";
$_IMAGES_SVG["txt"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzODQg
Mzg0Ij48cGF0aCBmaWxsPSIjZWZlZWVlIiBkPSJNNjQgMHYzODRoMjI0bDk2LTk2VjB6Ii8+PHBh
dGggZmlsbD0iI2FiYWJhYiIgZD0iTTI4OCAyODh2OTZsOTYtOTZ6Ii8+PHBhdGggZmlsbD0iI2Rl
ZGVkZCIgZD0iTTE5MiAzODRoOTZ2LTk2eiIvPjxwYXRoIGZpbGw9IiMxNTQ5OGEiIGQ9Ik0wIDk2
djExMmgyNTZWOTZIMHoiLz48ZyBmaWxsPSIjZmZmIj48cGF0aCBkPSJNNzYuOTI4IDEyOS4xODRI
NjEuOTUyVjE4Ni40SDQ4LjEyOHYtNTcuMjE2SDMzLjE1MnYtMTEuMzI4aDQzLjc3NnYxMS4zMjh6
TTEwNC44OCAxMTcuODU2bDguODMyIDIyLjE3NiA4LjczNi0yMi4xNzZoMTUuMDU2bC0xNS45MiAz
My44ODggMTcuMDcyIDM0LjY1NmgtMTUuNTUybC05Ljg4OC0yMy4yMzItOS43OTIgMjMuMjMySDg4
LjI3MmwxNy4wNzItMzQuNjU2LTE1LjcyOC0zMy44ODhoMTUuMjY0ek0xOTMuNzYgMTI5LjE4NGgt
MTQuOTkyVjE4Ni40SDE2NC45NnYtNTcuMjE2aC0xNC45NzZ2LTExLjMyOGg0My43NzZ2MTEuMzI4
eiIvPjwvZz48L3N2Zz4=";
$_IMAGES_SVG["doc"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0Njgu
MjkzIDQ2OC4yOTMiPjxwYXRoIGZpbGw9IiNlMWU2ZTkiIGQ9Ik0zMzcuMzM1IDBIOTUuMjE5Qzg0
Ljg3NCAwIDc2LjQ4OCA4LjM4NiA3Ni40ODggMTguNzMydjQzMC44MjljMCAxMC4zNDUgOC4zODYg
MTguNzMyIDE4LjczMiAxOC43MzJoMzA1Ljk1YzEwLjM0NSAwIDE4LjczMi04LjM4NiAxOC43MzIt
MTguNzMyVjgyLjU2N0wzMzcuMzM1IDB6Ii8+PHBhdGggZmlsbD0iIzI3YTJkYiIgZD0iTTQ4LjM5
IDI1OC4wNjdoMzcxLjUxMnYxMjguM0g0OC4zOXoiLz48ZyBmaWxsPSIjZWJmMGYzIj48cGF0aCBk
PSJNMTgyLjcyMiAyOTMuNzQ0YzcuNTY3IDYuODUgMTEuMzQyIDE2LjM3NyAxMS4zNDIgMjguNTgz
IDAgMTIuMjAxLTMuNjY1IDIxLjg2MS0xMS4wMDQgMjguOTcxLTcuMzM5IDcuMTE1LTE4LjU3MSAx
MC42Ny0zMy42ODcgMTAuNjdoLTI2LjA1NnYtNzguNTAxaDI2Ljk1MmMxNC4wNzQgMCAyNC44OTUg
My40MjcgMzIuNDUzIDEwLjI3N3ptLTIuMDIgMjguOTE2YzAtMTcuOTY4LTEwLjI5MS0yNi45NTIt
MzAuODgxLTI2Ljk1MmgtMTMuMjUydjUzLjc5M2gxNC43MTRjOS41MDUgMCAxNi43ODktMi4yNjIg
MjEuODQzLTYuNzk1IDUuMDUzLTQuNTI3IDcuNTc2LTExLjIwOCA3LjU3Ni0yMC4wNDZ6TTI3Ni44
MjggMzUxLjEyOWMtNy45MzMgNy43NS0xNy43MzkgMTEuNjI1LTI5LjQxOSAxMS42MjVzLTIxLjQ4
Ni0zLjg3NS0yOS40MTktMTEuNjI1Yy03Ljk0Mi03Ljc0NS0xMS45MDgtMTcuNDA2LTExLjkwOC0y
OC45NzEgMC0xMS41NyAzLjk2Ni0yMS4yMjYgMTEuOTA4LTI4Ljk3NiA3LjkzMy03Ljc1IDE3Ljcz
OS0xMS42MiAyOS40MTktMTEuNjJzMjEuNDg2IDMuODcgMjkuNDE5IDExLjYyYzcuOTQyIDcuNzUg
MTEuOTA4IDE3LjQwNiAxMS45MDggMjguOTc2IDAgMTEuNTY1LTMuOTY2IDIxLjIyNS0xMS45MDgg
MjguOTcxem0tOS43MDYtNDkuMTMyYy01LjM1Ni01LjUzOC0xMS45MjctOC4zMDctMTkuNzEzLTgu
MzA3LTcuNzg3IDAtMTQuMzU4IDIuNzY5LTE5LjcxMyA4LjMwNy01LjM0NiA1LjU0My04LjAyNCAx
Mi4yNi04LjAyNCAyMC4xNjFzMi42NzggMTQuNjE4IDguMDI0IDIwLjE1NmM1LjM1NiA1LjU0MyAx
MS45MjcgOC4zMTIgMTkuNzEzIDguMzEyIDcuNzg3IDAgMTQuMzU4LTIuNzY5IDE5LjcxMy04LjMx
MiA1LjM0Ni01LjUzOCA4LjAyNC0xMi4yNTYgOC4wMjQtMjAuMTU2cy0yLjY3Ny0xNC42MTktOC4w
MjQtMjAuMTYxek0zNDEuMjk2IDM0OS45NWM0LjU2IDAgOC40OS0uNzYzIDExLjc5LTIuMjk4IDMu
MjktMS41MzUgNi43MzYtMy45ODkgMTAuMzM2LTcuMzU3bDguNTI3IDguNzZjLTguMzA4IDkuMjA4
LTE4LjM5NyAxMy44MTQtMzAuMjYgMTMuODE0LTExLjg3MiAwLTIxLjcxNS0zLjgyLTI5LjUzOC0x
MS40NTYtNy44MjMtNy42MzYtMTEuNzM1LTE3LjI5Ni0xMS43MzUtMjguOTc2czMuOTg1LTIxLjQw
OSAxMS45NjMtMjkuMmM3Ljk2OS03Ljc4MiAxOC4wNDEtMTEuNjc1IDMwLjIwNS0xMS42NzVzMjIu
MzI3IDQuNDkyIDMwLjQ4OCAxMy40NzZsLTguNDE3IDkuMjA4Yy0zLjc0Ny0zLjU5Mi03LjI4NC02
LjEtMTAuNjItNy41MjYtMy4zMjctMS40MjEtNy4yMzgtMi4xMzQtMTEuNzM1LTIuMTM0LTcuOTMz
IDAtMTQuNTk1IDIuNTY4LTE5Ljk4NyA3LjY5NS01LjM5MiA1LjEyNy04LjA4OCAxMS42OC04LjA4
OCAxOS42NTRzMi42NzggMTQuNjM2IDguMDMzIDE5Ljk4N2M1LjM1NyA1LjM1NSAxMS42OTkgOC4w
MjggMTkuMDM4IDguMDI4eiIvPjwvZz48cGF0aCBmaWxsPSIjMmQ5M2JhIiBkPSJNNDguMzkgMzg2
LjM2NGwyOC4wOTggMjYuMTI3di0yNi4xMjd6Ii8+PHBhdGggZmlsbD0iI2ViZjBmMyIgZD0iTTMz
Ny4zMzYgODIuNTY3aDgyLjU2NkwzMzcuMzM1IDB6Ii8+PHBhdGggZmlsbD0iI2Q1ZDZkYiIgZD0i
TTM1My4yMjEgODIuNTY3bDY2LjY4MSAzOC42ODhWODIuNTY3eiIvPjwvc3ZnPg==";
$_IMAGES_SVG["pdf"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0OTYu
NSA0OTYuNSI+PHBhdGggZmlsbD0iI2RhZWZmNiIgZD0iTTMwMi4zOTggMEg1OC42MDhjLTE3LjEy
OSAwLTMxLjAzIDEzLjg4Ni0zMS4wMyAzMS4wM3Y0MzQuNDI0YzAgMTcuMTQ0IDEzLjg4NiAzMS4w
MyAzMS4wMyAzMS4wM2gzNDUuMjljMTcuMTI5IDAgMzEuMDMtMTMuODg2IDMxLjAzLTMxLjAzdi0z
MzIuOTRMMzAyLjM5OCAweiIvPjxwYXRoIGZpbGw9IiM4NmRhZjEiIGQ9Ik0zMDIuMzk4IDB2MTAx
LjQ4NWMwIDE3LjE0NCAxMy44ODYgMzEuMDMgMzEuMDMgMzEuMDNoMTAxLjQ4NUwzMDIuMzk4IDB6
Ii8+PHBhdGggZmlsbD0iI2VkNjI2MiIgZD0iTTE5MS41NTggMjY3Ljk2MmgyNzcuMzY0djEwOC41
NkgxOTEuNTU4eiIvPjxnIGZpbGw9IiNmZmYiPjxwYXRoIGQ9Ik0yMjMuNTk3IDM2Mi44MjJ2LTgx
LjE3NWgyNi4yOThjOS45NjEgMCAxNi40NjIuNDAzIDE5LjQ4NyAxLjIxIDQuNjU1IDEuMjI2IDgu
NTQ5IDMuODYzIDExLjY4MyA3Ljk0NHM0LjcwMSA5LjM1NiA0LjcwMSAxNS44MWMwIDQuOTgtLjkg
OS4xNjktMi43MTUgMTIuNTY3LTEuODE1IDMuMzk4LTQuMTExIDYuMDY2LTYuODg5IDguMDA2LTIu
NzkzIDEuOTM5LTUuNjE2IDMuMjI3LTguNTAyIDMuODQ4LTMuOTEuNzc2LTkuNTczIDEuMTY0LTE3
LjAwNSAxLjE2NGgtMTAuNjl2MzAuNjI3bC0xNi4zNjgtLjAwMXptMTYuMzg0LTY3LjQyOXYyMy4w
NGg4Ljk2OGM2LjQ1NCAwIDEwLjc4My0uNDE5IDEyLjk1NS0xLjI3MnMzLjg3OS0yLjE3MiA1LjEy
LTMuOTg3IDEuODYyLTMuOTEgMS44NjItNi4zMTVjMC0yLjk0OC0uODY5LTUuMzg0LTIuNjA3LTcu
MzA4cy0zLjkyNS0zLjExOS02LjU5NC0zLjZjLTEuOTU1LS4zNzItNS44OTYtLjU1OS0xMS43OTIt
LjU1OWgtNy45MTJ6TTI5OS4xNzEgMjgxLjY2MmgyOS45NmM2Ljc0OSAwIDExLjkuNTEyIDE1LjQ1
MyAxLjU1MiA0Ljc2MyAxLjM5NiA4Ljg0NCAzLjg5NCAxMi4yNDEgNy40NzggMy4zOTggMy41ODQg
NS45NzMgNy45NTkgNy43NTggMTMuMTU3IDEuNzY5IDUuMTgyIDIuNjUzIDExLjU5IDIuNjUzIDE5
LjE5MiAwIDYuNjg3LS44MjIgMTIuNDQzLTIuNDk4IDE3LjI2OC0yLjAzMyA1LjkxMS00LjkzNCAx
MC42OS04LjY4OCAxNC4zMzYtMi44MzkgMi43NjItNi42ODcgNC45MzQtMTEuNTEyIDYuNDctMy42
MTUgMS4xNDgtOC40NTYgMS43MjItMTQuNTA3IDEuNzIyaC0zMC44NDR2LTgxLjE3NWgtLjAxNnpt
MTYuMzk5IDEzLjczMXY1My43NmgxMi4yNDFjNC41NzcgMCA3Ljg4Mi0uMjY0IDkuOTE0LS43NzYg
Mi42NTMtLjY2NyA0Ljg3Mi0xLjc4NCA2LjYyNS0zLjM4MiAxLjc1My0xLjU4MyAzLjE4MS00LjIw
NSA0LjI5OC03LjgzNSAxLjEwMi0zLjYzMSAxLjY2LTguNTk1IDEuNjYtMTQuODY0IDAtNi4yNjgt
LjU1OS0xMS4wOTMtMS42Ni0xNC40NDUtMS4xMDItMy4zNjctMi42NTMtNS45NzMtNC42NTUtNy44
NjYtMS45ODYtMS44NzctNC41My0zLjE2NS03LjU4Ny0zLjgxNy0yLjI4MS0uNTEyLTYuNzgtLjc3
Ni0xMy40NTItLjc3NmwtNy4zODQuMDAxek0zODEuMjMxIDM2Mi44MjJ2LTgxLjE3NWg1NS42NTN2
MTMuNzMxSDM5Ny42M3YxOS4yMDhoMzMuODg1djEzLjczMUgzOTcuNjN2MzQuNDloLTE2LjR2LjAx
NXoiLz48L2c+PHBhdGggZmlsbD0iI2NjZTRlYSIgZD0iTTE1MS4xODcgMzMxLjcxNEg5OS40MTNj
LTguMzc4IDAtMTQuNjMxLTcuMTUyLTE0LjUyMi0xNS41MTV2LS4zMjZjLS4xMDktOC40ODcgNi4x
NDQtMTUuNTE1IDE0LjUyMi0xNS41MTVoNDIuODg0YzkuMzcxIDAgMTcuNzQ5LTcuMDI4IDE4LjMw
OC0xNi40MTUuNDUtMTAuMDU0LTcuNDc4LTE4LjMwOC0xNy40MjQtMTguMzA4bC00MC44NjctLjAx
NmMtOS45MTQgMC0xNy44ODktOC4yODUtMTcuMzYxLTE4LjMyMy40OTYtOS40MDIgOC45MzctMTYu
NDQ2IDE4LjMzOS0xNi40NDZoMzkuNDRjOS4zNzEtLjI2NCAxNi45MjctNy45NDQgMTYuOTI3LTE3
LjM3NyAwLTkuNjA0LTcuODItMTcuNDI0LTE3LjQyNC0xNy40MjRIODYuMDA4Yy04LjAzNyAwLTE0
LjYzMS02LjU5NC0xNC41MjItMTQuNjMxdi0xLjIyNmMtLjU1OS03LjkyOCA1LjgwMy0xNC42MzEg
MTMuNzMxLTE0LjYzMWg2NS45NTVjOS40OTUgMCAxNy44NTgtNy4wMjggMTguNDE2LTE2LjQxNS40
NS0xMC4wNTQtNy40NzgtMTguMzA4LTE3LjQyNC0xOC4zMDhIMTA3LjQ1Yy04LjAzNyAwLTE0LjYz
MS02LjU5NC0xNC42MzEtMTQuNjMxbC4xMjQtMS4wMjR2LS4yMTdjLS4xMDktOC40ODcgNi44MTEt
MTUuNDA3IDE1LjI5OC0xNS40MDdoMzYuMzUyYzkuNDk1IDAgMTcuODU4LTcuMDI4IDE4LjQxNi0x
Ni41MjQuNDUtOS45NDUtNy40NzgtMTguMzA4LTE3LjQyNC0xOC4zMDhoLTQwLjU4OGMtOC4zNzgg
MC0xNC42MzEtNy4wMjgtMTQuNTIyLTE1LjQwN3YtLjIzMy0uMjE3Yy0uMTA5LTguMzc4IDYuMTQ0
LTE1LjQwNyAxNC41MjItMTUuNDA3aDM4LjM2OWM5LjM3MSAwIDE3Ljc0OS03LjA0NCAxOC4zMDgt
MTYuNDE1LjM0MS03LjgyLTQuNDM3LTE0LjQyOS0xMS4yMzMtMTcuMDM2SDU4LjYwOGMtMTcuMTI5
IDAtMzEuMDMgMTMuODg2LTMxLjAzIDMxLjAzVjQ2NS40N2MwIDE3LjE0NCAxMy44ODYgMzEuMDMg
MzEuMDMgMzEuMDNoOTEuMDQzYzYuNDctMi41NDQgMTEuMDkzLTguNzgyIDExLjA5My0xNi4xMzYg
MC05LjYwNC03LjgyLTE3LjI5OS0xNy40MjQtMTcuMjk5SDg2LjAwOGMtOC4wMzcgMC0xNC42MzEt
Ni41OTQtMTQuNTIyLTE0LjYzMVY0NDcuMWMtLjU1OS03LjkyOCA1LjgwMy0xNC41MjIgMTMuNzMx
LTE0LjUyMmg1OC4xNTFjOS40OTUgMCAxNy44NTgtNy4wMjggMTguNDMyLTE2LjUyNC40MzQtOS45
NDUtNy40NzgtMTguMzIzLTE3LjQyNC0xOC4zMjNIMTA3LjQ1Yy04LjAzNyAwLTE0LjYzMS02LjQ3
LTE0LjYzMS0xNC42MzFsLjEwOS0xLjAwOHYtLjIxN2MtLjEwOS04LjQ4NyA2LjgxMS0xNS40MDcg
MTUuMjk4LTE1LjQwN2g0MS45MzdjOS40OTUgMCAxNy44NzMtNy4wMjggMTguNDMyLTE2LjQxNS40
NS0xMC4wNjktNy40NzgtMTguMzM5LTE3LjQwOC0xOC4zMzl6Ii8+PC9zdmc+";
$_IMAGES_SVG["code"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii0zOSAwIDUx
MiA1MTIiPjxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDI9IjQzMy4zMzQiIHkxPSIyNTYiIHkyPSIy
NTYiIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIj48c3RvcCBvZmZzZXQ9IjAiIHN0b3At
Y29sb3I9IiM4MGQ4ZmYiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiNlYTgwZmMiLz48
L2xpbmVhckdyYWRpZW50PjxwYXRoIGZpbGw9InVybCgjYSkiIGQ9Ik00MjcuNDc3IDEyMy44NTVs
LTExOC0xMThjLS4wNjctLjA2Ni0uMTQxLS4xMi0uMjA3LS4xODNBMTkuOTc0IDE5Ljk3NCAwIDAg
MCAyOTUuMzM2IDBIMjBDOC45NTMgMCAwIDguOTUzIDAgMjB2NDcyYzAgMTEuMDQ3IDguOTUzIDIw
IDIwIDIwaDM5My4zMzZjMTEuMDQzIDAgMjAtOC45NTMgMjAtMjBWMTM4YzAtNS40MjItMi4yNDIt
MTAuNTI3LTUuODYtMTQuMTQ1em0tMTEyLjE0MS01NS41N0wzNjUuMDUgMTE4aC00OS43MTV6TTQw
IDQ3MlY0MGgyMzUuMzM2djk4YzAgMTEuMDQ3IDguOTUzIDIwIDIwIDIwaDk4djMxNHptMTQ5LjMz
Ni0yMDBMMTMyIDMxNWw1Ny4zMzYgNDNjOC44MzYgNi42MjkgMTAuNjI1IDE5LjE2NCA0IDI4LTYu
NjYgOC44NzUtMTkuMjA3IDEwLjU5OC0yOCA0bC03OC42NjgtNTljLTEwLjY0OC03Ljk4NC0xMC42
NzItMjMuOTk2IDAtMzJsNzguNjY4LTU5YzguODM2LTYuNjI5IDIxLjM3MS00LjgzNiAyOCA0IDYu
NjI1IDguODM2IDQuODM2IDIxLjM3MS00IDI4em0xNTcuMzMyIDU5TDI2OCAzOTBjLTguODc1IDYu
NjU2LTIxLjQwMiA0Ljc5My0yOC00LTYuNjI5LTguODM2LTQuODM2LTIxLjM3MSA0LTI4bDU3LjMz
Ni00M0wyNDQgMjcyYy04LjgzNi02LjYyOS0xMC42MjktMTkuMTY0LTQtMjhzMTkuMTY0LTEwLjYy
NSAyOC00bDc4LjY2OCA1OWMxMC42NDQgNy45ODQgMTAuNjcyIDIzLjk5NiAwIDMyem0wIDAiLz48
L3N2Zz4=";
$_IMAGES_SVG["webpage"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIg
NTEyIj48bGluZWFyR3JhZGllbnQgaWQ9ImEiIHgyPSI1MTIiIHkxPSIyNTYiIHkyPSIyNTYiIGdy
YWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIj48c3RvcCBvZmZzZXQ9IjAiIHN0b3AtY29sb3I9
IiMwMGYzOGQiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiMwMDllZmYiLz48L2xpbmVh
ckdyYWRpZW50PjxwYXRoIGZpbGw9InVybCgjYSkiIGQ9Ik01MTIgMjU2YzAgMTQxLjM4Ny0xMTQu
NjEzIDI1Ni0yNTYgMjU2UzAgMzk3LjM4NyAwIDI1NiAxMTQuNjEzIDAgMjU2IDBzMjU2IDExNC42
MTMgMjU2IDI1NnptMCAwIi8+PGcgZmlsbD0iI2ZmZiI+PHBhdGggZD0iTTM4OS4yMDMgOTEuNDg0
SDEyMi43OTdjLTIzLjI2MiAwLTQyLjE4NCAxOC45MjYtNDIuMTg0IDQyLjE4OHYyNDQuNjU2YzAg
MjMuMjYyIDE4LjkyMiA0Mi4xODggNDIuMTg0IDQyLjE4OGgyNjYuNDA2YzIzLjI2MiAwIDQyLjE4
NC0xOC45MjYgNDIuMTg0LTQyLjE4OFYxMzMuNjcyYzAtMjMuMjYyLTE4LjkyMi00Mi4xODgtNDIu
MTg0LTQyLjE4OHptLTI2Ni40MDYgMzBoMjY2LjQwNmM2LjcxOSAwIDEyLjE4NCA1LjQ3IDEyLjE4
NCAxMi4xODh2MzkuMzY3SDExMC42MTN2LTM5LjM2N2MwLTYuNzE5IDUuNDY1LTEyLjE4OCAxMi4x
ODQtMTIuMTg4em0yNjYuNDA2IDI2OS4wMzJIMTIyLjc5N2MtNi43MTkgMC0xMi4xODQtNS40Ny0x
Mi4xODQtMTIuMTg4VjIwMy4wNGgyOTAuNzc0djE3NS4yOWMwIDYuNzE4LTUuNDY1IDEyLjE4Ny0x
Mi4xODQgMTIuMTg3em0wIDAiLz48cGF0aCBkPSJNMTU2Ljc3NyAxNDcuMjYyYzAgNy41MDgtNi4w
ODYgMTMuNTkzLTEzLjU5MyAxMy41OTMtNy41MDQgMC0xMy41OS02LjA4NS0xMy41OS0xMy41OTMg
MC03LjUwNCA2LjA4Ni0xMy41OSAxMy41OS0xMy41OSA3LjUwNyAwIDEzLjU5MyA2LjA4NiAxMy41
OTMgMTMuNTl6bTAgME0yMDQuMzUyIDE0Ny4yNjJjMCA3LjUwOC02LjA4NiAxMy41OTMtMTMuNTk0
IDEzLjU5My03LjUwOCAwLTEzLjU5NC02LjA4NS0xMy41OTQtMTMuNTkzIDAtNy41MDQgNi4wODYt
MTMuNTkgMTMuNTk0LTEzLjU5IDcuNTA4IDAgMTMuNTk0IDYuMDg2IDEzLjU5NCAxMy41OXptMCAw
TTI1Ny4wMzEgMjE2LjM0NGMtLjM0Ny0uMDItLjY5MS0uMDI4LTEuMDMxLS4wMjhzLS42ODQuMDA4
LTEuMDMxLjAyOGMtNDMuODkuNTU0LTc5LjQzIDM2LjQxNC03OS40MyA4MC40MzMgMCA0NC4wMiAz
NS41NCA3OS44OCA3OS40MyA4MC40MzQuMzQ3LjAxNi42OTEuMDI3IDEuMDMxLjAyN3MuNjg0LS4w
MTEgMS4wMzEtLjAyN2M0My44OS0uNTU1IDc5LjQzLTM2LjQxNCA3OS40My04MC40MzQgMC00NC4w
Mi0zNS41NC03OS44NzktNzkuNDMtODAuNDMzem01Mi45OTIgNjcuOTMzSDI5Mi43NWMtLjc2Mi0x
Mi4wODItMi42MjUtMjMuNDU3LTUuNDUzLTMzLjI1NyAxMS4yNzMgNy43MzggMTkuNTUgMTkuNTMg
MjIuNzI2IDMzLjI1N3ptLTY1LjczIDI1aDIzLjQxOGMtMS43IDIzLjgxNy03Ljk2OSAzOC4xNDUt
MTEuNzExIDQyLjIwMy0zLjc0Mi00LjA1OC0xMC4wMTItMTguMzg2LTExLjcwNy00Mi4yMDN6bTAt
MjVjMS42OTUtMjMuODE2IDcuOTY5LTM4LjE0NCAxMS43MDctNDIuMjAzIDMuNzQyIDQuMDU5IDEw
LjAxMiAxOC4zODcgMTEuNzEgNDIuMjAzem0tMTkuNTktMzMuMjU3Yy0yLjgyOCA5LjgtNC42ODcg
MjEuMTc1LTUuNDUzIDMzLjI1N2gtMTcuMjczYzMuMTc1LTEzLjcyNiAxMS40NTMtMjUuNTIgMjIu
NzI2LTMzLjI1N3ptLTIyLjcyNiA1OC4yNTdoMTcuMjczYy43NjYgMTIuMDgyIDIuNjI1IDIzLjQ1
NyA1LjQ1MyAzMy4yNTgtMTEuMjczLTcuNzM4LTE5LjU1LTE5LjUzMS0yMi43MjYtMzMuMjU4em04
NS4zMiAzMy4yNTRjMi44MjgtOS43OTcgNC42OTEtMjEuMTcyIDUuNDUzLTMzLjI1NGgxNy4yNzNj
LTMuMTc1IDEzLjcyNy0xMS40NTMgMjUuNTItMjIuNzI2IDMzLjI1NHptMCAwIi8+PC9nPjwvc3Zn
Pg==";
$_IMAGES_SVG["archive"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii00OCAwIDQ2
NCA0NjQiPjxwYXRoIGZpbGw9IiNmZmI3MzAiIGQ9Ik0wIDQ2NGgzNjhWMEg4MEwwIDgwem0wIDAi
Lz48cGF0aCBmaWxsPSIjZmZkOTkxIiBkPSJNODAgODBWMEwwIDgwem0wIDAiLz48cGF0aCBmaWxs
PSIjZjc5ZDFlIiBkPSJNMjM5LjcxIDM0OC4zNTJMMjI4IDI3Mmg4di00OGgtNjR2NDhoOGwtMTEu
NzQyIDc2LjM1MkEyMy45OTUgMjMuOTk1IDAgMCAwIDE5MS45NDYgMzc2SDIxNmEyNC4wMDIgMjQu
MDAyIDAgMCAwIDIzLjcxLTI3LjY0OHptMCAwIi8+PHBhdGggZmlsbD0iI2RlZGVkZSIgZD0iTTIx
OS43NDIgMzQ4LjM1MkwyMDggMjcyaDh2LTQ4aC02NHY0OGg4bC0xMS43NDIgNzYuMzUyYTI0LjAw
MyAyNC4wMDMgMCAwIDAgNS41MTIgMTkuMjg1QTIzLjk5MyAyMy45OTMgMCAwIDAgMTcyIDM3Nmgy
NGEyMy45OTMgMjMuOTkzIDAgMCAwIDE4LjIzLTguMzYzIDI0LjAwMyAyNC4wMDMgMCAwIDAgNS41
MTItMTkuMjg1em0wIDAiLz48cGF0aCBmaWxsPSIjZmQ4ODJmIiBkPSJNMjE2IDQwVjI0aC0yNFYw
aC0xNnYyNGgtMjR2MTZoMjR2MTZoLTI0djE2aDI0djE2aC0yNHYxNmgyNHYxNmgtMjR2MTZoMjR2
MTZoLTI0djE2aDI0djE2aC0yNHYxNmgyNHY0OGgxNnYtNDhoMjR2LTE2aC0yNHYtMTZoMjR2LTE2
aC0yNHYtMTZoMjR2LTE2aC0yNHYtMTZoMjRWODhoLTI0VjcyaDI0VjU2aC0yNFY0MHptMCAwIi8+
PHBhdGggZmlsbD0iI2M3YzdjNyIgZD0iTTE3NiAzMzZoMTZ2MTZoLTE2em0wIDAiLz48cGF0aCBm
aWxsPSIjZjc5ZDFlIiBkPSJNMCA4MGg2NHYxNkgwem0wIDAiLz48cGF0aCBmaWxsPSIjYzdjN2M3
IiBkPSJNMjEwLjQ2NSAyODhMMjA4IDI3MmgtNDhsLTIuNDY1IDE2em0wIDAiLz48cGF0aCBmaWxs
PSIjZjc5ZDFlIiBkPSJNMTc2IDIyNGgxNnYyNGgtMTZ6bTAgMCIvPjwvc3ZnPg==";
$_IMAGES_SVG["directory"] = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0
b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZl
cnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEi
IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93
d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNTgg
NTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDU4IDU4OyIgeG1sOnNwYWNlPSJw
cmVzZXJ2ZSI+DQo8cGF0aCBzdHlsZT0iZmlsbDojRUZDRTRBOyIgZD0iTTQ2LjMyNCw1Mi41SDEu
NTY1Yy0xLjAzLDAtMS43NzktMC45NzgtMS41MS0xLjk3M2wxMC4xNjYtMjcuODcxDQoJYzAuMTg0
LTAuNjgyLDAuODAzLTEuMTU2LDEuNTEtMS4xNTZINTYuNDljMS4wMywwLDEuNTEsMC45ODQsMS41
MSwxLjk3M0w0Ny44MzQsNTEuMzQ0QzQ3LjY1LDUyLjAyNiw0Ny4wMzEsNTIuNSw0Ni4zMjQsNTIu
NXoiLz4NCjxnPg0KCTxwYXRoIHN0eWxlPSJmaWxsOiNFQkJBMTY7IiBkPSJNNTAuMjY4LDEyLjVI
MjVsLTUtN0gxLjczMkMwLjc3Niw1LjUsMCw2LjI3NSwwLDcuMjMyVjQ5Ljk2YzAuMDY5LDAuMDAy
LDAuMTM4LDAuMDA2LDAuMjA1LDAuMDENCgkJbDEwLjAxNS0yNy4zMTRjMC4xODQtMC42ODMsMC44
MDMtMS4xNTYsMS41MS0xLjE1Nkg1MnYtNy4yNjhDNTIsMTMuMjc1LDUxLjIyNCwxMi41LDUwLjI2
OCwxMi41eiIvPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8
L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9n
Pg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4N
CjxnPg0KPC9nPg0KPC9zdmc+DQo=";
$_IMAGES_SVG["unknown"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0MTAg
NDEwIj48cGF0aCBmaWxsPSIjZGVkZGUwIiBkPSJNMTI1IDM0MHYtMzBoODB2LTMwaC04MHYtMzBo
ODB2LTMwaC04MHYtMzBoODB2LTMwaC04MHYtMzBoODBWMGgtODB2NzBINTYuNzA3TDU1IDcxLjc1
VjQxMGgxNTB2LTcweiIvPjxwYXRoIGZpbGw9IiNjZGNkZDAiIGQ9Ik0yMDUgMHY3MGg4MHYzMGgt
ODB2MzBoODB2MzBoLTgwdjMwaDgwdjMwaC04MHYzMGg4MHYzMGgtODB2MTMwaDE1MFYweiIvPjxw
YXRoIGZpbGw9IiNmZmYiIGQ9Ik0yMDUgNzBoODB2MzBoLTgwek0xMjUgMzEwaDgwdjMwaC04MHpN
MTI1IDE2MGgxNjB2LTMwSDEyNXpNMTI1IDIyMGgxNjB2LTMwSDEyNXpNMTI1IDI4MGgxNjB2LTMw
SDEyNXoiLz48cGF0aCBmaWxsPSIjYWNhYmIxIiBkPSJNMTI1IDBMNTUgNzBoNzB6Ii8+PC9zdmc+";
$_IMAGES_SVG["map"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MyA1
MyI+PHBhdGggZmlsbD0iIzQzYjA1YyIgZD0iTTE4IDIzLjI0M0wyLjI1NiA3LjQ5OSAwIDh2NDNs
Mi4wOTktLjQ2NkwxOCAzNC42MzJ6Ii8+PHBhdGggZmlsbD0iIzQ4YTBkYyIgZD0iTTkuMzcyIDQ4
LjkxN0wxOCA0N3YtNi43MTF6Ii8+PHBhdGggZmlsbD0iIzQzYjA1YyIgZD0iTTE4IDRMNi44ODQg
Ni40NyAxOCAxNy41ODZ6TTQ5LjQ0OSAzLjE4NEwzNSA4djkuNjMyeiIvPjxwYXRoIGZpbGw9IiM0
OGEwZGMiIGQ9Ik0zNSAyMy4yODlWNTBsMTgtN1Y1LjI4OXoiLz48cGF0aCBmaWxsPSIjM2Q5OTRm
IiBkPSJNMjYuNTIzIDI2LjEwOUwzNSAxNy42MzJWOEwxOCA0djEzLjU4NnpNMTggMjMuMjQzdjEx
LjM4OWw1LjY5NS01LjY5NHoiLz48cGF0aCBmaWxsPSIjNDM5M2JmIiBkPSJNMTggNDAuMjg5VjQ3
bDE3IDNWMjMuMjg5eiIvPjxwYXRoIGZpbGw9IiNlZmNlNGEiIGQ9Ik0xOCAxNy41ODZMNi44ODQg
Ni40NyAyLjI1NiA3LjQ5OSAxOCAyMy4yNDN6TTE4IDM0LjYzMkwyLjA5OSA1MC41MzRsNy4yNzMt
MS42MTdMMTggNDAuMjg5ek0zNSAyMy4yODlsMTgtMThWMmwtMy41NTEgMS4xODRMMzUgMTcuNjMy
eiIvPjxwYXRoIGZpbGw9IiNkNmI0NDUiIGQ9Ik0yNi41MjMgMjYuMTA5TDE4IDE3LjU4NnY1LjY1
N2w1LjY5NSA1LjY5NUwxOCAzNC42MzJ2NS42NTdsMTctMTd2LTUuNjU3eiIvPjwvc3ZnPg==";
$_IMAGES_SVG["apk"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1OCA1
OCI+PHBhdGggZmlsbD0iIzQyNGE2MCIgZD0iTTQxLjU5NSA1OGgtMjUuMTlBMy40MDYgMy40MDYg
MCAwIDEgMTMgNTQuNTk1VjMuNDA1QTMuNDA2IDMuNDA2IDAgMCAxIDE2LjQwNSAwaDI1LjE4OUEz
LjQwNiAzLjQwNiAwIDAgMSA0NSAzLjQwNXY1MS4xODlBMy40MDYgMy40MDYgMCAwIDEgNDEuNTk1
IDU4eiIvPjxwYXRoIGZpbGw9IiNlN2VjZWQiIGQ9Ik0xMyA2aDMydjQwSDEzeiIvPjxjaXJjbGUg
Y3g9IjI5IiBjeT0iNTIiIHI9IjMiIGZpbGw9IiMyNjJiMzUiLz48cGF0aCBmaWxsPSIjMjYyYjM1
IiBkPSJNMjkgNGgtNGExIDEgMCAxIDEgMC0yaDRhMSAxIDAgMSAxIDAgMnpNMzMgNGgtMWExIDEg
MCAxIDEgMC0yaDFhMSAxIDAgMSAxIDAgMnoiLz48Y2lyY2xlIGN4PSIyOSIgY3k9IjIxIiByPSIx
MiIgZmlsbD0iIzczODNiZiIvPjxwYXRoIGZpbGw9IiM0MjRhNjAiIGQ9Ik0zMS45MzIgMjguNTY3
YTEuMzIyIDEuMzIyIDAgMCAxLS43My0xLjE4MXYtMS4zNThjLjA5Ny0uMTExLjIwOS0uMjUzLjMy
Ny0uNDIxYTcuOTcgNy45NyAwIDAgMCAxLjA3Ni0yLjE2OGMuNDQ0LS4xMzcuNzcxLS41NDcuNzcx
LTEuMDM0di0xLjQ0OWMwLS4zMTktLjE0Mi0uNjA0LS4zNjItLjgwM3YtMi4wOTZzLjQzLTMuMjYx
LTMuOTg2LTMuMjYxLTMuOTg2IDMuMjYxLTMuOTg2IDMuMjYxdjIuMDk2YTEuMDc4IDEuMDc4IDAg
MCAwLS4zNjIuODAzdjEuNDQ5YzAgLjM4Mi4yMDEuNzE4LjUwMS45MTJhNy4xOCA3LjE4IDAgMCAw
IDEuMzExIDIuNzExdjEuMzI1YzAgLjQ4My0uMjY0LjkyOC0uNjg4IDEuMTU5bC0zLjIzMyAxLjc2
M2MtLjIyMS4xMi0uNDE4LjI3MS0uNTk3LjQzOUExMS45MzMgMTEuOTMzIDAgMCAwIDI5IDMzYzIu
NjIzIDAgNS4wNDEtLjg1MSA3LjAxNi0yLjI3OGEyLjg5NCAyLjg5NCAwIDAgMC0uNjIyLS40MjRs
LTMuNDYyLTEuNzMxeiIvPjxwYXRoIGZpbGw9IiNhZmI2YmIiIGQ9Ik0xNiAzN2gyNnY2SDE2eiIv
PjxwYXRoIGZpbGw9IiM2MWI4NzIiIGQ9Ik0xNiAzN2g2djZoLTZ6Ii8+PC9zdmc+";
$_IMAGES_SVG["circuit"] = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0ODAu
MDA4IDQ4MC4wMDgiPjxnIGZpbGw9IiM2NmNlZGIiPjxjaXJjbGUgY3g9IjM0NC4wMDQiIGN5PSI0
NTYiIHI9IjE2Ii8+PHBhdGggZD0iTTgwLjAwNCAyNDhjMCA4Ljg0IDcuMTYgMTYgMTYgMTYgNC40
MTYgMCA4LjQxNi0xLjc5MiAxMS4zMTItNC42ODhzNC42ODgtNi44OTYgNC42ODgtMTEuMzEyYzAt
OC44NC03LjE2LTE2LTE2LTE2cy0xNiA3LjE2LTE2IDE2em04LTIwOGM4Ljg0IDAgMTYtNy4xNiAx
Ni0xNnMtNy4xNi0xNi0xNi0xNi0xNiA3LjE2LTE2IDE2YzAgNC40MTYgMS43OTIgOC40MTYgNC42
ODggMTEuMzEyUzgzLjU4OCA0MCA4OC4wMDQgNDB6Ii8+PGNpcmNsZSBjeD0iNTYuMDA0IiBjeT0i
MTUyIiByPSIxNiIvPjxwYXRoIGQ9Ik04OC4wMDQgMTEyYzguODQgMCAxNi03LjE2IDE2LTE2YTE1
Ljk1IDE1Ljk1IDAgMCAwLTQuNjg4LTExLjMxMkExNS45NSAxNS45NSAwIDAgMCA4OC4wMDQgODBj
LTguODQgMC0xNiA3LjE2LTE2IDE2czcuMTYgMTYgMTYgMTZ6Ii8+PGNpcmNsZSBjeD0iMTg0LjAw
NCIgY3k9IjE1MiIgcj0iMTYiLz48Y2lyY2xlIGN4PSIxNjAuMDA0IiBjeT0iOTYiIHI9IjE2Ii8+
PGNpcmNsZSBjeD0iMjQwLjAwNCIgY3k9IjIwMCIgcj0iMTYiLz48Y2lyY2xlIGN4PSIxOTIuMDA0
IiBjeT0iMjQiIHI9IjE2Ii8+PGNpcmNsZSBjeD0iMjY0LjAwNCIgY3k9IjI0IiByPSIxNiIvPjxj
aXJjbGUgY3g9IjMzNi4wMDQiIGN5PSIyNCIgcj0iMTYiLz48Y2lyY2xlIGN4PSI0NTYuMDA0IiBj
eT0iMTM2IiByPSIxNiIvPjxjaXJjbGUgY3g9IjQwOC4wMDQiIGN5PSI5NiIgcj0iMTYiLz48Y2ly
Y2xlIGN4PSIyODguMDA0IiBjeT0iMjQ4IiByPSIxNiIvPjxjaXJjbGUgY3g9IjM0NC4wMDQiIGN5
PSIyOTYiIHI9IjE2Ii8+PGNpcmNsZSBjeD0iMjE2LjAwNCIgY3k9IjI5NiIgcj0iMTYiLz48cGF0
aCBkPSJNMzgwLjY5MiAyMjAuNjg4QTE1Ljk1IDE1Ljk1IDAgMCAwIDM3Ni4wMDQgMjMyYzAgOC44
NCA3LjE2IDE2IDE2IDE2czE2LTcuMTYgMTYtMTYtNy4xNi0xNi0xNi0xNmExNS45NSAxNS45NSAw
IDAgMC0xMS4zMTIgNC42ODh6TTQ1Ni4wMDQgMjAwYy00LjQxNiAwLTguNDE2IDEuNzkyLTExLjMx
MiA0LjY4OHMtNC42ODggNi44OTYtNC42ODggMTEuMzEyYzAgOC44NCA3LjE2IDE2IDE2IDE2czE2
LTcuMTYgMTYtMTYtNy4xNi0xNi0xNi0xNnoiLz48Y2lyY2xlIGN4PSIzOTIuMDA0IiBjeT0iMzQ0
IiByPSIxNiIvPjxjaXJjbGUgY3g9IjM5Mi4wMDQiIGN5PSI0MDgiIHI9IjE2Ii8+PGNpcmNsZSBj
eD0iMzA0LjAwNCIgY3k9IjE4NCIgcj0iMTYiLz48L2c+PGcgZmlsbD0iIzAxMDEzMyI+PHBhdGgg
ZD0iTTE4NC4wMDQgMTI4Yy0xMC40MTYgMC0xOS4yMTYgNi43MTItMjIuNTI4IDE2SDc4LjUzMmMt
My4zMTItOS4yODgtMTIuMTEyLTE2LTIyLjUyOC0xNi0xMy4yMzIgMC0yNCAxMC43NjgtMjQgMjRz
MTAuNzY4IDI0IDI0IDI0YzEwLjQxNiAwIDE5LjIxNi02LjcxMiAyMi41MjgtMTZoODIuOTUyYzMu
MzEyIDkuMjg4IDEyLjExMiAxNiAyMi41MjggMTYgMTMuMjMyIDAgMjQtMTAuNzY4IDI0LTI0cy0x
MC43NzYtMjQtMjQuMDA4LTI0em0tMTI4IDMyYy00LjQwOCAwLTgtMy41ODQtOC04czMuNTkyLTgg
OC04IDggMy41ODQgOCA4LTMuNTkyIDgtOCA4em0xMjggMGMtNC40MDggMC04LTMuNTg0LTgtOHMz
LjU5Mi04IDgtOCA4IDMuNTg0IDggOC0zLjU5MiA4LTggOHoiLz48cGF0aCBkPSJNMjE2LjAwNCAy
MDBjMCAxMy4yMzIgMTAuNzY4IDI0IDI0IDI0czI0LTEwLjc2OCAyNC0yNGMwLTEwLjQxNi02Ljcx
Mi0xOS4yMTYtMTYtMjIuNTI4VjEyOGMwLTIuMTI4LS44NC00LjE2LTIuMzQ0LTUuNjU2bC0zMi0z
MkE3Ljk3OSA3Ljk3OSAwIDAgMCAyMDguMDA0IDg4aC0yNS40NzJjLTMuMzEyLTkuMjg4LTEyLjEx
Mi0xNi0yMi41MjgtMTYtMTMuMjMyIDAtMjQgMTAuNzY4LTI0IDI0czEwLjc2OCAyNCAyNCAyNGMx
MC40MTYgMCAxOS4yMTYtNi43MTIgMjIuNTI4LTE2aDIyLjE2bDI3LjMxMiAyNy4zMTJ2NDYuMTZj
LTkuMjg4IDMuMzEyLTE2IDEyLjExMi0xNiAyMi41Mjh6bS01Ni05NmMtNC40MDggMC04LTMuNTg0
LTgtOHMzLjU5Mi04IDgtOCA4IDMuNTg0IDggOC0zLjU5MiA4LTggOHptODAgMTA0Yy00LjQwOCAw
LTgtMy41ODQtOC04czMuNTkyLTggOC04YzQuNDA4IDAgOCAzLjU4NCA4IDhzLTMuNTkyIDgtOCA4
em0yNC0yMDhjLTEzLjIzMiAwLTI0IDEwLjc2OC0yNCAyNHMxMC43NjggMjQgMjQgMjRjMy42OTYg
MCA3LjE1Mi0uOTA0IDEwLjI4LTIuNDA4bDc3LjcyIDc3LjcyVjIwMGMwIDIuMTI4Ljg0IDQuMTYg
Mi4zNDQgNS42NTZsMTYuMDY0IDE2LjA2NGMtMS41MDQgMy4xMjgtMi40MDggNi41ODQtMi40MDgg
MTAuMjggMCAxMy4yMzIgMTAuNzY4IDI0IDI0IDI0czI0LTEwLjc2OCAyNC0yNC0xMC43NjgtMjQt
MjQtMjRjLTMuNjk2IDAtNy4xNTIuOTA0LTEwLjI4IDIuNDA4bC0xMy43Mi0xMy43MlYxMjBjMC0y
LjEyOC0uODQtNC4xNi0yLjM0NC01LjY1NkwyODUuNTk2IDM0LjI4YzEuNTA0LTMuMTI4IDIuNDA4
LTYuNTg0IDIuNDA4LTEwLjI4IDAtMTMuMjMyLTEwLjc2OC0yNC0yNC0yNHptMTM2IDIzMmMwIDQu
NDE2LTMuNTkyIDgtOCA4cy04LTMuNTg0LTgtOCAzLjU5Mi04IDgtOCA4IDMuNTg0IDggOHptLTEz
Ni0yMDBjLTQuNDA4IDAtOC0zLjU4NC04LThzMy41OTItOCA4LTggOCAzLjU4NCA4IDgtMy41OTIg
OC04IDh6bTIwMCA4MS40NzJWODBjMC0yLjEyOC0uODQtNC4xNi0yLjM0NC01LjY1NmwtNTYtNTZB
Ny45NzkgNy45NzkgMCAwIDAgNDAwLjAwNCAxNmgtNDEuNDcyQzM1NS4yMiA2LjcxMiAzNDYuNDIg
MCAzMzYuMDA0IDBjLTEzLjIzMiAwLTI0IDEwLjc2OC0yNCAyNHMxMC43NjggMjQgMjQgMjRjMTAu
NDE2IDAgMTkuMjE2LTYuNzEyIDIyLjUyOC0xNmgzOC4xNmw1MS4zMTIgNTEuMzEydjMwLjE2Yy05
LjI4OCAzLjMxMi0xNiAxMi4xMTItMTYgMjIuNTI4IDAgMTMuMjMyIDEwLjc2OCAyNCAyNCAyNHMy
NC0xMC43NjggMjQtMjRjMC0xMC40MTYtNi43MTItMTkuMjE2LTE2LTIyLjUyOHpNMzM2LjAwNCAz
MmMtNC40MDggMC04LTMuNTg0LTgtOHMzLjU5Mi04IDgtOCA4IDMuNTg0IDggOC0zLjU5MiA4LTgg
OHptMTIwIDExMmMtNC40MDggMC04LTMuNTg0LTgtOHMzLjU5Mi04IDgtOCA4IDMuNTg0IDggOC0z
LjU5MiA4LTggOHptLTg4IDE1MmMwLTEzLjIzMi0xMC43NjgtMjQtMjQtMjQtMTAuNDE2IDAtMTku
MjE2IDYuNzEyLTIyLjUyOCAxNmgtODIuOTQ0Yy0zLjMxMi05LjI4OC0xMi4xMTItMTYtMjIuNTI4
LTE2cy0xOS4yMTYgNi43MTItMjIuNTI4IDE2aC00Ni4xNmwtMjkuNzItMjkuNzJjMS41MDQtMy4x
MjggMi40MDgtNi41ODQgMi40MDgtMTAuMjggMC0xMy4yMzItMTAuNzY4LTI0LTI0LTI0cy0yNCAx
MC43NjgtMjQgMjQgMTAuNzY4IDI0IDI0IDI0YzMuNjk2IDAgNy4xNTItLjkwNCAxMC4yOC0yLjQw
OGwzMi4wNjQgMzIuMDY0YTcuOTc5IDcuOTc5IDAgMCAwIDUuNjU2IDIuMzQ0aDQ5LjQ3MmMzLjMx
MiA5LjI4OCAxMi4xMTIgMTYgMjIuNTI4IDE2czE5LjIxNi02LjcxMiAyMi41MjgtMTZoODIuOTUy
YTIzLjk3NiAyMy45NzYgMCAwIDAgMTQuNTI4IDE0LjUyOFY0MzMuNDhjLTkuMjg4IDMuMzEyLTE2
IDEyLjExMi0xNiAyMi41MjggMCAxMy4yMzIgMTAuNzY4IDI0IDI0IDI0czI0LTEwLjc2OCAyNC0y
NGMwLTEwLjQxNi02LjcxMi0xOS4yMTYtMTYtMjIuNTI4VjMxOC41MjhjOS4yOC0zLjMxMiAxNS45
OTItMTIuMTEyIDE1Ljk5Mi0yMi41Mjh6bS0yODAtNDhjMC00LjQxNiAzLjU5Mi04IDgtOHM4IDMu
NTg0IDggOC0zLjU5MiA4LTggOC04LTMuNTg0LTgtOHptMjU2IDQwYzQuNDA4IDAgOCAzLjU4NCA4
IDhzLTMuNTkyIDgtOCA4LTgtMy41ODQtOC04IDMuNTkyLTggOC04em0tMTI4IDE2Yy00LjQwOCAw
LTgtMy41ODQtOC04czMuNTkyLTggOC04IDggMy41ODQgOCA4LTMuNTkyIDgtOCA4em0xMjggMTYw
Yy00LjQwOCAwLTgtMy41ODQtOC04czMuNTkyLTggOC04IDggMy41ODQgOCA4LTMuNTkyIDgtOCA4
eiIvPjxwYXRoIGQ9Ik0yODAuMDA0IDE4NGMwIDEzLjIzMiAxMC43NjggMjQgMjQgMjRzMjQtMTAu
NzY4IDI0LTI0YzAtMTAuNDE2LTYuNzEyLTE5LjIxNi0xNi0yMi41MjhWMTM2YzAtMi4xMjgtLjg0
LTQuMTYtMi4zNDQtNS42NTZMMjEzLjU5NiAzNC4yOGMxLjUwNC0zLjEyOCAyLjQwOC02LjU4NCAy
LjQwOC0xMC4yOCAwLTEzLjIzMi0xMC43NjgtMjQtMjQtMjQtMTAuNDE2IDAtMTkuMjE2IDYuNzEy
LTIyLjUyOCAxNmgtMzMuNDcyYy00LjQxNiAwLTggMy41NzYtOCA4djIwLjY4OGwtMjkuNzIgMjku
NzJDOTUuMTU2IDcyLjkwNCA5MS43IDcyIDg4LjAwNCA3MmMtMTMuMjMyIDAtMjQgMTAuNzY4LTI0
IDI0czEwLjc2OCAyNCAyNCAyNCAyNC0xMC43NjggMjQtMjRjMC0zLjY5Ni0uOTA0LTcuMTUyLTIu
NDA4LTEwLjI4bDMyLjA2NC0zMi4wNjRBNy45NjUgNy45NjUgMCAwIDAgMTQ0LjAwNCA0OFYzMmgy
NS40NzJjMy4zMTIgOS4yODggMTIuMTEyIDE2IDIyLjUyOCAxNiAzLjY5NiAwIDcuMTUyLS45MDQg
MTAuMjgtMi40MDhsOTMuNzIgOTMuNzJ2MjIuMTZjLTkuMjg4IDMuMzEyLTE2IDEyLjExMi0xNiAy
Mi41Mjh6bS0xOTItODBjLTQuNDA4IDAtOC0zLjU4NC04LThzMy41OTItOCA4LTggOCAzLjU4NCA4
IDgtMy41OTIgOC04IDh6bTEwNC03MmMtNC40MDggMC04LTMuNTg0LTgtOHMzLjU5Mi04IDgtOCA4
IDMuNTg0IDggOC0zLjU5MiA4LTggOHptMTEyIDE2MGMtNC40MDggMC04LTMuNTg0LTgtOHMzLjU5
Mi04IDgtOCA4IDMuNTg0IDggOC0zLjU5MiA4LTggOHoiLz48cGF0aCBkPSJNMjYuMzQ4IDIwNS42
NTZBNy45NzkgNy45NzkgMCAwIDAgMzIuMDA0IDIwOGg4NC42ODhsNDUuNjU2IDQ1LjY1NmE3Ljk3
OSA3Ljk3OSAwIDAgMCA1LjY1NiAyLjM0NGg5Ny40NzJjMy4zMTIgOS4yODggMTIuMTEyIDE2IDIy
LjUyOCAxNiAxMy4yMzIgMCAyNC0xMC43NjggMjQtMjRzLTEwLjc2OC0yNC0yNC0yNGMtMTAuNDE2
IDAtMTkuMjE2IDYuNzEyLTIyLjUyOCAxNmgtOTQuMTZsLTQ1LjY1Ni00NS42NTZhNy45NzkgNy45
NzkgMCAwIDAtNS42NTYtMi4zNDRIMzUuMzE2bC0xOS4zMTItMTkuMzEydi02NS4zNzZsNjEuNzIt
NjEuNzJDODAuODUyIDQ3LjA5NiA4NC4zMDggNDggODguMDA0IDQ4YzEzLjIzMiAwIDI0LTEwLjc2
OCAyNC0yNHMtMTAuNzY4LTI0LTI0LTI0LTI0IDEwLjc2OC0yNCAyNGMwIDMuNjk2LjkwNCA3LjE1
MiAyLjQwOCAxMC4yOEwyLjM0OCA5OC4zNDRBNy45NjUgNy45NjUgMCAwIDAgLjAwNCAxMDR2NzJj
MCAyLjEyOC44NCA0LjE2IDIuMzQ0IDUuNjU2bDI0IDI0ek0yODguMDA0IDI0MGM0LjQwOCAwIDgg
My41ODQgOCA4cy0zLjU5MiA4LTggOC04LTMuNTg0LTgtOCAzLjU5Mi04IDgtOHptLTIwMC0yMjRj
NC40MDggMCA4IDMuNTg0IDggOHMtMy41OTIgOC04IDgtOC0zLjU4NC04LTggMy41OTItOCA4LTh6
TTQ2MS42NiAyNTMuNjU2YTcuOTY1IDcuOTY1IDAgMCAwIDIuMzQ0LTUuNjU2di05LjQ3MmM5LjI4
OC0zLjMxMiAxNi0xMi4xMTIgMTYtMjIuNTI4IDAtMTMuMjMyLTEwLjc2OC0yNC0yNC0yNC0zLjY5
NiAwLTcuMTUyLjkwNC0xMC4yOCAyLjQwOGwtMjkuNzItMjkuNzJ2LTQ2LjE2YzkuMjg4LTMuMzEy
IDE2LTEyLjExMiAxNi0yMi41MjggMC0xMy4yMzItMTAuNzY4LTI0LTI0LTI0cy0yNCAxMC43Njgt
MjQgMjRjMCAxMC40MTYgNi43MTIgMTkuMjE2IDE2IDIyLjUyOFYxNjhjMCAyLjEyOC44NCA0LjE2
IDIuMzQ0IDUuNjU2bDMyLjA2NCAzMi4wNjRjLTEuNTA0IDMuMTI4LTIuNDA4IDYuNTg0LTIuNDA4
IDEwLjI4IDAgMTAuNDE2IDYuNzEyIDE5LjIxNiAxNiAyMi41Mjh2Ni4xNmwtNjEuNjU2IDYxLjY1
NmE3Ljk2NSA3Ljk2NSAwIDAgMC0yLjM0NCA1LjY1NnY5LjQ3MmMtOS4yODggMy4zMTItMTYgMTIu
MTEyLTE2IDIyLjUyOHM2LjcxMiAxOS4yMTYgMTYgMjIuNTI4djE4Ljk1MmMtOS4yODggMy4zMTIt
MTYgMTIuMTEyLTE2IDIyLjUyOCAwIDEzLjIzMiAxMC43NjggMjQgMjQgMjRzMjQtMTAuNzY4IDI0
LTI0YzAtMTAuNDE2LTYuNzEyLTE5LjIxNi0xNi0yMi41Mjh2LTE4Ljk1MmM5LjI4OC0zLjMxMiAx
Ni0xMi4xMTIgMTYtMjIuNTI4cy02LjcxMi0xOS4yMTYtMTYtMjIuNTI4di02LjE2bDYxLjY1Ni02
MS42NTZ6TTQwOC4wMDQgODhjNC40MDggMCA4IDMuNTg0IDggOHMtMy41OTIgOC04IDgtOC0zLjU4
NC04LTggMy41OTItOCA4LTh6bTU2IDEyOGMwIDQuNDE2LTMuNTkyIDgtOCA4cy04LTMuNTg0LTgt
OCAzLjU5Mi04IDgtOCA4IDMuNTg0IDggOHptLTcyIDIwMGMtNC40MDggMC04LTMuNTg0LTgtOHMz
LjU5Mi04IDgtOCA4IDMuNTg0IDggOC0zLjU5MiA4LTggOHptMC02NGMtNC40MDggMC04LTMuNTg0
LTgtOHMzLjU5Mi04IDgtOCA4IDMuNTg0IDggOC0zLjU5MiA4LTggOHoiLz48L2c+PC9zdmc+";


$_IMAGES_SVG["out"] = $_IMAGES_SVG["circuit"];
$_IMAGES_SVG["elf"] = $_IMAGES_SVG["circuit"];
$_IMAGES_SVG["bin"] = $_IMAGES_SVG["circuit"];
$_IMAGES_SVG["zip"] = $_IMAGES_SVG["archive"];
$_IMAGES_SVG["7z"] = $_IMAGES_SVG["archive"];
$_IMAGES_SVG["bz2"] = $_IMAGES_SVG["archive"];
$_IMAGES_SVG["cab"] = $_IMAGES_SVG["archive"];
$_IMAGES_SVG["gz"] = $_IMAGES_SVG["archive"];
$_IMAGES_SVG["rar"] = $_IMAGES_SVG["archive"];
$_IMAGES_SVG["tar"] = $_IMAGES_SVG["archive"];
$_IMAGES_SVG["tgz"] = $_IMAGES_SVG["archive"];
$_IMAGES_SVG["htm"] = $_IMAGES_SVG["webpage"];
$_IMAGES_SVG["html"] = $_IMAGES_SVG["webpage"];
$_IMAGES_SVG["xml"] = $_IMAGES_SVG["code"];
$_IMAGES_SVG["css"] = $_IMAGES_SVG["code"];
$_IMAGES_SVG["docx"] = $_IMAGES_SVG["doc"];
$_IMAGES_SVG["odt"] = $_IMAGES_SVG["doc"];
$_IMAGES_SVG["ipa"] = $_IMAGES_SVG["apk"];


/************ Attribution ************/
//<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.flaticon.com/authors/becris" title="Becris">Becris</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.flaticon.com/authors/vectors-market" title="Vectors Market">Vectors Market</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.flaticon.com/authors/dinosoftlabs" title="DinosoftLabs">DinosoftLabs</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
//<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
/************ Attribution ************/

/***************************************************************************/
/*   HERE COMES THE CODE.                                                  */
/*   DON'T CHANGE UNLESS YOU KNOW WHAT YOU ARE DOING ;)                    */
/***************************************************************************/

//
// The class that displays images (icons and thumbnails)
//
class ImageServer
{
	//
	// Checks if an image is requested and displays one if needed
	//
	public static function showImage()
	{
		global $_IMAGES;
		global $_IMAGES_SVG;
		if(isset($_GET['img']))
		{
			$mtime = gmdate('r', filemtime($_SERVER['SCRIPT_FILENAME']));
			$etag = md5($mtime.$_SERVER['SCRIPT_FILENAME']);

			if ((isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $mtime)
				|| (isset($_SERVER['HTTP_IF_NONE_MATCH']) && str_replace('"', '', stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) == $etag))
			{
				header('HTTP/1.1 304 Not Modified');
				return true;
			}
			else
			{
				header('ETag: "'.$etag.'"');
				header('Last-Modified: '.$mtime);
				if(strlen($_GET['img']) > 0 && isset($_IMAGES_SVG[$_GET['img']]))
				{
					header('Content-type: image/svg+xml');
					print base64_decode($_IMAGES_SVG[$_GET['img']]);
				}
				else if(strlen($_GET['img']) > 0 && isset($_IMAGES[$_GET['img']]))
				{
					header('Content-type: image/gif');
					print base64_decode($_IMAGES[$_GET['img']]);
				}
				else
				{
					header('Content-type: image/svg+xml');
					print base64_decode($_IMAGES_SVG["unknown"]);
				}
			}
			return true;
		}
		else if(isset($_GET['thumb']))
		{
			if(strlen($_GET['thumb']) > 0 && EncodeExplorer::getConfig('thumbnails') == true)
			{
				ImageServer::showThumbnail($_GET['thumb']);
			}
			return true;
		}
		return false;
	}

	public static function isEnabledPdf()
	{
		if(class_exists("Imagick"))
			return true;
		return false;
	}

	public static function openPdf($file)
	{
		if(!ImageServer::isEnabledPdf())
			return null;

		$im = new Imagick($file.'[0]');
		$im->setImageFormat( "png" );
		$str = $im->getImageBlob();
		$im2 = imagecreatefromstring($str);
		return $im2;
	}

	//
	// Creates and returns a thumbnail image object from an image file
	//
	public static function createThumbnail($file)
	{
		if(is_int(EncodeExplorer::getConfig('thumbnails_width')))
			$max_width = EncodeExplorer::getConfig('thumbnails_width');
		else
			$max_width = 200;

		if(is_int(EncodeExplorer::getConfig('thumbnails_height')))
			$max_height = EncodeExplorer::getConfig('thumbnails_height');
		else
			$max_height = 200;

		if(File::isPdfFile($file))
			$image = ImageServer::openPdf($file);
		else
			$image = ImageServer::openImage($file);
		if($image == null)
			return;

		imagealphablending($image, true);
		imagesavealpha($image, true);

		$width = imagesx($image);
		$height = imagesy($image);

		$new_width = $max_width;
		$new_height = $max_height;
		if(($width/$height) > ($new_width/$new_height))
			$new_height = $new_width * ($height / $width);
		else
			$new_width = $new_height * ($width / $height);

		if($new_width >= $width && $new_height >= $height)
		{
			$new_width = $width;
			$new_height = $height;
		}

		$new_image = ImageCreateTrueColor($new_width, $new_height);
		imagealphablending($new_image, true);
		imagesavealpha($new_image, true);
		$trans_colour = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
		imagefill($new_image, 0, 0, $trans_colour);

		imagecopyResampled ($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

		return $new_image;
	}

	//
	// Function for displaying the thumbnail.
	// Includes attempts at cacheing it so that generation is minimised.
	//
	public static function showThumbnail($file)
	{
		if(filemtime($file) < filemtime($_SERVER['SCRIPT_FILENAME']))
			$mtime = gmdate('r', filemtime($_SERVER['SCRIPT_FILENAME']));
		else
			$mtime = gmdate('r', filemtime($file));

		$etag = md5($mtime.$file);

		if ((isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $mtime)
			|| (isset($_SERVER['HTTP_IF_NONE_MATCH']) && str_replace('"', '', stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) == $etag))
		{
			header('HTTP/1.1 304 Not Modified');
			return;
		}
		else
		{
			header('ETag: "'.$etag.'"');
			header('Last-Modified: '.$mtime);
			header('Content-Type: image/png');
			$image = ImageServer::createThumbnail($file);
			imagepng($image);
		}
	}

	//
	// A helping function for opening different types of image files
	//
	public static function openImage ($file)
	{
		$size = getimagesize($file);
		switch($size["mime"])
		{
			case "image/jpeg":
				$im = imagecreatefromjpeg($file);
			break;
			case "image/gif":
				$im = imagecreatefromgif($file);
			break;
			case "image/png":
				$im = imagecreatefrompng($file);
			break;
			default:
				$im=null;
			break;
		}
		return $im;
	}
}

//
// The class for logging user activity
//
class Logger
{
	public static function log($message)
	{
		global $encodeExplorer;
		if(strlen(EncodeExplorer::getConfig('log_file')) > 0)
		{
			if(Location::isFileWritable(EncodeExplorer::getConfig('log_file')))
			{
				$message = "[" . date("Y-m-d h:i:s", mktime()) . "] ".$message." (".$_SERVER["HTTP_USER_AGENT"].")\n";
				error_log($message, 3, EncodeExplorer::getConfig('log_file'));
			}
			else
				$encodeExplorer->setErrorString("log_file_permission_error");
		}
	}

	public static function logAccess($path, $isDir)
	{
		$message = $_SERVER['REMOTE_ADDR']." ".GateKeeper::getUserName()." accessed ";
		$message .= $isDir?"dir":"file";
		$message .= " ".$path;
		Logger::log($message);
	}

	public static function logQuery()
	{
		if(isset($_POST['log']) && strlen($_POST['log']) > 0)
		{
			Logger::logAccess($_POST['log'], false);
			return true;
		}
		else
			return false;
	}

	public static function logCreation($path, $isDir)
	{
		$message = $_SERVER['REMOTE_ADDR']." ".GateKeeper::getUserName()." created ";
		$message .= $isDir?"dir":"file";
		$message .= " ".$path;
		Logger::log($message);
	}

	public static function emailNotification($path, $isFile)
	{
		if(strlen(EncodeExplorer::getConfig('upload_email')) > 0)
		{
			$message = "This is a message to let you know that ".GateKeeper::getUserName()." ";
			$message .= ($isFile?"uploaded a new file":"created a new directory")." in Encode Explorer.\n\n";
			$message .= "Path : ".$path."\n";
			$message .= "IP : ".$_SERVER['REMOTE_ADDR']."\n";
			mail(EncodeExplorer::getConfig('upload_email'), "Upload notification", $message);
		}
	}
}

//
// The class controls logging in and authentication
//
class GateKeeper
{
	public static function init()
	{
		global $encodeExplorer;
		if(strlen(EncodeExplorer::getConfig("session_name")) > 0)
				session_name(EncodeExplorer::getConfig("session_name"));

		if(count(EncodeExplorer::getConfig("users")) > 0)
			session_start();
		else
			return;

		if(isset($_GET['logout']))
		{
			$_SESSION['ee_user_name'] = null;
			$_SESSION['ee_user_pass'] = null;
		}

		if(isset($_POST['user_pass']) && strlen($_POST['user_pass']) > 0)
		{
			if(GateKeeper::isUser((isset($_POST['user_name'])?$_POST['user_name']:""), $_POST['user_pass']))
			{
				$_SESSION['ee_user_name'] = isset($_POST['user_name'])?$_POST['user_name']:"";
				$_SESSION['ee_user_pass'] = $_POST['user_pass'];

				$addr  = $_SERVER['PHP_SELF'];
				$param = '';

				if(isset($_GET['m']))
					$param .= (strlen($param) == 0 ? '?m' : '&m');

				if(isset($_GET['s']))
					$param .= (strlen($param) == 0 ? '?s' : '&s');

				if(isset($_GET['dir']) && strlen($_GET['dir']) > 0)
				{
					$param .= (strlen($param) == 0 ? '?dir=' : '&dir=');
					$param .= urlencode($_GET['dir']);
				}
				header( "Location: ".$addr.$param);
			}
			else
				$encodeExplorer->setErrorString("wrong_pass");
		}
	}

	public static function isUser($userName, $userPass)
	{
		foreach(EncodeExplorer::getConfig("users") as $user)
		{
			if($user[1] == $userPass)
			{
				if(strlen($userName) == 0 || $userName == $user[0])
				{
					return true;
				}
			}
		}
		return false;
	}

	public static function isLoginRequired()
	{
		if(EncodeExplorer::getConfig("require_login") == false){
			return false;
		}
		return true;
	}

	public static function isUserLoggedIn()
	{
		if(isset($_SESSION['ee_user_name'], $_SESSION['ee_user_pass']))
		{
			if(GateKeeper::isUser($_SESSION['ee_user_name'], $_SESSION['ee_user_pass']))
				return true;
		}
		return false;
	}

	public static function isAccessAllowed()
	{
		if(!GateKeeper::isLoginRequired() || GateKeeper::isUserLoggedIn())
			return true;
		return false;
	}

	public static function isUploadAllowed(){
		if(EncodeExplorer::getConfig("upload_enable") == true && GateKeeper::isUserLoggedIn() == true && GateKeeper::getUserStatus() == "admin")
			return true;
		return false;
	}

	public static function isNewdirAllowed(){
		if(EncodeExplorer::getConfig("newdir_enable") == true && GateKeeper::isUserLoggedIn() == true && GateKeeper::getUserStatus() == "admin")
			return true;
		return false;
	}

	public static function isDeleteAllowed(){
		if(EncodeExplorer::getConfig("delete_enable") == true && GateKeeper::isUserLoggedIn() == true && GateKeeper::getUserStatus() == "admin")
			return true;
		return false;
	}

	public static function getUserStatus(){
		if(GateKeeper::isUserLoggedIn() == true && EncodeExplorer::getConfig("users") != null && is_array(EncodeExplorer::getConfig("users"))){
			foreach(EncodeExplorer::getConfig("users") as $user){
				if($user[0] != null && $user[0] == $_SESSION['ee_user_name'])
					return $user[2];
			}
		}
		return null;
	}

	public static function getUserName()
	{
		if(GateKeeper::isUserLoggedIn() == true && isset($_SESSION['ee_user_name']) && strlen($_SESSION['ee_user_name']) > 0)
			return $_SESSION['ee_user_name'];
		if(isset($_SERVER["REMOTE_USER"]) && strlen($_SERVER["REMOTE_USER"]) > 0)
			return $_SERVER["REMOTE_USER"];
		if(isset($_SERVER['PHP_AUTH_USER']) && strlen($_SERVER['PHP_AUTH_USER']) > 0)
			return $_SERVER['PHP_AUTH_USER'];
		return "an anonymous user";
	}

	public static function showLoginBox(){
		if(!GateKeeper::isUserLoggedIn() && count(EncodeExplorer::getConfig("users")) > 0)
			return true;
		return false;
	}
}

//
// The class for any kind of file managing (new folder, upload, etc).
//
class FileManager
{
	/* Obsolete code
	function checkPassword($inputPassword)
	{
		global $encodeExplorer;
		if(strlen(EncodeExplorer::getConfig("upload_password")) > 0 && $inputPassword == EncodeExplorer::getConfig("upload_password"))
		{
			return true;
		}
		else
		{
			$encodeExplorer->setErrorString("wrong_password");
			return false;
		}
	}
	*/
	function newFolder($location, $dirname)
	{
		global $encodeExplorer;
		if(strlen($dirname) > 0)
		{
			$forbidden = array(".", "/", "\\");
			for($i = 0; $i < count($forbidden); $i++)
			{
				$dirname = str_replace($forbidden[$i], "", $dirname);
			}

			if(!$location->uploadAllowed())
			{
				// The system configuration does not allow uploading here
				$encodeExplorer->setErrorString("upload_not_allowed");
			}
			else if(!$location->isWritable())
			{
				// The target directory is not writable
				$encodeExplorer->setErrorString("upload_dir_not_writable");
			}
			else if(!mkdir($location->getDir(true, false, false, 0).$dirname, EncodeExplorer::getConfig("new_dir_mode")))
			{
				// Error creating a new directory
				$encodeExplorer->setErrorString("new_dir_failed");
			}
			else if(!chmod($location->getDir(true, false, false, 0).$dirname, EncodeExplorer::getConfig("new_dir_mode")))
			{
				// Error applying chmod
				$encodeExplorer->setErrorString("chmod_dir_failed");
			}
			else
			{
				// Directory successfully created, sending e-mail notification
				Logger::logCreation($location->getDir(true, false, false, 0).$dirname, true);
				Logger::emailNotification($location->getDir(true, false, false, 0).$dirname, false);
			}
		}
	}

	function uploadFile($location, $userfile)
	{
		global $encodeExplorer;
		$name = basename($userfile['name']);
		if(get_magic_quotes_gpc())
			$name = stripslashes($name);

		$upload_dir = $location->getFullPath();
		$upload_file = $upload_dir . $name;

		if(function_exists("finfo_open") && function_exists("finfo_file"))
			$mime_type = File::getFileMime($userfile['tmp_name']);
		else
			$mime_type = $userfile['type'];

		$extension = File::getFileExtension($userfile['name']);

		if(!$location->uploadAllowed())
		{
			$encodeExplorer->setErrorString("upload_not_allowed");
		}
		else if(!$location->isWritable())
		{
			$encodeExplorer->setErrorString("upload_dir_not_writable");
		}
		else if(!is_uploaded_file($userfile['tmp_name']))
		{
			$encodeExplorer->setErrorString("failed_upload");
		}
		else if(is_array(EncodeExplorer::getConfig("upload_allow_type")) && count(EncodeExplorer::getConfig("upload_allow_type")) > 0 && !in_array($mime_type, EncodeExplorer::getConfig("upload_allow_type")))
		{
			$encodeExplorer->setErrorString("upload_type_not_allowed");
		}
		else if(is_array(EncodeExplorer::getConfig("upload_reject_extension")) && count(EncodeExplorer::getConfig("upload_reject_extension")) > 0 && in_array($extension, EncodeExplorer::getConfig("upload_reject_extension")))
		{
			$encodeExplorer->setErrorString("upload_type_not_allowed");
		}
		else if(!@move_uploaded_file($userfile['tmp_name'], $upload_file))
		{
			$encodeExplorer->setErrorString("failed_move");
		}
		else
		{
			chmod($upload_file, EncodeExplorer::getConfig("upload_file_mode"));
			Logger::logCreation($location->getDir(true, false, false, 0).$name, false);
			Logger::emailNotification($location->getDir(true, false, false, 0).$name, true);
		}
	}

	public static function delete_dir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir."/".$object) == "dir")
						FileManager::delete_dir($dir."/".$object);
					else
						unlink($dir."/".$object);
				}
			}
			reset($objects);
			rmdir($dir);
		}
	}

	public static function delete_file($file){
		if(is_file($file)){
			unlink($file);
		}
	}

	//
	// The main function, checks if the user wants to perform any supported operations
	//
	function run($location)
	{
		if(isset($_POST['userdir']) && strlen($_POST['userdir']) > 0){
			if($location->uploadAllowed() && GateKeeper::isUserLoggedIn() && GateKeeper::isAccessAllowed() && GateKeeper::isNewdirAllowed()){
				$this->newFolder($location, $_POST['userdir']);
			}
		}

		if(isset($_FILES['userfile']['name']) && strlen($_FILES['userfile']['name']) > 0){
			if($location->uploadAllowed() && GateKeeper::isUserLoggedIn() && GateKeeper::isAccessAllowed() && GateKeeper::isUploadAllowed()){
				$this->uploadFile($location, $_FILES['userfile']);
			}
		}

		if(isset($_GET['del'])){
			if(GateKeeper::isUserLoggedIn() && GateKeeper::isAccessAllowed() && GateKeeper::isDeleteAllowed()){
				$split_path = Location::splitPath($_GET['del']);
				$path = "";
				for($i = 0; $i < count($split_path); $i++){
					$path .= $split_path[$i];
					if($i + 1 < count($split_path))
						$path .= "/";
				}
				if($path == "" || $path == "/" || $path == "\\" || $path == ".")
					return;

				if(is_dir($path))
					FileManager::delete_dir($path);
				else if(is_file($path))
					FileManager::delete_file($path);
			}
		}
	}
}

//
// Dir class holds the information about one directory in the list
//
class Dir
{
	var $name;
	var $location;
	var $customDir;

	//
	// Constructor
	//
	function Dir($name, $location, $customDir)
	{
		$this->name = $name;
		$this->location = $location;
		$this->customDir = $customDir;
	}

	function getName()
	{
		return $this->name;
	}

	function getNameHtml()
	{
		return htmlspecialchars($this->name);
	}

	function getNameEncoded()
	{
		return rawurlencode($this->name);
	}

	//
	// Debugging output
	//
	function debug()
	{
		print("Dir name (htmlspecialchars): ".$this->getName()."\n");
		print("Dir location: ".$this->location->getDir(true, false, false, 0)."\n");
	}
}

//
// File class holds the information about one file in the list
//
class File
{
	var $name;
	var $location;
	var $size;
	//var $extension;
	var $type;
	var $modTime;
	var $path;
	var $isDir;

	//
	// Constructor
	//
	function File($name, $location)
	{
		$this->name = $name;
		$this->location = $location;

		// Custom Code  Tom Riedl
        $this->path = $this->location->getDir(true, false, false, 0).$this->getName();
		$this->isDir = is_dir($this->path);
		$this->type = $this->isDir ? "directory" : File::getFileType($this->path);
		$this->size = File::getFileSize($this->path);
		$this->modTime = filemtime($this->path);
	}

	function getName()
	{
		return $this->name;
	}

	function getNameEncoded()
	{
		return rawurlencode($this->name);
	}

	function getNameHtml()
	{
		return htmlspecialchars($this->name);
	}

	function getSize()
	{
		return $this->isDir ? "" : $this->size;
	}

	function getType()
	{
		return $this->type;
	}

	function getModTime()
	{
		return $this->modTime;
	}

	//
	// Determine the size of a file
	//
	public static function getFileSize($file)
	{
		$sizeInBytes = filesize($file);

		// If filesize() fails (with larger files), try to get the size from unix command line.
		if (EncodeExplorer::getConfig("large_files") == true || !$sizeInBytes || $sizeInBytes < 0) {
			$sizeInBytes=exec("ls -l '$file' | awk '{print $5}'");
		}
		return $sizeInBytes;
	}

	public static function getFileType($filepath)
	{
		/*
		 * This extracts the information from the file contents.
		 * Unfortunately it doesn't properly detect the difference between text-based file types.
		 *
		$mime_type = File::getMimeType($filepath);
		$mime_type_chunks = explode("/", $mime_type, 2);
		$type = $mime_type_chunks[1];
		*/
		return File::getFileExtension($filepath);
	}

	public static function getFileMime($filepath)
	{
		$fhandle = finfo_open(FILEINFO_MIME);
		$mime_type = finfo_file($fhandle, $filepath);
		$mime_type_chunks = preg_split('/\s+/', $mime_type);
		$mime_type = $mime_type_chunks[0];
		$mime_type_chunks = explode(";", $mime_type);
		$mime_type = $mime_type_chunks[0];
		return $mime_type;
	}

	public static function getFileExtension($filepath)
	{
		return strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
	}

	//
	// Debugging output
	//
	function debug()
	{
		print("File name: ".$this->getName()."\n");
		print("File location: ".$this->location->getDir(true, false, false, 0)."\n");
		print("File size: ".$this->size."\n");
		print("File modTime: ".$this->modTime."\n");
	}

	function isImage()
	{
		$type = $this->getType();
		if($type == "png" || $type == "jpg" || $type == "gif" || $type == "jpeg")
			return true;
		return false;
	}

	function isPdf()
	{
		if(strtolower($this->getType()) == "pdf")
			return true;
		return false;
	}

	public static function isPdfFile($file)
	{
		if(File::getFileType($file) == "pdf")
			return true;
		return false;
	}

	function isValidForThumb()
	{
		if($this->isImage() || ($this->isPdf() && ImageServer::isEnabledPdf()))
			return true;
		return false;
	}
}

class Location
{
	var $path;

	//
	// Split a file path into array elements
	//
	public static function splitPath($dir)
	{
		$dir = stripslashes($dir);
		$path1 = preg_split("/[\\\\\/]+/", $dir);
		$path2 = array();
		for($i = 0; $i < count($path1); $i++)
		{
			if($path1[$i] == ".." || $path1[$i] == "." || $path1[$i] == "")
				continue;
			$path2[] = $path1[$i];
		}
		return $path2;
	}

	//
	// Get the current directory.
	// Options: Include the prefix ("./"); URL-encode the string; HTML-encode the string; return directory n-levels up
	//
	function getDir($prefix, $encoded, $html, $up)
	{
		$dir = "";
		if($prefix == true)
			$dir .= "./";
		for($i = 0; $i < ((count($this->path) >= $up && $up > 0)?count($this->path)-$up:count($this->path)); $i++)
		{
			$temp = $this->path[$i];
			if($encoded)
				$temp = rawurlencode($temp);
			if($html)
				$temp = htmlspecialchars($temp);
			$dir .= $temp."/";
		}
		return $dir;
	}

	function getPathLink($i, $html)
	{
		if($html)
			return htmlspecialchars($this->path[$i]);
		else
			return $this->path[$i];
	}

	function getFullPath()
	{
		return (strlen(EncodeExplorer::getConfig('basedir')) > 0?EncodeExplorer::getConfig('basedir'):dirname($_SERVER['SCRIPT_FILENAME']))."/".$this->getDir(true, false, false, 0);
	}

	//
	// Debugging output
	//
	function debug()
	{
		print_r($this->path);
		print("Dir with prefix: ".$this->getDir(true, false, false, 0)."\n");
		print("Dir without prefix: ".$this->getDir(false, false, false, 0)."\n");
		print("Upper dir with prefix: ".$this->getDir(true, false, false, 1)."\n");
		print("Upper dir without prefix: ".$this->getDir(false, false, false, 1)."\n");
	}


	//
	// Set the current directory
	//
	function init()
	{
		if(!isset($_GET['dir']) || strlen($_GET['dir']) == 0)
		{
			$this->path = $this->splitPath(EncodeExplorer::getConfig('starting_dir'));
		}
		else
		{
			$this->path = $this->splitPath($_GET['dir']);
		}
	}

	//
	// Checks if the current directory is below the input path
	//
	function isSubDir($checkPath)
	{
		for($i = 0; $i < count($this->path); $i++)
		{
			if(strcmp($this->getDir(true, false, false, $i), $checkPath) == 0)
				return true;
		}
		return false;
	}

	//
	// Check if uploading is allowed into the current directory, based on the configuration
	//
	function uploadAllowed()
	{
		if(EncodeExplorer::getConfig('upload_enable') != true)
			return false;
		if(EncodeExplorer::getConfig('upload_dirs') == null || count(EncodeExplorer::getConfig('upload_dirs')) == 0)
			return true;

		$upload_dirs = EncodeExplorer::getConfig('upload_dirs');
		for($i = 0; $i < count($upload_dirs); $i++)
		{
			if($this->isSubDir($upload_dirs[$i]))
				return true;
		}
		return false;
	}

	function isWritable()
	{
		return is_writable($this->getDir(true, false, false, 0));
	}

	public static function isDirWritable($dir)
	{
		return is_writable($dir);
	}

	public static function isFileWritable($file)
	{
		if(file_exists($file))
		{
			if(is_writable($file))
				return true;
			else
				return false;
		}
		else if(Location::isDirWritable(dirname($file)))
			return true;
		else
			return false;
	}
}

class EncodeExplorer
{
	var $location;
	var $dirs;
	var $files;
	var $sort_by;
	var $sort_as;
	var $mobile;
	var $logging;
	var $spaceUsed;
	var $lang;

	//
	// Determine sorting, calculate space.
	//
	function init()
	{
		$this->sort_by = "";
		$this->sort_as = "";
		if(isset($_GET["sort_by"], $_GET["sort_as"]))
		{
			if($_GET["sort_by"] == "name" || $_GET["sort_by"] == "size" || $_GET["sort_by"] == "mod")
				if($_GET["sort_as"] == "asc" || $_GET["sort_as"] == "desc")
				{
					$this->sort_by = $_GET["sort_by"];
					$this->sort_as = $_GET["sort_as"];
				}
		}
		if(strlen($this->sort_by) <= 0 || strlen($this->sort_as) <= 0)
		{
			$this->sort_by = "name";
			$this->sort_as = "desc";
		}


		global $_TRANSLATIONS;
		if(isset($_GET['lang'], $_TRANSLATIONS[$_GET['lang']]))
			$this->lang = $_GET['lang'];
		else
			$this->lang = EncodeExplorer::getConfig("lang");

		$this->mobile = false;
		if(EncodeExplorer::getConfig("mobile_enabled") == true)
		{
			if((EncodeExplorer::getConfig("mobile_default") == true || isset($_GET['m'])) && !isset($_GET['s']))
				$this->mobile = true;
		}

		$this->logging = false;
		if(EncodeExplorer::getConfig("log_file") != null && strlen(EncodeExplorer::getConfig("log_file")) > 0)
			$this->logging = true;
	}

	//
	// Read the file list from the directory
	//
	function readDir()
	{
		global $encodeExplorer;
		//
		// Reading the data of files and directories
		//
		if($open_dir = @opendir($this->location->getFullPath()))
		{
			$this->dirs = array();
			$this->files = array();
			while ($object = readdir($open_dir))
			{
				if($object != "." && $object != "..")
				{
					$target = $this->location->getDir(true, false, false, 0)."/".$object;
					if(is_dir($target))
					{
						if(!in_array($object, EncodeExplorer::getConfig('hidden_dirs')))
							$this->dirs[] = new Dir($object, $this->location, is_file($target."/index.php") || is_file($target."/index.html") || is_file($target."/index.htm"));
					}
					else if(!in_array($object, EncodeExplorer::getConfig('hidden_files')))
						$this->files[] = new File($object, $this->location);
				}
			}
			closedir($open_dir);
		}
		else
		{
			$encodeExplorer->setErrorString("unable_to_read_dir");;
		}
	}

	//
	// A recursive function for calculating the total used space
	//
	function sum_dir($start_dir, $ignore_files, $levels = 1)
	{
		if ($dir = opendir($start_dir))
		{
			$total = 0;
			while ((($file = readdir($dir)) !== false))
			{
				if (!in_array($file, $ignore_files))
				{
					if ((is_dir($start_dir . '/' . $file)) && ($levels - 1 >= 0))
					{
						$total += $this->sum_dir($start_dir . '/' . $file, $ignore_files, $levels-1);
					}
					elseif (is_file($start_dir . '/' . $file))
					{
						$total += File::getFileSize($start_dir . '/' . $file) / 1024;
					}
				}
			}

			closedir($dir);
			return $total;
		}
	}

	function calculateSpace()
	{
		if(EncodeExplorer::getConfig('calculate_space_level') <= 0)
			return;
		$ignore_files = array('..', '.');
		$start_dir = getcwd();
		$spaceUsed = $this->sum_dir($start_dir, $ignore_files, EncodeExplorer::getConfig('calculate_space_level'));
		$this->spaceUsed = round($spaceUsed/1024, 2);
	}

	function sort()
	{
		if(is_array($this->files)){
			usort($this->files, "EncodeExplorer::cmp_".$this->sort_by);
			if($this->sort_as == "desc")
				$this->files = array_reverse($this->files);
		}

		if(is_array($this->dirs)){
			usort($this->dirs, "EncodeExplorer::cmp_name");
			if($this->sort_by == "name" && $this->sort_as == "desc")
				$this->dirs = array_reverse($this->dirs);
		}
	}

	function makeArrow($sort_by)
	{
		if($this->sort_by == $sort_by && $this->sort_as == "asc")
		{
			$sort_as = "desc";
			$img = "arrow_up";
		}
		else
		{
			$sort_as = "asc";
			$img = "arrow_down";
		}

		if($sort_by == "name")
			$text = $this->getString("file_name");
		else if($sort_by == "size")
			$text = $this->getString("size");
		else if($sort_by == "mod")
			$text = $this->getString("last_changed");

		return "<a href=\"".$this->makeLink(false, false, $sort_by, $sort_as, null, $this->location->getDir(false, true, false, 0))."\">
			$text <img style=\"border:0;width:16px;height:16px;\" alt=\"".$sort_as."\" src=\"?img=".$img."\" /></a>";
	}

	function makeLink($switchVersion, $logout, $sort_by, $sort_as, $delete, $dir)
	{
		$link = "?";
		if($switchVersion == true && EncodeExplorer::getConfig("mobile_enabled") == true)
		{
			if($this->mobile == false)
				$link .= "m&amp;";
			else
				$link .= "s&amp;";
		}
		else if($this->mobile == true && EncodeExplorer::getConfig("mobile_enabled") == true && EncodeExplorer::getConfig("mobile_default") == false)
			$link .= "m&amp;";
		else if($this->mobile == false && EncodeExplorer::getConfig("mobile_enabled") == true && EncodeExplorer::getConfig("mobile_default") == true)
			$link .= "s&amp;";

		if($logout == true)
		{
			$link .= "logout";
			return $link;
		}

		if(isset($this->lang) && $this->lang != EncodeExplorer::getConfig("lang"))
			$link .= "lang=".$this->lang."&amp;";

		if($sort_by != null && strlen($sort_by) > 0)
			$link .= "sort_by=".$sort_by."&amp;";

		if($sort_as != null && strlen($sort_as) > 0)
			$link .= "sort_as=".$sort_as."&amp;";

		$link .= "dir=".$dir;
		if($delete != null)
			$link .= "&amp;del=".$delete;
		return $link;
	}

	function makeIcon($l)
	{
		$l = strtolower($l);
		return "?img=".$l;
	}

	function formatModTime($time)
	{
		$timeformat = "d.m.y H:i:s";
		if(EncodeExplorer::getConfig("time_format") != null && strlen(EncodeExplorer::getConfig("time_format")) > 0)
			$timeformat = EncodeExplorer::getConfig("time_format");
		return date($timeformat, $time);
	}

	function formatSize($size)
	{
		$sizes = Array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB');
		$y = $sizes[0];
		for ($i = 1; (($i < count($sizes)) && ($size >= 1024)); $i++)
		{
			$size = $size / 1024;
			$y  = $sizes[$i];
		}
		return round($size, 2)." ".$y;
	}

	//
	// Debugging output
	//
	function debug()
	{
		print("Explorer location: ".$this->location->getDir(true, false, false, 0)."\n");
		for($i = 0; $i < count($this->dirs); $i++)
			$this->dirs[$i]->output();
		for($i = 0; $i < count($this->files); $i++)
			$this->files[$i]->output();
	}

	//
	// Comparison functions for sorting.
	//

	public static function cmp_name($b, $a)
	{
		return strcasecmp($a->name, $b->name);
	}

	public static function cmp_size($a, $b)
	{
		return ($a->size - $b->size);
	}

	public static function cmp_mod($b, $a)
	{
		return ($a->modTime - $b->modTime);
	}

	//
	// The function for getting a translated string.
	// Falls back to english if the correct language is missing something.
	//
	public static function getLangString($stringName, $lang)
	{
		global $_TRANSLATIONS;
		if(isset($_TRANSLATIONS[$lang]) && is_array($_TRANSLATIONS[$lang])
			&& isset($_TRANSLATIONS[$lang][$stringName]))
			return $_TRANSLATIONS[$lang][$stringName];
		else if(isset($_TRANSLATIONS["en"]))// && is_array($_TRANSLATIONS["en"])
			//&& isset($_TRANSLATIONS["en"][$stringName]))
			return $_TRANSLATIONS["en"][$stringName];
		else
			return "Translation error";
	}

	function getString($stringName)
	{
		return EncodeExplorer::getLangString($stringName, $this->lang);
	}

	//
	// The function for getting configuration values
	//
	public static function getConfig($name)
	{
		global $_CONFIG;
		if(isset($_CONFIG, $_CONFIG[$name]))
			return $_CONFIG[$name];
		return null;
	}

	public static function setError($message)
	{
		global $_ERROR;
		if(isset($_ERROR) && strlen($_ERROR) > 0)
			;// keep the first error and discard the rest
		else
			$_ERROR = $message;
	}

	function setErrorString($stringName)
	{
		EncodeExplorer::setError($this->getString($stringName));
	}

	//
	// Main function, activating tasks
	//
	function run($location)
	{
		$this->location = $location;
		$this->calculateSpace();
		$this->readDir();
		$this->sort();
		$this->outputHtml();
	}

	public function printLoginBox()
	{
		?>
		<div id="login">
		<form enctype="multipart/form-data" action="<?php print $this->makeLink(false, false, null, null, null, ""); ?>" method="post">
		<?php
		if(GateKeeper::isLoginRequired())
		{
			$require_username = false;
			foreach(EncodeExplorer::getConfig("users") as $user){
				if($user[0] != null && strlen($user[0]) > 0){
					$require_username = true;
					break;
				}
			}
			if($require_username)
			{
			?>
			<div><label for="user_name"><?php print $this->getString("username"); ?>:</label>
			<input type="text" name="user_name" value="" id="user_name" /></div>
			<?php
			}
			?>
			<div><label for="user_pass"><?php print $this->getString("password"); ?>:</label>
			<input type="password" name="user_pass" id="user_pass" /></div>
			<div><input type="submit" value="<?php print $this->getString("log_in"); ?>" class="button" /></div>
		</form>
		</div>
	<?php
		}
	}

	//
	// Printing the actual page
	//
	function outputHtml()
	{
		global $_ERROR;
		global $_START_TIME;
?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $this->getConfig('lang'); ?>" lang="<?php print $this->getConfig('lang'); ?>">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php print $this->getConfig('charset'); ?>">
<link rel="icon" type="image/svg+xml" href="?img=directory">
<?php css(); ?>
<!-- <meta charset="<?php print $this->getConfig('charset'); ?>" /> -->
<?php
if(($this->getConfig('log_file') != null && strlen($this->getConfig('log_file')) > 0)
	|| ($this->getConfig('thumbnails') != null && $this->getConfig('thumbnails') == true && $this->mobile == false)
	|| (GateKeeper::isDeleteAllowed()))
{
?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {
<?php
	if(GateKeeper::isDeleteAllowed()){
?>
	$('td.del a').click(function(){
		var answer = confirm('Are you sure you want to delete : \'' + $(this).attr("data-name") + "\' ?");
		return answer;
	});
<?php
	}
	if($this->logging == true)
	{
?>
		function logFileClick(path)
		{
			 $.ajax({
					async: false,
					type: "POST",
					data: {log: path},
					contentType: "application/x-www-form-urlencoded; charset=UTF-8",
					cache: false
				});
		}

		$("a.file").click(function(){
			logFileClick("<?php print $this->location->getDir(true, true, false, 0);?>" + $(this).html());
			return true;
		});
<?php
	}
	if(EncodeExplorer::getConfig("thumbnails") == true && $this->mobile == false)
	{
?>
		function positionThumbnail(e) {
			xOffset = 30;
			yOffset = 10;
			$("#thumb").css("left",(e.clientX + xOffset) + "px");

			diff = 0;
			if(e.clientY + $("#thumb").height() > $(window).height())
				diff = e.clientY + $("#thumb").height() - $(window).height();

			$("#thumb").css("top",(e.pageY - yOffset - diff) + "px");
		}

		$("a.thumb").hover(function(e){
			$("#thumb").remove();
			$("body").append("<div id=\"thumb\"><img src=\"?thumb="+ $(this).attr("href") +"\" alt=\"Preview\" \/><\/div>");
			positionThumbnail(e);
			$("#thumb").fadeIn("medium");
		},
		function(){
			$("#thumb").remove();
		});

		$("a.thumb").mousemove(function(e){
			positionThumbnail(e);
			});

		$("a.thumb").click(function(e){$("#thumb").remove(); return true;});
<?php
	}
?>
	});
//]]>
</script>
<?php
}
?>
<title><?php if(EncodeExplorer::getConfig('main_title') != null) print EncodeExplorer::getConfig('main_title'); ?></title>
</head>
<body class="<?php print ($this->mobile == true?"mobile":"standard");?>">
<?php
//
// Print the error (if there is something to print)
//
if(isset($_ERROR) && strlen($_ERROR) > 0)
{
	print "<div id=\"error\">".$_ERROR."</div>";
}
?>
<div id="frame">
<?php
if(EncodeExplorer::getConfig('show_top') == true)
{
?>
<div id="top">
	<a href="<?php print $this->makeLink(false, false, null, null, null, ""); ?>"><span><?php if(EncodeExplorer::getConfig('main_title') != null) print EncodeExplorer::getConfig('main_title'); ?></span></a>
<?php
if(EncodeExplorer::getConfig("secondary_titles") != null && is_array(EncodeExplorer::getConfig("secondary_titles")) && count(EncodeExplorer::getConfig("secondary_titles")) > 0 && $this->mobile == false)
{
	$secondary_titles = EncodeExplorer::getConfig("secondary_titles");
	print "<div class=\"subtitle\">".$secondary_titles[array_rand($secondary_titles)]."</div>\n";
}
?>
</div>
<?php
}

// Checking if the user is allowed to access the page, otherwise showing the login box
if(!GateKeeper::isAccessAllowed())
{
	$this->printLoginBox();
}
else
{
if($this->mobile == false && EncodeExplorer::getConfig("show_path") == true)
{
?>
<div class="breadcrumbs">
<?php
	if (count($this->location->path))
	{
		print '<a href="?dir=">'.$this->getString("root")."</a> ";
	}
?>
<?php
	for($i = 0; $i < count($this->location->path); $i++)
	{
		print "&gt; <a href=\"".$this->makeLink(false, false, null, null, null, $this->location->getDir(false, true, false, count($this->location->path) - $i - 1))."\">";
		print $this->location->getPathLink($i, true);
		print "</a>\n";
	}
?>
</div>
<?php
}
?>

<!-- START: List table -->
<table class="table">
<?php
if($this->mobile == false)
{
?>
<tr class="row two header">
	<td class="icon"> </td>
	<td class="name"><?php print $this->makeArrow("name");?></td>
	<td class="size"><?php print $this->makeArrow("size"); ?></td>
	<td class="changed"><?php print $this->makeArrow("mod"); ?></td>
	<?php if($this->mobile == false && GateKeeper::isDeleteAllowed()){?>
	<td class="del"><?php print EncodeExplorer::getString("del"); ?></td>
	<?php } ?>
</tr>
<?php
}
?>
<?php
	$link = "";
	if (count($this->location->path))
	{
		$link = $this->makeLink(false, false, null, null, null, $this->location->getDir(false, true, false, 1));
	}
	else
	{
		$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
		$link = strlen($uri_parts[0]) > 0 && $uri_parts[0] != "/" ? "//".$_SERVER['HTTP_HOST'].$uri_parts[0].".." : "";
	}
	if (strlen($link) > 0)
	{
		print "<tr class=\"row two\" onclick=\"window.location.href = '".$link."'; return false;\">";
		print '<td class="icon"><a href="'.$link.'"><img alt="dir" src="?img=directoryup" /></a></td>';
		print '<td colspan="'.(($this->mobile == true?2:(GateKeeper::isDeleteAllowed()?4:3))).'" class="long">';
		print '<a class="item" href="'.$link.'">..</a></td>';
		print '</tr>';
	}
?>
<?php
//
// Ready to display folders and files.
//
$row = 1;

//
// Folders first
//
if($this->dirs)
{
	foreach ($this->dirs as $dir)
	{
		$target = "";
		if ($dir->customDir)
		{
			$target = $this->location->getDir(false, true, false, 0).$dir->getNameEncoded();
		}
		else
		{
			$target = $this->makeLink(false, false, null, null, null, $this->location->getDir(false, true, false, 0).$dir->getNameEncoded());
		}

		$row_style = ($row ? "one" : "two");
		print "<tr class=\"row ".$row_style."\" onclick=\"window.location.href = '".$target."'; return false;\">\n";
		print "<td class=\"icon\"><img alt=\"dir\" src=\"?img=directory\" /></td>\n";
		print "<td class=\"name\" colspan=\"".($this->mobile == true?2:3)."\">\n";
		print "<a href=\"".$target."\" class=\"item dir\">";
		print $dir->getNameHtml();
		print "</a>\n";
		print "</td>\n";
		if($this->mobile == false && GateKeeper::isDeleteAllowed()){
			print "<td class=\"del\"><a data-name=\"".htmlentities($dir->getName())."\" href=\"".$this->makeLink(false, false, null, null, $this->location->getDir(false, true, false, 0).$dir->getNameEncoded(), $this->location->getDir(false, true, false, 0))."\"><img src=\"?img=del\" alt=\"Delete\" /></a></td>";
		}
		print "</tr>\n";
		$row =! $row;
	}
}

//
// Now the files
//
if($this->files)
{
	$count = 0;
	foreach ($this->files as $file)
	{
		$row_style = ($row ? "one" : "two");
		$target = $this->location->getDir(false, true, false, 0).$file->getNameEncoded();
		$openNew = EncodeExplorer::getConfig('open_in_new_window') ? "target=\"_blank\"" : "";
		print "<tr class=\"row ".$row_style.(++$count == count($this->files)?" last":"")."\" onclick=\"window.location.href = '".$target."'; return false;\">\n";
		print "<td class=\"icon\"><a href=\"".$target."\" ".$openNew."><img alt=\"".$file->getType()."\" src=\"".$this->makeIcon($file->getType())."\" /></a></td>\n";
		print "<td class=\"name\">\n";
		print "\t\t<a href=\"".$target."\" ".$openNew;
		print " class=\"item file";
		if($file->isValidForThumb())
			print " thumb";
		print "\">";
		print $file->getNameHtml();
		if($this->mobile == true)
		{
			print "<span class =\"size\">".$this->formatSize($file->getSize())."</span>";
		}
		print "</a>\n";
		print "</td>\n";
		if($this->mobile != true)
		{
			print "<td class=\"size\">".$this->formatSize($file->getSize())."</td>\n";
			print "<td class=\"changed\">".$this->formatModTime($file->getModTime())."</td>\n";
		}
		if($this->mobile == false && GateKeeper::isDeleteAllowed()){
			print "<td class=\"del\">
				<a data-name=\"".htmlentities($file->getName())."\" href=\"".$this->makeLink(false, false, null, null, $this->location->getDir(false, true, false, 0).$file->getNameEncoded(), $this->location->getDir(false, true, false, 0))."\">
					<img src=\"?img=del\" alt=\"Delete\" />
				</a>
			</td>";
		}
		print "</tr>\n";
		$row =! $row;
	}
}


//
// The files and folders have been displayed
//
?>

</table>
<!-- END: List table -->
<?php
}
?>
</div>

<?php
if(GateKeeper::isAccessAllowed() && GateKeeper::showLoginBox()){
?>
<!-- START: Login area -->
<form enctype="multipart/form-data" method="post">
	<div id="login_bar">
	<?php print $this->getString("username"); ?>:
	<input type="text" name="user_name" value="" id="user_name" />
	<?php print $this->getString("password"); ?>:
	<input type="password" name="user_pass" id="user_pass" />
	<input type="submit" class="submit" value="<?php print $this->getString("log_in"); ?>" />
	<div class="bar"></div>
	</div>
</form>
<!-- END: Login area -->
<?php
}

if(GateKeeper::isAccessAllowed() && $this->location->uploadAllowed() && (GateKeeper::isUploadAllowed() || GateKeeper::isNewdirAllowed()))
{
?>
<!-- START: Upload area -->
<form enctype="multipart/form-data" method="post">
	<div id="upload">
		<?php
		if(GateKeeper::isNewdirAllowed()){
		?>
		<div id="newdir_container">
			<input name="userdir" type="text" class="upload_dirname" />
			<input type="submit" value="<?php print $this->getString("make_directory"); ?>" />
		</div>
		<?php
		}
		if(GateKeeper::isUploadAllowed()){
		?>
		<div id="upload_container">
			<input name="userfile" type="file" class="upload_file" />
			<input type="submit" value="<?php print $this->getString("upload"); ?>" class="upload_sumbit" />
		</div>
		<?php
		}
		?>
		<div class="bar"></div>
	</div>
</form>
<!-- END: Upload area -->
<?php
}

?>
<!-- START: Info area -->
<div id="info">
<?php
if(GateKeeper::isUserLoggedIn())
	print "<a href=\"".$this->makeLink(false, true, null, null, null, "")."\">".$this->getString("log_out")."</a> | ";

if(EncodeExplorer::getConfig("mobile_enabled") == true)
{
	print "<a href=\"".$this->makeLink(true, false, null, null, null, $this->location->getDir(false, true, false, 0))."\">\n";
	print ($this->mobile == true)?$this->getString("standard_version"):$this->getString("mobile_version")."\n";
	print "</a> | \n";
}
if(GateKeeper::isAccessAllowed() && $this->getConfig("calculate_space_level") > 0 && $this->mobile == false)
{
	global $_CONFIG;
	print $this->getString("total_used_space").": ".$this->spaceUsed." MiB (within ".$_CONFIG['calculate_space_level']." levels) | ";
}
if($this->mobile == false && $this->getConfig("show_load_time") == true)
{
	printf($this->getString("page_load_time")." | ", (microtime(TRUE) - $_START_TIME)*1000);
}
?>
<a href="http://encode-explorer.siineiolekala.net">Encode Explorer</a> |
<a href="https://cyancor.com/">CyanCor</a>
</div>
<!-- END: Info area -->
</body>
</html>

<?php
	}
}

//
// This is where the system is activated.
// We check if the user wants an image and show it. If not, we show the explorer.
//
$encodeExplorer = new EncodeExplorer();
$encodeExplorer->init();

GateKeeper::init();

if(!ImageServer::showImage() && !Logger::logQuery())
{
	$location = new Location();
	$location->init();
	if(GateKeeper::isAccessAllowed())
	{
		Logger::logAccess($location->getDir(true, false, false, 0), true);
		$fileManager = new FileManager();
		$fileManager->run($location);
	}
	$encodeExplorer->run($location);
}
?>

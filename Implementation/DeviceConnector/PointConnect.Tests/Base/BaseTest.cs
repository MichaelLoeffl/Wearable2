using System;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using PointConnect.Tests.CoreFunctionality;
using PointConnector.StreamComposing;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace PointConnect.Tests.Base;

public class BaseTest : BleTestBase
{
    private int _screenCounter = 0;
    
    protected string RunTest(string test)
    {
        string testResult = Execute("Test", test);
        Console.Write(testResult);
        Assert.False(testResult.Contains("\n{FAIL}"), "Test failed");
        return testResult;
    }

    protected string Execute(string command, string text)
    {
        return CommunicationService.Instance.ExecuteAndWait(command, text);
    }
    
    protected string Execute(string command)
    {
        return CommunicationService.Instance.ExecuteAndWait(command);
    }

    private string GetFileName(string baseName, string? modifier = null)
    {
        var path = Path.Combine(TestContext.CurrentContext.Test.ClassName!.Split(".").Reverse().FirstOrDefault()!, TestContext.CurrentContext.Test.Name);
        if (string.IsNullOrEmpty(modifier))
        {
            return Path.Combine(path, baseName + ".png");
        }

        return Path.Combine(path, modifier + "-" + baseName + ".png");
    }
    
    protected bool CompareImage(Image<Rgb24> image, string reference)
    {
        var maskColor = new Rgb24(255, 0, 255);
        var refName = Path.Combine("ReferenceImages", GetFileName(reference));
        if (File.Exists(refName))
        {
            var refImage = Image.Load<Rgb24>(refName);
            if (refImage.Width != image.Width || refImage.Height != image.Height)
            {
                return false;
            }

            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    var refColor = refImage[x, y];
                    if (refColor != maskColor && image[x, y] != refImage[x, y])
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    protected Image TakeScreenshot(string name)
    {
        Image? Result = null;
        Action<Image<Rgb24>> func = image =>
        {
            var filename = Path.Combine("Screenshots", GetFileName(name, _screenCounter++.ToString()));
            Directory.CreateDirectory(Path.GetDirectoryName(filename)!);
            image.SaveAsPng(filename);
            Result = image;
            Assert.True(CompareImage(image, name), "Image does not look like reference");
        };
        try
        {
            CommunicationService.Instance.ScreenshotReady += func;
            Execute("Screenshot");
            if (Result == null)
            {
                Assert.Fail("Screenshot requested but not received");
            }

            return Result!;
        }
        finally
        {
            CommunicationService.Instance.ScreenshotReady -= func;
        }
    }

    protected string Touch(int position)
    {
        return CommunicationService.Instance.ExecuteAndWait("Touch", new byte[1] {(byte) (position & 0x7F)});
    }
    
    protected string TouchRelease()
    {
        return CommunicationService.Instance.ExecuteAndWait("TouchRelease");
    }

    protected int GetScroll()
    {
        var result = CommunicationService.Instance.ExecuteAndWait("GetScrollPos");
        var lines = result.Split("\n");
        var prefix = "ScrollPos: ";
        return int.Parse(lines.First(s => s.StartsWith(prefix)).Substring(prefix.Length));
    }
    
    protected void ScrollToPosition(int desiredPosition)
    {
        var diff = -1;
        int actualPosition = desiredPosition;
        for (var i = 0; i < 5 && diff != 0; i++)
        {
            actualPosition = GetScroll();
            diff = desiredPosition - actualPosition;
            Swipe(diff);
        }
        Assert.AreEqual(desiredPosition, actualPosition, "Unable to scroll to desired scroll position");
    }

    protected void Swipe(int pixels)
    {
        int allPixels = 0;
        while (pixels != 0)
        {
            var take = Math.Clamp(pixels, -75, 75);
            pixels -= take;
            if (Math.Abs(pixels) < 40)
            {
                take += pixels;
                pixels = 0;
            }
            var startPos = 64 - take / 2;
            Touch(startPos);
            Touch(startPos + take);
            Thread.Sleep(300);
            TouchRelease();

            allPixels += take;
        }
    }
    
    [SetUp]
    public void Setup()
    { 
        CommunicationService.Instance.Initialize();
    }

    protected void PressHomeButton()
    {
        Execute("ButtonClick");
    }
    
    protected void Navigate(Screens screens)
    {
        var handle = Expect("App activated: " + Enum.GetName(screens));
        PressHomeButton();
        if (screens == Screens.Home)
        {
        }
        else
        {
            return;
        }

        handle.WaitOne(3000);
    }

    private StreamWaitHandle Expect(params string[] appActivatedLauncher)
    {
        return CommunicationService.Instance.Expect(appActivatedLauncher);
    }
}
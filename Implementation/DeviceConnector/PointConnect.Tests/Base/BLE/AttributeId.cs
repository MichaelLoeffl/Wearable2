namespace PointConnect.Tests.Base.BLE;

internal enum AttributeId
{
    AppIdentifier = 0,
    Title = 1, //(Needs to be followed by a 2-bytes max length parameter)
    Subtitle = 2, //(Needs to be followed by a 2-bytes max length parameter)
    Message = 3, //(Needs to be followed by a 2-bytes max length parameter)
    MessageSize = 4,
    Date = 5,
    PositiveActionLabel = 6,
    NegativeActionLabel = 7,
}
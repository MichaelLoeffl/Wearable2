using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace PointConnect.Tests.Base.BLE;

public class NotificationCenter
{
    private static ConcurrentDictionary<UInt32, Notification> _notifications = new();
    public static NotificationCenter Instance { get; } = new();

    private static BlockingCollection<(string command, byte[])> _requestQueue = new();
    
    private NotificationCenter()
    {
        if (!BitConverter.IsLittleEndian)
        {
            throw new Exception("Not little endian!");
        }

        CommunicationService.Instance.BinaryDataReceived += HandleRequests;
    }

    public bool Add(Notification notification)
    {
        if (!IsRegistered(notification))
        {
            _notifications[notification.UID] = notification;
            var handle = CommunicationService.Instance.Expect(new[] {$"Message received - Title: {notification.Title} - Message: {notification.Message}"});
            CommunicationService.Instance.ExecuteAndWait("Notification", notification.GetNotificationSourceData().ToArray());
            return handle.WaitOne(16000);
        }
        else
        {
            CommunicationService.Instance.ExecuteAndWait("Notification", notification.GetNotificationSourceData().ToArray());
            return true;            
        }
    }

    private static void HandleRequests(string command, byte[] data)
    {
        if (command == "ANCS_NotificationRequest")
        {
            Console.WriteLine("ANCS_NotificationRequest received");
            _requestQueue.Add((command, data));
            var uid = BitConverter.ToUInt32(data.Skip(1).Take(4).ToArray());
            if (_notifications.TryGetValue(uid, out var notification))
            {
                CommunicationService.Instance.ExecuteAndForget("AncsAttributeData", notification.GetAttributeResponseBody(GetRequestedAttributes(data.Skip(5))).ToArray());
            }
        }
    }
    
    private static IEnumerable<(AttributeId id, int maxLength)> GetRequestedAttributes(IEnumerable<byte> data)
    {
        AttributeId id = AttributeId.Title;
        int size = 0;
        int scount = -1;
        foreach (var b in data)
        {
            if (scount >= 0)
            {
                size |= b << (scount * 8);
                if (++scount == 2)
                {
                    yield return (id, size);
                    scount = -1;
                    size = 0;
                }
            }
            else
            {
                id = (AttributeId)b;
                switch (id)
                {
                    case AttributeId.Title:
                    case AttributeId.Subtitle:
                    case AttributeId.Message:
                        scount = 0;
                        size = 0;
                        break;               
                    default:
                        yield return (id, 0);
                        break;
                }
            }
        }
    }

    public void Delete(Notification notification)
    {
        if (_notifications.Remove(notification.UID, out var removed))
        {
            removed.Delete();
            CommunicationService.Instance.ExecuteAndWait("Notification", notification.GetNotificationSourceData().ToArray());
        }
    }

    public bool IsRegistered(Notification notification)
    {
        return _notifications.ContainsKey(notification.UID);
    }
}
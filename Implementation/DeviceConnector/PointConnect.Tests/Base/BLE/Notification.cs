using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PointConnect.Tests.Base.BLE;

public class Notification
{
    public string Title { get; }
    public string Message { get; }
    public uint UID => _uid;

    private static UInt32 _uidCounter = 0;
    
    private readonly UInt32 _uid;
    private bool _sent = false;

    private bool IsRegistered => NotificationCenter.Instance.IsRegistered(this);

    public Notification(string title, string message)
    {
        Title = title;
        Message = message;
        _uid = (Interlocked.Increment(ref _uidCounter) & 0x00FFFFFF) | 0x42000000;
    }

    internal IEnumerable<byte> GetAttributeResponseBody(IEnumerable<(AttributeId id, int maxLength)> requestList)
    {
        yield return 0;
        foreach (var b in BitConverter.GetBytes(_uid))
        {
            yield return b;
        }
        
        foreach (var attribute in requestList)
        {
            yield return (byte)attribute.id;
            Console.WriteLine("ANCS Request for " + Enum.GetName(attribute.id));
            switch (attribute.id)
            {
                case AttributeId.Title:
                    foreach (var b in ToAttributeDataBlock(Title))
                    {
                        yield return b;
                    }
                    break;
                case AttributeId.Message:
                    foreach (var b in ToAttributeDataBlock(Message))
                    {
                        yield return b;
                    }
                    break;
                case AttributeId.AppIdentifier:
                    foreach (var b in ToAttributeDataBlock("Telegram"))
                    {
                        yield return b;
                    }
                    break;
                default:
                    yield return 0;
                    break;
            }
        }
    }

    internal IEnumerable<byte> GetNotificationSourceData()
    {
        // EventID
        if (!IsRegistered)
        {
            yield return 2;
        }
        else if (_sent)
        {
            yield return 1;
        }
        else
        {
            _sent = true;
            yield return 0;
        }
        
        // Event Flags
        yield return 0;
        
        // Category ID
        yield return 0;
        
        // Category Count - Currently not used
        yield return 1;

        foreach (var b in BitConverter.GetBytes(_uid))
        {
            yield return b;
        }
    }

    private IEnumerable<byte> ToAttributeDataBlock(string text)
    {
        if (text.Length == 0)
        {
            yield return 0;
            yield return 0;
        }
        else
        {
            foreach (var b in BitConverter.GetBytes((UInt16) text.Length))
            {
                yield return b;
            }

            foreach (var b in Encoding.UTF8.GetBytes(text))
            {
                yield return b;
            }
        }
    }

    private void SendAttributes()
    {
        CommunicationService.Instance.ExecuteAndWait("AncsAttributeData", BuildMessage().ToArray());
    }

    private IEnumerable<byte> BuildMessage()
    {
        yield return 0; // CommandID
        foreach (var b in BitConverter.GetBytes(_uid))
        {
            yield return b;
        }

        yield return (byte) AttributeId.Title;
    }

    public override int GetHashCode()
    {
        return unchecked((int)_uid);
    }

    public void Delete()
    {
        NotificationCenter.Instance.Delete(this);
    }
}
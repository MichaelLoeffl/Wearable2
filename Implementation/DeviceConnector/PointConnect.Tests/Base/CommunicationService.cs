using System;
using System.Collections.Generic;
using System.Diagnostics;
using PointConnector;
using PointConnector.DeviceManager;
using PointConnector.StreamComposing;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace PointConnect.Tests.Base;

public class CommunicationChannel
{
    public CommunicationChannel()
    {
        
    }
}

public class CommunicationService
{
    private IDeviceController? _controller;

    private static CommunicationService _instance = new();

    public event Action<string, byte[]>? BinaryDataReceived;
    public event Action<Image<Rgb24>>? ScreenshotReady;

    private bool _initialized;
    
    public static CommunicationService Instance => _instance;

    public String ScreenshotName { get; set; } = "";
    
    private CommunicationService()
    {
        
    }

    private void ControllerOnBinaryPackageReceived(PackageType type, string identifier, byte[] data, ushort coalitionid)
    {
        BinaryDataReceived?.Invoke(identifier, data);
        if (identifier == "Screenshot")
        {
            IEnumerable<Rgb24> ScreenshotDecoder(IEnumerable<byte> screenshotData)
            {
                Rgb24 DecodePixel(UInt16 pixel)
                {
                    var red = (byte)Math.Clamp((float)((pixel >> 11) & 0x1F) / 0x1F * 255, 0, 255);
                    var green = (byte)Math.Clamp((float)((pixel >> 5) & 0x3F) / 0x3F * 255, 0, 255);
                    var blue = (byte)Math.Clamp((float)((pixel >> 0) & 0x1F) / 0x1F * 255, 0, 255);

                    // Make sure to not return masking color but stay close enough to the original image
                    if (red == 255 && green == 0 && blue == 255)
                    {
                        red = 254;
                        blue = 254;
                    }
                    return new Rgb24(red, green, blue);
                }

                using var stream = screenshotData.GetEnumerator();
                while (stream.MoveNext())
                {
                    byte b = stream.Current;
                    var count = b & 0x7F;
                    if ((b & 0x80) != 0)
                    {
                        stream.MoveNext();
                        b = stream.Current;
                        count |= (b & 0x7F) << 7;
                    }
                    
                    // New Color
                    stream.MoveNext();
                    ushort c = stream.Current;
                    stream.MoveNext();
                    c |= (ushort)((stream.Current << 8) & 0xFF00);
                    var color = DecodePixel(c);
                    
                    for (int i = 0; i < count; i++)
                    {
                        yield return color;
                    }
                }
            }

            var image = new Image<Rgb24>(56, 128);
            int x = 0;
            int y = 0;
            foreach (var pixel in ScreenshotDecoder(data))
            {
                image[image.Width - x - 1, y] = pixel;
                y++;
                if (y == image.Height)
                {
                    x++;
                    x %= image.Width;
                    y = 0;
                }
            }

            ScreenshotReady?.Invoke(image);
        }
    }

    public string ExecuteAndWait(string command, string? data = null)
    {
        Debug.Assert(_controller != null, nameof(_controller) + " != null");
        return _controller.SendAndWaitForCompletion(PackageType.Text, command, data ?? "");
    }
    
    public string ExecuteAndWait(string command, byte[] data)
    {
        Debug.Assert(_controller != null, nameof(_controller) + " != null");
        return _controller.SendAndWaitForCompletion(PackageType.Binary, command, data);
    }
    
    public void ExecuteAndForget(string command, string? data = null)
    {
        Debug.Assert(_controller != null, nameof(_controller) + " != null");
        _controller.SendAndForget(PackageType.Text, command, data ?? "");
    }
    
    public void ExecuteAndForget(string command, byte[] data)
    {
        Debug.Assert(_controller != null, nameof(_controller) + " != null");
        _controller.SendAndForget(PackageType.Binary, command, data);
    }
    
    public string ExecuteMetaAndWait(string command, string? data = null)
    {
        Debug.Assert(_controller != null, nameof(_controller) + " != null");
        return _controller.SendAndWaitForCompletion(PackageType.Meta, command, data ?? "");
    }

    ~CommunicationService()
    {
        try
        {
            _controller!.CloseConnection();
        }
        catch
        {
            // Ignored
        }
                
    }

    public StreamWaitHandle Expect(string[] expectation)
    {
        Debug.Assert(_controller != null, nameof(_controller) + " != null");
        return new StreamWaitHandle(_controller, expectation);
    }

    public void Initialize()
    {
        lock (_instance)
        {
            if (!_initialized)
            {
                _initialized = true;
                _controller = Connector.Connect();
                _controller.BinaryPackageReceived += ControllerOnBinaryPackageReceived;
                _controller.TextPackageReceived += TextReceived;

                Console.WriteLine("Connection established");
                _controller.FlashAndWaitForRestart();
            }
        }
    }

    private void TextReceived(PackageType type, string identifier, string text, ushort coalitionid)
    {
        Console.WriteLine(text);
    }
}
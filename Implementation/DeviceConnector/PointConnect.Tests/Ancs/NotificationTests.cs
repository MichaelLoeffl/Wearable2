using NUnit.Framework;
using PointConnect.Tests.Base;
using PointConnect.Tests.Base.BLE;

namespace PointConnect.Tests.Ancs;

[TestFixture]
public class NotificationTests : BaseTest
{
    [Test]
    public void Test()
    {
        var notification = new Notification("Test", "Lorem ipsum");
        Assert.IsTrue(NotificationCenter.Instance.Add(notification), "Failed to add notification");
        TakeScreenshot("Notification");
    }
}
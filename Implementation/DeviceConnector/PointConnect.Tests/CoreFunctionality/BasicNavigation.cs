using System.Threading;
using NUnit.Framework;
using PointConnect.Tests.Base;

namespace PointConnect.Tests.CoreFunctionality;

[TestFixture]
public class BasicNavigation : BaseTest
{
    [Test]
    public void GoToHomeScreen()
    {
        Navigate(Screens.Home);
        Assert.AreEqual(0, GetScroll());
        TakeScreenshot("Home");
        ScrollToPosition(-164);
        TakeScreenshot("Swiped");
        Assert.AreEqual(-164, GetScroll());
        PressHomeButton();
        // Give some time to scroll back
        Thread.Sleep(500);
        Assert.AreEqual(0, GetScroll());
        TakeScreenshot("BackHome");
    }
}

public enum Screens
{
    Home
}
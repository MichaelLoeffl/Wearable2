namespace PointConnector.StreamComposing;

public interface IDeviceConnection
{
    public event Action ConnectionClosed;
}
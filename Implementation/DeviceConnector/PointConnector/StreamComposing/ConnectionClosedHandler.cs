namespace PointConnector.StreamComposing;

public abstract class ConnectionClosedHandler : IDeviceConnection
{
    private event Action? ConnectionClosedBacking;

    public event Action? ConnectionClosed
    {
        add
        {
            lock (this)
            {
                if (!_closed)
                {
                    ConnectionClosedBacking += value;
                }
                else
                {
                    value?.Invoke();
                }
            }
        }
        remove => ConnectionClosedBacking -= value;
    }

    private bool _closed = false;

    protected void FireConnectionClosed()
    {
        lock (this)
        {
            _closed = true;
            ConnectionClosedBacking?.Invoke();
        }
    }
}
namespace PointConnector.StreamComposing;

public enum PackageType
{
    Unknown = 0,
    Meta = '#',
    BinaryMeta = '&',
    Text = '>',
    Binary = '*',
    CoalitionEnd = ';'
}
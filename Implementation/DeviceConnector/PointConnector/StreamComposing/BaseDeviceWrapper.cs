using System.Collections.Concurrent;

namespace PointConnector.StreamComposing;

public abstract class BaseDeviceWrapper : ConnectionClosedHandler, IDeviceCommunicator
{
    public event TextPackageEvent? TextPackageReceived;
    public event BinaryPackageEvent? BinaryPackageReceived;
    public event Action<ushort>? CoalitionComplete;

    private readonly IDeviceCommunicator? _inner;

    protected IDeviceCommunicator Inner
    {
        get => _inner!;
        init
        {
            _inner = value;
            _inner.TextPackageReceived += InnerOnTextPackageReceived;
            _inner.BinaryPackageReceived += InnerOnBinaryPackageReceived;
            _inner.CoalitionComplete += FireAcknowledge;
            _inner.ConnectionClosed += InnerOnConnectionClosed;
        }
    }

    private void InnerOnConnectionClosed()
    {
        FireConnectionClosed();
        Inner.TextPackageReceived -= InnerOnTextPackageReceived;
        Inner.BinaryPackageReceived -= InnerOnBinaryPackageReceived;
        Inner.CoalitionComplete -= FireAcknowledge;
        Inner.ConnectionClosed -= InnerOnConnectionClosed;
    }

    protected void FireAcknowledge(ushort id)
    {
        if (id != 0)
        {
            CoalitionComplete?.Invoke(id);
        }
    }
    
    public void SendAcknowledge(UInt16 coalitionId)
    {
        Inner.SendAcknowledge(coalitionId);
    }

    public void CloseConnection()
    {
        Inner.CloseConnection();
    }

    private void InnerOnBinaryPackageReceived(PackageType packageType, string identifier, byte[] data, ushort coalitionId)
    {
        this.AckReceive(packageType, identifier, data, coalitionId);
        BinaryPackageReceived?.Invoke(packageType, identifier, data, coalitionId);
    }

    private void InnerOnTextPackageReceived(PackageType packageType, string identifier, string text, ushort coalitionId)
    {
        this.AckReceive(packageType, identifier, text, coalitionId);
        TextPackageReceived?.Invoke(packageType, identifier, text, coalitionId);
    }

    public virtual StreamWaitHandle SendCommand(PackageType type, string command, string? text, UInt16 coalitionId = 0)
    {
        if (type == PackageType.Binary || type == PackageType.BinaryMeta)
        {
            throw new Exception("Invalid package type");
        }
        this.AckSend(type, command, text, coalitionId);
        return Inner.SendCommand(type, command, text, coalitionId);
    }

    public virtual StreamWaitHandle SendCommand(PackageType type, string command, byte[]? data = null, UInt16 coalitionId = 0)
    {
        if ((type == PackageType.Text || type == PackageType.Meta) && data != null)
        {
            throw new Exception("Invalid package type");
        }
        this.AckSend(type, command, data, coalitionId);
        return Inner.SendCommand(type, command, data, coalitionId);
    }

}

static class LogHelper
{
    public static void AckReceive(this object self, PackageType type, string command, byte[]? data, ushort coalitionId)
    {
        var name = (self.GetType().Name + "                                           ").Substring(0, 24);
        var packageType = (Enum.GetName(type)! + "                                    ").Substring(0, 13);
        var c = (command + ":" + coalitionId + "                                      ").Substring(0, 20);
        StreamJournal.Log(
            $"↘ {name} {packageType} {c} {FormatData(data)}");
    }

    public static  void AckReceive(this object self, PackageType type, string command, string? text, ushort coalitionId)
    {
        var name = (self.GetType().Name + "                                           ").Substring(0, 24);
        var packageType = (Enum.GetName(type)! + "                                    ").Substring(0, 13);
        var c = (command + ":" + coalitionId + "                                      ").Substring(0, 20);
        StreamJournal.Log($"↘ {name} {packageType!} {c} {text ?? null}");
    }

    public static  void AckSend(this object self, PackageType type, string command, string? text, ushort coalitionId)
    {
        var name = (self.GetType().Name + "                                           ").Substring(0, 24);
        var packageType = (Enum.GetName(type)! + "                                    ").Substring(0, 13);
        var c = (command + ":" + coalitionId + "                                      ").Substring(0, 20);
        StreamJournal.Log($"↖ {name} {packageType} {c} {text ?? null}");
    }

    public static  void AckSend(this object self, PackageType type, string command, byte[]? data, ushort coalitionId)
    {
        var name = (self.GetType().Name + "                                           ").Substring(0, 24);
        var packageType = (Enum.GetName(type)! + "                                    ").Substring(0, 13);
        var c = (command + ":" + coalitionId + "                                      ").Substring(0, 20);
        StreamJournal.Log($"↖ {name} {packageType} {c} {FormatData(data)}");
    }

    private static string FormatData(byte[]? data)
    {
        if (data == null)
        {
            return "null";
        }

        if (data.Length >= 16)
        {
            return string.Join(" ", data.Take(15).Select(b => b.ToString("X2"))) + "..."; 
        }
        return string.Join(" ", data.Select(b => b.ToString("X2")));
    }
}
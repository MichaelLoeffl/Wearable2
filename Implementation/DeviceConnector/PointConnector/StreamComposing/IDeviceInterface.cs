namespace PointConnector.StreamComposing;

public delegate void TextPackageEvent(PackageType type, string identifier, string text, UInt16 coalitionId);
public delegate void BinaryPackageEvent(PackageType type, string identifier, byte[] data, UInt16 coalitionId);

public interface IDeviceCommunicator : IDeviceConnection
{
    public event TextPackageEvent TextPackageReceived;
    public event BinaryPackageEvent BinaryPackageReceived;
    public event Action<UInt16> CoalitionComplete;

    public StreamWaitHandle SendCommand(PackageType type, string command, string? text, UInt16 coalitionId = 0);
    public StreamWaitHandle SendCommand(PackageType type, string command, byte[]? data = null, UInt16 coalitionId = 0);
    void SendAcknowledge(ushort coalitionId);
    void CloseConnection();
}
namespace PointConnector.StreamComposing;

public class CoalitionWrapper : BaseDeviceWrapper
{
    private static UInt16 _coalitionId;
    private static object _syncRoot = new object();
    
    public CoalitionWrapper(IDeviceCommunicator inner)
    {
        Inner = inner;
    }

    public override StreamWaitHandle SendCommand(PackageType type, string command, string? text, ushort coalitionId = 0)
    {
        if (coalitionId == 0)
        {
            lock (_syncRoot)
            {
                _coalitionId++;
                if (_coalitionId >= 0xFFFF)
                {
                    _coalitionId = 1;
                }
                
                coalitionId = _coalitionId;
            }
        }
        
        return base.SendCommand(type, command, text, coalitionId);
    }

    public override StreamWaitHandle SendCommand(PackageType type, string command, byte[]? data = null, ushort coalitionId = 0)
    {
        if (coalitionId == 0)
        {
            lock (_syncRoot)
            {
                _coalitionId++;
                if (_coalitionId >= 0xFFFF)
                {
                    _coalitionId = 1;
                }

                coalitionId = _coalitionId;
            }
        }
        return base.SendCommand(type, command, data, coalitionId);
    }
}
using System.Text;

namespace PointConnector.StreamComposing;

public class ChannelComposer : ConnectionClosedHandler, IDeviceCommunicator
{
    public event TextPackageEvent? TextPackageReceived;
    public event BinaryPackageEvent? BinaryPackageReceived;
    public event Action<ushort>? CoalitionComplete;
    public event BinaryPackageEvent? PackageError;

    private readonly PackageComposer _baseComposer;

    public ChannelComposer(PackageComposer baseComposer)
    {
        _baseComposer = baseComposer;
        _baseComposer.ConnectionClosed += FireConnectionClosed;
        _baseComposer.PackageReceived += BaseComposerOnPackageReceived;
        PackageError += (type, identifier, data, id) => Console.WriteLine($"Package processing error: [{id}] {identifier}");
    }

    protected void AcknowledgeCoalition(UInt16 coalition)
    {
        CoalitionComplete?.Invoke(coalition);
    }

    private void BaseComposerOnPackageReceived(List<byte> package)
    {
        if (package.Count <= 0)
        {
            // empty package
            return;
        }

        string helper = Encoding.UTF8.GetString(package.ToArray());

        var packageType = GetPackageType(package[0]);
        UInt16 coalitionId = 0;
        var dataIdentifier = string.Empty;
        var data = new List<byte>();
        
        try
        {
            var position = 1;
            if (package[position] == ':')
            {
                position++;
                coalitionId = package[position++];
                coalitionId <<= 8;
                coalitionId |= package[position++];
            }

            if (packageType == PackageType.CoalitionEnd)
            {
                StreamJournal.Log($"↘ {GetType().Name} Ack: " + coalitionId);
                CoalitionComplete?.Invoke(coalitionId);
                return;
            }

            var identifierPosition = position;
            var identifierLength = 0;
            foreach (var b in package.Skip(position))
            {
                if (b == ' ')
                {
                    data = package.GetRange(identifierPosition + identifierLength + 1, package.Count - identifierLength - identifierPosition - 1);
                    break;
                }
                identifierLength++;
            }
            
            dataIdentifier = Encoding.UTF8.GetString(package.GetRange(identifierPosition, identifierLength).ToArray());

            if (packageType == PackageType.Binary || packageType == PackageType.BinaryMeta)
            {
                var dataArray = data.ToArray();
                this.AckReceive(packageType, dataIdentifier, dataArray, coalitionId);
                BinaryPackageReceived?.Invoke(packageType, dataIdentifier, dataArray, coalitionId);
            }
            else
            {
                var text = Encoding.UTF8.GetString(data.ToArray());
                this.AckReceive(packageType, dataIdentifier, text, coalitionId);
                TextPackageReceived?.Invoke(packageType, dataIdentifier, text, coalitionId);
            }
        }
        catch
        {
            var dataArray = data.ToArray();
            StreamJournal.Log($"↘ {GetType().Name} Package error: " + coalitionId + ":" + dataIdentifier);
            this.AckReceive(packageType, dataIdentifier, dataArray, coalitionId);
            PackageError?.Invoke(packageType, dataIdentifier, data?.ToArray() ?? Array.Empty<byte>(), coalitionId);
        }
    }

    private PackageType GetPackageType(byte b)
    {
        if (Enum.IsDefined(typeof(PackageType), (PackageType)b))
        {
            return (PackageType) b;
        }

        return PackageType.Unknown;
    }
    public StreamWaitHandle SendCommand(PackageType type, string command, string? text, UInt16 coalitionId = 0)
    {
        var waitHandle = new StreamWaitHandle(this, coalitionId);
        this.AckSend(type, command, text, coalitionId);
        _baseComposer.WritePackage(CommandBuilder(type, command, Encoding.UTF8.GetBytes(text ?? ""), coalitionId));
        return waitHandle;
    }

    public StreamWaitHandle SendCommand(PackageType type, string command, byte[]? data = null, UInt16 coalitionId = 0)
    {
        var waitHandle = new StreamWaitHandle(this, coalitionId);
        this.AckSend(type, command, data, coalitionId);
        _baseComposer.WritePackage(CommandBuilder(type, command, data, coalitionId));
        return waitHandle;
    }

    IEnumerable<byte> CommandBuilder(PackageType type, string command, byte[]? data, ushort coalitionId)
    {
        yield return (byte)type;

        if (coalitionId != 0)
        {
            yield return (byte) ':';
            yield return (byte)((coalitionId >> 8) & 0xFF);
            yield return (byte)(coalitionId & 0xFF);
        }
        
        foreach (var b in Encoding.UTF8.GetBytes(command))
        {
            yield return b;
        }

        if (data != null && data.Length != 0)
        {
            yield return (byte)' ';
            foreach (var b in data)
            {
                yield return b;
            }
        }
    }

    IEnumerable<byte> AckBuilder(UInt16 coalitionId)
    {
        yield return (byte)PackageType.CoalitionEnd;

        if (coalitionId != 0)
        {
            yield return (byte) ':';
            yield return (byte)((coalitionId >> 8) & 0xFF);
            yield return (byte)(coalitionId & 0xFF);
        }
    }

    public void SendAcknowledge(ushort coalitionId)
    {
        if (coalitionId == 0)
        {
            return;
        }
        _baseComposer.WritePackage(AckBuilder(coalitionId));
    }

    public void CloseConnection()
    {
        _baseComposer.CloseConnection();
    }
}
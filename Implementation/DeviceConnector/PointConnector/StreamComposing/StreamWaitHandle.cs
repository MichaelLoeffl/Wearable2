using System.Text;

namespace PointConnector.StreamComposing;

public class StreamWaitHandle : WaitHandle
{
    private int _found = 0;
    private string[]? _currentLines;
    private readonly Queue<string> _allLines;
    private ManualResetEvent? _event;
    private readonly IDeviceCommunicator _stream;
    private readonly ushort _coalitionId;
    private bool _done = false;
    private StringBuilder _channelText = new StringBuilder();

    public string GetText()
    {
        return _channelText.ToString();
        
    }

    public StreamWaitHandle(IDeviceCommunicator stream, params string[] expect)
    {
        _stream = stream;
        _allLines = new Queue<string>(expect);
        if (_allLines.Any())
        {
            _currentLines = _allLines.Dequeue().Split("\n");
            _stream.TextPackageReceived += StreamOnLineReceived;
        }
        else
        {
            _currentLines = null;
            _done = true;
        }
    }
    
    public StreamWaitHandle(IDeviceCommunicator stream, UInt16 coalitionId)
    {
        _stream = stream;
        _coalitionId = coalitionId;
        _allLines = new Queue<string>();

        if (coalitionId == 0)
        {
            _done = true;
        }
        else
        {
            _stream.TextPackageReceived += StreamOnLineReceived;
            _stream.CoalitionComplete += StreamOnCoalitionComplete;
        }
    }

    private void StreamOnCoalitionComplete(ushort coalitionId)
    {
        if (_coalitionId == coalitionId)
        {
            _done = true;
            _stream.TextPackageReceived -= StreamOnLineReceived;
            _stream.CoalitionComplete -= StreamOnCoalitionComplete;
            _event?.Set();
        }
    }

    private void StreamOnLineReceived(PackageType type, string identifier, string text, ushort coalitionId)
    {
        if (identifier != "Text")
        {
            return;
        }

        if (coalitionId == _coalitionId || coalitionId == 0 || _coalitionId == 0)
        {
            _channelText.AppendLine(text);
        }

        if (_currentLines == null)
        {
            return;
        }

        if (_currentLines.Length > _found)
        {
            if (text == _currentLines[_found])
            {
                _found++;
            }
            else
            {
                _found = 0;
            }
        }

        if (_currentLines.Length == _found)
        {
            if (_allLines.Any())
            {
                _currentLines = _allLines.Dequeue().Split("\n");
                _found = 0;
            }
            else
            {
                _currentLines = null;
                _done = true;
                _stream.TextPackageReceived -= StreamOnLineReceived;
                _stream.CoalitionComplete -= StreamOnCoalitionComplete;
                _event?.Set();
            }
        }
    }

    public override void Close()
    {
        _event?.Close();
        Dispose();
    }

    protected override void Dispose(bool explicitDisposing)
    {
        _event?.Dispose();
        _stream.TextPackageReceived -= StreamOnLineReceived;
        _stream.CoalitionComplete -= StreamOnCoalitionComplete;
        base.Dispose(explicitDisposing);
    }

    public override bool WaitOne()
    {
        if (_done)
        {
            return true;
        }
        _event ??= new ManualResetEvent(false);
        return _event.WaitOne();
    }

    public override bool WaitOne(int millisecondsTimeout)
    {
        if (_done)
        {
            return true;
        }
        _event ??= new ManualResetEvent(false);
        return _event.WaitOne(millisecondsTimeout);
    }

    public override bool WaitOne(int millisecondsTimeout, bool exitContext)
    {
        if (_done)
        {
            return true;
        }
        _event ??= new ManualResetEvent(false);
        return _event.WaitOne(millisecondsTimeout, exitContext);
    }

    public override bool WaitOne(TimeSpan timeout)
    {
        if (_done)
        {
            return true;
        }
        _event ??= new ManualResetEvent(false);
        return _event.WaitOne(timeout);
    }

    public override bool WaitOne(TimeSpan timeout, bool exitContext)
    {
        if (_done)
        {
            return true;
        }
        _event ??= new ManualResetEvent(false);
        return _event.WaitOne(timeout, exitContext);
    }
}
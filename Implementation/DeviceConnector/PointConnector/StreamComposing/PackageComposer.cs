using System.Text;
using System.Text.Unicode;

namespace PointConnector.StreamComposing;

public class PackageComposer
{
    private Stream? _baseStream;
    private readonly byte[] _buffer = new byte[512];

    private readonly List<Byte> _readBuffer = new();
    
    public event Action<List<byte>>? PackageReceived;
    public event Action? ConnectionClosed;
    private bool _readEscape = false;

    private static bool IsEscapeChar(byte b)
    {
        return b == '\0' || b == '\n' || b == '\\';
    }
    
    public PackageComposer(Stream baseStream)
    {
        _baseStream = baseStream;
        
        _baseStream.BeginRead(_buffer, 0, _buffer.Length, OnData, null);
    }

    private void OnData(IAsyncResult ar)
    {
        var stream = _baseStream;
        if (stream == null)
        {
            return;
        }

        try
        {
            var bytes = stream.EndRead(ar);
            if (bytes > 0)
            {
                foreach (var b in _buffer.Take(bytes))
                {
                    if (!_readEscape)
                    {
                        switch (b)
                        {
                            case (byte) '\0':
                            case (byte) '\n':
                                PackageReceived?.Invoke(_readBuffer);
                                _readBuffer.Clear();
                                continue;
                            case (byte) '\\':
                                _readEscape = true;
                                continue;
                        }
                    }

                    _readBuffer.Add(b);
                    _readEscape = false;
                }

                stream.BeginRead(_buffer, 0, _buffer.Length, OnData, null);

                return;
            }
            
            CloseConnection();
        }
        catch (ObjectDisposedException)
        {
            CloseConnection();
        }
        catch (IOException)
        {
            CloseConnection();
        }
    }

    public void CloseConnection()
    {
        lock (_buffer)
        {
            var stream = _baseStream;
            if (stream != null)
            {
                _baseStream = null;
                ConnectionClosed?.Invoke();
                stream.Dispose();
            }
        }
    }
    
    public void WritePackage(IEnumerable<byte> bytes)
    {
        try
        {
            var stream = _baseStream;
            if (stream == null)
            {
                return;
            }
            lock (stream)
            {
#if DEBUG
                var tmp = bytes.ToArray();
                var message = Encoding.UTF8.GetString(tmp);
                bytes = tmp;
#endif

                var flushCount = 0;
                foreach (var b in bytes)
                {
                    if (IsEscapeChar(b))
                    {
                        stream.WriteByte((byte) '\\');
                    }

                    stream.WriteByte(b);

                    if (flushCount++ >= 62)
                    {
                        stream.Flush();
                        flushCount = 0;
                    }
                }

                stream.WriteByte((byte) '\n');
                stream.Flush();
            }
        }
        catch
        {
            CloseConnection();
        }
    }
}
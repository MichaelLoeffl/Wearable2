namespace PointConnector.Settings;

public class RepositoryConfig
{
    public string OutputHexPath { get; set; } = "../../../../../../Build/CypressDevelopmentBoard/Debug/PointConnectOS.hex";
    public string GeneratedSourcesPath { get; set; } = ".";
}
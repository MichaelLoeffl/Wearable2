using PointConnector.StreamComposing;

namespace PointConnector.DeviceManager;

internal class DeviceControllerImpl : BaseDeviceWrapper, IDeviceController
{
    public DeviceControllerImpl(IDeviceCommunicator communicator)
    {
        Inner = communicator;
    }
    
    public string FlashAndWaitForRestart()
    {
        var handle = new StreamWaitHandle(Inner, "------------------------------\nStartup complete\n------------------------------");

        StreamJournal.Log("Flashing file " + Path.GetFullPath(Config.Instance.Repository.OutputHexPath));
        var payload = File.ReadAllBytes(Config.Instance.Repository.OutputHexPath);
        this.AckSend(PackageType.BinaryMeta, "Flash", payload, 0);
        Inner.SendCommand(PackageType.BinaryMeta, "Flash", payload);
        handle.WaitOne(60000);
        return handle.GetText();
    }
    
    public string SendAndWaitForCompletion(PackageType type, string command, byte[] data, int timeout = 3000)
    {
        this.AckSend(type, command, data, 0);
        var handle = Inner.SendCommand(type, command, data);
        handle.WaitOne(timeout);
        return handle.GetText();
    }

    public void SendAndForget(PackageType type, string command, string data)
    {
        this.AckSend(type, command, data, 0);
        Inner.SendCommand(type, command, data);
    }

    public void SendAndForget(PackageType type, string command, byte[] data)
    {
        this.AckSend(type, command, data, 0);
        Inner.SendCommand(type, command, data);
    }

    public string SendAndWaitForCompletion(PackageType type, string command, string data, int timeout = 3000)
    {
        this.AckSend(type, command, data, 0);
        var handle = Inner.SendCommand(type, command, data);
        handle.WaitOne(timeout);
        return handle.GetText();
    }
}
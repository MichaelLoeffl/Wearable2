using PointConnector.StreamComposing;

namespace PointConnector.DeviceManager;

public interface IDeviceController : IDeviceCommunicator
{
    public string FlashAndWaitForRestart();
    string SendAndWaitForCompletion(PackageType type, string command, string data, int timeout = 3000);
    string SendAndWaitForCompletion(PackageType type, string command, byte[] data, int timeout = 3000);
    
    void SendAndForget(PackageType type, string command, string data);
    void SendAndForget(PackageType type, string command, byte[] data);
}
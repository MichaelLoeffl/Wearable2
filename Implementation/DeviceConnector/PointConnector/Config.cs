using System.Text.Json;
using PointConnector.Settings;

namespace PointConnector;

public class Config
{
    public OpenOcdConfig OpenOcd { get; set; } = new();
    public RepositoryConfig Repository {get;set;} = new();
    
    private static Config? _instance;

    public static Config Instance
    {
        get
        {
            try
            {
                return _instance ??= JsonSerializer.Deserialize<Config>(File.ReadAllText("Config.json"))!;
            }
            catch
            {
                Console.WriteLine("Unable to load Config.json");
                _instance ??= new Config();

                return _instance;
            }
        }
    }

    public Config()
    {
    }
}
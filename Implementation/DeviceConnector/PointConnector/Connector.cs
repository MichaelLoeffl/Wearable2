using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using PointConnector.DeviceManager;
using PointConnector.Hardware;
using PointConnector.StreamComposing;

namespace PointConnector;

public class Connector
{
    public static IDeviceController Connect(string? port = null)
    {
        try
        {
            return Wrap(new UartDevice(port));
        }
        catch
        {
            Console.WriteLine("Cannot connect to local device");
            // No device available
        }
        
        try
        {
            Console.WriteLine("Trying to connect to local service");
            var client = new TcpClient();
            client.Connect(IPAddress.Loopback, 25531);
            Console.WriteLine("Success");
            return Wrap(Wrap(client.GetStream()));
        }
        catch (Exception e)
        {
            Console.WriteLine("Cannot connect to local service: " + e.Message);
            // No service available
        }
        
        try
        {
            Console.WriteLine("Trying to connect to remote service");
            var client = new TcpClient();
            client.Connect("HardwareRocker", 25531);
            return Wrap(Wrap(client.GetStream()));
        }
        catch (Exception e)
        {
            Console.WriteLine("Cannot connect to remote service: " + e.Message);
            // No hardware rocker available
            throw;
        }
    }

    public static IDeviceCommunicator Wrap(Stream stream)
    {
        return new ChannelComposer(new PackageComposer(stream));
    }
    
    public static IDeviceController Wrap(IDeviceCommunicator device)
    {
        return new DeviceControllerImpl(new CoalitionWrapper(device));
    }
}
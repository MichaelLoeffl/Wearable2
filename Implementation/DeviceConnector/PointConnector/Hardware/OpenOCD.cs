using System.Diagnostics;
using System.IO.MemoryMappedFiles;
using System.Reflection;
using NetCoreAudio;

namespace PointConnector.Hardware;

public static class OpenOCD
{
    static readonly Player Player = new();

    private static string AddParameter(string command, string path)
    {
        return " " + command + " \"" + path + "\" ";
    }
    public static void FlashDevice(string file)
    {
        try
        {
            Player.Play(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "LongFlash.mp3"));
        }
        catch (Exception e)
        {
            Console.WriteLine("Exception: " + e);
        }
        
        try
        {
            string tools = Path.GetFullPath(Config.Instance.OpenOcd.ToolsPath);
            string generatedSources = Path.GetFullPath(Config.Instance.Repository.GeneratedSourcesPath);
            Console.WriteLine("WorkingDirectory: " + Environment.CurrentDirectory);
            Console.WriteLine("ToolsPath: " + tools);
            Console.WriteLine("GeneratedSources: " + Config.Instance.Repository.GeneratedSourcesPath);
            Console.WriteLine("Filename: " + file);

            var flashProcess = new Process();
            
            flashProcess.StartInfo = new ProcessStartInfo
            {
                WorkingDirectory = Config.Instance.OpenOcd.ToolsPath,
                FileName = Path.Combine(Config.Instance.OpenOcd.ToolsPath, "openocd/bin/openocd"),
                Arguments = AddParameter("-s", Path.Combine(Config.Instance.OpenOcd.ToolsPath, "openocd/bin/")) +
                            AddParameter("-s", Config.Instance.Repository.GeneratedSourcesPath) +
                            AddParameter("-f", "interface/kitprog3.cfg") +
                            AddParameter("-f", "target/psoc6.cfg") +
                            AddParameter("-c", "psoc6.cpu.cm4 configure -rtos auto -rtos-wipe-on-reset-halt 10") +
                            AddParameter("-c", "psoc6 sflash_restrictions 1") +
                            AddParameter("-c", "init; reset init; flash write_image erase " + file + @"; init; reset init; reset run; exit")
                ,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = false,
                UseShellExecute = false
            };
            
            Console.WriteLine(flashProcess.StartInfo.FileName + "  " + flashProcess.StartInfo.Arguments);

            flashProcess.StartInfo.Arguments = flashProcess.StartInfo.Arguments.ReplaceLineEndings("");

            Console.WriteLine("Flashing...");
            flashProcess.Start();
            flashProcess.WaitForExit(60000);
            if (!flashProcess.HasExited)
            {
                flashProcess.Kill();
            }

            if (flashProcess.ExitCode != 0)
            {
                var output = flashProcess.StandardOutput.ReadToEnd();
                Console.Write(output);
                output = flashProcess.StandardError.ReadToEnd();
                Console.Error.Write(output);
                throw new Exception("Unable to flash device");
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Exception: "+ e);
            throw new Exception("Unable to flash device");
        }
        finally
        {
            Player.Stop();            
        }
    }
}
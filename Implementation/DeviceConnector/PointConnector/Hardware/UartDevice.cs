using System.IO.Ports;
using PointConnector.StreamComposing;

namespace PointConnector.Hardware;

public class UartDevice : BaseDeviceWrapper
{
    private const int SerialPortBaudRate = 115200;
    private const Parity SerialPortParity = Parity.None;
    private const int SerialPortDataBits = 8;
    private const StopBits SerialPortStopBits = StopBits.One;
    private const Handshake SerialPortHandshake = Handshake.None;

    public static string? Port = null;

    public UartDevice(string? port = null)
    {
        var serialPort = new SerialPort();

        // Allow the user to set the appropriate properties.
        serialPort.PortName = port ?? Port ?? SerialPort.GetPortNames().FirstOrDefault() ?? "None";
        serialPort.BaudRate = SerialPortBaudRate;
        serialPort.Parity = SerialPortParity;
        serialPort.DataBits = SerialPortDataBits;
        serialPort.StopBits = SerialPortStopBits;
        serialPort.Handshake = SerialPortHandshake;
        
        serialPort.Open();
        Inner = Connector.Wrap(serialPort.BaseStream); 
    }

    public StreamWaitHandle RetryHandler(string command, Func<StreamWaitHandle> func)
    {
        for (int i = 0; i < 3; i++)
        {
            using var execCheck = new StreamWaitHandle(this, "Executing: " + command);
            var waitHandle = func();
            if (execCheck.WaitOne(2500))
            {
                if (waitHandle.WaitOne(30000))
                {
                    return waitHandle;
                }
                
                throw new Exception("Command '" + command + "' registered but coalition not closed");
            }

            execCheck.GetText();
            Console.WriteLine($"Execute '{command}' failed - retry");
        }

        throw new Exception("Unable to execute command '" + command + "' - no positive response");
    }

    public override StreamWaitHandle SendCommand(PackageType type, string command, string? text, ushort coalitionId = 0)
    {
        switch (type)
        {
            case PackageType.Meta:
            {
                var waitHandle = new StreamWaitHandle(this, coalitionId);
                this.AckReceive(type, command, text, coalitionId);
                HandleMetaCommands(type, command, text, coalitionId);
                return waitHandle;
            }
            
            case PackageType.Text:
            {
                return RetryHandler(command, () => base.SendCommand(type, command, text, coalitionId));
            }
            
            default:
                throw new Exception("Invalid package type");
        }
    }

    public override StreamWaitHandle SendCommand(PackageType type, string command, byte[]? data = null, UInt16 coalitionId = 0)
    {
        this.AckSend(type, command, data, coalitionId);
        switch (type)
        {
            case PackageType.BinaryMeta:
            {
                var waitHandle = new StreamWaitHandle(this, coalitionId);
                this.AckReceive(type, command, data, coalitionId);
                HandleMetaCommands(type, command, data, coalitionId);
                return waitHandle;
            }
            
            case PackageType.Binary:
            {
                return RetryHandler(command, () => base.SendCommand(type, command, data, coalitionId));
            }
            
            default:
                throw new Exception("Invalid package type");
        }
    }

    private void HandleMetaCommands(PackageType type, string command, string? text, ushort coalitionId)
    {
        
    }

    private void HandleMetaCommands(PackageType type, string command, byte[]? data, ushort coalitionId)
    {
        if (command == "Flash")
        {
            if (data != null)
            {
                bool flashed = false;
                try
                {
                    var file = Path.GetTempFileName();
                    File.WriteAllBytes(file, data);

                    var lastflashedHex = "LastFlashed.hex";
                    if (!TestFileEqual(file, lastflashedHex))
                    {
                        OpenOCD.FlashDevice(file);
                        flashed = true;
                        try
                        {
                            File.Delete(lastflashedHex);
                            File.Copy(file, lastflashedHex);
                        }
                        catch 
                        {
                            // ignored
                        }
                    }
                    File.Delete(file);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    if (!flashed)
                    {
                        SendCommand(PackageType.Text, "Restart", "", coalitionId);
                    }
                    FireAcknowledge(coalitionId);
                }
            }
        }
    }

    private bool TestFileEqual(string file, string lastflashedHex)
    {
        try
        {
            var file1 = File.ReadAllBytes(file);
            var file2 = File.ReadAllBytes(lastflashedHex);
            return file1.SequenceEqual(file2);
        }
        catch
        {
            return false;
        }
        
    }
}

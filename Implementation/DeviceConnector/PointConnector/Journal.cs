namespace PointConnector;

public static class StreamJournal
{
    private static readonly string FileName = "journal.log";

    static StreamJournal()
    {
        if (File.Exists(FileName))
        {
            File.WriteAllText(FileName, "New log\n");
        }
    }

    public static void Log(string message)
    {
        lock (FileName)
        {
            File.AppendAllText(FileName, $"[{DateTime.Now.ToString("s")}] {message}\n");
        }
    }
}
﻿using System.Collections.ObjectModel;
using System.IO.Ports;
using PointConnector.TcpConnection;

namespace PointConnector;

public static class Program
{
    [STAThread]
    public static int Main(string[] args)
    {
        var portName = SelectSerialPort(args.FirstOrDefault());
        
        /*if (portName == null)
        {
            Console.WriteLine("No serial port selected - exiting");
            return 1;
        }*/

        var controller = Connector.Connect(portName);
        controller.TextPackageReceived += (_, identifier, text, id) =>
        {
            if (identifier == "Text")
            {
                Console.WriteLine($"[{id}] {text}");
            }
        };
            
        
        var ethernetService = new EthernetService(controller);

        ethernetService.WaitForExit();
        return 0;
    }

    private static string? SelectSerialPort(string? argument = null)
    {
        var selectedPort = argument;

        var names = SerialPort.GetPortNames();
        if (names.Contains(selectedPort))
        {
            return selectedPort;
        }

        if (names.Length == 0)
        {
            Console.WriteLine("No serial ports available");
            return null;
        }

        if (names.Length == 1)
        {
            return names[0];
        }

        selectedPort = names[0];
        
        while (true)
        {
            Console.Clear();

            if (!names.Contains(selectedPort))
            {
                selectedPort = names.FirstOrDefault();
            }
            
            foreach (var name in names)
            {
                if (name == selectedPort)
                {
                    Console.WriteLine("> " + name);
                }
                else
                {
                    Console.WriteLine("  " + name);
                }
            }

            var key = Console.ReadKey();

            var selectedIndex = Array.FindIndex(names, s => s == selectedPort);
            switch (key.Key)
            {
                case ConsoleKey.DownArrow:
                    selectedIndex++;                    
                    break;
                    
                case ConsoleKey.UpArrow:
                    selectedIndex--;
                    break;
                    
                case ConsoleKey.Escape:
                    Console.WriteLine("Selection canceled");
                    return null;
                    
                case ConsoleKey.Enter:
                    if (names.Contains(selectedPort))
                    {
                        return selectedPort;
                    }
                    break;
            }

            if (selectedIndex < names.Length && selectedIndex >= 0)
            {
                selectedPort = names[selectedIndex];
            }
            
            names = SerialPort.GetPortNames();
        }
    }
}
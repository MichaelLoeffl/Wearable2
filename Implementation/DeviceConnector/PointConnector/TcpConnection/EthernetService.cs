using System.Net;
using System.Net.Sockets;
using PointConnector.DeviceManager;
using PointConnector.StreamComposing;

namespace PointConnector.TcpConnection;

public class EthernetService
{
    private readonly TcpListener _listener;
    private readonly IDeviceController _communicator;
    private IDeviceController? _client;
    private bool _exit;

    public EthernetService(IDeviceController communicator)
    {
        _communicator = communicator;
        _communicator.TextPackageReceived += CommunicatorOnTextPackageReceived;
        _communicator.BinaryPackageReceived += CommunicatorOnBinaryPackageReceived;
        _communicator.CoalitionComplete += CommunicatorOnCoalitionComplete;
        _communicator.ConnectionClosed += OnExit;
        _listener = new TcpListener(IPAddress.Any, 25531);
        Listen();
    }

    private void OnExit()
    {
        _exit = true;
        try
        {
            _client?.CloseConnection();
        }
        catch
        {
            // ignored
        }
        try
        {
            _client?.CloseConnection();
        }
        catch
        {
            _communicator.CloseConnection();
        }
        try
        {
            _client?.CloseConnection();
        }
        catch
        {
            _listener.Stop();
        }
    }

    private void CommunicatorOnCoalitionComplete(ushort coalitionId)
    {
        var client = _client;
        if (client != null)
        {
            client.SendAcknowledge(coalitionId);
        }
    }

    private void CommunicatorOnTextPackageReceived(PackageType packageType, string identifier, string text, ushort coalitionId)
    {
        this.AckReceive(packageType, identifier, text, coalitionId);
        var client = _client;
        if (client != null)
        {
            this.AckSend(packageType, identifier, text, coalitionId);
            client.SendCommand(packageType, identifier, text, coalitionId);
        }
    }
    
    private void CommunicatorOnBinaryPackageReceived(PackageType packageType, string identifier, byte[] data, ushort coalitionId)
    {   
        this.AckReceive(packageType, identifier, data, coalitionId);
        var client = _client;
        if (client != null)
        {
            this.AckSend(packageType, identifier, data, coalitionId);
            client.SendCommand(packageType, identifier, data, coalitionId);
        }
    }

    private void Listen()
    {
        Console.WriteLine("Listening for clients...");
        _listener.Start();
        _listener.BeginAcceptTcpClient(OnNewClient, _communicator);
    }

    private void OnNewClient(IAsyncResult ar)
    {
        Console.WriteLine("Client connected");
        _listener.Stop();
        
        _client = Connector.Wrap(Connector.Wrap(_listener.EndAcceptTcpClient(ar).GetStream()));
        _client.TextPackageReceived += ClientOnTextPackageReceived;
        _client.BinaryPackageReceived += ClientOnBinaryPackageReceived;
        _client.CoalitionComplete += AckReceived;
        _client.ConnectionClosed += ClientOnConnectionClosed;
        _client.SendCommand(PackageType.Meta, "Service", "Hello");
    }

    private void AckReceived(ushort coalitionId)
    {
        StreamJournal.Log($"↘ {GetType().Name} Ack: " + coalitionId);
        Console.WriteLine("C -> S: Ack " + coalitionId);
        _communicator.SendAcknowledge(coalitionId);
    }

    private void ClientOnBinaryPackageReceived(PackageType packageType, string identifier, byte[] data, ushort coalitionId)
    {
        this.AckReceive(packageType, identifier, data, coalitionId);
        Console.WriteLine("C -> S: " + Enum.GetName(packageType) + " " + identifier + " - " + data.Length + "bytes");
        
        this.AckSend(packageType, identifier, data, coalitionId);
        _communicator.SendCommand(packageType, identifier, data?.ToArray(), coalitionId);

        if (packageType == PackageType.BinaryMeta)
        {
            HandleMeta(identifier);
        }
    }

    private void HandleMeta(string identifier)
    {
        if (identifier == "Exit")
        {
            OnExit();
        }
    }

    private void ClientOnTextPackageReceived(PackageType packageType, string identifier, string text, ushort coalitionId)
    {
        this.AckReceive(packageType, identifier, text, coalitionId);
        Console.WriteLine("C -> S: " + Enum.GetName(packageType) + " " + identifier +  " - " + text);
        this.AckSend(packageType, identifier, text, coalitionId);
        _communicator.SendCommand(packageType, identifier, text, coalitionId);
        
        if (packageType == PackageType.Meta)
        {
            HandleMeta(identifier);
        }
    }

    private void ClientOnConnectionClosed()
    {
        if (_client != null)
        {
            _client.TextPackageReceived -= ClientOnTextPackageReceived;
            _client.BinaryPackageReceived -= ClientOnBinaryPackageReceived;
            _client.CoalitionComplete -= AckReceived;
            _client.ConnectionClosed -= ClientOnConnectionClosed;
            _client = null;
            Console.WriteLine("Client disconnected");
        }

        if (_exit)
        {
            Console.WriteLine("Exiting...");
            return;
        }
        
        Listen();
    }

    public void WaitForExit()
    {
        while (!_exit)
        {
            Thread.Sleep(100);
        }
    }
}
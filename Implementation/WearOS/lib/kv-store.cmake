psoc6_load_library(
  NAME kv-store
  VERSION 1.0.1
)

set(KV_STORE_SOURCES
  ${KV_STORE_DIR}/mtb_kvstore.h
  ${KV_STORE_DIR}/mtb_kvstore.c
)
set(KV_STORE_INCLUDE_DIRS
  ${KV_STORE_DIR}
)
set(KV_STORE_LINK_LIBRARIES
  mtb-hal-cat1
)

add_library(kv-store STATIC EXCLUDE_FROM_ALL ${KV_STORE_SOURCES})
target_include_directories(kv-store PUBLIC ${KV_STORE_INCLUDE_DIRS})
target_link_libraries(kv-store PUBLIC ${KV_STORE_LINK_LIBRARIES})

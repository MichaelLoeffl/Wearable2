psoc6_load_library(
  NAME display-eink-e2271cs021
  VERSION 1.1.0
)

set(DISPLAY_EINK_E2271CS021_SOURCES
  ${DISPLAY_EINK_E2271CS021_DIR}/mtb_e2271cs021.h
  ${DISPLAY_EINK_E2271CS021_DIR}/mtb_e2271cs021.c
  ${DISPLAY_EINK_E2271CS021_DIR}/mtb_e2271cs021_display.h
  ${DISPLAY_EINK_E2271CS021_DIR}/mtb_e2271cs021_hw_interface.h
  ${DISPLAY_EINK_E2271CS021_DIR}/mtb_e2271cs021_hw_interface.c
  ${DISPLAY_EINK_E2271CS021_DIR}/mtb_e2271cs021_pervasive_configuration.h
  ${DISPLAY_EINK_E2271CS021_DIR}/mtb_e2271cs021_pervasive_hardware_driver.h
  ${DISPLAY_EINK_E2271CS021_DIR}/mtb_e2271cs021_pervasive_hardware_driver.c
)
set(DISPLAY_EINK_E2271CS021_INCLUDE_DIRS
  ${DISPLAY_EINK_E2271CS021_DIR}
)
set(DISPLAY_EINK_E2271CS021_LINK_LIBRARIES
  mtb-hal-cat1
)

set(DISPLAY_EINK_E2271CS021_EMWIN_SOURCES
  ${DISPLAY_EINK_E2271CS021_DIR}/configs/emwin/GUIConf.c
  ${DISPLAY_EINK_E2271CS021_DIR}/configs/emwin/GUI_X.c
  ${DISPLAY_EINK_E2271CS021_DIR}/configs/emwin/LCDConf.h
  ${DISPLAY_EINK_E2271CS021_DIR}/configs/emwin/LCDConf.c
  ${DISPLAY_EINK_E2271CS021_DIR}/configs/emwin/emwin.h
)
set(DISPLAY_EINK_E2271CS021_EMWIN_INCLUDE_DIRS
  ${DISPLAY_EINK_E2271CS021_DIR}/configs/emwin
)
set(DISPLAY_EINK_E2271CS021_EMWIN_DEFINES
  EMWIN_ENABLED
)
set(DISPLAY_EINK_E2271CS021_EMWIN_LINK_LIBRARIES
  display-eink-e2271cs021
)

add_library(display-eink-e2271cs021 STATIC EXCLUDE_FROM_ALL ${DISPLAY_EINK_E2271CS021_SOURCES})
target_include_directories(display-eink-e2271cs021 PUBLIC ${DISPLAY_EINK_E2271CS021_INCLUDE_DIRS})
target_link_libraries(display-eink-e2271cs021 PUBLIC ${DISPLAY_EINK_E2271CS021_LINK_LIBRARIES})

add_library(display-eink-e2271cs021-emwin INTERFACE EXCLUDE_FROM_ALL)
target_sources(display-eink-e2271cs021-emwin INTERFACE ${DISPLAY_EINK_E2271CS021_EMWIN_SOURCES})
target_include_directories(display-eink-e2271cs021-emwin INTERFACE ${DISPLAY_EINK_E2271CS021_EMWIN_INCLUDE_DIRS})
target_compile_definitions(display-eink-e2271cs021-emwin INTERFACE ${DISPLAY_EINK_E2271CS021_EMWIN_DEFINES})
target_link_libraries(display-eink-e2271cs021-emwin INTERFACE ${DISPLAY_EINK_E2271CS021_EMWIN_LINK_LIBRARIES})

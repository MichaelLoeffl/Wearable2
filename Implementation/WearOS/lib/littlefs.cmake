psoc6_load_library(
  NAME littlefs
  URL  https://github.com/littlefs-project/littlefs
  TAG  v2.4.1
)

set(LITTLEFS_SOURCES
  ${LITTLEFS_DIR}/lfs.h
  ${LITTLEFS_DIR}/lfs.c
  ${LITTLEFS_DIR}/lfs_util.h
  ${LITTLEFS_DIR}/lfs_util.c
)
set(LITTLEFS_INCLUDE_DIRS
  ${LITTLEFS_DIR}
)

add_library(littlefs STATIC EXCLUDE_FROM_ALL ${LITTLEFS_SOURCES})
target_include_directories(littlefs PUBLIC ${LITTLEFS_INCLUDE_DIRS})

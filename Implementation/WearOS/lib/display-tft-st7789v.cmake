psoc6_load_library(
  NAME display-tft-st7789v
  VERSION 1.0.1
)

set(DISPLAY_TFT_ST7789V_SOURCES
  ${DISPLAY_TFT_ST7789V_DIR}/mtb_st7789v.h
  ${DISPLAY_TFT_ST7789V_DIR}/mtb_st7789v.c
)
set(DISPLAY_TFT_ST7789V_INCLUDE_DIRS
  ${DISPLAY_TFT_ST7789V_DIR}
)
set(DISPLAY_TFT_ST7789V_LINK_LIBRARIES
  mtb-hal-cat1
)

set(DISPLAY_TFT_ST7789V_EMWIN_SOURCES
  ${DISPLAY_TFT_ST7789V_DIR}/configs/emwin/GUIConf.c
  ${DISPLAY_TFT_ST7789V_DIR}/configs/emwin/GUI_X.c
  ${DISPLAY_TFT_ST7789V_DIR}/configs/emwin/LCDConf.h
  ${DISPLAY_TFT_ST7789V_DIR}/configs/emwin/LCDConf.c
  ${DISPLAY_TFT_ST7789V_DIR}/configs/emwin/emwin.h
)
set(DISPLAY_TFT_ST7789V_EMWIN_INCLUDE_DIRS
  ${DISPLAY_TFT_ST7789V_DIR}/configs/emwin
)
set(DISPLAY_TFT_ST7789V_EMWIN_DEFINES
  EMWIN_ENABLED
)
set(DISPLAY_TFT_ST7789V_EMWIN_LINK_LIBRARIES
  display-tft-st7789v
)

add_library(display-tft-st7789v STATIC EXCLUDE_FROM_ALL ${DISPLAY_TFT_ST7789V_SOURCES})
target_include_directories(display-tft-st7789v PUBLIC ${DISPLAY_TFT_ST7789V_INCLUDE_DIRS})
target_link_libraries(display-tft-st7789v PUBLIC ${DISPLAY_TFT_ST7789V_LINK_LIBRARIES})

add_library(display-tft-st7789v-emwin INTERFACE EXCLUDE_FROM_ALL)
target_sources(display-tft-st7789v-emwin INTERFACE ${DISPLAY_TFT_ST7789V_EMWIN_SOURCES})
target_include_directories(display-tft-st7789v-emwin INTERFACE ${DISPLAY_TFT_ST7789V_EMWIN_INCLUDE_DIRS})
target_compile_definitions(display-tft-st7789v-emwin INTERFACE ${DISPLAY_TFT_ST7789V_EMWIN_DEFINES})
target_link_libraries(display-tft-st7789v-emwin INTERFACE ${DISPLAY_TFT_ST7789V_EMWIN_LINK_LIBRARIES})

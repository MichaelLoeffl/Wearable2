psoc6_load_library(
  NAME sensor-xensiv-dps3xx
  VERSION 1.0.0
)

set(SENSOR_XENSIV_DPS3XX_SOURCES
  ${SENSOR_XENSIV_DPS3XX_DIR}/xensiv_dps3xx.h
  ${SENSOR_XENSIV_DPS3XX_DIR}/xensiv_dps3xx.c
  ${SENSOR_XENSIV_DPS3XX_DIR}/xensiv_dps3xx_mtb.h
  ${SENSOR_XENSIV_DPS3XX_DIR}/xensiv_dps3xx_mtb.c
)
set(SENSOR_XENSIV_DPS3XX_INCLUDE_DIRS
  ${SENSOR_XENSIV_DPS3XX_DIR}
)
set(SENSOR_XENSIV_DPS3XX_LINK_LIBRARIES
  mtb-hal-cat1
)

add_library(sensor-xensiv-dps3xx STATIC EXCLUDE_FROM_ALL ${SENSOR_XENSIV_DPS3XX_SOURCES})
target_include_directories(sensor-xensiv-dps3xx PUBLIC ${SENSOR_XENSIV_DPS3XX_INCLUDE_DIRS})
target_link_libraries(sensor-xensiv-dps3xx PUBLIC ${SENSOR_XENSIV_DPS3XX_LINK_LIBRARIES})

psoc6_load_library(
  NAME http-client
  VERSION 1.0.0
)

set(HTTP_CLIENT_SOURCES
  ${HTTP_CLIENT_DIR}/include/cy_http_client_api.h
  ${HTTP_CLIENT_DIR}/source/cy_http_client_api.c
)
set(HTTP_CLIENT_INCLUDE_DIRS
  ${HTTP_CLIENT_DIR}/include
)
set(HTTP_CLIENT_LIBRARIES
  abstraction-rtos
  secure-sockets
  aws-iot-device-sdk-port
)

add_library(http-client STATIC EXCLUDE_FROM_ALL ${HTTP_CLIENT_SOURCES})
target_include_directories(http-client PUBLIC ${HTTP_CLIENT_INCLUDE_DIRS})
target_link_libraries(http-client PUBLIC ${HTTP_CLIENT_LIBRARIES})

psoc6_load_library(
  NAME http-server
  VERSION 2.0.0
)

set(HTTP_SERVER_SOURCES
  ${HTTP_SERVER_DIR}/include/cy_http_server.h
  ${HTTP_SERVER_DIR}/source/cy_http_server.c
  ${HTTP_SERVER_DIR}/source/port/cy_tcpip_port.h
  ${HTTP_SERVER_DIR}/source/port/cy_tls_port.h
  ${HTTP_SERVER_DIR}/source/port/COMPONENT_SECURE_SOCKETS/cy_tcpip_port_secure_sockets.c
  ${HTTP_SERVER_DIR}/source/port/COMPONENT_SECURE_SOCKETS/cy_tls_stack_datastructures.h
)
set(HTTP_SERVER_INCLUDE_DIRS
  ${HTTP_SERVER_DIR}/include
  ${HTTP_SERVER_DIR}/source/port
  ${HTTP_SERVER_DIR}/source/port/COMPONENT_SECURE_SOCKETS
)
set(HTTP_SERVER_LIBRARIES
  abstraction-rtos
  linked-list-utilities
  network-utilities
  secure-sockets
)

add_library(http-server STATIC EXCLUDE_FROM_ALL ${HTTP_SERVER_SOURCES})
target_include_directories(http-server PUBLIC ${HTTP_SERVER_INCLUDE_DIRS})
target_link_libraries(http-server PUBLIC ${HTTP_SERVER_LIBRARIES})

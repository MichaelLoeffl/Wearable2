psoc6_load_library(
  NAME CY8CKIT-028-SENSE
  VERSION 1.0.0
)

set(CY8CKIT_028_SENSE_SOURCES
  ${CY8CKIT_028_SENSE_DIR}/cy8ckit_028_sense.h
  ${CY8CKIT_028_SENSE_DIR}/cy8ckit_028_sense.c
  ${CY8CKIT_028_SENSE_DIR}/cy8ckit_028_sense_pins.h
)
set(CY8CKIT_028_SENSE_INCLUDE_DIRS
  ${CY8CKIT_028_SENSE_DIR}
)
set(CY8CKIT_028_SENSE_LINK_LIBRARIES
  mtb-hal-cat1
  bsp
  audio-codec-wm8960
  display-oled-ssd1306
  sensor-orientation-bmx160
  sensor-xensiv-dps3xx
)

add_library(CY8CKIT-028-SENSE STATIC EXCLUDE_FROM_ALL ${CY8CKIT_028_SENSE_SOURCES})
target_include_directories(CY8CKIT-028-SENSE PUBLIC ${CY8CKIT_028_SENSE_INCLUDE_DIRS})
target_link_libraries(CY8CKIT-028-SENSE PUBLIC ${CY8CKIT_028_SENSE_LINK_LIBRARIES})

psoc6_load_library(
  NAME aws-iot
  URL  https://github.com/aws/aws-iot-device-sdk-embedded-C
  TAG  202011.00
)

include(${AWS_IOT_DIR}/libraries/standard/coreHTTP/httpFilePaths.cmake)
include(${AWS_IOT_DIR}/libraries/standard/coreJSON/jsonFilePaths.cmake)
include(${AWS_IOT_DIR}/libraries/standard/coreMQTT/mqttFilePaths.cmake)
# set(AWS_IOT_HTTP_SOURCES
#   ${AWS_IOT_DIR}/libraries/standard/coreHTTP/source/include/core_http_client.h
#   ${AWS_IOT_DIR}/libraries/standard/coreHTTP/source/include/core_http_client_private.h
#   ${AWS_IOT_DIR}/libraries/standard/coreHTTP/source/include/core_http_config_defaults.h
#   ${AWS_IOT_DIR}/libraries/standard/coreHTTP/source/interface/transport_interface.h
#   ${AWS_IOT_DIR}/libraries/standard/coreHTTP/source/core_http_client.c
  
# )
# set(AWS_IOT_BASE_INCLUDE_DIRS
#   ${AWS_IOT_DIR}/libraries
#   ${AWS_IOT_DIR}/libraries/platform
#   ${AWS_IOT_DIR}/libraries/standard/common/include
#   ${AWS_IOT_DIR}/ports/common/include
# )

add_library(aws-iot-http STATIC EXCLUDE_FROM_ALL ${HTTP_SOURCES})
target_compile_definitions(aws-iot-http PUBLIC HTTP_DO_NOT_USE_CUSTOM_CONFIG)
target_include_directories(aws-iot-http PUBLIC ${HTTP_INCLUDE_PUBLIC_DIRS})

add_library(aws-iot-json STATIC EXCLUDE_FROM_ALL ${JSON_SOURCES})
target_include_directories(aws-iot-json PUBLIC ${JSON_INCLUDE_PUBLIC_DIRS})

add_library(aws-iot-mqtt STATIC EXCLUDE_FROM_ALL ${MQTT_SOURCES} ${MQTT_SERIALIZER_SOURCES})
target_compile_definitions(aws-iot-mqtt PUBLIC MQTT_DO_NOT_USE_CUSTOM_CONFIG)
target_include_directories(aws-iot-mqtt PUBLIC ${MQTT_INCLUDE_PUBLIC_DIRS})

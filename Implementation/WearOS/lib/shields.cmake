# Include shield and sensor libraries
include(lib/audio-codec-wm8960.cmake)
include(lib/display-eink-e2271cs021.cmake)
include(lib/display-oled-ssd1306.cmake)
include(lib/display-tft-st7789v.cmake)
include(lib/bmi160_driver.cmake)
include(lib/bmm155-sensor-api.cmake)
include(lib/sensor-motion-bmi160.cmake)
include(lib/sensor-light.cmake)
include(lib/thermistor.cmake)
include(lib/sensor-xensiv-dps3xx.cmake)
include(lib/CY8CKIT-028-EPD.cmake)
include(lib/CY8CKIT-028-SENSE.cmake)
include(lib/CY8CKIT-028-TFT.cmake)
include(lib/CY8CKIT-032.cmake)

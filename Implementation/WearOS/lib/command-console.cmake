psoc6_load_library(
  NAME command-console
  VERSION 3.0.0
)

set(COMMAND_CONSOLE_SOURCES
  ${COMMAND_CONSOLE_DIR}/include/command_console.h
  ${COMMAND_CONSOLE_DIR}/source/command_console/command_console.c
  ${COMMAND_CONSOLE_DIR}/source/command_console/COMPONENT_PSOC6HAL/command_utility.h
  ${COMMAND_CONSOLE_DIR}/source/command_console/COMPONENT_PSOC6HAL/command_utility.cpp
)
set(COMMAND_CONSOLE_DEFINES
  ANYCLOUD
  HAVE_SNPRINTF
)
set(COMMAND_CONSOLE_INCLUDE_DIRS
  ${COMMAND_CONSOLE_DIR}/include
  ${COMMAND_CONSOLE_DIR}/source/command_console/COMPONENT_PSOC6HAL
)
set(COMMAND_CONSOLE_LINK_LIBRARIES
  mtb-hal-cat1
  retarget-io
  connectivity-utilities
)

set(COMMAND_CONSOLE_BLUETOOTH_SOURCES
  ${COMMAND_CONSOLE_DIR}/source/bluetooth_utility/COMPONENT_WICED_BLE/bt_cfg.h
  ${COMMAND_CONSOLE_DIR}/source/bluetooth_utility/COMPONENT_WICED_BLE/bt_cfg.c
  ${COMMAND_CONSOLE_DIR}/source/bluetooth_utility/COMPONENT_WICED_BLE/bt_utility.h
  ${COMMAND_CONSOLE_DIR}/source/bluetooth_utility/COMPONENT_WICED_BLE/bt_utility.c
)
set(COMMAND_CONSOLE_BLUETOOTH_INCLUDE_DIRS
  ${COMMAND_CONSOLE_DIR}/source/bluetooth_utility
)
set(COMMAND_CONSOLE_BLUETOOTH_LINK_LIBRARIES
  command-console
  bluetooth-freertos
)

set(COMMAND_CONSOLE_IPERF_SOURCES
  ${COMMAND_CONSOLE_DIR}/source/iperf/compat/delay.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/compat/gettimeofday.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/compat/inet_ntop.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/compat/inet_pton.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/compat/iperf_error.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/compat/signal.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/compat/snprintf.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/compat/string.c  
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/Client.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/Extractor.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/Launch.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/List.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/Listener.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/Locale.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/PerfSocket.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/ReportCSV.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/ReportDefault.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/Reporter.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/Server.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/Settings.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/SocketAddr.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/checkdelay.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/checkisoch.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/checkpdfs.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/checksums.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/gnu_getopt.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/gnu_getopt_long.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/histogram.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/igmp_querier.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/isochronous.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/main.cpp
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/pdfs.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/service.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/sockets.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/stdio.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/src/tcp_window_size.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/condition.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/gettimeofday.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/iperf_debug.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/iperf_getopt.h
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/iperf_getopt_long.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/iperf_sockets.h
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/iperf_sockets.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/platform_wait_api.h
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/rtos_config.h
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/thread.c
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS/usleep.c
  ${COMMAND_CONSOLE_DIR}/source/iperf_utility/iperf_utility.h
  ${COMMAND_CONSOLE_DIR}/source/iperf_utility/iperf_utility.cpp
)
set(COMMAND_CONSOLE_IPERF_INCLUDE_DIRS
  ${COMMAND_CONSOLE_DIR}/source/iperf/include
  ${COMMAND_CONSOLE_DIR}/source/iperf/include/COMPONENT_SECURE_SOCKETS
  ${COMMAND_CONSOLE_DIR}/source/iperf/rtos/COMPONENT_SECURE_SOCKETS
  ${COMMAND_CONSOLE_DIR}/source/iperf_utility
)
set(COMMAND_CONSOLE_IPERF_LINK_LIBRARIES
  command-console
  wifi-mw-core
  lwip
)

set(COMMAND_CONSOLE_WIFI_SOURCES
  ${COMMAND_CONSOLE_DIR}/source/wifi_utility/wifi_utility.h
  ${COMMAND_CONSOLE_DIR}/source/wifi_utility/COMPONENT_WCM/wifi_utility.c
)
set(COMMAND_CONSOLE_WIFI_INCLUDE_DIRS
${COMMAND_CONSOLE_DIR}/source/wifi_utility
)
set(COMMAND_CONSOLE_WIFI_LINK_LIBRARIES
  command-console
  wifi-connection-manager
)

add_library(command-console STATIC EXCLUDE_FROM_ALL ${COMMAND_CONSOLE_SOURCES})
target_compile_definitions(command-console PUBLIC ${COMMAND_CONSOLE_DEFINES})
target_include_directories(command-console PUBLIC ${COMMAND_CONSOLE_INCLUDE_DIRS})
target_link_libraries(command-console PUBLIC ${COMMAND_CONSOLE_LINK_LIBRARIES})

add_library(command-console-bluetooth STATIC EXCLUDE_FROM_ALL ${COMMAND_CONSOLE_BLUETOOTH_SOURCES})
target_include_directories(command-console-bluetooth PUBLIC ${COMMAND_CONSOLE_BLUETOOTH_INCLUDE_DIRS})
target_link_libraries(command-console-bluetooth PUBLIC ${COMMAND_CONSOLE_BLUETOOTH_LINK_LIBRARIES})

add_library(command-console-iperf STATIC EXCLUDE_FROM_ALL ${COMMAND_CONSOLE_IPERF_SOURCES})
target_include_directories(command-console-iperf PUBLIC ${COMMAND_CONSOLE_IPERF_INCLUDE_DIRS})
target_link_libraries(command-console-iperf PUBLIC ${COMMAND_CONSOLE_IPERF_LINK_LIBRARIES})

add_library(command-console-wifi STATIC EXCLUDE_FROM_ALL ${COMMAND_CONSOLE_WIFI_SOURCES})
target_include_directories(command-console-wifi PUBLIC ${COMMAND_CONSOLE_WIFI_INCLUDE_DIRS})
target_link_libraries(command-console-wifi PUBLIC ${COMMAND_CONSOLE_WIFI_LINK_LIBRARIES})

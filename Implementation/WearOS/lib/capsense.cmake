psoc6_load_library(
  NAME capsense
  VERSION 3.0.0
)

set(CAPSENSE_SOURCES
  ${CAPSENSE_DIR}/cy_capsense.h
  ${CAPSENSE_DIR}/cy_capsense_centroid.h
  ${CAPSENSE_DIR}/cy_capsense_centroid.c
  ${CAPSENSE_DIR}/cy_capsense_common.h
  ${CAPSENSE_DIR}/cy_capsense_control.h
  ${CAPSENSE_DIR}/cy_capsense_control.c
  ${CAPSENSE_DIR}/cy_capsense_csd_v2.h
  ${CAPSENSE_DIR}/cy_capsense_csd_v2.c
  ${CAPSENSE_DIR}/cy_capsense_csx_v2.h
  ${CAPSENSE_DIR}/cy_capsense_csx_v2.c
  ${CAPSENSE_DIR}/cy_capsense_filter.h
  ${CAPSENSE_DIR}/cy_capsense_filter.c
  ${CAPSENSE_DIR}/cy_capsense_generator_v3.h
  ${CAPSENSE_DIR}/cy_capsense_generator_v3.c
  ${CAPSENSE_DIR}/cy_capsense_gesture_lib.h
  ${CAPSENSE_DIR}/cy_capsense_lib.h
  ${CAPSENSE_DIR}/cy_capsense_processing.h
  ${CAPSENSE_DIR}/cy_capsense_processing.c
  ${CAPSENSE_DIR}/cy_capsense_selftest.h
  ${CAPSENSE_DIR}/cy_capsense_selftest.c
  ${CAPSENSE_DIR}/cy_capsense_selftest_v2.h
  ${CAPSENSE_DIR}/cy_capsense_selftest_v2.c
  ${CAPSENSE_DIR}/cy_capsense_selftest_v3.h
  ${CAPSENSE_DIR}/cy_capsense_selftest_v3.c
  ${CAPSENSE_DIR}/cy_capsense_sensing.h
  ${CAPSENSE_DIR}/cy_capsense_sensing.c
  ${CAPSENSE_DIR}/cy_capsense_sensing_v2.h
  ${CAPSENSE_DIR}/cy_capsense_sensing_v2.c
  ${CAPSENSE_DIR}/cy_capsense_sensing_v3.h
  ${CAPSENSE_DIR}/cy_capsense_sensing_v3.c
  ${CAPSENSE_DIR}/cy_capsense_sm_base_full_wave_v3.h
  ${CAPSENSE_DIR}/cy_capsense_structure.h
  ${CAPSENSE_DIR}/cy_capsense_structure.c
  ${CAPSENSE_DIR}/cy_capsense_tuner.h
  ${CAPSENSE_DIR}/cy_capsense_tuner.c
)
set(CAPSENSE_INCLUDE_DIRS
  ${CAPSENSE_DIR}
)
set(CAPSENSE_LINK_LIBRARIES
  mtb-pdl-cat1
)

if(HARDFP IN_LIST COMPONENTS)
  if(${TOOLCHAIN} STREQUAL GCC OR ${TOOLCHAIN} STREQUAL LLVM)
    list(APPEND CAPSENSE_LINK_LIBRARIES
      ${CAPSENSE_DIR}/COMPONENT_HARDFP/TOOLCHAIN_GCC_ARM/libcy_capsense.a
    )
  elseif(${TOOLCHAIN} STREQUAL ARM)
    list(APPEND CAPSENSE_LINK_LIBRARIES
      ${CAPSENSE_DIR}/COMPONENT_HARDFP/TOOLCHAIN_ARM/libcy_capsense.ar
    )
  elseif(${TOOLCHAIN} STREQUAL IAR)
    list(APPEND CAPSENSE_LINK_LIBRARIES
      ${CAPSENSE_DIR}/COMPONENT_HARDFP/TOOLCHAIN_IAR/libcy_capsense.a
    )
  else()
    message(FATAL_ERROR "capsense: TOOLCHAIN ${TOOLCHAIN} is not supported.")
  endif()
else() # SOFTFP or no FPU
  if(${TOOLCHAIN} STREQUAL GCC OR ${TOOLCHAIN} STREQUAL LLVM)
    list(APPEND CAPSENSE_LINK_LIBRARIES
      ${CAPSENSE_DIR}/COMPONENT_SOFTFP/TOOLCHAIN_GCC_ARM/libcy_capsense.a
    )
  elseif(${TOOLCHAIN} STREQUAL ARM)
    list(APPEND CAPSENSE_LINK_LIBRARIES
      ${CAPSENSE_DIR}/COMPONENT_SOFTFP/TOOLCHAIN_ARM/libcy_capsense.ar
    )
  elseif(${TOOLCHAIN} STREQUAL IAR)
    list(APPEND CAPSENSE_LINK_LIBRARIES
      ${CAPSENSE_DIR}/COMPONENT_SOFTFP/TOOLCHAIN_IAR/libcy_capsense.a
    )
  else()
    message(FATAL_ERROR "capsense: TOOLCHAIN ${TOOLCHAIN} is not supported.")
  endif()
endif()

add_library(capsense INTERFACE EXCLUDE_FROM_ALL)
target_sources(capsense INTERFACE ${CAPSENSE_SOURCES})
target_include_directories(capsense INTERFACE ${CAPSENSE_INCLUDE_DIRS})
target_link_libraries(capsense INTERFACE ${CAPSENSE_LINK_LIBRARIES})
if(TARGET bsp_design_capsense)
  target_include_directories(bsp_design_capsense PUBLIC ${CAPSENSE_INCLUDE_DIRS})
endif()

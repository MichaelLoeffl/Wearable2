psoc6_load_library(
  NAME connectivity-utilities
  VERSION 3.0.2
)

add_library(connectivity-utilities INTERFACE EXCLUDE_FROM_ALL)
target_sources(connectivity-utilities INTERFACE
  ${CONNECTIVITY_UTILITIES_DIR}/cy_result_mw.h
)
target_include_directories(connectivity-utilities INTERFACE
  ${CONNECTIVITY_UTILITIES_DIR}
)
target_link_libraries(connectivity-utilities INTERFACE
  core-lib
)

add_library(log-utilities STATIC EXCLUDE_FROM_ALL
  ${CONNECTIVITY_UTILITIES_DIR}/cy_log/cy_log.h
  ${CONNECTIVITY_UTILITIES_DIR}/cy_log/cy_log.c
)
target_include_directories(log-utilities PUBLIC
  ${CONNECTIVITY_UTILITIES_DIR}/cy_log
)
target_link_libraries(log-utilities PUBLIC
  core-lib
  abstraction-rtos
)

add_library(string-utilities STATIC EXCLUDE_FROM_ALL
  ${CONNECTIVITY_UTILITIES_DIR}/cy_string/cy_string_utils.h
  ${CONNECTIVITY_UTILITIES_DIR}/cy_string/cy_string_utils.c
)
target_include_directories(string-utilities PUBLIC
  ${CONNECTIVITY_UTILITIES_DIR}/cy_string
)

add_library(json-utilities STATIC EXCLUDE_FROM_ALL
  ${CONNECTIVITY_UTILITIES_DIR}/JSON_parser/cy_json_parser.h
  ${CONNECTIVITY_UTILITIES_DIR}/JSON_parser/cy_json_parser.c
)
target_include_directories(json-utilities PUBLIC
  ${CONNECTIVITY_UTILITIES_DIR}/JSON_parser
)
target_link_libraries(json-utilities PUBLIC
  connectivity-utilities
)

add_library(linked-list-utilities STATIC EXCLUDE_FROM_ALL
  ${CONNECTIVITY_UTILITIES_DIR}/linked_list/cy_linked_list.h
  ${CONNECTIVITY_UTILITIES_DIR}/linked_list/cy_linked_list.c
)
target_include_directories(linked-list-utilities PUBLIC
  ${CONNECTIVITY_UTILITIES_DIR}/linked_list
)
target_link_libraries(linked-list-utilities PUBLIC
  connectivity-utilities
)

add_library(network-utilities STATIC EXCLUDE_FROM_ALL
  ${CONNECTIVITY_UTILITIES_DIR}/network/cy_nw_helper.h
  ${CONNECTIVITY_UTILITIES_DIR}/network/COMPONENT_LWIP/cy_nw_helper.c
)
target_include_directories(network-utilities PUBLIC
  ${CONNECTIVITY_UTILITIES_DIR}/network
)
target_link_libraries(network-utilities PRIVATE
  abstraction-rtos
  lwip
)

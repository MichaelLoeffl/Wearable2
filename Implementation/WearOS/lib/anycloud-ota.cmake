psoc6_load_library(
  NAME anycloud-ota
  VERSION 4.0.0
)

set(ANYCLOUD_OTA_MCUBOOT_SOURCES
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/bootutil/include/bootutil/bootutil.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/bootutil/include/bootutil/bootutil_log.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/bootutil/include/bootutil/ignore.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/bootutil/src/bootutil_misc.c
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/bootutil/src/bootutil_priv.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/include/flash_map_backend/flash_map_backend.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/include/cy_flash_psoc6.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/include/cy_smif_psoc6.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/include/cy_smif_psoc6.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/cy_flash_map.c
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/cy_flash_psoc6.c
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/cy_smif_psoc6.c
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/mem_config/mem_config_sfdp.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/mem_config/mem_config_sfdp.c
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/mcuboot_header/mcuboot_config/mcuboot_config.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/mcuboot_header/mcuboot_config/mcuboot_logging.h
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/sysflash/sysflash.h
)
set(ANYCLOUD_OTA_MCUBOOT_INCLUDE_DIRS
  ${ANYCLOUD_OTA_DIR}/source/mcuboot
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/bootutil/include
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/bootutil/include/bootutil
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/include
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/cy_flash_pal/include/flash_map_backend
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/mcuboot_header
  ${ANYCLOUD_OTA_DIR}/source/mcuboot/sysflash
)
set(ANYCLOUD_OTA_MCUBOOT_LINK_LIBRARIES
  mtb-pdl-cat1
  serial-flash
  json-utilities
  bsp
)

add_library(anycloud-ota-mcuboot STATIC EXCLUDE_FROM_ALL ${ANYCLOUD_OTA_MCUBOOT_SOURCES})
target_include_directories(anycloud-ota-mcuboot PUBLIC ${ANYCLOUD_OTA_MCUBOOT_INCLUDE_DIRS})
target_link_libraries(anycloud-ota-mcuboot PUBLIC ${ANYCLOUD_OTA_MCUBOOT_LINK_LIBRARIES})

set(ANYCLOUD_OTA_SOURCES
  ${ANYCLOUD_OTA_DIR}/include/cy_ota_api.h
  ${ANYCLOUD_OTA_DIR}/include/cy_ota_defaults.h
  ${ANYCLOUD_OTA_DIR}/source/cy_ota_agent.c
  ${ANYCLOUD_OTA_DIR}/source/cy_ota_ble.c
  ${ANYCLOUD_OTA_DIR}/source/cy_ota_http.c
  ${ANYCLOUD_OTA_DIR}/source/cy_ota_internal.h
  ${ANYCLOUD_OTA_DIR}/source/cy_ota_mqtt.c
  ${ANYCLOUD_OTA_DIR}/source/cy_ota_storage.c
  ${ANYCLOUD_OTA_DIR}/source/cy_ota_untar.c
  ${ANYCLOUD_OTA_DIR}/source/port_support/untar/untar.h
  ${ANYCLOUD_OTA_DIR}/source/port_support/untar/untar.c
)
set(ANYCLOUD_OTA_INCLUDE_DIRS
  ${ANYCLOUD_OTA_DIR}/include
  ${ANYCLOUD_OTA_DIR}/source/port_support/untar
  ${CMAKE_SOURCE_DIR}/configs/ota
)
set(ANYCLOUD_OTA_LINK_LIBRARIES
  anycloud-ota-mcuboot
  retarget-io
  wifi-connection-manager
  serial-flash
)

add_library(anycloud-ota INTERFACE EXCLUDE_FROM_ALL)
target_sources(anycloud-ota INTERFACE ${ANYCLOUD_OTA_SOURCES})
target_include_directories(anycloud-ota INTERFACE ${ANYCLOUD_OTA_INCLUDE_DIRS})
target_link_libraries(anycloud-ota INTERFACE ${ANYCLOUD_OTA_LINK_LIBRARIES})

add_library(anycloud-ota-bluetooth INTERFACE EXCLUDE_FROM_ALL)
target_compile_definitions(anycloud-ota-bluetooth INTERFACE
  COMPONENT_OTA_BLUETOOTH
)
target_link_libraries(anycloud-ota-bluetooth INTERFACE
  anycloud-ota
  btstack
)

add_library(anycloud-ota-http INTERFACE EXCLUDE_FROM_ALL)
target_compile_definitions(anycloud-ota-http INTERFACE
  COMPONENT_OTA_HTTP
)
target_link_libraries(anycloud-ota-http INTERFACE
  anycloud-ota
  aws-iot-device-sdk-port
  http-client
)

add_library(anycloud-ota-mqtt INTERFACE EXCLUDE_FROM_ALL)
target_compile_definitions(anycloud-ota-mqtt INTERFACE
  COMPONENT_OTA_MQTT
)
target_link_libraries(anycloud-ota-mqtt INTERFACE
  anycloud-ota
  aws-iot-device-sdk-port
  mqtt
)

# TODO: declare paths to custom linker scripts

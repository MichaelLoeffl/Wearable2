psoc6_load_library(
  NAME display-oled-ssd1306
  VERSION 1.0.1
)

set(DISPLAY_OLED_SSD1306_SOURCES
  ${DISPLAY_OLED_SSD1306_DIR}/mtb_ssd1306.h
  ${DISPLAY_OLED_SSD1306_DIR}/mtb_ssd1306.c
  ${DISPLAY_OLED_SSD1306_DIR}/mtb_ssd1306_i2c.h
)
set(DISPLAY_OLED_SSD1306_INCLUDE_DIRS
  ${DISPLAY_OLED_SSD1306_DIR}
)
set(DISPLAY_OLED_SSD1306_LINK_LIBRARIES
  mtb-hal-cat1
)

set(DISPLAY_OLED_SSD1306_EMWIN_SOURCES
  ${DISPLAY_OLED_SSD1306_DIR}/configs/emwin/GUIConf.c
  ${DISPLAY_OLED_SSD1306_DIR}/configs/emwin/GUI_X.c
  ${DISPLAY_OLED_SSD1306_DIR}/configs/emwin/LCDConf.h
  ${DISPLAY_OLED_SSD1306_DIR}/configs/emwin/LCDConf.c
  ${DISPLAY_OLED_SSD1306_DIR}/configs/emwin/emwin.h
)
set(DISPLAY_OLED_SSD1306_EMWIN_INCLUDE_DIRS
  ${DISPLAY_OLED_SSD1306_DIR}/configs/emwin
)
set(DISPLAY_OLED_SSD1306_EMWIN_DEFINES
  EMWIN_ENABLED
)
set(DISPLAY_OLED_SSD1306_EMWIN_LINK_LIBRARIES
  display-oled-ssd1306
)

add_library(display-oled-ssd1306 STATIC EXCLUDE_FROM_ALL ${DISPLAY_OLED_SSD1306_SOURCES})
target_compile_definitions(display-oled-ssd1306 PRIVATE ${DISPLAY_OLED_SSD1306_DEFINES})
target_include_directories(display-oled-ssd1306 PUBLIC ${DISPLAY_OLED_SSD1306_INCLUDE_DIRS})
target_link_libraries(display-oled-ssd1306 PUBLIC ${DISPLAY_OLED_SSD1306_LINK_LIBRARIES})

add_library(display-oled-ssd1306-emwin INTERFACE EXCLUDE_FROM_ALL)
target_sources(display-oled-ssd1306-emwin INTERFACE ${DISPLAY_OLED_SSD1306_EMWIN_SOURCES})
target_include_directories(display-oled-ssd1306-emwin INTERFACE ${DISPLAY_OLED_SSD1306_EMWIN_INCLUDE_DIRS})
target_compile_definitions(display-oled-ssd1306-emwin INTERFACE ${DISPLAY_OLED_SSD1306_EMWIN_DEFINES})
target_link_libraries(display-oled-ssd1306-emwin INTERFACE ${DISPLAY_OLED_SSD1306_EMWIN_LINK_LIBRARIES})

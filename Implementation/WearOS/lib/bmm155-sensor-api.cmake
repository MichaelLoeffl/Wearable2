psoc6_load_library(
  NAME bmm155-sensor-api
  URL  https://github.com/BoschSensortec/BMM150-Sensor-API
  TAG  bmm150_v2.0.0
)

set(BMM155_SENSOR_API_SOURCES
  ${BMM155_SENSOR_API_DIR}/bmm150.h
  ${BMM155_SENSOR_API_DIR}/bmm150.c
  ${BMM155_SENSOR_API_DIR}/bmm150_defs.h
)
set(BMM155_SENSOR_API_INCLUDE_DIRS
  ${BMM155_SENSOR_API_DIR}
)

add_library(bmm155-sensor-api STATIC EXCLUDE_FROM_ALL ${BMM155_SENSOR_API_SOURCES})
target_include_directories(bmm155-sensor-api PUBLIC ${BMM155_SENSOR_API_INCLUDE_DIRS})

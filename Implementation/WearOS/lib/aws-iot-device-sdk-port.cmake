psoc6_load_library(
  NAME aws-iot-device-sdk-port
  VERSION 1.0.1
)

set(AWS_IOT_DEVICE_SDK_PORT_SOURCES
  ${AWS_IOT_DEVICE_SDK_PORT_DIR}/include/cy_tcpip_port_secure_sockets.h
  ${AWS_IOT_DEVICE_SDK_PORT_DIR}/source/COMPONENT_SECURE_SOCKETS/cy_clock.c
  ${AWS_IOT_DEVICE_SDK_PORT_DIR}/source/COMPONENT_SECURE_SOCKETS/cy_retry_utils.c
  ${AWS_IOT_DEVICE_SDK_PORT_DIR}/source/COMPONENT_SECURE_SOCKETS/cy_tcpip_port_secure_sockets.c
  ${AWS_IOT_DEVICE_SDK_PORT_DIR}/source/include/cy_aws_iot_sdk_port_log.h
)
set(AWS_IOT_DEVICE_SDK_PORT_INCLUDE_DIRS
  ${AWS_IOT_DEVICE_SDK_PORT_DIR}/include
  ${AWS_IOT_DEVICE_SDK_PORT_DIR}/source/include
  ${AWS_IOT_DIR}/platform/include
)
set(AWS_IOT_DEVICE_SDK_PORT_LINK_LIBRARIES
  secure-sockets
  aws-iot-http
  aws-iot-json
  aws-iot-mqtt
)

add_library(aws-iot-device-sdk-port STATIC EXCLUDE_FROM_ALL ${AWS_IOT_DEVICE_SDK_PORT_SOURCES})
target_include_directories(aws-iot-device-sdk-port PUBLIC ${AWS_IOT_DEVICE_SDK_PORT_INCLUDE_DIRS})
target_link_libraries(aws-iot-device-sdk-port PUBLIC ${AWS_IOT_DEVICE_SDK_PORT_LINK_LIBRARIES})

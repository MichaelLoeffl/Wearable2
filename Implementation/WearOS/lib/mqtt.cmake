psoc6_load_library(
  NAME mqtt
  VERSION 3.1.1
)

# Avoid conflict with mqttFilePaths.cmake
set(CY_MQTT_SOURCES
  ${MQTT_DIR}/include/core_mqtt_config.h
  ${MQTT_DIR}/include/cy_mqtt_api.h
  ${MQTT_DIR}/source/cy_mqtt_api.c
)
set(MQTT_INCLUDE_DIRS
  ${MQTT_DIR}/include
)
set(MQTT_LINK_LIBRARIES
  aws-iot-device-sdk-port
  network-utilities
)

add_library(mqtt STATIC EXCLUDE_FROM_ALL ${CY_MQTT_SOURCES})
target_include_directories(mqtt PUBLIC ${MQTT_INCLUDE_DIRS})
target_link_libraries(mqtt PUBLIC ${MQTT_LINK_LIBRARIES})

psoc6_load_library(
  NAME ntshell
  URL  https://github.com/iotexpert/middleware-ntshell
  TAG  latest-v2.X
)

set(NTSHELL_SOURCES
  ${NTSHELL_DIR}/core/ntconf.h
  ${NTSHELL_DIR}/core/ntint.h
  ${NTSHELL_DIR}/core/ntlibc.h
  ${NTSHELL_DIR}/core/ntlibc.c
  ${NTSHELL_DIR}/core/ntshell.h
  ${NTSHELL_DIR}/core/ntshell.c
  ${NTSHELL_DIR}/core/text_editor.h
  ${NTSHELL_DIR}/core/text_editor.c
  ${NTSHELL_DIR}/core/text_history.h
  ${NTSHELL_DIR}/core/text_history.c
  ${NTSHELL_DIR}/core/vtrecv.h
  ${NTSHELL_DIR}/core/vtrecv.c
  ${NTSHELL_DIR}/core/vtsend.h
  ${NTSHELL_DIR}/core/vtsend.c
  ${NTSHELL_DIR}/util/ntopt.h
  ${NTSHELL_DIR}/util/ntopt.c
  ${NTSHELL_DIR}/util/ntstdio.h
  ${NTSHELL_DIR}/util/ntstdio.c
)
set(NTSHELL_INCLUDE_DIRS
  ${NTSHELL_DIR}/core
  ${NTSHELL_DIR}/util
)

set(NTSHELL_PSOC6_SOURCES
  ${NTSHELL_DIR}/psoc6sdk/psoc6_ntshell_port.h
  ${NTSHELL_DIR}/psoc6sdk/psoc6_ntshell_port.c
)
set(NTSHELL_PSOC6_INCLUDE_DIRS
  ${NTSHELL_DIR}/psoc6sdk
)
set(NTSHELL_PSOC6_LINK_LIBRARIES
  ntshell
)

add_library(ntshell STATIC EXCLUDE_FROM_ALL ${NTSHELL_SOURCES})
target_include_directories(ntshell PUBLIC ${NTSHELL_INCLUDE_DIRS})

add_library(ntshell-psoc6 INTERFACE EXCLUDE_FROM_ALL)
target_sources(ntshell-psoc6 INTERFACE ${NTSHELL_PSOC6_SOURCES})
target_include_directories(ntshell-psoc6 INTERFACE ${NTSHELL_PSOC6_INCLUDE_DIRS})
target_link_libraries(ntshell-psoc6 INTERFACE ${NTSHELL_PSOC6_LINK_LIBRARIES})

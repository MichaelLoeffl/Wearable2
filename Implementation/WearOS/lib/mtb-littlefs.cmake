psoc6_load_library(
  NAME mtb-littlefs
  VERSION 1.0.0
)

set(MTB_LITTLEFS_SOURCES
  ${MTB_LITTLEFS_DIR}/include/lfs_qspi_memslot.h
  ${MTB_LITTLEFS_DIR}/include/lfs_sd_bd.h
  ${MTB_LITTLEFS_DIR}/include/lfs_spi_flash_bd.h
  ${MTB_LITTLEFS_DIR}/source/lfs_qspi_memslot.c
  ${MTB_LITTLEFS_DIR}/source/lfs_sd_bd.c
  ${MTB_LITTLEFS_DIR}/source/lfs_spi_flash_bd.c
)
set(MTB_LITTLEFS_INCLUDE_DIRS
  ${MTB_LITTLEFS_DIR}/include
)
set(MTB_LITTLEFS_LINK_LIBRARIES
  mtb-hal-cat1
  littlefs
  serial-flash
)

add_library(mtb-littlefs STATIC EXCLUDE_FROM_ALL ${MTB_LITTLEFS_SOURCES})
target_include_directories(mtb-littlefs PUBLIC ${MTB_LITTLEFS_INCLUDE_DIRS})
target_link_libraries(mtb-littlefs PUBLIC ${MTB_LITTLEFS_LINK_LIBRARIES})

#define TEST_DECLARATION
#include "Common/Testing.hpp"
#include "Common/Common.h"

CFUNC void UartFinishBlock();
CFUNC void _putchar(char character);

void TestWrite(const char* text)
{
    static bool prefix = true;

    while (*text != '\0')
    {
        if (prefix)
        {
            UartStartTextBlock("Text");
            prefix = false;
        }

        if (*text == '\n')
        {
            UartFinishBlock();
            prefix = true;
        }
        else
        {
            _putchar(*text);
        }
        text++;
    }
}

#include "Common/Utils.h"
#include "Common/Common.h"
#include "Common/Testing.hpp"
#include <string.h>

TEST_SUITE(UtilsTests)
{
    TEST_CASE(ToStringTests)
    {
        uint8_t buffer[32];
        memset(buffer, '#', sizeof(buffer));
        ToString(0, buffer, sizeof(buffer));
        EXPECT(strcmp("0", (char*)buffer) == 0, "0 was not correctly printed");

        memset(buffer, '#', sizeof(buffer));
        ToString(-12, buffer, sizeof(buffer));
        EXPECT(strcmp("-12", (char*)buffer) == 0, "-12 was not correctly printed");

        memset(buffer, '#', sizeof(buffer));
        ToString(13723, buffer, sizeof(buffer));
        EXPECT(strcmp("13723", (char*)buffer) == 0, "13723 was not correctly printed");

        memset(buffer, '#', sizeof(buffer));
        ToString(-13723, buffer, sizeof(buffer));
        EXPECT(strcmp("-13723", (char*)buffer) == 0, "-13723 was not correctly printed");
    };

    TEARDOWN()
    {

    };
};

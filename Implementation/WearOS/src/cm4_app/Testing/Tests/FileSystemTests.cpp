#include "Common/Common.h"
#include "Common/Testing.hpp"
#include "Drivers/Core.h"
#include "AppSystemM4.hpp"

TEST_SUITE(FileSystem)
{
    TEST_CASE(MyTest)
    {
        uint32_t start = GetTickCount();
        while (GetTickCount() - start < 100)
        {
            appSystem_.DoEvents();
        }

        LOG("Took %d ticks", GetTickCount() - start);

        EXPECT(1 == 1, "Wait, what?");
    };

    TEARDOWN()
    {

    };
};

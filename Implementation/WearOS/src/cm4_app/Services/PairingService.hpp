#ifndef PAIRINGSERVICE_HPP
#define PAIRINGSERVICE_HPP

#include "Common/App.hpp"
#include <string>

class PairingService
{
private:
    bool pairingActive_          = false;
    uint32_t currentPairingCode_ = 0;

    App* pairingApp_ = NULL;

protected:
public:
    PairingService();

    void SetPairingApp(App* app) { pairingApp_ = app; }

    void SetPairingActive(bool active);
    void SetPairingCode(uint32_t code);
    uint32_t GetPairingCode();
    bool GetPairingActive();
};

#endif /* PAIRINGSERVICE_HPP */

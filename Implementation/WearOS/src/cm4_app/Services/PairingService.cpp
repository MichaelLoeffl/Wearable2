#include "PairingService.hpp"
#include "AppSystemM4.hpp"
#include <cstdlib>

extern "C"
{
    void ShowPairingCode(uint32_t code);
    void PairingComplete();
}

PairingService::PairingService()
{
}

void ShowPairingCode(uint32_t code)
{
    appSystem_.GetPairingService()->SetPairingCode(code);
    appSystem_.GetPairingService()->SetPairingActive(true);
}

void PairingComplete()
{
    appSystem_.GetPairingService()->SetPairingCode(0);
}

void PairingService::SetPairingActive(bool active)
{
    pairingActive_ = active;

    if (active)
    {
        appSystem_.StartApp(*pairingApp_);
    }
    else
    {
        appSystem_.CloseApp(*pairingApp_);
    }
}

void PairingService::SetPairingCode(uint32_t code)
{
    currentPairingCode_ = code;
}

uint32_t PairingService::GetPairingCode()
{
    if (!pairingActive_)
    {
        return 0;
    }

    return currentPairingCode_;
}

bool PairingService::GetPairingActive()
{
    return pairingActive_;
}

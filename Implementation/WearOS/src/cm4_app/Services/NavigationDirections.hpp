#ifndef NAVIGATIONDIRECTIONS_HPP
#define NAVIGATIONDIRECTIONS_HPP

enum NavigationDirections
{
    None = 0,
    Straight = 1,
    Start = 2,
    Finish = 3,
    SlightLeft = 4,
    Left = 5,
    SharpLeft = 6,
    SharpRight = 7,
    Right = 8,
    SlightRight = 9,
    ForkRight = 10,
    ForkLeft = 11,
    UTurn = 12,
    Reserved13 = 13,
    Reserved14 = 14,
    RoundaboutExitLeft = 15,
    RoundaboutExitRight = 16,
    RoundaboutCounterClockwiseExit1of1 = 17,
    RoundaboutCounterClockwiseExit1of2 = 18,
    RoundaboutCounterClockwiseExit1of3 = 19,
    RoundaboutCounterClockwiseExit2of2 = 20,
    RoundaboutCounterClockwiseExit2of3 = 21,
    RoundaboutCounterClockwiseExit3of3 = 22,
    RoundaboutClockwiseExit1of1 = 23,
    RoundaboutClockwiseExit1of2 = 24,
    RoundaboutClockwiseExit1of3 = 25,
    RoundaboutClockwiseExit2of2 = 26,
    RoundaboutClockwiseExit2of3 = 27,
    RoundaboutClockwiseExit3of3 = 28,
    RoundaboutFallback = 29,
    OutOfRoute = 30
};

#endif /* NAVIGATIONDIRECTIONS_HPP */

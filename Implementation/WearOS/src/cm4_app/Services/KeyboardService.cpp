#include "KeyboardService.hpp"

CFUNC void HidsSimulateKeyboard(uint8_t key);

CFUNC void SendVolume(bool up);

void HidKeyPress(uint8_t key)
{
    HidsSimulateKeyboard(key);
}

void SendVolumeUp()
{
    SendVolume(true);
}

void SendVolumeDown()
{
    SendVolume(false);
}

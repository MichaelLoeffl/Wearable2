#include "TimeService.hpp"
#include "Drivers/RtcDriver.h"
#include "Settings.hpp"
#include "Settings/SettingsDefines.hpp"
#include <cstdlib>

class RemoteOffsetSetting : BaseSetting2<int32_t, char[32]>
{
public:
    RemoteOffsetSetting() : BaseSetting2(SETTINGS_REMOTE_OFFSET) {}

    void SetOffset(int32_t value) { Value0 = value; }

    int32_t getOffset() { return Value0; };
};

RemoteOffsetSetting offsetSetting;
class LocalOffsetSetting : BaseSetting2<int32_t, char[32]>
{
public:
    LocalOffsetSetting() : BaseSetting2(SETTINGS_LOCAL_OFFSET) {}

    void SetOffset(int32_t value) { Value0 = value; }

    int32_t getOffset() { return -Value0; };
};

LocalOffsetSetting localOffsetSetting;

TimeService::TimeService()
{
}

Timespan TimeService::GetRemoteOffset()
{
    remoteOffset_ = Timespan(offsetSetting.getOffset(), 0, 0);

    return remoteOffset_;
}

void TimeService::SetRemoteOffset(const Timespan& value)
{
    remoteOffset_ = value;

    offsetSetting.SetOffset(value.TotalMinutes());
}

DateTime TimeService::GetLocalTime()
{
    return DateTime::Now() + Timespan(localOffsetSetting.getOffset(), 0, 0);
}

DateTime TimeService::GetRemoteTime()
{
    DateTime time = DateTime::Now();
    return time + remoteOffset_;
}

const char* TimeService::GetDayOfWeekText(uint16_t dow)
{
    // RTC runs on Zeller's congruence (https://en.wikipedia.org/wiki/Zeller%27s_congruence)
    switch (dow)
    {
        case 1:
            return "MON";
        case 2:
            return "TUE";
        case 3:
            return "WED";
        case 4:
            return "THU";
        case 5:
            return "FRI";
        case 6:
            return "SAT";
        case 7:
            return "SUN";
        default:
            return "???";
    }
}

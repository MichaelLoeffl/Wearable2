#include "FileReaderSetting.hpp"
#include "DeviceSettings.hpp"
#include "Drivers/FileSystem.hpp"
#include "Common/Utils.h"
#include <stdio.h>
#include <string.h>

FileReaderSetting fileReaderSetting_;

static char fileNameBuffer[FILE_SETTINGS_PAYLOAD_SIZE];
static uint8_t tempBuffer[FILE_SETTINGS_PAYLOAD_SIZE];

void FileReaderSetting::Written(bool hadErrors)
{
    // Do not call the parent because we want to prevent the persisting of the setting
    if (hadErrors)
    {
        return;
    }

    uint32_t filePos = Value0;
    int16_t length   = Value1;

    StringCopySafe(Value2, fileNameBuffer, FILE_SETTINGS_PAYLOAD_SIZE);

    if (length > (int16_t)(sizeof(Value2) - 1) / 2)
    {
        length = (int16_t)(sizeof(Value2) - 1) / 2;
    }

    File f(fileNameBuffer, FileAccessFlags::Read);

    LOG("Opening file...");
    if (f.IsOpen())
    {
        if (f.SeekAbsolute(filePos) >= 0)
        {
            Value1 = f.Read(tempBuffer, length);

            if (Value1 >= length / 2)
            {
                Value1 = length / 2;
            }
            for (int16_t pos = 0; pos < Value1; pos++)
            {
                sprintf(&Value2[pos * 2], "%02x", tempBuffer[pos]);
            }
            Value2[Value1 * 2] = '\0';
        }
    }
    else
    {
        LOG("File not available...");
    }

    if (Value1 == 0)
    {
        Value1 = -1;
    }

    LOG("Read: %s", Value2);
    Settings::UpdateReadProperty("file", fileNameBuffer);
}

#ifndef SETTINGSSTORAGE_HPP
#define SETTINGSSTORAGE_HPP

#include "BaseSetting.hpp"
#include "SettingsDefines.hpp"

class HandSideSetting : public BaseSetting1<int32_t>
{
public:
    HandSideSetting() : BaseSetting1<int32_t>(SETTINGS_HAND_SIDE){};
};

class DnDSetting : public BaseSetting3<bool, int32_t, int32_t>
{
public:
    DnDSetting() : BaseSetting3<bool, int32_t, int32_t>(SETTINGS_DND){};
};

class LanguageSetting : public BaseSetting1<char[12]>
{
public:
    LanguageSetting() : BaseSetting1<char[12]>(SETTINGS_LANGUAGE){};
};

class MetricUnitSetting : public BaseSetting1<bool>
{
public:
    MetricUnitSetting() : BaseSetting1<bool>(SETTINGS_METRICUNITS){};
};

class HourFormatSetting : public BaseSetting1<bool>
{
public:
    HourFormatSetting() : BaseSetting1<bool>(SETTINGS_24HOURS){};
};

class SettingsStorage
{
private:
public:
    HandSideSetting HandSide;
    DnDSetting DoNotDisturb;
    LanguageSetting Language;
    MetricUnitSetting MetricUnit;
    HourFormatSetting HourFormat;
};

extern SettingsStorage settingsStorage_;

#endif /* SETTINGSSTORAGE_HPP */

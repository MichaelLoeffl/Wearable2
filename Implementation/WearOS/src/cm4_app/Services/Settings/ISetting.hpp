#ifndef ISETTING_HPP
#define ISETTING_HPP

#include <Common/Common.h>
#include <Common/ccronexpr.h>
#include "Common/DateTime.hpp"
#include <type_traits>

enum class SettingsValueDataType
{
    Unknown,
    Boolean,
    Integer8,
    UnsignedInteger8,
    Integer16,
    UnsignedInteger16,
    Integer32,
    UnsignedInteger32,
    Integer64,
    UnsignedInteger64,
    Float,
    String,
    DateTime,
    CronExpression,
};

CFUNC void MainM4();

class ISetting
{
    friend void MainM4();

private:
    const char* settingID_;
    ISetting* next_ = nullptr;

    static void Initialize();

protected:
    void Register(const char* settingsID);

public:
    virtual uint8_t GetValueCount()                           = 0;
    virtual void* GetValuePtr(uint8_t index)                  = 0;
    virtual int GetValueMemorySize(uint8_t index)             = 0;
    virtual SettingsValueDataType GetValueType(uint8_t index) = 0;

    virtual void Written(bool hadErrors);

    const char* GetSettingID() { return settingID_; }

    virtual void Load();
    virtual void Save();

    virtual ~ISetting() {};

    static ISetting* GetSetting(const char* settingID, uint16_t strLength);
    static ISetting* GetSetting(const char* settingID);
};

#endif /* ISETTING_HPP */

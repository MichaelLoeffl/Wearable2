#ifndef BASESETTING_HPP
#define BASESETTING_HPP

#include "ISetting.hpp"

template<typename T> SettingsValueDataType GetDataTypeIdentifier(T& datatype)
{
    if (std::is_same<T, int8_t>::value)
    {
        return SettingsValueDataType::Integer8;
    }
    else if (std::is_same<T, uint8_t>::value)
    {
        return SettingsValueDataType::UnsignedInteger8;
    }

    if (std::is_same<T, int16_t>::value)
    {
        return SettingsValueDataType::Integer16;
    }
    else if (std::is_same<T, uint16_t>::value)
    {
        return SettingsValueDataType::UnsignedInteger16;
    }

    if (std::is_same<T, int32_t>::value)
    {
        return SettingsValueDataType::Integer32;
    }
    else if (std::is_same<T, uint32_t>::value)
    {
        return SettingsValueDataType::UnsignedInteger32;
    }

    if (std::is_same<T, int64_t>::value)
    {
        return SettingsValueDataType::Integer64;
    }
    else if (std::is_same<T, uint64_t>::value)
    {
        return SettingsValueDataType::UnsignedInteger64;
    }

    if (std::is_same<T, float>::value)
    {
        return SettingsValueDataType::Float;
    }

    if (std::is_same<T, bool>::value)
    {
        return SettingsValueDataType::Boolean;
    }

    if (std::is_same<T, char*>::value)
    {
        return SettingsValueDataType::String;
    }

    if (std::is_same<T, uint8_t*>::value)
    {
        return SettingsValueDataType::String;
    }

    if (std::is_same<T, DateTime>::value)
    {
        return SettingsValueDataType::DateTime;
    }

    if (std::is_same<T, cron_expr>::value)
    {
        return SettingsValueDataType::CronExpression;
    }

    if (std::is_array<T>::value)
    {
        return SettingsValueDataType::String;
    }

    return SettingsValueDataType::Unknown;
}

template<typename T> class BaseSetting1 : public ISetting
{
private:
public:
    BaseSetting1(const char* name) { Register(name); }
    virtual ~BaseSetting1() = default;

    T Value;

    uint8_t GetValueCount() override { return 1; }

    void* GetValuePtr(uint8_t index)
    {
        if (index > 0)
        {
            return nullptr;
        }
        return (void*)&Value;
    }

    SettingsValueDataType GetValueType(uint8_t index) override
    {
        if (index > 0)
        {
            return SettingsValueDataType::Unknown;
        }

        return GetDataTypeIdentifier(Value);
    }

    int GetValueMemorySize(uint8_t index) override
    {
        if (index > 0)
        {
            return 0;
        }

        return sizeof(Value);
    }
};

template<typename T1, typename T2> class BaseSetting2 : public ISetting
{
private:
public:
    BaseSetting2(const char* name) { Register(name); }
    virtual ~BaseSetting2() = default;

    T1 Value0;
    T2 Value1;

    uint8_t GetValueCount() override { return 2; }

    void* GetValuePtr(uint8_t index)
    {
        switch (index)
        {
            case 0:
                return &Value0;
            case 1:
                return &Value1;
            default:
                return nullptr;
        }
    }

    SettingsValueDataType GetValueType(uint8_t index) override
    {
        switch (index)
        {
            case 0:
                return GetDataTypeIdentifier(Value0);
            case 1:
                return GetDataTypeIdentifier(Value1);
            default:
                return SettingsValueDataType::Unknown;
        }
    }

    int GetValueMemorySize(uint8_t index) override
    {
        switch (index)
        {
            case 0:
                return sizeof(Value0);
            case 1:
                return sizeof(Value1);
            default:
                return 0;
        }
    }
};

template<typename T1, typename T2, typename T3> class BaseSetting3 : public ISetting
{
private:
public:
    BaseSetting3(const char* name) { Register(name); }
    virtual ~BaseSetting3() = default;

    T1 Value0;
    T2 Value1;
    T3 Value2;

    uint8_t GetValueCount() override { return 3; }

    void* GetValuePtr(uint8_t index)
    {
        switch (index)
        {
            case 0:
                return &Value0;
            case 1:
                return &Value1;
            case 2:
                return &Value2;
            default:
                return nullptr;
        }
    }

    SettingsValueDataType GetValueType(uint8_t index) override
    {
        switch (index)
        {
            case 0:
                return GetDataTypeIdentifier(Value0);
            case 1:
                return GetDataTypeIdentifier(Value1);
            case 2:
                return GetDataTypeIdentifier(Value2);
            default:
                return SettingsValueDataType::Unknown;
        }
    }

    int GetValueMemorySize(uint8_t index) override
    {
        switch (index)
        {
            case 0:
                return sizeof(Value0);
            case 1:
                return sizeof(Value1);
            case 2:
                return sizeof(Value2);
            default:
                return 0;
        }
    }
};

#endif /* BASESETTING_HPP */

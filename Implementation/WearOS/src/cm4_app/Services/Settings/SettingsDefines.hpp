#ifndef SETTINGSDEFINES_HPP
#define SETTINGSDEFINES_HPP

#define SETTINGS_FILE "file"
#define SETTINGS_HAND_SIDE "handSide"
#define SETTINGS_DND "dnd"
#define SETTINGS_24HOURS "hours24"
#define SETTINGS_METRICUNITS "metric"
#define SETTINGS_LANGUAGE "language"
#define SETTINGS_ALTITUDE "altitude"
#define SETTINGS_PAYMENT_BALANCE "balance"
#define SETTINGS_RECENT_WORKOUT "recentWorkout"
#define SETTINGS_REMOTE_OFFSET "remoteOffset"
#define SETTINGS_LOCAL_OFFSET "priTime"
#define SETTINGS_USER_WEIGHT "userWeight"
#define SETTINGS_USER_AGE "userAge"
#define SETTINGS_MAX_HEARTRATE "userMaxHeartrate"
#define SETTINGS_FITNESS_LEVEL "userFitnessLevel"

#define FILENAME_WORKOUT_SUMMARY "workoutSummary"

#endif /* SETTINGSDEFINES_HPP */

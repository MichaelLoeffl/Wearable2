#ifndef DEVICESETTINGS_HPP
#define DEVICESETTINGS_HPP

#include <Common/Common.h>

class Settings
{
private:
public:
    static bool LoadSetting(const char* setting, void* buffer, int size);
    static bool SaveSetting(const char* setting, void* buffer, int size);
    template<typename T> static bool Load(const char* name, T& buffer) { return LoadSetting(name, &buffer, sizeof(T)); }
    template<typename T> static bool Save(const char* name, T& buffer) { return SaveSetting(name, &buffer, sizeof(T)); }

    static void UpdateReadProperty(const char* settingsId, const char* name = nullptr);
};

#endif /* DEVICESETTINGS_HPP */

#include "JsonBufferHelpers.hpp"
#include "Common/DateTime.hpp"
#include "Services/Settings.hpp"
#include <cstring>
#include <string>

uint16_t PrintText(uint8_t* buffer, uint16_t size, const char* text)
{
    for (int i = 0; i < size; i++)
    {
        if (text[i] == '\0')
        {
            return i;
        }
        buffer[i] = text[i];
    }

    if (text[size] != '\0')
    {
        return 0;
    }

    return size;
}

uint16_t PrintBoolean(uint8_t* buffer, uint16_t size, bool value)
{
    return PrintText(buffer, size, value ? "true" : "false");
}

uint16_t PrintBuffer(uint8_t* buffer, uint16_t size, const uint8_t* sourceBuffer, uint8_t length)
{
    if (length > size)
    {
        return 0;
    }

    memcpy(buffer, sourceBuffer, length);

    return length;
}

uint16_t PrintBufferZT(uint8_t* buffer, uint16_t size, const uint8_t* sourceBuffer)
{
    uint16_t length = strlen((const char*)sourceBuffer);
    if (length > size)
    {
        return 0;
    }

    memcpy(buffer, sourceBuffer, length);

    return length;
}

uint16_t PrintString(uint8_t* buffer, uint16_t size, const char* string)
{
    uint16_t length = strlen(string);
    if (length + 2 > size)
    {
        return 0;
    }

    memcpy(&buffer[1], string, length);
    buffer[0]          = '"';
    buffer[length + 1] = '"';

    return length + 2;
}

uint16_t PrintDateTime(uint8_t* buffer, uint16_t size, const DateTime* time)
{
    if (size <= 22)
    {
        return 0;
    }
    int result = sprintf((char*)buffer,
                         "\"%d-%d-%dT%d:%d:%dZ\"",
                         time->GetYear() % 9999,
                         time->GetMonth(),
                         time->GetDay(),
                         time->GetHour(),
                         time->GetMinute(),
                         time->GetSecond());
    if (result <= 0)
    {
        return 0;
    }
    return result;
}

uint16_t PrintCronExpression(uint8_t* buffer, uint16_t size, const cron_expr* time)
{
    return PrintString(buffer, size, "cron");
}

uint16_t PrintValue(uint8_t* buffer, uint16_t size, SettingsValueDataType valueType, void* valuePointer)
{
    char tempBuffer[32];
    memset(tempBuffer, '0', 32);

    switch (valueType)
    {
        case SettingsValueDataType::Boolean:
            return PrintBoolean(buffer, size, *(bool*)valuePointer);

        case SettingsValueDataType::String:
            return PrintString(buffer, size, (const char*)valuePointer);

        case SettingsValueDataType::DateTime:
            return PrintDateTime(buffer, size, (DateTime*)valuePointer);

        case SettingsValueDataType::Integer8:
            sprintf(tempBuffer, "%u", *(int8_t*)valuePointer);
            break;
        case SettingsValueDataType::Integer16:
            sprintf(tempBuffer, "%u", *(int16_t*)valuePointer);
            break;
        case SettingsValueDataType::Integer32:
            sprintf(tempBuffer, "%lu", *(int32_t*)valuePointer);
            break;
        case SettingsValueDataType::Integer64:
            sprintf(tempBuffer, "%llu", *(int64_t*)valuePointer);
            break;
        case SettingsValueDataType::UnsignedInteger8:
            sprintf(tempBuffer, "%i", *(uint8_t*)valuePointer);
            break;
        case SettingsValueDataType::UnsignedInteger16:
            sprintf(tempBuffer, "%d", *(uint16_t*)valuePointer);
            break;
        case SettingsValueDataType::UnsignedInteger32:
            sprintf(tempBuffer, "%ld", *(uint32_t*)valuePointer);
            break;
        case SettingsValueDataType::UnsignedInteger64:
            sprintf(tempBuffer, "%lld", *(uint64_t*)valuePointer);
            break;

        case SettingsValueDataType::CronExpression:
            return PrintCronExpression(buffer, size, (cron_expr*)valuePointer);
        default:
            return 0;
    }

    auto len = strlen(tempBuffer);
    if (len <= size)
    {
        memcpy(buffer, tempBuffer, len);
        return len;
    }
    return 0;
}

uint16_t WriteValue(uint8_t* buffer, uint16_t size, ISetting* setting)
{
    if (setting->GetValueCount() == 1)
    {
        return PrintValue(buffer, size, setting->GetValueType(0), setting->GetValuePtr(0));
    }
    else
    {
        if (size <= 3)
        {
            return 0;
        }
        uint16_t used  = 0;
        buffer[used++] = '[';

        for (uint8_t i = 0; i < setting->GetValueCount(); i++)
        {
            if (i != 0)
            {
                if (used >= size)
                {
                    return 0;
                }
                buffer[used++] = ',';
            }

            uint16_t written = PrintValue(&buffer[used], size - 1 - used, setting->GetValueType(i), setting->GetValuePtr(i));
            if (written == 0)
            {
                return 0;
            }
            used += written;
        }

        if (used >= size)
        {
            return 0;
        }
        buffer[used++] = ']';

        return used;
    }
}
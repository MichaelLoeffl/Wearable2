#ifndef JSONBUFFERHELPERS_HPP
#define JSONBUFFERHELPERS_HPP

#include "Common/Common.h"

class ISetting;

uint16_t WriteValue(uint8_t* buffer, uint16_t size, ISetting* setting);

#endif /* JSONBUFFERHELPERS_HPP */

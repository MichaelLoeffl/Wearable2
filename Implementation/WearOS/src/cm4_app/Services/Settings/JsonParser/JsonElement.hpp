#ifndef JSONELEMENT_HPP
#define JSONELEMENT_HPP

#include "Common/Common.h"
#include "Common/ccronexpr.h"

enum class JsonElementType
{
    Object,
    Array,
    String,
    Number,
    Boolean,
    Null,
    None
};

#define JSON_INVALID_POSITION (0xFFFF)

class JsonElement
{
    friend class JsonParser;

private:
    const uint8_t* buffer_ = nullptr;
    uint16_t start_        = 0;
    uint16_t position_     = 0;
    uint16_t end_          = JSON_INVALID_POSITION;
    uint16_t bufferLength_ = 0;
    bool isFloat_          = false;
    bool isExponent_       = false;
    const uint8_t* name_   = (const uint8_t*)"";
    uint8_t nameLength_    = 0;

    bool invalid_ = false;

    JsonElementType elementType_ = JsonElementType::None;

    void Prepare();
    JsonElement(const uint8_t* buffer, uint16_t position, uint16_t length);
    JsonElement(const uint8_t* buffer, uint16_t position, uint16_t length, const uint8_t* name, uint16_t memberNameLength);

    JsonElement(const char* errorMessage);

    bool Consume(char expected);
    bool Consume(char expectedA, char expectedB);

    bool ConsumeDigit1_9();
    bool ConsumeDigit();
    bool ConsumeExponent();
    bool ConsumeFraction();
    bool ConsumeInteger();
    bool ConsumeNumber();
    bool ConsumeBoolean();
    bool ConsumeNull();

public:
    JsonElementType GetElementType() { return elementType_; };
    JsonElement GetFirstMember();
    JsonElement GetNextMember();
    JsonElement GetNextSibling();

    const uint8_t* GetName() { return name_; };
    uint16_t GetNameLength() { return nameLength_; };

    const uint8_t* GetValue() { return &buffer_[start_]; };
    uint16_t GetLength();
    const uint8_t* GetBody();
    uint16_t GetBodyLength();

    uint16_t GetEnd()
    {
        auto length = GetLength();
        if (length == 0)
        {
            return JSON_INVALID_POSITION;
        }
        return start_ + length;
    }

    JsonElement(const uint8_t* buffer, uint16_t length);

    bool TryParse(uint8_t& out);
    bool TryParse(uint16_t& out);
    bool TryParse(uint32_t& out);
    bool TryParse(uint64_t& out);
    bool TryParse(int8_t& out);
    bool TryParse(int16_t& out);
    bool TryParse(int32_t& out);
    bool TryParse(int64_t& out);
    bool TryParse(bool& out);
    bool TryParse(cron_expr& out);
};

#endif /* JSONELEMENT_HPP */

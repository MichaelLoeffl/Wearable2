#include "JsonElement.hpp"
#include <cstring>

bool IsWhitespace(uint8_t character)
{
    switch (character)
    {
        case ' ':
        case '\r':
        case '\n':
        case '\t':
            return true;
        default:
            return false;
    }
}

bool JsonElement::TryParse(cron_expr& out)
{
    if (this->GetElementType() != JsonElementType::String)
    {
        return false;
    }

    char buffer[32];
    uint16_t length = this->GetBodyLength();
    if (length >= sizeof(buffer))
    {
        length = sizeof(buffer) - 1;
    }

    memcpy(buffer, (const char*)this->GetBody(), length);
    buffer[length] = '\0';

    cron_expr expr;
    memset(&expr, 0, sizeof(expr));

    const char* err = NULL;
    cron_parse_expr(buffer, &expr, &err);

    if (err)
    {
        return false;
    }
    out = expr;
    return true;
}

bool JsonElement::TryParse(bool& out)
{
    if (this->GetElementType() != JsonElementType::Boolean)
    {
        return false;
    }

    if (this->GetBody()[0] == 't')
    {
        out = true;
    }
    else
    {
        out = false;
    }
    return true;
}

bool JsonElement::TryParse(uint8_t& out)
{
    uint64_t t;
    if (TryParse(t) && t <= 0xFF)
    {
        out = (uint8_t)t;
        return true;
    }
    return false;
}

bool JsonElement::TryParse(uint16_t& out)
{
    uint64_t t;
    if (TryParse(t) && t <= 0xFFFF)
    {
        out = (uint16_t)t;
        return true;
    }
    return false;
}

bool JsonElement::TryParse(uint32_t& out)
{
    uint64_t t;
    if (TryParse(t) && t <= 0xFFFFFFFF)
    {
        out = (uint32_t)t;
        return true;
    }
    return false;
}

bool JsonElement::TryParse(uint64_t& out)
{
    if (this->GetElementType() != JsonElementType::Number)
    {
        return false;
    }

    uint64_t temp = 0;

    for (int i = 0; i < this->GetBodyLength(); i++)
    {
        switch (uint8_t b = this->GetBody()[i])
        {
            case '+':
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            {
                temp *= 10;
                temp += b - '0';
                break;
            }
            default:
            {
                return false;
            }
        }
    }
    out = temp;
    return true;
}

bool JsonElement::TryParse(int8_t& out)
{
    int64_t t;
    if (TryParse(t) && t <= 127 && t >= -128)
    {
        out = (int8_t)t;
        return true;
    }
    return false;
}

bool JsonElement::TryParse(int16_t& out)
{
    int64_t t;
    if (TryParse(t) && t <= 32767 && t >= -32768)
    {
        out = (int16_t)t;
        return true;
    }
    return false;
}

bool JsonElement::TryParse(int32_t& out)
{
    int64_t t;
    if (TryParse(t) && t <= 2147483647 && t >= -2147483648)
    {
        out = (int32_t)t;
        return true;
    }
    return false;
}

bool JsonElement::TryParse(int64_t& out)
{
    if (this->GetElementType() != JsonElementType::Number)
    {
        return false;
    }

    int64_t temp = 0;
    bool neg     = false;

    for (int i = 0; i < this->GetBodyLength(); i++)
    {
        switch (uint8_t b = this->GetBody()[i])
        {
            case '+':
                break;
            case '-':
                neg = true;
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            {
                temp *= 10;
                temp += b - '0';
                if (neg != (temp < 0))
                {
                    temp = -temp;
                }

                break;
            }
            default:
            {
                return false;
            }
        }
    }

    out = temp;
    return true;
}

void ConsumeWhitespaces(const uint8_t* buffer, uint16_t& position, uint16_t bufferLength)
{
    if (position >= bufferLength || position == JSON_INVALID_POSITION)
    {
        position = JSON_INVALID_POSITION;
        return;
    }

    uint16_t p = position;

    while (IsWhitespace(buffer[p]))
    {
        if (++p >= bufferLength)
        {
            position = JSON_INVALID_POSITION;
            return;
        }
    }
    position = p;
}

JsonElement::JsonElement(const uint8_t* buffer, uint16_t bufferLength)
{
    start_        = 0;
    bufferLength_ = bufferLength;
    buffer_       = buffer;
    invalid_      = false;
    name_         = (const uint8_t*)"";

    Prepare();
}

JsonElement::JsonElement(const char* errorMessage)
{
    start_        = JSON_INVALID_POSITION;
    bufferLength_ = 0;
    buffer_       = nullptr;
    position_     = JSON_INVALID_POSITION;
    invalid_      = true;
    name_         = (const uint8_t*)errorMessage;
    nameLength_   = strlen((const char*)name_);
}

JsonElement::JsonElement(const uint8_t* buffer, uint16_t position, uint16_t bufferLength, const uint8_t* name, uint16_t nameLength)
{
    start_        = position;
    bufferLength_ = bufferLength;
    buffer_       = buffer;
    position_     = JSON_INVALID_POSITION;
    invalid_      = false;
    name_         = (const uint8_t*)"";

    Prepare();

    name_       = name;
    nameLength_ = nameLength;
}

const uint8_t* JsonElement::GetBody()
{
    switch (elementType_)
    {
        case JsonElementType::Array:
        case JsonElementType::Object:
        case JsonElementType::String:
            return &GetValue()[1];
        default:
        {
            return GetValue();
        }
    }
}

uint16_t JsonElement::GetBodyLength()
{
    uint16_t len = GetLength();
    switch (elementType_)
    {
        case JsonElementType::Array:
        case JsonElementType::Object:
        case JsonElementType::String:
            if (len >= 2)
            {
                return len - 2;
            }
            return 0;
        default:
        {
            return len;
        }
    }
}

JsonElement::JsonElement(const uint8_t* buffer, uint16_t position, uint16_t bufferLength)
{
    start_        = position;
    bufferLength_ = bufferLength;
    buffer_       = buffer;
    position_     = JSON_INVALID_POSITION;
    invalid_      = false;
    name_         = (const uint8_t*)"";

    Prepare();
}

void JsonElement::Prepare()
{
    name_       = (const uint8_t*)"";
    nameLength_ = 0;
    position_   = JSON_INVALID_POSITION;
    end_        = JSON_INVALID_POSITION;

    if (start_ >= bufferLength_ || buffer_ == nullptr)
    {
        invalid_     = true;
        elementType_ = JsonElementType::None;
        name_        = (const uint8_t*)"Invalid Json";
        return;
    }

    while (IsWhitespace(buffer_[start_]))
    {
        start_++;
        if (start_ >= bufferLength_)
        {
            invalid_     = true;
            elementType_ = JsonElementType::None;
            return;
        }
    }

    switch (buffer_[start_])
    {
        case '{':
        {
            elementType_ = JsonElementType::Object;
            position_    = start_;
            break;
        }
        case '[':
        {
            elementType_ = JsonElementType::Array;
            position_    = start_;
            break;
        }
        case 't':
        {
            elementType_ = JsonElementType::Boolean;
            break;
        }
        case 'f':
        {
            elementType_ = JsonElementType::Boolean;
            break;
        }
        case 'n':
        {
            elementType_ = JsonElementType::Null;
            break;
        }
        case '"':
        {
            elementType_ = JsonElementType::String;
            break;
        }
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '-':
        {
            elementType_ = JsonElementType::Number;
            break;
        }
        default:
        {
            invalid_     = true;
            elementType_ = JsonElementType::None;
            break;
        }
    }
}

bool JsonElement::ConsumeDigit()
{
    if (position_ >= bufferLength_)
    {
        return false;
    }
    switch (buffer_[position_])
    {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            position_++;
            return true;
    }
    return false;
}

bool JsonElement::ConsumeDigit1_9()
{
    if (position_ >= bufferLength_)
    {
        return false;
    }
    switch (buffer_[position_])
    {
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            position_++;
            return true;
    }
    return false;
}

bool JsonElement::Consume(char expected)
{
    if (position_ >= bufferLength_)
    {
        return false;
    }
    if (buffer_[position_] == expected)
    {
        position_++;
        return true;
    }
    return false;
}

bool JsonElement::Consume(char expectedA, char expectedB)
{
    if (position_ >= bufferLength_)
    {
        return false;
    }
    if (buffer_[position_] == expectedA || buffer_[position_] == expectedB)
    {
        position_++;
        return true;
    }
    return false;
}

bool JsonElement::ConsumeExponent()
{
    uint16_t oldPos = position_;
    if (Consume('E', 'e'))
    {
        if (Consume('+', '-'))
        {
            if (ConsumeDigit())
            {
                while (ConsumeDigit())
                    ;
                return true;
            }
        }
    }

    position_ = oldPos;
    return false;
}

bool JsonElement::ConsumeFraction()
{
    uint16_t oldPos = position_;
    if (Consume('.'))
    {
        if (ConsumeDigit())
        {
            while (ConsumeDigit())
                ;
            return true;
        }
    }

    position_ = oldPos;
    return false;
}

bool JsonElement::ConsumeInteger()
{
    uint16_t oldPos = position_;
    Consume('-');
    if (!Consume('0'))
    {
        if (!ConsumeDigit1_9())
        {
            position_ = oldPos;
            return false;
        }
        else
        {
            while (ConsumeDigit())
                ;
        }
    }
    return true;
}

bool JsonElement::ConsumeNumber()
{
    uint16_t oldPos = position_;
    if (ConsumeInteger())
    {
        if (ConsumeFraction())
        {
            isFloat_ = true;
        }
        if (ConsumeExponent())
        {
            isExponent_ = true;
        }
        return true;
    }
    position_ = oldPos;
    return false;
}

bool JsonElement::ConsumeBoolean()
{
    bool correct    = true;
    uint16_t oldPos = position_;

    if (Consume('t'))
    {
        correct &= Consume('r');
        correct &= Consume('u');
        correct &= Consume('e');
    }
    else if (Consume('f'))
    {
        correct &= Consume('a');
        correct &= Consume('l');
        correct &= Consume('s');
        correct &= Consume('e');
    }
    else
    {
        correct = false;
    }

    if (!correct)
    {
        position_ = oldPos;
    }
    return correct;
}

bool JsonElement::ConsumeNull()
{
    bool correct    = true;
    uint16_t oldPos = position_;

    if (Consume('n'))
    {
        correct &= Consume('u');
        correct &= Consume('l');
        correct &= Consume('l');
    }
    else
    {
        correct = false;
    }

    if (!correct)
    {
        position_ = oldPos;
    }
    return correct;
}

uint16_t JsonElement::GetLength()
{
    if (invalid_)
    {
        return JSON_INVALID_POSITION;
    }

    if (end_ != JSON_INVALID_POSITION && end_ > start_)
    {
        return end_ - start_;
    }

    uint16_t oldPosition = position_;
    switch (elementType_)
    {
        case JsonElementType::Array:
        case JsonElementType::Object:
        {
            while (GetNextMember().GetElementType() != JsonElementType::None)
            {
                // Loop through all members
            }
            end_ = position_ + 1;
            break;
        }
        case JsonElementType::String:
        {
            bool correct = false;
            bool escape  = false;
            end_         = start_;
            while (++end_ < bufferLength_)
            {
                if (!escape)
                {
                    if (buffer_[end_] == '"')
                    {
                        correct = true;
                        end_++;
                        break;
                    }
                    else if (buffer_[end_] == '\\')
                    {
                        escape = true;
                    }
                }
                else
                {
                    escape = false;
                }
            }
            if (!correct)
            {
                end_     = JSON_INVALID_POSITION;
                invalid_ = true;
            }
            break;
        }
        case JsonElementType::Number:
        {
            position_ = start_;
            if (!ConsumeNumber())
            {
                invalid_ = true;
            }
            end_ = position_;
            break;
        }
        case JsonElementType::Boolean:
        {
            position_ = start_;
            if (!ConsumeBoolean())
            {
                invalid_ = true;
            }
            end_ = position_;
            break;
        }
        case JsonElementType::Null:
        {
            position_ = start_;
            if (!ConsumeNull())
            {
                invalid_ = true;
            }
            end_ = position_;
            break;
        }
        case JsonElementType::None:
        {
            end_ = JSON_INVALID_POSITION;
            break;
        }
    }

    position_ = oldPosition;

    if (end_ == JSON_INVALID_POSITION)
    {
        return JSON_INVALID_POSITION;
    }

    if (end_ >= bufferLength_)
    {
        end_ = bufferLength_;
    }

    return end_ - start_;
}

JsonElement JsonElement::GetFirstMember()
{
    if (elementType_ == JsonElementType::Array || elementType_ == JsonElementType::Object)
    {
        position_ = start_;
        return GetNextMember();
    }
    return JsonElement("Operation invalid");
}

JsonElement JsonElement::GetNextMember()
{
    if (invalid_ || position_ == JSON_INVALID_POSITION)
    {
        return JsonElement("Operation invalid");
    }

    if (elementType_ == JsonElementType::Object)
    {
        ConsumeWhitespaces(buffer_, position_, bufferLength_);
        if (position_ == JSON_INVALID_POSITION)
        {
            return JsonElement("Invalid Json");
        }

        switch (buffer_[position_])
        {
            case '{':
            {
                if (position_ == start_)
                {
                    position_++;
                    return GetNextMember();
                }
                return JsonElement("Invalid Json");
            }
            case '"':
            {
                JsonElement memberName = JsonElement(buffer_, position_, bufferLength_);

                auto startOfMemberValue = memberName.GetEnd();
                ConsumeWhitespaces(buffer_, startOfMemberValue, bufferLength_);
                if (startOfMemberValue == JSON_INVALID_POSITION)
                {
                    return JsonElement("Unexpected end of json");
                }
                if (buffer_[startOfMemberValue] != ':')
                {
                    return JsonElement("':' expected");
                }
                position_ = startOfMemberValue;
                startOfMemberValue++;
                ConsumeWhitespaces(buffer_, startOfMemberValue, bufferLength_);

                JsonElement result = JsonElement(buffer_, startOfMemberValue, bufferLength_, memberName.GetBody(), memberName.GetBodyLength());

                if (result.GetElementType() == JsonElementType::None)
                {
                    return JsonElement("Invalid Json");
                }

                return result;
            }
            case ':':
            {
                JsonElement currentMember = JsonElement(buffer_, position_ + 1, bufferLength_);
                position_                 = currentMember.GetEnd();
                if (position_ == JSON_INVALID_POSITION)
                {
                    return JsonElement("Unexpected end of json");
                }
                switch (buffer_[position_])
                {
                    case ',':
                    {
                        position_++;
                        return GetNextMember();
                    }
                    case '}':
                    {
                        return JsonElement("End of object");
                    }
                    default:
                    {
                        return JsonElement("Invalid Json");
                    }
                }
            }
            case '}':
            {
                return JsonElement("End of object");
            }
        }
    }
    else if (elementType_ == JsonElementType::Array)
    {
        ConsumeWhitespaces(buffer_, position_, bufferLength_);
        if (position_ == JSON_INVALID_POSITION)
        {
            return JsonElement("Invalid Json");
        }

        switch (buffer_[position_])
        {
            case '[':
            {
                auto nextElement = JsonElement(buffer_, position_ + 1, bufferLength_);

                if (position_ == start_)
                {
                    position_++;
                    if (nextElement.GetElementType() == JsonElementType::None)
                    {
                        return GetNextMember();
                    }

                    return nextElement;
                }

                position_ = nextElement.GetEnd();
                return GetNextMember();
            }
            case ']':
            {
                return JsonElement("End of array");
            }
            case ',':
            {
                position_++;
                return JsonElement(buffer_, position_, bufferLength_);
            }
            default:
            {
                auto nextElement = JsonElement(buffer_, position_, bufferLength_);
                position_        = nextElement.GetEnd();
                return GetNextMember();
            }
        }
    }

    return JsonElement("Operation invalid");
}

JsonElement JsonElement::GetNextSibling()
{
    if (invalid_)
    {
        return JsonElement(nullptr, 0, 0);
    }

    auto end = GetEnd();
    return JsonElement(buffer_, end, bufferLength_);
}

#ifndef FILEREADERSETTING_HPP
#define FILEREADERSETTING_HPP

#include "BaseSetting.hpp"
#include "SettingsDefines.hpp"

#define FILE_SETTINGS_PAYLOAD_SIZE (64)

// Length is signed becaus it returns -1 if an error happened.
class FileReaderSetting : public BaseSetting3<uint32_t, int16_t, char[FILE_SETTINGS_PAYLOAD_SIZE]>
{
public:
    FileReaderSetting() : BaseSetting3<uint32_t, int16_t, char[FILE_SETTINGS_PAYLOAD_SIZE]>(SETTINGS_FILE)
    {
        Value0    = 0;
        Value1    = 0;
        Value2[0] = 0;
    };

    void Written(bool hadErrors) override;

    void Load() override{
        // Do nothing
    };

    void Save() override{
        // Do nothing
    };
};

extern FileReaderSetting fileReaderSetting_;

#endif /* FILEREADERSETTING_HPP */

#include "ISetting.hpp"
#include "BaseSetting.hpp"
#include "Common/Debugging.h"
#include "Drivers/FileSystem.hpp"
#include "Drivers/Core.h"
#include "Common/Utils.h"

static ISetting* settingsConsumerList_ = nullptr;
static bool settingsInitialized_       = false;

void ISetting::Register(const char* settingsID)
{
    ISetting** current = &settingsConsumerList_;
    while (*current != nullptr)
    {
        if (*current == this)
        {
            return;
        }

        assert(strcmp((*current)->settingID_, settingsID) != 0);
        current = &(*current)->next_;
    }

    settingID_  = settingsID;
    *current    = this;
    this->next_ = nullptr;

    if (settingsInitialized_)
    {
        Load();
    }
}

void ISetting::Initialize()
{
    if (settingsInitialized_)
    {
        return;
    }

    settingsInitialized_ = true;

    ISetting** current = &settingsConsumerList_;
    while (*current != nullptr)
    {
        CoreDisableInterrupts();
        (*current)->Load();

        LOG("Setting loaded: \"%s\"", (*current)->GetSettingID());

        CoreEnableInterrupts();
        current = &(*current)->next_;
    }
}

void ISetting::Load()
{
    File f(settingID_, FileAccessFlags::Read);
    if (f.IsOpen())
    {
        for (uint8_t i = 0; i < GetValueCount(); i++)
        {
            void* dataPointer = GetValuePtr(i);
            if (dataPointer != nullptr && GetValueMemorySize(i) > 0)
            {
                if (f.Read(dataPointer, GetValueMemorySize(i)) < GetValueMemorySize(i))
                {
                    memset(dataPointer, 0, GetValueMemorySize(i));
                }
            }
        }
    }
}

void ISetting::Save()
{
    File f(settingID_, FileAccessFlags::Create | FileAccessFlags::Write);
    if (f.IsOpen())
    {
        for (uint8_t i = 0; i < GetValueCount(); i++)
        {
            void* dataPointer = GetValuePtr(i);
            if (dataPointer != nullptr)
            {
                f.Write(dataPointer, GetValueMemorySize(i));
            }
        }
    }
}

void ISetting::Written(bool hadErrors)
{
    if (hadErrors)
    {
        Load();
    }
    else
    {
        Save();
    }
}

ISetting* ISetting::GetSetting(const char* settingID)
{
    return ISetting::GetSetting(settingID, strlen(settingID));
}

ISetting* ISetting::GetSetting(const char* settingID, uint16_t strLength)
{
    ISetting** current = &settingsConsumerList_;
    while (*current != nullptr)
    {
        bool match = true;
        int i      = 0;
        for (; i < strLength; i++)
        {
            if (settingID[i] != (*current)->settingID_[i])
            {
                match = false;
                break;
            }
            else if (settingID[i] == '\0')
            {
                break;
            }
            else if ((*current)->settingID_[i] == '\0')
            {
                match = false;
                break;
            }
        }

        if (match && (*current)->settingID_[i] == '\0')
        {
            return *current;
        }

        current = &(*current)->next_;
    }
    return nullptr;
}

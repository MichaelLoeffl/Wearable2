#include "DeviceSettings.hpp"
#include "AppSystemM4.hpp"
#include "BaseSetting.hpp"
#include "Common/Common.h"
#include "Drivers/FileSystem.hpp"
#include "JsonParser/JsonBufferHelpers.hpp"
#include "JsonParser/JsonElement.hpp"
#include "Notifications/NotificationService.hpp"
#include "Common/Utils.h"
#include <cstring>

#define TEMP_BUFFER_SIZE (64)
static char buffer_[TEMP_BUFFER_SIZE];

bool Settings::LoadSetting(const char* setting, void* buffer, int size)
{
    StringCopySafe("settings_", buffer_, TEMP_BUFFER_SIZE);
    StringCopySafe(setting, &buffer_[9], TEMP_BUFFER_SIZE - 9);

    File settingsFile(buffer_, FileAccessFlags::Read);
    if (settingsFile.IsOpen())
    {
        settingsFile.SeekAbsolute(0);
        if (settingsFile.Read(buffer, size) == size)
        {
            return true;
        }
    }

    return false;
}

bool Settings::SaveSetting(const char* setting, void* buffer, int size)
{
    StringCopySafe("settings_", buffer_, TEMP_BUFFER_SIZE);
    StringCopySafe(setting, &buffer_[9], TEMP_BUFFER_SIZE - 9);

    File settingsFile(buffer_, FileAccessFlags::Read | FileAccessFlags::Write | FileAccessFlags::Create);
    if (settingsFile.IsOpen())
    {
        settingsFile.SeekAbsolute(0);
        if (settingsFile.Write(buffer, size) == size)
        {
            return true;
        }
    }

    return false;
}

#define SETTING_READ_BUFFER_SIZE (64)

CFUNC void UpdateReadCharacteristic(uint8_t* val, uint16_t length);

CFUNC void UpdateReadString(const uint8_t* settingsId, uint16_t length, const uint8_t* name, uint16_t nameLength)
{
    uint8_t buffer[SETTING_READ_BUFFER_SIZE];

    auto setting = ISetting::GetSetting((const char*)settingsId, length);
    if (setting == nullptr)
    {
        LOG_INLINE("Requested setting not found: \"");

        /*char tmp[2];
        tmp[0] = 'X';
        tmp[1] = '\0';*/
        for (int i = 0; i < length; i++)
        {
            // tmp[0] = settingsId[i];
            // LOG_INLINE("%c", tmp);
            LOG_INLINE("%c", settingsId[i]);
        }

        LOG("\"");

        return;
    }

    LOG("Requested setting: %s", setting->GetSettingID());

    uint16_t pos = 0;

    buffer[pos++] = '{';
    buffer[pos++] = '"';

    for (int i = 0; i < nameLength && pos < SETTING_READ_BUFFER_SIZE; i++)
    {
        if (name[i] == '\0')
        {
            break;
        }
        buffer[pos++] = name[i];
    }

    buffer[pos++] = '"';
    buffer[pos++] = ':';

    uint16_t written = WriteValue(&buffer[pos], SETTING_READ_BUFFER_SIZE - pos - 2, setting);
    pos += written;

    if (written == 0)
    {
        return;
    }

    buffer[pos++] = '}';
    buffer[pos++] = '\0';

    LOG("Read Property updated: %s", buffer);
    UpdateReadCharacteristic(buffer, pos);
}

CFUNC void Settings::UpdateReadProperty(const char* settingsId, const char* name)
{
    if (name == nullptr)
    {
        name = settingsId;
    }

    UpdateReadString((const uint8_t*)settingsId, strlen((const char*)settingsId), (const uint8_t*)name, strlen((const char*)name));
}

bool WriteSettingIndex(uint16_t index, ISetting& setting, JsonElement& element)
{
    void* data = setting.GetValuePtr(index);

    if (data == nullptr)
    {
        return false;
    }

    switch (setting.GetValueType(index))
    {
        case SettingsValueDataType::Integer8:
            return element.TryParse(*((int8_t*)data));
        case SettingsValueDataType::Integer16:
            return element.TryParse(*((int16_t*)data));
        case SettingsValueDataType::Integer32:
            return element.TryParse(*((int32_t*)data));
        case SettingsValueDataType::Integer64:
            return element.TryParse(*((int64_t*)data));
        case SettingsValueDataType::UnsignedInteger8:
            return element.TryParse(*((uint8_t*)data));
        case SettingsValueDataType::UnsignedInteger16:
            return element.TryParse(*((uint16_t*)data));
        case SettingsValueDataType::UnsignedInteger32:
            return element.TryParse(*((uint32_t*)data));
        case SettingsValueDataType::UnsignedInteger64:
            return element.TryParse(*((uint64_t*)data));
        case SettingsValueDataType::Boolean:
            return element.TryParse(*((bool*)data));
        case SettingsValueDataType::CronExpression:
            return element.TryParse(*((cron_expr*)data));
        case SettingsValueDataType::String:
        {
            uint8_t* str = (uint8_t*)data;
            int size     = setting.GetValueMemorySize(index);
            if (size <= 0)
            {
                return false;
            }
            str[size - 1] = 0;
            for (int i = 0; i < size; i++)
            {
                if (i == size - 1 || i >= element.GetBodyLength())
                {
                    str[i] = 0;
                    break;
                }
                str[i] = element.GetBody()[i];
            }
            return true;
        }

        default:
            return false;
    }
}

void WriteSetting(ISetting& setting, JsonElement& element)
{
    bool success = true;
    if (element.GetElementType() == JsonElementType::Array)
    {
        auto child = element.GetFirstMember();
        for (uint16_t i = 0; i < setting.GetValueCount(); i++)
        {
            if (child.GetElementType() == JsonElementType::None)
            {
                break;
            }
            else
            {
                success &= WriteSettingIndex(i, setting, child);
            }
            child = element.GetNextMember();
        }
    }
    else
    {
        success &= WriteSettingIndex(0, setting, element);
    }

    setting.Written(!success);
}

CFUNC void UpdateSetting(const uint8_t* string, uint16_t length)
{
    LOG_INLINE("UpdateSetting called: \"");
    // char tmp[2];
    // tmp[0] = 'X';
    // tmp[1] = '\0';
    for (int i = 0; i < length; i++)
    {
        // tmp[0] = string[i];
        LOG_INLINE("%c", string[i]);
    }
    LOG("\"");

    if (string[0] != '{' && string[0] != '\"')
    {
        LOG("UpdateSetting called but no valid json detected");
        return;
    }

    JsonElement element(string, length);

    if (element.GetElementType() == JsonElementType::Object)
    {
        element = element.GetFirstMember();
        if (element.GetElementType() != JsonElementType::None && element.GetNameLength() != 0)
        {
            auto setting = ISetting::GetSetting((const char*)element.GetName(), element.GetNameLength());

            if (setting != nullptr)
            {
                LOG("Writing setting %s", setting->GetSettingID());
                WriteSetting(*setting, element);
            }
            else
            {
                LOG_INLINE("Setting not found: \"");
                // tmp[0] = 'X';
                // tmp[1] = '\0';
                for (int i = 0; i < element.GetNameLength(); i++)
                {
                    // tmp[0] = element.GetName()[i];
                    LOG_INLINE("%c", element.GetName()[i]);
                }
                LOG("\"");
            }
        }
    }
    else if (element.GetElementType() == JsonElementType::String)
    {
        UpdateReadString(element.GetBody(), element.GetBodyLength(), element.GetBody(), element.GetBodyLength());
    }
    else
    {
    }
}

void UpdateSetting(const char* string)
{
    UpdateSetting((const uint8_t*)string, strlen(string));
}

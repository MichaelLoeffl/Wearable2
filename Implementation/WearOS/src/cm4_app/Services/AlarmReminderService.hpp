#ifndef ALARMREMINDERSERVICE_HPP
#define ALARMREMINDERSERVICE_HPP

#include "Common/DateTime.hpp"
#include "Common/Timespan.hpp"
#include "Drivers/RtcDriver.h"
#include <string>

#define ALARM_TITLE_MAX_LENGTH (16)
#define MAX_ALARMS (5)

enum AlarmTypes
{
    Alarm    = 0,
    Reminder = 1,

    Count
};

struct AlarmData
{
    char Title[ALARM_TITLE_MAX_LENGTH] = {0};
    bool Active                        = false;
    bool Repeat                        = true;
    bool Weekdays[7]                   = {false};
    DateTime AlarmTime;
};

class AlarmReminderService
{
private:
    AlarmData alarms_[AlarmTypes::Count][MAX_ALARMS];

protected:
public:
    AlarmReminderService();

    AlarmData* GetAlarms(AlarmTypes alarmType);
};

#endif /* ALARMREMINDERSERVICE_HPP */

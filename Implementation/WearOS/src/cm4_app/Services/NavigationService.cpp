#include "NavigationService.hpp"
#include "AppSystemM4.hpp"
#include <cstdlib>

NavigationService::NavigationService()
{
}

void NavigationService::Update(u_int32_t identifier, uint8_t direction, uint32_t distance, const uint8_t* street)
{
    navigationActive_ = true;

    currentDirection_ = static_cast<NavigationDirections>(direction);
    currentDistance_  = distance;

    // appSystem_.StartApp(*navigationApp_);
}

void NavigationService::Start()
{
    navigationActive_ = true;
    // appSystem_.StartApp(*navigationApp_);
}

void NavigationService::Stop()
{
    navigationActive_ = false;
}

bool NavigationService::IsActive()
{
    return navigationActive_;
}

uint32_t NavigationService::GetDistance()
{
    return currentDistance_;
}

NavigationDirections NavigationService::GetDirection()
{
    return currentDirection_;
}

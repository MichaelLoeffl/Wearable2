#ifndef TIMESERVICE_HPP
#define TIMESERVICE_HPP

#include "Common/DateTime.hpp"
#include "Common/Timespan.hpp"
#include "Drivers/RtcDriver.h"
#include <string>

struct OffsetData
{
    char CityName[32] = { '\0' };
    int32_t OffsetMinutes = 0;
};

class TimeService
{
private:
    Timespan remoteOffset_ = 2;

protected:
public:
    TimeService();

    Timespan GetRemoteOffset();
    void SetRemoteOffset(const Timespan& offset);

    DateTime GetLocalTime();
    DateTime GetRemoteTime();

    const char* GetDayOfWeekText(uint16_t dow);
};

#endif /* TIMESERVICE_HPP */

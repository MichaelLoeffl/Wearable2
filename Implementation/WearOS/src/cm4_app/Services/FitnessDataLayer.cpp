#include "FitnessDataLayer.hpp"
#include "Settings/SettingsDefines.hpp"
#include "Drivers/FileSystem.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PrintHelper(format, value)                              \
    {                                                           \
        int result = sprintf(&buffer[position], format, value); \
        if (result < 0)                                         \
        {                                                       \
            error    = true;                                    \
            position = bufferFill;                              \
        }                                                       \
        else                                                    \
        {                                                       \
            position += result;                                 \
        }                                                       \
    }

#define PrintHelperNoArgs(format)                        \
    {                                                    \
        int result = sprintf(&buffer[position], format); \
        if (result < 0)                                  \
        {                                                \
            error    = true;                             \
            position = bufferFill;                       \
        }                                                \
        else                                             \
        {                                                \
            position += result;                          \
        }                                                \
    }

const int buffersize     = 1024;
const int securityRegion = 64;
static char buffer[1024];
static int bufferFill   = 0;
static bool firstObject = false;

void FitnessDataLayer::StartTracking(DateTime& date, const char* activity)
{
    bool error   = false;
    bufferFill   = 0;
    int position = bufferFill;
    firstObject  = true;
    int year     = date.GetYear();
    if (year < 2000)
    {
        // WORKAROUND: This is not necessary in the simulator, maybe we should fix this in DateTime
        year += 2000;
    }
    PrintHelper("{\"%04d-", year);
    PrintHelper("%02d-", date.GetMonth());
    PrintHelper("%02dT", date.GetDay());
    PrintHelper("%02d:", date.GetHour());
    PrintHelper("%02d:", date.GetMinute());
    PrintHelper("%02d", date.GetSecond());
    PrintHelperNoArgs("\":{");
    PrintHelper("\"activity\":{\"cat\":\"%s\"}", activity);

    bufferFill = position;

    if (!error)
    {
        DeleteFile(SETTINGS_RECENT_WORKOUT);
        File f(SETTINGS_RECENT_WORKOUT, FileAccessFlags::Create | FileAccessFlags::Write);
        f.Write(buffer, bufferFill);
    }
    bufferFill = 0;
}

void FitnessDataLayer::TakeSnapshot(SnapshotData& snapshot)
{
    bool error   = false;
    int position = bufferFill;

    PrintHelper(",\"%lu\":{", snapshot.SecondsSinceStart);

    PrintHelper("\"h\":%d", snapshot.HeartrateAverage);

    if (snapshot.Altitude != 0.0)
    {
        PrintHelper(",\"a\":%d", (int)(snapshot.Altitude + 0.5f));
    }
    if (snapshot.Steps > 0)
    {
        PrintHelper(",\"s\":%lu", snapshot.Steps);
    }

    PrintHelperNoArgs("}");

    bufferFill = position;

    if (bufferFill > buffersize - securityRegion && !error)
    {
        File f(SETTINGS_RECENT_WORKOUT, FileAccessFlags::Create | FileAccessFlags::Append);
        f.Write(buffer, bufferFill);
        bufferFill = 0;
    }
}

void FitnessDataLayer::StopTracking(ActivitySummary& summary)
{
    bool error   = false;
    int position = bufferFill;

    // Free buffer to make room for ending
    File f(SETTINGS_RECENT_WORKOUT, FileAccessFlags::Create | FileAccessFlags::Append);
    f.Write(buffer, bufferFill);
    bufferFill = 0;
    position   = 0;

    PrintHelperNoArgs(",\"summary\":{");

    PrintHelper("\"energy\":%d", summary.EnergyJoule);

    PrintHelperNoArgs("}}}");

    if (!error)
    {
        f.Write(buffer, position);
    }
}

void FitnessDataLayer::LoadSummary(ActivitySummary& summary)
{
    File f(FILENAME_WORKOUT_SUMMARY, FileAccessFlags::Read);
    if (f.IsOpen())
    {
        f.Read(&summary.Activity, sizeof(summary.Activity));
        f.Read(&summary.StartTime, sizeof(summary.StartTime));
        f.Read(&summary.Duration, sizeof(summary.Duration));

        f.Read(&summary.HeartrateAverage, sizeof(summary.HeartrateAverage));
        f.Read(&summary.MaxHeartrate, sizeof(summary.MaxHeartrate));
        f.Read(&summary.MinHeartrate, sizeof(summary.MinHeartrate));

        f.Read(&summary.EnergyJoule, sizeof(summary.EnergyJoule));

        f.Read(&summary.Steps, sizeof(summary.Steps));

        f.Read(&summary.AltitudeUp, sizeof(summary.AltitudeUp));
        f.Read(&summary.AltitudeDown, sizeof(summary.AltitudeDown));
        f.Read(&summary.MaxAltitude, sizeof(summary.MaxAltitude));
        f.Read(&summary.MinAltitude, sizeof(summary.MinAltitude));
    }
}

void FitnessDataLayer::SaveSummary(ActivitySummary& summary)
{
    File f(FILENAME_WORKOUT_SUMMARY, FileAccessFlags::Create | FileAccessFlags::Write);
    if (f.IsOpen())
    {
        f.Write(&summary.Activity, sizeof(summary.Activity));
        f.Write(&summary.StartTime, sizeof(summary.StartTime));
        f.Write(&summary.Duration, sizeof(summary.Duration));

        f.Write(&summary.HeartrateAverage, sizeof(summary.HeartrateAverage));
        f.Write(&summary.MaxHeartrate, sizeof(summary.MaxHeartrate));
        f.Write(&summary.MinHeartrate, sizeof(summary.MinHeartrate));

        f.Write(&summary.EnergyJoule, sizeof(summary.EnergyJoule));

        f.Write(&summary.Steps, sizeof(summary.Steps));

        f.Write(&summary.AltitudeUp, sizeof(summary.AltitudeUp));
        f.Write(&summary.AltitudeDown, sizeof(summary.AltitudeDown));
        f.Write(&summary.MaxAltitude, sizeof(summary.MaxAltitude));
        f.Write(&summary.MinAltitude, sizeof(summary.MinAltitude));
    }
}

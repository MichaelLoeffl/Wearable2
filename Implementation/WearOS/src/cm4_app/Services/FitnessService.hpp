#ifndef FITNESSSERVICE_HPP
#define FITNESSSERVICE_HPP

#include "Common/App.hpp"
#include "Common/Timespan.hpp"
#include "Common/FitnessActivities.hpp"
#include "Common/FitnessIntensities.hpp"
#include "Drivers/HeartrateDriver.h"
#include "Services/Settings.hpp"

// The sample snapshot length in seconds
#define SNAPSHOT_LENGTH (10)

struct ActivitySummary
{
    FitnessActivities Activity = FitnessActivities::None;
    DateTime StartTime;
    Timespan Duration = 0;

    uint32_t HeartrateSum     = 0;
    uint32_t HeartrateSamples = 0;
    uint32_t HeartrateAverage = 0;
    uint16_t MaxHeartrate     = 0;
    uint16_t MinHeartrate     = 0;
    uint16_t EnergyJoule      = 0;
    uint32_t Steps            = 0;
    float AltitudeUp          = 0.0;
    float AltitudeDown        = 0.0;
    float MaxAltitude         = 0.0;
    float MinAltitude         = 0.0;
};

struct SnapshotData
{
    uint32_t SecondsSinceStart = 0;
    uint32_t HeartrateSum      = 0;
    uint32_t HeartrateSamples  = 0;
    uint16_t HeartrateAverage  = 0;
    uint32_t Steps             = 0;
    float Altitude             = 0.0;
};

class UserWeight : public BaseSetting1<int16_t>
{
public:
    UserWeight() : BaseSetting1<int16_t>(SETTINGS_USER_WEIGHT){};
};

class UserAge : public BaseSetting1<int16_t>
{
public:
    UserAge() : BaseSetting1<int16_t>(SETTINGS_USER_AGE){};
};

class UserMaxHeartrate : public BaseSetting1<int16_t>
{
public:
    UserMaxHeartrate() : BaseSetting1<int16_t>(SETTINGS_MAX_HEARTRATE){};
};

class FitnessLevel : public BaseSetting1<int16_t>
{
public:
    FitnessLevel() : BaseSetting1<int16_t>(SETTINGS_FITNESS_LEVEL){};
};

class FitnessService
{
private:
    App* fitnessApp_ = nullptr;

    UserWeight userWeight_;
    UserAge userAge_;
    UserMaxHeartrate userMaxHeartrate_;
    FitnessLevel fitnessLevel_;

    FitnessActivities currentActivity_ = FitnessActivities::None;

    float seaLevel_ = 1013.25f;

    int heartrateValue_  = INVALID_HEARTRATE;
    uint16_t startSteps_ = 0;
    float lastAltitude_  = 0;

    ActivitySummary activitySummary_;

    void StartSensors();
    void StopSensors();
    void SensorTick();

    void TakeSnapshot();
    void ResetSnapshot();

    void BuildSummary();

    SnapshotData snapshot_;
    uint16_t snapshotLastSecond_ = 0;

    // TODO: This has to be configurable
    float bodyWeight_  = 75.0;
    uint16_t birthYear = 1970;

public:
    void SetFitnessApp(App* app) { fitnessApp_ = app; }
    void OpenFitnessApp();

    void LoadSummary();

    bool ActivityActive();

    bool StartActivity(FitnessActivities activity);
    bool StopActivity();

    void SecondElapsed();

    Timespan GetDuration();
    uint16_t GetHeartrate();
    uint16_t GetSteps();
    float GetAltitudeUp();
    float GetAltitudeDown();

    FitnessActivities GetCurrentActivity();
    ActivitySummary GetSummary();

    bool HasSteps(FitnessActivities activity);
    bool HasAltitude(FitnessActivities activity);

    uint16_t GetEnergy(FitnessActivities activity, Timespan duration, uint16_t averageHeartrate, uint16_t bodyWeight);
    float GetMets(FitnessActivities activity, FitnessIntensities intensity);
};

#endif /* FITNESSSERVICE_HPP */

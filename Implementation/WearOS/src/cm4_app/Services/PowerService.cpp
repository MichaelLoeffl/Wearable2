#include "PowerService.hpp"
#include "deps/BatteryDriver.h"
#include "Drivers/Bluetooth.h"
#include <cstdlib>

PowerService::PowerService()
{
}

void PowerService::ActivatePowerSaving(bool active, bool manuallyActivated)
{
    powerSavingActive_ = active;
    autoPowerSaving_   = !active || !manuallyActivated;
}

bool PowerService::IsPowerSavingActive()
{
    return powerSavingActive_;
}

bool PowerService::IsAutoPowerSavingActive()
{
    return autoPowerSaving_;
}

void PowerService::ActivateFlightMode(bool active)
{
    if (active)
    {
        //BleStop();
    }
    else
    {
        //BleStart();
    }
    flightMode_ = active;
}

bool PowerService::IsFlightModeActive()
{
    return flightMode_;
}

bool PowerService::IsCharging()
{
    return BatteryIsCharging();
}

uint16_t PowerService::GetBatteryLevel()
{
    batteryLevel_ = BatteryGetPercent();
    CheckAutoPowerSaving();
    return batteryLevel_;
}

void PowerService::CheckAutoPowerSaving()
{
    if (!autoPowerSaving_)
    {
        return;
    }

    if (batteryLevel_ <= BATTERY_PERCENT_FOR_AUTO_POWER_SAVING && !powerSavingActive_)
    {
        ActivatePowerSaving(true, false);
    }
    else if (batteryLevel_ > BATTERY_PERCENT_FOR_AUTO_POWER_SAVING && powerSavingActive_)
    {
        ActivatePowerSaving(false, false);
    }
}

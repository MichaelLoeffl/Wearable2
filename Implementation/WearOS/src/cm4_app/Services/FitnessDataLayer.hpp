#ifndef ACTIVITYTRACKER_HPP
#define ACTIVITYTRACKER_HPP

#include "FitnessService.hpp"
#include "Common/DateTime.hpp"

class FitnessDataLayer
{
public:
    void StartTracking(DateTime& time, const char* activity);
    void TakeSnapshot(SnapshotData& snapshot);
    void StopTracking(ActivitySummary& summary);
    void LoadSummary(ActivitySummary& summary);
    void SaveSummary(ActivitySummary& summary);
};

#endif /* ACTIVITYTRACKER_HPP */

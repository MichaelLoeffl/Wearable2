#ifndef SCHEDULINGSERVICE_HPP_ACA654
#define SCHEDULINGSERVICE_HPP_ACA654

#include "Common/DateTime.hpp"
#include <stdint.h>

class ISchedulable
{
    friend class SchedulingService;

private:
    ISchedulable* next_ = nullptr;
    DateTime dueDate_;

protected:
    void Schedule(const DateTime& dueDate);
    void Deschedule();

public:
    virtual void Notify() = 0;
    virtual ~ISchedulable() {};
};

class SchedulingService
{
public:
    void Remove(ISchedulable& schedulable);
    void Add(ISchedulable& schedulable);
    void Notify(ISchedulable& app);
    void Update();
};

#endif /* SCHEDULINGSERVICE_HPP_ACA654 */

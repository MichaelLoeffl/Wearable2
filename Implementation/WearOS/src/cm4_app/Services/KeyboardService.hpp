#ifndef KEYBOARDSERVICE_HPP
#define KEYBOARDSERVICE_HPP

#include "Common/Common.h"

void HidKeyPress(uint8_t key);
void SendVolumeUp();
void SendVolumeDown();

#endif /* KEYBOARDSERVICE_HPP */

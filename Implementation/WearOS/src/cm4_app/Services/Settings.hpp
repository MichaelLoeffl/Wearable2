#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include "Settings/BaseSetting.hpp"
#include "Settings/DeviceSettings.hpp"
#include "Settings/SettingsDefines.hpp"
#include "Settings/SettingsStorage.hpp"

#endif /* SETTINGS_HPP */

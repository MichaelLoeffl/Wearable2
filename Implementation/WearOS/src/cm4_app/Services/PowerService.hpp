#ifndef POWERSERVICE_HPP
#define POWERSERVICE_HPP

#include "Common/App.hpp"
#include <string>

#define BATTERY_PERCENT_FOR_AUTO_POWER_SAVING (15)

class PowerService
{
private:
    bool powerSavingActive_ = false;
    bool autoPowerSaving_   = true;
    bool flightMode_ = false;
    uint16_t batteryLevel_  = 0;

protected:
public:
    PowerService();

    void ActivatePowerSaving(bool active, bool manuallyActivated);
    bool IsPowerSavingActive();
    bool IsAutoPowerSavingActive();

    void ActivateFlightMode(bool active);
    bool IsFlightModeActive();

    void CheckAutoPowerSaving();

    bool IsCharging();
    uint16_t GetBatteryLevel();
};

#endif /* POWERSERVICE_HPP */

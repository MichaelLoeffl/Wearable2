#include "Settings.hpp"

extern uint16_t touchEmulation;

class RemoteTouchEmulationSetting : ISetting
{
public:
    RemoteTouchEmulationSetting() { Register("_emuTouch"); }

    uint8_t GetValueCount() override { return 1; }
    void* GetValuePtr(uint8_t index) override { return &touchEmulation; }
    int GetValueMemorySize(uint8_t index) override { return sizeof(uint16_t); }
    SettingsValueDataType GetValueType(uint8_t index) override { return SettingsValueDataType::UnsignedInteger16; }

    void Load() override { return; }
    void Save() override { return; }
};

RemoteTouchEmulationSetting touchEmulatonSetting;
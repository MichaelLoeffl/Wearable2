#include "AlarmReminderService.hpp"
#include "Drivers/RtcDriver.h"
#include "Settings.hpp"
#include <cstdlib>

AlarmReminderService::AlarmReminderService()
{
    for (int i = 0; i < MAX_ALARMS; i++)
    {
        sprintf(alarms_[AlarmTypes::Alarm][i].Title, "Alarm %d", i + 1);
        sprintf(alarms_[AlarmTypes::Reminder][i].Title, "Reminder %d", i + 1);
    }
}

AlarmData* AlarmReminderService::GetAlarms(AlarmTypes alarmType)
{
    return alarms_[alarmType];
}
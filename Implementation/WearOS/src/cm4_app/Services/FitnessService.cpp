#include "FitnessService.hpp"
#include "Drivers/AltimeterDriver.h"
#include "AppSystemM4.hpp"
#include "Common/Utils.h"
#include "Drivers/FileSystem.hpp"
#include "FitnessDataLayer.hpp"
#include "Drivers/HeartrateDriver.h"
#include "Drivers/StepsDriver.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static FitnessDataLayer dataLayer_;

bool FitnessService::ActivityActive()
{
    return currentActivity_ != FitnessActivities::None;
}

const char* ActivityToString(FitnessActivities activity)
{
    switch (activity)
    {
        case FitnessActivities::Cycling:
            return "Cycling";
        case FitnessActivities::Running:
            return "Running";
        case FitnessActivities::Strength:
            return "Strength";
        case FitnessActivities::Walking:
            return "Walking";
        case FitnessActivities::Yoga:
            return "Yoga";
        default:
            return "Unknown";
    }
}

bool FitnessService::StartActivity(FitnessActivities activity)
{
    if (currentActivity_ != FitnessActivities::None)
    {
        return false;
    }

    currentActivity_ = activity;

    // Reset values
    heartrateValue_ = INVALID_HEARTRATE;
    startSteps_     = HAL_StepCounter_GetCountedSteps();
    lastAltitude_   = HAL_Altimeter_GetAltitude(seaLevel_);

    activitySummary_.Activity         = activity;
    activitySummary_.StartTime        = DateTime::Now();
    activitySummary_.HeartrateSum     = 0;
    activitySummary_.HeartrateSamples = 0;
    activitySummary_.MaxHeartrate     = 0;
    activitySummary_.MinAltitude      = 0;
    activitySummary_.EnergyJoule      = 0;
    activitySummary_.Steps            = 0;
    activitySummary_.AltitudeUp       = 0;
    activitySummary_.AltitudeDown     = 0;
    activitySummary_.MaxAltitude      = 0;
    activitySummary_.MinAltitude      = 0;

    ResetSnapshot();

    StartSensors();

    DateTime time = DateTime::Now();
    dataLayer_.StartTracking(time, ActivityToString(activity));

    return true;
}

void FitnessService::BuildSummary()
{
    activitySummary_.Duration         = DateTime::Now() - activitySummary_.StartTime;
    activitySummary_.HeartrateAverage = activitySummary_.HeartrateSamples <= 0 ? 0 : activitySummary_.HeartrateSum / activitySummary_.HeartrateSamples;

    activitySummary_.EnergyJoule = GetEnergy(activitySummary_.Activity, activitySummary_.Duration, activitySummary_.HeartrateAverage, bodyWeight_);

    dataLayer_.SaveSummary(activitySummary_);
}

bool FitnessService::StopActivity()
{
    if (currentActivity_ == FitnessActivities::None)
    {
        return false;
    }

    currentActivity_ = FitnessActivities::None;

    StopSensors();

    BuildSummary();

    dataLayer_.StopTracking(activitySummary_);

    return true;
}

FitnessActivities FitnessService::GetCurrentActivity()
{
    return currentActivity_;
}

void FitnessService::OpenFitnessApp()
{
    appSystem_.StartApp(*fitnessApp_);
}

void FitnessService::StartSensors()
{
    HeartrateStartMeasurement();
}

void FitnessService::StopSensors()
{
    HeartrateEndMeasurement();
}

void FitnessService::SensorTick()
{
    if (currentActivity_ == FitnessActivities::None)
    {
        return;
    }
    else
    {
        heartrateValue_ = HeartrateUpdate();
        if (heartrateValue_ != INVALID_HEARTRATE)
        {
            activitySummary_.HeartrateSum += heartrateValue_;
            activitySummary_.HeartrateSamples++;

            snapshot_.HeartrateSum += heartrateValue_;
            snapshot_.HeartrateSamples++;
        }
    }

    if (HasSteps(currentActivity_))
    {
        uint32_t steps         = HAL_StepCounter_GetCountedSteps();
        activitySummary_.Steps = steps - startSteps_;
        snapshot_.Steps        = activitySummary_.Steps;
    }

    if (HasAltitude(currentActivity_))
    {
        float altitude     = HAL_Altimeter_GetAltitude(seaLevel_);
        snapshot_.Altitude = altitude;
        float diff         = lastAltitude_ - altitude;

        const float hysteresis = 10.0f;
        if (diff > hysteresis)
        {
            activitySummary_.AltitudeUp += diff;
            lastAltitude_ = altitude;
        }
        else if (diff < -hysteresis)
        {
            // Subtract negative diff from altitudeDown_ to achieve positive value
            activitySummary_.AltitudeDown -= diff;
            lastAltitude_ = altitude;
        }
    }

    activitySummary_.Duration   = DateTime::Now() - activitySummary_.StartTime;
    snapshot_.SecondsSinceStart = activitySummary_.Duration.TotalSeconds();
    if (snapshot_.SecondsSinceStart - snapshotLastSecond_ >= SNAPSHOT_LENGTH)
    {
        snapshotLastSecond_ = snapshot_.SecondsSinceStart;
        TakeSnapshot();
    }
}

Timespan FitnessService::GetDuration()
{
    return DateTime::Now() - activitySummary_.StartTime;
}

uint16_t FitnessService::GetHeartrate()
{
    return heartrateValue_;
}

uint16_t FitnessService::GetSteps()
{
    return activitySummary_.Steps;
}

float FitnessService::GetAltitudeUp()
{
    return activitySummary_.AltitudeUp;
}

float FitnessService::GetAltitudeDown()
{
    return activitySummary_.AltitudeDown;
}

void FitnessService::SecondElapsed()
{
    SensorTick();
}

ActivitySummary FitnessService::GetSummary()
{
    return activitySummary_;
}

bool FitnessService::HasSteps(FitnessActivities activity)
{
    return activity == FitnessActivities::Running || activity == FitnessActivities::Walking || activity == FitnessActivities::Other;
}

bool FitnessService::HasAltitude(FitnessActivities activity)
{
    return activity == FitnessActivities::Running || activity == FitnessActivities::Walking || activity == FitnessActivities::Cycling ||
           activity == FitnessActivities::Other;
}

uint16_t FitnessService::GetEnergy(FitnessActivities activity, Timespan duration, uint16_t averageHeartrate, uint16_t bodyWeight)
{
    // clang-format off
    // TODO: consider calculating this based on the max heartrate percentage
    FitnessIntensities intensity = FitnessIntensities::Normal;
    if (averageHeartrate <= 0) { intensity = FitnessIntensities::Normal; }
    else if (averageHeartrate > 185) { intensity = FitnessIntensities::VeryHigh; }
    else if (averageHeartrate > 160) { intensity = FitnessIntensities::High; }
    else if (averageHeartrate < 60) { intensity = FitnessIntensities::VeryLow; }
    else if (averageHeartrate < 90) { intensity = FitnessIntensities::Low; }
    // clang-format on

    float mets = GetMets(activity, intensity);

    // clang-format off
    if (intensity == FitnessIntensities::VeryLow) { mets -= 1.0; }
    else if (intensity == FitnessIntensities::Low) { mets -= 0.5; }
    else if (intensity == FitnessIntensities::High) { mets += 0.5; }
    else if (intensity == FitnessIntensities::VeryHigh) { mets += 1.0; }
    // clang-format on

    uint16_t energyKiloJoule = (uint16_t)(((float)bodyWeight * mets * (float)(duration.TotalHours() + (duration.TotalMinutes() / 60.0))) / 4.184);
    return energyKiloJoule;
}

float FitnessService::GetMets(FitnessActivities activity, FitnessIntensities intensity)
{
    // https://metscalculator.com/
    switch (activity)
    {
        case FitnessActivities::Running:
            return 7.5;
        case FitnessActivities::Walking:
            return 3.0;
        case FitnessActivities::Cycling:
            return 5.8;
        case FitnessActivities::Strength:
            return 5.8;
        case FitnessActivities::Yoga:
            return 3.8;
        default:
            return 6.0;
    }
}

void FitnessService::TakeSnapshot()
{
    snapshot_.HeartrateAverage = snapshot_.HeartrateSamples > 0 ? snapshot_.HeartrateSum / snapshot_.HeartrateSamples : 0;

    dataLayer_.TakeSnapshot(snapshot_);

    ResetSnapshot();
}

void FitnessService::ResetSnapshot()
{
    snapshot_.SecondsSinceStart = 0;
    snapshot_.HeartrateSum      = 0;
    snapshot_.HeartrateSamples  = 0;
    snapshot_.HeartrateAverage  = 0;
    snapshot_.Steps             = 0;
    snapshot_.Altitude          = 0.0;
}

void FitnessService::LoadSummary()
{
    dataLayer_.LoadSummary(activitySummary_);
}
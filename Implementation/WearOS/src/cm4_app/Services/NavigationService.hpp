#ifndef NAVIGATIONSERVICE_HPP
#define NAVIGATIONSERVICE_HPP

#include "Common/App.hpp"
#include "NavigationDirections.hpp"
#include <string>

class NavigationService
{
private:
    bool pairingActive_ = false;

    App* navigationApp_ = nullptr;

    bool navigationActive_                 = false;
    NavigationDirections currentDirection_ = NavigationDirections::None;
    uint32_t currentDistance_              = 0;

protected:
public:
    NavigationService();

    void SetNavigationApp(App* app) { navigationApp_ = app; }

    void Start();
    void Stop();
    void Update(u_int32_t identifier, uint8_t direction, uint32_t distance, const uint8_t* street);

    bool IsActive();
    uint32_t GetDistance();
    NavigationDirections GetDirection();
};

#endif /* NAVIGATIONSERVICE_HPP */

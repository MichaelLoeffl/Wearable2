#include "VibrationService.hpp"
#include "AppSystemM4.hpp"
#include "Drivers/Core.h"
#include "Drivers/Motor.h"
#include "Drivers/Wakelock.h"

#define VIBRATION_MSB_MASK (0x80000000)

VibrationServiceImplementation VibrationService;
static Wakelock VibrationWakelock_;

#if BOARD_TYPE == BOARD_TYPE_CYPRESS
#define VIBRATION_DIVIDER (150)
#elif BOARD_TYPE == BOARD_TYPE_QINNO
#define VIBRATION_DIVIDER (100)
#endif

void VibrationTick(uint32_t deltaT)
{
    VibrationService.MotorUpdate(deltaT);
}

void VibrationServiceImplementation::Vibrate(uint32_t pattern)
{
    if (pattern == 0)
    {
        return;
    }

    if (multiCatch == true)
    {
        return;
    }
    multiCatch = true;

    uint8_t length          = 32;
    uint32_t shiftedPattern = pattern;
    for (uint8_t shift = 0; shift < length; shift++)
    {
        if (shiftedPattern & VIBRATION_MSB_MASK)
        {
            length = length - shift;
            break;
        }
        shiftedPattern <<= 1;
    }
    // Add pause
    length += 4;

    if (32 - patternBufferBlocked >= length)
    {
        patternBuffer |= (shiftedPattern >> patternBufferBlocked);
        patternBufferBlocked += length;

        UpdateWakelock(&VibrationWakelock_, POWER_STATE_BACKGROUND_PASSIVE);
    }
    else
    {
        // Pattern did not fit into queue
    }
}

void VibrationServiceImplementation::MotorUpdate(uint32_t deltaT)
{
    static uint8_t divider = 0;

    if (patternBuffer != 0)
    {
        WakeDeviceIntoSnooze();
    }

    divider += deltaT;
    if (divider >= VIBRATION_DIVIDER)
    {
        divider -= VIBRATION_DIVIDER;
        multiCatch = false;
    }
    else
    {
        return;
    }

    bool vibrate = false;
    if (patternBufferBlocked > 0)
    {
        vibrate = (patternBuffer & VIBRATION_MSB_MASK) != 0;

        patternBuffer <<= 1;
        patternBufferBlocked--;
    }

    if (vibrate)
    {
        MotorStart();
        UpdateWakelock(&VibrationWakelock_, POWER_STATE_BACKGROUND_PASSIVE);
    }
    else
    {
        MotorStop();
        if (patternBuffer == 0 && patternBufferBlocked == 0)
        {
            ReleaseWakelock(&VibrationWakelock_);
        }
    }
}

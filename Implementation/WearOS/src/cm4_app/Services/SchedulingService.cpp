#include "SchedulingService.hpp"
#include "AppSystemM4.hpp"
#include "Common/DateTime.hpp"
#include "Common/Timespan.hpp"
#include "Drivers/Core.h"
#include "Drivers/RtcDriver.h"

static ISchedulable* firstToCall_ = nullptr;

SchedulingService schedulingService_;

void ISchedulable::Schedule(const DateTime& dueDate)
{
    dueDate_ = dueDate;
    schedulingService_.Add(*this);
}

void ISchedulable::Deschedule()
{
    schedulingService_.Remove(*this);
}

void SchedulingService::Remove(ISchedulable& schedulable)
{
    ISchedulable** current_ = &firstToCall_;

    while (*current_ != nullptr)
    {
        if (*current_ == &schedulable)
        {
            *current_         = (*current_)->next_;
            schedulable.next_ = nullptr;

            if (firstToCall_ != nullptr && (DateTime::Now() - firstToCall_->dueDate_).TotalMilliseconds() > 1000)
            {
                HAL_RTC_SetAlarm(true, firstToCall_->dueDate_.GetHour(), firstToCall_->dueDate_.GetMinute(), firstToCall_->dueDate_.GetSecond());
            }
            else
            {
            }
            return;
        }

        current_ = &(*current_)->next_;
    }
}

void SchedulingService::Add(ISchedulable& schedulable)
{
    ISchedulable** current_ = &firstToCall_;

    Remove(schedulable);
    while (*current_ != nullptr && (*current_)->dueDate_ < schedulable.dueDate_)
    {
        current_ = &(*current_)->next_;
    }

    schedulable.next_ = *current_;
    *current_         = &schedulable;

    if (firstToCall_ != nullptr && (DateTime::Now() - firstToCall_->dueDate_).TotalMilliseconds() > 1000)
    {
        HAL_RTC_SetAlarm(true, firstToCall_->dueDate_.GetHour(), firstToCall_->dueDate_.GetMinute(), firstToCall_->dueDate_.GetSecond());
    }
    else
    {
    }
}

void SchedulingService::Update()
{
    auto date = DateTime::Now();

    while (firstToCall_ != nullptr && firstToCall_->dueDate_ <= date)
    {
        auto c       = firstToCall_;
        firstToCall_ = c->next_;
        c->next_     = nullptr;

        c->Notify();
    }

    if (firstToCall_ != nullptr && (date - firstToCall_->dueDate_).TotalMilliseconds() > 1000)
    {
        HAL_RTC_SetAlarm(true, firstToCall_->dueDate_.GetHour(), firstToCall_->dueDate_.GetMinute(), firstToCall_->dueDate_.GetSecond());
    }
    else
    {
    }
}
#ifndef VIBRATIONSERVICE_HPP_3A1F93
#define VIBRATIONSERVICE_HPP_3A1F93

#include "Common/Common.h"

CFUNC void VibrationTick(uint32_t deltaT);

#define VIBRATE_NOTIFICATION (0x00007C1F)
#define VIBRATE_CLICK (0x00000001)
#define VIBRATE_HOME (0x00000003)

#define VIBRATION_PAUSE (4)

class VibrationServiceImplementation
{
private:
    uint32_t patternBuffer          = 0;
    uint8_t patternBufferBlocked    = 0;
    bool multiCatch                 = false;

public:
    void MotorUpdate(uint32_t deltaT);
    void Vibrate(uint32_t pattern);
};

extern VibrationServiceImplementation VibrationService;

#endif /* VIBRATIONSERVICE_HPP_3A1F93 */

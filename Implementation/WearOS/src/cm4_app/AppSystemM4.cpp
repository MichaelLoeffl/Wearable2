#include "AppSystemM4.hpp"
#include "ButtonLogic.h"
#include "Drivers/Buttons.h"
#include "Common/App.hpp"
#include "Drivers/Core.h"
#include "Drivers/FileSystem.hpp"
#include "Common/LinkQueueIterator.h"
#include "Notifications/NotificationService.hpp"
#include "Services/VibrationService.hpp"
#include "Common/Testing.hpp"
#include "Drivers/Wakelock.h"


#define APP_SYSTEM_DEFINE_APPS
#include "AppListing.hpp"
#undef APP_SYSTEM_DEFINE_APPS

#include "Drivers/Core.h"
#include "Drivers/DebugUart.h"
#include "Drivers/Motor.h"
#include "Renderer.hpp"
#include "Services/AlarmReminderService.hpp"
#include "Services/FitnessService.hpp"
#include "Services/NavigationService.hpp"
#include "Services/PairingService.hpp"
#include "Services/PowerService.hpp"
#include "Services/SchedulingService.hpp"
#include "Services/Settings.hpp"
#include "Services/TimeService.hpp"
#include "Drivers/Touch.h"
#include "Drivers/TouchHandler.hpp"
#include "Drivers/Wakelock.h"
#include <stdarg.h>
#include "Drivers/Implementation/BLE/BleStackHandler.hpp"

#include "PinMapping.h"

#include "Drivers/Bluetooth.h"

AppSystemM4 appSystem_;

uint16_t debounce;

Renderer renderer_;
TouchHandler touchHandler_;
PairingService pairingService_;
TimeService timeService_;
NavigationService navigationService_;
PowerService powerService_;
FitnessService fitnessService_;
AlarmReminderService alarmReminderService_;

// Services and Apps
extern NotificationService notificationService_;
extern SchedulingService schedulingService_;

uint16_t lastticks;
Wakelock tickLock;

class StartCountSetting : public BaseSetting1<uint32_t>
{
public:
    StartCountSetting() : BaseSetting1("startCount") {}
    virtual ~StartCountSetting() {};
};

static StartCountSetting startCount;

void UpdateSetting(const char* string);

extern "C"
{
    void UpdateBLE();

    void MainM4()
    {
        CoreInitialize();

        DateTime::UpdateRtcTime(0);

        FileSystemInitialize();

        ISetting::Initialize();

        DeviceUserIntervention(15000);

        VibrationService.Vibrate(0x15);

        startCount.Value++;
        startCount.Save();

        BleStackHandler.Initialize();

#ifdef SIMULATOR
        printf("StartCount: %d\n", startCount.Value);
#endif
        appSystem_.Startup();
    }
}

NotificationService* AppSystemM4::GetNotificationService()
{
    return &notificationService_;
}
SchedulingService* AppSystemM4::GetSchedulingService()
{
    return &schedulingService_;
}
TimeService* AppSystemM4::GetTimeService()
{
    return &timeService_;
}
PairingService* AppSystemM4::GetPairingService()
{
    return &pairingService_;
}
NavigationService* AppSystemM4::GetNavigationService()
{
    return &navigationService_;
}
PowerService* AppSystemM4::GetPowerService()
{
    return &powerService_;
}
FitnessService* AppSystemM4::GetFitnessService()
{
    return &fitnessService_;
}
AlarmReminderService* AppSystemM4::GetAlarmReminderService()
{
    return &alarmReminderService_;
}

CFUNC void ShowCode(uint16_t code);
CFUNC bool DisplaySend();

extern bool DisplayRefreshRequired;

#define LOGGG(x)                        \
    time_ = StopwatchStop(&stopwatch_); \
    if (time_ > 20)                     \
    {                                   \
        LOG(x ": %d", time_);           \
    }                                   \
    StopwatchStart(&stopwatch_)

CFUNC void SendScreenshot();
CFUNC void PrintButtons();

class testScheduler : public ISchedulable
{
    void Notify() override
    {
        Timespan span(3000);
        Schedule(DateTime::Now() + span);
    }
};

testScheduler s;

CFUNC void UpdateScreen();

void AppSystemM4::BeforeMainLoop()
{
    LOG("");
    LOG("------------------------------");
    LOG("Running self-test");
    LOG("------------------------------");
    LOG("");
    TestSuiteRoot.Run();

    LOG("");
    LOG("------------------------------");
    LOG("Startup complete");
    LOG("------------------------------");
    LOG("");
}

void AppSystemM4::MainLoop(uint32_t deltaT)
{
    if (UpdatePowerManager(deltaT))
    {
        CoreSleep(0);
    }

    BleUpdate();
    PowerState powerState = GetCurrentPowerState();

    ReleaseWakelock(&tickLock);

    switch (powerState)
    {
    // Fall-Trough all states
    case POWER_STATE_ACTIVE:
        HandleButton();
        HandleGraphics(deltaT);
        /* no break */

    case POWER_STATE_BACKGROUND_ACTIVE:
    case POWER_STATE_WORKOUT:
    case POWER_STATE_BACKGROUND_PASSIVE:
        VibrationTick(deltaT);
        /*if (DateTime::UpdateRtcTime(deltaT))
        {
            SecondElapsed();
            schedulingService_.Update();
            schedulingService_.Add(s);
        }*/
        /* no break */

    case POWER_STATE_PASSIVE:
        UpdateDebugUart();
        UpdateDebugInterface(deltaT);
        break;
        // other cases should never happen
    case POWER_STATE_DEEP_SLEEP:
        break;
    default:
        // Wait... what? why?
        HaltDevice(0xF5);
    }
}

void AppSystemM4::HandleButton()
{
    bool clicked, longPressed;
    if (ButtonGetEvents(&clicked, &longPressed))
    {
        DeviceUserIntervention(5000);
    }

    if (clicked)
    {
        LOG("Home Pressed");
        VibrationService.Vibrate(VIBRATE_CLICK);
        BackPressed();
    }

    if (longPressed)
    {
        LOG("Home Long-Pressed");
        VibrationService.Vibrate(VIBRATE_HOME);
        BackToLauncher();
    }
}

void AppSystemM4::HandleGraphics(uint32_t deltaT)
{
    lv_tick_inc(deltaT);

    NOTIFY_RENDERING_STARTED();
    static uint16_t delay = 0;
    delay += deltaT;
    if (delay >= 30)
    {
        delay = 0;
        appSystem_.Draw();
        lv_task_handler();
    }
    NOTIFY_RENDERING_FINISHED();

    DisplaySend();
}

void AppSystemM4::WokenUp()
{
    GetCurrentApp()->Invalidate();
}

void AppSystemM4::UserInteraction()
{
    DeviceUserIntervention(FOREGROUND_DEFAULT_KEEP_AWAKE);
}

App& AppSystemM4::Initialize()
{
    lv_init();

#define APP_SYSTEM_REGISTER_APPS
#include "AppListing.hpp"
#undef APP_SYSTEM_REGISTER_APPS

    notificationService_.SetNotificationApp(&NotificationModalApp_);
    pairingService_.SetPairingApp(&PairingApp_);
    navigationService_.SetNavigationApp(&NavigationApp_);
    fitnessService_.SetFitnessApp(&FitnessApp_);

    renderer_.Initialize();
    touchHandler_.Initialize();
    touchHandler_.PostInit();

    return Launcher_;
}

void AppSystemM4::SecondElapsed()
{
    GetCurrentApp()->SecondElapsed();
    fitnessService_.SecondElapsed();
}

void AppSystemM4::Draw()
{
    GetCurrentApp()->Draw();
}

void AppSystemM4::BackPressed()
{
    GetCurrentApp()->BackPressed();
}

void AppSystemM4::LongPressHome()
{
    // TODO: start shutdown app
}

void AppSystemM4::UpdateNotificationCount(AppIds appId, uint16_t count)
{
    App* app;
    LinkQueueIterator<APP_QUEUE_ALL_APPS> iterator = IterateQueue<APP_QUEUE_ALL_APPS>();
    while ((app = iterator.GetCurrentApp()) != nullptr)
    {
        if (app->AppId() == appId)
        {
            app->SetBadge(count > 0);
        }
        iterator.Next();
    }
}

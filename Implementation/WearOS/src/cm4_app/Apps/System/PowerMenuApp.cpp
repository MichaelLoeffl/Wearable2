#include "PowerMenuApp.hpp"
#include "AppSystemM4.hpp"
#include "Apps/Design/Design.h"
#include "Apps/Icons/Icons.h"
#include "Drivers/Bluetooth.h"
#include "Drivers/Core.h"

#include "../../lvgl/src/lv_objx/lv_img.h"
#include "../../lvgl/src/lv_objx/lv_imgbtn.h"
#include "../../lvgl/src/lv_objx/lv_page.h"
#include "Services/PowerService.hpp"
#include "Services/VibrationService.hpp"

CFUNC int LittleFS_Format();

static void ActionClick(lv_obj_t* btn, lv_event_t event)
{
    if (event != LV_EVENT_CLICKED)
    {
        return;
    }

    intptr_t buttonIndex = (intptr_t)lv_obj_get_user_data(btn);
    switch (buttonIndex)
    {
        case 0:
        {
            //BleClearBondList();
            break;
        }
        case 1:
        {
            PowerService* powerService = appSystem_.GetPowerService();
            powerService->ActivateFlightMode(!powerService->IsFlightModeActive());
            break;
        }
        case 2:
        {
            CoreShutdown();
            break;
        }
        default:
        {
            break;
        }
    }

    VibrationService.Vibrate(0x33);
    appSystem_.BackToLauncher();
}

void PowerMenuApp::Initialize()
{
}

void PowerMenuApp::Activated()
{
    activitySelectContainer_ = lv_cont_create(page_, nullptr);
    lv_page_glue_obj(activitySelectContainer_, true);
    lv_obj_align(activitySelectContainer_, page_, LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_set_size(activitySelectContainer_, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_obj_set_pos(activitySelectContainer_, 0, 0);

    static uint16_t buttonCount = 3;
    lv_img_dsc_t* buttonIcons[buttonCount];

    buttonIcons[0]             = &EnergyAppIcon;
    PowerService* powerService = appSystem_.GetPowerService();
    if (powerService->IsFlightModeActive())
    {
        buttonIcons[1] = &NavigationAppIcon;
    }
    else
    {
        buttonIcons[1] = &FlightModeAppIcon;
    }
    buttonIcons[2] = &YogaAppIcon;

    pageSize_ = 0;
    for (intptr_t i = 0; i < buttonCount; i++)
    {
        lv_obj_t* imageButton = lv_imgbtn_create(activitySelectContainer_, nullptr);
        lv_img_dsc_t* image   = buttonIcons[i];
        if (image == nullptr)
        {
            continue;
        }

        lv_imgbtn_set_src(imageButton, LV_BTN_STATE_REL, image);
        lv_imgbtn_set_src(imageButton, LV_BTN_STATE_PR, image);
        lv_imgbtn_set_src(imageButton, LV_BTN_STATE_TGL_REL, image);
        lv_imgbtn_set_src(imageButton, LV_BTN_STATE_TGL_PR, image);
        lv_obj_set_user_data(imageButton, (void*)i);
        lv_obj_set_event_cb(imageButton, ActionClick);
        lv_imgbtn_set_toggle(imageButton, true);
        lv_obj_set_size(imageButton, image->header.w, image->header.h);
        lv_obj_set_pos(imageButton, pageSize_ + APP_ICON_MARGIN / 2, (SCREEN_HEIGHT - image->header.h) / 2);
        lv_btn_set_fit(imageButton, true);
        lv_page_glue_obj(imageButton, true);
        lv_btn_set_layout(imageButton, LV_LAYOUT_OFF);

        pageSize_ += APP_ICON_MARGIN + image->header.w;
    }

    lv_obj_set_size(activitySelectContainer_, pageSize_, SCREEN_HEIGHT);
}

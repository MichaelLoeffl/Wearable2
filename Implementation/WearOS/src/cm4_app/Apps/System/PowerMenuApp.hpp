#ifndef POWERMENUAPP_HPP
#define POWERMENUAPP_HPP

#include "Apps/Icons/Icons.h"
#include "Common/App.hpp"

class PowerMenuApp : public App
{
private:
    lv_obj_t* activitySelectContainer_;

    int16_t pageSize_;

public:
    PowerMenuApp() : App(AppIds::PowerMenu) {}

    AppFolders AppFolder() override { return AppFolders::System; }

    void Initialize() override;
    void Activated() override;
};

#endif /* POWERMENUAPP_HPP */

#ifndef ILISTVIEW_H_07E106
#define ILISTVIEW_H_07E106

#include "../../lvgl/src/lv_core/lv_obj.h"

#define MAX_LISTVIEW_ITEMS (20)

class IListView
{
public:
    lv_obj_t* Container;
    lv_obj_t* NoContentLabel;

    virtual void ZoomItemIn(uint8_t index){};
    virtual void ZoomOut(){};
};

#endif /* ILISTVIEW_H_07E106 */

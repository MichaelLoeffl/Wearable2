#ifndef BUTTONLISTVIEW_H
#define BUTTONLISTVIEW_H

#include "ButtonListViewItem.h"
#include "IButtonListView.h"

typedef void (*ButtonListViewFunc)(uint32_t id);

class ButtonListView : public IButtonListView
{
    friend class ButtonListViewItem;

private:
    uint8_t itemCount_ = 0;
    uint8_t zoomedIndex_;
    ButtonListViewItem items_[MAX_BUTTONLISTVIEW_ITEMS];

    ButtonListViewFunc itemClickAction_ = nullptr;

    lv_obj_t* page_;

protected:
public:
    ButtonListView() {}

    void Create(lv_obj_t* parent);
    void Clear();

    ButtonListViewItem* AddItem(uint32_t identifier, const char* line1, const char* line2);
    void ItemClicked(uint8_t index) override;
    void ScrollToItem(int index);

    void SetItemClickAction(ButtonListViewFunc action) { itemClickAction_ = action; }

    void Show(bool visible);
};

#endif /* BUTTONLISTVIEW_H */

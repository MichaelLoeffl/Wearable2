#include <lvgl/src/lv_objx/lv_cont.h>
#include <lvgl/src/lv_objx/lv_label.h>
#include <lvgl/src/lv_objx/lv_page.h>
#include "ButtonListView.h"
#include "../Design/Design.h"
#include "ButtonListViewItem.h"
#include <string>

void ButtonListView::Create(lv_obj_t* parent)
{
    itemCount_ = 0;
    page_      = parent;
    Container  = lv_cont_create(page_, nullptr);
    lv_obj_align(Container, parent, LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_set_size(Container, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_obj_set_pos(Container, 0, 0);
    lv_page_glue_obj(Container, true);
    lv_page_set_edge_flash(page_, true);
    lv_page_set_sb_mode(page_, LV_SB_MODE_ON);

    NoContentLabel = lv_label_create(Container, nullptr);
    lv_label_set_static_text(NoContentLabel, "No Items");
    lv_page_glue_obj(NoContentLabel, true);
    lv_obj_align(NoContentLabel, Container, LV_ALIGN_IN_TOP_MID, 0, 20);
}

void ButtonListView::Clear()
{
    for (int index = 0; index < MAX_BUTTONLISTVIEW_ITEMS; index++)
    {
        ButtonListViewItem* item = &items_[index];
        if (item->IsCreated)
        {
            item->Destroy();
        }
    }

    itemCount_ = 0;

    ScrollToItem(0);
}

void ButtonListView::ItemClicked(uint8_t index)
{
    if (itemClickAction_ != nullptr)
    {
        ButtonListViewItem* item = &items_[index];
        itemClickAction_(item->identifier_);
    }
}

void ButtonListView::ScrollToItem(int index)
{
    lv_area_t area;
    lv_obj_t* scrollable = lv_page_get_scrl(page_);
    lv_obj_get_coords(scrollable, &area);
    lv_coord_t scroll = area.x1;
#ifdef SIMULATOR
    scroll -= SIMULATOR_PADDING;
#endif

    lv_page_scroll_hor(page_, -scroll - (index * BUTTONLISTVIEW_ITEM_WIDTH));
}

ButtonListViewItem* ButtonListView::AddItem(uint32_t identifier, const char* line1, const char* line2)
{
    if (itemCount_ >= MAX_BUTTONLISTVIEW_ITEMS)
    {
        return nullptr;
    }

    ButtonListViewItem* item = &items_[itemCount_];
    item->Create(this, itemCount_, identifier, line1, line2);

    itemCount_++;

    lv_obj_set_size(Container, itemCount_ * BUTTONLISTVIEW_ITEM_WIDTH, SCREEN_HEIGHT);

    return item;
}

void ButtonListView::Show(bool visible)
{
    lv_obj_set_hidden(Container, !visible);
}
#ifndef LISTVIEW_H_CF3DB4
#define LISTVIEW_H_CF3DB4

#include "IListView.h"
#include "ListViewItem.h"

typedef void (*ListViewFunc)(uint32_t id);

class IListViewDataSource
{
public:
    virtual bool GetItem(int index, uint32_t& identifier, const char*& line1, const char*& line2, const lv_img_dsc_t*& imageSource) = 0;
};

class ListView : public IListView
{
    friend class ListViewItem;

private:
    uint8_t itemCount_ = 0;
    uint8_t zoomedIndex_;
    uint16_t position_;
    ListViewItem items_[MAX_LISTVIEW_ITEMS];

    ListViewFunc itemOpenAction_ = nullptr;

    lv_obj_t* page_;

protected:
public:
    IListViewDataSource* DataSource = nullptr;

    ListView() {}

    void Create(lv_obj_t* parent);

    void UpdateItems();
    void ZoomItemIn(uint8_t index) override;
    void ZoomOut() override;
    void ScrollToItem(int index);

    void SetItemOpenAction(ListViewFunc action) { itemOpenAction_ = action; }
    void SetBadge(uint32_t identifier, bool active);
};

#endif /* LISTVIEW_H_CF3DB4 */

#include "ListView.h"
#include "../Design/Design.h"
#include "ListViewItem.h"
#include <string>
#include "../../lvgl/src/lv_objx/lv_cont.h"
#include "../../lvgl/src/lv_objx/lv_label.h"
#include "../../lvgl/src/lv_objx/lv_page.h"

void ListView::Create(lv_obj_t* parent)
{
    itemCount_ = 0;
    page_      = parent;
    Container  = lv_cont_create(page_, nullptr);
    lv_obj_align(Container, parent, LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_set_size(Container, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_obj_set_pos(Container, 0, 0);
    lv_page_glue_obj(Container, true);
    lv_page_set_edge_flash(page_, true);
    lv_page_set_sb_mode(page_, LV_SB_MODE_ON);

    NoContentLabel = lv_label_create(Container, nullptr);
    lv_label_set_static_text(NoContentLabel, "No Items");
    lv_page_glue_obj(NoContentLabel, true);
    lv_obj_align(NoContentLabel, Container, LV_ALIGN_IN_TOP_MID, 0, 20);
}

void ListView::ZoomItemIn(uint8_t index)
{
    zoomedIndex_ = index;

    for (size_t i = 0; i < itemCount_; i++)
    {
        if (i != index)
        {
            lv_obj_set_hidden(items_[i].container_, true);
        }
    }

    lv_obj_set_size(Container, itemCount_ * LISTVIEW_ITEM_WIDTH + (index + 1 >= itemCount_ ? SCREEN_WIDTH - LISTVIEW_ITEM_WIDTH : 0), SCREEN_HEIGHT);
    ScrollToItem(index);
    lv_page_glue_obj(Container, false);

    if (itemOpenAction_ != nullptr)
    {
        ListViewItem* item = &items_[index];
        itemOpenAction_(item->identifier_);
    }
}

void ListView::ZoomOut()
{
    for (size_t i = 0; i < itemCount_; i++)
    {
        lv_obj_set_hidden(items_[i].container_, false);
    }

    lv_obj_set_size(Container, itemCount_ * LISTVIEW_ITEM_WIDTH, SCREEN_HEIGHT);
    if (zoomedIndex_ > itemCount_ - 1)
    {
        lv_page_scroll_hor(page_, SCREEN_WIDTH - LISTVIEW_ITEM_WIDTH);
    }
    else if (zoomedIndex_ != 0)
    {
        lv_page_scroll_hor(page_, (SCREEN_WIDTH - LISTVIEW_ITEM_WIDTH) / 2);
    }

    lv_page_glue_obj(Container, true);
}

void ListView::ScrollToItem(int index)
{
    lv_area_t area;
    lv_obj_t* scrollable = lv_page_get_scrl(page_);
    lv_obj_get_coords(scrollable, &area);
    lv_coord_t scroll = area.x1;
#ifdef SIMULATOR
    scroll -= SIMULATOR_PADDING;
#endif

    lv_page_scroll_hor(page_, -scroll - (index * LISTVIEW_ITEM_WIDTH));
}

void ListView::UpdateItems()
{
    for (int i = 0; i < MAX_LISTVIEW_ITEMS; i++)
    {
        uint32_t identifier;
        const char* line1;
        const char* line2;
        const lv_img_dsc_t* image;
        if (DataSource == nullptr || !DataSource->GetItem(i, identifier, line1, line2, image))
        {
            for (int a = i; a < itemCount_; a++)
            {
                items_[a].Delete();
            }
            itemCount_ = i;

            break;
        }

        if (i >= itemCount_)
        {
            items_[i].Create(this);
            itemCount_++;
        }

        items_[i].Update(i, identifier, line1, line2, image);
    }

    if (itemCount_ == 1)
    {
        ZoomItemIn(0);
    }

    lv_obj_set_hidden(NoContentLabel, itemCount_ != 0);
}

void ListView::SetBadge(uint32_t identifier, bool active)
{
    for (size_t i = 0; i < itemCount_; i++)
    {
        if (items_[i].identifier_ == identifier)
        {
            items_[i].SetBadge(active);
        }
    }
}
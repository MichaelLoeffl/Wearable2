#ifndef BUTTONLISTVIEWITEM_H
#define BUTTONLISTVIEWITEM_H

#include "Common/App.hpp"
#include "IButtonListView.h"

#define BUTTONLISTVIEW_ITEM_WIDTH (48)
#define BUTTONLISTVIEW_ITEM_HEIGHT SCREEN_HEIGHT
#define BUTTONLISTVIEW_ITEM_MARGIN_VERT (10)
#define BUTTONLISTVIEW_ITEM_ICON_LEFT (8)

class ButtonListViewItem
{
    friend class ButtonListView;

private:
    IButtonListView* listView_;
    int index_;
    uint32_t identifier_;
    lv_obj_t* container_;
    lv_obj_t* label1_;
    lv_obj_t* label2_;

    void Create(IButtonListView* listView, int itemCount, uint32_t identifier, const char* line1, const char* line2);
    void Destroy();
    static void ClickItem(lv_obj_t* btn, lv_event_t event);

public:
    ButtonListViewItem() {}

    bool IsCreated = false;
};

#endif /* BUTTONLISTVIEWITEM_H */

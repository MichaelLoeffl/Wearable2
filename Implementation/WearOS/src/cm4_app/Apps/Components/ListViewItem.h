#ifndef LISTVIEWITEM_H_DF6A51
#define LISTVIEWITEM_H_DF6A51

#include "Common/App.hpp"
#include "IListView.h"

#define LISTVIEW_ITEM_WIDTH (SCREEN_WIDTH - 30)
#define LISTVIEW_ITEM_HEIGHT SCREEN_HEIGHT
#define LISTVIEW_ITEM_MARGIN_VERT (12)
#define LISTVIEW_ITEM_TEXT_LEFT (30)
#define LISTVIEW_ITEM_TEXT_MARGIN_RIGHT (4)
#define LISTVIEW_ITEM_ICON_LEFT (8)

class ListViewItem
{
    friend class ListView;

private:
    IListView* listView_;
    int index_;
    uint32_t identifier_;
    lv_obj_t* container_ = nullptr;
    lv_obj_t* label1_ = nullptr;
    lv_obj_t* label2_ = nullptr;
    lv_obj_t* image_ = nullptr;
    lv_obj_t* notificationSymbol_ = nullptr;
    bool isOpen_ = false;

    void Create(IListView* listView);
    void Update(int index, uint32_t identifier, const char* line1, const char* line2, const lv_img_dsc_t* imageSource);
    void Delete();
    static void ZoomItem(lv_obj_t* btn, lv_event_t event);

public:
    ListViewItem() {}

    void SetBadge(bool active);
};

#endif /* LISTVIEWITEM_H_DF6A51 */

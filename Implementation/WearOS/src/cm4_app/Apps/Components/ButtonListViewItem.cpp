#include "../../lvgl/src/lv_objx/lv_btn.h"
#include "../../lvgl/src/lv_objx/lv_img.h"
#include "../../lvgl/src/lv_objx/lv_label.h"
#include "../../lvgl/src/lv_objx/lv_page.h"
#include "ButtonListViewItem.h"
#include "../Design/Design.h"
#include "../Icons/Icons.h"
#include "../../Services/VibrationService.hpp"

extern lv_font_t Roboto18;
extern lv_font_t FreePixel15;

void ButtonListViewItem::ClickItem(lv_obj_t* btn, lv_event_t event)
{
    if (event != LV_EVENT_CLICKED)
    {
        return;
    }

    ButtonListViewItem* self = (ButtonListViewItem*)lv_obj_get_user_data(btn);
    if (self == NULL)
    {
        return;
    }

    self->listView_->ItemClicked(self->index_);
    VibrationService.Vibrate(VIBRATE_CLICK);
}

void ButtonListViewItem::Create(IButtonListView* listView, int index, uint32_t identifier, const char* line1, const char* line2)
{
    listView_   = listView;
    index_      = index;
    identifier_ = identifier;

    static lv_style_t containerStyle;
    static lv_style_t containerPressedStyle;
    lv_style_copy(&containerStyle, &lv_style_plain);
    containerStyle.body.border.width      = 0;
    containerStyle.body.grad_color.full   = COLOR_DARK_GRAY;
    containerStyle.body.main_color.full   = COLOR_DARK_GRAY;
    containerStyle.body.border.part       = LV_BORDER_RIGHT;
    containerStyle.body.border.color.full = COLOR_BLACK;
    containerStyle.body.border.width      = 2;
    containerStyle.text.color.full        = COLOR_WHITE;
    containerStyle.text.font              = &FreePixel15;
    lv_style_copy(&containerPressedStyle, &containerStyle);
    containerPressedStyle.body.grad_color.full = COLOR_DARK_GRAY_INK;
    containerPressedStyle.body.main_color.full = COLOR_DARK_GRAY_INK;

    static lv_style_t hugeStyle;
    lv_style_copy(&hugeStyle, &lv_style_plain);
    hugeStyle.text.font       = &Roboto18;
    hugeStyle.text.color.full = COLOR_WHITE;
    hugeStyle.line.color.full = COLOR_WHITE;

    container_ = lv_btn_create(listView_->Container, nullptr);
    lv_btn_set_style(container_, LV_BTN_STYLE_REL, &containerStyle);
    lv_btn_set_style(container_, LV_BTN_STYLE_PR, &containerPressedStyle);
    lv_btn_set_style(container_, LV_BTN_STYLE_TGL_REL, &containerStyle);
    lv_btn_set_style(container_, LV_BTN_STYLE_TGL_PR, &containerStyle);
    lv_btn_set_style(container_, LV_BTN_STYLE_INA, &containerStyle);
    lv_obj_set_size(container_, BUTTONLISTVIEW_ITEM_WIDTH, BUTTONLISTVIEW_ITEM_HEIGHT);
    lv_btn_set_layout(container_, LV_LAYOUT_OFF);
    lv_obj_align(container_, listView_->Container, LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_set_pos(container_, index * BUTTONLISTVIEW_ITEM_WIDTH, 0);
    lv_page_glue_obj(container_, true);
    lv_btn_set_ink_in_time(container_, INK_IN);
    lv_btn_set_ink_wait_time(container_, INK_WAIT);
    lv_btn_set_ink_out_time(container_, INK_OUT);
    lv_obj_set_user_data(container_, this);
    lv_obj_set_event_cb(container_, ClickItem);

    label1_ = lv_label_create(container_, NULL);
    lv_label_set_static_text(label1_, line1);
    lv_label_set_long_mode(label1_, LV_LABEL_LONG_CROP);
    lv_obj_set_width(label1_, BUTTONLISTVIEW_ITEM_WIDTH);
    lv_obj_align(label1_, container_, LV_ALIGN_IN_TOP_MID, 0, BUTTONLISTVIEW_ITEM_MARGIN_VERT);
    lv_label_set_align(label1_, LV_LABEL_ALIGN_CENTER);
    lv_label_set_style(label1_, LV_LABEL_STYLE_MAIN, &hugeStyle);

    label2_ = lv_label_create(container_, NULL);
    lv_label_set_static_text(label2_, line2);
    lv_label_set_long_mode(label2_, LV_LABEL_LONG_CROP);
    lv_obj_set_width(label2_, BUTTONLISTVIEW_ITEM_WIDTH);
    lv_label_set_align(label2_, LV_LABEL_ALIGN_CENTER);
    lv_obj_align(label2_, container_, LV_ALIGN_IN_BOTTOM_LEFT, 0, -BUTTONLISTVIEW_ITEM_MARGIN_VERT);

    IsCreated = true;
}

void ButtonListViewItem::Destroy()
{
    lv_obj_del(label1_);
    lv_obj_del(label2_);
    lv_obj_del(container_);

    IsCreated = false;
}
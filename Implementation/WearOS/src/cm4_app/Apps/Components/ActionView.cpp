#include <lvgl/src/lv_objx/lv_cont.h>
#include <lvgl/src/lv_objx/lv_img.h>
#include <lvgl/src/lv_objx/lv_imgbtn.h>
#include <lvgl/src/lv_objx/lv_label.h>
#include <lvgl/src/lv_objx/lv_page.h>
#include "ActionView.h"
#include "../Design/Design.h"
#include "../Icons/Icons.h"
#include <Services/VibrationService.hpp>

#define ACTIONVIEW_PADDING (5)
#define ACTIONVIEW_ITEM_MARGIN (5)
#define ACTIONVIEW_CONTENT_TOP (10)
#define ACTIONVIEW_CONTENT_BOTTOM (10)
#define BUTTON_IMAGE_WIDTH (21)
#define BUTTON_IMAGE_HEIGHT (32)
#define BUTTON_ICON_SIZE (16)

extern lv_font_t Roboto18;
extern lv_font_t FreePixel15;

void ActionView::Create(lv_obj_t* parent)
{
    // TODO: Find a way to make the elements stick
    // lv_obj_t* parentOfParent = parent; // lv_obj_get_parent(parent);

    container_ = lv_cont_create(parent, nullptr);
    lv_page_glue_obj(container_, true);
    lv_obj_align(container_, parent, LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_set_size(container_, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_obj_set_pos(container_, 0, 0);
    lv_obj_set_user_data(container_, this);
    lv_obj_set_event_cb(container_, ItemClick);

    image_ = lv_img_create(container_, nullptr);
    lv_obj_set_pos(image_, 0, 0);

    static lv_style_t regularStyle;
    lv_style_copy(&regularStyle, &lv_style_plain);
    regularStyle.text.font       = &FreePixel15;
    regularStyle.text.color.full = COLOR_WHITE;
    regularStyle.line.color.full = COLOR_WHITE;

    titleLabel_ = lv_label_create(container_, nullptr);
    lv_label_set_style(titleLabel_, LV_LABEL_STYLE_MAIN, &regularStyle);
    lv_label_set_long_mode(titleLabel_, LV_LABEL_LONG_CROP);
    lv_obj_align(titleLabel_, container_, LV_ALIGN_IN_TOP_LEFT, 0, 0);

    contentLabel_ = lv_label_create(container_, nullptr);
    lv_label_set_style(contentLabel_, LV_LABEL_STYLE_MAIN, &regularStyle);
    lv_label_set_long_mode(contentLabel_, LV_LABEL_LONG_CROP);
    lv_obj_align(contentLabel_, container_, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
    // cspell: disable-next-line
    lv_label_set_long_mode(contentLabel_, LV_LABEL_LONG_SROLL_CIRC);

    additionalContentLabel_ = lv_label_create(container_, nullptr);
    lv_label_set_style(additionalContentLabel_, LV_LABEL_STYLE_MAIN, &regularStyle);
    lv_label_set_long_mode(additionalContentLabel_, LV_LABEL_LONG_CROP);
    lv_obj_align(additionalContentLabel_, container_, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
    // cspell: disable-next-line
    lv_label_set_long_mode(additionalContentLabel_, LV_LABEL_LONG_EXPAND);
    lv_obj_set_hidden(additionalContentLabel_, true);

    static lv_style_t hugeStyle;
    lv_style_copy(&hugeStyle, &lv_style_plain);
    hugeStyle.text.font       = &Roboto18;
    hugeStyle.text.color.full = COLOR_WHITE;
    hugeStyle.line.color.full = COLOR_WHITE;
    hugeContentLabel_         = lv_label_create(container_, nullptr);
    lv_label_set_style(hugeContentLabel_, LV_LABEL_STYLE_MAIN, &hugeStyle);
    lv_label_set_long_mode(hugeContentLabel_, LV_LABEL_LONG_CROP);
    lv_obj_align(hugeContentLabel_, container_, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);

    primaryButtonImage_ = lv_imgbtn_create(container_, nullptr);
    lv_imgbtn_set_src(primaryButtonImage_, LV_IMGBTN_STYLE_REL, &ActionButtonRightHighlightControl);
    lv_imgbtn_set_src(primaryButtonImage_, LV_IMGBTN_STYLE_PR, &ActionButtonRightHighlightControl);
    lv_obj_set_size(primaryButtonImage_, (&ActionButtonRightHighlightControl)->header.w, (&ActionButtonRightHighlightControl)->header.h);
    lv_obj_set_pos(primaryButtonImage_, SCREEN_WIDTH - BUTTON_IMAGE_WIDTH, (SCREEN_HEIGHT - BUTTON_IMAGE_HEIGHT) / 2);
    lv_obj_set_user_data(primaryButtonImage_, this);
    lv_obj_set_event_cb(primaryButtonImage_, ItemClick);
    primaryButtonIcon_ = lv_img_create(container_, nullptr);
    lv_obj_set_size(primaryButtonIcon_, BUTTON_ICON_SIZE, BUTTON_ICON_SIZE);
    lv_obj_set_pos(primaryButtonIcon_, SCREEN_WIDTH - BUTTON_ICON_SIZE - 2, (SCREEN_HEIGHT - BUTTON_ICON_SIZE) / 2);
    lv_obj_set_user_data(primaryButtonIcon_, this);
    lv_obj_set_event_cb(primaryButtonIcon_, ItemClick);

    secondaryButtonImage_ = lv_imgbtn_create(container_, nullptr);
    lv_imgbtn_set_src(secondaryButtonImage_, LV_IMGBTN_STYLE_REL, &ActionButtonLeftControl);
    lv_imgbtn_set_src(secondaryButtonImage_, LV_IMGBTN_STYLE_PR, &ActionButtonLeftControl);
    lv_obj_set_size(secondaryButtonImage_, (&ActionButtonLeftControl)->header.w, (&ActionButtonLeftControl)->header.h);
    lv_obj_set_pos(secondaryButtonImage_, 0, (SCREEN_HEIGHT - BUTTON_IMAGE_HEIGHT) / 2);
    lv_obj_set_user_data(secondaryButtonImage_, this);
    lv_obj_set_event_cb(secondaryButtonImage_, ItemClick);
    secondaryButtonIcon_ = lv_img_create(container_, nullptr);
    lv_obj_set_size(secondaryButtonIcon_, BUTTON_ICON_SIZE, BUTTON_ICON_SIZE);
    lv_obj_set_pos(secondaryButtonIcon_, 2, (SCREEN_HEIGHT - BUTTON_ICON_SIZE) / 2);
    lv_obj_set_user_data(secondaryButtonIcon_, this);
    lv_obj_set_event_cb(secondaryButtonIcon_, ItemClick);
}

void ActionView::SetStyle(ActionViewStyles style)
{
    style_ = style;

    bool leftButtonVisible = style_ == ActionViewStyles::IconDoubleAction || style_ == ActionViewStyles::HugeDoubleAction ||
                             style_ == ActionViewStyles::OngoingDoubleAction || style_ == ActionViewStyles::IconDoubleActionHugeText;
    bool rightButtonVisible = style_ != ActionViewStyles::IconNoAction && style_ != ActionViewStyles::IconNoActionHugeText &&
                              style_ != ActionViewStyles::HugeNoAction && style_ != ActionViewStyles::OngoingNoAction;
    bool hugeMode = IsHugeMode();
    bool ongoingMode =
        style_ == ActionViewStyles::OngoingNoAction || style_ == ActionViewStyles::OngoingSingleAction || style_ == ActionViewStyles::OngoingDoubleAction;
    bool imageVisible = !hugeMode || style_ == ActionViewStyles::IconSingleActionHugeText || style_ == ActionViewStyles::IconNoActionHugeText;
    lv_obj_set_hidden(secondaryButtonImage_, !leftButtonVisible);
    lv_obj_set_hidden(secondaryButtonIcon_, !leftButtonVisible);
    lv_obj_set_hidden(primaryButtonImage_, !rightButtonVisible);
    lv_obj_set_hidden(primaryButtonIcon_, !rightButtonVisible);
    lv_obj_set_hidden(image_, !imageVisible);
    lv_obj_set_hidden(titleLabel_, ongoingMode);
    lv_obj_set_hidden(contentLabel_, hugeMode);
    lv_obj_set_hidden(hugeContentLabel_, !hugeMode);

    lv_coord_t imageWidth  = lv_obj_get_width(image_);
    int contentLeft        = leftButtonVisible ? BUTTON_IMAGE_WIDTH + ACTIONVIEW_ITEM_MARGIN : ACTIONVIEW_PADDING + 2;
    int contentRight       = rightButtonVisible ? BUTTON_IMAGE_WIDTH + ACTIONVIEW_ITEM_MARGIN : ACTIONVIEW_PADDING;
    int contentWidth       = SCREEN_WIDTH - contentLeft - contentRight;
    int imageOffsetTitle   = imageVisible && !ongoingMode ? imageWidth + ACTIONVIEW_ITEM_MARGIN : 0;
    int imageOffsetContent = style_ == ActionViewStyles::IconSingleActionHugeText || style_ == ActionViewStyles::IconNoActionHugeText
                                 ? imageOffsetTitle
                                 : 0; //(style_ != ActionViewStyles::IconDoubleAction && style_ != ActionViewStyles::IconNoAction) ? imageOffsetTitle : 0;

    lv_obj_set_x(titleLabel_, contentLeft + imageOffsetTitle);
    lv_obj_set_width(titleLabel_, contentWidth - imageOffsetTitle);
    lv_obj_set_width(contentLabel_, contentWidth - imageOffsetContent);
    lv_obj_set_width(hugeContentLabel_, contentWidth - imageOffsetTitle);
    lv_obj_set_x(image_, ongoingMode ? (SCREEN_WIDTH - imageWidth) / 2 : contentLeft);

    if (hugeMode)
    {
        lv_obj_set_y(image_, ACTIONVIEW_CONTENT_TOP - 1);
        lv_obj_set_y(titleLabel_, ACTIONVIEW_CONTENT_TOP);
        lv_obj_align(hugeContentLabel_, container_, LV_ALIGN_IN_BOTTOM_LEFT, contentLeft + imageOffsetContent, -ACTIONVIEW_CONTENT_BOTTOM);
    }
    else
    {
        lv_obj_set_y(image_, ACTIONVIEW_CONTENT_TOP - 1);
        lv_obj_set_y(titleLabel_, ACTIONVIEW_CONTENT_TOP);
        lv_obj_align(contentLabel_, container_, LV_ALIGN_IN_BOTTOM_LEFT, contentLeft + imageOffsetContent, -ACTIONVIEW_CONTENT_BOTTOM);
    }
}

bool ActionView::IsHugeMode()
{
    return style_ == ActionViewStyles::HugeNoAction || style_ == ActionViewStyles::HugeSingleAction || style_ == ActionViewStyles::HugeDoubleAction ||
           style_ == ActionViewStyles::IconNoActionHugeText || style_ == ActionViewStyles::IconSingleActionHugeText ||
           style_ == ActionViewStyles::IconDoubleActionHugeText;
}

void ActionView::SetImage(lv_img_dsc_t* imageSource)
{
    lv_img_set_src(image_, imageSource);
    lv_obj_set_size(image_, imageSource->header.w, imageSource->header.h);
}

void ActionView::SetTitle(const char* title)
{
    lv_label_set_static_text(titleLabel_, title);
}

void ActionView::SetContent(const char* content)
{
    lv_label_set_static_text(contentLabel_, content);
    lv_label_set_static_text(hugeContentLabel_, content);
    AlignAdditionalContent();
}

void ActionView::SetAdditionalContent(const char* content)
{
    lv_label_set_static_text(additionalContentLabel_, content);
    additionalContentSet_ = true;
    lv_obj_set_hidden(additionalContentLabel_, false);
    lv_label_set_long_mode(hugeContentLabel_, LV_LABEL_LONG_EXPAND);
    AlignAdditionalContent();
}

void ActionView::SetLeft(uint16_t left)
{
    lv_obj_set_x(container_, left);
}

void ActionView::AlignAdditionalContent()
{
    if (!additionalContentSet_)
    {
        return;
    }

    bool hugeMode          = IsHugeMode();
    lv_obj_t* contentLabel = hugeMode ? hugeContentLabel_ : contentLabel_;
    int left               = lv_obj_get_x(contentLabel) + lv_obj_get_width(contentLabel) + ACTIONVIEW_ITEM_MARGIN;
    int top                = lv_obj_get_y(contentLabel) + lv_obj_get_height(contentLabel) - lv_obj_get_height(additionalContentLabel_) - (hugeMode ? 1 : 0);
    lv_obj_set_pos(additionalContentLabel_, left, top);
    lv_obj_set_size(container_, left + lv_obj_get_width(additionalContentLabel_), SCREEN_HEIGHT);
}

void ActionView::ItemClick(lv_obj_t* object, lv_event_t event)
{
    if (event != LV_EVENT_CLICKED)
    {
        return;
    }

    ActionView* self = (ActionView*)lv_obj_get_user_data(object);
    if (self == nullptr)
    {
        return;
    }

    if ((object == self->primaryButtonImage_ || object == self->primaryButtonIcon_) && self->primaryButtonClickAction_ != nullptr)
    {
        self->primaryButtonClickAction_();
        VibrationService.Vibrate(VIBRATE_CLICK);
    }
    else if ((object == self->secondaryButtonImage_ || object == self->secondaryButtonIcon_) && self->secondaryButtonClickAction_ != nullptr)
    {
        self->secondaryButtonClickAction_();
        VibrationService.Vibrate(VIBRATE_CLICK);
    }
    else if (object == self->container_ && self->contentClickAction_ != nullptr)
    {
        self->contentClickAction_();
        VibrationService.Vibrate(VIBRATE_CLICK);
    }
}

void ActionView::SetButtons(ActionViewButtons primaryButton, ActionViewButtons secondaryButton)
{
    for (int i = 0; i < 2; i++)
    {
        lv_obj_t* image          = i <= 0 ? primaryButtonIcon_ : secondaryButtonIcon_;
        ActionViewButtons button = i <= 0 ? primaryButton : secondaryButton;
        lv_img_dsc_t* imageSource;
        switch (button)
        {
            case ActionViewButtons::Confirm:
                imageSource = &ConfirmActionView;
                break;
            case ActionViewButtons::Cancel:
                imageSource = &CancelActionView;
                break;
            case ActionViewButtons::Play:
                imageSource = &PlayActionView;
                break;
            case ActionViewButtons::Pause:
                imageSource = &PauseActionView;
                break;
            case ActionViewButtons::Reset:
                imageSource = &ResetActionView;
                break;
            case ActionViewButtons::Reply:
                imageSource = &ReplyActionView;
                break;
            case ActionViewButtons::Plus:
                imageSource = &PlusActionView;
                break;
            case ActionViewButtons::Minus:
                imageSource = &MinusActionView;
                break;
            case ActionViewButtons::Stop:
                imageSource = &StopActionView;
                break;
            case ActionViewButtons::Camera:
                imageSource = &CameraActionView;
                break;
            default:
                continue;
                break;
        }
        lv_img_set_src(image, imageSource);
    }
}

void ActionView::Show(bool visible)
{
    lv_obj_set_hidden(container_, !visible);
    // lv_obj_set_hidden(titleLabel_, !visible);
    // lv_obj_set_hidden(image_, !visible);
}
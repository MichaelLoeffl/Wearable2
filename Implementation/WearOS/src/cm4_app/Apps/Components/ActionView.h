#ifndef ACTIONVIEW_H_E17396
#define ACTIONVIEW_H_E17396

#include "Common/App.hpp"

enum ActionViewStyles : int
{
    IconNoAction = 0,
    IconSingleAction,
    IconDoubleAction,
    IconNoActionHugeText,
    IconSingleActionHugeText,
    IconDoubleActionHugeText,

    HugeNoAction,
    HugeSingleAction,
    HugeDoubleAction,

    OngoingNoAction,
    OngoingSingleAction,
    OngoingDoubleAction,

    ActionViewStylesCount
};

enum class ActionViewButtons
{
    None = 0,
    Confirm,
    Cancel,
    Play,
    Pause,
    Reset,
    Reply,
    Plus,
    Minus,
    Stop,
    Camera,
};

typedef void (*ActionViewFunc)();

class ActionView
{
private:
    lv_obj_t* container_;
    lv_obj_t* secondaryButtonImage_;
    lv_obj_t* primaryButtonImage_;
    lv_obj_t* secondaryButtonIcon_;
    lv_obj_t* primaryButtonIcon_;
    lv_obj_t* titleLabel_;
    lv_obj_t* contentLabel_;
    lv_obj_t* hugeContentLabel_;
    lv_obj_t* additionalContentLabel_;
    lv_obj_t* image_;

    bool additionalContentSet_ = false;

    ActionViewFunc primaryButtonClickAction_   = nullptr;
    ActionViewFunc secondaryButtonClickAction_ = nullptr;
    ActionViewFunc contentClickAction_         = nullptr;

    ActionViewStyles style_ = ActionViewStyles::IconNoAction;
    static void ItemClick(lv_obj_t* object, lv_event_t event);

    bool IsHugeMode();
    void AlignAdditionalContent();

protected:
public:
    ActionView() {}

    void Create(lv_obj_t* parent);
    void SetStyle(ActionViewStyles style);
    ActionViewStyles GetStyle() { return style_; }
    void SetImage(lv_img_dsc_t* imageSource);
    void SetTitle(const char* title);
    void SetContent(const char* content);
    void SetAdditionalContent(const char* content);
    void SetButtons(ActionViewButtons primaryButton, ActionViewButtons secondaryButton = ActionViewButtons::None);

    void SetPrimaryButtonAction(ActionViewFunc action) { primaryButtonClickAction_ = action; }
    void SetSecondaryButtonAction(ActionViewFunc action) { secondaryButtonClickAction_ = action; }
    void SetContentClickAction(ActionViewFunc action) { contentClickAction_ = action; }

    void SetLeft(uint16_t left);

    void Show(bool visible);
};

#endif /* ACTIONVIEW_H_E17396 */

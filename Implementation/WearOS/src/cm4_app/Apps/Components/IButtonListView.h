#ifndef IBUTTONLISTVIEW_H
#define IBUTTONLISTVIEW_H

#include "../../lvgl/src/lv_core/lv_obj.h"

#define MAX_BUTTONLISTVIEW_ITEMS (30)

class IButtonListView
{
public:
    lv_obj_t* Container;
    lv_obj_t* NoContentLabel;

    virtual void ItemClicked(uint8_t index){};
};

#endif /* IBUTTONLISTVIEW_H */

#include "ListViewItem.h"
#include "../Design/Design.h"
#include "../Icons/Icons.h"
#include <Services/VibrationService.hpp>
#include "../../lvgl/src/lv_objx/lv_btn.h"
#include "../../lvgl/src/lv_objx/lv_img.h"
#include "../../lvgl/src/lv_objx/lv_label.h"
#include "../../lvgl/src/lv_objx/lv_page.h"

extern lv_font_t FreePixel15;

void ListViewItem::ZoomItem(lv_obj_t* btn, lv_event_t event)
{
    if (event != LV_EVENT_CLICKED)
    {
        return;
    }

    ListViewItem* self = (ListViewItem*)lv_obj_get_user_data(btn);
    if (self == nullptr)
    {
        return;
    }

    self->isOpen_ = !self->isOpen_;
    if (self->isOpen_)
    {
        lv_obj_set_size(self->container_, SCREEN_WIDTH, LISTVIEW_ITEM_HEIGHT);
        lv_obj_set_width(self->label1_, SCREEN_WIDTH - LISTVIEW_ITEM_TEXT_LEFT - LISTVIEW_ITEM_TEXT_MARGIN_RIGHT);
        // cspell: disable-next-line
        lv_label_set_long_mode(self->label2_, LV_LABEL_LONG_SROLL_CIRC);
        lv_obj_set_width(self->label2_, SCREEN_WIDTH - LISTVIEW_ITEM_TEXT_LEFT - LISTVIEW_ITEM_TEXT_MARGIN_RIGHT);
        self->listView_->ZoomItemIn(self->index_);
        VibrationService.Vibrate(VIBRATE_CLICK);
    }
    else
    {
        lv_obj_set_size(self->container_, LISTVIEW_ITEM_WIDTH, LISTVIEW_ITEM_HEIGHT);
        lv_obj_set_width(self->label1_, LISTVIEW_ITEM_WIDTH - LISTVIEW_ITEM_TEXT_LEFT - LISTVIEW_ITEM_TEXT_MARGIN_RIGHT);
        lv_label_set_long_mode(self->label2_, LV_LABEL_LONG_CROP);
        lv_obj_set_width(self->label2_, LISTVIEW_ITEM_WIDTH - LISTVIEW_ITEM_TEXT_LEFT - LISTVIEW_ITEM_TEXT_MARGIN_RIGHT);
        self->listView_->ZoomOut();
        VibrationService.Vibrate(VIBRATE_CLICK);
    }
}

void ListViewItem::Update(int index, uint32_t identifier, const char* line1, const char* line2, const lv_img_dsc_t* imageSource)
{
    index_      = index;
    identifier_ = identifier;

    lv_obj_set_pos(container_, index * LISTVIEW_ITEM_WIDTH, 0);

    lv_label_set_static_text(label1_, line1);
    lv_label_set_static_text(label2_, line2);

    lv_img_set_src(image_, imageSource);
    lv_obj_set_size(image_, imageSource->header.w, imageSource->header.h);
}

void ListViewItem::Delete()
{
    if (container_ == nullptr)
    {
        return;
    }

    lv_obj_del(container_);
    container_ = nullptr;
}

void ListViewItem::Create(IListView* listView)
{
    listView_ = listView;

    static lv_style_t containerStyle;
    static lv_style_t containerPressedStyle;
    lv_style_copy(&containerStyle, &lv_style_plain);
    containerStyle.body.border.width      = 0;
    containerStyle.body.grad_color.full   = COLOR_BLACK;
    containerStyle.body.main_color.full   = COLOR_BLACK;
    containerStyle.body.border.part       = LV_BORDER_RIGHT;
    containerStyle.body.border.color.full = COLOR_GRAY;
    containerStyle.body.border.width      = 1;
    containerStyle.text.color.full        = COLOR_WHITE;
    containerStyle.text.font              = &FreePixel15;
    lv_style_copy(&containerPressedStyle, &containerStyle);
    containerPressedStyle.body.grad_color.full = COLOR_DARK_GRAY_INK;
    containerPressedStyle.body.main_color.full = COLOR_DARK_GRAY_INK;

    container_ = lv_btn_create(listView_->Container, nullptr);
    lv_btn_set_style(container_, LV_BTN_STYLE_REL, &containerStyle);
    lv_btn_set_style(container_, LV_BTN_STYLE_PR, &containerPressedStyle);
    lv_btn_set_style(container_, LV_BTN_STYLE_TGL_REL, &containerStyle);
    lv_btn_set_style(container_, LV_BTN_STYLE_TGL_PR, &containerStyle);
    lv_btn_set_style(container_, LV_BTN_STYLE_INA, &containerStyle);
    lv_obj_set_size(container_, LISTVIEW_ITEM_WIDTH, LISTVIEW_ITEM_HEIGHT);
    lv_btn_set_layout(container_, LV_LAYOUT_OFF);
    lv_obj_align(container_, listView_->Container, LV_ALIGN_IN_TOP_LEFT, 0, 0);

    lv_page_glue_obj(container_, true);
    lv_btn_set_ink_in_time(container_, INK_IN);
    lv_btn_set_ink_wait_time(container_, INK_WAIT);
    lv_btn_set_ink_out_time(container_, INK_OUT);
    lv_obj_set_user_data(container_, this);
    lv_obj_set_event_cb(container_, ZoomItem);

    image_ = lv_img_create(container_, nullptr);
    lv_obj_set_pos(image_, LISTVIEW_ITEM_ICON_LEFT, LISTVIEW_ITEM_MARGIN_VERT);

    label1_ = lv_label_create(container_, nullptr);
    lv_label_set_long_mode(label1_, LV_LABEL_LONG_CROP);
    lv_obj_set_width(label1_, LISTVIEW_ITEM_WIDTH - LISTVIEW_ITEM_TEXT_LEFT - LISTVIEW_ITEM_TEXT_MARGIN_RIGHT);
    lv_obj_align(label1_, container_, LV_ALIGN_IN_TOP_LEFT, LISTVIEW_ITEM_TEXT_LEFT, LISTVIEW_ITEM_MARGIN_VERT);

    label2_ = lv_label_create(container_, nullptr);
    lv_label_set_long_mode(label2_, LV_LABEL_LONG_CROP);
    lv_obj_set_width(label2_, LISTVIEW_ITEM_WIDTH - LISTVIEW_ITEM_TEXT_LEFT - LISTVIEW_ITEM_TEXT_MARGIN_RIGHT);
    lv_obj_align(label2_, container_, LV_ALIGN_IN_BOTTOM_LEFT, LISTVIEW_ITEM_TEXT_LEFT, -LISTVIEW_ITEM_MARGIN_VERT);

    notificationSymbol_ = lv_img_create(container_, nullptr);
    lv_img_set_src(notificationSymbol_, &NotificationStatus);
    lv_obj_set_size(notificationSymbol_, NotificationStatus.header.w, NotificationStatus.header.h);
    lv_obj_set_pos(notificationSymbol_, 20, 8);
}

void ListViewItem::SetBadge(bool active)
{
    lv_obj_set_hidden(notificationSymbol_, !active);
}
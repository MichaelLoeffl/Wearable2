#ifndef CALENDARAPP_H
#define CALENDARAPP_H

#include "Apps/Icons/Icons.h"
#include "Apps/Abstract/NotificationsBaseApp.hpp"

class CalendarApp : public NotificationsBaseApp
{
public:
    CalendarApp() : NotificationsBaseApp(AppIds::Calendar, AppIds::Calendar) {}
};

#endif /* CALENDARAPP_H */

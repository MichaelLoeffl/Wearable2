#ifndef REMINDERAPP_H_1A76C2
#define REMINDERAPP_H_1A76C2

#include "Apps/Abstract/AlarmReminderBaseApp.hpp"
#include "Services/AlarmReminderService.hpp"

class ReminderApp : public AlarmReminderBaseApp
{
public:
    ReminderApp() : AlarmReminderBaseApp(AppIds::Reminder, AlarmTypes::Reminder) {}
};

#endif /* REMINDERAPP_H_1A76C2 */

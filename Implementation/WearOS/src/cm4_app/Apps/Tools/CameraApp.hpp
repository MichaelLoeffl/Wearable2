#ifndef CAMERAAPP_H
#define CAMERAAPP_H

#include "Apps/Abstract/SingleValueBaseApp.hpp"
#include "Services/Settings/BaseSetting.hpp"
#include "Services/Settings/SettingsDefines.hpp"

class CameraApp : public SingleValueBaseApp
{
private:
public:
    CameraApp() : SingleValueBaseApp(AppIds::Camera) {}

    char additionalText_[32] = {0};

    void SetUp() override;
    void PrimaryAction() override;
};

#endif /* CAMERAAPP_H */

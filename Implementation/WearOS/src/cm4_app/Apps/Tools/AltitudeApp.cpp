#include "AltitudeApp.hpp"
#include "Drivers/AltimeterDriver.h"
#include <cstdio>

void AltitudeApp::SetUp()
{
    actionView_.SetTitle("Altitude");
    actionView_.SetButtons(ActionViewButtons::None);
    actionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
}

void AltitudeApp::Update()
{
    float altitude = HAL_Altimeter_GetAltitude(1013.25f);
    float pressure = HAL_Altimeter_GetPressure();

    int intVal = altitude;
    sprintf(valueText_, "%d m", intVal);
    actionView_.SetContent(valueText_);

    sprintf(additionalText_, "%.0f hPa / %.2f Bar", pressure, pressure / 1000.0);
    actionView_.SetAdditionalContent(additionalText_);
}

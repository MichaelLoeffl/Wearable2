#include "CameraApp.hpp"
#include "AppSystemM4.hpp"
#include "Services/KeyboardService.hpp"

void CameraApp::SetUp()
{
    actionView_.SetTitle("Camera");
    actionView_.SetContent("+");
    actionView_.SetButtons(ActionViewButtons::Camera);
    actionView_.SetStyle(ActionViewStyles::IconSingleActionHugeText);
}

void CameraApp::PrimaryAction()
{
    SendVolumeUp();
}

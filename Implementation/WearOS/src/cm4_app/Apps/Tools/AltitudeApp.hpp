#ifndef ALTITUDEAPP_H
#define ALTITUDEAPP_H

#include "Apps/Abstract/SingleValueBaseApp.hpp"
#include "Services/Settings/BaseSetting.hpp"
#include "Services/Settings/SettingsDefines.hpp"

class AltitudeSetting : public BaseSetting1<int32_t>
{
public:
    AltitudeSetting() : BaseSetting1<int32_t>(SETTINGS_ALTITUDE){};
};

class AltitudeApp : public SingleValueBaseApp
{
private:
    AltitudeSetting altitudeSetting_;

public:
    AltitudeApp() : SingleValueBaseApp(AppIds::Altitude) {}

    char additionalText_[32] = {0};

    void SetUp() override;
    void Update() override;
};

#endif /* ALTITUDEAPP_H */

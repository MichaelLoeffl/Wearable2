#ifndef LAUNCHER_H_0CBF74
#define LAUNCHER_H_0CBF74

#include "Common/App.hpp"
#include "Common/AppFolders.hpp"
#include "Common/DateTime.hpp"
#include "Drivers/Display.h"

class Launcher : public App
{
private:
    DateTime delay_;
    lv_obj_t* homeContainer_ = nullptr;
    lv_obj_t* homeContainerCanvas_ = nullptr;

    lv_obj_t* batterySymbol_ = nullptr;
    lv_obj_t* chargingSymbol_ = nullptr;
    lv_obj_t* powerSavingSymbol_ = nullptr;
    lv_obj_t* flightModeSymbol_ = nullptr;
    lv_obj_t* notificationSymbol_ = nullptr;
    lv_obj_t* activitySymbol_ = nullptr;

    uint16_t batteryLevel_ = 0;

    bool showLocalTime_ = true;

    FitnessActivities currentActivity_ = FitnessActivities::None;

    void UpdateHome();
    void UpdateStatusIcons();
    void RotateHomeCanvas();
    static void HomeClicked(lv_obj_t* btn, lv_event_t event);

public:
    Launcher() : App(AppIds::Launcher) { delay_ = 0; }
    virtual ~Launcher() {}

    AppFolders AppFolder() override { return AppFolders::Home; }
    bool IgnoreRotation() override { return true; }

protected:
    void Initialize() override;

    void Restore() override;
    void Activated() override;
    void Deactivated() override;
    void Shutdown() override;
    void SecondElapsed() override;

    void Draw() override;

public:
    void BackPressed() override;
    void BackLongPressed() override;
};

#endif /* LAUNCHER_H_0CBF74 */

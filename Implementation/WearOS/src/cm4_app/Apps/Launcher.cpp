#include "Launcher.hpp"
#include "../lvgl/src/lv_core/lv_disp.h"
#include "../lvgl/src/lv_objx/lv_btn.h"
#include "../lvgl/src/lv_objx/lv_canvas.h"
#include "../lvgl/src/lv_objx/lv_img.h"
#include "../lvgl/src/lv_objx/lv_imgbtn.h"
#include "../lvgl/src/lv_objx/lv_list.h"
#include "AppSystemM4.hpp"
#include "Apps/Design/Design.h"
#include "Apps/Icons/Icons.h"
#include "Apps/System/PowerMenuApp.hpp"
#include "Common/AppSystem.hpp"
#include "Drivers/Bluetooth.h"
#include "Drivers/Touch.h"
#include "Notifications/NotificationService.hpp"
#include "Services/FitnessService.hpp"
#include "Services/PowerService.hpp"
#include "Services/TimeService.hpp"
#include "Services/VibrationService.hpp"

extern lv_font_t Roboto18;
extern lv_font_t FreePixel15;

#include "AppListing.hpp"
#include "Common/Utils.h"

static const int32_t returnToMainDelay_ = 8000;
static bool returnToClock_              = false;

extern "C"
{
    extern lv_img_dsc_t Settings;
}

void Launcher::Initialize()
{
}

void Launcher::Restore()
{
}

void Launcher::SecondElapsed()
{
    UpdateHome();
}

static void StartApp(lv_obj_t* btn, lv_event_t event)
{
    if (event != LV_EVENT_CLICKED)
    {
        return;
    }

    App* app = (App*)lv_obj_get_user_data(btn);

    if (app != nullptr)
    {
        appSystem_.StartApp(*app);

        VibrationService.Vibrate(VIBRATE_CLICK);
    }
}

void Launcher::HomeClicked(lv_obj_t* btn, lv_event_t event)
{
    if (event != LV_EVENT_CLICKED)
    {
        return;
    }

    Launcher* self = (Launcher*)lv_obj_get_user_data(btn);

    if (self->currentActivity_ != FitnessActivities::None)
    {
        FitnessService* fitnessService = appSystem_.GetFitnessService();
        fitnessService->OpenFitnessApp();
    }
    else
    {
        self->showLocalTime_ = !self->showLocalTime_;
        self->UpdateHome();
    }

    VibrationService.Vibrate(VIBRATE_CLICK);
}

void Launcher::Activated()
{
    NotificationService* notificationService = appSystem_.GetNotificationService();
    FitnessService* fitnessService           = appSystem_.GetFitnessService();

    currentActivity_ = fitnessService->GetCurrentActivity();

    // MainScreen
    lv_obj_t* container = lv_cont_create(page_, nullptr);
    lv_obj_set_size(container, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_page_glue_obj(container, true);

    homeContainer_ = lv_cont_create(container, nullptr);
    static lv_style_t emptyStyle;
    lv_style_copy(&emptyStyle, &lv_style_plain);
    emptyStyle.body.border.width    = 0;
    emptyStyle.body.grad_color.full = COLOR_BLACK;
    emptyStyle.body.main_color.full = COLOR_BLACK;
    lv_obj_set_style(homeContainer_, &emptyStyle);
    lv_obj_set_size(homeContainer_, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_obj_set_pos(homeContainer_, 0, 0);
    lv_page_glue_obj(homeContainer_, true);

    lv_obj_set_user_data(homeContainer_, this);
    lv_obj_set_event_cb(homeContainer_, HomeClicked);

    homeContainerCanvas_ = lv_canvas_create(homeContainer_, nullptr);

    batterySymbol_ = lv_img_create(homeContainer_, nullptr);
    lv_img_set_src(batterySymbol_, &Battery0Status);
    lv_obj_set_size(batterySymbol_, Battery0Status.header.w, Battery0Status.header.h);
    lv_obj_set_pos(batterySymbol_, SCREEN_WIDTH - Battery0Status.header.w - 3, (SCREEN_HEIGHT - Battery0Status.header.h) / 2);

    chargingSymbol_ = lv_img_create(homeContainer_, nullptr);
    lv_img_set_src(chargingSymbol_, &ChargingStatus);
    lv_obj_set_size(chargingSymbol_, ChargingStatus.header.w, ChargingStatus.header.h);
    lv_obj_set_pos(chargingSymbol_, SCREEN_WIDTH - ChargingStatus.header.w - 3, (SCREEN_HEIGHT - ChargingStatus.header.h) / 2 + Battery0Status.header.h);

    powerSavingSymbol_ = lv_img_create(homeContainer_, nullptr);
    lv_img_set_src(powerSavingSymbol_, &PowerSavingStatus);
    lv_obj_set_size(powerSavingSymbol_, PowerSavingStatus.header.w, PowerSavingStatus.header.h);
    lv_obj_set_pos(powerSavingSymbol_,
                   SCREEN_WIDTH - PowerSavingStatus.header.w - 3,
                   (SCREEN_HEIGHT - PowerSavingStatus.header.h) / 2 + Battery0Status.header.h);

    flightModeSymbol_ = lv_img_create(homeContainer_, nullptr);
    lv_img_set_src(flightModeSymbol_, &FlightModeStatus);
    lv_obj_set_size(flightModeSymbol_, FlightModeStatus.header.w, FlightModeStatus.header.h);
    lv_obj_set_pos(flightModeSymbol_, 0, 0);

    notificationSymbol_ = lv_img_create(homeContainer_, nullptr);
    lv_img_set_src(notificationSymbol_, &NotificationStatus);
    lv_obj_set_size(notificationSymbol_, NotificationStatus.header.w, NotificationStatus.header.h);
    lv_obj_set_pos(notificationSymbol_, 3, (SCREEN_HEIGHT - NotificationStatus.header.h) / 2);

    activitySymbol_ = lv_img_create(container, nullptr);
    lv_img_set_src(activitySymbol_, Tools::GetFitnessActivityLargeIcon(currentActivity_));
    lv_obj_set_size(activitySymbol_, RunningActivity.header.w, RunningActivity.header.h);
    lv_obj_set_pos(activitySymbol_, 80, (SCREEN_HEIGHT - RunningActivity.header.h) / 2);
    lv_obj_set_hidden(activitySymbol_, currentActivity_ == FitnessActivities::None);

    UpdateHome();
    UpdateStatusIcons();

    // Add app list
    int size = 0;

    App* app;
    LinkQueueIterator<APP_QUEUE_ALL_APPS> iterator = System()->IterateQueue<APP_QUEUE_ALL_APPS>();
    while ((app = iterator.GetCurrentApp()) != nullptr)
    {
        if (app != this && app->AppFolder() == AppFolders::Launcher)
        {
            lv_obj_t* imageButton = lv_imgbtn_create(page_, nullptr);
            lv_img_dsc_t* image   = app->GetAppImage();
            if (image == nullptr)
            {
                image = &SettingsAppIcon;
            }

            lv_imgbtn_set_src(imageButton, LV_BTN_STATE_REL, image);
            lv_imgbtn_set_src(imageButton, LV_BTN_STATE_PR, image);
            lv_imgbtn_set_src(imageButton, LV_BTN_STATE_TGL_REL, image);
            lv_imgbtn_set_src(imageButton, LV_BTN_STATE_TGL_PR, image);
            lv_obj_set_user_data(imageButton, app);
            lv_obj_set_event_cb(imageButton, StartApp);
            lv_imgbtn_set_toggle(imageButton, true);
            lv_obj_set_size(imageButton, image->header.w, image->header.h);
            lv_obj_set_pos(imageButton, SCREEN_WIDTH + size + APP_ICON_MARGIN / 2, (SCREEN_HEIGHT - image->header.h) / 2);
            lv_btn_set_fit(imageButton, true);
            lv_page_glue_obj(imageButton, true);
            lv_btn_set_layout(imageButton, LV_LAYOUT_OFF);

            lv_obj_t* notificationImage = lv_img_create(page_, nullptr);
            lv_img_set_src(notificationImage, &NotificationBlackBorderStatus);
            lv_obj_set_pos(notificationImage, SCREEN_WIDTH + size + APP_ICON_MARGIN / 2 - 3, (SCREEN_HEIGHT - image->header.h) / 2 - 3);
            app->SetBadgeImage(notificationImage);
            app->SetBadge(notificationService->GetUnreadNotificationCount(app->AppId()) > 0);

            size += APP_ICON_MARGIN + image->header.w;
        }
        iterator.Next();
    }

    SetPageSize(SCREEN_WIDTH + size);
}

void Launcher::Deactivated()
{
    App* app;
    LinkQueueIterator<APP_QUEUE_ALL_APPS> iterator = System()->IterateQueue<APP_QUEUE_ALL_APPS>();
    while ((app = iterator.GetCurrentApp()) != nullptr)
    {
        if (app != this && app->AppFolder() == AppFolders::Launcher)
        {
            app->SetBadgeImage(nullptr);
        }
        iterator.Next();
    }
}

void Launcher::Shutdown()
{
}

void Launcher::UpdateHome()
{
    NotificationService* notificationService = appSystem_.GetNotificationService();
    auto notificationCount                   = notificationService->GetUnreadNotificationCount(AppIds::Any);

    lv_obj_set_hidden(notificationSymbol_, notificationCount <= 0);

    TimeService* timeService = appSystem_.GetTimeService();
    DateTime dateTime        = showLocalTime_ ? timeService->GetLocalTime() : timeService->GetRemoteTime();

    auto dayOfWeekText = timeService->GetDayOfWeekText(dateTime.GetDayOfWeek());
    auto remoteOffset  = timeService->GetRemoteOffset();

    char hourText[16];
    char minuteText[16];

    sprintf(hourText, "%02d", (int)dateTime.GetHour());
    sprintf(minuteText, "%02d", (int)dateTime.GetMinute());

    static lv_style_t style;
    lv_style_copy(&style, &lv_style_plain);
    style.text.font            = &FreePixel15;
    style.text.color.full      = COLOR_WHITE;
    style.line.color.full      = COLOR_WHITE;
    style.body.main_color.full = COLOR_DARK_GRAY;
    style.body.grad_color.full = COLOR_DARK_GRAY;

    static lv_color_t canvasBuffer[SCREEN_WIDTH * SCREEN_HEIGHT];
    static lv_color_t tempCanvasBuffer[SCREEN_WIDTH * SCREEN_HEIGHT];
    lv_canvas_set_buffer(homeContainerCanvas_, (void*)tempCanvasBuffer, SCREEN_HEIGHT, SCREEN_WIDTH, LV_IMG_CF_TRUE_COLOR);
    lv_obj_align(homeContainerCanvas_, nullptr, LV_ALIGN_IN_TOP_LEFT, 0, 0);

    lv_canvas_fill_bg(homeContainerCanvas_, LV_COLOR_BLACK);

    if (showLocalTime_ || currentActivity_ != FitnessActivities::None)
    {
        style.text.font = &Roboto18;
        lv_canvas_draw_text(homeContainerCanvas_, 0, 24, SCREEN_HEIGHT, &style, hourText, LV_LABEL_ALIGN_CENTER);
        lv_canvas_draw_text(homeContainerCanvas_, 0, 49, SCREEN_HEIGHT, &style, minuteText, LV_LABEL_ALIGN_CENTER);

        lv_point_t points[] = {{13, 74}, {SCREEN_HEIGHT - 13, 74}};
        lv_canvas_draw_line(homeContainerCanvas_, points, 2, &style);

        if (currentActivity_ == FitnessActivities::None)
        {
            style.text.font = &FreePixel15;
            lv_canvas_draw_text(homeContainerCanvas_, 0, 83, SCREEN_HEIGHT, &style, dayOfWeekText, LV_LABEL_ALIGN_CENTER);

            uint8_t buffer[3];
            ToString(dateTime.GetDay(), buffer, sizeof(buffer));
            lv_canvas_draw_text(homeContainerCanvas_, 0, 98, SCREEN_HEIGHT, &style, (const char*)buffer, LV_LABEL_ALIGN_CENTER);
        }
    }
    else
    {
        char offsetText[4];
        sprintf(offsetText, "%+2d", (int)remoteOffset.TotalHours());
        style.text.font = &FreePixel15;
        lv_canvas_draw_text(homeContainerCanvas_, 0, 30, SCREEN_HEIGHT, &style, offsetText, LV_LABEL_ALIGN_CENTER);
        style.text.font = &Roboto18;
        lv_canvas_draw_text(homeContainerCanvas_, 0, 55, SCREEN_HEIGHT, &style, hourText, LV_LABEL_ALIGN_CENTER);
        lv_canvas_draw_text(homeContainerCanvas_, 0, 80, SCREEN_HEIGHT, &style, minuteText, LV_LABEL_ALIGN_CENTER);
    }

    for (int x = 14; x < SCREEN_WIDTH - 14; x++)
    {
        for (int y = 0; y < SCREEN_HEIGHT; y++)
        {
            canvasBuffer[x + y * SCREEN_WIDTH] = tempCanvasBuffer[(SCREEN_HEIGHT - y - 1) + x * SCREEN_HEIGHT];
        }
    }
    // Draw boxes
    for (int x = 0; x < 14; x++)
    {
        for (int y = 0; y < SCREEN_HEIGHT; y++)
        {
            canvasBuffer[x + y * SCREEN_WIDTH].full                    = COLOR_DARKEST_GRAY;
            canvasBuffer[SCREEN_WIDTH - x - 1 + y * SCREEN_WIDTH].full = COLOR_DARKEST_GRAY;
        }
    }

    lv_canvas_set_buffer(homeContainerCanvas_, (void*)canvasBuffer, SCREEN_WIDTH, SCREEN_HEIGHT, LV_IMG_CF_TRUE_COLOR);

    // Battery bar
    // TODO: Do not update battery level here, it should be done periodically
    UpdateStatusIcons();
    float percent = batteryLevel_ / 100.0f;

    style.body.main_color.full = percent > 0.15f ? COLOR_WHITE : COLOR_RED;
    style.body.grad_color.full = style.body.main_color.full;

    int barStart  = SCREEN_HEIGHT - 23;
    int barLength = (int)((SCREEN_HEIGHT - 47) * percent + 0.5f);
    if (barLength < 2)
    {
        barLength = 2;
    }

    lv_canvas_draw_rect(homeContainerCanvas_, SCREEN_WIDTH - 8, barStart - barLength, 3, barLength, &style);
}

void Launcher::RotateHomeCanvas()
{
}

void Launcher::Draw()
{
    lv_area_t area;
    lv_obj_t* scrollable = lv_page_get_scrl(page_);
    lv_obj_get_coords(scrollable, &area);
    int16_t scroll = area.x1;
#ifdef SIMULATOR
    scroll -= SIMULATOR_PADDING;
#endif

    uint32_t x, y;

    if (TouchGetTouchPosition(&x, &y))
    {
        delay_ = DateTime::Now();
    }
    else
    {
        if (returnToClock_  || ((DateTime::Now() - delay_).TotalMilliseconds() > returnToMainDelay_))
        {
            delay_ = DateTime::Now();
            lv_page_scroll_hor(page_, -scroll);
            lv_obj_invalidate(page_);
            returnToClock_ = false;
        }
    }
}

void Launcher::BackLongPressed()
{
    appSystem_.StartApp(PowerMenuApp_);
}

void Launcher::BackPressed()
{
    returnToClock_ = true;
    appSystem_.StartApp(*this);
}

void Launcher::UpdateStatusIcons()
{
    PowerService* powerService = appSystem_.GetPowerService();
    batteryLevel_              = powerService->GetBatteryLevel();
    bool charging              = powerService->IsCharging();
    lv_obj_set_hidden(chargingSymbol_, !charging);
    lv_obj_set_hidden(powerSavingSymbol_, charging || !powerService->IsPowerSavingActive());
    lv_obj_set_hidden(flightModeSymbol_, !powerService->IsFlightModeActive());
}

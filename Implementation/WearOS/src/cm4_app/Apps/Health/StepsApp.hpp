#ifndef STEPSAPP_H
#define STEPSAPP_H

#include "Apps/Abstract/SingleValueBaseApp.hpp"
#include "Services/SchedulingService.hpp"

class StepsApp : public SingleValueBaseApp, public ISchedulable
{
public:
    StepsApp() : SingleValueBaseApp(AppIds::Steps) {}

    void SetUp() override;
    void Update() override;

    void Notify() override;
private:
    void ResetTomorrow();
};

#endif /* STEPSAPP_H */

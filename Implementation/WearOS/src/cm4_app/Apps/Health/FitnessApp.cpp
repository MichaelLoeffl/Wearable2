#include "FitnessApp.hpp"
#include "AppSystemM4.hpp"
#include "Apps/Design/Design.h"
#include "Apps/Icons/Icons.h"
#include "Drivers/Touch.h"
#include "Common/Tools.hpp"
#include <cstdio>
#include <stdint.h>
#include "../../lvgl/src/lv_objx/lv_btn.h"
#include "../../lvgl/src/lv_objx/lv_btnm.h"
#include "../../lvgl/src/lv_objx/lv_canvas.h"
#include "../../lvgl/src/lv_objx/lv_img.h"
#include "../../lvgl/src/lv_objx/lv_imgbtn.h"
#include "../../lvgl/src/lv_objx/lv_label.h"
#include "../../lvgl/src/lv_objx/lv_page.h"

#define PAGER_HEIGHT (5)
#define PAGER_DISTANCE (10)

FitnessApp* currentFitnessApp_;

static void SelectActivityClick(lv_obj_t* btn, lv_event_t event)
{
    if (event != LV_EVENT_CLICKED)
    {
        return;
    }

    FitnessActivities activity = (FitnessActivities)(int64_t)(void*)lv_obj_get_user_data(btn);
    currentFitnessApp_->PrepareActivity(activity);
}

static void StartActivityClick()
{
    currentFitnessApp_->StartActivity();
}

static void ActionViewContentClick()
{
    if (currentFitnessApp_->CurrentMode != ViewModes::Workout)
    {
        return;
    }

    currentFitnessApp_->SetTappedState();
}

static void ActionViewPrimaryClick()
{
    currentFitnessApp_->PauseContinueActivity();
}

static void ActionViewSecondaryClick()
{
    currentFitnessApp_->StopActivity();
}

static void EnergyActionViewClick()
{
    currentFitnessApp_->ToggleEnergyView();
}

static void AltitudeActionViewClick()
{
    if (currentFitnessApp_->CurrentMode == ViewModes::Summary)
    {
        currentFitnessApp_->ToggleAltitudeView();
    }
    else
    {
        ActionViewContentClick();
    }
}

void FitnessApp::Initialize()
{
    currentFitnessApp_ = this;
    fitnessService_    = appSystem_.GetFitnessService();
    fitnessService_->LoadSummary();
}

void FitnessApp::Activated()
{
    if (CurrentMode != ViewModes::Workout)
    {
        CurrentMode = ViewModes::ActivitySelect;
    }
    if (currentActivity_ == FitnessActivities::Recent)
    {
        currentActivity_ = FitnessActivities::None;
    }
    actionViewState_ = ActionViewStates::Regular;

    pageScrollable_ = lv_page_get_scrl(page_);

    activitySelectContainer_ = lv_cont_create(page_, nullptr);
    lv_page_glue_obj(activitySelectContainer_, true);
    lv_obj_align(activitySelectContainer_, page_, LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_set_size(activitySelectContainer_, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_obj_set_pos(activitySelectContainer_, 0, 0);

    ActivitySummary summary = fitnessService_->GetSummary();

    int activityCount       = (int)FitnessActivities::Count;
    activitySelectPageSize_ = 0;
    for (int i = 0; i < activityCount; i++)
    {
        FitnessActivities activity = i > 0 ? (FitnessActivities)i : FitnessActivities::Recent;

        if (activity == FitnessActivities::Recent && summary.Activity == FitnessActivities::None)
        {
            continue;
        }

        lv_obj_t* imageButton = lv_imgbtn_create(activitySelectContainer_, nullptr);
        lv_img_dsc_t* image   = Tools::GetFitnessActivityIcon(activity);
        if (image == nullptr)
        {
            continue;
        }

        lv_imgbtn_set_src(imageButton, LV_BTN_STATE_REL, image);
        lv_imgbtn_set_src(imageButton, LV_BTN_STATE_PR, image);
        lv_imgbtn_set_src(imageButton, LV_BTN_STATE_TGL_REL, image);
        lv_imgbtn_set_src(imageButton, LV_BTN_STATE_TGL_PR, image);
        lv_obj_set_event_cb(imageButton, SelectActivityClick);
        lv_imgbtn_set_toggle(imageButton, true);
        lv_obj_set_size(imageButton, image->header.w, image->header.h);
        lv_obj_set_pos(imageButton, activitySelectPageSize_ + APP_ICON_MARGIN / 2, (SCREEN_HEIGHT - image->header.h) / 2);
        lv_btn_set_fit(imageButton, true);
        lv_page_glue_obj(imageButton, true);
        lv_btn_set_layout(imageButton, LV_LAYOUT_OFF);
        lv_obj_set_user_data(imageButton, (void*)activity);

        activitySelectPageSize_ += APP_ICON_MARGIN + image->header.w;
    }

    lv_obj_set_size(activitySelectContainer_, activitySelectPageSize_, SCREEN_HEIGHT);

    summaryActionView_.Create(page_);
    summaryActionView_.SetButtons(ActionViewButtons::Play);
    summaryActionView_.SetPrimaryButtonAction(StartActivityClick);

    timeActionView_.Create(page_);
    timeActionView_.SetTitle("--:--");
    timeActionView_.SetImage(&WatchLocalAppIconSimple);
    timeActionView_.SetContent("00:00:00");
    timeActionView_.SetContentClickAction(ActionViewContentClick);
    timeActionView_.SetPrimaryButtonAction(ActionViewPrimaryClick);
    timeActionView_.SetSecondaryButtonAction(ActionViewSecondaryClick);

    heartrateActionView_.Create(page_);
    heartrateActionView_.SetTitle("--- bpm");
    heartrateActionView_.SetImage(&HeartrateAppIconSimple);
    heartrateActionView_.SetContent("00:00:00");
    heartrateActionView_.SetContentClickAction(ActionViewContentClick);
    heartrateActionView_.SetPrimaryButtonAction(ActionViewPrimaryClick);
    heartrateActionView_.SetSecondaryButtonAction(ActionViewSecondaryClick);

    stepsActionView_.Create(page_);
    stepsActionView_.SetTitle("0");
    stepsActionView_.SetImage(&PedometerAppIconSimple);
    stepsActionView_.SetContent("00:00:00");
    stepsActionView_.SetContentClickAction(ActionViewContentClick);
    stepsActionView_.SetPrimaryButtonAction(ActionViewPrimaryClick);
    stepsActionView_.SetSecondaryButtonAction(ActionViewSecondaryClick);

    altitudeUpActionView_.Create(page_);
    altitudeUpActionView_.SetTitle("0 m");
    altitudeUpActionView_.SetImage(&AltitudeUpAppIconSimple);
    altitudeUpActionView_.SetContent("00:00:00");
    altitudeUpActionView_.SetContentClickAction(AltitudeActionViewClick);
    altitudeUpActionView_.SetPrimaryButtonAction(ActionViewPrimaryClick);
    altitudeUpActionView_.SetSecondaryButtonAction(ActionViewSecondaryClick);

    altitudeDownActionView_.Create(page_);
    altitudeDownActionView_.SetTitle("0 m");
    altitudeDownActionView_.SetImage(&AltitudeDownAppIconSimple);
    altitudeDownActionView_.SetContent("00:00:00");
    altitudeDownActionView_.SetContentClickAction(AltitudeActionViewClick);
    altitudeDownActionView_.SetPrimaryButtonAction(ActionViewPrimaryClick);
    altitudeDownActionView_.SetSecondaryButtonAction(ActionViewSecondaryClick);

    energyActionView_.Create(page_);
    energyActionView_.SetTitle("kcal");
    energyActionView_.SetImage(&CaloriesAppIconSimple);
    energyActionView_.SetContent("0");
    energyActionView_.SetContentClickAction(EnergyActionViewClick);

    uint8_t pos;
    allComponents_[pos = 0]                                        = &timeActionView_;
    allComponents_[++pos]                                          = &heartrateActionView_;
    allComponents_[++pos]                                          = &stepsActionView_;
    allComponents_[++pos]                                          = &altitudeUpActionView_;
    allComponents_[++pos]                                          = &altitudeDownActionView_;
    activityComponents_[(int)FitnessActivities::Running][pos = 0]  = &timeActionView_;
    activityComponents_[(int)FitnessActivities::Running][++pos]    = &heartrateActionView_;
    activityComponents_[(int)FitnessActivities::Running][++pos]    = &stepsActionView_;
    activityComponents_[(int)FitnessActivities::Running][++pos]    = &altitudeUpActionView_;
    activityComponents_[(int)FitnessActivities::Running][++pos]    = &altitudeDownActionView_;
    activityComponents_[(int)FitnessActivities::Walking][pos = 0]  = &timeActionView_;
    activityComponents_[(int)FitnessActivities::Walking][++pos]    = &heartrateActionView_;
    activityComponents_[(int)FitnessActivities::Walking][++pos]    = &stepsActionView_;
    activityComponents_[(int)FitnessActivities::Walking][++pos]    = &altitudeUpActionView_;
    activityComponents_[(int)FitnessActivities::Walking][++pos]    = &altitudeDownActionView_;
    activityComponents_[(int)FitnessActivities::Cycling][pos = 0]  = &timeActionView_;
    activityComponents_[(int)FitnessActivities::Cycling][++pos]    = &heartrateActionView_;
    activityComponents_[(int)FitnessActivities::Cycling][++pos]    = nullptr; // &stepsActionView_;
    activityComponents_[(int)FitnessActivities::Cycling][++pos]    = &altitudeUpActionView_;
    activityComponents_[(int)FitnessActivities::Cycling][++pos]    = &altitudeDownActionView_;
    activityComponents_[(int)FitnessActivities::Strength][pos = 0] = &timeActionView_;
    activityComponents_[(int)FitnessActivities::Strength][++pos]   = &heartrateActionView_;
    activityComponents_[(int)FitnessActivities::Strength][++pos]   = nullptr; // &stepsActionView_;
    activityComponents_[(int)FitnessActivities::Strength][++pos]   = nullptr; // &altitudeUpActionView_;
    activityComponents_[(int)FitnessActivities::Strength][++pos]   = nullptr; // &altitudeDownActionView_;
    activityComponents_[(int)FitnessActivities::Yoga][pos = 0]     = &timeActionView_;
    activityComponents_[(int)FitnessActivities::Yoga][++pos]       = &heartrateActionView_;
    activityComponents_[(int)FitnessActivities::Yoga][++pos]       = nullptr; // &stepsActionView_;
    activityComponents_[(int)FitnessActivities::Yoga][++pos]       = nullptr; // &altitudeUpActionView_;
    activityComponents_[(int)FitnessActivities::Yoga][++pos]       = nullptr; // &altitudeDownActionView_;
    activityComponents_[(int)FitnessActivities::Other][pos = 0]    = &timeActionView_;
    activityComponents_[(int)FitnessActivities::Other][++pos]      = &heartrateActionView_;
    activityComponents_[(int)FitnessActivities::Other][++pos]      = &stepsActionView_;
    activityComponents_[(int)FitnessActivities::Other][++pos]      = &altitudeUpActionView_;
    activityComponents_[(int)FitnessActivities::Other][++pos]      = &altitudeDownActionView_;

    // Pager
    static lv_color_t pagerCanvasBuffer[SCREEN_WIDTH * PAGER_HEIGHT];
    pager_ = lv_canvas_create(page_, nullptr);
    lv_canvas_set_buffer(pager_, (void*)pagerCanvasBuffer, SCREEN_WIDTH, PAGER_HEIGHT, LV_IMG_CF_TRUE_COLOR);
    lv_obj_align(pager_, nullptr, LV_ALIGN_IN_BOTTOM_LEFT, 0, -3);

    UpdateView();
    if (CurrentMode == ViewModes::Workout)
    {
        UpdateSensorData();
    }
}

void FitnessApp::PrepareActivity(FitnessActivities activity)
{
    currentActivity_ = activity;
    CurrentMode      = activity == FitnessActivities::Recent ? ViewModes::Summary : ViewModes::StartActivity;

    UpdateView();
}

void FitnessApp::UpdatePager(uint8_t activePage)
{
    lv_canvas_fill_bg(pager_, LV_COLOR_BLACK);

    uint8_t marginLeft = (SCREEN_WIDTH - pageCount_ * PAGER_DISTANCE) / 2;
    for (uint8_t i = 0; i < pageCount_; i++)
    {
        lv_canvas_draw_img(pager_, marginLeft + PAGER_DISTANCE * i, 0, i == activePage ? &PagerActiveStatus : &PagerInactiveStatus, &lv_style_plain);
    }
}

void FitnessApp::UpdateActionViews()
{
    ActionViewStyles style;
    ActionViewButtons primaryButton;
    ActionViewButtons secondaryButton = ActionViewButtons::Stop;
    switch (actionViewState_)
    {
        case ActionViewStates::Tapped:
            style         = ActionViewStyles::IconSingleActionHugeText;
            primaryButton = ActionViewButtons::Pause;
            break;
        case ActionViewStates::Paused:
            style         = ActionViewStyles::IconDoubleActionHugeText;
            primaryButton = ActionViewButtons::Play;
            break;
        default:
            style = ActionViewStyles::IconNoActionHugeText;
            primaryButton = ActionViewButtons::None;
            break;
    }

    // Hide views used in summary
    energyActionView_.Show(false);

    pageCount_ = 0;
    for (uint8_t i = 0; i < MAX_ACTIVITY_COMPONENTS; i++)
    {
        bool visible = CurrentMode == ViewModes::Workout && activityComponents_[(int)currentActivity_][i] != nullptr;
        allComponents_[i]->Show(visible);
        if (visible)
        {
            allComponents_[i]->SetStyle(style);
            allComponents_[i]->SetButtons(primaryButton, secondaryButton);
            allComponents_[i]->SetLeft(pageCount_ * SCREEN_WIDTH);

            pageCount_++;
        }
    }

    UpdatePager(0);
    SetPageSize(pageCount_ * SCREEN_WIDTH);
}

void FitnessApp::UpdateView()
{
    lv_area_t area;
    lv_obj_get_coords(pageScrollable_, &area);
    int16_t scroll = area.x1;
#ifdef SIMULATOR
    scroll -= SIMULATOR_PADDING;
#endif
    lv_page_scroll_hor(page_, -scroll);

    lv_obj_set_hidden(activitySelectContainer_, CurrentMode != ViewModes::ActivitySelect);
    lv_obj_set_hidden(pager_, CurrentMode != ViewModes::Workout && CurrentMode != ViewModes::Summary);
    summaryActionView_.Show(CurrentMode == ViewModes::StartActivity || CurrentMode == ViewModes::Summary);

    switch (CurrentMode)
    {
        case ViewModes::ActivitySelect:
            AdjustRotation(true);
            SetPageSize(activitySelectPageSize_);
            break;
        case ViewModes::StartActivity:
        case ViewModes::Summary:
            AdjustRotation();
            SetPageSize(SCREEN_WIDTH);
            break;
        case ViewModes::Workout:
        default:
            AdjustRotation();
            break;
    }

    UpdateActionViews();

    if (CurrentMode == ViewModes::StartActivity)
    {
        summaryActionView_.SetStyle(ActionViewStyles::IconSingleActionHugeText);
        summaryActionView_.SetContent("00:00:00");
    }
    else if (CurrentMode == ViewModes::Summary)
    {
        summaryActionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
    }

    FitnessActivities activity = CurrentMode == ViewModes::Summary ? fitnessService_->GetSummary().Activity : currentActivity_;
    summaryActionView_.SetImage(Tools::GetFitnessActivitySmallIcon(activity));
    // TODO: This is ugly but ok as long as localization is not yet implemented
    switch (activity)
    {
        case FitnessActivities::Running:
            summaryActionView_.SetTitle("Running");
            break;
        case FitnessActivities::Walking:
            summaryActionView_.SetTitle("Walking");
            break;
        case FitnessActivities::Cycling:
            summaryActionView_.SetTitle("Cycling");
            break;
        case FitnessActivities::Strength:
            summaryActionView_.SetTitle("Strength");
            break;
        case FitnessActivities::Yoga:
            summaryActionView_.SetTitle("Yoga");
            break;
        case FitnessActivities::Other:
            summaryActionView_.SetTitle("Other Activity");
            break;
        default:
            summaryActionView_.SetTitle("Unknown Activity");
            break;
    }

    if (CurrentMode == ViewModes::Summary)
    {
        ShowSummary();
    }
}

void FitnessApp::StartActivity()
{
    CurrentMode = ViewModes::Workout;

    fitnessService_->StartActivity(currentActivity_);

    UpdateView();
    UpdateSensorData();
}

void FitnessApp::PauseActivity()
{
    actionViewState_ = ActionViewStates::Paused;
    UpdateActionViews();
}

void FitnessApp::ContinueActivity()
{
    actionViewState_ = ActionViewStates::Regular;
    UpdateActionViews();
}

void FitnessApp::PauseContinueActivity()
{
    switch (actionViewState_)
    {
        case ActionViewStates::Tapped:
            PauseActivity();
            break;
        case ActionViewStates::Paused:
            ContinueActivity();
            break;
        default:
            break;
    }
}

void FitnessApp::StopActivity()
{
    CurrentMode      = ViewModes::Summary;
    currentActivity_ = FitnessActivities::Recent;
    actionViewState_ = ActionViewStates::Regular;

    fitnessService_->StopActivity();

    UpdateView();
}

void FitnessApp::SetTappedState()
{
    switch (actionViewState_)
    {
        case ActionViewStates::Regular:
            actionViewState_ = ActionViewStates::Tapped;
            break;
        case ActionViewStates::Tapped:
            actionViewState_ = ActionViewStates::Regular;
            break;
        default:
            break;
    }

    UpdateActionViews();
}

void FitnessApp::Deactivated()
{
}

void FitnessApp::Shutdown()
{
}

void FitnessApp::Draw()
{
    if (CurrentMode != ViewModes::Workout && CurrentMode != ViewModes::Summary)
    {
        return;
    }

    lv_area_t area;
    lv_obj_get_coords(pageScrollable_, &area);
    int16_t scroll = area.x1 - SIMULATOR_PADDING;

    int snapToPage = -(int)((scroll / (float)SCREEN_WIDTH) - 0.5f);
    if (scroll != lastScroll_)
    {
        lv_obj_align(pager_, nullptr, LV_ALIGN_IN_BOTTOM_LEFT, -scroll, -3);
        if (snapToPage != currentPage_)
        {
            UpdatePager(snapToPage);
            currentPage_ = snapToPage;
        }
    }

    uint32_t x, y;
    if (TouchGetTouchPosition(&x, &y))
    {
        // Do nothing
        scrollSnappingStarted_ = false;
    }
    else
    {
        if (scrollSnappingStarted_)
        {
            return;
        }

        if (scroll != lastScroll_)
        {
            lastScroll_ = scroll;
            return;
        }
        scrollSnappingStarted_ = true;

        int newScroll = scroll - (-snapToPage * SCREEN_WIDTH);

        lv_page_scroll_hor(page_, -newScroll);
    }
}

void FitnessApp::UpdateSensorData()
{
    int32_t diff    = fitnessService_->GetDuration().TotalSeconds();
    uint8_t hours   = (uint8_t)(diff / 3600);
    uint8_t minutes = (uint8_t)((diff - hours * 3600) / 60);
    uint8_t seconds = (uint8_t)(diff - hours * 3600 - minutes * 60);

    DateTime now = DateTime::Now();

    sprintf(durationTitleText_, "%02d:%02d", now.GetHour(), now.GetMinute());
    timeActionView_.SetTitle(durationTitleText_);
    sprintf(durationText_, "%02d:%02d:%02d", hours, minutes, seconds);
    timeActionView_.SetContent(durationText_);
    heartrateActionView_.SetContent(durationText_);
    stepsActionView_.SetContent(durationText_);
    altitudeUpActionView_.SetContent(durationText_);
    altitudeDownActionView_.SetContent(durationText_);

    int heartrate = fitnessService_->GetHeartrate();
    sprintf(heartrateText_, heartrate > 300 ? "---" : "%d bpm", heartrate);
    heartrateActionView_.SetTitle(heartrateText_);

    int steps = fitnessService_->GetSteps();
    sprintf(stepsText_, "%d", steps);
    stepsActionView_.SetTitle(stepsText_);

    int altitude = fitnessService_->GetAltitudeUp();
    sprintf(altitudeUpText_, "%d m", altitude);
    altitudeUpActionView_.SetTitle(altitudeUpText_);

    altitude = fitnessService_->GetAltitudeDown();
    sprintf(altitudeDownText_, "%d m", altitude);
    altitudeDownActionView_.SetTitle(altitudeDownText_);
}

void FitnessApp::SecondElapsed()
{
    if (CurrentMode == ViewModes::Workout)
    {
        UpdateSensorData();
    }
}

void FitnessApp::ShowSummary()
{
    ActivitySummary summary = fitnessService_->GetSummary();

    showEnergyInKcal_ = true;
    showAltitudeUp_   = true;

    pageCount_ = 0;

    // Date
    sprintf(dateText_, "%04d-%02d-%02d", summary.StartTime.GetYear(), summary.StartTime.GetMonth(), summary.StartTime.GetDay());
    summaryActionView_.SetContent(dateText_);
    summaryActionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
    summaryActionView_.SetLeft(pageCount_ * SCREEN_WIDTH);
    summaryActionView_.Show(true);
    pageCount_ += 1;

    // Duration
    int32_t diff    = summary.Duration.TotalSeconds();
    uint8_t hours   = (uint8_t)(diff / 3600);
    uint8_t minutes = (uint8_t)((diff - hours * 3600) / 60);
    uint8_t seconds = (uint8_t)((diff - hours * 3600 - minutes * 60));
    sprintf(durationText_, "%02d:%02d:%02d", hours, minutes, seconds);
    timeActionView_.SetTitle("Duration");
    timeActionView_.SetContent(durationText_);
    timeActionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
    timeActionView_.SetLeft(pageCount_ * SCREEN_WIDTH);
    timeActionView_.Show(true);
    pageCount_ += 1;

    // Energy
    sprintf(energyText, "%d", (int)(summary.EnergyJoule * 4.184));
    energyActionView_.SetContent(energyText);
    energyActionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
    energyActionView_.SetLeft(pageCount_ * SCREEN_WIDTH);
    energyActionView_.Show(true);
    pageCount_ += 1;

    // Heartrate
    heartrateActionView_.SetTitle("Heartrate");
    sprintf(heartrateText_, "%d avg", (int)summary.HeartrateAverage);
    heartrateActionView_.SetContent(heartrateText_);
    heartrateActionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
    heartrateActionView_.SetLeft(pageCount_ * SCREEN_WIDTH);
    heartrateActionView_.Show(true);
    pageCount_ += 1;

    // Steps
    if (fitnessService_->HasSteps(summary.Activity))
    {
        stepsActionView_.SetTitle("Steps");
        sprintf(stepsText_, "%d", (int)summary.Steps);
        stepsActionView_.SetContent(stepsText_);
        stepsActionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
        stepsActionView_.SetLeft(pageCount_ * SCREEN_WIDTH);
        stepsActionView_.Show(true);
        pageCount_ += 1;
    }

    // Altitude
    if (fitnessService_->HasAltitude(summary.Activity))
    {
        altitudeUpActionView_.SetTitle("Altitude");
        sprintf(altitudeUpText_, "%d m", (int)summary.AltitudeUp);
        altitudeUpActionView_.SetContent(altitudeUpText_);
        altitudeUpActionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
        altitudeUpActionView_.SetLeft(pageCount_ * SCREEN_WIDTH);
        altitudeUpActionView_.Show(true);

        altitudeDownActionView_.SetTitle("Altitude");
        sprintf(altitudeDownText_, "%d m", (int)summary.AltitudeDown);
        altitudeDownActionView_.SetContent(altitudeDownText_);
        altitudeDownActionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
        altitudeDownActionView_.SetLeft(pageCount_ * SCREEN_WIDTH);
        altitudeDownActionView_.Show(false);
        pageCount_ += 1;
    }

    UpdatePager(0);
    SetPageSize(pageCount_ * SCREEN_WIDTH);
}

void FitnessApp::ToggleEnergyView()
{
    showEnergyInKcal_       = !showEnergyInKcal_;
    ActivitySummary summary = fitnessService_->GetSummary();

    energyActionView_.SetTitle(showEnergyInKcal_ ? "kcal" : "kJ");
    sprintf(energyText, "%d", showEnergyInKcal_ ? (int)(summary.EnergyJoule * 4.184) : summary.EnergyJoule);
    energyActionView_.SetContent(energyText);
}

void FitnessApp::ToggleAltitudeView()
{
    showAltitudeUp_ = !showAltitudeUp_;

    altitudeUpActionView_.Show(showAltitudeUp_);
    altitudeDownActionView_.Show(!showAltitudeUp_);
}

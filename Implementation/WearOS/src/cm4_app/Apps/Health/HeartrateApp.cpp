#include "HeartrateApp.hpp"
#include <cstdio>
#include <stdint.h>

// Should ne updated at least every 100ms, otherwise data loss in ADC
#define UPDATE_EVERY_N_FRAMES (50 / MS_PER_FRAME)

void HeartrateApp::SetUp()
{
    actionView_.SetTitle("Heartrate");
    actionView_.SetButtons(ActionViewButtons::None);
    actionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
}

void HeartrateApp::Activated()
{
    SingleValueBaseApp::Activated();

    HeartrateStartMeasurement();
}

void HeartrateApp::Deactivated()
{
    SingleValueBaseApp::Deactivated();

    HeartrateEndMeasurement();
}

void HeartrateApp::Update()
{
    if (heartrateValue_ == INVALID_HEARTRATE)
    {
        actionView_.SetContent("...");
    }
    else
    {
        sprintf(valueText_, "%d", heartrateValue_);
        actionView_.SetContent(valueText_);
    }
}

void HeartrateApp::SecondElapsed()
{
    heartrateValue_ = HeartrateUpdate();
    Update();
}

#ifndef FITNESSAPP_HPP
#define FITNESSAPP_HPP

#include "Apps/Components/ActionView.h"
#include "Common/App.hpp"
#include "Common/FitnessActivities.hpp"
#include "Services/FitnessService.hpp"

#define MAX_ACTIVITY_COMPONENTS (5)

enum class ViewModes
{
    ActivitySelect = 0,
    StartActivity,
    Summary,
    Workout
};

enum class ActionViewStates
{
    Regular = 0,
    Tapped,
    Paused
};

class FitnessApp : public App
{
private:
    ActionView summaryActionView_;

    ActionView timeActionView_;
    ActionView heartrateActionView_;
    ActionView stepsActionView_;
    ActionView altitudeUpActionView_;
    ActionView altitudeDownActionView_;
    ActionView energyActionView_;

    FitnessActivities currentActivity_ = FitnessActivities::None;
    ActionViewStates actionViewState_  = ActionViewStates::Regular;

    lv_obj_t* activitySelectContainer_  = nullptr;
    lv_obj_t* pager_                    = nullptr;
    lv_obj_t* pageScrollable_           = nullptr;

    int activitySelectPageSize_ = 0;
    bool scrollSnappingStarted_ = false;
    int lastScroll_             = 0;
    uint8_t currentPage_        = 0;
    uint8_t pageCount_          = 0;

    FitnessService* fitnessService_ = nullptr;

    void UpdateView();
    void UpdateActionViews();
    void UpdatePager(uint8_t activePage);
    void UpdateSensorData();

    void ShowSummary();

    ActionView* activityComponents_[(int)FitnessActivities::Count][MAX_ACTIVITY_COMPONENTS] = {nullptr};
    ActionView* allComponents_[MAX_ACTIVITY_COMPONENTS]                                     = {nullptr};

    bool showEnergyInKcal_ = true;
    bool showAltitudeUp_   = true;

    char dateText_[16]          = {0};
    char heartrateText_[16]     = {0};
    char stepsText_[16]         = {0};
    char durationTitleText_[10] = {0};
    char durationText_[12]      = {0};
    char altitudeUpText_[16]    = {0};
    char altitudeDownText_[16]  = {0};
    char energyText[16]         = {0};

public:
    FitnessApp() : App(AppIds::Fitness) {}

    ViewModes CurrentMode = ViewModes::ActivitySelect;

    void PrepareActivity(FitnessActivities activity);
    void StartActivity();
    void PauseContinueActivity();
    void PauseActivity();
    void ContinueActivity();
    void StopActivity();

    void SetTappedState();

    void Initialize() override;
    void Activated() override;
    void Deactivated() override;
    void Shutdown() override;
    void Draw() override;

    void ToggleEnergyView();
    void ToggleAltitudeView();

    void SecondElapsed() override;
};

#endif /* FITNESSAPP_HPP */

#include "StepsApp.hpp"
#include "AppSystemM4.hpp"
#include "Common/Timespan.hpp"
#include "Drivers/StepsDriver.h"
#include <cstdio>

void StepsApp::SetUp()
{
    actionView_.SetTitle("Steps");
    actionView_.SetButtons(ActionViewButtons::None);
    actionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);

    ResetTomorrow();
}

void StepsApp::Notify()
{
    HAL_StepCounter_Reset();
    ResetTomorrow();
}

void StepsApp::ResetTomorrow()
{
    DateTime dateTime = DateTime::StartOfDay() + Timespan(24, 0, 0, 0);
    Schedule(dateTime);
}

void StepsApp::Update()
{
    int value = HAL_StepCounter_GetCountedSteps();
    sprintf(valueText_, "%d", value);
    actionView_.SetContent(valueText_);
}

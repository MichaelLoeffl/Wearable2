#ifndef HEARTRATEAPP_H
#define HEARTRATEAPP_H

#include "Apps/Abstract/SingleValueBaseApp.hpp"
#include "Drivers/HeartrateDriver.h"

class HeartrateApp : public SingleValueBaseApp
{
private:
    int heartrateValue_ = INVALID_HEARTRATE;

public:
    HeartrateApp() : SingleValueBaseApp(AppIds::Heartrate) {}

    void Activated() override;
    void Deactivated() override;

    void SetUp() override;
    void Update() override;
    void SecondElapsed() override;
};

#endif /* HEARTRATEAPP_H */

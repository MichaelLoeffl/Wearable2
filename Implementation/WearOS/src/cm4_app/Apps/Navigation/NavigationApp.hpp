#ifndef NAVIGATIONAPP_H_65E316
#define NAVIGATIONAPP_H_65E316

#include "Common/App.hpp"

class NavigationApp : public App
{
private:
    lv_obj_t* container_;
    lv_obj_t* titleLabel_;
    lv_obj_t* contentLabel_;
    lv_obj_t* image_;

public:
    NavigationApp() : App(AppIds::Navigation) {}
    virtual ~NavigationApp() {}

    lv_img_dsc_t* GetAppImage()  override;

protected:
    virtual void Initialize();

    void Restore() override;
    void Activated() override;
    void Deactivated() override;
    void Shutdown() override;
    void Draw() override;

    void SecondElapsed() override;
};

#endif /* NAVIGATIONAPP_H_65E316 */

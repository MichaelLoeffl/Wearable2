#include "NavigationApp.hpp"
#include "AppSystemM4.hpp"
#include "Apps/Design/Design.h"
#include "Apps/Icons/Icons.h"
#include "Drivers/Core.h"
#include "Drivers/KomootService.h"
#include "Services/NavigationDirections.hpp"
#include "Services/NavigationService.hpp"
#include "stdio.h"
#include <cstdlib>
#include "../../lvgl/src/lv_objx/lv_btn.h"
#include "../../lvgl/src/lv_objx/lv_imgbtn.h"
#include "../../lvgl/src/lv_objx/lv_label.h"
#include "../../lvgl/src/lv_objx/lv_page.h"


#define TEXT_LEFT (50)

extern lv_font_t Roboto18;

lv_img_dsc_t* NavigationApp::GetAppImage()
{
    return &NavigationAppIcon;
}

void NavigationApp::Initialize()
{
}

void NavigationApp::Restore()
{
}

void NavigationApp::Activated()
{
    container_ = lv_cont_create(page_, nullptr);
    lv_obj_align(container_, page_, LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_set_size(container_, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_obj_set_pos(container_, 0, 0);
    lv_obj_set_user_data(container_, this);
    // lv_obj_set_event_cb(container_, ItemClick);

    image_ = lv_img_create(container_, nullptr);
    lv_img_set_src(image_, &GotoStartNavigation);
    lv_obj_set_pos(image_, 0, (SCREEN_HEIGHT - LeftNavigation.header.h) / 2);

    titleLabel_ = lv_label_create(container_, nullptr);
    lv_obj_set_pos(titleLabel_, TEXT_LEFT, 10);
    lv_label_set_static_text(titleLabel_, "");

    static lv_style_t hugeStyle;
    lv_style_copy(&hugeStyle, &lv_style_plain);
    hugeStyle.text.font       = &Roboto18;
    hugeStyle.text.color.full = COLOR_WHITE;
    hugeStyle.line.color.full = COLOR_WHITE;
    contentLabel_             = lv_label_create(container_, nullptr);
    lv_label_set_style(contentLabel_, LV_LABEL_STYLE_MAIN, &hugeStyle);
    lv_obj_set_pos(contentLabel_, TEXT_LEFT, SCREEN_HEIGHT - 25);
    lv_label_set_static_text(contentLabel_, "");

    SecondElapsed();
}

void NavigationApp::Deactivated()
{
}

void NavigationApp::Shutdown()
{
}

CFUNC void BleStartScan();

void NavigationApp::SecondElapsed()
{
    if (!KomootConnectionCheck(false))
    {
        //BleStartScan();
    }

    NavigationService* navigationService = appSystem_.GetNavigationService();
    bool isActive                        = navigationService->IsActive();
    int distanceNext                     = navigationService->GetDistance();
    NavigationDirections direction       = navigationService->GetDirection();

    DeviceUserIntervention(3000);

    if (!isActive)
    {
        lv_label_set_static_text(titleLabel_, "Start komoot");
        lv_label_set_static_text(contentLabel_, "---");
        return;
    }

    lv_img_dsc_t* imageFinal;
    switch (direction)
    {
        case NavigationDirections::Straight:
            imageFinal = &StraightNavigation;
            lv_label_set_static_text(titleLabel_, "Go Straight");
            break;
        case NavigationDirections::Start:
            imageFinal = &StartNavigation;
            lv_label_set_static_text(titleLabel_, "Start");
            break;
        case NavigationDirections::Finish:
            imageFinal = &FinishNavigation;
            lv_label_set_static_text(titleLabel_, "Arrived");
            break;
        case NavigationDirections::SlightLeft:
            imageFinal = &SlightLeftNavigation;
            lv_label_set_static_text(titleLabel_, "Slight Left");
            break;
        case NavigationDirections::Left:
            imageFinal = &LeftNavigation;
            lv_label_set_static_text(titleLabel_, "Left");
            break;
        case NavigationDirections::SharpLeft:
            imageFinal = &SharpLeftNavigation;
            lv_label_set_static_text(titleLabel_, "Sharp Left");
            break;
        case NavigationDirections::SharpRight:
            imageFinal = &SharpRightNavigation;
            lv_label_set_static_text(titleLabel_, "Sharp Right");
            break;
        case NavigationDirections::Right:
            imageFinal = &RightNavigation;
            lv_label_set_static_text(titleLabel_, "Right");
            break;
        case NavigationDirections::SlightRight:
            imageFinal = &SlightRightNavigation;
            lv_label_set_static_text(titleLabel_, "Slight Right");
            break;
        case NavigationDirections::ForkRight:
            imageFinal = &ForkRightNavigation;
            lv_label_set_static_text(titleLabel_, "Fork Right");
            break;
        case NavigationDirections::ForkLeft:
            imageFinal = &ForkLeftNavigation;
            lv_label_set_static_text(titleLabel_, "Fork Left");
            break;
        case NavigationDirections::UTurn:
            imageFinal = &UTurnNavigation;
            lv_label_set_static_text(titleLabel_, "U-Turn");
            break;
        case NavigationDirections::RoundaboutExitLeft:
            imageFinal = &RoundaboutExitLeftNavigation;
            lv_label_set_static_text(titleLabel_, "Exit Left");
            break;
        case NavigationDirections::RoundaboutExitRight:
            imageFinal = &RoundaboutExitRightNavigation;
            lv_label_set_static_text(titleLabel_, "Exit Right");
            break;
        case NavigationDirections::RoundaboutCounterClockwiseExit1of1:
            imageFinal = &RoundaboutCounterClockwiseExit1of1Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 1 of 1");
            break;
        case NavigationDirections::RoundaboutCounterClockwiseExit1of2:
            imageFinal = &RoundaboutCounterClockwiseExit1of2Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 1 of 2");
            break;
        case NavigationDirections::RoundaboutCounterClockwiseExit1of3:
            imageFinal = &RoundaboutCounterClockwiseExit1of3Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 1 of 3");
            break;
        case NavigationDirections::RoundaboutCounterClockwiseExit2of2:
            imageFinal = &RoundaboutCounterClockwiseExit2of2Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 2 of 2");
            break;
        case NavigationDirections::RoundaboutCounterClockwiseExit2of3:
            imageFinal = &RoundaboutCounterClockwiseExit2of3Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 2 of 3");
            break;
        case NavigationDirections::RoundaboutCounterClockwiseExit3of3:
            imageFinal = &RoundaboutCounterClockwiseExit3of3Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 3 of 3");
            break;
        case NavigationDirections::RoundaboutClockwiseExit1of1:
            imageFinal = &RoundaboutClockwiseExit1of1Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 1 of 1");
            break;
        case NavigationDirections::RoundaboutClockwiseExit1of2:
            imageFinal = &RoundaboutClockwiseExit1of2Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 1 of 2");
            break;
        case NavigationDirections::RoundaboutClockwiseExit1of3:
            imageFinal = &RoundaboutClockwiseExit1of3Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 1 of 3");
            break;
        case NavigationDirections::RoundaboutClockwiseExit2of2:
            imageFinal = &RoundaboutClockwiseExit2of2Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 2 of 2");
            break;
        case NavigationDirections::RoundaboutClockwiseExit2of3:
            imageFinal = &RoundaboutClockwiseExit2of3Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 2 of 3");
            break;
        case NavigationDirections::RoundaboutClockwiseExit3of3:
            imageFinal = &RoundaboutClockwiseExit3of3Navigation;
            lv_label_set_static_text(titleLabel_, "Exit 3 of 3");
            break;
        case NavigationDirections::RoundaboutFallback:
            imageFinal = &RoundaboutFallbackNavigation;
            lv_label_set_static_text(titleLabel_, "Roundabout Fallback");
            break;
        case NavigationDirections::OutOfRoute:
            imageFinal = &OutOfRouteNavigation;
            lv_label_set_static_text(titleLabel_, "Out of Route");
            break;
        default:
            imageFinal = &GotoStartNavigation;
            lv_label_set_static_text(titleLabel_, "");
            break;
    }

    lv_img_set_src(image_, imageFinal);

    if (direction == NavigationDirections::Start || direction == NavigationDirections::Finish)
    {
        lv_label_set_static_text(contentLabel_, "");
    }
    else
    {
        static char totalTextBuffer[11];
        sprintf(totalTextBuffer, "%d m", distanceNext);
        lv_label_set_static_text(contentLabel_, totalTextBuffer);
    }
}

void NavigationApp::Draw()
{
}

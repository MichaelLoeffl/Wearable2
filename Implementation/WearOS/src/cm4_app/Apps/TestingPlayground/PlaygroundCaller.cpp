#ifdef DEBUG

#include "PlaygroundCaller.h"
#include "AppSystemM4.hpp"
#include "PlaygroundApp.hpp"

extern PlaygroundApp PlaygroundApp_;

CFUNC void SetPlaygroundText(const char* text)
{
    PlaygroundApp_.UpdateText(text);
}

#endif
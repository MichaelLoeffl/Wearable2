#ifdef DEBUG

#ifndef PLAYGROUND_CALLER_H
#define PLAYGROUND_CALLER_H

#include "Common/Common.h"

CFUNC void SetPlaygroundText(const char* text);

#endif // PLAYGROUND_CALLER_H
#endif // DEBUG
#ifdef DEBUG

#include "PlaygroundApp.hpp"
#include "AppSystemM4.hpp"
#include "deps/BatteryDriver.h"
#include "Drivers/Display.h"
#include "PlaygroundCaller.h"
#include "Common/Utils.h"
#include <cstdio>
#include <stdint.h>

static char PlaygroundBuffer_[PLAYGROUND_BUFFER_SIZE];

void PlaygroundApp::Initialize()
{
    // Testcode can go here - Called once during system startup
}

void PlaygroundApp::SetUp()
{
    strcpy(PlaygroundBuffer_, "Kind of long, scrolling output");
    // Setup GUI for our app
#if 0
    actionView_.SetTitle("Disk IO");
#else
    actionView_.SetTitle("Disp/Batt");
#endif
    actionView_.SetButtons(ActionViewButtons::Confirm, ActionViewButtons::Cancel);
    actionView_.SetStyle(ActionViewStyles::IconDoubleAction);
    actionView_.SetContent(PlaygroundBuffer_);
    // Testcode can go here - Called when App starts
    SecondElapsed(); // initial display
}

void PlaygroundApp::Deactivated()
{
    // Testcode can go here - Called when App is being closed

    SingleValueBaseApp::Deactivated();
}

#ifndef SIMULATOR
// Flash disk test code:
CFUNC int littlefs_test_main();
#endif

void PlaygroundApp::SecondElapsed()
{
#ifndef SIMULATOR
    char buffer[40];
#if 0
    static int disk_io_ctr = 0;
    // Testcode can go here - Updated once per second
    // Flash disk test code:
    if (appSystem_.GetCurrentApp() == this)
    {
        if (disk_io_ctr < 30) // don't wear out flash
        {
            disk_io_ctr++;
            int bootcount = littlefs_test_main();
            sprintf(buffer, "%d", bootcount);
        }
        else
            sprintf(buffer, "tired...");

        UpdateText(buffer);
    }
    else // restart after change
        disk_io_ctr = 0;
#else
    // Testcode can go here - Updated once per second
    // Flash disk test code:
    if (appSystem_.GetCurrentApp() == this)
    {
        TBatteryState battState = BatteryGetState();
        if (battState.ErrorCode)
        {
            if (battState.ErrorCode == BATT_TIMEOUT)
                sprintf(buffer, "TIMEOUT");
            else
                sprintf(buffer, "Err.0x%02X", battState.ErrorCode);
        }
        else if (!battState.GaugeValid)
            sprintf(buffer, "wait...");
        else if (battState.Charging)
            sprintf(buffer, "Chrg %d%%", battState.Gauge);
        else
            sprintf(buffer, "Volt %d%%", battState.Gauge);

        UpdateText(buffer);
    }

#endif

#endif
}

void PlaygroundApp::Draw()
{
    // Testcode can go here - Updated on draw (ca. 30 times per second)
}

CFUNC void FullyEraseMemory();

void PlaygroundApp::PrimaryAction()
{
    // Testcode can go here - Right button touched

    //FullyEraseMemory();
}

void PlaygroundApp::SecondaryAction()
{
    // Testcode can go here - Left button touched

    static int test = 3;

    test++;
    test %= 4;

    DisplaySetBrightness(test);

    char buffer[15];
    sprintf(buffer, "%d", test);
    UpdateText(buffer);
}

void PlaygroundApp::ContentAction()
{
    // Testcode can go here - Center section touched

    DisplayRotate(!DisplayIsRotated());
}

void PlaygroundApp::UpdateText(const char* text)
{
    StringCopySafe(text, PlaygroundBuffer_, PLAYGROUND_BUFFER_SIZE);

    if (appSystem_.GetCurrentApp() == this)
    {
        actionView_.SetContent(PlaygroundBuffer_);
    }
}

#endif /* DEBUG */

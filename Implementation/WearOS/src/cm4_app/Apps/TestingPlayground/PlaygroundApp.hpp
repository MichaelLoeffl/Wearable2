#ifdef DEBUG
#ifndef PLAYGROUNDAPP_HPP
#define PLAYGROUNDAPP_HPP

#include "Apps/Abstract/SingleValueBaseApp.hpp"
#include "Drivers/HeartrateDriver.h"

#define PLAYGROUND_BUFFER_SIZE (128)

class PlaygroundApp : public SingleValueBaseApp
{
private:
public:
    PlaygroundApp() : SingleValueBaseApp(AppIds::Unknown) {}

    void Initialize() override;
    void SetUp() override;
    void Deactivated() override;

    void SecondElapsed() override;
    void Draw() override;

    void PrimaryAction() override;
    void SecondaryAction() override;
    void ContentAction() override;

    void UpdateText(const char* text);
};

#endif /* PLAYGROUNDAPP_HPP */
#endif /* DEBUG */
#ifndef NOTIFICATIONSBASEAPP_H_9AB396
#define NOTIFICATIONSBASEAPP_H_9AB396

#include "../../Common/App.hpp"
#include "../../Apps/Components/ListView.h"
#include "../../Notifications/NotificationService.hpp"

class NotificationsBaseApp : public App, public IListViewDataSource, public INotificationServiceUpdateHandler
{
private:
    int drawCounter_;
    lv_obj_t* steps;
    lv_obj_t* container;

    NotificationService* notificationService_;
    ListView listView_;

    AppIds notificationFilter_;

protected:
    void Initialize() override;
    void Activated() override;
    void Deactivated() override;

public:
    NotificationsBaseApp(AppIds appId, AppIds notificationFilter) : App(appId) { notificationFilter_ = notificationFilter; }
    virtual ~NotificationsBaseApp() {}

    virtual void OnNotificationSourceUpdate();
    virtual lv_img_dsc_t* GetNotificationImage(Notification* notification);

    void ItemOpenend(uint32_t id);

    bool GetItem(int index, uint32_t& identifier, const char*& line1, const char*& line2, const lv_img_dsc_t*& imageSource) override;
};

#endif /* NOTIFICATIONSBASEAPP_H_9AB396 */

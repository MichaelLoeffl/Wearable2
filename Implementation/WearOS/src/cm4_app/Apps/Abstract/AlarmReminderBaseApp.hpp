#ifndef ALARMREMINDERBASEAPP_HPP
#define ALARMREMINDERBASEAPP_HPP

#include "../Components/ListView.h"
#include "Common/App.hpp"
#include "Notifications/NotificationService.hpp"
#include "Services/AlarmReminderService.hpp"

class AlarmReminderBaseApp : public App, public IListViewDataSource, public INotificationServiceUpdateHandler
{
private:
    int drawCounter_;
    lv_obj_t* steps;
    lv_obj_t* container;

    ListView listView_;

    AlarmTypes alarmType_;

protected:
    void Initialize() override;
    void Activated() override;
    void Deactivated() override;

    AlarmReminderService* alarmReminderService_;

public:
    AlarmReminderBaseApp(AppIds appId, AlarmTypes alarmType) : App(appId) { alarmType_ = alarmType; }
    virtual ~AlarmReminderBaseApp() {}

    virtual void OnNotificationSourceUpdate();
    virtual lv_img_dsc_t* GetNotificationImage(Notification* notification);

    void ItemOpenend(uint32_t id);

    bool GetItem(int index, uint32_t& identifier, const char*& line1, const char*& line2, const lv_img_dsc_t*& imageSource) override;
};

#endif /* ALARMREMINDERBASEAPP_HPP */

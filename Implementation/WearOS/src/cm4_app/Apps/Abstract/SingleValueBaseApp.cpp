#include "../../lvgl/src/lv_objx/lv_btn.h"
#include "../../lvgl/src/lv_objx/lv_imgbtn.h"
#include "../../lvgl/src/lv_objx/lv_label.h"
#include "../../lvgl/src/lv_objx/lv_page.h"
#include "SingleValueBaseApp.hpp"
#include "../../Apps/Icons/Icons.h"
#include "../../Common/AppIds.hpp"
#include "../../Common/Tools.hpp"
#include "../../Drivers/Display.h"
#include "stdio.h"

SingleValueBaseApp* currentSingleValueBaseApp_;
char SingleValueBaseApp::valueText_[128] = {0};

void SingleValueBaseAppPrimaryAction()
{
    currentSingleValueBaseApp_->PrimaryAction();
}
void SingleValueBaseAppSecondaryAction()
{
    currentSingleValueBaseApp_->SecondaryAction();
}
void SingleValueBaseAppContentAction()
{
    currentSingleValueBaseApp_->ContentAction();
}

void SingleValueBaseApp::Initialize()
{
}

void SingleValueBaseApp::Activated()
{
    currentSingleValueBaseApp_ = this;

    actionView_.Create(page_);
    actionView_.SetImage(Tools::GetSmallIcon(AppId()));
    actionView_.SetContentClickAction(SingleValueBaseAppContentAction);
    actionView_.SetPrimaryButtonAction(SingleValueBaseAppPrimaryAction);
    actionView_.SetSecondaryButtonAction(SingleValueBaseAppSecondaryAction);

    SetUp();
    Update();
}

void SingleValueBaseApp::SecondElapsed()
{
    Update();
}
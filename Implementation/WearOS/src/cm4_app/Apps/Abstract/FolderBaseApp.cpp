#include "lvgl/src/lv_objx/lv_btn.h"
#include "lvgl/src/lv_objx/lv_img.h"
#include "lvgl/src/lv_objx/lv_imgbtn.h"
#include "lvgl/src/lv_objx/lv_label.h"
#include "lvgl/src/lv_objx/lv_page.h"
#include "FolderBaseApp.hpp"
#include "AppSystemM4.hpp"
#include "Apps/Design/Design.h"
#include "Apps/Icons/Icons.h"
#include "Common/AppFolders.hpp"
#include "Common/AppSystem.hpp"
#include "Drivers/Display.h"
#include <cassert>

static void StartApp(lv_obj_t* btn, lv_event_t event)
{
    if (event != LV_EVENT_CLICKED)
    {
        return;
    }

    App* app = (App*)lv_obj_get_user_data(btn);

    if (app != nullptr)
    {
        appSystem_.StartApp(*app, nullptr, false);
    }
}

void FolderBaseApp::Initialize()
{
}

void FolderBaseApp::Activated()
{
    App* app;
    LinkQueueIterator<APP_QUEUE_ALL_APPS> iterator = System()->IterateQueue<APP_QUEUE_ALL_APPS>();

    int size = 0;

    while ((app = iterator.GetCurrentApp()) != nullptr)
    {
        if (app != this && app->AppFolder() == appFolder_)
        {
            lv_obj_t* imageButton = lv_imgbtn_create(page_, nullptr);
            lv_img_dsc_t* image   = app->GetAppImage();
            if (image == nullptr)
            {
                image = &SettingsAppIcon;
            }

            lv_imgbtn_set_src(imageButton, LV_BTN_STATE_REL, image);
            lv_imgbtn_set_src(imageButton, LV_BTN_STATE_PR, image);
            lv_imgbtn_set_src(imageButton, LV_BTN_STATE_TGL_REL, image);
            lv_imgbtn_set_src(imageButton, LV_BTN_STATE_TGL_PR, image);
            lv_obj_set_event_cb(imageButton, StartApp);
            lv_imgbtn_set_toggle(imageButton, true);
            lv_obj_set_size(imageButton, image->header.w, image->header.h);
            lv_obj_set_pos(imageButton, size + APP_ICON_MARGIN / 2, (SCREEN_HEIGHT - image->header.h) / 2);
            lv_btn_set_fit(imageButton, true);
            lv_page_glue_obj(imageButton, true);
            lv_btn_set_layout(imageButton, LV_LAYOUT_OFF);
            lv_obj_set_user_data(imageButton, app);

            lv_obj_t* notificationImage = lv_img_create(page_, nullptr);
            lv_img_set_src(notificationImage, &NotificationBlackBorderStatus);
            lv_obj_set_pos(notificationImage, size + APP_ICON_MARGIN / 2 + image->header.w - 7, (SCREEN_HEIGHT - image->header.h) / 2 - 3);
            app->SetBadgeImage(notificationImage);

            size += APP_ICON_MARGIN + image->header.w;
        }
        iterator.Next();
    }

    SetPageSize(size);
}

void FolderBaseApp::Deactivated()
{
    App* app;
    LinkQueueIterator<APP_QUEUE_ALL_APPS> iterator = System()->IterateQueue<APP_QUEUE_ALL_APPS>();
    while ((app = iterator.GetCurrentApp()) != nullptr)
    {
        if (app != this && app->AppFolder() == appFolder_)
        {
            app->SetBadgeImage(nullptr);
        }
        iterator.Next();
    }
}

void FolderBaseApp::Shutdown()
{
}

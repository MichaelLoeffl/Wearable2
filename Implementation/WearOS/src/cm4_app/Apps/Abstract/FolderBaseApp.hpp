#ifndef FOLDERAPP_HPP
#define FOLDERAPP_HPP

#include "Common/App.hpp"
#include "Common/AppFolders.hpp"

class FolderBaseApp : public App
{
private:
    lv_obj_t* container = nullptr;
    AppFolders appFolder_;

public:
    FolderBaseApp(AppIds appId, AppFolders appFolder) : App(appId), appFolder_(appFolder) { }
    virtual ~FolderBaseApp() {}

protected:
    void Initialize() override;
    void Activated() override;
    void Deactivated() override;
    void Shutdown() override;
};

#endif /* FOLDERAPP_HPP */

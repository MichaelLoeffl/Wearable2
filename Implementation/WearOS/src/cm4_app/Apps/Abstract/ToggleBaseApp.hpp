#ifndef TOGGLEBASEAPP_HPP
#define TOGGLEBASEAPP_HPP

#include "../../lvgl/src/lv_core/lv_obj.h"
#include "Common/App.hpp"

class ToggleBaseApp : public App
{
private:
    lv_obj_t* image_;
    lv_obj_t* toggle_;
protected:
    ToggleBaseApp(AppIds appId) : App(appId) {}
    virtual ~ToggleBaseApp() {}

    lv_obj_t* titleLabel_;
    lv_obj_t* contentLabel_;

    void Initialize() override;
    void Activated() override;

    virtual void SetUp()  = 0;
    void SetImage(lv_img_dsc_t* imageSource);

public:
    virtual void ToggleAction(bool activated) = 0;
    void UpdateStateLabel();
    bool GetState();
    void SetState(bool state, bool animated = true);
};

#endif /* TOGGLEBASEAPP_HPP */

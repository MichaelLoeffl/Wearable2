#include "lvgl/src/lv_objx/lv_btn.h"
#include "lvgl/src/lv_objx/lv_imgbtn.h"
#include "lvgl/src/lv_objx/lv_label.h"
#include "lvgl/src/lv_objx/lv_page.h"
#include "AlarmReminderBaseApp.hpp"
#include "AppSystemM4.hpp"
#include "Apps/Icons/Icons.h"
#include "Common/AppIds.hpp"
#include "Common/Tools.hpp"
#include "Drivers/Display.h"
#include "stdio.h"

AlarmReminderBaseApp* currentAlarmReminderBaseApp_;
void AlarmReminderBaseAppPrimaryAction(uint32_t id)
{
    currentAlarmReminderBaseApp_->ItemOpenend(id);
}

lv_img_dsc_t* AlarmReminderBaseApp::GetNotificationImage(Notification* notification)
{
    return Tools::GetSmallIcon(notification->AppId);
}

void AlarmReminderBaseApp::Initialize()
{
    alarmReminderService_ = appSystem_.GetAlarmReminderService();
}

void AlarmReminderBaseApp::Deactivated()
{
}

void AlarmReminderBaseApp::Activated()
{
    currentAlarmReminderBaseApp_ = this;

    listView_.Create(page_);
    listView_.DataSource = this;
    listView_.SetItemOpenAction(&AlarmReminderBaseAppPrimaryAction);
    OnNotificationSourceUpdate();
}

void AlarmReminderBaseApp::OnNotificationSourceUpdate()
{
    listView_.UpdateItems();
}

bool AlarmReminderBaseApp::GetItem(int index, uint32_t& identifier, const char*& line1, const char*& line2, const lv_img_dsc_t*& imageSource)
{
    if (index >= MAX_ALARMS)
    {
        return false;
    }

    AlarmData* alarmData = alarmReminderService_->GetAlarms(alarmType_);

    identifier  = index;
    line1       = alarmData[index].Title;
    line2       = "06:00";
    imageSource = Tools::GetSmallIcon(AppId());

    return true;
}

void AlarmReminderBaseApp::ItemOpenend(uint32_t id)
{
    // TODO: Activate/Deactivate? This has to be checked with the requirements.
}
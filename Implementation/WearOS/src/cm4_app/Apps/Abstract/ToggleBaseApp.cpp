#include <lvgl/src/lv_objx/lv_page.h>
#include "ToggleBaseApp.hpp"
#include "../Design/Design.h"
#include "Apps/Icons/Icons.h"
#include "Common/AppIds.hpp"
#include "Common/Tools.hpp"
#include "Drivers/Display.h"
#include "lvgl/lvgl.h"
#include "stdio.h"

#define CONTENT_TOP (5)
#define ITEM_MARGIN (5)

extern lv_font_t Roboto18;
extern lv_font_t FreePixel15;

ToggleBaseApp* currentToggleBaseApp_;
void ToggleBaseAppToggleAction(lv_obj_t* obj, lv_event_t event)
{
    if (event == LV_EVENT_VALUE_CHANGED)
    {
        bool state = lv_sw_get_state(obj);
        currentToggleBaseApp_->ToggleAction(state);
        currentToggleBaseApp_->UpdateStateLabel();
    }
}

void ToggleBaseAppContentClickAction(lv_obj_t* obj, lv_event_t event)
{
    if (event != LV_EVENT_CLICKED)
    {
        return;
    }

    currentToggleBaseApp_->SetState(!currentToggleBaseApp_->GetState());
    currentToggleBaseApp_->ToggleAction(currentToggleBaseApp_->GetState());
}

void ToggleBaseApp::Initialize()
{
}

void ToggleBaseApp::Activated()
{
    currentToggleBaseApp_ = this;

    static lv_style_t regularStyle;
    lv_style_copy(&regularStyle, &lv_style_plain);
    regularStyle.text.font       = &FreePixel15;
    regularStyle.text.color.full = COLOR_WHITE;
    regularStyle.line.color.full = COLOR_WHITE;

    static lv_style_t hugeStyle;
    lv_style_copy(&hugeStyle, &lv_style_plain);
    hugeStyle.text.font       = &Roboto18;
    hugeStyle.text.color.full = COLOR_WHITE;
    hugeStyle.line.color.full = COLOR_WHITE;

    image_ = lv_img_create(page_, nullptr);
    lv_obj_set_pos(image_, 0, 0);
    SetImage(Tools::GetSmallIcon(AppId()));
    lv_obj_set_pos(image_, 0, CONTENT_TOP);

    titleLabel_ = lv_label_create(page_, nullptr);
    lv_label_set_style(titleLabel_, LV_LABEL_STYLE_MAIN, &regularStyle);
    lv_label_set_long_mode(titleLabel_, LV_LABEL_LONG_CROP);
    lv_obj_align(titleLabel_, page_, LV_ALIGN_IN_TOP_LEFT, 0, 0);
    lv_obj_set_pos(titleLabel_, lv_obj_get_width(image_) + ITEM_MARGIN, CONTENT_TOP);
    lv_obj_set_width(titleLabel_, SCREEN_WIDTH - lv_obj_get_x(titleLabel_));

    contentLabel_ = lv_label_create(page_, nullptr);
    lv_label_set_style(contentLabel_, LV_LABEL_STYLE_MAIN, &hugeStyle);
    // lv_label_set_long_mode(contentLabel_, LV_LABEL_LONG_CROP);
    lv_obj_set_pos(contentLabel_, 0, SCREEN_HEIGHT - lv_obj_get_height(contentLabel_) - CONTENT_TOP);
    lv_label_set_static_text(contentLabel_, "---");

    lv_obj_set_event_cb(page_, ToggleBaseAppContentClickAction);
    lv_obj_set_event_cb(titleLabel_, ToggleBaseAppContentClickAction);
    lv_obj_set_event_cb(contentLabel_, ToggleBaseAppContentClickAction);
    lv_obj_set_event_cb(image_, ToggleBaseAppContentClickAction);

    /*Create styles for the switch*/
    static lv_style_t backgroundStyle;
    static lv_style_t indicatorStyle;
    static lv_style_t knobOnStyle;
    static lv_style_t knobOffStyle;

    lv_style_copy(&backgroundStyle, &lv_style_pretty);
    backgroundStyle.body.radius         = LV_RADIUS_CIRCLE;
    backgroundStyle.body.main_color     = lv_color_hex(0x000000);
    backgroundStyle.body.grad_color     = lv_color_hex(0x000000);
    backgroundStyle.body.padding.top    = -2;
    backgroundStyle.body.padding.bottom = -2;
    backgroundStyle.body.padding.left   = -2;
    backgroundStyle.body.padding.right  = -2;
    backgroundStyle.body.border.color   = lv_color_hex(COLOR_WHITE);
    backgroundStyle.body.border.opa     = 255;
    backgroundStyle.body.border.width   = 2;

    lv_style_copy(&indicatorStyle, &lv_style_pretty_color);
    indicatorStyle.body.radius         = LV_RADIUS_CIRCLE;
    indicatorStyle.body.main_color     = lv_color_hex(0xffffff);
    indicatorStyle.body.grad_color     = lv_color_hex(0xffffff);
    indicatorStyle.body.padding.left   = 0;
    indicatorStyle.body.padding.right  = 0;
    indicatorStyle.body.padding.top    = 0;
    indicatorStyle.body.padding.bottom = 0;

    lv_style_copy(&knobOffStyle, &lv_style_pretty);
    knobOffStyle.body.radius         = LV_RADIUS_CIRCLE;
    knobOffStyle.body.main_color     = lv_color_hex(COLOR_WHITE);
    knobOffStyle.body.grad_color     = lv_color_hex(COLOR_WHITE);
    knobOffStyle.body.shadow.width   = 0;
    knobOffStyle.body.padding.left   = 4;
    knobOffStyle.body.padding.right  = 4;
    knobOffStyle.body.padding.top    = 4;
    knobOffStyle.body.padding.bottom = 4;
    knobOffStyle.body.border.color   = lv_color_hex(COLOR_BLACK);
    knobOffStyle.body.border.opa     = 255;
    knobOffStyle.body.border.width   = 2;
    knobOffStyle.body.border.part    = LV_BORDER_FULL;

    lv_style_copy(&knobOnStyle, &lv_style_pretty_color);
    knobOnStyle.body.radius       = LV_RADIUS_CIRCLE;
    knobOnStyle.body.main_color   = lv_color_hex(COLOR_BLACK);
    knobOnStyle.body.grad_color   = lv_color_hex(COLOR_BLACK);
    knobOnStyle.body.border.color = lv_color_hex(COLOR_WHITE);
    knobOnStyle.body.border.opa   = 255;
    knobOnStyle.body.border.width = 2;

    /*Create a switch and apply the styles*/
    toggle_ = lv_sw_create(page_, nullptr);
    lv_sw_set_style(toggle_, LV_SW_STYLE_BG, &backgroundStyle);
    lv_sw_set_style(toggle_, LV_SW_STYLE_INDIC, &indicatorStyle);
    lv_sw_set_style(toggle_, LV_SW_STYLE_KNOB_ON, &knobOnStyle);
    lv_sw_set_style(toggle_, LV_SW_STYLE_KNOB_OFF, &knobOffStyle);
    lv_obj_set_size(toggle_, 30, 14);
    lv_obj_set_pos(toggle_, SCREEN_WIDTH / 2 - 15, lv_obj_get_y(contentLabel_) + 2);
    lv_obj_set_event_cb(toggle_, ToggleBaseAppToggleAction);

    SetUp();
}

void ToggleBaseApp::SetImage(lv_img_dsc_t* imageSource)
{
    lv_img_set_src(image_, imageSource);
    lv_obj_set_size(image_, imageSource->header.w, imageSource->header.h);
}

void ToggleBaseApp::UpdateStateLabel()
{
    bool state = lv_sw_get_state(toggle_);
    lv_label_set_static_text(contentLabel_, state ? "On" : "Off");
}

bool ToggleBaseApp::GetState()
{
    return lv_sw_get_state(toggle_);
}

void ToggleBaseApp::SetState(bool state, bool animated)
{
    state ? lv_sw_on(toggle_, animated ? LV_ANIM_ON : LV_ANIM_OFF) : lv_sw_off(toggle_, animated ? LV_ANIM_ON : LV_ANIM_OFF);
    UpdateStateLabel();
}

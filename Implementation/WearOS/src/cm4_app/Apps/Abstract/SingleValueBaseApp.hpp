#ifndef SINGLEVALUEBASEAPP_HPP
#define SINGLEVALUEBASEAPP_HPP

#include "../../Common/App.hpp"
#include "../../Apps/Components/ActionView.h"

class SingleValueBaseApp : public App
{
protected:
    SingleValueBaseApp(AppIds appId) : App(appId) {}
    virtual ~SingleValueBaseApp() {}

    static char valueText_[128];

    ActionView actionView_;

    void Initialize() override;
    void Activated() override;
    void SecondElapsed() override;

    virtual void Update() {};
    virtual void SetUp()  = 0;

public:
    virtual void PrimaryAction() {};
    virtual void SecondaryAction() {};
    virtual void ContentAction() {};
};

#endif /* SINGLEVALUEBASEAPP_HPP */

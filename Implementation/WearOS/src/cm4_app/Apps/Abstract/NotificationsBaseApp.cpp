#include "../../lvgl/src/lv_objx/lv_btn.h"
#include "../../lvgl/src/lv_objx/lv_imgbtn.h"
#include "../../lvgl/src/lv_objx/lv_label.h"
#include "../../lvgl/src/lv_objx/lv_page.h"
#include "NotificationsBaseApp.hpp"
#include "../../AppSystemM4.hpp"
#include "../../Apps/Icons/Icons.h"
#include "../../Common/AppIds.hpp"
#include "../../Common/Tools.hpp"
#include "../../Drivers/Display.h"
#include "stdio.h"
#include "../../Notifications/Notification.hpp"

NotificationsBaseApp* currentNotificationBaseApp_;
void SingleValueBaseAppPrimaryAction(uint32_t id)
{
    currentNotificationBaseApp_->ItemOpenend(id);
}

lv_img_dsc_t* NotificationsBaseApp::GetNotificationImage(Notification* notification)
{
    return Tools::GetSmallIcon(notification->AppId);
}

void NotificationsBaseApp::Initialize()
{
}

void NotificationsBaseApp::Deactivated()
{
    notificationService_->RemoveNotificationServiceUpdateHandler(this);
}

void NotificationsBaseApp::Activated()
{
    currentNotificationBaseApp_ = this;

    notificationService_ = appSystem_.GetNotificationService();
    notificationService_->AddNotificationServiceUpdateHandler(this);

    listView_.Create(page_);
    listView_.DataSource = this;
    listView_.SetItemOpenAction(&SingleValueBaseAppPrimaryAction);
    OnNotificationSourceUpdate();
}

void NotificationsBaseApp::OnNotificationSourceUpdate()
{
    listView_.UpdateItems();
}

bool NotificationsBaseApp::GetItem(int index, uint32_t& identifier, const char*& line1, const char*& line2, const lv_img_dsc_t*& imageSource)
{
    Notification* notification = notificationService_->GetNotification(index, notificationFilter_);

    if (notification != nullptr)
    {
        identifier  = notification->Identifier;
        line1       = notification->Title;
        line2       = notification->Message;
        imageSource = GetNotificationImage(notification);
        return true;
    }

    return false;
}

void NotificationsBaseApp::ItemOpenend(uint32_t id)
{
    listView_.SetBadge(id, false);
    notificationService_->MarkRead(id);
}
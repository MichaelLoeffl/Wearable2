#ifndef ENERGYAPP_HPP
#define ENERGYAPP_HPP

#include "Apps/Abstract/ToggleBaseApp.hpp"

class EnergyApp : public ToggleBaseApp
{
public:
    EnergyApp() : ToggleBaseApp(AppIds::Energy) {}

    void SetUp() override;
    void ToggleAction(bool activated) override;

    AppFolders AppFolder() override { return AppFolders::SettingsApps; }
};

#endif /* ENERGYAPP_HPP */

#include "AboutApp.hpp"
#include "AppSystemM4.hpp"
#include "Apps/Icons/Icons.h"
#include "Meta.h"
#include "../../lvgl/src/lv_objx/lv_btn.h"
#include "../../lvgl/src/lv_objx/lv_imgbtn.h"
#include "../../lvgl/src/lv_objx/lv_label.h"
#include "../../lvgl/src/lv_objx/lv_page.h"


#ifndef SIMULATOR

uint32_t GetChipID()
{
#define CY_BLE_SFLASH_DIE_X_MASK (0x3Fu)
#define CY_BLE_SFLASH_DIE_X_BITS (6u)
#define CY_BLE_SFLASH_DIE_Y_MASK (0x3Fu)
#define CY_BLE_SFLASH_DIE_Y_BITS (6u)
#define CY_BLE_SFLASH_DIE_XY_BITS (CY_BLE_SFLASH_DIE_X_BITS + CY_BLE_SFLASH_DIE_Y_BITS)
#define CY_BLE_SFLASH_DIE_WAFER_MASK (0x1Fu)
#define CY_BLE_SFLASH_DIE_WAFER_BITS (5u)
#define CY_BLE_SFLASH_DIE_XYWAFER_BITS (CY_BLE_SFLASH_DIE_XY_BITS + CY_BLE_SFLASH_DIE_WAFER_BITS)
#define CY_BLE_SFLASH_DIE_LOT_MASK (0x7Fu)
#define CY_BLE_SFLASH_DIE_LOT_BITS (7u)

    uint32_t chip_id = 0;
    /*chip_id = ((uint32_t)SFLASH->DIE_X & (uint32_t)CY_BLE_SFLASH_DIE_X_MASK) |
                ((uint32_t)(((uint32_t)SFLASH->DIE_Y) & ((uint32_t)CY_BLE_SFLASH_DIE_Y_MASK)) << CY_BLE_SFLASH_DIE_X_BITS) |
                ((uint32_t)(((uint32_t)SFLASH->DIE_WAFER) & ((uint32_t)CY_BLE_SFLASH_DIE_WAFER_MASK)) << CY_BLE_SFLASH_DIE_XY_BITS) |
                ((uint32_t)(((uint32_t)SFLASH->DIE_LOT[0]) & ((uint32_t)CY_BLE_SFLASH_DIE_LOT_MASK)) << CY_BLE_SFLASH_DIE_XYWAFER_BITS);
                */

    return chip_id;
}

#else

uint32_t GetChipID()
{
    return 0x123456;
}

#endif

void AboutApp::Initialize()
{
}

void AboutApp::Restore()
{
}

void AboutApp::Activated()
{
    lv_obj_t* container = lv_cont_create(page_, NULL);
    lv_obj_set_pos(container, 0, 0);
    lv_obj_set_size(container, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_obj_align(container, NULL, LV_LAYOUT_OFF, 0, 0);
    lv_page_glue_obj(container, true);

    lv_obj_t* steps = lv_label_create(container, NULL);
    lv_label_set_static_text(steps, APP_VERSION);
    lv_page_glue_obj(steps, true);
    lv_obj_align(steps, container, LV_ALIGN_CENTER, 0, -10);

    static char buffer[20] = {0};
    uint32_t chip_id       = GetChipID();

    sprintf(buffer, "CHIP-ID %02lX-%02lX-%02lX", ((chip_id >> 16) & 0xFF), ((chip_id >> 8) & 0xFF), (chip_id & 0xFF));

    lv_obj_t* label_chip_id = lv_label_create(container, NULL);
    lv_label_set_static_text(label_chip_id, buffer);
    lv_page_glue_obj(label_chip_id, true);
    lv_obj_align(label_chip_id, container, LV_ALIGN_CENTER, 0, 10);
}

void AboutApp::Deactivated()
{
}

void AboutApp::Shutdown()
{
}

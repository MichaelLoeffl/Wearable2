#ifndef SETTINGSAPP_H
#define SETTINGSAPP_H

#include "Apps/Abstract/FolderBaseApp.hpp"
#include "Common/AppFolders.hpp"

class SettingsApp : public FolderBaseApp
{
public:
    SettingsApp() : FolderBaseApp(AppIds::Settings, AppFolders::SettingsApps) {}
    virtual ~SettingsApp() {}
};

#endif /* SETTINGSAPP_H */

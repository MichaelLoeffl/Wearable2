#ifndef FLIGHTMODEAPP_HPP
#define FLIGHTMODEAPP_HPP

#include "Apps/Abstract/ToggleBaseApp.hpp"

class FlightModeApp : public ToggleBaseApp
{
public:
    FlightModeApp() : ToggleBaseApp(AppIds::FlightMode) {}

    void SetUp() override;
    void ToggleAction(bool activated) override;

    AppFolders AppFolder() override { return AppFolders::SettingsApps; }
};

#endif /* FLIGHTMODEAPP_HPP */

#ifndef ABOUTAPP_H_09BA74
#define ABOUTAPP_H_09BA74

#include "Common/App.hpp"

class AboutApp : public App
{
private:
public:
    AboutApp() : App(AppIds::About) {}
    virtual ~AboutApp() {}

    AppFolders AppFolder() override { return AppFolders::SettingsApps; }

protected:
    virtual void Initialize();

    virtual void Restore();
    virtual void Activated();
    virtual void Deactivated();
    virtual void Shutdown();
};

#endif /* ABOUTAPP_H_09BA74 */

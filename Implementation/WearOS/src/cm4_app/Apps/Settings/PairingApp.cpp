#include "PairingApp.hpp"
#include "AppSystemM4.hpp"
#include "Services/PairingService.hpp"
#include <cstdio>
#include <stdint.h>

void PairingApp::UpdatePairingCode()
{
    uint32_t pairingCode = appSystem_.GetPairingService()->GetPairingCode();

    if (pairingCode != 0 && pairingCode <= 999999)
    {
        appSystem_.UserInteraction();
    }

    if (pairingCode == code_)
    {
        return;
    }

    if (pairingCode > 999999 || pairingCode == 0)
    {
        code_ = 0;
        sprintf(valueText_, "- - - - - -");
        actionView_.SetContent(valueText_);
        return;
    }

    code_ = pairingCode;

    uint8_t digits[6] = {0};
    for (size_t i = 0; i < 6; i++)
    {
        digits[i] = pairingCode % 10;
        pairingCode /= 10;
    }
    sprintf(valueText_, "%1d %1d %1d %1d %1d %1d", digits[5], digits[4], digits[3], digits[2], digits[1], digits[0]);
    actionView_.SetContent(valueText_);
}

void PairingApp::BackPressed()
{
    if (code_ == 0)
    {
        App::BackPressed();
    }
}

void PairingApp::SetUp()
{
    actionView_.SetTitle("Pairing Code");
    actionView_.SetButtons(ActionViewButtons::None);
    actionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
    UpdatePairingCode();
}

void PairingApp::Activated()
{
    SingleValueBaseApp::Activated();

    PairingService* pairingService = appSystem_.GetPairingService();
    pairingService->SetPairingActive(true);
}

void PairingApp::Deactivated()
{
    SingleValueBaseApp::Deactivated();
}

void PairingApp::SecondElapsed()
{
    UpdatePairingCode();
    if (code_ == 0)
    {
        BackPressed();
    }
}

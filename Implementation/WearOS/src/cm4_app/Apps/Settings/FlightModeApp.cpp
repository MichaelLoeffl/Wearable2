#include "FlightModeApp.hpp"
#include "AppSystemM4.hpp"
#include "Drivers/Core.h"
#include "Services/PowerService.hpp"
#include <cstdio>


void FlightModeApp::SetUp()
{
    lv_label_set_static_text(titleLabel_, "Flight Mode");
    PowerService* powerService = appSystem_.GetPowerService();
    SetState(powerService->IsFlightModeActive(), false);
}

void FlightModeApp::ToggleAction(bool activated)
{
    PowerService* powerService = appSystem_.GetPowerService();
    powerService->ActivateFlightMode(activated);
}
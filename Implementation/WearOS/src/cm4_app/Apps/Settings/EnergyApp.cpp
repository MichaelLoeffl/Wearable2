#include "EnergyApp.hpp"
#include "AppSystemM4.hpp"
#include "Common/IconCache.hpp"
#include "Services/PowerService.hpp"
#include <cstdio>

void EnergyApp::SetUp()
{
    lv_label_set_static_text(titleLabel_, "Energy Saving");
    PowerService* powerService = appSystem_.GetPowerService();
    bool powerSavingActive     = powerService->IsPowerSavingActive();
    bool autoPowerSavingActive = powerService->IsAutoPowerSavingActive();

    SetState(!autoPowerSavingActive && powerSavingActive, false);
}

void EnergyApp::ToggleAction(bool activated)
{
    PowerService* powerService = appSystem_.GetPowerService();
    powerService->ActivatePowerSaving(activated, true);
}
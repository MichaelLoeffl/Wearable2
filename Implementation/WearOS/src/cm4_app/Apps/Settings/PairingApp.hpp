#ifndef PAIRINGAPP_HPP
#define PAIRINGAPP_HPP

#include "Apps/Abstract/SingleValueBaseApp.hpp"

class PairingApp : public SingleValueBaseApp
{
private:
    uint32_t code_      = 0;
    char valueText_[15] = {0};

public:
    PairingApp() : SingleValueBaseApp(AppIds::Pairing) {}

    AppFolders AppFolder() { return AppFolders::System; }
    void BackPressed() override;
    void BackLongPressed() override{};

    void Activated() override;
    void Deactivated() override;
    void UpdatePairingCode();

    void SetUp() override;
    void SecondElapsed() override;
};

#endif /* PAIRINGAPP_HPP */

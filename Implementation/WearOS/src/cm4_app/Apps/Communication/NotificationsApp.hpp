#ifndef NOTIFICATIONSAPP_H_9EF511
#define NOTIFICATIONSAPP_H_9EF511

#include "Apps/Icons/Icons.h"
#include "Apps/Abstract/NotificationsBaseApp.hpp"

class NotificationsApp : public NotificationsBaseApp
{
public:
    NotificationsApp() : NotificationsBaseApp(AppIds::Notifications, AppIds::Notifications) {}
};

#endif /* NOTIFICATIONSAPP_H_9EF511 */

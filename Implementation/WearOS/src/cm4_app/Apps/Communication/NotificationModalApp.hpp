#ifndef NOTIFICATIONMODALAPP_H_43334F
#define NOTIFICATIONMODALAPP_H_43334F

#include "Common/App.hpp"
#include "Apps/Components/ActionView.h"
#include "Apps/Components/ListView.h"
#include "Apps/Icons/Icons.h"
#include "Notifications/Notification.hpp"

class NotificationModalApp : public App
{
private:
    ActionView actionView_;
    Notification* notification_ = nullptr;
    uint8_t secondsVisible_     = 0;

public:
    NotificationModalApp() : App(AppIds::NotificationModal) {}
    virtual ~NotificationModalApp() {}

    AppFolders AppFolder() override { return AppFolders::System; }

    void SetNotification(Notification* notification);

    void PrimaryAction();
    void SecondaryAction();
    void ContentAction();

protected:
    void Initialize() override;
    void Activated() override;
    void SecondElapsed() override;
};

#endif /* NOTIFICATIONMODALAPP_H_43334F */

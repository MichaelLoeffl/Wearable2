#ifndef EMAILAPP_H_65CC68
#define EMAILAPP_H_65CC68

#include "Apps/Icons/Icons.h"
#include "Apps/Abstract/NotificationsBaseApp.hpp"

class EmailApp : public NotificationsBaseApp
{
public:
    EmailApp() : NotificationsBaseApp(AppIds::Email, AppIds::Email) {}
};

#endif /* EMAILAPP_H_65CC68 */

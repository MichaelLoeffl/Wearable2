#ifndef PHONEAPP_H_30B2D4
#define PHONEAPP_H_30B2D4

#include "Apps/Icons/Icons.h"
#include "Apps/Abstract/NotificationsBaseApp.hpp"

class PhoneApp : public NotificationsBaseApp
{
public:
    PhoneApp() : NotificationsBaseApp(AppIds::Phone, AppIds::Phone) {}
};

#endif /* PHONEAPP_H_30B2D4 */

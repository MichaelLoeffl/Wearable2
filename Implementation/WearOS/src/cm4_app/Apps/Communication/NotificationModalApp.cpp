#include <lvgl/src/lv_objx/lv_btn.h>
#include <lvgl/src/lv_objx/lv_imgbtn.h>
#include <lvgl/src/lv_objx/lv_label.h>
#include <lvgl/src/lv_objx/lv_page.h>
#include "NotificationModalApp.hpp"
#include "AppSystemM4.hpp"
#include "Apps/Abstract/NotificationsBaseApp.hpp"
#include "Apps/Icons/Icons.h"
#include "Common/Tools.hpp"
#include "Notifications/NotificationService.hpp"

#define MAX_SECONDS_VISIBLE (10)

NotificationModalApp* currentNotificationModalApp_;
void NotificationModalPrimaryAction()
{
    currentNotificationModalApp_->PrimaryAction();
}
void NotificationModalSecondaryAction()
{
    currentNotificationModalApp_->SecondaryAction();
}
void NotificationModalContentAction()
{
    currentNotificationModalApp_->ContentAction();
}

void NotificationModalApp::Initialize()
{
}

void NotificationModalApp::SetNotification(Notification* notification)
{
    if (notification == nullptr)
    {
        BackPressed();
        return;
    }

    notification_ = notification;

    actionView_.SetTitle(notification_->Title);
    actionView_.SetContent(notification_->Message);
    actionView_.SetImage(Tools::GetSmallIcon(notification_->AppId));
    actionView_.SetContentClickAction(NotificationModalContentAction);
    actionView_.SetPrimaryButtonAction(NotificationModalPrimaryAction);
    actionView_.SetSecondaryButtonAction(NotificationModalSecondaryAction);

    switch (notification_->AppId)
    {
        case AppIds::Email:
            actionView_.SetStyle(ActionViewStyles::IconNoAction);
            break;
        case AppIds::Timer:
        case AppIds::Alarm:
            actionView_.SetButtons(ActionViewButtons::Confirm);
            actionView_.SetStyle(ActionViewStyles::HugeSingleAction);
            break;
        case AppIds::IncomingCall:
            actionView_.SetButtons(ActionViewButtons::Confirm, ActionViewButtons::Cancel);
            actionView_.SetStyle(ActionViewStyles::HugeDoubleAction);
            break;

        default:
            actionView_.SetButtons(ActionViewButtons::Confirm);
            actionView_.SetStyle(ActionViewStyles::IconSingleAction);
            break;
    }

    secondsVisible_ = 0;
}

void NotificationModalApp::Activated()
{
    currentNotificationModalApp_ = this;

    actionView_.Create(page_);

    auto notificationService = appSystem_.GetNotificationService();
    SetNotification(notificationService->GetLatestNotification());
}

void NotificationModalApp::PrimaryAction()
{
    switch (notification_->AppId)
    {
        case AppIds::IncomingCall:
            appSystem_.GetNotificationService()->SendPositiveAction(notification_->Identifier);
            break;
        default:
            break;
    }

    BackPressed();
}

void NotificationModalApp::SecondaryAction()
{
    switch (notification_->AppId)
    {
        case AppIds::IncomingCall:
            appSystem_.GetNotificationService()->SendNegativeAction(notification_->Identifier);
            break;
        default:
            break;
    }

    BackPressed();
}

void NotificationModalApp::ContentAction()
{
    appSystem_.StartApp(notification_->AppId);
}

void NotificationModalApp::SecondElapsed()
{
    secondsVisible_++;
    if (secondsVisible_ > MAX_SECONDS_VISIBLE && !notification_->IsModal)
    {
        secondsVisible_ = 0;
        BackPressed();
    }
}
#include "PaymentApp.hpp"
#include <cstdio>
#include <string.h>

void PaymentApp::SetUp()
{
    actionView_.SetTitle("Balance");
    actionView_.SetButtons(ActionViewButtons::None);
    actionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);

    LoadBalance();
}

void PaymentApp::Update()
{
    sprintf(valueText_, "%.2f %s", balance_.Value0 / 100.0, balance_.Value1);
    actionView_.SetContent(valueText_);
}

void PaymentApp::LoadBalance()
{
    Update();
}
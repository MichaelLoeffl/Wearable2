#ifndef PAYMENTAPP_H
#define PAYMENTAPP_H

#include "Apps/Abstract/SingleValueBaseApp.hpp"
#include "Services/Settings.hpp"

class BalanceData : public BaseSetting2<int32_t, char[6]>
{
public:
    BalanceData() : BaseSetting2<int32_t, char[6]>(SETTINGS_PAYMENT_BALANCE){};
};

class PaymentApp : public SingleValueBaseApp
{
private:
    BalanceData balance_;

    void LoadBalance();

public:
    PaymentApp() : SingleValueBaseApp(AppIds::Payment) {}

    void SetUp() override;
    void Update() override;
};

#endif /* PAYMENTAPP_H */

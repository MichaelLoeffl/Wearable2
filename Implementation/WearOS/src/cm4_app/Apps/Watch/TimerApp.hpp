#ifndef TIMERAPP_H
#define TIMERAPP_H

#include "Apps/Abstract/SingleValueBaseApp.hpp"
#include "Common/Timespan.hpp"
#include "Services/SchedulingService.hpp"

class TimerApp : public SingleValueBaseApp, public ISchedulable
{
private:
    DateTime startTime_;
    Timespan pauseTime_;

    Timespan timerLength_;
    bool editMode_;
    uint8_t secondsInEditMode_ = 0;

    void UpdateTime();
    void UpdateButtons();

public:
    TimerApp() : SingleValueBaseApp(AppIds::Timer), startTime_(0), pauseTime_(0), timerLength_(0, 0, 10, 0, 0) {}

    void SetUp() override;
    void SecondElapsed() override;

    void PrimaryAction() override;
    void SecondaryAction() override;
    void ContentAction() override;

    uint32_t GetElapsedMiliseconds();

    void Notify() override;

    AppFolders AppFolder() override { return AppFolders::WatchApps; }
};

#endif /* TIMERAPP_H */

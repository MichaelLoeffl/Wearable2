#include "WatchLocalApp.hpp"
#include "AppSystemM4.hpp"
#include "Common/DateTime.hpp"
#include "Services/TimeService.hpp"
#include <cstdio>
#include <stdint.h>

void WatchLocalApp::SetUp()
{
    actionView_.SetButtons(ActionViewButtons::None);
    actionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
}

void WatchLocalApp::Update()
{
    DateTime dateTime = DateTime::Now();

    TimeService* timeService = appSystem_.GetTimeService();
    auto dayOfWeekText       = timeService->GetDayOfWeekText(dateTime.GetDayOfWeek());

    sprintf(titleText_, "%s %02d.%02d.", dayOfWeekText, (int)dateTime.GetDay(), (int)dateTime.GetMonth());
    actionView_.SetTitle(titleText_);

    sprintf(valueText_, "%02d:%02d:%02d", (int)dateTime.GetHour(), (int)dateTime.GetMinute(), (int)dateTime.GetSecond());

    actionView_.SetContent(valueText_);
}
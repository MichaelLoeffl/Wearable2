#include "TimerApp.hpp"
#include "AppSystemM4.hpp"
#include "Notifications/NotificationService.hpp"
#include "Services/TimeService.hpp"
#include <cstdio>
#include <stdint.h>

void TimerApp::SetUp()
{
    pauseTime_ = 0;
    UpdateButtons();
}

uint32_t TimerApp::GetElapsedMiliseconds()
{
    return (DateTime::Now() - startTime_).TotalMilliseconds();
}

void TimerApp::UpdateButtons()
{
    UpdateTime();

    if (editMode_)
    {
        actionView_.SetTitle("Set mm:ss");
        actionView_.SetButtons(ActionViewButtons::Plus, ActionViewButtons::Minus);
        actionView_.SetStyle(ActionViewStyles::IconDoubleActionHugeText);
        return;
    }

    if (pauseTime_.TotalMilliseconds() > 0)
    {
        actionView_.SetTitle("Paused");
        actionView_.SetButtons(ActionViewButtons::Play, ActionViewButtons::Reset);
        actionView_.SetStyle(ActionViewStyles::IconDoubleActionHugeText);
        return;
    }

    if (startTime_.Valid())
    {
        actionView_.SetTitle("Counting");
        actionView_.SetButtons(ActionViewButtons::Pause);
        actionView_.SetStyle(ActionViewStyles::IconSingleActionHugeText);
    }
    else
    {
        actionView_.SetTitle("Timer");
        actionView_.SetButtons(ActionViewButtons::Play, ActionViewButtons::Reply);
        actionView_.SetStyle(ActionViewStyles::IconDoubleActionHugeText);
    }
}

void TimerApp::ContentAction()
{
    if (editMode_)
    {
        editMode_ = false;
        UpdateButtons();
        return;
    }
}

void TimerApp::PrimaryAction()
{
    if (editMode_)
    {
        // Increment
        timerLength_       = timerLength_ + 10000;
        secondsInEditMode_ = 0;
        UpdateTime();
        return;
    }

    if (startTime_.Valid())
    {
        // Pause
        pauseTime_ = DateTime::Now() - startTime_;
        startTime_ = DateTime::Zero();
        Deschedule();
        UpdateButtons();
    }
    else
    {
        // Start
        startTime_ = DateTime::Now() - pauseTime_;
        pauseTime_ = Timespan(0);
        Schedule(DateTime::Now() + (timerLength_ - (DateTime::Now() - startTime_)));
        UpdateButtons();
    }
}

void TimerApp::SecondaryAction()
{
    if (editMode_)
    {
        // Decrement
        if (timerLength_.TotalSeconds() > 10)
        {
            timerLength_ = timerLength_ - 10000;
        }
        secondsInEditMode_ = 0;
        UpdateTime();
        return;
    }

    if (pauseTime_.TotalMilliseconds() > 0)
    {
        pauseTime_ = 0;
        UpdateButtons();
        return;
    }

    if (!startTime_.Valid())
    {
        editMode_          = true;
        secondsInEditMode_ = 0;
        UpdateButtons();
        return;
    }

    pauseTime_ = 0;
    actionView_.SetButtons(ActionViewButtons::Play);
    actionView_.SetStyle(ActionViewStyles::IconSingleActionHugeText);
    UpdateTime();
}

void TimerApp::Notify()
{
    if (startTime_.Valid())
    {
        startTime_ = DateTime::Zero();
        pauseTime_ = 0;
        if (appSystem_.GetCurrentApp() == this)
        {
            UpdateButtons();
        }
    }

    NotificationService* notificationService = appSystem_.GetNotificationService();
    auto id                                  = notificationService->AddNotification(AppIds::Timer, "Timer end", "00:00");
    notificationService->PresentLatestNotification(id->Identifier);
}

void TimerApp::UpdateTime()
{
    uint16_t minutes = 0;
    uint16_t seconds = 0;
    uint16_t diff    = 0;
    if (editMode_)
    {
        diff = timerLength_.TotalSeconds();
    }
    else if (pauseTime_.TotalMilliseconds() > 0)
    {
        diff = (timerLength_ - pauseTime_).TotalSeconds();
    }
    else if (startTime_.Valid())
    {
        int32_t elapsed = GetElapsedMiliseconds();
        diff            = elapsed < timerLength_.TotalMilliseconds() ? (timerLength_ - elapsed).TotalSeconds() : 0;
    }
    else
    {
        diff = timerLength_.TotalSeconds();
    }
    if (diff < 0)
    {
        diff = 0;
    }
    minutes = diff / 60;
    seconds = diff - minutes * 60;

    sprintf(valueText_, "- %02d:%02d", minutes, seconds);
    actionView_.SetContent(valueText_);
}

void TimerApp::SecondElapsed()
{
    UpdateTime();

    if (editMode_)
    {
        secondsInEditMode_++;
        if (secondsInEditMode_ > 4)
        {
            ContentAction();
        }
        return;
    }
}
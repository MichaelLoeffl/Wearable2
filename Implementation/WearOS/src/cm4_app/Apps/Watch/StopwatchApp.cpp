#include "StopwatchApp.hpp"
#include "AppSystemM4.hpp"
#include "Common/Timespan.hpp"
#include <cstdio>
#include <stdint.h>

void StopwatchApp::SetUp()
{
    actionView_.SetTitle("Stopwatch");
    actionView_.SetButtons(!startTime_.Valid() ? ActionViewButtons::Play : ActionViewButtons::Pause, ActionViewButtons::Reset);
    actionView_.SetStyle(pauseTime_.TotalMilliseconds() == 0 ? ActionViewStyles::IconSingleActionHugeText : ActionViewStyles::IconDoubleActionHugeText);
}

void StopwatchApp::Update()
{
    uint16_t minutes = 0;
    uint16_t seconds = 0;
    uint16_t diff    = 0;
    if (startTime_.Valid())
    {
        diff = GetElapsedSeconds();
    }
    else
    {
        diff = pauseTime_.TotalSeconds();
    }
    minutes = diff / 60;
    seconds = diff - minutes * 60;

    sprintf(valueText_, "%02d:%02d", minutes, seconds);
    actionView_.SetContent(valueText_);
}

uint16_t StopwatchApp::GetElapsedSeconds()
{
    Timespan diff = DateTime::Now() - startTime_;
    return diff.TotalSeconds();
}

void StopwatchApp::PrimaryAction()
{
    if (!startTime_.Valid())
    {
        startTime_ = DateTime::Now() - pauseTime_;
        pauseTime_ = Timespan(0);
        actionView_.SetButtons(ActionViewButtons::Pause);
        actionView_.SetStyle(ActionViewStyles::IconSingleActionHugeText);
    }
    else
    {
        pauseTime_ = DateTime::Now() - startTime_;
        startTime_ = DateTime::Zero();
        actionView_.SetButtons(ActionViewButtons::Play, ActionViewButtons::Reset);
        actionView_.SetStyle(ActionViewStyles::IconDoubleActionHugeText);
        Update();
    }
}

void StopwatchApp::SecondaryAction()
{
    pauseTime_ = Timespan(0);
    actionView_.SetButtons(ActionViewButtons::Play);
    actionView_.SetStyle(ActionViewStyles::IconSingleActionHugeText);
    Update();
}
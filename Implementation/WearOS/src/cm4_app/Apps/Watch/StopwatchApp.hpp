#ifndef STOPWATCHAPP_H
#define STOPWATCHAPP_H

#include "Common/Timespan.hpp"
#include "Apps/Abstract/SingleValueBaseApp.hpp"

class StopwatchApp : public SingleValueBaseApp
{
private:
    DateTime startTime_;
    Timespan pauseTime_;

public:
    StopwatchApp() : SingleValueBaseApp(AppIds::Stopwatch), startTime_(0), pauseTime_(0) {}

    void SetUp() override;
    void Update() override;

    void PrimaryAction() override;
    void SecondaryAction() override;

    uint16_t GetElapsedSeconds();

    AppFolders AppFolder() override { return AppFolders::WatchApps; }
};

#endif /* STOPWATCHAPP_H */

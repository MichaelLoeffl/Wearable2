#ifndef WATCHAPP_H_916AE0
#define WATCHAPP_H_916AE0

#include "Apps/Abstract/FolderBaseApp.hpp"
#include "Common/AppFolders.hpp"

class WatchApp : public FolderBaseApp
{
public:
    WatchApp() : FolderBaseApp(AppIds::WatchApps, AppFolders::WatchApps) {}
    virtual ~WatchApp() {}

    bool IgnoreRotation() override { return true; }
};

#endif /* WATCHAPP_H_916AE0 */

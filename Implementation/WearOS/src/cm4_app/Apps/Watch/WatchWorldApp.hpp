#ifndef WATCHWORLDAPP_HPP
#define WATCHWORLDAPP_HPP

#include "Apps/Abstract/SingleValueBaseApp.hpp"

class WatchWorldApp : public SingleValueBaseApp
{
private:
    bool editMode_ = false;

public:
    WatchWorldApp() : SingleValueBaseApp(AppIds::WatchWorld) {}

    void SetUp() override;
    void Update() override;
    void UpdateEditMode();

    void PrimaryAction() override;
    void SecondaryAction() override;

    AppFolders AppFolder() override { return AppFolders::WatchApps; }

private:
    char titleText_[16] = {0};
};

#endif /* WATCHWORLDAPP_HPP */

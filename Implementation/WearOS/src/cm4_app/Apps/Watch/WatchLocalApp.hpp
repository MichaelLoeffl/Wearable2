#ifndef WATCHLOCALAPP_H
#define WATCHLOCALAPP_H

#include "Apps/Abstract/SingleValueBaseApp.hpp"

class WatchLocalApp : public SingleValueBaseApp
{
public:
    WatchLocalApp() : SingleValueBaseApp(AppIds::WatchLocal) {}

    void SetUp() override;
    void Update() override;

    AppFolders AppFolder() override { return AppFolders::WatchApps; }

private:
    char titleText_[16] = {0};
};

#endif /* WATCHLOCALAPP_H */

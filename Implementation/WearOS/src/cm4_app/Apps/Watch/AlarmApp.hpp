#ifndef ALARMAPP_HPP
#define ALARMAPP_HPP

#include "Apps/Abstract/AlarmReminderBaseApp.hpp"
#include "Services/AlarmReminderService.hpp"

class AlarmApp : public AlarmReminderBaseApp
{
public:
    AlarmApp() : AlarmReminderBaseApp(AppIds::Alarm, AlarmTypes::Alarm) {}

    AppFolders AppFolder() override { return AppFolders::WatchApps; }
};

#endif /* ALARMAPP_HPP */

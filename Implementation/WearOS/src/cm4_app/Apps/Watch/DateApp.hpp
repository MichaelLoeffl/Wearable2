#ifndef DATEAPP_HPP
#define DATEAPP_HPP

#include "Apps/Abstract/SingleValueBaseApp.hpp"

class DateApp : public SingleValueBaseApp
{
public:
    DateApp() : SingleValueBaseApp(AppIds::Date) {}

    void SetUp() override;
    void Update() override;

    AppFolders AppFolder() override { return AppFolders::WatchApps; }

private:
    char titleText_[32] = {0};
};

#endif /* DATEAPP_HPP */

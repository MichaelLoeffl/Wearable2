#include "AlarmApp.hpp"
#include "Common/ccronexpr.h"
#include "Services/Settings.hpp"
#include <cstdio>

class CronSetting : public BaseSetting2<uint8_t[32], uint8_t[32]>
{
protected:
    cron_expr cronExpression;

public:
    CronSetting(const char* name) : BaseSetting2(name) {}

    void Written(bool hadErrors) override
    {
        if (!hadErrors)
        {
            Value0[sizeof(Value0) - 1] = '\0';

            cron_expr expr;
            memset(&expr, 0, sizeof(expr));

            const char* err = NULL;
            cron_parse_expr((const char*)Value0, &expr, &err);

            if (err)
            {
                hadErrors = true;
            }
            else
            {
                cronExpression = expr;
            }
        }

        BaseSetting2::Written(hadErrors);
    }
};

CronSetting cron1("alarms/1");
CronSetting cron2("alarms/2");
CronSetting cron3("alarms/3");
CronSetting cron4("alarms/4");
CronSetting cron5("alarms/5");

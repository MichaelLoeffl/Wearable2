#include "DateApp.hpp"
#include "AppSystemM4.hpp"
#include "Services/TimeService.hpp"
#include <cstdio>
#include <stdint.h>

void DateApp::SetUp()
{
    actionView_.SetButtons(ActionViewButtons::None);
    actionView_.SetStyle(ActionViewStyles::IconNoActionHugeText);
}

void DateApp::Update()
{
    DateTime dateTime = DateTime::Now();

    TimeService* timeService = appSystem_.GetTimeService();
    auto dayOfWeekText       = timeService->GetDayOfWeekText(dateTime.GetDayOfWeek());

    sprintf(titleText_, "%s, KW %d", dayOfWeekText, (int)dateTime.GetCalendarWeek(true));
    actionView_.SetTitle(titleText_);

    sprintf(valueText_, "%02d.%02d.%04d", (int)dateTime.GetDay(), (int)dateTime.GetMonth(), (int)dateTime.GetYear());
    actionView_.SetContent(valueText_);
}
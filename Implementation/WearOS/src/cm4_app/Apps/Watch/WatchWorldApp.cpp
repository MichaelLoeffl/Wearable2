#include "WatchWorldApp.hpp"
#include "AppSystemM4.hpp"
#include "Services/TimeService.hpp"
#include <cstdio>
#include <stdint.h>

#define UTC_MIN_OFFSET (-12)
#define UTC_MAX_OFFSET (+14)
#define UTC_OFFSET_DELTA (UTC_MAX_OFFSET - UTC_MIN_OFFSET)

void WatchWorldApp::SetUp()
{
    actionView_.SetButtons(ActionViewButtons::Reset);
    actionView_.SetStyle(ActionViewStyles::IconSingleActionHugeText);
}

void WatchWorldApp::Update()
{
    if (editMode_)
    {
        return;
    }

    TimeService* timeService = appSystem_.GetTimeService();
    DateTime dateTime        = timeService->GetRemoteTime();

    auto dayOfWeekText = timeService->GetDayOfWeekText(dateTime.GetDayOfWeek());

    sprintf(titleText_, "%s %02d.%02d.", dayOfWeekText, (int)dateTime.GetDay(), (int)dateTime.GetMonth());
    actionView_.SetTitle(titleText_);

    sprintf(valueText_, "%02d:%02d:%02d", (int)dateTime.GetHour(), (int)dateTime.GetMinute(), (int)dateTime.GetSecond());

    actionView_.SetContent(valueText_);
}

void WatchWorldApp::UpdateEditMode()
{
    TimeService* timeService = appSystem_.GetTimeService();
    auto offset              = timeService->GetRemoteOffset();
    sprintf(valueText_, "%+02d", (int)offset.TotalHours());
    actionView_.SetContent(valueText_);
}

void WatchWorldApp::PrimaryAction()
{
    editMode_ = !editMode_;
    if (editMode_)
    {
        actionView_.SetTitle("Offset");
        actionView_.SetButtons(ActionViewButtons::Confirm, ActionViewButtons::Plus);
        actionView_.SetStyle(ActionViewStyles::HugeDoubleAction);
        UpdateEditMode();
    }
    else
    {
        actionView_.SetTitle("Roaming Time");
        actionView_.SetButtons(ActionViewButtons::Reset);
        actionView_.SetStyle(ActionViewStyles::IconSingleActionHugeText);
        Update();
    }
}

void WatchWorldApp::SecondaryAction()
{
    TimeService* timeService = appSystem_.GetTimeService();
    auto offset              = timeService->GetRemoteOffset().TotalHours();
    offset++;
    if (offset > UTC_OFFSET_DELTA)
    {
        offset = -UTC_OFFSET_DELTA;
    }
    if (offset < -UTC_OFFSET_DELTA)
    {
        offset = +UTC_OFFSET_DELTA;
    }
    timeService->SetRemoteOffset(Timespan(offset, 0, 0, 0));

    UpdateEditMode();
}

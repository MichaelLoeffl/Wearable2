#ifndef ICONS_H_F87CDE
#define ICONS_H_F87CDE

extern "C"
{
#include "../../lvgl/src/lv_objx/lv_img.h"

    extern lv_img_dsc_t AlarmAppIcon;
    extern lv_img_dsc_t AltitudeAppIcon;
    extern lv_img_dsc_t BluetoothAppIcon;
    extern lv_img_dsc_t CalendarAppIcon;
    extern lv_img_dsc_t DateAppIcon;
    extern lv_img_dsc_t HeartrateAppIcon;
    extern lv_img_dsc_t MailAppIcon;
    extern lv_img_dsc_t MastercardAppIcon;
    extern lv_img_dsc_t MessagesAppIcon;
    extern lv_img_dsc_t NavigationAppIcon;
    extern lv_img_dsc_t PaymentAppIcon;
    extern lv_img_dsc_t PedometerAppIcon;
    extern lv_img_dsc_t PhoneAppIcon;
    extern lv_img_dsc_t PointConnectAppIcon;
    extern lv_img_dsc_t ReminderAppIcon;
    extern lv_img_dsc_t SettingsAppIcon;
    extern lv_img_dsc_t StopwatchAppIcon;
    extern lv_img_dsc_t TimerAppIcon;
    extern lv_img_dsc_t TurnLeftAppIcon;
    extern lv_img_dsc_t TurnRightAppIcon;
    extern lv_img_dsc_t WatchLocalAppIcon;
    extern lv_img_dsc_t WatchLocalAppIcon;
    extern lv_img_dsc_t WatchWorldAppIcon;
    extern lv_img_dsc_t EnergyAppIcon;
    extern lv_img_dsc_t FlightModeAppIcon;
    extern lv_img_dsc_t PaymentAppIcon;
    extern lv_img_dsc_t AboutAppIcon;
    extern lv_img_dsc_t RunningAppIcon;
    extern lv_img_dsc_t WalkingAppIcon;
    extern lv_img_dsc_t CyclingAppIcon;
    extern lv_img_dsc_t StrengthAppIcon;
    extern lv_img_dsc_t YogaAppIcon;
    extern lv_img_dsc_t OtherActivityAppIcon;
    extern lv_img_dsc_t RecentActivityAppIcon;
    extern lv_img_dsc_t CameraAppIcon;

    // AppIconsSimple
    extern lv_img_dsc_t AlarmAppIconSimple;
    extern lv_img_dsc_t AltitudeAppIconSimple;
    extern lv_img_dsc_t CalendarAppIconSimple;
    extern lv_img_dsc_t DateAppIconSimple;
    extern lv_img_dsc_t HeartrateAppIconSimple;
    extern lv_img_dsc_t MailAppIconSimple;
    extern lv_img_dsc_t MessagesAppIconSimple;
    extern lv_img_dsc_t NavigationAppIconSimple;
    extern lv_img_dsc_t PedometerAppIconSimple;
    extern lv_img_dsc_t PhoneAppIconSimple;
    extern lv_img_dsc_t ReminderAppIconSimple;
    extern lv_img_dsc_t SettingsAppIconSimple;
    extern lv_img_dsc_t StopwatchAppIconSimple;
    extern lv_img_dsc_t TimerAppIconSimple;
    extern lv_img_dsc_t WatchLocalAppIconSimple;
    extern lv_img_dsc_t WatchWorldAppIconSimple;
    extern lv_img_dsc_t WhatsAppAppIconSimple;
    extern lv_img_dsc_t EnergyAppIconSimple;
    extern lv_img_dsc_t FlightModeAppIconSimple;
    extern lv_img_dsc_t RunningAppIconSimple;
    extern lv_img_dsc_t WalkingAppIconSimple;
    extern lv_img_dsc_t CyclingAppIconSimple;
    extern lv_img_dsc_t StrengthAppIconSimple;
    extern lv_img_dsc_t YogaAppIconSimple;
    extern lv_img_dsc_t OtherActivityAppIconSimple;
    extern lv_img_dsc_t AltitudeUpAppIconSimple;
    extern lv_img_dsc_t AltitudeDownAppIconSimple;
    extern lv_img_dsc_t PaymentAppIconSimple;
    extern lv_img_dsc_t CaloriesAppIconSimple;
    extern lv_img_dsc_t CameraAppIconSimple;

    // Activities
    extern lv_img_dsc_t RunningActivity;
    extern lv_img_dsc_t WalkingActivity;
    extern lv_img_dsc_t CyclingActivity;
    extern lv_img_dsc_t StrengthActivity;
    extern lv_img_dsc_t YogaActivity;
    extern lv_img_dsc_t OtherActivityActivity;

    // Components
    extern lv_img_dsc_t ActionButtonLeftControl;
    extern lv_img_dsc_t ActionButtonRightControl;
    extern lv_img_dsc_t ActionButtonLeftHighlightControl;
    extern lv_img_dsc_t ActionButtonRightHighlightControl;

    // ActionView
    extern lv_img_dsc_t CancelActionView;
    extern lv_img_dsc_t ConfirmActionView;
    extern lv_img_dsc_t PauseActionView;
    extern lv_img_dsc_t PlayActionView;
    extern lv_img_dsc_t ReplyActionView;
    extern lv_img_dsc_t ResetActionView;
    extern lv_img_dsc_t PlusActionView;
    extern lv_img_dsc_t MinusActionView;
    extern lv_img_dsc_t StopActionView;
    extern lv_img_dsc_t CameraActionView;

    // Status
    extern lv_img_dsc_t NotificationStatus;
    extern lv_img_dsc_t NotificationBlackBorderStatus;
    extern lv_img_dsc_t Battery0Status;
    extern lv_img_dsc_t ChargingStatus;
    extern lv_img_dsc_t PowerSavingStatus;
    extern lv_img_dsc_t FlightModeStatus;
    extern lv_img_dsc_t PagerActiveStatus;
    extern lv_img_dsc_t PagerInactiveStatus;

    // Navigation
    extern lv_img_dsc_t FinishNavigation;
    extern lv_img_dsc_t GotoStartNavigation;
    extern lv_img_dsc_t OutOfRouteNavigation;

    extern lv_img_dsc_t ForkLeftNavigation;
    extern lv_img_dsc_t ForkRightNavigation;
    extern lv_img_dsc_t LeftNavigation;
    extern lv_img_dsc_t RightNavigation;
    extern lv_img_dsc_t RoundaboutExitLeftNavigation;
    extern lv_img_dsc_t RoundaboutExitRightNavigation;
    extern lv_img_dsc_t RoundaboutClockwiseExit1of1Navigation;
    extern lv_img_dsc_t RoundaboutClockwiseExit1of2Navigation;
    extern lv_img_dsc_t RoundaboutClockwiseExit1of3Navigation;
    extern lv_img_dsc_t RoundaboutClockwiseExit2of2Navigation;
    extern lv_img_dsc_t RoundaboutClockwiseExit2of3Navigation;
    extern lv_img_dsc_t RoundaboutClockwiseExit3of3Navigation;
    extern lv_img_dsc_t RoundaboutCounterClockwiseExit1of1Navigation;
    extern lv_img_dsc_t RoundaboutCounterClockwiseExit1of2Navigation;
    extern lv_img_dsc_t RoundaboutCounterClockwiseExit1of3Navigation;
    extern lv_img_dsc_t RoundaboutCounterClockwiseExit2of2Navigation;
    extern lv_img_dsc_t RoundaboutCounterClockwiseExit2of3Navigation;
    extern lv_img_dsc_t RoundaboutCounterClockwiseExit3of3Navigation;
    extern lv_img_dsc_t RoundaboutFallbackNavigation;
    extern lv_img_dsc_t SharpLeftNavigation;
    extern lv_img_dsc_t SharpRightNavigation;
    extern lv_img_dsc_t SlightLeftNavigation;
    extern lv_img_dsc_t SlightRightNavigation;
    extern lv_img_dsc_t StartNavigation;
    extern lv_img_dsc_t StraightNavigation;
    extern lv_img_dsc_t UTurnNavigation;
}

#endif /* ICONS_H_F87CDE */

#ifndef DEBUG_UART_H
#define DEBUG_UART_H

#include "Common/Common.h"
#include "Common/Debugging.h"

CFUNC void _putchar(char character);
CFUNC void UartInitialize();
CFUNC void UartPut(char character);
CFUNC void UartPush(const char* data, size_t length);
CFUNC size_t UartReadBuffer(char* buffer, size_t length);
CFUNC bool UartRead(char* character);
CFUNC void UpdateDebugUart();
CFUNC bool UartInitialized();
CFUNC void UartFlush();
CFUNC bool UartActive();
CFUNC bool DebugUartGoToSleep();
CFUNC void DebugUartWakeUp();

#endif // DEBUG_UART_H

#ifndef GPIOHELPER_H
#define GPIOHELPER_H

#include "../Common/Common.h"

#define PIN(port, pinnumber)            ((uint16_t)(((port & 0x1F) << 3) | (pinnumber & 0x7)))
#define LOW_ACTIVE(port, pinnumber)     (PIN(port, pinnumber))
#define HIGH_ACTIVE(port, pinnumber)    (PIN(port, pinnumber) | 0x8000)
#define IS_LOW_ACTIVE(pin)              (uint16_t)((pin & 0x8000) == 0)
#define IS_HIGH_ACTIVE(pin)             (uint16_t)((pin & 0x8000) != 0)

#define GET_PIN_NUMBER(pin)             (pin & 0x07)
#define GET_PORT_NUMBER(pin)            ((pin >> 3) & 0x1F)

#define NO_PIN  (0)

#define INIT_PIN_ARRAY(x)   { uint16_t temp_pin_array[] = { x, NO_PIN }; GpiosInitializeOutput(temp_pin_array); }
#define SET_PIN_ARRAY(x)    { uint16_t temp_pin_array[] = { x, NO_PIN }; GpiosSet(temp_pin_array); }
#define CLEAR_PIN_ARRAY(x)  { uint16_t temp_pin_array[] = { x, NO_PIN }; GpiosClear(temp_pin_array); }

CFUNC void GpioInitializeOutput(uint16_t pin);
CFUNC void GpioSet(uint16_t pin);
CFUNC void GpioClear(uint16_t pin);

CFUNC void* GetPortAddress(uint16_t pin);

#define GetPort(x) (GPIO_PRT_Type*)GetPortAddress(x)

CFUNC void GpiosInitializeOutput(const uint16_t* pinArray);
CFUNC void GpiosSet(const uint16_t* pinArray);
CFUNC void GpiosClear(const uint16_t* pinArray);

#endif // GPIOHELPER_H

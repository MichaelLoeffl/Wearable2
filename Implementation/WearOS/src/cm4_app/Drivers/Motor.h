#ifndef MOTOR_H
#define MOTOR_H

#include "Common/Common.h"

CFUNC void MotorInit();
CFUNC void MotorStart();
CFUNC void MotorStop();

#endif /* MOTOR_H */

/*
 * AmbiLight.h
 *
 *  Created on: 21.05.2019
*  Author: Nadja Stärk
 *  Copyright (C) 2019 qinno GmbH - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited
 */

#ifndef POINT_TEC_AMBIENT_SENSOR_DRIVER_H
#define POINT_TEC_AMBIENT_SENSOR_DRIVER_H

#include "Common/Common.h"

#define APDS_9306_065_I2C_ADRESS 0x52U
#define APDS_9306_065_REG_MAIN_CTRL 0x00U
#define APDS_9306_065_REG_ALS_MEAS_RATE 0x04U
#define APDS_9306_065_REG_ALS_GAIN 0x05U
#define APDS_9306_065_REG_PART_ID 0x06U
#define APDS_9306_065_REG_MAIN_STATUS 0x07U
#define APDS_9306_065_REG_CLEAR_DATA_0 0x0AU
#define APDS_9306_065_REG_CLEAR_DATA_1 0x0BU
#define APDS_9306_065_REG_CLEAR_DATA_2 0x0CU
#define APDS_9306_065_REG_ALS_DATA_0 0x0DU
#define APDS_9306_065_REG_ALS_DATA_1 0x0EU
#define APDS_9306_065_REG_ALS_DATA_2 0x0FU
#define APDS_9306_065_REG_INT_CFG 0x19U
#define APDS_9306_065_REG_INT_PERSISTENCE 0x1AU
#define APDS_9306_065_REG_ALS_THRES_UP_0 0x21U
#define APDS_9306_065_REG_ALS_THRES_UP_1 0x22U
#define APDS_9306_065_REG_ALS_THRES_UP_2 0x23U
#define APDS_9306_065_REG_ALS_THRES_LOW_0 0x24U
#define APDS_9306_065_REG_ALS_THRES_LOW_1 0x25U
#define APDS_9306_065_REG_ALS_THRES_LOW_2 0x26U
#define APDS_9306_065_REG_ALS_THRES_VAR 0x27U

#define APDS_9306_065_PART_ID 0xB3U

bool AmbiLight_Identify();
bool AmbiLight_Init();
void OnBoard_Ambi_Init();
uint16_t OnBoard_Ambi_ReadData();
void OnBoard_Ambi_Disable();
uint32_t AmbiLight_ReadData();
uint32_t AmbiLight_ClearData();
uint8_t AmbiLight_Read8(uint8_t regAdr);
uint32_t AmbiLight_Read32(uint8_t regAdr);
void AmbiLight_Write8(uint8_t regAdr, uint8_t data);

#endif // POINT_TEC_AMBILIGHT_H

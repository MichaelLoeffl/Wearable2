#ifndef WATCHDOG_H
#define WATCHDOG_H

#include "Common/Common.h"

CFUNC void WatchdogInit();
CFUNC void WatchdogServe();

#endif

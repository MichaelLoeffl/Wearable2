#ifndef POINT_TEC_TOUCH_H
#define POINT_TEC_TOUCH_H

#include "Common/Common.h"

CFUNC void TouchInitialize();
CFUNC void TouchCleanup();
CFUNC void TouchUpdate(uint32_t deltaT);
CFUNC void TouchPostInit();

CFUNC bool TouchGetTouchPosition(uint32_t* x, uint32_t* y);

// Callbacks
CFUNC void TouchGestureCallback(uint16_t gesture);

#endif // POINT_TEC_TOUCH_H

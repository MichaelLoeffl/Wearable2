#ifndef FILE_SYSTEM_HPP
#define FILE_SYSTEM_HPP

#include "Common/Common.h"

#ifdef SIMULATOR
#include <fstream>
#include <iostream>

#else
#include <LittleFS.h>
#endif

enum class FileAccessFlags
{
    Read      = 1 << 0,
    Write     = 1 << 1,
    Append    = 1 << 2,
    Create    = 1 << 3,
    CreateNew = 1 << 4,
    Truncate  = 1 << 5
};

[[maybe_unused]] static int operator&(FileAccessFlags a, FileAccessFlags b)
{
    return (int)a & (int)b;
}

[[maybe_unused]] static FileAccessFlags operator|(FileAccessFlags a, FileAccessFlags b)
{
    return (FileAccessFlags)((int)a | (int)b);
}

void FileSystemInitialize();
void FileSystemCleanup();
void DeleteFile(const char* filename);

class File
{
public:
    File(const char* fileName, FileAccessFlags access);
    ~File();

    int32_t GetReadPosition();
    int32_t GetWritePosition();
    int32_t SeekAbsolute(int32_t position);
    int32_t SeekRelative(int32_t offset);
    int32_t SeekBack(int32_t offset);

    int32_t Read(void* buffer, uint32_t size);
    int32_t Write(const void* buffer, uint32_t size);

    bool IsOpen();

private:
#ifdef SIMULATOR
    std::fstream fileHandle;
#else
    lfs_file fileHandle;
    bool isOpen_;
#endif
};

#endif /* FILE_SYSTEM_HPP */

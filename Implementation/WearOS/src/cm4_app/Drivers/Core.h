#ifndef POINT_TEC_CORE_H
#define POINT_TEC_CORE_H

#include "Common/Common.h"
#include <stdbool.h>
#include <stdint.h>

#define FOREGROUND_DEFAULT_KEEP_AWAKE (10000)

#define SYSTICK_CLOCK_SPEED     (32767)

// Handler declaration without implementation
CFUNC void SystickInterruptHandler();

CFUNC void CoreInitialize();
CFUNC void CoreSleep(uint8_t sleepmode);
CFUNC void CoreEnableInterrupts();
CFUNC void CoreDisableInterrupts();
CFUNC void RestartIntoBootloader(uint32_t reason);
CFUNC void CoreShutdown();
CFUNC void CoreHibernate();
CFUNC void HaltDevice(uint8_t error);

CFUNC bool DeviceSleeping();
CFUNC bool DeviceInDeepSleep();
CFUNC void WakeFromDeepSleep();
CFUNC void WakeDeviceIntoSnooze();
CFUNC void DeviceUserIntervention(uint16_t keepAwake);

CFUNC uint16_t GetCoalitionId();
CFUNC void SetCoalitionId(uint16_t newId);

CFUNC void CoreEnableSysTick();
CFUNC void CoreDisableSysTick();

CFUNC uint32_t GetTickCount();

CFUNC void StopwatchStart(uint32_t* instance);
CFUNC uint32_t StopwatchStop(uint32_t* instance);

CFUNC void BusyLoopStart();
CFUNC void BusyLoopStop();

#if (__CORTEX_M == 0)
CFUNC void CoreStartCoreM4();
#endif

#endif // POINT_TEC_CORE_H

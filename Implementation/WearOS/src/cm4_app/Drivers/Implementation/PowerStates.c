#include "Drivers/PowerStates.h"
#include "Common/Debugging.h"
#include "Drivers/Display.h"
#include "Drivers/Core.h"
#include <cy_pdl.h>

void SwitchPowerState(PowerState state, PowerState previous)
{
    if (state > POWER_STATE_PASSIVE)
    {
        Cy_SysClk_ClkFastSetDivider(0);
    }
    else if (previous > POWER_STATE_PASSIVE)
    {
        LOG("PowerState: Deep Sleep");
        //Cy_SysClk_ClkFastSetDivider(255);
        DisplayTurnOff();
    }

    switch (state)
    {
    case POWER_STATE_DEEP_SLEEP:
        break;
    case POWER_STATE_BACKGROUND_PASSIVE:
    case POWER_STATE_PASSIVE:
        break;
    case POWER_STATE_WORKOUT:
        LOG("PowerState: Workout");
        DisplayTurnOff();
        break;
    case POWER_STATE_BACKGROUND_ACTIVE:
        LOG("PowerState: Background Active");
        DisplayTurnOff();

        break;
    case POWER_STATE_ACTIVE:
        LOG("PowerState: Active");
        DisplayTurnOn();
        break;
    case POWER_STATE_MAX:
        HaltDevice(0x50);
        break;
    }

    previous = state;
}

PowerState DetermineRequiredPowerState(PeriperalRequirementFlags requirementFlags)
{
    if (requirementFlags & (REQ_PERIPHERAL_DISPLAY))
    {
        return POWER_STATE_ACTIVE;
    }

    if (requirementFlags & (REQ_PERIPHERAL_FLASH_WRITE | REQ_PERIPHERAL_FLASH_READ))
    {
        return POWER_STATE_BACKGROUND_ACTIVE;
    }

    if (requirementFlags & (REQ_PERIPHERAL_HEARTRATE))
    {
        return POWER_STATE_WORKOUT;
    }

    if (requirementFlags & (REQ_PERIPHERAL_MOTOR))
    {
        return POWER_STATE_PASSIVE;
    }

    return POWER_STATE_ACTIVE;
}

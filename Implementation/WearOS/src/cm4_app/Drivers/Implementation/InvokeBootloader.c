#include "Drivers/InvokeBootloader.h"

#include <cy_pdl.h>
#include <cyhal.h>
#include <cybsp.h>

CFUNC void UartFlush();

CY_SECTION(".cy_boot_noinit.appId") __USED static uint8_t cy_bootload_appId;

void RestartDevice(void)
{
    LOG("RestartDevice called");
    UartFlush();
    NVIC_SystemReset();
}

void InvokeBootloader(void)
{
    LOG("InvokeBootloader called");
    UartFlush();
    cy_bootload_appId = 0;
    NVIC_SystemReset();
}

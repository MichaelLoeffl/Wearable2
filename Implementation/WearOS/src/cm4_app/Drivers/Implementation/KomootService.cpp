#include "Drivers/KomootService.h"
#include "AppSystemM4.hpp"
#include "Services/NavigationService.hpp"


int navigationConnectionAssumed = 0;

#pragma pack(1)
struct KomootData
{
    uint32_t Identifier;
    uint8_t Direction;
    uint32_t Distance;
    uint8_t Street[32];
};

CFUNC void KomootDataHandler(void* vptr)
{
    navigationConnectionAssumed = 15;
    KomootData* komootData      = (KomootData*)vptr;
    auto navService             = appSystem_.GetNavigationService();
    navService->Update(komootData->Identifier, komootData->Direction, komootData->Distance, komootData->Street);
}

CFUNC bool KomootConnectionCheck(bool peek)
{
    if (!peek && navigationConnectionAssumed > 0)
    {
        navigationConnectionAssumed--;
    }

    return navigationConnectionAssumed > 0;
}

#include "Drivers/AltimeterDriver.h"

uint32_t altimeterTestValue_ = 0;

/**
 * @brief this function initializes the air pressure sensor
 * @param none
 * @return none
 */
void HAL_Altimeter_Init()
{
}

/**
 * @brief this function disables the air pressure sensor
 * @param none
 * @return none
 */
void HAL_Altimeter_Disable()
{
}

/**
 * @brief this function gets the current temperature from the sensor
 * @param none
 * @return the measured temperature
 */
float HAL_Altimeter_GetTemperature()
{
    return 23.0;
}

/**
 * @brief this function measures the current pressure
 * @param none
 * @return the current pressure
 */
float HAL_Altimeter_GetPressure()
{
    return 1100;
}

/**
 * @brief this function calculates the altitude based on pressure and given seaLevel in hPa
 * @param The current hPa at sea level
 * @return The approximate altitude above sea level in meters
 */
float HAL_Altimeter_GetAltitude(float seaLevelhPa)
{
    altimeterTestValue_++;
    return 1250.63 + (altimeterTestValue_ % 20);
}

#include "DisplaySPI.h"
#include <cy_scb_spi.h>
#include <cy_sysclk.h>
#include <cy_gpio.h>
#include <cyhal_dma.h>
#include <cyhal_hw_types.h>
#include <cy_sysint.h>
#include "PinMapping.h"
#include "Drivers/Core.h"
#include "Drivers/Display.h"
#include <cycfg.h>

static cyhal_dma_t displayDMA_;
static cyhal_dma_cfg_t displayDmaConfig_ =
{
    .src_addr       = 0x0,                      // Start from address
    .src_increment  = 1,                        // Increment source by 1 byte
    .dst_addr       = 0x0,                      // Destination from address
    .dst_increment  = 0,                        // Increment destination by 0 bytes
    .transfer_width = 8,                        // 8 bit transfer
    .length         = 0x0,                      // Transfer length * 8Bits
    .burst_size     = 4,                        // Transfer 4 words a a time
    .action         = CYHAL_DMA_TRANSFER_BURST, // Notify when each burst is done
};

static bool initialized_ = false;
static bool dmaInitialized_ = false;
static bool dmaActive_ = false;
void dma_event_callback(void* callback_arg, cyhal_dma_event_t event);
void DisplaySpiInitiateDmaTransfer();

/* Allocate context for SPI operation */
static cy_stc_scb_spi_context_t spiContext;
static uint16_t fifoSize_;

/* Assign SPI interrupt number and priority */
#define SPI_INTR_NUM        ((IRQn_Type) scb_5_interrupt_IRQn)
#define SPI_INTR_PRIORITY   (7U)

#define SPI_TX_TRIGGER_LEVEL (4)
/* Populate configuration structure (code specific for CM4) */
const cy_stc_sysint_t spiIntrConfig =
{
    .intrSrc      = SPI_INTR_NUM,
    .intrPriority = SPI_INTR_PRIORITY,
};

static const uint8_t* transferPosition_;
static volatile uint16_t transferCount_;


cy_stc_dma_descriptor_config_t DisplayDMA_Descriptor_0_config =
        {
                .retrigger = CY_DMA_RETRIG_IM,
                .interruptType = CY_DMA_DESCR,
                .triggerOutType = CY_DMA_DESCR,
                .channelState = CY_DMA_CHANNEL_ENABLED,
                .triggerInType = CY_DMA_X_LOOP,
                .dataSize = CY_DMA_BYTE,
                .srcTransferSize = CY_DMA_TRANSFER_SIZE_DATA,
                .dstTransferSize = CY_DMA_TRANSFER_SIZE_DATA,
                .descriptorType = CY_DMA_2D_TRANSFER,
                .srcAddress = NULL,
                .dstAddress = NULL,
                .srcXincrement = 1,
                .dstXincrement = 0,
                .xCount = 112,
                .srcYincrement = 0,
                .dstYincrement = 0,
                .yCount = 128,
                .nextDescriptor = NULL,
        };
cy_stc_dma_descriptor_t DisplayDMA_Descriptor_0 =
        {
                .ctl = 0UL,
                .src = 0UL,
                .dst = 0UL,
                .xCtl = 0UL,
                .yCtl = 0UL,
                .nextPtr = 0UL,
        };
cy_stc_dma_channel_config_t DisplayDMA_channelConfig =
        {
                .descriptor = &DisplayDMA_Descriptor_0,
                .preemptable = false,
                .priority = 3,
                .enable = false,
                .bufferable = false,
        };

void Transfer()
{
    if (!dmaInitialized_)
    {
        uint32_t numToCopy = Cy_SCB_GetFifoSize(SCB5) - Cy_SCB_GetNumInTxFifo(SCB5);
        for (int i = 0; i < numToCopy && transferCount_ > 0; i++)
        {
            Cy_SCB_WriteTxFifo(SCB5, (uint32_t) *transferPosition_);
            transferPosition_++;
            transferCount_--;
        }
        if (transferCount_ > 0)
        {
            Cy_SCB_SetTxInterruptMask(SCB5, CY_SCB_TX_INTR_LEVEL);
            Cy_SCB_ClearTxInterrupt(SCB5, CY_SCB_TX_INTR_LEVEL);
        }
        else
        {
            Cy_SCB_SetTxInterruptMask(SCB5, 0);
        }
    }
    else
    {
        Cy_SCB_SetTxInterruptMask(SCB5, CY_SCB_TX_INTR_LEVEL);
        Cy_SCB_ClearTxInterrupt(SCB5, CY_SCB_TX_INTR_LEVEL);
    }
    /*else if (!cyhal_dma_is_busy(&displayDMA_))
    {
        cyhal_dma_configure(&displayDMA_, &displayDmaConfig_);
        cyhal_dma_start_transfer(&displayDMA_);
    }*/

}

void SPI_Isr()
{
    Transfer();
}

void DisplaySpiWriteSingleByte(uint8_t byte, bool command)
{
    Cy_SCB_WriteTxFifo(SCB5, (uint32_t)byte);
    while (!DisplaySpiTransferComplete());
}

void DisplaySpiTransfer()
{
    if (dmaInitialized_)
    {
        DisplaySpiInitiateDmaTransfer();
        return;
    }

    CoreDisableInterrupts();
    transferCount_ = DISPLAY_WIDTH * DISPLAY_HEIGHT * 2;
    transferPosition_ = (const uint8_t*)frontBuffer_;
    Transfer();
    CoreEnableInterrupts();
}

bool DisplaySpiTransferComplete()
{
    if (dmaInitialized_)
    {
        return !dmaActive_;
    }

    if (Cy_SCB_SPI_IsTxComplete(SCB5))
    {
        if (transferCount_ == 0)
        {
            return true;
        }
    }
    return false;

    //return !initialized_ || (Cy_SCB_SPI_IsTxComplete(SCB5) && transferCount_ == 0) && !dmaActive_;
}

cyhal_spi_t displaySpi_;

void DisplaySpiInitialize()
{
    /* Master configuration */
    cy_stc_scb_spi_config_t spiConfig =
    {
        .spiMode  = CY_SCB_SPI_MASTER,
        .subMode  = CY_SCB_SPI_MOTOROLA,
        .sclkMode = CY_SCB_SPI_CPHA0_CPOL0,
        .oversample = 5UL,
        .rxDataWidth              = 8UL,
        .txDataWidth              = 8UL,
        .enableMsbFirst           = true,
        .enableInputFilter        = false,
        .enableFreeRunSclk        = false,
        .enableMisoLateSample     = false,
        .enableTransferSeperation = false,
        .ssPolarity               = CY_SCB_SPI_ACTIVE_LOW,
        .enableWakeFromSleep      = false,
        .rxFifoTriggerLevel  = 0UL,
        .rxFifoIntEnableMask = 0UL,
        .txFifoTriggerLevel  = SPI_TX_TRIGGER_LEVEL,
        .txFifoIntEnableMask = 1UL,
        .masterSlaveIntEnableMask = 0UL,
    };

    #define SPI_MOSI    PIN(11, 0)
    #define SPI_SCLK    PIN(5, 2)
    #define SPI_SS      PIN(5, 4)

    /*cyhal_gpio_t mosi = (cyhal_gpio_t)CYHAL_GET_GPIO(GET_PORT_NUMBER(SPI_MOSI), GET_PIN_NUMBER(SPI_MOSI));
    cyhal_gpio_t sclk = (cyhal_gpio_t)CYHAL_GET_GPIO(GET_PORT_NUMBER(SPI_SCLK), GET_PIN_NUMBER(SPI_SCLK));
    cyhal_gpio_t ss = (cyhal_gpio_t)CYHAL_GET_GPIO(GET_PORT_NUMBER(SPI_SS), GET_PIN_NUMBER(SPI_SS));
    //cy_rslt_t result = cyhal_spi_init(&displaySpi_, (cyhal_gpio_t)mosi, NC, (cyhal_gpio_t)sclk, (cyhal_gpio_t)ss, (cyhal_uart_cfg_t*)&spiConfig);*/

    fifoSize_ = Cy_SCB_GetFifoSize(SCB5);
    /* Configure SPI to operate */
    (void) Cy_SCB_SPI_Init(SCB5, &spiConfig, &spiContext);



    /* Connect SCB1 SPI function to pins */
    Cy_GPIO_SetHSIOM(P11_0_PORT,    0,  P11_0_SCB5_SPI_MOSI);
    Cy_GPIO_SetHSIOM(P5_2_PORT,     2,  P5_2_SCB5_SPI_CLK);
    Cy_GPIO_SetHSIOM(P5_4_PORT,     4,  P5_4_SCB5_SPI_SELECT1);
    /* Configure SCB1 pins for SPI Master operation */
    Cy_GPIO_SetDrivemode(P11_0_PORT,    0,  CY_GPIO_DM_STRONG_IN_OFF);
    Cy_GPIO_SetDrivemode(P5_2_PORT,     2,  CY_GPIO_DM_STRONG_IN_OFF);
    Cy_GPIO_SetDrivemode(P5_4_PORT,     4,  CY_GPIO_DM_STRONG_IN_OFF);

    /* Assign divider type and number for SPI */
    #define SPI_CLK_DIV_TYPE    (CY_SYSCLK_DIV_8_BIT)
    #define SPI_CLK_DIV_NUM     (4U)
    /* Connect assigned divider to be a clock source for SPI */
    Cy_SysClk_PeriphAssignDivider(PCLK_SCB5_CLOCK, SPI_CLK_DIV_TYPE, SPI_CLK_DIV_NUM);
    /* SPI master desired data rate is 25 Mbps.
    * The SPI master data rate = (clk_scb / Oversample).
    * For clk_peri = 100 MHz, select divider value 1 and get SCB clock = (100 MHz / 1) = 100 MHz.
    * Select Oversample = 4. These setting results SPI data rate = 100 MHz / 4 = 25 Mbps.
    */
    Cy_SysClk_PeriphSetDivider   (SPI_CLK_DIV_TYPE, SPI_CLK_DIV_NUM, 0UL);
    Cy_SysClk_PeriphEnableDivider(SPI_CLK_DIV_TYPE, SPI_CLK_DIV_NUM);

    /* Hook interrupt service routine and enable interrupt */
    Cy_SysInt_Init(&spiIntrConfig, &SPI_Isr);
    NVIC_EnableIRQ(SPI_INTR_NUM);

    Cy_SCB_SetTxFifoLevel(SCB5,2);

    /* Enable SPI to operate */
    Cy_SCB_SPI_Enable(SCB5);

    Cy_SCB_SPI_SetActiveSlaveSelect(SCB5, CY_SCB_SPI_SLAVE_SELECT1);

    initialized_ = true;
}

void dma_event_callback(void* callback_arg, cyhal_dma_event_t event)
{
    bool space = (Cy_SCB_GetFifoSize(SCB5) - Cy_SCB_GetNumInTxFifo(SCB5) >= displayDmaConfig_.burst_size);
    bool time = !cyhal_dma_is_busy(&displayDMA_);

    // Do work specific to each burst complete
    // If all bursts are complete, start another
    if (time && space && event == CYHAL_DMA_TRANSFER_COMPLETE)
    {
        transferPosition_ += displayDmaConfig_.burst_size;
        transferCount_ -= displayDmaConfig_.burst_size;

        if (transferCount_ != 0)
        {
            cyhal_dma_start_transfer(&displayDMA_);
            //Cy_SCB_SetTxInterruptMask(SCB5, CY_SCB_TX_INTR_LEVEL);
            //Cy_SCB_ClearTxInterrupt(SCB5, CY_SCB_TX_INTR_LEVEL);
            return;
        }
    }

    dmaActive_ = false;
}

void DisplaySpiInitiateDmaTransfer()
{
    if (dmaInitialized_)
    {
        cy_rslt_t       rslt;
        dmaActive_ = true;
        displayDmaConfig_.src_addr = (uint32_t)frontBuffer_;
        transferCount_ = DISPLAY_WIDTH * DISPLAY_HEIGHT * 2;
        transferPosition_ = (const uint8_t*)frontBuffer_;

        dmaActive_ = true;
        cyhal_dma_start_transfer(&displayDMA_);
    }
}

#define DisplayDMA_ENABLED 1U
#define DisplayDMA_HW DW0
#define DisplayDMA_CHANNEL 1U
#define DisplayDMA_IRQ cpuss_interrupts_dw0_0_IRQn


void tx_dma_complete(void)
{
    Cy_DMA_Channel_ClearInterrupt(DisplayDMA_HW, DisplayDMA_CHANNEL);
}

void configure_display_dma()
{
    cy_en_dma_status_t dma_init_status;

    /* Init descriptor */
    dma_init_status = Cy_DMA_Descriptor_Init(&DisplayDMA_Descriptor_0, &DisplayDMA_Descriptor_0_config);
    if (dma_init_status!=CY_DMA_SUCCESS)
    {
        for (;;);
    }
    dma_init_status = Cy_DMA_Channel_Init(DisplayDMA_HW, DisplayDMA_CHANNEL, &DisplayDMA_channelConfig);
    if (dma_init_status!=CY_DMA_SUCCESS)
    {
        for (;;);
    }

    /* Set source and destination for descriptor 1 */
    Cy_DMA_Descriptor_SetSrcAddress(&DisplayDMA_Descriptor_0, (uint32_t *) frontBuffer_);
    Cy_DMA_Descriptor_SetDstAddress(&DisplayDMA_Descriptor_0, (uint32_t *) &SCB5->TX_FIFO_WR);

    /* Set next descriptor to NULL to stop the chain execution after descriptor 1
    *  is completed.
    */
    Cy_DMA_Descriptor_SetNextDescriptor(Cy_DMA_Channel_GetCurrentDescriptor(DisplayDMA_HW, DisplayDMA_CHANNEL), NULL);

    cy_stc_sysint_t DmaIntConfig =
    {
        .intrSrc      = (IRQn_Type)DisplayDMA_IRQ,
        .intrPriority = 7u,
    };


    /* Initialize and enable the interrupt from TxDma */
    Cy_SysInt_Init  (&DmaIntConfig, &tx_dma_complete);
    NVIC_EnableIRQ(DmaIntConfig.intrSrc);

    /* Enable DMA interrupt source */
    Cy_DMA_Channel_SetInterruptMask(DisplayDMA_HW, DisplayDMA_CHANNEL, CY_DMA_INTR_MASK);

    Cy_TrigMux_Connect(TRIG0_IN_TR_GROUP13_OUTPUT9, TRIG0_OUT_CPUSS_DW0_TR_IN0, false, TRIGGER_TYPE_LEVEL);
    Cy_TrigMux_Connect(TRIG13_IN_SCB5_TR_TX_REQ, TRIG13_OUT_TR_GROUP0_INPUT36, false, TRIGGER_TYPE_LEVEL);

    /* Enable Data Write block but keep channel disabled to not trigger
    *  descriptor execution because TX FIFO is empty and SCB keeps active level
    *  for DMA.
    */
    Cy_DMA_Enable(DisplayDMA_HW);
}

void TestBench()
{
    configure_display_dma();

    return;
    cy_rslt_t       rslt;
    cyhal_dma_t     dma;
    cyhal_dma_t     dma1;
    cyhal_dma_t     dma2;
    cyhal_dma_t     dma3;
    cyhal_dma_t     dma4;
    cyhal_dma_t     dma5;
    cyhal_dma_t     dma6;
    cyhal_dma_cfg_t cfg =
            {
                    .src_addr       = (uint32_t)backBuffer_,   // Start from address
                    .src_increment  = 1,                       // Increment source by 1 word
                    .dst_addr       = (uint32_t)frontBuffer_,  // Destination from address
                    .dst_increment  = 1,                       // Increment destination by 1 word
                    .transfer_width = 32,                      // 32 bit transfer
                    .length         = 0x10,                    // Transfer 64 bytes (16 transfers of 4 bytes)
                    .burst_size     = 0,                       // Transfer everything in a single burst
                    .action         = CYHAL_DMA_TRANSFER_FULL, // Notify when everything is done
            };


    for (int i = 0; i < DISPLAY_WIDTH * DISPLAY_HEIGHT; i++)
    {
        frontBuffer_[i] = i;
    }
    // Allocate the DMA channel for use
    rslt = cyhal_dma_init(&dma1, CYHAL_DMA_PRIORITY_HIGH, CYHAL_DMA_DIRECTION_MEM2MEM);
    rslt = cyhal_dma_init(&dma2, CYHAL_DMA_PRIORITY_HIGH, CYHAL_DMA_DIRECTION_MEM2MEM);
    rslt = cyhal_dma_init(&dma3, CYHAL_DMA_PRIORITY_HIGH, CYHAL_DMA_DIRECTION_MEM2MEM);
    rslt = cyhal_dma_init(&dma4, CYHAL_DMA_PRIORITY_HIGH, CYHAL_DMA_DIRECTION_MEM2MEM);
    rslt = cyhal_dma_init(&dma5, CYHAL_DMA_PRIORITY_HIGH, CYHAL_DMA_DIRECTION_MEM2MEM);
    rslt = cyhal_dma_init(&dma6, CYHAL_DMA_PRIORITY_HIGH, CYHAL_DMA_DIRECTION_MEM2MEM);
    rslt = cyhal_dma_init(&dma, CYHAL_DMA_PRIORITY_HIGH, CYHAL_DMA_DIRECTION_MEM2MEM);

    // Configure the channel for the upcoming transfer
    rslt = cyhal_dma_configure(&dma, &cfg);

    // Begin the transfer
    rslt = cyhal_dma_start_transfer(&dma);

    // If the DMA channel is no longer needed, it can be freed up for other uses
    while (cyhal_dma_is_busy(&dma))
    {
    }
    cyhal_dma_free(&dma);

    for (int i = 0; i < 64; i++)
    {
        CY_ASSERT(frontBuffer_[i] == backBuffer_[i]);
    }
}


void DisplaySpiInitializeDMA()
{
    TestBench();
    dmaInitialized_ = true;
    dmaActive_ = true;

    Cy_DMA_Channel_SetDescriptor(DisplayDMA_HW, DisplayDMA_CHANNEL, &DisplayDMA_Descriptor_0);
    Cy_DMA_Channel_Enable(DisplayDMA_HW, DisplayDMA_CHANNEL);
    Cy_SCB_SetTxInterruptMask(SCB5, CY_SCB_TX_INTR_LEVEL);
    Cy_SCB_ClearTxInterrupt(SCB5, CY_SCB_TX_INTR_LEVEL);
    return;

    cy_rslt_t       rslt;
    cyhal_dma_t     dma;

    displayDmaConfig_.src_addr       = (uint32_t)backBuffer_;                   // Start from address
    displayDmaConfig_.src_increment  = 1;                                       // Increment source by 1 byte
    displayDmaConfig_.dst_addr       = (uint32_t)frontBuffer_;                  // Destination address
    displayDmaConfig_.dst_increment  = 1;                                       // Destination address does not change
    displayDmaConfig_.transfer_width = 16;                                       // 8 bit transfer
    displayDmaConfig_.length         = DISPLAY_WIDTH * DISPLAY_HEIGHT;          // Transfer full display (2 bytes per pixel)
    displayDmaConfig_.burst_size     = DISPLAY_WIDTH;                                       // Calculated based on fifo size
    displayDmaConfig_.action         = CYHAL_DMA_TRANSFER_FULL;                 // Notify when everything is done

    /*for (int i = (Cy_SCB_GetFifoSize(SCB5) - SPI_TX_TRIGGER_LEVEL); i > 0; i--)
    {
        if (displayDmaConfig_.length % i == 0)
        {
            displayDmaConfig_.burst_size = i;
            break;
        }
    }*/

    // Allocate the DMA channel for use
    rslt = cyhal_dma_init(&displayDMA_, CYHAL_DMA_PRIORITY_DEFAULT, CYHAL_DMA_DIRECTION_MEM2MEM);

    CY_ASSERT(rslt == CY_RSLT_SUCCESS);

    for (int i = 0; i < DISPLAY_WIDTH * DISPLAY_HEIGHT; i++)
    {
        frontBuffer_[i] = i;
    }

    // Configure the channel for the upcoming transfer
    rslt = cyhal_dma_configure(&displayDMA_, &displayDmaConfig_);

    CY_ASSERT(rslt == CY_RSLT_SUCCESS);

    dmaInitialized_ = true;
    dmaActive_ = true;

    // Begin the transfer
    rslt = cyhal_dma_start_transfer(&displayDMA_);

    CY_ASSERT(rslt == CY_RSLT_SUCCESS);

    while (cyhal_dma_is_busy(&displayDMA_))
    {

    }

    for (int i = 0; i < DISPLAY_WIDTH * DISPLAY_HEIGHT; i++)
    {
        CY_ASSERT(frontBuffer_[i] == backBuffer_[i]);
    }
}


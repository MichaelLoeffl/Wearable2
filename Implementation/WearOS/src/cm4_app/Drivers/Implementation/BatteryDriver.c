#include "deps/BatteryDriver.h"

uint32_t batterySimulatorValue_ = 100;

void BatteryInitialize()
{
    // dummy
}

void BatteryDisable()
{
    // dummy
}

void BatteryUpdate(int ms)
{
    (void)ms;
    // dummy
}

bool BatteryIsCharging()
{
    return false;
}

uint16_t BatteryGetPercent()
{
    batterySimulatorValue_ = batterySimulatorValue_ < 10 ? 100 : batterySimulatorValue_ - 10;
    return batterySimulatorValue_;
}

TBatteryState BatteryGetState()
{
    TBatteryState battState;

    battState.Charging   = 0;
    battState.ErrorCode  = 0;
    battState.GaugeValid = 1;
    battState.ReceiveCnt = 1;
    battState.Gauge      = 85;

    return (battState);
}

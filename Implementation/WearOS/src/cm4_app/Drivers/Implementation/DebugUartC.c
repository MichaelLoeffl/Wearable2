#include <cyhal.h>
#include <cyhal_uart.h>
#include <cy_gpio.h>
#include <cybsp.h>
#include <cyhal_gpio.h>
#include <cyhal_hwmgr.h>

#include "PinMapping.h"
#include "Drivers/Wakelock.h"
#include "Drivers/DebugUart.h"

cyhal_uart_t debugUart_;
bool uartDisabled_ = false;

void UartInterrupt()
{
    Cy_GPIO_ClearInterrupt(GPIO_PRT5, 0);
    NVIC_DisableIRQ(ioss_interrupts_gpio_5_IRQn);
    WakeDeviceIntoSnooze();
}

bool DebugUartGoToSleep()
{
    if (!UartInitialized())
    {
        return true;
    }

    if (UartActive())
    {
        return false;
    }
    uartDisabled_ = true;

    uint32_t port = GET_PORT_NUMBER(UART_RX_PIN);
    uint32_t pin = GET_PIN_NUMBER(UART_RX_PIN);

    if (port == 5)
    {
        Cy_SCB_UART_Disable(debugUart_.base, NULL);

        Cy_GPIO_SetDrivemode(Cy_GPIO_PortToAddr(port), pin, CY_GPIO_DM_HIGHZ);
        Cy_GPIO_SetInterruptEdge(Cy_GPIO_PortToAddr(port), pin, CY_GPIO_INTR_FALLING);
        Cy_GPIO_SetInterruptMask(Cy_GPIO_PortToAddr(port), pin, 1);
        Cy_GPIO_SetVtrip(Cy_GPIO_PortToAddr(port), pin, CY_GPIO_VTRIP_CMOS);

        /* Configure CM4+ CPU GPIO interrupt vector for Port 5 (RxD pin) */
        cy_stc_sysint_t intrCfg = {.intrSrc      = ioss_interrupts_gpio_5_IRQn, // Interrupt source is GPIO port 5 interrupt
                                   .intrPriority = 2UL};
        Cy_SysInt_Init(&intrCfg, UartInterrupt);
        NVIC_ClearPendingIRQ(intrCfg.intrSrc);
        Cy_GPIO_ClearInterrupt(GPIO_PRT5, 0);
        NVIC_EnableIRQ(intrCfg.intrSrc);
    }
    else
    {
        LOG("ERROR: Debug UART is not configured on port 5 which is required for wakeup");
    }

    return true;
}

void DebugUartWakeUp()
{
    if (UartInitialized())
    {
        return;
    }
    uint32_t port = GET_PORT_NUMBER(UART_RX_PIN);
    uint32_t pin = GET_PIN_NUMBER(UART_RX_PIN);

    if (port == 5)
    {
        Cy_GPIO_SetInterruptMask(Cy_GPIO_PortToAddr(port), pin, 0);
        Cy_GPIO_SetInterruptEdge(Cy_GPIO_PortToAddr(port), pin, CY_GPIO_INTR_DISABLE);
        Cy_GPIO_ClearInterrupt(GPIO_PRT5, 0);
        NVIC_DisableIRQ(ioss_interrupts_gpio_5_IRQn);

        Cy_SCB_UART_Enable(debugUart_.base);

        uartDisabled_ = false;
    }
    else
    {
        LOG("ERROR: Debug UART is not configured on port 5 which is required for wakeup");
    }
}

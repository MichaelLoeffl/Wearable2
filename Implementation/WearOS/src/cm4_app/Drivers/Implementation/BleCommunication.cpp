#include "Drivers/BleCommunication.h"
#include "AppSystemM4.hpp"
//#include "BLE/AppleNotificationCenterService.h"
#include "Common/AppIds.hpp"
#include "Notifications/NotificationService.hpp"

#define EMPTY_ID (0xFFFFFFFF)

struct NotificationIdMapping
{
    uint32_t NotificationID;
    uint32_t RemoteID;
};

static NotificationIdMapping idMap_[MAX_NOTIFICATIONS];

uint32_t GetNotificationID(uint32_t remoteID)
{
    for (uint16_t i = 0; i < MAX_NOTIFICATIONS; i++)
    {
        if (idMap_[i].RemoteID == remoteID && idMap_[i].NotificationID != 0)
        {
            return idMap_[i].NotificationID;
        }
    }

    return 0;
}

Notification* GetNotification(uint32_t remoteID)
{
    uint32_t notificationID  = GetNotificationID(remoteID);
    auto notificationService = appSystem_.GetNotificationService();
    auto notification        = notificationService->GetNotification(notificationID);
    return notification;
}

bool GetRemoteID(uint32_t notificationID, uint32_t* remoteID)
{
    if (notificationID == 0)
    {
        *remoteID = 0;
        return false;
    }

    for (uint16_t i = 0; i < MAX_NOTIFICATIONS; i++)
    {
        if (idMap_[i].NotificationID == notificationID)
        {
            *remoteID = idMap_[i].RemoteID;
            return false;
        }
    }

    return true;
}

void SetRemoteID(uint32_t notificationID, uint32_t remoteID)
{
    auto notificationService = appSystem_.GetNotificationService();

    for (uint16_t i = 0; i < MAX_NOTIFICATIONS; i++)
    {
        if (idMap_[i].NotificationID == notificationID)
        {
            idMap_[i].RemoteID = remoteID;
            return;
        }
    }

    for (uint16_t i = 0; i < MAX_NOTIFICATIONS; i++)
    {
        if (idMap_[i].NotificationID == 0)
        {
            idMap_[i].NotificationID = notificationID;
            idMap_[i].RemoteID       = remoteID;
            return;
        }
    }

    for (uint16_t i = 0; i < MAX_NOTIFICATIONS; i++)
    {
        if (notificationService->GetNotification(idMap_[i].NotificationID) == nullptr)
        {
            idMap_[i].NotificationID = notificationID;
            idMap_[i].RemoteID       = remoteID;
            return;
        }
    }
}

void AddNotification(AppIds appID, uint32_t remoteID)
{
    auto notificationService = appSystem_.GetNotificationService();
    auto notification        = GetNotification(remoteID);
    if (notification == nullptr)
    {
        notification = notificationService->AddNotification(appID);
    }

    if (notification != nullptr)
    {
        SetRemoteID(notification->Identifier, remoteID);
    }
}

void UpdateNotificationTitle(uint32_t remoteID, const unsigned char* title, const uint16_t length)
{
    auto notification = GetNotification(remoteID);
    if (notification != nullptr)
    {
        notification->SetTitle((char*)title, length);
    }
}

bool GetIncompleteNotification(uint32_t* remoteID)
{
    auto notificationService   = appSystem_.GetNotificationService();
    Notification* notification = notificationService->GetIncomplete();
    if (notification != nullptr)
    {
        GetRemoteID(notification->Identifier, remoteID);
        return true;
    }
    return false;
}

void UpdateNotificationMessage(uint32_t remoteID, const unsigned char* message, const uint16_t length)
{
    auto notification = GetNotification(remoteID);
    if (notification != nullptr)
    {
        notification->SetMessage((char*)message, length);
    }
}

void UpdateNotificationAppIdentifier(uint32_t remoteID, const unsigned char* message, const uint16_t length)
{
    auto notification = GetNotification(remoteID);
    if (notification != nullptr)
    {
        notification->SetAppIdentifier((char*)message, length);
    }
}

void UpdateHasPositiveAction(uint32_t remoteID, bool value)
{
    auto notification = GetNotification(remoteID);
    notification->SetHasPositiveAction(value);
}

void UpdateHasNegativeAction(uint32_t remoteID, bool value)
{
    auto notification = GetNotification(remoteID);
    notification->SetHasNegativeAction(value);
}

void RemoveAllNotifications()
{
    appSystem_.GetNotificationService()->RemoveAllNotifications();
}

void RemoveNotification(uint32_t remoteID)
{
    uint32_t notificationID = GetNotificationID(remoteID);
    appSystem_.GetNotificationService()->RemoveNotification(notificationID);
}

void SendPositiveNotificationAction(uint32_t notificationId)
{
    uint32_t remoteId;
    GetRemoteID(notificationId, &remoteId);
    //SendNotificationAction(remoteId, 0);
}

void SendNegativeNotificationAction(uint32_t notificationId)
{
    uint32_t remoteId;
    GetRemoteID(notificationId, &remoteId);
    //SendNotificationAction(remoteId, 1);
}

#include "PinMapping.h"
#include "Drivers/Wakelock.h"
#include "Drivers/Core.h"
#include "Meta.h"
#include "Drivers/Display.h"
#include "Drivers/Touch.h"
#include "Drivers/Buttons.h"
#include "Drivers/DebugUart.h"
#include "Drivers/Bluetooth.h"
#include "Drivers/Motor.h"
#include "LittleFS.h"
#include <cy_pdl.h>
#include <cyhal.h>
#include <cybsp.h>
#include "Drivers/Clocks.h"
#include <stdio.h>
#include <cyhal.h>
#include <cyhal_uart.h>
#include "Drivers/RtcDriver.h"
#include "Drivers/HeartrateDriver.h"
#include "Drivers/AmbientSensorDriver.h"
#include "Drivers/Touch.h"

// TODO: Split into 2 files for M4 and M0

#if (__CORTEX_M == 0)

extern uint8_t __cy_app_core1_start_addr; /**< core1 vector table address, if present */
CY_SECTION(".app_version") const uint8_t appVersion[APP_VERSION_MAX_LENGTH] = APP_VERSION;

#endif

uint32_t tickCounter = 0;
static uint16_t coalitionId_ = 0;

uint32_t GetTickCount()
{
    return tickCounter;
}

void StopwatchStart(uint32_t* instance)
{
    *instance = GetTickCount();
}

uint32_t StopwatchStop(uint32_t* instance)
{
    return GetTickCount() - *instance;
}

void BusyLoopStart()
{
    NOTIFY_BUSYLOOP_ACTIVE();
}

void BusyLoopStop()
{
    NOTIFY_BUSYLOOP_INACTIVE();
}

uint16_t GetCoalitionId()
{
    return coalitionId_;
}

void SetCoalitionId(uint16_t newId)
{
    coalitionId_ = newId;
}

void TestBench();

void CoreInitialize()
{
#if (__CORTEX_M == 4)
    CoreDisableInterrupts();

    cybsp_init();

    INITIALIZE_ALL_DEBUG_PINS();
    NOTIFY_CPU_ACTIVE();

    ClocksInit();

#ifdef UART_ENABLED
    UartInitialize();
#endif

    /* setting up SWV debug channel with 2MBaud */

    LOG("");
    LOG("");
    LOG("");
    LOG("------------------------------");
    LOG("Wear OS " APP_VERSION);
    LOG("------------------------------");

    //LOG_INLINE("Debugger... ");
    /*bool DebuggerAttached = false;//SWV_Setup(50000000, 2000000);
    if (DebuggerAttached)
    {
        Cy_SysLib_Delay(1000);
        // ESC c = reset terminal
        SWV_puts("\x1B"
                 "c"
                 "\x1B"
                 "c");
        Cy_SysLib_Delay(200);
        SWV_puts("\r\nSWV channel active...\r\n");
        Cy_SysLib_Delay(200);
        LOG("Attached");
    }
    else
    {
        LOG("Not attached");
    }*/

    LOG("Initializing SysTick");

    if(CY_SYSCLK_CLKLF_IN_ILO != Cy_SysClk_ClkLfGetSource())
    {
        Cy_SysClk_ClkLfSetSource(CY_SYSCLK_CLKLF_IN_ILO);
        LOG("Systick clock source set to ILO");
    }
    Cy_SysClk_IloHibernateOn(true);
    Cy_SysClk_IloEnable();

    Cy_SysTick_Init(CY_SYSTICK_CLOCK_SOURCE_CLK_LF, 32);
    Cy_SysTick_SetCallback(0, SystickInterruptHandler);
    Cy_SysTick_EnableInterrupt();

    Cy_SysClk_ClkBakSetSource(CY_SYSCLK_BAK_IN_CLKLF);

    Cy_SysTick_Enable();

    LOG("Initializing SMIF");
    /* Init SMIF to access external flash */

#ifdef DISPLAY_ENABLED
    LOG("Initializing Display");
    DisplayInitialize();
    DisplayTurnOn();
#endif


    LOG("Initializing Ambient Sensor");
    //AmbiLight_Init();
    //OnBoard_Ambi_Init();

    HeartrateInitialize();

    LOG("Systick clock source %d", Cy_SysTick_GetClockSource());

    LOG("Initializing Button Interface");
    ButtonInitialize();

    LOG("Initializing RTC");
    HAL_RTC_Init();



    CoreEnableInterrupts();
    //SmifInit();
    /* Put the device in XIP mode */
    //SmifEnterXipMode();

    /* C1. Stop the ILO to reduce current consumption */
    /*LOG("Stopping ILO");
    Cy_SysClk_IloDisable();*/

    /* TouchInitialize() needs interrupts to be enabled */
    LOG("Initializing Touch");
    TouchInitialize();

    LOG("Initializing I2C");
    /*if (I2C_Sens_OK()) // I2C not stalled
    {
        I2C_Sens_Start();
        LOG("Initializing StepCounter");
        HAL_StepCounter_Enable();
        LOG("Initializing Altimeter");
        HAL_Altimeter_Init();
    }*/

    Cy_SysClk_ClkHfDisable(1);
    Cy_SysClk_ClkHfDisable(3);
    Cy_SysClk_ClkHfDisable(4);

    MotorInit();
#ifdef CPU_ACTIVE_PIN
    cyhal_gpio_init(CPU_ACTIVE_PIN, CYHAL_GPIO_DIR_OUTPUT, CYHAL_GPIO_DRIVE_STRONG, CPU_ACTIVE_PIN_ON);
#endif

    LOG("Initialization done");

#else
    /* Unfreeze IO after Hibernate */
    if (Cy_SysPm_GetIoFreezeStatus())
    {
        Cy_SysPm_IoUnfreeze();
    }

    CoreEnableInterrupts();

    /* Start BLE Controller for dual core mode */
    Cy_BLE_Start(NULL);

    /* Enable CM4.  CY_CORTEX_M4_APPL_ADDR must be updated if CM4 memory layout is changed. */
    Cy_SysEnableCM4((uint32_t)(&__cy_app_core1_start_addr));
#endif
}

void CoreEnableSysTick()
{
    /*CoreDisableInterrupts();
    //CapSense_ModClk_Enable();

    Cy_SysClk_IloHibernateOn(true);
    Cy_SysTick_Enable();*/
}

void CoreDisableSysTick()
{
    /*CoreDisableInterrupts();
    //CapSense_ModClk_Disable();
    Cy_SysClk_IloHibernateOn(false);
    Cy_SysTick_Disable();*/
}

//extern const cy_stc_sysint_t SMIF_1_SMIF_IRQ_cfg;

void EnterDeepSleep()
{
    NOTIFY_CPU_INACTIVE();
    if (DebugUartGoToSleep())
    {
        Cy_SysPm_DeepSleep(CY_SYSPM_WAIT_FOR_INTERRUPT);
        DebugUartWakeUp();
    }
    NOTIFY_CPU_ACTIVE();
}

void CoreSleep(uint8_t sleepmode)
{
    // TODO: Sleep modes
#if (defined CPU_CORTEX_M4) || (__CORTEX_M == 4)

    if (!DeviceSleeping() || !DebugUartGoToSleep())
    {
        return;
    }

    //NVIC_DisableIRQ(SMIF_1_SMIF_IRQ_cfg.intrSrc); /* Disable the SMIF interrupt */
    //Cy_SMIF_Disable(SMIF_1_HW);

    //Cy_SysClk_ClkHfDisable(2);
    //Cy_SysClk_FllDisable();

    //Cy_SysPm_SystemSetMinRegulatorCurrent();
    //Cy_SysPm_BuckSetVoltage1(CY_SYSPM_BUCK_OUT1_VOLTAGE_0_9V);

    Cy_SysTick_Disable();

    NOTIFY_CPU_INACTIVE();

    do
    {
        EnterDeepSleep();
    } while (DeviceSleeping());
    NOTIFY_CPU_ACTIVE();

    Cy_SysTick_Enable();

    //Cy_SysPm_BuckSetVoltage1(CY_SYSPM_BUCK_OUT1_VOLTAGE_1_1V);
    //Cy_SysPm_SystemSetNormalRegulatorCurrent();

    //Cy_SysClk_FllEnable(200000u);
    //Cy_SysClk_ClkHfEnable(2);
    //Cy_SMIF_Enable(SMIF_1_HW, &SMIF_1_context);

    //NVIC_EnableIRQ(SMIF_1_SMIF_IRQ_cfg.intrSrc); /* Finally, Enable the SMIF interrupt */
#else
    Cy_SysPm_DeepSleep(CY_SYSPM_WAIT_FOR_INTERRUPT);
#endif
}

void CoreShutdown()
{
#if (defined CPU_CORTEX_M4) || (__CORTEX_M == 4)
    LOG("Shutting down...");
    DisplayTurnOff();
    TouchCleanup();

    BleShutdownBlocking();

    CoreHibernate();
#endif
}

void WakeFromDeepSleep()
{

}

static uint16_t interruptDisableCount_ = 0;

void CoreEnableInterrupts()
{
    interruptDisableCount_--;
    if (interruptDisableCount_ == 0)
    {
        NOTIFY_ISR_ENABLED();
        __enable_irq();
    }
    else if (interruptDisableCount_ >= 0xFFFE)
    {
        for(;;);
    }
}

void CoreDisableInterrupts()
{
    __disable_irq();
    interruptDisableCount_++;
    NOTIFY_ISR_DISABLED();
}

void RestartIntoBootloader(uint32_t reason)
{
    //Cy_BLE_Stop();
    //Cy_BLE_StackShutdown();

    // TODO: Leave shutdown to stack handler and wait for it to shut down properly
    BleShutdownBlocking();

    //InvokeBootloader();
}

void CoreHibernate()
{
    LOG("Putting cores into hibernation");
    UartFlush();
    Cy_SysPm_Hibernate();
}

void HaltDevice(uint8_t error)
{
#if (__CORTEX_M == 4)
    DisplayError(error);
    LOG("Device halted");
#endif

    Cy_SysLib_Delay(10);

    CoreHibernate();
    for (;;)
    {
        // Wait for watchdog
    }
}

#if (__CORTEX_M == 0)
void CoreStartCoreM4()
{
    Cy_SysEnableCM4(CY_CORTEX_M4_APPL_ADDR);
}
#endif

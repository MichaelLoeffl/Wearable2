#include "BleCommon.hpp"

extern "C"
{
#include <cy_ble_gap.h>
#include <cy_ble_stack.h>
#include <cybsp.h>
#include "GeneratedSource/cycfg_ble.h"
#include <cyhal.h>
}

bool CheckBleErrorInternal_(cy_en_ble_api_result_t result, const char* info)
{
#define CheckError(x)                   \
    else if (result == x)               \
    {                                   \
        LOG("%s API Error: " #x, info); \
        return true;                    \
    }

    if (result == CY_BLE_SUCCESS)
    {
        LOG("%s succeeded", info);
        return false;
    }
    CheckError(CY_BLE_SUCCESS)
    CheckError(CY_BLE_INFO_FLASH_WRITE_IN_PROGRESS)
    CheckError(CY_BLE_ERROR_INVALID_PARAMETER)
    CheckError(CY_BLE_ERROR_INVALID_OPERATION)
    CheckError(CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED)
    CheckError(CY_BLE_ERROR_INSUFFICIENT_RESOURCES)
    CheckError(CY_BLE_ERROR_OOB_NOT_AVAILABLE)
    CheckError(CY_BLE_ERROR_NO_CONNECTION)
    CheckError(CY_BLE_ERROR_NO_DEVICE_ENTITY)
    CheckError(CY_BLE_ERROR_DEVICE_ALREADY_EXISTS)
    CheckError(CY_BLE_ERROR_REPEATED_ATTEMPTS)
    CheckError(CY_BLE_ERROR_GAP_ROLE)
    CheckError(CY_BLE_ERROR_SEC_FAILED)
    CheckError(CY_BLE_ERROR_L2CAP_PSM_WRONG_ENCODING)
    CheckError(CY_BLE_ERROR_L2CAP_PSM_ALREADY_REGISTERED)
    CheckError(CY_BLE_ERROR_L2CAP_PSM_NOT_REGISTERED)
    CheckError(CY_BLE_ERROR_L2CAP_CONNECTION_ENTITY_NOT_FOUND)
    CheckError(CY_BLE_ERROR_L2CAP_PSM_NOT_IN_RANGE)
    CheckError(CY_BLE_ERROR_UNSUPPORTED_FEATURE_OR_PARAMETER_VALUE)
    CheckError(CY_BLE_ERROR_FLASH_WRITE_NOT_PERMITED)
    CheckError(CY_BLE_ERROR_FLASH_WRITE)
    CheckError(CY_BLE_ERROR_MIC_AUTH_FAILED)
    CheckError(CY_BLE_ERROR_HARDWARE_FAILURE)
    CheckError(CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE)
    CheckError(CY_BLE_ERROR_NTF_DISABLED)
    CheckError(CY_BLE_ERROR_IND_DISABLED)
    CheckError(CY_BLE_ERROR_CHAR_IS_NOT_DISCOVERED)
    CheckError(CY_BLE_ERROR_CONTROLLER_BUSY)
    CheckError(CY_BLE_ERROR_INVALID_STATE)

    LOG("%s API Error: 0x%x", result);
    return true;
}

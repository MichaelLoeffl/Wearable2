#include "Common/Common.h"
#include "BleCommon.hpp"

#ifndef SIMULATOR

//#include "project.h"
extern cy_stc_ble_conn_handle_t appConnHandle;

void UpdateReadCharacteristic(uint8_t* val, uint16_t length)
{
    /* Store data in database */
    cy_stc_ble_gatts_db_attr_val_info_t dbAttrValInfo;
    dbAttrValInfo.handleValuePair.attrHandle = CY_BLE_WATCH_CONTROL_SETTINGSREADER_CHAR_HANDLE;
    dbAttrValInfo.handleValuePair.value.len  = length;
    dbAttrValInfo.handleValuePair.value.val  = val;
    dbAttrValInfo.offset                     = 0u;
    dbAttrValInfo.flags                      = CY_BLE_GATT_DB_LOCALLY_INITIATED;

    if (Cy_BLE_GATTS_WriteAttributeValueCCCD(&dbAttrValInfo) != CY_BLE_GATT_ERR_NONE)
    {
        Cy_BLE_GATTS_SendNotification(&appConnHandle, &dbAttrValInfo.handleValuePair);
    }
}

#else // SIMULATOR

void UpdateReadCharacteristic(uint8_t* val, uint16_t length)
{
    (void)val;
    (void)length;

    printf("-> ");
    printf((const char*)val);
    printf("\n");
}

#endif

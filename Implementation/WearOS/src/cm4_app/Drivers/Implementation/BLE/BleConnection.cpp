#include "BleConnection.hpp"

extern cy_stc_ble_conn_handle_t appConnHandle;

CFUNC bool App_IsDeviceInBondList(uint8_t bdHandle);

namespace BluetoothLowEnergy {

BleConnection::BleConnection()
{

}

void BleConnection::OnGattConnectionIndication(cy_stc_ble_conn_handle_t* connectionHandle)
{
    appConnHandle = *connectionHandle;
}

void BleConnection::OnConnected(uint8_t bdHandle)
{
    connectionHandle_ = bdHandle;
    connected_ = true;

    /* Set security keys for new device which is not already bonded */
    if (App_IsDeviceInBondList(connectionHandle_) == 0u)
    {
        keyInfo.SecKeyParam.bdHandle = connectionHandle_;
        CyBleCheckForError(Cy_BLE_GAP_SetSecurityKeys(&keyInfo));
    }
}

void BleConnection::OnDisconnected()
{
    connectionHandle_ = 0;
    connected_ = false;
}

bool BleConnection::IsActive()
{
    return connected_;
}

uint8_t BleConnection::GetConnectionHandle()
{
    return connectionHandle_;
}

void BleConnection::OnConnectionComplete(cy_stc_ble_gap_enhance_conn_complete_param_t* params)
{
    LOG("Device %d connected", params->bdHandle);
}



} // namespace BluetoothLowEnergy

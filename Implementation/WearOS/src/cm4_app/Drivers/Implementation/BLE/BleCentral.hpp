#ifndef DRIVERS_IMPLEMENTATION_BLE_BLECENTRAL_HPP_
#define DRIVERS_IMPLEMENTATION_BLE_BLECENTRAL_HPP_

namespace BluetoothLowEnergy
{

class BleCentral
{
public:
    BleCentral();
    virtual ~BleCentral();
};

}

#endif // DRIVERS_IMPLEMENTATION_BLE_BLECENTRAL_HPP_

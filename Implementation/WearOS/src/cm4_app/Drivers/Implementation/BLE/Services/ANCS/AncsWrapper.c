#include "AppleNotificationCenterService.h"
#include "ancsc.h"
#include <cy_ble_event_handler.h>
#include "../../BleCommon.hpp"

cy_stc_ble_conn_handle_t appConnHandle;

static bool startup = true;

extern bool Authenticated;

void AncsCallbackFunction(uint32_t event, void* eventParam)
{
    volatile cy_stc_ble_ancs_char_value_t* param = (cy_stc_ble_ancs_char_value_t*)eventParam;

    switch (event)
    {
        case CY_BLE_EVT_ANCSC_NOTIFICATION:
        {
            if (param->charIndex == CY_BLE_ANCS_NS)
            {
                HandleAncsMessage(event, param->value->val, param->value->len, param->value->actualLen);
            }
            else if (param->charIndex == CY_BLE_ANCS_DS)
            {
                HandleAncsAttributeData(event, param->value->val, param->value->len, param->value->actualLen);
            }
            break;
        }
        case CY_BLE_EVT_ANCSC_WRITE_DESCR_RESPONSE:
        {
            if (((cy_stc_ble_ancs_char_value_t*)eventParam)->charIndex == CY_BLE_ANCS_NS) /* If NS notification is enabled */
            {
                uint16_t cccd;
                /* Enable Data Source notification */
                cccd = CY_BLE_CCCD_NOTIFICATION;

                if (CyBleCheckForError(Cy_BLE_ANCSC_SetCharacteristicDescriptor(appConnHandle, CY_BLE_ANCS_DS, CY_BLE_ANCS_CCCD, CY_BLE_CCCD_LEN, (uint8_t*)&cccd)))
                {
                    startup = true;
                }
                else
                {
                    LOG("CCCD write request: 0x%2.2x", cccd);
                }
            }
            break;
        }
    }
}

void SendUartNotificationRequest(uint8_t* buffer, uint16_t size)
{
    UartSendData("ANCS_NotificationRequest", buffer, size);
}

void SendNotificationRequest(uint8_t* buffer, uint16_t size)
{
    (void)Cy_BLE_ANCSC_SetCharacteristicValue(appConnHandle, CY_BLE_ANCS_CP, size, buffer);
}

void AncsStart()
{
    startup = true;
}

void AncsDisconnect()
{
}

void AncsProcess()
{
    static uint8_t timeout = 0;

    if (Cy_BLE_GetConnectionState(appConnHandle) < CY_BLE_CONN_STATE_CONNECTED
        || !Authenticated)
    {
        HandleAncsProcess(false);
        return;
    }

    if (timeout > 0)
    {
        timeout--;
        return;
    }

    if (startup)
    {
        uint16_t cccd = CY_BLE_CCCD_NOTIFICATION;
        if (CyBleCheckForError(Cy_BLE_ANCSC_SetCharacteristicDescriptor(appConnHandle, CY_BLE_ANCS_NS, CY_BLE_ANCS_CCCD, CY_BLE_CCCD_LEN, (uint8_t*)&cccd)))
        {
            startup = true;
            timeout = 100;
        }
        else
        {
            startup = false;
        }
    }
    else
    {
        HandleAncsProcess(true);
    }
}

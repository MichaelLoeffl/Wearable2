#ifndef APPLENOTIFICATIONCENTERSERVICE_H
#define APPLENOTIFICATIONCENTERSERVICE_H

#include "Common/Common.h"

CFUNC void AncsCallbackFunction(uint32_t event, void* eventParam);
CFUNC void HandleAncsMessage(uint32_t event, uint8_t* value, uint16_t len, uint16_t actualLen);
CFUNC void HandleAncsAttributeData(uint32_t event, uint8_t* value, uint16_t len, uint16_t actualLen);
CFUNC void HandleAncsStart();
CFUNC void HandleAncsProcess(bool connected);

CFUNC void SendNotificationRequest(uint8_t* buffer, uint16_t size);
CFUNC void SendNotificationAction(uint32_t notificationId, uint8_t action);
CFUNC void AncsStart();

#endif /* APPLENOTIFICATIONCENTERSERVICE_H */

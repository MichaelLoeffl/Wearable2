#include "AppleNotificationCenterService.h"
#include "Drivers/BleCommunication.h"
#include "Drivers/Core.h"

enum class CategoryID
{
    Other              = 0,
    IncomingCall       = 1,
    MissedCall         = 2,
    Voicemail          = 3,
    Social             = 4,
    Schedule           = 5,
    Email              = 6,
    News               = 7,
    HealthAndFitness   = 8,
    BusinessAndFinance = 9,
    Location           = 10,
    Entertainment      = 11
};

enum class EventID
{
    NotificationAdded    = 0,
    NotificationModified = 1,
    NotificationRemoved  = 2
};

enum class EventFlags
{
    Silent         = (1 << 0),
    Important      = (1 << 1),
    PreExisting    = (1 << 2),
    PositiveAction = (1 << 3),
    NegativeAction = (1 << 4)
};

enum class CommandID
{
    GetNotificationAttributes = 0,
    GetAppAttributes          = 1,
    PerformNotificationAction = 2
};

enum class NotificationAttributeID
{
    AppIdentifier       = 0,
    Title               = 1,
    Subtitle            = 2,
    Message             = 3,
    MessageSize         = 4,
    Date                = 5,
    PositiveActionLabel = 6,
    NegativeActionLabel = 7
};

enum class AppAttributeID
{
    DisplayName = 0
};

enum ActionID
{
    Positive = 0,
    Negative = 1
};

#pragma pack(1)
struct AncsNotificationMessage
{
    uint8_t EventId;
    uint8_t EventFlags;
    uint8_t CategoryId;
    uint8_t CategoryCount;
    uint32_t NotificationUID;
};

#pragma pack(1)
struct AncsPerformNotificationActionCommand
{
    uint8_t CommandId;
    uint32_t NotificationUID;
    uint8_t Action;
};

#pragma pack(1)
struct AncsGetNotificationAttributeCommand
{
    uint8_t CommandId;
    uint32_t NotificationUID;
    uint8_t AttributeList[0];
};

#pragma pack(1)
struct NotificationAttribute
{
    uint8_t AttributeID;
    uint16_t AttributeLength;
    uint8_t AttributeData[0];
};

#pragma pack(1)
struct AncsNotificationAttributeResponse
{
    uint8_t CommandId;
    uint32_t NotificationUID;
    uint8_t AttributeList[0];
};

static uint32_t notificationInFlight = 0;
#define IN_FLIGHT_TIMEOUT (500)

static bool uartInterfaceActive_ = false;
static uint32_t lastRequest_ = 0;

CFUNC void SendUartNotificationRequest(uint8_t* buffer, uint16_t size);

void HandleAncsProcess(bool connected)
{
    if ((GetTickCount() - lastRequest_) < IN_FLIGHT_TIMEOUT)
    {
        WakeDeviceIntoSnooze();
        return;
    }
    else
    {
        lastRequest_ = GetTickCount();
//        LOG("Notification check");
        notificationInFlight = 0;
    }

    uint32_t request = 0;

    bool incomplete = GetIncompleteNotification(&request);
    if (notificationInFlight == 0 && incomplete)
    {
        notificationInFlight          = request;
        uint8_t buffer[32]            = {0};
        auto command                  = (AncsGetNotificationAttributeCommand*)buffer;
        uint8_t pos                   = 0;
        command->CommandId            = 0;
        command->NotificationUID      = notificationInFlight;
        command->AttributeList[pos++] = (uint8_t)NotificationAttributeID::Title;
        command->AttributeList[pos++] = 32;
        command->AttributeList[pos++] = 0;

        command->AttributeList[pos++] = (uint8_t)NotificationAttributeID::Message;
        command->AttributeList[pos++] = 128;
        command->AttributeList[pos++] = 0;

        command->AttributeList[pos++] = (uint8_t)NotificationAttributeID::AppIdentifier;
        command->AttributeList[pos++] = (uint8_t)NotificationAttributeID::PositiveActionLabel;
        command->AttributeList[pos++] = (uint8_t)NotificationAttributeID::NegativeActionLabel;

        if (uartInterfaceActive_ && ((notificationInFlight & 0xFF000000) == 0x42000000))
        {
            SendUartNotificationRequest(buffer, pos + 5);
            WakeDeviceIntoSnooze();
        }
        else if (connected)
        {
            SendNotificationRequest(buffer, pos + 5);
            WakeDeviceIntoSnooze();
        }
        else
        {
            notificationInFlight          = 0;
        }
    }
}

void SendNotificationAction(uint32_t notificationId, uint8_t action)
{
    uint8_t buffer[6]        = {0};
    auto command             = (AncsPerformNotificationActionCommand*)buffer;
    command->CommandId       = 2;
    command->NotificationUID = notificationId;
    command->Action          = action;
    SendNotificationRequest(buffer, 6);
}

void ModifyNotification(AncsNotificationMessage* message)
{
    AppIds appId;
    switch ((CategoryID)message->CategoryId)
    {
        case CategoryID::Schedule:
            appId = AppIds::Calendar;
            break;
        case CategoryID::IncomingCall:
            appId = AppIds::IncomingCall;
            break;
        case CategoryID::MissedCall:
            appId = AppIds::Phone;
            break;
        case CategoryID::Email:
            appId = AppIds::Email;
            break;
        case CategoryID::Social:
        default:
            appId = AppIds::Notifications;
            break;
    }

    AddNotification(appId, message->NotificationUID);
}

void RemoveNotification(AncsNotificationMessage* message)
{
    RemoveNotification(message->NotificationUID);
}

void HandleAncsAttributeData(uint32_t event, uint8_t* value, uint16_t len, uint16_t actualLen)
{
    auto response = ((AncsNotificationAttributeResponse*)value);
    if (notificationInFlight == response->NotificationUID)
    {
        notificationInFlight = 0;
    }

    int length = 0;
    int count  = 5;
    for (uint8_t* position = response->AttributeList; count < len; position += length)
    {
        uint8_t attribute = position[0];
        length            = ((int)(position[2]) << 8) | ((int)(position[1]));
        // Skip attribute header and move to data section
        position += 3;
        count = count + length + 3;

        switch ((NotificationAttributeID)attribute)
        {
            case NotificationAttributeID::Title:
            {
                LOG("ANCS Title received for message %d", response->NotificationUID);
                UpdateNotificationTitle(response->NotificationUID, position, (uint16_t)length);
                break;
            }
            case NotificationAttributeID::Message:
            {
                LOG("ANCS Message received for message %d", response->NotificationUID);
                UpdateNotificationMessage(response->NotificationUID, position, (uint16_t)length);
                break;
            }
            case NotificationAttributeID::AppIdentifier:
            {
                LOG("ANCS AppIdentifier received for message %d", response->NotificationUID);
                UpdateNotificationAppIdentifier(response->NotificationUID, position, (uint16_t)length);
                break;
            }
            case NotificationAttributeID::PositiveActionLabel:
            {
                LOG("ANCS PositiveActionLabel received for message %d", response->NotificationUID);
                UpdateHasPositiveAction(response->NotificationUID, length > 0 && position[0] != 0);
                break;
            }
            case NotificationAttributeID::NegativeActionLabel:
            {
                LOG("ANCS NegativeActionLabel received for message %d", response->NotificationUID);
                UpdateHasNegativeAction(response->NotificationUID, length > 0 && position[0] != 0);
                break;
            }
            case NotificationAttributeID::Date:
                LOG("ANCS Date received for message %d", response->NotificationUID);
                break;
            case NotificationAttributeID::MessageSize:
                LOG("ANCS MessageSize received for message %d", response->NotificationUID);
                break;
            case NotificationAttributeID::Subtitle:
                LOG("ANCS Subtitle received for message %d", response->NotificationUID);
                break;
        }
    };
}

void HandleAncsMessage(uint32_t event, uint8_t* value, uint16_t len, uint16_t actualLen)
{
    auto message = ((AncsNotificationMessage*)value);
    if (event == 0)
    {
        uartInterfaceActive_ = true;
    }

    switch (message->EventId)
    {
        case (int)EventID::NotificationAdded:
        case (int)EventID::NotificationModified:
        {
            ModifyNotification(message);
            break;
        }
        case (int)EventID::NotificationRemoved:
        {
            RemoveNotification(message);
            break;
        }
    }
}

void HandleAncsStart()
{
    volatile int i = 0;
    i++;
}


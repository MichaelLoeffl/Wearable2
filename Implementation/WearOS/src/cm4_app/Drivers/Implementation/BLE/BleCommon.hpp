#ifndef DRIVERS_IMPLEMENTATION_BLE_BLECOMMON_HPP_
#define DRIVERS_IMPLEMENTATION_BLE_BLECOMMON_HPP_

#include "Common/Common.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include <cy_ble_gap.h>
#include <cy_ble_stack.h>
#include <cybsp.h>
#include "GeneratedSource/cycfg_ble.h"
#include <cyhal.h>

#ifdef __cplusplus
}
#endif

#define CyBleCheckForError(x) CheckBleErrorInternal_(x, #x)
CFUNC bool CheckBleErrorInternal_(cy_en_ble_api_result_t result, const char* info);


#endif // DRIVERS_IMPLEMENTATION_BLE_BLECOMMON_HPP_

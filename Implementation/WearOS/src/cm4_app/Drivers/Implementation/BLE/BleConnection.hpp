#ifndef DRIVERS_IMPLEMENTATION_BLE_BLECONNECTION_HPP_
#define DRIVERS_IMPLEMENTATION_BLE_BLECONNECTION_HPP_

#include "BleCommon.hpp"

namespace BluetoothLowEnergy
{

class BleConnection
{
    friend class StackHandler;
private:
    uint8_t connectionHandle_ = 0;
    bool connected_ = false;
    cy_stc_ble_gap_sec_key_info_t keyInfo;

    void OnConnected(uint8_t bdHandle);
    void OnDisconnected();
    void OnConnectionComplete(cy_stc_ble_gap_enhance_conn_complete_param_t* params);
    void OnGattConnectionIndication(cy_stc_ble_conn_handle_t* connectionHandle);

public:
    bool IsActive();

    uint8_t GetConnectionHandle();

    BleConnection();
};

}

#endif // DRIVERS_IMPLEMENTATION_BLE_BLECONNECTION_HPP_

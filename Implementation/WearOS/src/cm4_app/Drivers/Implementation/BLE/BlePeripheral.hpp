#ifndef DRIVERS_IMPLEMENTATION_BLE_BLEPERIPHERAL_HPP_
#define DRIVERS_IMPLEMENTATION_BLE_BLEPERIPHERAL_HPP_

#include "Common/Common.h"

namespace BluetoothLowEnergy
{

class BlePeripheral
{

public:
    BlePeripheral();
    virtual ~BlePeripheral();

    // Advertising
    bool IsAdvertising();
    void StartAdvertising();
    void StopAdvertising();

    // Connection handling
    void DisplayPasscode(uint32_t passcode);
};

}

#endif // DRIVERS_IMPLEMENTATION_BLE_BLEPERIPHERAL_HPP_

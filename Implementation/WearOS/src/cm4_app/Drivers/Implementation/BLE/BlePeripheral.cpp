#include "BlePeripheral.hpp"
#include "BleStackHandler.hpp"

extern "C"
{
#include "GeneratedSource/cycfg_ble.h"
#include <cyhal.h>
#include <cybsp.h>
#include <cy_ble_stack.h>
#include <cy_ble_gap.h>
}

using namespace BluetoothLowEnergy;

extern StackHandler bleStackHandler_;

BlePeripheral::BlePeripheral()
{

}

BlePeripheral::~BlePeripheral()
{

}


void BlePeripheral::DisplayPasscode(uint32_t passcode)
{
    //ShowPairingCode(passcode);
}

bool BlePeripheral::IsAdvertising()
{
    auto state = Cy_BLE_GetAdvertisementState();
    return state != CY_BLE_ADV_STATE_STOPPED && state != CY_BLE_ADV_STATE_STOP_INITIATED;
}

void BlePeripheral::StartAdvertising()
{
    Cy_BLE_GAPP_StartAdvertisement(CY_BLE_ADVERTISING_SLOW, 0);
}

void BlePeripheral::StopAdvertising()
{
    Cy_BLE_GAPP_StopAdvertisement();
}

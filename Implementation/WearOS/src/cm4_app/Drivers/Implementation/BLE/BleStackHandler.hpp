#ifndef BLE_STACK_HANDLER_HPP
#define BLE_STACK_HANDLER_HPP

#include "Common/Common.h"
#include "BlePeripheral.hpp"
#include "BleCentral.hpp"
#include "BleServer.hpp"
#include "BleClient.hpp"
#include "BleConnection.hpp"

CFUNC void CypressBleCallback(uint32_t event, void *eventParam);

#define BLE_MAX_CONNECTIONS (4)

namespace BluetoothLowEnergy
{
    class StackHandler
    {
    private:
        BleConnection* GetConnection(uint8_t bdHandle);
        void DeviceConnected(cy_stc_ble_gap_connected_param_t* p);
        void DeviceDisconnected(cy_stc_ble_gap_disconnect_param_t* p);
        void ConnectionEstablished(cy_stc_ble_conn_estb_param_t* p);
        void DeviceAuthenticated(cy_stc_ble_gap_auth_info_t* authInfo);

        void StackOn(void* eventParam);
        void StackOff(void* eventParam);
        void SecurityKeyGenerationComplete(cy_stc_ble_gap_sec_key_param_t* keyParams);

        bool active_;
    public:

        BlePeripheral Peripheral;
        BleCentral Central;

        BleServer Server;
        BleClient Client;

        BleConnection Connections[BLE_MAX_CONNECTIONS];

        void Initialize();
        bool IsActive() {return active_;}
        void HandleBleEvents(uint32_t event, void* eventParam);
        void Shutdown();
    };
}

extern BluetoothLowEnergy::StackHandler BleStackHandler;

#endif // BLE_STACK_HANDLER_HPP

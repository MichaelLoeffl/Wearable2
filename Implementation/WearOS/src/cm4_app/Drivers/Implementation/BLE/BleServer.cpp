#include "Drivers/Implementation/BLE/BleServer.hpp"
#include "BleCommon.hpp"
#include "Drivers/Core.h"

using namespace BluetoothLowEnergy;

CFUNC void UpdateSetting(const uint8_t* string, uint16_t length);

BleServer::BleServer()
{
}


void BleServer::PasskeyRequest(uint32_t* pairingCode)
{
    LOG("Pairing %d", *pairingCode);
}

void BleServer::WriteRequest(cy_stc_ble_gatt_write_param_t* writeParameters)
{
    Cy_BLE_GATTS_WriteRsp(writeParameters->connHandle);

    if (writeParameters->handleValPair.attrHandle == CY_BLE_WATCH_CONTROL_RESTARTCHARACTERISTIC_CHAR_HANDLE)
    {
        if (writeParameters->handleValPair.value.len >= 1)
        {
            // Restart into Bootloader
            switch (writeParameters->handleValPair.value.val[0])
            {
                case 0:
                {
                    LOG("Do nothing");
                    // Do nothing
                    break;
                }
                case 1:
                {
                    LOG("Restarting into bootloader 1");
                    //RestartIntoBootloader(1);
                    break;
                }
                default:
                {
                    LOG("Restarting into bootloader 2");
                    //RestartIntoBootloader(2);
                    break;
                }
            }
        }
    }
    else if (writeParameters->handleValPair.attrHandle == CY_BLE_WATCH_CONTROL_SETTINGSWRITER_CHAR_HANDLE)
    {
        if (writeParameters->handleValPair.value.len >= 1)
        {
            UpdateSetting(writeParameters->handleValPair.value.val, writeParameters->handleValPair.value.len);
        }
    }
}

void BleServer::ReadRequest(cy_stc_ble_gatts_char_val_read_req_t* readParameters)
{
#if (CY_BLE_GATT_DB_CCCD_COUNT != 0u)
    (void)Cy_BLE_GATTS_ReadAttributeValueCCCDReqHandler((cy_stc_ble_gatts_char_val_read_req_t*)readParameters);
#endif  /* (CY_BLE_GATT_DB_CCCD_COUNT != 0u) */

    (void)Cy_BLE_InvokeServiceEventHandler(CY_BLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ, (void*)readParameters);
}

void BleServer::PrepareLongWrite(cy_stc_ble_gatts_prep_write_req_param_t* prepWriteParam)
{
    /* Check if this is the first prepare write request */
    prepWriteParam->gattErrorCode = 0;
}

void BleServer::ExecuteLongWrite(cy_stc_ble_gatts_exec_write_req_t* writeParameters)
{
    uint16_t length = 0;
    for (int i = 0; i < writeParameters->prepWriteReqCount; i++)
    {
        length += writeParameters->baseAddr[0].handleValuePair.value.len;
    }
    UpdateSetting(writeParameters->baseAddr[0].handleValuePair.value.val, length);
    writeParameters->gattErrorCode = 0;
}


#ifndef DRIVERS_IMPLEMENTATION_BLE_BLESERVER_HPP_
#define DRIVERS_IMPLEMENTATION_BLE_BLESERVER_HPP_

#include "Common/Common.h"
#include "BleCommon.hpp"

namespace BluetoothLowEnergy
{

class BleServer
{
    friend class StackHandler;

public:
    BleServer();
private:
    void PasskeyRequest(uint32_t* pairingCode);
    void WriteRequest(cy_stc_ble_gatt_write_param_t* writeParameters);
    void ReadRequest(cy_stc_ble_gatts_char_val_read_req_t* readParameters);
    void DeviceConnected(cy_stc_ble_gap_connected_param_t* p);
    void DeviceConnectionComplete(cy_stc_ble_gap_enhance_conn_complete_param_t* params);
    void PrepareLongWrite(cy_stc_ble_gatts_prep_write_req_param_t* readParameters);
    void ExecuteLongWrite(cy_stc_ble_gatts_exec_write_req_t* readParameters);
};

}

#endif // DRIVERS_IMPLEMENTATION_BLE_BLESERVER_HPP_

#include "BleStackHandler.hpp"
#include "BleCommon.hpp"
#include "PinMapping.h"
#include "Drivers/Wakelock.h"
#include "GeneratedSource/cycfg_ble.h"
#include <cy_ble.h>
#include <cyhal.h>
#include <cybsp.h>
#include <cy_ble_stack.h>

using namespace BluetoothLowEnergy;
StackHandler BleStackHandler;

static bool flashWritePending_ = false;
static Wakelock bleWakelock_;

static cy_stc_ble_gap_sec_key_info_t keyInfo;
extern cy_stc_ble_conn_handle_t appConnHandle;

bool Authenticated = false;

CFUNC void App_DisplayBondList();
CFUNC void AncsProcess();
CFUNC void AncsCallbackFunction(uint32_t event, void* eventParam);

CFUNC void BleUpdate()
{
    AncsProcess();
    if (bleWakelock_ == POWER_STATE_DEEP_SLEEP)
    {
        return;
    }
    NOTIFY_BLE_EVENT_INDICATOR();

    ReleaseWakelock(&bleWakelock_);
    Cy_BLE_ProcessEvents();

    if (flashWritePending_)
    {
        flashWritePending_ = false;
        Cy_BLE_StoreBondingData();
    }
    if (Cy_BLE_ControllerEnterLPM(CY_BLE_BLESS_DEEPSLEEP) != CY_BLE_BLESS_DEEPSLEEP)
    {
        CLEAR_BLE_EVENT_INDICATOR();
    }
}

CFUNC void BleShutdownBlocking()
{
    BleStackHandler.Shutdown();
    while (BleStackHandler.IsActive())
    {
        UpdateWakelock(&bleWakelock_, POWER_STATE_PASSIVE);
        BleUpdate();
    }
}

CFUNC void BleEventReadyCallback()
{
    UpdateWakelock(&bleWakelock_, POWER_STATE_PASSIVE);
}

CFUNC void bless_interrupt_handler()
{
    Cy_BLE_BlessIsrHandler();
}

void StackHandler::Shutdown()
{
    Cy_BLE_StackShutdown();
}

void StackHandler::Initialize()
{
    static const cy_stc_sysint_t blessIsrCfg =
    {
        .intrSrc      = bless_interrupt_IRQn,
        .intrPriority = 1u
    };

    cy_ble_config.hw->blessIsrConfig = &blessIsrCfg;
    Cy_SysInt_Init(cy_ble_config.hw->blessIsrConfig, bless_interrupt_handler);
    Cy_BLE_RegisterEventCallback(CypressBleCallback);

    Cy_BLE_ANCS_RegisterAttrCallback(AncsCallbackFunction);
    Cy_BLE_Init(&cy_ble_config);
    Cy_BLE_EnableLowPowerMode();
    Cy_BLE_RegisterAppHostCallback(BleEventReadyCallback);
    Cy_BLE_Enable();
    Cy_BLE_SetCustomEventMask(CY_BLE_CONN_ESTB_EVENT_MASK);
    UpdateWakelock(&bleWakelock_, POWER_STATE_PASSIVE);
}

void StackHandler::StackOn(void* eventParam)
{
    active_ = true;
    Authenticated = false;

    keyInfo.localKeysFlag    = CY_BLE_GAP_SMP_INIT_ENC_KEY_DIST | CY_BLE_GAP_SMP_INIT_IRK_KEY_DIST | CY_BLE_GAP_SMP_INIT_CSRK_KEY_DIST;
    keyInfo.exchangeKeysFlag = CY_BLE_GAP_SMP_INIT_ENC_KEY_DIST | CY_BLE_GAP_SMP_INIT_IRK_KEY_DIST | CY_BLE_GAP_SMP_INIT_CSRK_KEY_DIST |
                               CY_BLE_GAP_SMP_RESP_ENC_KEY_DIST | CY_BLE_GAP_SMP_RESP_IRK_KEY_DIST | CY_BLE_GAP_SMP_RESP_CSRK_KEY_DIST;

    CyBleCheckForError(Cy_BLE_GAP_GenerateKeys(&keyInfo));

    App_DisplayBondList();
    UpdateWakelock(&bleWakelock_, POWER_STATE_PASSIVE);
}

CFUNC void AncsStart();

BleConnection* StackHandler::GetConnection(uint8_t bdHandle)
{
    for (int i = 0; i < BLE_MAX_CONNECTIONS; i++)
    {
        if (Connections[i].IsActive() && Connections[i].GetConnectionHandle() == bdHandle)
        {
            // Connection exists
            return &Connections[i];
        }
    }
    for (int i = 0; i < BLE_MAX_CONNECTIONS; i++)
    {
        if (!Connections[i].IsActive() || Connections[i].GetConnectionHandle() == bdHandle)
        {
            keyInfo.SecKeyParam.bdHandle = bdHandle;
            Connections[i].OnConnected(bdHandle);
            AncsStart();
            return &Connections[i];
        }
    }

    LOG("Error: Device connected but no free connection!");
    return nullptr;
}

void StackHandler::DeviceConnected(cy_stc_ble_gap_connected_param_t* p)
{
    Authenticated = false;
    GetConnection(p->bdHandle);
}

void StackHandler::ConnectionEstablished(cy_stc_ble_conn_estb_param_t* p)
{
    GetConnection(p->bdHandle);
}

void StackHandler::StackOff(void* eventParam)
{
    active_ = false;
}

void StackHandler::SecurityKeyGenerationComplete(cy_stc_ble_gap_sec_key_param_t* keyParams)
{
    keyInfo.SecKeyParam = *keyParams;
    CyBleCheckForError(Cy_BLE_GAP_SetIdAddress(&cy_ble_deviceAddress));
    Peripheral.StartAdvertising();
}

void DeviceAuthRequest(cy_stc_ble_gap_auth_info_t* authInfo)
{
    LOG("CY_BLE_EVT_GAP_AUTH_REQ: security=0x%x, bonding=0x%x, ekeySize=0x%x, err=0x%x",
            authInfo->security,
            authInfo->bonding,
            authInfo->ekeySize,
            authInfo->authErr);
    if (cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX].security == (CY_BLE_GAP_SEC_MODE_1 | CY_BLE_GAP_SEC_LEVEL_1))
    {
        cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX].authErr = CY_BLE_GAP_AUTH_ERROR_PAIRING_NOT_SUPPORTED;
    }

    cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX].bdHandle = authInfo->bdHandle;

    if (CyBleCheckForError(Cy_BLE_GAPP_AuthReqReply(&cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX])))
    {
        if (CyBleCheckForError(Cy_BLE_GAP_RemoveOldestDeviceFromBondedList()))
        {
        }
        else
        {
            CyBleCheckForError(Cy_BLE_GAPP_AuthReqReply(&cy_ble_configPtr->authInfo[CY_BLE_SECURITY_CONFIGURATION_0_INDEX]));
        }
    }
}

void StackHandler::DeviceAuthenticated(cy_stc_ble_gap_auth_info_t* authInfo)
{
    Authenticated = true;
    CyBleCheckForError(Cy_BLE_GATTC_StartDiscovery(appConnHandle));
}

void AdvertisementStateChanged(size_t _)
{
    if (Cy_BLE_GetAdvertisementState() == CY_BLE_ADV_STATE_STOPPED)
    {
        /* The fast and slow advertising period is complete, go to Low-power
        * mode (Hibernate) and wait for an external user event to wake up
        * the device again */
        Cy_BLE_Disable();
    }
}

void SetDeviceAddressComplete()
{
    /* Reads the BD device address from BLE Controller's memory */
    CyBleCheckForError(Cy_BLE_GAP_GetBdAddress());
}

void GetDeviceAddressComplete(cy_stc_ble_events_param_generic_t* params)
{
    uint8_t* addressBytes = ((cy_stc_ble_bd_addrs_t*)(params->eventParams))->publicBdAddr;
    (void)addressBytes;
    LOG("BLE Device Address: %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x", addressBytes[5], addressBytes[4], addressBytes[3], addressBytes[2], addressBytes[1], addressBytes[0]);
}

void StackHandler::DeviceDisconnected(cy_stc_ble_gap_disconnect_param_t* params)
{
    LOG("Device %d disconnected", params->bdHandle);
    auto connection = GetConnection(params->bdHandle);
    if (connection)
    {
        connection->OnDisconnected();
    }
    BleStackHandler.Peripheral.StartAdvertising();
    Authenticated = false;
}

void FlashWritePending()
{
    flashWritePending_ = true;
}

CFUNC void UpdateSetting(const uint8_t* string, uint16_t length);
CFUNC void RestartIntoBootloader(uint32_t reason);


#define MapEvent(eventCode, target, type) \
    case eventCode:                       \
        LOG("BLE event: " #eventCode);    \
        target((type)eventParam);         \
        break;
#define MapVoidEvent(eventCode, target) \
    case eventCode:                     \
        LOG("BLE event: " #eventCode);  \
        target();                       \
        break;
#define UnmappedEvent(eventCode)                            \
    case eventCode:                                         \
        LOG("BLE event (not mapped): '" #eventCode "'");    \
        break;

#define MapServerEvent(eventCode, target, type) \
    case eventCode:                             \
        LOG("BLE server event: " #eventCode);   \
        Server.target((type)eventParam);        \
        break;

#define MapStackEvent(eventCode, target, type)      \
    case eventCode:                                 \
        LOG("BLE stack event: " #eventCode);        \
        BleStackHandler.target((type)eventParam);  \
        break;

#define MapConnectionEvent(eventCode, target, type)                                     \
    case eventCode:                                                                     \
    {                                                                                   \
        LOG("BLE connection event: " #eventCode);                                       \
                                                                                        \
        auto connection = GetConnection(((type)eventParam)->bdHandle);                  \
        if (connection != nullptr)                                                      \
        {                                                                               \
            connection->target((type)eventParam);                                       \
            return;                                                                     \
        }                                                                               \
        LOG("Connection event for non-existing connection: '" #eventCode "'");          \
        break;                                                                          \
    }

void StackHandler::HandleBleEvents(uint32_t event, void* eventParam)
{
    switch (event)
    {
        case CY_BLE_EVT_GAP_CONNECTION_UPDATE_COMPLETE:
        {
            #if (CY_BLE_GAP_ROLE_PERIPHERAL || CY_BLE_GAP_ROLE_CENTRAL)
                (void)Cy_BLE_InvokeServiceEventHandler((uint32_t)event, (void*)eventParam);
            #endif /*(CY_BLE_GAP_ROLE_PERIPHERAL || CY_BLE_GAP_ROLE_CENTRAL) */
            break;
        }

        MapStackEvent(CY_BLE_EVT_STACK_ON, StackOn, void*);
        MapStackEvent(CY_BLE_EVT_STACK_SHUTDOWN_COMPLETE, StackOff, void*);


        MapEvent(CY_BLE_EVT_GAP_DEVICE_CONNECTED, DeviceConnected, cy_stc_ble_gap_connected_param_t*);
        MapEvent(CY_BLE_EVT_GAP_CONN_ESTB, ConnectionEstablished, cy_stc_ble_conn_estb_param_t*);


        MapConnectionEvent(CY_BLE_EVT_GAP_ENHANCE_CONN_COMPLETE, OnConnectionComplete, cy_stc_ble_gap_enhance_conn_complete_param_t*);
        MapConnectionEvent(CY_BLE_EVT_GATT_CONNECT_IND, OnGattConnectionIndication, cy_stc_ble_conn_handle_t*);

        MapServerEvent(CY_BLE_EVT_GAP_PASSKEY_DISPLAY_REQUEST, PasskeyRequest, uint32_t*);


        MapEvent(CY_BLE_EVT_GAP_AUTH_REQ, DeviceAuthRequest, cy_stc_ble_gap_auth_info_t*);
        MapEvent(CY_BLE_EVT_GAP_AUTH_COMPLETE, DeviceAuthenticated, cy_stc_ble_gap_auth_info_t*);
        MapEvent(CY_BLE_EVT_GAP_DEVICE_DISCONNECTED, DeviceDisconnected, cy_stc_ble_gap_disconnect_param_t*);
        MapEvent(CY_BLE_EVT_GAP_KEYS_GEN_COMPLETE, SecurityKeyGenerationComplete, cy_stc_ble_gap_sec_key_param_t*);


        MapVoidEvent(CY_BLE_EVT_PENDING_FLASH_WRITE, FlashWritePending);

        MapVoidEvent(CY_BLE_EVT_SET_DEVICE_ADDR_COMPLETE, SetDeviceAddressComplete);
        MapEvent(CY_BLE_EVT_GET_DEVICE_ADDR_COMPLETE, GetDeviceAddressComplete, cy_stc_ble_events_param_generic_t*);

        MapEvent(CY_BLE_EVT_GAPP_ADVERTISEMENT_START_STOP, AdvertisementStateChanged, size_t);

        MapServerEvent(CY_BLE_EVT_GATTS_WRITE_REQ, WriteRequest, cy_stc_ble_gatt_write_param_t*);
        MapServerEvent(CY_BLE_EVT_GATTS_READ_CHAR_VAL_ACCESS_REQ, ReadRequest, cy_stc_ble_gatts_char_val_read_req_t*);
        MapServerEvent(CY_BLE_EVT_GATTS_PREP_WRITE_REQ, PrepareLongWrite, cy_stc_ble_gatts_prep_write_req_param_t*);
        MapServerEvent(CY_BLE_EVT_GATTS_EXEC_WRITE_REQ, ExecuteLongWrite, cy_stc_ble_gatts_exec_write_req_t*);

        UnmappedEvent(CY_BLE_EVT_INVALID);
        UnmappedEvent(CY_BLE_EVT_TIMEOUT);
        UnmappedEvent(CY_BLE_EVT_STACK_BUSY_STATUS);
        UnmappedEvent(CY_BLE_EVT_MEMORY_REQUEST);
        UnmappedEvent(CY_BLE_EVT_FLASH_CORRUPT);
        UnmappedEvent(CY_BLE_EVT_HARDWARE_ERROR);
        UnmappedEvent(CY_BLE_EVT_WRITE_AUTH_PAYLOAD_TO_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_READ_AUTH_PAYLOAD_TO_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GET_CHANNEL_MAP_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_LE_SET_EVENT_MASK_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_LE_PING_AUTH_TIMEOUT);
        UnmappedEvent(CY_BLE_EVT_SET_DATA_LENGTH_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_SUGGESTED_DATA_LENGTH_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GET_DATA_LENGTH_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_DATA_LENGTH_CHANGE);
        UnmappedEvent(CY_BLE_EVT_GET_PEER_RPA_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GET_LOCAL_RPA_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_RPA_TO_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_RPA_ENABLE_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_HOST_CHANNEL_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_ADD_DEVICE_TO_RPA_LIST_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_REMOVE_DEVICE_FROM_RPA_LIST_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_ADD_DEVICE_TO_WHITE_LIST_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_REMOVE_DEVICE_FROM_WHITE_LIST_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GET_PHY_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_DEFAULT_PHY_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_PHY_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_PHY_UPDATE_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_PRIVACY_MODE_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_LL_CNTRL_PROC_PENDING_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SOFT_RESET_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GET_RSSI_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GET_TX_PWR_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_TX_PWR_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GET_CLK_CONFIG_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_CLK_CONFIG_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_RANDOM_NUM_GEN_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_AES_ENCRYPT_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_AES_CCM_ENCRYPT_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_AES_CCM_DECRYPT_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_SLAVE_LATENCY_MODE_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_RADIO_TEMPERATURE);
        UnmappedEvent(CY_BLE_EVT_RADIO_VOLTAGE_LEVEL);
        UnmappedEvent(CY_BLE_EVT_AES_CMAC_GEN_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_EVENT_MASK_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_CE_LENGTH_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_SET_CONN_PRIORITY_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_HCI_PKT_RCVD);
        UnmappedEvent(CY_BLE_EVT_GAPC_SCAN_PROGRESS_RESULT);
        UnmappedEvent(CY_BLE_EVT_GAP_PASSKEY_ENTRY_REQUEST);
        UnmappedEvent(CY_BLE_EVT_GAP_AUTH_FAILED);
        UnmappedEvent(CY_BLE_EVT_GAP_ENCRYPT_CHANGE);
        //UnmappedEvent(CY_BLE_EVT_GAP_CONNECTION_UPDATE_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GAPC_SCAN_START_STOP);
        UnmappedEvent(CY_BLE_EVT_GAP_KEYINFO_EXCHNGE_CMPLT);
        UnmappedEvent(CY_BLE_EVT_GAP_NUMERIC_COMPARISON_REQUEST);
        UnmappedEvent(CY_BLE_EVT_GAP_KEYPRESS_NOTIFICATION);
        UnmappedEvent(CY_BLE_EVT_GAP_OOB_GENERATED_NOTIFICATION);
        UnmappedEvent(CY_BLE_EVT_GAPC_DIRECT_ADV_REPORT);
        UnmappedEvent(CY_BLE_EVT_GAP_SMP_NEGOTIATED_AUTH_INFO);
        UnmappedEvent(CY_BLE_EVT_GAP_DEVICE_ADDR_GEN_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GAP_RESOLVE_DEVICE_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GAP_GEN_SET_LOCAL_P256_KEYS_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GAP_CREATE_CONN_CANCEL_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GAPP_UPDATE_ADV_SCAN_DATA_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GAP_ADV_TX);
        UnmappedEvent(CY_BLE_EVT_GATTC_ERROR_RSP);
        UnmappedEvent(CY_BLE_EVT_GATT_DISCONNECT_IND);
        UnmappedEvent(CY_BLE_EVT_GATTS_XCNHG_MTU_REQ);
        UnmappedEvent(CY_BLE_EVT_GATTC_XCHNG_MTU_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTC_READ_BY_GROUP_TYPE_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTC_READ_BY_TYPE_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTC_FIND_INFO_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTC_FIND_BY_TYPE_VALUE_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTC_READ_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTC_READ_BLOB_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTC_READ_MULTI_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTC_WRITE_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTS_WRITE_CMD_REQ);
        //UnmappedEvent(CY_BLE_EVT_GATTS_PREP_WRITE_REQ);
        //UnmappedEvent(CY_BLE_EVT_GATTS_EXEC_WRITE_REQ);
        UnmappedEvent(CY_BLE_EVT_GATTC_EXEC_WRITE_RSP);
        UnmappedEvent(CY_BLE_EVT_GATTC_HANDLE_VALUE_NTF);
        UnmappedEvent(CY_BLE_EVT_GATTC_HANDLE_VALUE_IND);
        UnmappedEvent(CY_BLE_EVT_GATTS_HANDLE_VALUE_CNF);
        UnmappedEvent(CY_BLE_EVT_GATTS_DATA_SIGNED_CMD_REQ);
        UnmappedEvent(CY_BLE_EVT_GATTC_STOP_CMD_COMPLETE);
        UnmappedEvent(CY_BLE_EVT_GATTC_LONG_PROCEDURE_END);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CONN_PARAM_UPDATE_REQ);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CONN_PARAM_UPDATE_RSP);
        UnmappedEvent(CY_BLE_EVT_L2CAP_COMMAND_REJ);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CBFC_CONN_IND);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CBFC_CONN_CNF);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CBFC_DISCONN_IND);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CBFC_DISCONN_CNF);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CBFC_DATA_READ);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CBFC_RX_CREDIT_IND);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CBFC_TX_CREDIT_IND);
        UnmappedEvent(CY_BLE_EVT_L2CAP_CBFC_DATA_WRITE_IND);
        default:
            LOG("Unhandled BLE event: 0x%04X", event);
    }
}

CFUNC void CypressBleCallback(uint32_t event, void* eventParam)
{
    NOTIFY_BLE_EVENT_INDICATOR();
    BleStackHandler.HandleBleEvents(event, eventParam);
    CLEAR_BLE_EVENT_INDICATOR();
}

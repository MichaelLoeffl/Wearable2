#include "Drivers/HeartrateDriver.h"
#include "Common/Common.h"
#include "PinMapping.h"

uint32_t heartrateTestValue_ = INVALID_HEARTRATE;

uint16_t heartrateInitCounter_ = 0;

void HeartrateInitialize()
{
    NOTIFY_HEARTRATE_INACTIVE();
}

bool HeartrateStartMeasurement()
{
    NOTIFY_HEARTRATE_ACTIVE();

    heartrateInitCounter_++;
    if (heartrateInitCounter_ > 1)
    {
        return true;
    }

    heartrateTestValue_ = 48;
    return true;
}

uint32_t HeartrateUpdate()
{
    heartrateTestValue_++;
    if (heartrateTestValue_ < 50 || heartrateTestValue_ >= INVALID_HEARTRATE)
    {
        return INVALID_HEARTRATE;
    }
    if (heartrateTestValue_ > 180)
    {
        heartrateTestValue_ = 160;
    }

    return heartrateTestValue_;
}

void HeartrateEndMeasurement()
{
    if (heartrateInitCounter_ == 0)
    {
        return;
    }

    heartrateInitCounter_--;

    if (heartrateInitCounter_ == 0)
    {
        NOTIFY_HEARTRATE_INACTIVE();
    }
}

uint32_t HeartrateGetHeartrate()
{
    return heartrateTestValue_;
}

﻿#ifndef SIMULATOR

#include "Drivers/FileSystem.hpp"
#include "LittleFS.h"

// extern lfs_t FileSystem;

void FileSystemInitialize()
{
    //LittleFS_Init();
}

void FileSystemCleanup()
{
}

void DeleteFile(const char* filename)
{
    // lfs_remove(&FileSystem, filename);
}

File::File(const char* fileName, FileAccessFlags access) : isOpen_(false)
{
    /*int flags = 0;

    if ((access & FileAccessFlags::Read) != 0)
    {
        flags |= LFS_O_RDONLY;
    }

    if ((access & FileAccessFlags::Write) != 0)
    {
        flags |= LFS_O_WRONLY;
    }

    if ((access & FileAccessFlags::CreateNew) != 0)
    {
        flags |= LFS_O_CREAT | LFS_O_EXCL;
    }
    else if ((access & FileAccessFlags::Create) != 0)
    {
        flags |= LFS_O_CREAT;
    }

    if ((access & FileAccessFlags::Append) != 0)
    {
        flags |= LFS_O_APPEND;
    }

    if ((access & FileAccessFlags::Truncate) != 0)
    {
        flags |= LFS_O_TRUNC;
    }

    ShowCode(Code_FileOpen);
#ifdef LFS_NO_MALLOC
    // use static file buffer per file
    int err = lfs_file_opencfg(&FileSystem, &fileHandle, fileName, flags, &fileConfig0);
#else
    // use dynamic file buffers (heap)
    int err = lfs_file_open(&FileSystem, &fileHandle, fileName, flags);
#endif
    isOpen_ = (err == 0);*/
}

bool File::IsOpen()
{
    return false;
    return isOpen_;
}

int32_t File::Read(void* buffer, uint32_t size)
{
    return -1;
    /*
    if (!IsOpen())
    {
        return -1;
    }

    ShowCode(Code_FileRead);
    auto temp = lfs_file_read(&FileSystem, &fileHandle, buffer, size);
    ShowCode(Code_FileReadComplete);
    return temp;*/
}

int32_t File::Write(const void* buffer, uint32_t size)
{
    return -1;
    /*
    if (!IsOpen())
    {
        return -1;
    }
    ShowCode(Code_FileWrite);
    auto temp = lfs_file_write(&FileSystem, &fileHandle, buffer, size);
    ShowCode(Code_FileWriteComplete);
    return temp;
    */
}

int32_t File::GetReadPosition()
{
    return -1;
    /*
    if (!IsOpen())
    {
        return -1;
    }
    return lfs_file_tell(&FileSystem, &fileHandle);
    */
}

int32_t File::GetWritePosition()
{
    return -1;
    /*
    if (!IsOpen())
    {
        return -1;
    }
    return lfs_file_tell(&FileSystem, &fileHandle);*/
}

int32_t File::SeekAbsolute(int32_t position)
{
    return -1;
    /*
    if (!IsOpen())
    {
        return -1;
    }
    return lfs_file_seek(&FileSystem, &fileHandle, position, LFS_SEEK_SET);*/
}

int32_t File::SeekRelative(int32_t offset)
{
    return -1;
    /*
    if (!IsOpen())
    {
        return -1;
    }
    return lfs_file_seek(&FileSystem, &fileHandle, offset, LFS_SEEK_CUR);*/
}

int32_t File::SeekBack(int32_t offset)
{
    return -1;
    /*
    if (!IsOpen())
    {
        return -1;
    }
    return lfs_file_seek(&FileSystem, &fileHandle, offset, LFS_SEEK_END);*/
}

File::~File()
{
    /*
    if (IsOpen())
    {
        lfs_file_close(&FileSystem, &fileHandle);
    }
    */
}

#endif // SIMULATOR

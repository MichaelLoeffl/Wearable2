#include "Drivers/Motor.h"
#include "PinMapping.h"

void MotorInit()
{

}

void MotorStart()
{
    NOTIFY_MOTOR_ACTIVE();
}

void MotorStop()
{
    NOTIFY_MOTOR_INACTIVE();
}

#include "Drivers/Gpio.h"
#include <cy_gpio.h>

CFUNC void* GetPortAddress(uint16_t pin)
{
    return (void*)Cy_GPIO_PortToAddr(GET_PORT_NUMBER(pin));
}

CFUNC void GpioInitializeOutput(uint16_t pin)
{
    Cy_GPIO_Pin_FastInit(Cy_GPIO_PortToAddr(GET_PORT_NUMBER(pin)), GET_PIN_NUMBER(pin), CY_GPIO_DM_STRONG, !IS_HIGH_ACTIVE(pin), HSIOM_SEL_GPIO);
}

CFUNC void GpioSet(uint16_t pin)
{
    Cy_GPIO_Write(Cy_GPIO_PortToAddr(GET_PORT_NUMBER(pin)), GET_PIN_NUMBER(pin), IS_HIGH_ACTIVE(pin));
}

CFUNC void GpioClear(uint16_t pin)
{
    Cy_GPIO_Write(Cy_GPIO_PortToAddr(GET_PORT_NUMBER(pin)), GET_PIN_NUMBER(pin), !IS_HIGH_ACTIVE(pin));
}

CFUNC void GpiosInitializeOutput(const uint16_t* pinArray)
{
    for (int i = 0; pinArray[i] != NO_PIN; i++)
    {
        GpioInitializeOutput(pinArray[i]);
    }
}

CFUNC void GpiosSet(const uint16_t* pinArray)
{
    for (int i = 0; pinArray[i] != NO_PIN; i++)
    {
        GpioSet(pinArray[i]);
    }
}

CFUNC void GpiosClear(const uint16_t* pinArray)
{
    for (int i = 0; pinArray[i] != NO_PIN; i++)
    {
        GpioClear(pinArray[i]);
    }
}

#include "Drivers/Clocks.h"

void InitializeILO();

void ClocksInit()
{
    // Initialize ILO (Internal Low-Speed Oscillator)
    // It is used for Systick and routed through CLK_LF
    // The ILO is active during Deep Sleep
    InitializeILO();
}

void InitializeILO()
{
}

#include "Drivers/hids.h"
#include "Common/Common.h"

void HidsInit()
{
}

void HidsCallBack(uint32_t event, void* eventParam)
{
}

void HidsSimulateKeyboard(uint8_t key)
{
}

void SendVolume(bool up)
{
}

bool HidsIsCapsLock()
{
    return false;
}

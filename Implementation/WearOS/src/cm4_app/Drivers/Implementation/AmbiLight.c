#include "Drivers/AmbientSensorDriver.h"

void OnBoard_Ambi_Init()
{
}

void ADC_1_ISR_Callback()
{
}

uint16_t OnBoard_Ambi_ReadData()
{
    return 0;
}

void OnBoard_Ambi_Disable()
{
}

bool AmbiLight_Identify()
{
    return true;
}

bool AmbiLight_Init()
{
    return true;
}


uint32_t AmbiLight_ReadData()
{
    return 0;
}

uint32_t AmbiLight_ClearData()
{
    return 0;
}

uint8_t AmbiLight_Read8(uint8_t regAdr)
{
    return 0;
}

uint32_t AmbiLight_Read32(uint8_t regAdr)
{
    return 0;
}

void AmbiLight_Write8(uint8_t regAdr, uint8_t data)
{
}



extern "C"
{
    #include "Drivers/AmbientSensorDriver.h"
}
#include "Drivers/Bluetooth.h"
#include "Drivers/Core.h"
#include "Drivers/Display.h"
#include "Drivers/Wakelock.h"
#include "Drivers/InvokeBootloader.h"
#include "Drivers/DebugUart.h"
#include "stdio.h"
#include <stdarg.h>
#include "PinMapping.h"
#include <string.h>
#include "Common/Testing.hpp"
#include "AppSystemM4.hpp"

#define DEBUG_INTERFACE_BUFFER_SIZE_BITS (8)
#define DEBUG_INTERFACE_BUFFER_SIZE_MASK ((1 << DEBUG_INTERFACE_BUFFER_SIZE_BITS) - 1)

CFUNC void UpdateSetting(const uint8_t* string, uint16_t length);

#define DEBUG_INTERFACE_BUFFER_SIZE (128)
int inputSize_ = 0;

char debugInterfaceBufferIn_[DEBUG_INTERFACE_BUFFER_SIZE];

extern "C" uint32_t debugInterfaceButton;
bool debugInterfaceTouchSimulaton = false;
uint8_t debugInterfaceTouchX      = false;
bool debugInterfaceTouchTouched   = false;
uint32_t commandID_ = 0;

extern uint16_t touchEmulation;

void SendScreenshot();

int printfDummy(const char* format, ...)
{
    va_list va;
    va_start(va, format);
    va_end(va);
    return 0;
}

CFUNC void HandleAncsMessage(uint32_t event, uint8_t* value, uint16_t len, uint16_t actualLen);
CFUNC void HandleAncsAttributeData(uint32_t event, uint8_t* value, uint16_t len, uint16_t actualLen);

void ExecuteCommand(const char* command, const char* data, int dataLength)
{
    if (strcmp(command, "Text") == 0)
    {
        if (data[0] == '{' || data[0] == '"')
        {
            UpdateSetting((const uint8_t*)data, strlen(data));
            return;
        }
    }

    if (strcmp("ButtonClick", command) == 0)
    {
        DeviceUserIntervention(5000);
        debugInterfaceButton = 200;
    }
    else if (strcmp("Restart", command) == 0)
    {
        UartFinishCoalition();
        RestartDevice();
    }
    else if (strcmp("Notification", command) == 0)
    {
        HandleAncsMessage(0, (uint8_t*)data, dataLength, dataLength);
    }
    else if (strcmp("AncsAttributeData", command) == 0)
    {
        HandleAncsAttributeData(0, (uint8_t*)data, dataLength, dataLength);
    }
    else if (strcmp("Test", command) == 0)
    {
        TestSuiteRoot.Run(data);
    }
    else if (strcmp("ButtonLongPress", command) == 0)
    {
        debugInterfaceButton = 1000;
    }
    else if (strcmp("AmbientSensor On", command) == 0)
    {
        OnBoard_Ambi_Init();
        LOG("AmbientSensor enabled");
    }
    else if (strcmp("AmbientSensor Off", command) == 0)
    {
        OnBoard_Ambi_Disable();
        LOG("AmbientSensor disabled");
    }
    else if (strcmp("Screenshot", command) == 0)
    {
        SendScreenshot();
    }
    else if (strcmp("Touch", command) == 0)
    {
        uint16_t te = 0x4000;
        uint16_t pos = data[0] & 0x7F;
        te |= 0x2000;
        te |= pos;
        te |= 0x8000;
        te |= 0x1000;
        touchEmulation = te;
        while (touchEmulation & 0x1000)
        {
            appSystem_.DoEvents();
        }
    }
    else if (strcmp("TouchRelease", command) == 0)
    {
        uint16_t te = 0x8000;
        te |= 0x1000;
        te |= 0x2000;
        te |= touchEmulation & 0x7F;
        touchEmulation = te;
        while (touchEmulation & 0x1000)
        {
            appSystem_.DoEvents();
        }
        return;
    }
    else if (strcmp("GetScrollPos", command) == 0)
    {
        LOG("ScrollPos: %hd", appSystem_.GetScrollPosition());
    }
    else
    {
        LOG("Unknown command %s", command);
    }
}

void SendScreenshot()
{
    NOTIFY_RENDER_TRANSFER_STARTED();
    uint16_t* pixel = backBuffer_;
    uint16_t* end   = &backBuffer_[DISPLAY_WIDTH * DISPLAY_HEIGHT];
    uint16_t currentColor  = *pixel;
    int numPixels   = 0;

    UartStartDataBlock("Screenshot");

    while (pixel != end)
    {
        if (*pixel != currentColor)
        {
            if (numPixels > 0)
            {
                if (numPixels > 127)
                {
                    _putchar((numPixels & 0x7F) | 0x80);
                    _putchar((numPixels >> 7) & 0x7F);
                }
                else
                {
                    _putchar(numPixels & 0x7F);
                }

                _putchar((currentColor >> 8) & 0xFF);
                _putchar(currentColor & 0xFF);
            }

            currentColor = *pixel;
            numPixels = 1;
        }
        else
        {
            numPixels++;
        }
        pixel++;
    }

    if (numPixels > 127)
    {
        _putchar((numPixels & 0x7F) | 0x80);
        _putchar((numPixels >> 7) & 0x7F);
    }
    else
    {
        _putchar(numPixels & 0x7F);
    }

    _putchar((currentColor >> 8) & 0xFF);
    _putchar(currentColor & 0xFF);

    UartFinishBlock();
    NOTIFY_RENDER_TRANSFER_FINISHED();
}

void PushInput(uint8_t c)
{
    if (inputSize_ >= DEBUG_INTERFACE_BUFFER_SIZE - 1)
    {
        inputSize_ = 0;
        LOG("Input buffer overflow");
    }
    debugInterfaceBufferIn_[inputSize_] = c;
    inputSize_++;
}

void ReadPackage()
{
    if (inputSize_ < 2)
    {
        return;
    }

    int start = 1;

    if (inputSize_ >= 4)
    {
        if (debugInterfaceBufferIn_[1] == ':')
        {
            uint16_t coalitionId = debugInterfaceBufferIn_[2];
            coalitionId <<= 8;
            coalitionId |= debugInterfaceBufferIn_[3];
            start = 4;
            SetCoalitionId(coalitionId);
        }
    }

    for (int i = start; i < inputSize_; i++)
    {
        //">[:coalationId]CommandId Data\0"
        if (debugInterfaceBufferIn_[i] == ' ' || debugInterfaceBufferIn_[i] == '\0')
        {
            debugInterfaceBufferIn_[i] = '\0';
            LOG("Executing: %s", &debugInterfaceBufferIn_[start]);
            LOG("CoalitionID: %d", (int)GetCoalitionId());
            if (i == inputSize_ - 1)
            {
                i--;
            }
            ExecuteCommand(&debugInterfaceBufferIn_[start], &debugInterfaceBufferIn_[i + 1], inputSize_ - i - 1);

            LOG("Finished Command with ID: %d", (int)GetCoalitionId());

            UartFinishCoalition();

            return;
        }
    }

    SetCoalitionId(0);
}

void ExecuteInput()
{
    PushInput('\0');
    ReadPackage();
    inputSize_ = 0;
}

void ReadAll()
{
    static bool escape = false;

    char c;

    while (UartRead(&c))
    {
        if (!escape)
        {
            switch (c)
            {
            case '\0':
            case '\n':
                ExecuteInput();
                continue;
            case '\\':
                escape = true;
                continue;
            }
        }

        PushInput(c);
        escape = false;
    }
}

void UpdateDebugInterface(uint32_t dT)
{
    ReadAll();
}

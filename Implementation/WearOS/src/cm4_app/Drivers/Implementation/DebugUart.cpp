#include "Common/Common.h"
#include "Drivers/Core.h"
#include "Common/RingBuffer.hpp"
#include "PinMapping.h"
#include "Drivers/DebugUart.h"
#include "Drivers/Wakelock.h"

extern "C"
{
#include <cyhal.h>
#include <cyhal_uart.h>
#include <cy_gpio.h>
#include <cybsp.h>
#include <cyhal_gpio.h>
#include <cyhal_hwmgr.h>
}

#define SEND_BUFFER_SIZE (256)
#define RECEIVE_BUFFER_SIZE (1024)

RingBuffer<char, RECEIVE_BUFFER_SIZE> ReadBuffer;
RingBuffer<char, SEND_BUFFER_SIZE> WriteBuffer;

extern "C" cyhal_uart_t debugUart_;
extern "C" bool uartDisabled_;
static bool initialized_ = false;
static char escapeBuffer_[2];
static Wakelock uartWakelock_;

CFUNC void InitDebugUart(uint32_t port, uint32_t pin);

void UartInitialize()
{
    escapeBuffer_[0] = '\\';

    const cyhal_uart_cfg_t uart_config =
    {
        .data_bits      = 8,
        .stop_bits      = 1,
        .parity         = CYHAL_UART_PARITY_NONE,
        .rx_buffer      = NULL,
        .rx_buffer_size = 0,
    };

    cyhal_gpio_t tx = (cyhal_gpio_t)CYHAL_GET_GPIO(GET_PORT_NUMBER(UART_TX_PIN), GET_PIN_NUMBER(UART_TX_PIN));
    cyhal_gpio_t rx = (cyhal_gpio_t)CYHAL_GET_GPIO(GET_PORT_NUMBER(UART_RX_PIN), GET_PIN_NUMBER(UART_RX_PIN));

    cy_rslt_t result = cyhal_uart_init(&debugUart_, (cyhal_gpio_t)tx, (cyhal_gpio_t)rx, NC, NC, (cyhal_clock_t*)NULL, (cyhal_uart_cfg_t*)&uart_config);

    if (result == CY_RSLT_SUCCESS)
    {
        result = cyhal_uart_set_baud(&debugUart_, UART_BAUDRATE, NULL);
    }
    initialized_ = result == CY_RSLT_SUCCESS;
}

CFUNC void UartFinishBlock();

void UartFinishBlock()
{
    UartPush("\n", 1);
}

void UartFinishCoalition()
{
    UartPush(";:", 2);
    auto id = GetCoalitionId();
    SetCoalitionId(0);
    _putchar((id >> 8) & 0xFF);
    _putchar(id & 0xFF);
    UartPush("\n", 1);

    LOG("Coalition finished: %hd", id);
}

CFUNC void _putchar(char character)
{
    if (character == '\n' || character == '\0' || character == '\\')
    {
        escapeBuffer_[1] = character;
        UartPush(escapeBuffer_, 2);
    }
    else
    {
        UartPush(&character, 1);
    }
}

CFUNC void UartPrintPrefix(char typeIdentifier, const char* channel)
{
    _putchar(typeIdentifier);
    _putchar(':');
    auto id = GetCoalitionId();
    _putchar((id >> 8) & 0xFF);
    _putchar(id & 0xFF);
    auto pos = channel;
    while (*pos != '\0')
    {
        _putchar(*pos);
        pos++;
    }
    _putchar(' ');
}

void UartStartTextBlock(const char* channel)
{
    UartPrintPrefix('>', channel);
}

void UartStartDataBlock(const char* channel)
{
    UartPrintPrefix('*', channel);
}

void UartSend(const char* channel, const char* text)
{
    UartStartTextBlock(channel);

    while (*text != '\0')
    {
        _putchar(*text);
        text++;
    }

    UartFinishBlock();
}

void UartSendData(const char* channel, const uint8_t* data, int dataLength)
{
    UartStartDataBlock(channel);

    for (int i = 0; i < dataLength; i++)
    {
        _putchar(*data);
        data++;
    }

    UartFinishBlock();
}

void TransferToUart()
{
    size_t length = WriteBuffer.Count();
    while (length > 0 && cyhal_uart_writable(&debugUart_) != 0)
    {
        const char* buffer = WriteBuffer.PeekBuffer(&length);
        // const_cast because cypress did not mark data field as const...
        cyhal_uart_write(&debugUart_, const_cast<void*>((void*)buffer), &length);
        WriteBuffer.Drop(length);
        length = WriteBuffer.Count();
    }

    if (!UartActive())
    {
        ReleaseWakelock(&uartWakelock_);
    }
}

void UartFlush()
{
    if (!initialized_)
    {
        return;
    }
    TransferToUart();
    while (UartActive())
    {
        BusyLoopStart();
        TransferToUart();
    }
    BusyLoopStop();
}

bool UartInitialized()
{
    return initialized_;
}

bool UartActive()
{
    if (uartDisabled_ || !initialized_)
    {
        return false;
    }
    bool active = WriteBuffer.Count() > 0 || cyhal_uart_is_tx_active(&debugUart_);
    UpdateWakelock(&uartWakelock_, active ? POWER_STATE_PASSIVE : POWER_STATE_DEEP_SLEEP);
    return active;
}

size_t TryPush(const char* data, size_t length)
{
    size_t written = 0;
    // Dequeue WriteBuffer
    TransferToUart();

    size_t writeBufferCount = WriteBuffer.Count();
    // If buffer is empty, try to push to uart directly
    if (writeBufferCount == 0 && cyhal_uart_writable(&debugUart_) != 0)
    {
        written = length;
        // const_cast because cypress did not mark data field as const...
        cyhal_uart_write(&debugUart_, const_cast<void*>((void*)data), &written);
    }

    size_t count = length - written;
    written += WriteBuffer.Push(&data[written], count);

    UpdateWakelock(&uartWakelock_, POWER_STATE_PASSIVE);

    return written;
}

void UartPush(const char* data, size_t length)
{
    if (!initialized_)
    {
        return;
    }
    size_t written = TryPush(data, length);
    while (written < length)
    {
        BusyLoopStart();
        written += TryPush(&data[written], length - written);
    }

    BusyLoopStop();
}

bool UartRead(char* character)
{
    if (!initialized_)
    {
        return false;
    }
    return ReadBuffer.TakeOne(character);
}

size_t UartReadBuffer(char* buffer, size_t length)
{
    if (!initialized_)
    {
        return 0;
    }
    size_t read;
    char* position = buffer;

    for (read = 0; read < length && ReadBuffer.TakeOne(position); read++)
    {
        position++;
    }

    return read;
}

bool ReadUartBuffer()
{
    size_t read, availableSpace;

    availableSpace = RECEIVE_BUFFER_SIZE;
    auto buffer = ReadBuffer.Lock(&availableSpace);
    read = availableSpace;

    cyhal_uart_read(&debugUart_, buffer, &read);


    ReadBuffer.Unlock(read);

    return read == availableSpace && ReadBuffer.Empty() > 0;
}

void UpdateDebugUart()
{
    if (!initialized_)
    {
        return;
    }
    TransferToUart();
    while (ReadUartBuffer())
    {

    }
}

#include "Drivers/TouchHandler.hpp"
#include "Drivers/Touch.h"
#include "lv_hal/lv_hal_indev.h"

void TouchGestureCallback(uint16_t gesture)
{
    // TODO
}

// bool TouchGetTouchPosition(uint16_t* x, uint16_t* y) { return true; }

bool TouchRead(lv_indev_drv_t* drv, lv_indev_data_t* data)
{
    uint32_t x, y;

    bool pressed = TouchGetTouchPosition(&x, &y);
    /*Store the collected data*/
    data->point.x = x;
    data->point.y = LV_VER_RES_MAX / 2;
    data->state   = pressed ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;

    return false;
}

void TouchHandler::PostInit()
{
    TouchPostInit();
}

void TouchHandler::Initialize()
{
    //TouchInitialize();

    lv_indev_drv_t touchDriver;
    lv_indev_drv_init(&touchDriver); /*Basic initialization*/
    touchDriver.drag_limit = 0;
    touchDriver.drag_throw = 0;
    touchDriver.type    = LV_INDEV_TYPE_POINTER;
    touchDriver.read_cb = &TouchRead;
    lv_indev_drv_register(&touchDriver); /*Register the driver in LittlevGL*/
}

#include "Drivers/Touch.h"
#include "PinMapping.h"
#include "Drivers/Wakelock.h"
#include "Drivers/Display.h"
#include <cybsp.h>
#include <cyhal.h>
#include <cycfg.h>
#include <cycfg_capsense.h>
#include "cycfg_ble.h"

static bool initialized_ = false;
static bool capsense_scan_complete = false;
uint16_t touchEmulation = 0;

static Wakelock touchWakelock_;

#define CAPSENSE_INTERRUPT_PRIORITY      (7u)
#define EZI2C_INTERRUPT_PRIORITY         (6u) /* EZI2C interrupt priority must be
                                          * higher than CapSense interrupt */

cy_stc_scb_ezi2c_context_t ezi2c_context;
cyhal_ezi2c_t sEzI2C;

static uint16_t touchPosition;
static bool touched;

extern cy_stc_capsense_context_t cy_capsense_context;

/*******************************************************************************
* Macros
*******************************************************************************/
#define ENABLE                       (1u)
#define DISABLE                      (0u)
#define CY_ASSERT_FAILED             (0u)

/*******************************************************************************
 * Global variables
 ******************************************************************************/

/*******************************************************************************
* Function Name: capsense_isr
********************************************************************************
* Summary:
*  Wrapper function for handling interrupts from CapSense block.
*
*******************************************************************************/
static void capsense_isr()
{
    Cy_CapSense_InterruptHandler(CYBSP_CSD_HW, &cy_capsense_context);
}

/*******************************************************************************
* Function Name: capsense_callback()
********************************************************************************
* Summary:
*  This function sets a flag to indicate end of a CapSense scan.
*
* Parameters:
*  cy_stc_active_scan_sns_t* : pointer to active sensor details.
*
*******************************************************************************/
void capsense_callback(cy_stc_active_scan_sns_t * ptrActiveScan)
{
    capsense_scan_complete = true;
    ReleaseWakelock(&touchWakelock_);
}

void TouchPrepareConfig()
{
    cy_stc_capsense_pin_config_t capsensePinConfig[] =
            {
                    {
                            Cy_GPIO_PortToAddr(GET_PORT_NUMBER(SLIDER_PIN_0)),
                            GET_PIN_NUMBER(SLIDER_PIN_0),
                    },
                    {
                            Cy_GPIO_PortToAddr(GET_PORT_NUMBER(SLIDER_PIN_1)),
                            GET_PIN_NUMBER(SLIDER_PIN_1),
                    },
                    {
                            Cy_GPIO_PortToAddr(GET_PORT_NUMBER(SLIDER_PIN_2)),
                            GET_PIN_NUMBER(SLIDER_PIN_2),
                    },
                    {
                            Cy_GPIO_PortToAddr(GET_PORT_NUMBER(SLIDER_PIN_3)),
                            GET_PIN_NUMBER(SLIDER_PIN_3),
                    },
                    {
                            Cy_GPIO_PortToAddr(GET_PORT_NUMBER(SLIDER_PIN_4)),
                            GET_PIN_NUMBER(SLIDER_PIN_4),
                    },
            };

    cy_capsense_context.ptrPinConfig = capsensePinConfig;

    cy_stc_capsense_common_config_t commonConfig;
    memcpy(&commonConfig, cy_capsense_context.ptrCommonConfig, sizeof(cy_stc_capsense_common_config_t));
    commonConfig.pinCmod    = GET_PIN_NUMBER(SLIDER_CMOD);
    commonConfig.portCmod   = Cy_GPIO_PortToAddr(GET_PORT_NUMBER(SLIDER_CMOD));
    cy_capsense_context.ptrCommonConfig = &commonConfig;


    // TODO: Shield
    if (SLIDER_SHIELD == NO_PIN)
    {
    }
}

static void process_touch()
{
    cy_stc_capsense_touch_t *slider_touch_info;
    uint16_t slider_pos;
    uint8_t slider_touch_status;
    static uint16_t slider_pos_prev;

    /* Get slider status */
    slider_touch_info = Cy_CapSense_GetTouchInfo(
            CY_CAPSENSE_LINEARSLIDER0_WDGT_ID, &cy_capsense_context);
    slider_touch_status = slider_touch_info->numPosition;
    slider_pos = slider_touch_info->ptrPosition->x;

    int temp = (slider_pos * 127) / cy_capsense_context.ptrWdConfig[CY_CAPSENSE_LINEARSLIDER0_WDGT_ID].xResolution;

    touched = 0 != slider_touch_status;
    /* Detect the new touch on slider */
    if (touched)
    {
        if (temp != slider_pos_prev)
        {
            DeviceUserIntervention(15000);
            touchPosition = (uint16_t)temp;
            LOG("Touch Position: %d", touchPosition);
            slider_pos_prev = touchPosition;
        }
    }
}

void TouchInitialize()
{
    uint32_t status = CYRET_SUCCESS;

    /* CapSense interrupt configuration */
    const cy_stc_sysint_t CapSense_interrupt_config =
            {
                    .intrSrc = CYBSP_CSD_IRQ,
                    .intrPriority = CAPSENSE_INTERRUPT_PRIORITY,
            };

    /* Capture the CSD HW block and initialize it to the default state. */
    status = Cy_CapSense_Init(&cy_capsense_context);
    if (CYRET_SUCCESS != status)
    {
        return;
    }

    /* Initialize CapSense interrupt */
    Cy_SysInt_Init(&CapSense_interrupt_config, capsense_isr);
    NVIC_ClearPendingIRQ(CapSense_interrupt_config.intrSrc);
    NVIC_EnableIRQ(CapSense_interrupt_config.intrSrc);

    /* Initialize the CapSense firmware modules. */
    status = Cy_CapSense_Enable(&cy_capsense_context);
    if (CYRET_SUCCESS != status)
    {
        return;
    }

    /* Assign a callback function to indicate end of CapSense scan. */
    status = Cy_CapSense_RegisterCallback(CY_CAPSENSE_END_OF_SCAN_E,
                                          capsense_callback, &cy_capsense_context);
    if (CYRET_SUCCESS != status)
    {
        return;
    }

    initialized_ = true;

    Cy_CapSense_ScanAllWidgets(&cy_capsense_context);
}

void TouchCleanup()
{
    Cy_CapSense_DeInit(&cy_capsense_context);
}

void TouchUpdate(uint32_t deltaT)
{
    if (capsense_scan_complete && GetCurrentPowerState() >= POWER_STATE_ACTIVE)
    {
        /* Process all widgets */
        Cy_CapSense_ProcessAllWidgets(&cy_capsense_context);

        /* Process touch input */
        process_touch();

        capsense_scan_complete = false;
        UpdateWakelock(&touchWakelock_, POWER_STATE_ACTIVE);

        /* Initiate next scan */
        Cy_CapSense_ScanAllWidgets(&cy_capsense_context);
    }
}

void TouchPostInit()
{

}

bool TouchGetTouchPosition(uint32_t* x, uint32_t* y)
{
    if (touchEmulation & 0x2000)
    {
        //DeviceUserIntervention(10000);
    }
    if (touchEmulation & 0x8000)
    {
        static bool touched = false;
        int emulationPosition = (touchEmulation & 0x7F);
        if (DisplayIsRotated())
        {
            emulationPosition = 127 - emulationPosition;
        }

        if (touched)
        {
            int direction = emulationPosition - touchPosition;
            touchPosition += CLAMP(direction, -5, 5);
        }
        else
        {
            touchPosition = emulationPosition;
        }

        touched = (touchEmulation & 0x4000) ? true : false;

        if ((touchEmulation & 0x1000) && (emulationPosition == touchPosition || !touched))
        {
            touchEmulation &= ~0x1000;
        }

        *x = touchPosition;
        *y = 28;

        return touched;
    }

    if (DisplayIsRotated())
    {
        *x = 127 - touchPosition;
    }
    else
    {
        *x = touchPosition;
    }

    *y = 28;
    return touched;
}

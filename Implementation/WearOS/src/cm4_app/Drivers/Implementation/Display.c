#include "Drivers/Display.h"
#include "lv_core/lv_disp.h"
#include "lv_hal/lv_hal_disp.h"
#include "lv_misc/lv_color.h"
#include <cy_syslib.h>
#include "PinMapping.h"
#include "DisplaySPI.h"
#include "Drivers/Core.h"
#include "Drivers/Wakelock.h"

#if (MAX_BRIGHTNESS != 3)
#error "MAX_BRIGHTNESS must be 3"
#endif

void DisplayPrepareForFrame();

// brightness table, 4 values from "dark" to "light"
const uint8_t brightnessTable_[MAX_BRIGHTNESS + 1] = {14, 24, 40, 68};
static uint8_t actBrightness_ = MAX_BRIGHTNESS;
static bool DisplayRefreshRequired_ = false;
static bool DisplayInitialized_ = false;
static Wakelock DisplayActive_;
static bool TransferActive_ = false;

TPixel buf1_[DISPLAY_HEIGHT][DISPLAY_WIDTH];
TPixel* backBuffer_ = &buf1_[0][0];
TPixel* frontBuffer_ = &buf1_[0][0];

#include <cy_gpio.h>
#include <cy_scb_spi.h>
#include <cy_sysint.h>
#define SPI_DISP_HW SCB5

#define SPI_INTR_NUM        ((IRQn_Type) scb_5_interrupt_IRQn)
#define SPI_INTR_PRIORITY   (7U)

#define SPI_TX_TRIGGER_LEVEL (4)
/* Populate configuration structure (code specific for CM4) */
const cy_stc_sysint_t spiIntrConfig2 =
        {
                .intrSrc      = SPI_INTR_NUM,
                .intrPriority = SPI_INTR_PRIORITY,
        };

void Delay(uint16_t microseconds)
{
    NOTIFY_BUSYLOOP_ACTIVE();
    CyDelayUs(microseconds);
    NOTIFY_BUSYLOOP_INACTIVE();
}

void DisplaySwap()
{
    CoreDisableInterrupts();
    TPixel* tmp = frontBuffer_;
    frontBuffer_ = backBuffer_;
    backBuffer_ = tmp;
    DisplayRefreshRequired_ = true;
    CoreEnableInterrupts();
}

void DisplayFlush(lv_disp_drv_t* disp_drv, const lv_area_t* area, lv_color_t* color_p)
{
    int32_t x1, y1;
    if (area->y1 == 0 && area->y2 == DISPLAY_HEIGHT - 1 && area->x1 == 0 && area->x2 == DISPLAY_WIDTH - 1)
    {
        TPixel* ptr            = backBuffer_;
        for (int32_t y = 0; y <= DISPLAY_HEIGHT * DISPLAY_WIDTH; y++)
        {
            uint16_t c = lv_color_to16(*color_p++);
            *(ptr++) = c;
        }
    }
    else
    {
        // check limits:
        x1 = (area->x1 < 0 ? 0 : area->x1);
        if (x1 >= DISPLAY_WIDTH)
        {
            lv_disp_flush_ready(disp_drv);
            return;
        }

        y1 = (area->y1 < 0 ? 0 : area->y1);
        if (y1 >= DISPLAY_HEIGHT)
        {
            lv_disp_flush_ready(disp_drv);
            return;
        }

        for (int32_t y = y1; (y <= area->y2) && (y < DISPLAY_HEIGHT); y++)
        {
            TPixel* ptr = &backBuffer_[y * DISPLAY_WIDTH + x1];

            for (int32_t x = x1; (x <= area->x2) && (x < DISPLAY_WIDTH); x++)
            {
                uint16_t c = lv_color_to16(*color_p++);
                *(ptr++) = c;
            }
        }
    }

    DisplaySwap();
    lv_disp_flush_ready(disp_drv);
}

/***************************************
 *            Constants
 ****************************************/

/* Command valid status */
#define TRANSFER_CMPLT (0x00UL)
#define TRANSFER_ERROR (0xFFUL)
#define READ_ERROR (TRANSFER_ERROR)
#define TRANSFER_IN_PROGRESS (0x10UL)

static void SendInitString();

static bool DisplayOn_ = false;

const uint8_t GAMMA_TABLE_[63]
    /* x^3 */
    = {0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  1,  2,  2,  2,  3,  3,  4,  5,  5,  6,  7,  8,  9,  10,  11,  12,  14,  15, 17,
       18, 20, 22, 24, 26, 28, 30, 33, 35, 38, 40, 43, 46, 49, 53, 56, 60, 64, 67, 71, 76, 80, 85, 89, 94, 99, 104, 110, 115, 121, 127};


// send display command with 0 parameters
static void SendCommand(uint8_t Cmd)
{
    NOTIFY_BUSYLOOP_ACTIVE();
    while (!DisplaySpiTransferComplete())
    {
    }
    START_DISPLAY_COMMAND();
    Cy_SCB_SPI_Write(SPI_DISP_HW, Cmd); // Command Byte
    while (!DisplaySpiTransferComplete())
    {
    }
    END_DISPLAY_COMMAND();
    NOTIFY_BUSYLOOP_INACTIVE();
}

static void SendDataByte(uint8_t data)
{
    NOTIFY_BUSYLOOP_ACTIVE();
    Cy_SCB_SPI_Write(SPI_DISP_HW, data); // Data Byte 0
    while (!DisplaySpiTransferComplete())
    {
    }
    NOTIFY_BUSYLOOP_INACTIVE();
}

// send display command with 1 parameter
static void SendCommand1(uint8_t cmd, uint8_t data)
{
    SendCommand(cmd);
    SendDataByte(data);
}
// send display command with 2 parameters
static void SendCommand2(uint8_t cmd, uint8_t data0, uint8_t data1)
{
    SendCommand(cmd);
    SendDataByte(data0);
    SendDataByte(data1);
}

// send display command with 3 parameters
static void SendCommand3(uint8_t cmd, uint8_t data0, uint8_t data1, uint8_t data2)
{
    SendCommand(cmd);
    SendDataByte(data0);
    SendDataByte(data1);
    SendDataByte(data2);
}

// send display command with <count> parameters
static void SendCommandTable(uint8_t cmd, const uint8_t* table, uint16_t count)
{
    SendCommand(cmd);

    for (; count > 0; count--)
    {
        SendDataByte(*table++);
    }
}

CFUNC void DisplaySpiInitializeDMA();

void DisplayInitialize()
{
    DisplaySpiInitialize();
    // Make sure nothing is transferred
    while (!DisplaySpiTransferComplete())
    {
        DisplaySpiTransfer(buf1_, 0);
    }

    DISPLAY_TRANSMISSION_START();

    // Switch on 16V supply
    ACTIVATE_DISPLAY();

    // output hw reset pulse at OLED_nRST for 100us
    START_DISPLAY_RESET();
    Delay(100);
    END_DISPLAY_RESET();
    Delay(100);


    // Switch on 16V supply
    ACTIVATE_DISPLAY();


    SendInitString();

    Delay(400);
    DisplayInitialized_ = true;
    DisplayOn_          = false;

    DISPLAY_TRANSMISSION_END();

    // output hw reset pulse at OLED_nRST for 100us
    START_DISPLAY_RESET();
    CyDelayUs(100);
    END_DISPLAY_RESET();
    CyDelayUs(100);

    for (int x = 0; x < DISPLAY_WIDTH; x++)
    {
        for (int y = 0; y < DISPLAY_HEIGHT; y++)
        {
            backBuffer_[y * DISPLAY_WIDTH + x] = 0xAAAA;
        }
    }

    //DisplaySpiInitializeDMA();
}

void DisplayTurnOn()
{
    if (!DisplayInitialized_ || DisplayOn_)
    {
        return;
    }

    // Turn on 16V supply
    ACTIVATE_DISPLAY();
    Delay(100);

    // Display on
    SendCommand(0xAF);

    Delay(300);

    SendCommand(0x5C); // Enable MCU to write data into RAM
    DisplayOn_ = true;
}

void DisplayTurnOff()
{
    if (!DisplayInitialized_ || !DisplayOn_)
    {
        return;
    }

    // clear screen buffer
    DisplayCleanup();

    // Display off
    SendCommand(0xAE);

    Delay(100);

    // Switch off 16V supply
    DEACTIVATE_DISPLAY();
    DisplayOn_ = false;
}

void DisplayStandBy()
{
    // TODO If datasheet is available
}

void DisplayError(uint8_t error)
{
    DisplayTurnOn();
    DisplayWaitForVSync();
    DisplayClear(0xFFFF);
    uint16_t map[] = {0x0C00, 0x5200, 0x0100, 0x9080, 0x23E0, 0x03E0, 0x0FF8, 0x1FFC, 0x1FFC, 0x3FFE, 0x3FDE, 0x1FDC, 0x1FBC, 0x0FF8, 0x07F0, 0x01C0};

    for (int pos = 0; pos < DISPLAY_WIDTH / 16; pos++)
    {
        if ((error >> pos) & 1)
        {
            int xx = pos * 16 + 2;
            int yy = (DISPLAY_HEIGHT - 16) / 2;

            for (int x = 0; x < 16; x++)
            {
                for (int y = 0; y < 16; y++)
                {
                    if ((map[y] >> (15 - x)) & 1)
                    {
                        backBuffer_[(y + yy) * DISPLAY_WIDTH + (x + xx)] = 0;
                    }
                }
            }
        }
    }

    DisplaySwap();
    DisplayWaitForVSync();
}

void DisplayClear(TPixel color)
{
    for (int x = 0; x < DISPLAY_WIDTH; x++)
    {
        for (int y = 0; y < DISPLAY_HEIGHT; y++)
        {
            backBuffer_[y * DISPLAY_WIDTH + x] = color;
        }
    }

    DisplaySwap();
}

void DisplayCleanup()
{
    if (!DisplayInitialized_)
    {
        return;
    }

    DisplayWaitForVSync();
    memset(backBuffer_, 0, sizeof(buf1_));
    DisplaySwap();
    DisplayWaitForVSync();
}

void DisplayPrepareForFrame()
{
    SendCommand2(0x15, // Set Column Address range
                 0x24, // start seg36 (see schematic of EPF1002)
                 0x5B); // end  seg91 (see schematic of EPF1002)

    SendCommand2(0x75, // Set Row Address range
                 0x00, // start com0
                 0x7F); // end  com127

    // Write start command
    SendCommand(0x5C); // Enable MCU to write data into RAM
    Delay(100);
    START_DISPLAY_COMMAND();
    Delay(100);
    END_DISPLAY_COMMAND();

    //SendCommand(0x83); // Enable MCU to write data into RAM
    DISPLAY_TRANSMISSION_START();
}

bool DisplayIdle()
{
    return !TransferActive_;
}

bool DisplaySend()
{
    if (!DisplayInitialized_)
    {
        return true;
    }

    if (!DisplaySpiTransferComplete())
    {
        TransferActive_ = true;
        return false;
    }

    if (TransferActive_)
    {
        // finish transfer
        //START_DISPLAY_COMMAND();
        Delay(100);
        TransferActive_ = false;
        DISPLAY_TRANSMISSION_END();
    }

    if (DisplayRefreshRequired_)
    {
        DisplayRefreshRequired_ = false;
        TransferActive_ = true;
        DisplayPrepareForFrame();
        DisplaySpiTransfer();
        return false;
    }
    else
    {
        return true;
    }
}

void DisplayWaitForVSync()
{
    while (!DisplaySend())
    {
        NOTIFY_BUSYLOOP_ACTIVE();
        // Do something while waiting

        Transfer();
    };

    NOTIFY_BUSYLOOP_INACTIVE();
}

static bool actRotate_ = false;

bool DisplayIsRotated()
{
    return (actRotate_);
}

void DisplayRotate(bool rotate)
{
    if ((!rotate) == (!actRotate_))
    {
        return;
    }

    actRotate_ = rotate;

    SendCommand1(0xFD, // unlock
                 0x12); // registers

    if (rotate)
    {
        SendCommand2(0xA0, // Set Re-map / Color Depth (Display RAM to Panel)
                     0x01 // vertical address inc. mode
                     | 0x02 // Column Add. 127 mapped to SEG0
                     //  |   0x04   // ColorSequence C B A
                     //  |   0x10 // Scan from COM[N-1] to COM0
                     | 0x20 // COM split odd/even (see schematic of EPF1002)
                     | 0x40, // 65k colors
                     0x00); // always zero
    }
    else
    {
        SendCommand2(0xA0, // Set Re-map / Color Depth (Display RAM to Panel)
                     0x01 // vertical address inc. mode
                     //  |   0x02   // Column Add. 127 mapped to SEG0
                     //  |   0x04   // ColorSequence C B A
                     | 0x10 // Scan from COM[N-1] to COM0
                     | 0x20 // COM split odd/even (see schematic of EPF1002)
                     | 0x40, // 65k colors
                     0x00); // always zero
    }
}

/// Sets display brightness
/// \param brightness range 0..3 with 3 being max brightness
void DisplaySetBrightness(uint8_t brightness)
{
    if (brightness > MAX_BRIGHTNESS)
    {
        brightness = MAX_BRIGHTNESS;
    }
    if (actBrightness_ == brightness)
    {
        return;
    }
    actBrightness_   = brightness;
    uint8_t contrast = brightnessTable_[brightness];

    SendCommand1(0xFD, // unlock
                 0x12); // registers

    SendCommand3(0xC1, // Set Contrast Current for Color A,B,C
                 contrast,
                 contrast,
                 contrast); // Contrast Value Color A,B,C
}

static void SendInitString()
{
    actRotate_       = 0;
    actBrightness_   = MAX_BRIGHTNESS;
    uint8_t contrast = brightnessTable_[actBrightness_];

    SendCommand1(0xFD, // unlock
                 0x12); // registers

    SendCommand1(0xFD, // unlock
                 0x12); // registers

    SendCommand(0xAE); // Display off

    SendCommand3(0xC1, // Set Contrast Current for Color A,B,C
                 contrast,
                 contrast,
                 contrast); // Contrast Value Color A,B,C    16V 180cd/m2

    SendCommand2(0x15, // Set Column Address range
                 0x24, // start seg36 (see schematic of EPF1002)
                 0x5B); // end  seg91 (see schematic of EPF1002)

    SendCommand2(0x75, // Set Row Address range
                 0x00, // start com0
                 0x7F); // end  com127

    SendCommand2(0xA0, // Set Re-map / Color Depth (Display RAM to Panel)
                 0x01 // vertical address inc. mode
                 //               | 0x02   // Column Add. 127 mapped to SEG0
                 //               | 0x04   // ColorSequence C B A
                 | 0x10 // Scan from COM[N-1] to COM0
                 | 0x20 // COM split odd/even (see schematic of EPF1002)
                 | 0x40, // 65k colors
                 0x00); // always zero

    SendCommand1(0xA1, // Set Display Start Line
                 0x00); // .. to 0

    SendCommand1(0xA2, // Set Display Offset
                 0x00); // .. to 0

    SendCommand(0xA6); // Reset to normal display

    SendCommand1(0xCA, // Set MUX Ratio
                 0x7F); // 4-127[reset127]

    SendCommand1(0xB1, // Set Reset (Phase 1) /Pre-charge (Phase 2)period
                 0x84); // A[3:0] Phase 1:1  A[7:4] Phase 2:1  *0は無効(1-15CLK)

    SendCommand1(0xB3, // Front Clock Divider (DivSet)/ Oscillator Frequency
                 0x20); //

    SendCommand1(0xB6, // Set Second Pre-charge Period
                 0x08); // 8 DCLKS [reset]

    SendCommandTable(0xB8, // set shared gray scale table for Color A B C
                     GAMMA_TABLE_,
                     63);

    SendCommand1(0xBB, // Set Pre-charge voltage
                 0x1E); // 0.5*VCC[reset]

    SendCommand1(0xBE, // Set VCOMH Voltage
                 0x05); // 0.82*VCC[reset]

    SendCommand1(0xAD, // VCOMH Dselect Level
                 0x90); // 80h:external IREF 90h:internal IREF
}

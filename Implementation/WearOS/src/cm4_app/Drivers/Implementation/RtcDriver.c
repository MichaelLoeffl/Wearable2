#include "Drivers/RtcDriver.h"

#include <cy_rtc.h>

#define RTC1_10_MONTH_OFFSET (28U)
#define RTC1_MONTH_OFFSET (24U)
#define RTC1_10_DAY_OFFSET (20U)
#define RTC1_DAY_OFFSET (16U)
#define RTC1_1000_YEAR_OFFSET (12U)
#define RTC1_100_YEAR_OFFSET (8U)
#define RTC1_10_YEAR_OFFSET (4U)
#define RTC1_YEAR_OFFSET (0U)

static const cy_stc_rtc_config_t rtc_config =
{
    .sec = 0U,
    .min = 0U,
    .hour = 12U,
    .amPm = CY_RTC_AM,
    .hrFormat = CY_RTC_24_HOURS,
    .dayOfWeek = CY_RTC_SUNDAY,
    .date = 1U,
    .month = CY_RTC_JANUARY,
    .year = 0U,
};

/**
 * @brief this function initializes the RTC
 * @param none
 * @return none
 */
void HAL_RTC_Init()
{
    Cy_RTC_Init(&rtc_config);
}

/**
 * @brief this function reads the current date and time from the RTC
 * @param cy_stc_rtc_config_t* time the time struct to save the data in it
 * @return none
 */
void HAL_RTC_GetDateAndTime(cy_stc_rtc_config_t* timeData)
{
    Cy_RTC_GetDateAndTime(timeData);
}

/**
 * @brief this function sets the date and time to the RTC
 * @param uint32_t sec     seconds to set
 * @param uint32_t min     minutes to set
 * @param uint32_t hour    hours to set
 * @param uint32_t date    date to set
 * @param uint32_t month   month to set
 * @param uint32_t year    year to set
 * @return none
 */
void HAL_RTC_SetDateAndTime(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second)
{
    Cy_RTC_SetDateAndTimeDirect(second, minute, hour, day, month, year);
}

void HAL_RTC_SetAlarm(bool active, uint32_t hour, uint32_t minute, uint32_t second)
{
    cy_stc_rtc_alarm_t alarmData;
    alarmData.almEn = active ? CY_RTC_ALARM_ENABLE : CY_RTC_ALARM_DISABLE;

    alarmData.hour   = hour;
    alarmData.hourEn = true;
    alarmData.min    = minute;
    alarmData.minEn  = true;
    alarmData.sec    = second;
    alarmData.secEn  = true;

    alarmData.dayOfWeek   = 1;
    alarmData.dayOfWeekEn = false;
    alarmData.date        = 1;
    alarmData.dateEn      = false;
    alarmData.month       = 1;
    alarmData.monthEn     = false;

    Cy_RTC_SetAlarmDateAndTime(&alarmData, 0);
}

void HAL_RTC_GetAlarm(cy_stc_rtc_alarm_t* alarmData)
{
    Cy_RTC_GetAlarmDateAndTime(alarmData, 0);
}

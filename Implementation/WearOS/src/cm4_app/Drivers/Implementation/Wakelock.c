#include "Drivers/Wakelock.h"
#include "Drivers/Display.h"
#include "Drivers/PowerStates.h"

#define BACKGROUND_ACTIVITY_TIMEOUT (1100)

static uint16_t timeoutForegroundActivity_ = 3000;
static uint16_t timeoutBackgroundActivity_ = BACKGROUND_ACTIVITY_TIMEOUT;

static volatile PowerState currentPowerState_ = POWER_STATE_ACTIVE;
static uint16_t wakeLockCounter_[POWER_STATE_MAX];

static Wakelock userInteractionWakelock_ = 0;
static Wakelock snoozeWakelock_          = 0;

void CoreEnableInterrupts();
void CoreDisableInterrupts();

PowerState DetermineCurrentPowerState()
{
    PowerState result = POWER_STATE_DEEP_SLEEP;
    for (int i = 1; i < POWER_STATE_MAX; i++)
    {
        if (wakeLockCounter_[i] > 0)
        {
            result = (PowerState)i;
        }
    }

    return result;
}

void ReleaseWakelock(Wakelock* wakelock)
{
    UpdateWakelock(wakelock, POWER_STATE_DEEP_SLEEP);
}

PowerState GetWakelockState(Wakelock* wakelock)
{
    return *wakelock;
}

bool IsWakelockReleased(Wakelock* wakelock)
{
    return *wakelock == POWER_STATE_DEEP_SLEEP;
}

void UpdateWakelock(Wakelock* wakelock, PowerState state)
{
    if (*wakelock == state)
    {
        return;
    }

    CoreDisableInterrupts();

    if (*wakelock < POWER_STATE_MAX && *wakelock != POWER_STATE_DEEP_SLEEP && wakeLockCounter_[*wakelock] > 0)
    {
        wakeLockCounter_[*wakelock]--;

        // lower PowerState if possible
        while ((currentPowerState_ >= 1) && (wakeLockCounter_[currentPowerState_] == 0))
        {
            // Fall down power states
            currentPowerState_--;
        }
    }

    *wakelock = state;
    if (*wakelock < POWER_STATE_MAX && *wakelock != POWER_STATE_DEEP_SLEEP)
    {
        wakeLockCounter_[*wakelock]++;

        if (currentPowerState_ < state)
        {
            currentPowerState_ = state;
        }
    }

    CoreEnableInterrupts();
}

bool DeviceSleeping()
{
    return currentPowerState_ == POWER_STATE_DEEP_SLEEP;
}

bool DeviceInDeepSleep()
{
    return currentPowerState_ == POWER_STATE_DEEP_SLEEP;
}

PowerState GetCurrentPowerState()
{
    return currentPowerState_;
}

void WakeDeviceIntoSnooze()
{
    timeoutBackgroundActivity_ = BACKGROUND_ACTIVITY_TIMEOUT;
    UpdateWakelock(&snoozeWakelock_, POWER_STATE_BACKGROUND_ACTIVE);
}

void DeviceUserIntervention(uint16_t keepAwake)
{
    if (timeoutForegroundActivity_ < keepAwake)
    {
        timeoutForegroundActivity_ = keepAwake;
    }
    UpdateWakelock(&userInteractionWakelock_, POWER_STATE_ACTIVE);
}

bool DecrementTime(uint16_t* timeoutValue, uint32_t deltaT)
{
    if (deltaT <= *timeoutValue)
    {
        *timeoutValue -= deltaT;
    }
    else
    {
        *timeoutValue = 0;
    }
    return *timeoutValue != 0;
}

void CheckForegroundActivity(uint32_t deltaT)
{
    bool status = DecrementTime(&timeoutForegroundActivity_, deltaT);

    if (status)
    {
        UpdateWakelock(&userInteractionWakelock_, POWER_STATE_ACTIVE);
    }
    else
    {
        ReleaseWakelock(&userInteractionWakelock_);
    }
}

void CheckBackgroundActivity(uint32_t deltaT)
{
    bool status = DecrementTime(&timeoutBackgroundActivity_, deltaT);

    if (timeoutForegroundActivity_)
    {
        timeoutBackgroundActivity_ = BACKGROUND_ACTIVITY_TIMEOUT;
        status                     = true;
    }

    if (status)
    {
        UpdateWakelock(&snoozeWakelock_, POWER_STATE_BACKGROUND_ACTIVE);
    }
    else
    {
        ReleaseWakelock(&snoozeWakelock_);
    }
}

bool UpdatePowerManager(uint32_t deltaT)
{
    CheckForegroundActivity(deltaT);
    CheckBackgroundActivity(deltaT);

    static PowerState lastPowerState = POWER_STATE_MAX;
    PowerState temp = currentPowerState_;
    if (lastPowerState != temp)
    {
        SwitchPowerState(temp, lastPowerState);
        lastPowerState = temp;
    }

    return DeviceSleeping();
}

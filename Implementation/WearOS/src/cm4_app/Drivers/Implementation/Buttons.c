#include "Drivers/Buttons.h"
#include "Drivers/Core.h"
#include <cy_gpio.h>
#include <cybsp.h>
#include <cyhal_gpio.h>
#include <cyhal_hwmgr.h>
#include "Common/Debugging.h"
#include "Drivers/PowerStates.h"
#include "PinMapping.h"

CFUNC void DeviceUserIntervention(uint16_t keepAwake);

static uint32_t buttonState_ = 0xFFFFFFFF;
static uint32_t longPressCounter_ = 0;
static bool pressed_ = false;
static bool unregisterClick_ = false;
static bool clicked_ = 0;
static bool longPressed_ = 0;

uint32_t debugInterfaceButton = 0;

void ButtonInterrupt()
{
    if (GetCurrentPowerState() != POWER_STATE_ACTIVE)
    {
        unregisterClick_ = true;
    }
    DeviceUserIntervention(5000);
    Cy_GPIO_ClearInterrupt(GPIO_PRT0, 4);
}

void ButtonUpdate(uint32_t deltaT)
{
    longPressCounter_++;
    static uint32_t cnt0, cnt1;
    uint32_t delta;
    uint32_t sample = GPIO_PRT_IN(CYHAL_GET_PORTADDR(P0_4));

    delta = sample ^ buttonState_;
    cnt1 = (cnt1 ^ cnt0) & (delta & sample);
    cnt0 = ~cnt0 & (delta & sample);
    buttonState_ ^= (delta & ~(cnt0 | cnt1));
    bool newstate = (buttonState_ & 0x10) == 0;

    if (debugInterfaceButton >= deltaT)
    {
        debugInterfaceButton -= deltaT;
        newstate = true;
    }
    else
    {
        debugInterfaceButton = 0;
    }

    if (longPressCounter_ == 100 && pressed_)
    {
        longPressed_ = true;
        unregisterClick_ = true;
    }

    if (pressed_ != newstate)
    {
        pressed_ = newstate;

        if (pressed_)
        {
            NOTIFY_BUTTON_PRESSED();
        }
        else
        {
            NOTIFY_BUTTON_DEPRESSED();
        }

        longPressCounter_ = 0;

        if (!pressed_)
        {
            if (!unregisterClick_)
            {
                clicked_ = true;
            }
            unregisterClick_ = false;
        }
        else
        {

        }
    }
}

void ButtonInitialize()
{
    // Initialize the User Button
    cyhal_gpio_init(P0_4, CYHAL_GPIO_DIR_INPUT, CYHAL_GPIO_DRIVE_PULLUP, CYBSP_BTN_OFF);
    // Enable the GPIO interrupt to wake-up the device
    cyhal_gpio_enable_event(P0_4, CYHAL_GPIO_IRQ_FALL, CYHAL_ISR_PRIORITY_DEFAULT, true);

    cy_stc_sysint_t intrCfg = {.intrSrc      = ioss_interrupts_gpio_0_IRQn, // Interrupt source is GPIO port 0 interrupt
                               .intrPriority = 4UL};
    // Initialize the interrupt with vector at Interrupt_Handler_Port0()
    Cy_SysInt_Init(&intrCfg, &ButtonInterrupt);
    // Enable the interrupt
    NVIC_EnableIRQ(intrCfg.intrSrc);
}

bool ButtonIsPressed()
{
    return pressed_;
}

bool ButtonGetEvents(bool* clicked, bool* longPressed)
{
    CoreDisableInterrupts();
    *clicked = clicked_;
    *longPressed = longPressed_;
    clicked_ = false;
    longPressed_ = false;
    CoreEnableInterrupts();
    return pressed_;
}

bool IsSecondButtonPressed()
{
    return false;
}

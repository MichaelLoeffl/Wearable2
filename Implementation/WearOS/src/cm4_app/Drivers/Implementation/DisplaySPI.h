#ifndef DISPLAY_SPI_H
#define DISPLAY_SPI_H

#include "Common/Common.h"

void DisplaySpiInitialize();
bool DisplaySpiTransferComplete();
void DisplaySpiWriteSingleByte(uint8_t byte, bool command);
void DisplaySpiTransfer();

#endif // DISPLAY_SPI_H

#include <stdint.h>

uint16_t stepCounterSteps_ = 0;

/**
 * @brief this function enables the step counter
 * @warning it does not initialize it!
 * @param none
 * @return none
 */
void HAL_StepCounter_Enable()
{
}

/**
 * @brief this function sets the step counter back to zero
 * @param none
 * @return none
 */
void HAL_StepCounter_Reset()
{
    stepCounterSteps_ = 0;
}

/**
 * @brief this function returns the counted steps since the last
 *                  reset (with the reset function) of the step counter
 * @param none
 * @return the counted steps
 */
uint16_t HAL_StepCounter_GetCountedSteps()
{
    stepCounterSteps_ += 100;
    return stepCounterSteps_;
}

#ifndef RTCDRIVER_H
#define RTCDRIVER_H

#include "Common/Common.h"
#include <cy_rtc.h>

/**
 * @brief  this function initializes the RTC
 * @param  none
 * @return none
 */
CFUNC void HAL_RTC_Init();
/**
 * @brief  this function reads the current date and time from the RTC
 * @param  cy_stc_rtc_config_t* time        the time struct to save the data in it
 * @return none
 */
CFUNC void HAL_RTC_GetDateAndTime(cy_stc_rtc_config_t* timeData);
/**
 * @brief  this function sets the date and time to the RTC
 * @param  uint32_t sec        seconds to set
 * @param  uint32_t min        minutes to set
 * @param  uint32_t hour    hours to set
 * @param  uint32_t date    date to set
 * @param  uint32_t month    month to set
 * @param  uint32_t year    year to set
 * @return none
 */
CFUNC void HAL_RTC_SetDateAndTime(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second);

CFUNC void HAL_RTC_SetAlarm(bool active, uint32_t hour, uint32_t minute, uint32_t second);

CFUNC void HAL_RTC_GetAlarm(cy_stc_rtc_alarm_t* alarmData);

#endif /* RTCDRIVER_H */

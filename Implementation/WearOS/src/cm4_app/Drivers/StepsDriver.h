#ifndef STEPSDRIVER_H
#define STEPSDRIVER_H

#include "Common/Common.h"
#include <stdint.h>

/**
 * @brief  this function enables the step counter
 * @warning it does not initialize it!
 * @param  none
 * @return none
 */
CFUNC bool HAL_StepCounter_Enable();

/**
 * @brief  this function sets the step counter back to zero
 * @param  none
 * @return none
 */
CFUNC void HAL_StepCounter_Reset();

/**
 * @brief  this function returns the counted steps since the last
 *                     reset (with the reset function) of the step counter
 * @param  none
 * @return the counted steps
 */
CFUNC uint16_t HAL_StepCounter_GetCountedSteps();

#endif /* STEPSDRIVER_H */

#ifndef BUTTONS_H
#define BUTTONS_H

#include "Common/Common.h"

CFUNC void ButtonInitialize();
CFUNC void ButtonUpdate();
CFUNC bool ButtonIsPressed();
CFUNC bool ButtonGetEvents(bool* clicked, bool* longPressed);

#endif /* BUTTONS_H */

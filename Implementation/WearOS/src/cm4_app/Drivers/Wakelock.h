#ifndef POWERMANAGER_HPP_7639D9
#define POWERMANAGER_HPP_7639D9

#include "Common/Common.h"
#include "Drivers/Core.h"
#include "Drivers/PowerStates.h"

typedef uint8_t Wakelock;
    
CFUNC bool UpdatePowerManager(uint32_t deltaT);
CFUNC void UpdateWakelock(Wakelock* wakelock, PowerState state);
CFUNC void ReleaseWakelock(Wakelock* wakelock);
CFUNC PowerState GetWakelockState(Wakelock* wakelock);
CFUNC bool IsWakelockReleased(Wakelock* wakelock);

#endif

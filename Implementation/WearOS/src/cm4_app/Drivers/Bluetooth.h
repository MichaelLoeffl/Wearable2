#ifndef BLUETOOTH_H_3E6D9B
#define BLUETOOTH_H_3E6D9B

#include "Common/Common.h"

CFUNC void BleClearBondList();
CFUNC void BleStartAdvertising();
CFUNC void BleStopAdvertising();
CFUNC bool BleIsAdvertising();
CFUNC int  BleBondedDevices();

CFUNC void BleInit();
CFUNC void BleUpdate();
CFUNC void BleStart();
CFUNC void BleStop();
CFUNC void BleShutdownBlocking();

#endif /* BLUETOOTH_H_3E6D9B */

#ifndef POWER_STATES_H
#define POWER_STATES_H

#include "Common/Common.h"

typedef enum
{
    REQ_PERIPHERAL_NONE        = 0,
    REQ_PERIPHERAL_HEARTRATE   = 1 << 0,
    REQ_PERIPHERAL_FLASH_READ  = 1 << 1,
    REQ_PERIPHERAL_FLASH_WRITE = 1 << 2,
    REQ_PERIPHERAL_MOTOR       = 1 << 3,
    REQ_PERIPHERAL_DISPLAY     = 1 << 4
} PeriperalRequirementFlags;

typedef enum
{
    POWER_STATE_DEEP_SLEEP = 0,
    POWER_STATE_PASSIVE,
    POWER_STATE_BACKGROUND_PASSIVE,
    POWER_STATE_WORKOUT,
    POWER_STATE_BACKGROUND_ACTIVE,
    POWER_STATE_ACTIVE,
    POWER_STATE_MAX,
} PowerState;

CFUNC void SwitchPowerState(PowerState state, PowerState previous);
CFUNC PowerState DetermineRequiredPowerState(PeriperalRequirementFlags requirementFlags);

CFUNC PowerState GetCurrentPowerState();

#endif

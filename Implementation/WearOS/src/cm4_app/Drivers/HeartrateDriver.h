#ifndef HEARTRATEDRIVER_H
#define HEARTRATEDRIVER_H

#include "Common/Common.h"

#define INVALID_HEARTRATE (0xF000)


CFUNC void HeartrateInitialize();
CFUNC bool HeartrateStartMeasurement();

// returns heart rate or INVALID_HEARTRATE if no heartrate has been found
CFUNC uint32_t HeartrateUpdate();
CFUNC void HeartrateEndMeasurement();
// returns heart rate or INVALID_HEARTRATE if no heartrate has been found
CFUNC uint32_t HeartrateGetHeartrate();

#endif /* HEARTRATEDRIVER_H */

#ifndef DISPLAY_H
#define DISPLAY_H

#include "Common/Common.h"

//#define RGB_to_TPixel(R, G, B) LV_COLOR_MAKE(R, G, B)

typedef uint16_t TPixel; // using RGB565 with swapped bytes

#define RGB_to_TPixel(R, G, B) ((((uint16_t)(B)&0x1F) << 8) | (((uint16_t)(G)&0x7) << 13) | (((uint16_t)(G)&0x38) >> 3) | (((uint16_t)(R)&0x1F) << 3))

#define DISPLAY_WIDTH (128)
#define DISPLAY_HEIGHT (56)

#define DISPLAY_BYTES_PER_PIXEL (sizeof(TPixel))

extern TPixel* backBuffer_;
extern TPixel* frontBuffer_;

CFUNC void DisplayInitialize();
CFUNC void DisplayError(uint8_t error);
CFUNC void DisplayClear(TPixel color);
CFUNC void DisplayCleanup();
CFUNC void DisplayClear(TPixel color);
CFUNC void DisplayTurnOn();
CFUNC void DisplayTurnOff();
CFUNC void DisplayRotate(bool rotate);
CFUNC bool DisplayIsRotated();

#define MAX_BRIGHTNESS 3
CFUNC void DisplaySetBrightness(uint8_t brightness);

CFUNC void DisplayWaitForVSync();

#endif /* DISPLAY_H */

#ifndef KOMOOTSERVICE_H_5FC5F5
#define KOMOOTSERVICE_H_5FC5F5

#include "Common/Common.h"

CFUNC void KomootDataHandler(void* data);
CFUNC bool KomootConnectionCheck(bool peek);

#endif /* KOMOOTSERVICE_H_5FC5F5 */
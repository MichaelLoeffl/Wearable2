#ifndef INVOKEBOOTLOADER_H
#define INVOKEBOOTLOADER_H

#include "Common/Common.h"

// Restarts the watch
CFUNC void RestartDevice(void);

// never returns; triggers sw reset
CFUNC void InvokeBootloader(void);

#endif /* INVOKEBOOTLOADER_H */

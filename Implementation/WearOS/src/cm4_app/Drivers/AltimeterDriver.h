#ifndef ALTIMETERDRIVER_H
#define ALTIMETERDRIVER_H

#include "Common/Common.h"

/**
 * @brief  this function initializes the air pressure sensor
 * @param  none
 * @return none
 */
CFUNC void HAL_Altimeter_Init();

/**
 * @brief  this function disables the air pressure sensor
 * @param  none
 * @return none
 */
CFUNC void HAL_Altimeter_Disable();

/**
 * @brief  this function gets the current temperature from the sensor
 * @param  none
 * @return the measured temperature
 */
CFUNC float HAL_Altimeter_GetTemperature();

/**
 * @brief  this function measures the current pressure
 * @param  none
 * @return the current pressure
 */
CFUNC float HAL_Altimeter_GetPressure();

/**
 * @brief  this function calculates the altitude based on pressure and given seaLevel in hPa
 * @param  The current hPa at sea level
 * @return The approximate altitude above sea level in meters
 */
CFUNC float HAL_Altimeter_GetAltitude(float seaLevelhPa);

#endif /* ALTIMETERDRIVER_H */

#ifndef BLECOMMUNICATION_H
#define BLECOMMUNICATION_H

#include "Common/AppIds.hpp"
#include "Common/Common.h"

CFUNC void AddNotification(AppIds appID, uint32_t remoteID);
CFUNC void UpdateNotificationMessage(uint32_t remoteID, const unsigned char* message, const uint16_t length);
CFUNC void UpdateNotificationTitle(uint32_t remoteID, const unsigned char* title, const uint16_t length);
CFUNC void UpdateNotificationAppIdentifier(uint32_t remoteID, const unsigned char* title, const uint16_t length);
CFUNC void UpdateHasPositiveAction(uint32_t remoteID, bool value);
CFUNC void UpdateHasNegativeAction(uint32_t remoteID, bool value);
CFUNC void RemoveNotification(uint32_t remoteID);
CFUNC void RemoveAllNotifications();
CFUNC bool GetIncompleteNotification(uint32_t* remoteID);
CFUNC void SendPositiveNotificationAction(uint32_t notificationId);
CFUNC void SendNegativeNotificationAction(uint32_t notificationId);

#endif /* BLECOMMUNICATION_H */

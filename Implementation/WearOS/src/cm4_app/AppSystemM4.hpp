#ifndef APP_SYSTEM_M4_H_78F55E
#define APP_SYSTEM_M4_H_78F55E

#include "Common/App.hpp"
#include "Common/AppSystem.hpp"

class NotificationService;
class SchedulingService;
class TimeService;
class PairingService;
class NavigationService;
class PowerService;
class FitnessService;
class PowerManager;
class AlarmReminderService;
class RemoteControlService;

class AppSystemM4 : public AppSystem
{
    friend PowerManager;

protected:
    bool HasGui() override { return true; }
    void BeforeMainLoop() override;
    void MainLoop(uint32_t delta) override;

public:
    AppSystemM4() {}

    App& Initialize() override;

    void WokenUp();
    void UserInteraction();

    NotificationService* GetNotificationService();
    SchedulingService* GetSchedulingService();
    TimeService* GetTimeService();
    PairingService* GetPairingService();
    NavigationService* GetNavigationService();
    PowerService* GetPowerService();
    FitnessService* GetFitnessService();
    AlarmReminderService* GetAlarmReminderService();
    RemoteControlService* GetRemoteControlService();

    void UpdateNotificationCount(AppIds appId, uint16_t count);

private:
    void HandleButton();
    void HandleGraphics(uint32_t deltaT);

    void Draw();
    void SecondElapsed();
    void BackPressed();
    void LongPressHome();
};

extern AppSystemM4 appSystem_;

#endif /* APP_SYSTEM_M4_H_78F55E */

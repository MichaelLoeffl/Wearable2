#include "Renderer.hpp"
#include "AppSystemM4.hpp"
#include "Common/App.hpp"
#include "Common/Graphics.h"
#include "Drivers/Core.h"
#include "Drivers/Display.h"
#include "Drivers/Touch.h"
#include "lv_core/lv_disp.h"
#include "lv_hal/lv_hal_disp.h"
#include "lv_misc/lv_task.h"
#include "lv_objx/lv_label.h"
#include "lv_themes/lv_theme.h"

lv_task_t* tasks[10];

CFUNC void DisplayFlush(lv_disp_drv_t* disp_drv, const lv_area_t* area, lv_color_t* color_p);

void Renderer::Initialize()
{
#ifndef SIMULATOR
    static lv_disp_buf_t disp_buf;
    static lv_color_t buf_1[DISPLAY_WIDTH * DISPLAY_HEIGHT];
    lv_disp_buf_init(&disp_buf, buf_1, nullptr, DISPLAY_WIDTH * DISPLAY_HEIGHT);

    lv_disp_drv_t displayDriver;
    lv_disp_drv_init(&displayDriver); // Basic initialization
    displayDriver.buffer   = &disp_buf;
    displayDriver.flush_cb = &DisplayFlush;
    lv_disp_drv_register(&displayDriver); // Register the driver in LittlevGL
#endif

    lv_theme_t* theme                      = lv_theme_night_init(0, NULL);
    theme->style.scr->body.main_color.full = 0xFFFF0000;

    theme->style.btn.ina->body.padding.bottom = 0;
    theme->style.btn.ina->body.padding.left   = 0;
    theme->style.btn.ina->body.padding.right  = 0;
    theme->style.btn.ina->body.padding.top    = 0;
    theme->style.btn.ina->body.padding.inner  = 0;

    theme->style.btn.rel->body.padding.bottom = 0;
    theme->style.btn.rel->body.padding.left   = 0;
    theme->style.btn.rel->body.padding.right  = 0;
    theme->style.btn.rel->body.padding.top    = 0;
    theme->style.btn.rel->body.padding.inner  = 0;

    theme->style.btn.pr->body.padding.left   = 0;
    theme->style.btn.pr->body.padding.right  = 0;
    theme->style.btn.pr->body.padding.top    = 0;
    theme->style.btn.pr->body.padding.bottom = 0;
    theme->style.btn.pr->body.padding.inner  = 0;

    theme->style.btn.tgl_pr->body.padding.left   = 0;
    theme->style.btn.tgl_pr->body.padding.right  = 0;
    theme->style.btn.tgl_pr->body.padding.top    = 0;
    theme->style.btn.tgl_pr->body.padding.bottom = 0;
    theme->style.btn.tgl_pr->body.padding.inner  = 0;

    theme->style.btn.tgl_rel->body.padding.left   = 0;
    theme->style.btn.tgl_rel->body.padding.right  = 0;
    theme->style.btn.tgl_rel->body.padding.top    = 0;
    theme->style.btn.tgl_rel->body.padding.bottom = 0;
    theme->style.btn.tgl_rel->body.padding.inner  = 0;

    theme->style.page.bg->body.padding.left      = 0;
    theme->style.page.bg->body.padding.right     = 0;
    theme->style.page.bg->body.padding.top       = 0;
    theme->style.page.bg->body.padding.bottom    = 0;
    theme->style.page.bg->body.padding.inner     = 0;
    theme->style.page.bg->body.radius            = 0;
    theme->style.page.bg->body.border.width      = 4;
    theme->style.page.bg->body.border.color.full = 0xFFFFFFFF;
    theme->style.page.scrl->body.main_color.full = 0xFF000000;
    theme->style.page.scrl->body.grad_color.full = 0xFF000000;

    theme->style.page.scrl->body.border.color.full = 0xFFFFFFFF;
    theme->style.page.scrl->body.border.width      = 0;

    theme->style.cont->body.border.color.full = 0xFFFFFFFF;
    theme->style.cont->body.padding.top       = 0;
    theme->style.cont->body.border.width      = 0;
    theme->style.cont->body.grad_color.full   = 0xFF000000;
    theme->style.cont->body.main_color.full   = 0xFF000000;

    theme->style.cb.bg->body.border.color.full = 0xFFFFFFFF;
    theme->style.panel->body.border.color.full = 0xFFFFFFFF;
    theme->style.sw.bg->body.border.color.full = 0xFFFFFFFF;

    theme->style.page.sb->line.width      = 0;
    theme->style.page.sb->line.color.full = 0xFFFFFFFF;

    theme->style.bg->body.padding.left   = 0;
    theme->style.bg->body.padding.right  = 0;
    theme->style.bg->body.padding.top    = 0;
    theme->style.bg->body.padding.bottom = 0;
    theme->style.bg->body.padding.inner  = 0;

    theme->style.page.scrl->body.padding.left   = 0;
    theme->style.page.scrl->body.padding.right  = 0;
    theme->style.page.scrl->body.padding.top    = 0;
    theme->style.page.scrl->body.padding.bottom = 0;
    theme->style.page.scrl->body.padding.inner  = 0;

    theme->style.page.sb->body.padding.left   = 0;
    theme->style.page.sb->body.padding.right  = 0;
    theme->style.page.sb->body.padding.top    = 0;
    theme->style.page.sb->body.padding.bottom = 0;
    theme->style.page.sb->body.padding.inner  = 0;

    theme->style.bg->body.border.color.full = 0xFFFFFFFF;

    lv_style_scr.body.main_color.full = 0;
    lv_style_scr.body.grad_color.full = 0;

#ifdef SIMULATOR
    theme->style.cont->body.border.color.full = 0xFFFFFFFF;
    theme->style.cont->body.border.width      = 0;

    lv_obj_t* screen  = lv_scr_act();
    screen->coords.x1 = SIMULATOR_PADDING;
    screen->coords.x2 = SIMULATOR_PADDING + SCREEN_WIDTH;
    screen->coords.y1 = SIMULATOR_PADDING;
    screen->coords.y2 = SIMULATOR_PADDING + SCREEN_HEIGHT;
#endif

    lv_theme_set_current(theme);
}

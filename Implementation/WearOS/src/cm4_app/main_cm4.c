#include "Drivers/Core.h"
#include "Drivers/Motor.h"
#include "Drivers/Touch.h"
#include <cybsp.h>
#include <cyhal.h>
#include "Drivers/Wakelock.h"
#include "PinMapping.h"
#include <cy_syslib.h>

#if BOARD_TYPE == BOARD_TYPE_CYPRESS
#pragma message "Compiling for Cypress devboard"
#elif BOARD_TYPE == BOARD_TYPE_QINNO
#pragma message "Compiling for qinno devboard"
#else
#error "BOARD_TYPE not specified"
#endif

extern uint32_t tickCounter;
extern Wakelock tickLock;

CFUNC void ButtonUpdate(uint32_t deltaT);

void SystickInterruptHandler()
{
    PowerState powerState = GetCurrentPowerState();
    tickCounter += 1;

    if (powerState > POWER_STATE_PASSIVE)
    {
        if ((tickCounter & 0x07) == 0)
        {
            ButtonUpdate(8);
        }
        if ((tickCounter & 0xFF) == 0)
        {
            UpdateWakelock(&tickLock, POWER_STATE_PASSIVE);
        }
    }

    if (powerState >= POWER_STATE_ACTIVE)
    {
        TouchUpdate(1);
    }
}

void Cy_SysLib_ProcessingFault()
{
    NOTIFY_PROCESSING_FAULT();
    HaltDevice(0x07);
}

void MainM4();

int main()
{
#ifdef DEBUG
    // Spin for some time to allow debugger to 'catch' and halt the CPU when restarting while debugging
    Cy_SysLib_Delay(200);
#endif

    MainM4();
}

#include "lvgl/lvgl.h"

/*******************************************************************************
 * Size: 14 px
 * Bpp: 4
 * Opts: 
 ******************************************************************************/

#ifndef FREEPIXEL14
#define FREEPIXEL14 1
#endif

#if FREEPIXEL14

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t gylph_bitmap[] = {
    /* U+20 " " */

    /* U+21 "!" */
    0x86, 0x86, 0x86, 0x86, 0x86, 0x86, 0x0, 0x54,
    0x86,

    /* U+22 "\"" */
    0x6, 0x40, 0x46, 0x2, 0xfa, 0xa, 0xf2, 0x2f,
    0xa0, 0xaf, 0x22, 0xfa, 0xa, 0xf2,

    /* U+23 "#" */
    0x4, 0xa0, 0xa4, 0x0, 0x4a, 0xa, 0x40, 0x1e,
    0xfd, 0xfe, 0x10, 0x4a, 0xa, 0x40, 0x4, 0xa0,
    0xa4, 0x0, 0x4a, 0xa, 0x40, 0x1e, 0xfd, 0xfe,
    0x10, 0x4a, 0xa, 0x40, 0x4, 0xa0, 0xa4, 0x0,

    /* U+24 "$" */
    0x0, 0x2, 0x10, 0x0, 0x3d, 0xee, 0xd1, 0x16,
    0x8, 0x60, 0x72, 0xc0, 0x86, 0x4, 0x2c, 0x8,
    0x60, 0x0, 0x16, 0xba, 0x67, 0x0, 0x8, 0x60,
    0xe0, 0x0, 0x86, 0xe, 0x1a, 0x8, 0x60, 0xc0,
    0x3d, 0xee, 0xd1, 0x0, 0x8, 0x60, 0x0,

    /* U+25 "%" */
    0x3, 0xd6, 0x0, 0x0, 0x19, 0x6, 0x40, 0x0,
    0x1b, 0x7, 0x50, 0xb0, 0x3, 0xd6, 0xa, 0x10,
    0x0, 0x0, 0x83, 0x0, 0x0, 0x5d, 0x50, 0x0,
    0x3, 0x80, 0x8d, 0x10, 0x18, 0x5, 0x40, 0xa0,
    0x1, 0x8, 0x60, 0xe0, 0x0, 0x0, 0x8d, 0x10,

    /* U+26 "&" */
    0x0, 0x5e, 0x50, 0x0, 0x26, 0x6, 0x20, 0x4,
    0xa0, 0xa4, 0x0, 0x6, 0xe5, 0x0, 0x3, 0xe7,
    0x0, 0x2, 0xc0, 0x86, 0xc2, 0x2c, 0x2, 0x86,
    0x2, 0xc0, 0xa, 0x40, 0x3, 0xdd, 0x5a, 0x10,

    /* U+27 "'" */
    0x26, 0x26, 0xf6, 0x6f, 0x66, 0xf6,

    /* U+28 "(" */
    0x0, 0x83, 0x8, 0x60, 0x5, 0x40, 0x58, 0x0,
    0x58, 0x0, 0x58, 0x0, 0x58, 0x0, 0x58, 0x0,
    0x57, 0x0, 0x8, 0x60, 0x0, 0x83,

    /* U+29 ")" */
    0x38, 0x0, 0x5, 0x80, 0x4, 0x50, 0x0, 0x86,
    0x0, 0x86, 0x0, 0x86, 0x0, 0x86, 0x0, 0x86,
    0x0, 0x75, 0x5, 0x80, 0x39, 0x0,

    /* U+2A "*" */
    0x0, 0x86, 0x0, 0x38, 0x86, 0xa1, 0x5, 0xee,
    0x30, 0x38, 0x86, 0xa1, 0x0, 0x21, 0x0,

    /* U+2B "+" */
    0x0, 0x4, 0x30, 0x0, 0x0, 0x86, 0x0, 0x0,
    0x8, 0x60, 0x1, 0xdd, 0xee, 0xdd, 0x0, 0x8,
    0x60, 0x0, 0x0, 0x86, 0x0, 0x0, 0x3, 0x20,
    0x0,

    /* U+2C "," */
    0x3, 0x93, 0x5, 0xf5, 0x5, 0xf5, 0x3e, 0x70,

    /* U+2D "-" */
    0x1d, 0xdd, 0xdd, 0x10,

    /* U+2E "." */
    0x4b, 0x46, 0xf6,

    /* U+2F "/" */
    0x0, 0x0, 0x8, 0x10, 0x0, 0x0, 0xc1, 0x0,
    0x0, 0x72, 0x0, 0x0, 0xa, 0x40, 0x0, 0x8,
    0x60, 0x0, 0x0, 0x54, 0x0, 0x0, 0x67, 0x0,
    0x0, 0x28, 0x20, 0x0, 0x4, 0xa0, 0x0, 0x2,
    0xc0, 0x0, 0x0, 0x18, 0x0, 0x0, 0x0,

    /* U+30 "0" */
    0x3, 0xdd, 0xd3, 0x1, 0xa0, 0x0, 0xa1, 0x2c,
    0x0, 0xc, 0x12, 0xc0, 0x8, 0xf1, 0x2c, 0x6,
    0x5c, 0x12, 0xfd, 0x60, 0xc1, 0x2c, 0x0, 0xc,
    0x12, 0xc0, 0x0, 0xc1, 0x3, 0xdd, 0xd3, 0x0,

    /* U+31 "1" */
    0x0, 0x86, 0x0, 0x5, 0xe6, 0x0, 0x38, 0x86,
    0x0, 0x0, 0x86, 0x0, 0x0, 0x86, 0x0, 0x0,
    0x86, 0x0, 0x0, 0x86, 0x0, 0x0, 0x86, 0x0,
    0x3d, 0xee, 0xd1,

    /* U+32 "2" */
    0x3, 0xdd, 0xd3, 0x1, 0x60, 0x0, 0x61, 0x2b,
    0x0, 0xc, 0x20, 0x10, 0x0, 0xc2, 0x0, 0x0,
    0x83, 0x0, 0x5, 0xd4, 0x0, 0x3, 0x80, 0x0,
    0x1, 0x80, 0x0, 0x0, 0x2f, 0xdd, 0xdd, 0x10,

    /* U+33 "3" */
    0x3, 0xee, 0xe3, 0x1, 0x80, 0x0, 0x81, 0x1,
    0x0, 0xc, 0x20, 0x0, 0x8, 0x30, 0x0, 0x5e,
    0x50, 0x0, 0x0, 0x8, 0xc1, 0x0, 0x0, 0xc,
    0x21, 0x90, 0x8, 0x30, 0x3, 0xdd, 0x40, 0x0,

    /* U+34 "4" */
    0x0, 0x6, 0xf4, 0x0, 0x4, 0x5a, 0x40, 0x0,
    0x68, 0xa4, 0x0, 0x4a, 0xa, 0x40, 0x1c, 0x70,
    0xa4, 0x1, 0xed, 0xdf, 0xe1, 0x0, 0x0, 0xa4,
    0x0, 0x0, 0xa, 0x40, 0x0, 0x0, 0xa4, 0x0,

    /* U+35 "5" */
    0x2f, 0xdd, 0xdd, 0x12, 0xb0, 0x0, 0x0, 0x2b,
    0x0, 0x0, 0x2, 0xb0, 0x0, 0x0, 0x17, 0x77,
    0x77, 0x0, 0x0, 0x0, 0xc2, 0x0, 0x0, 0xc,
    0x21, 0x90, 0x0, 0xb1, 0x3, 0xee, 0xe3, 0x0,

    /* U+36 "6" */
    0x0, 0x5e, 0xe3, 0x0, 0x38, 0x0, 0x0, 0x1b,
    0x0, 0x0, 0x2, 0xb0, 0x0, 0x0, 0x2d, 0x78,
    0x87, 0x2, 0xd4, 0x0, 0xc2, 0x2b, 0x0, 0xc,
    0x22, 0xb0, 0x0, 0xc2, 0x3, 0xdd, 0xd3, 0x0,

    /* U+37 "7" */
    0x2f, 0xdd, 0xdf, 0x21, 0x80, 0x0, 0xc2, 0x0,
    0x0, 0xc, 0x20, 0x0, 0x9, 0x40, 0x0, 0x0,
    0x62, 0x0, 0x0, 0x86, 0x0, 0x0, 0x38, 0x20,
    0x0, 0x5, 0x80, 0x0, 0x0, 0x58, 0x0, 0x0,

    /* U+38 "8" */
    0x3, 0xee, 0xe3, 0x1, 0x80, 0x0, 0x81, 0x2b,
    0x0, 0xc, 0x20, 0x38, 0x8, 0x30, 0x0, 0x5e,
    0x50, 0x1, 0x83, 0x3, 0x81, 0x2b, 0x0, 0xc,
    0x22, 0xb0, 0x0, 0xc2, 0x3, 0xdd, 0xd3, 0x0,

    /* U+39 "9" */
    0x3, 0xdd, 0xd3, 0x2, 0xb0, 0x0, 0xc2, 0x2b,
    0x0, 0xc, 0x22, 0xb0, 0x0, 0xc2, 0x1b, 0x0,
    0x8f, 0x20, 0x3d, 0xd4, 0xc2, 0x0, 0x0, 0xc,
    0x20, 0x0, 0x8, 0x30, 0x3, 0xee, 0x50, 0x0,

    /* U+3A ":" */
    0x6f, 0x64, 0xb4, 0x0, 0x0, 0x0, 0x4b, 0x46,
    0xf6,

    /* U+3B ";" */
    0x6, 0xf6, 0x4, 0xb4, 0x0, 0x0, 0x0, 0x0,
    0x3, 0x93, 0x5, 0xf5, 0x5, 0xf5, 0x3e, 0x70,

    /* U+3C "<" */
    0x0, 0x8, 0x30, 0x6, 0x50, 0x5, 0x60, 0x3,
    0xd6, 0x0, 0x0, 0x75, 0x0, 0x0, 0x83,

    /* U+3D "=" */
    0x1d, 0xdd, 0xdd, 0x10, 0x0, 0x0, 0x0, 0x1d,
    0xdd, 0xdd, 0x10,

    /* U+3E ">" */
    0x38, 0x0, 0x0, 0x56, 0x0, 0x0, 0x65, 0x0,
    0x0, 0x83, 0x5, 0xd5, 0x3, 0x80, 0x0,

    /* U+3F "?" */
    0x3, 0xee, 0xe3, 0x1, 0x60, 0x0, 0x61, 0x2c,
    0x0, 0xc, 0x20, 0x10, 0x0, 0xc2, 0x0, 0x0,
    0x83, 0x0, 0x4, 0xc5, 0x0, 0x0, 0x68, 0x0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x56, 0x0, 0x0,

    /* U+40 "@" */
    0x0, 0x5e, 0xe3, 0x0, 0x26, 0x0, 0x53, 0x4,
    0xa0, 0x0, 0xd2, 0xc0, 0x6f, 0x4d, 0x2c, 0x58,
    0x94, 0xd2, 0xc5, 0x89, 0x4d, 0x1b, 0x7, 0x6a,
    0x20, 0x27, 0x0, 0x0, 0x4, 0xa0, 0x9, 0x10,
    0x5, 0xdd, 0x30,

    /* U+41 "A" */
    0x3, 0xdd, 0xd3, 0x1, 0x80, 0x0, 0x81, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0,
    0xc, 0x22, 0xfd, 0xdd, 0xf2, 0x2c, 0x0, 0xc,
    0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0, 0xc, 0x20,

    /* U+42 "B" */
    0x2f, 0xdd, 0xd3, 0x2, 0xc0, 0x0, 0x61, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2e, 0x88,
    0x87, 0x2, 0xc0, 0x0, 0xc2, 0x2c, 0x0, 0xc,
    0x22, 0xc0, 0x0, 0xc2, 0x2f, 0xdd, 0xd3, 0x0,

    /* U+43 "C" */
    0x0, 0x5d, 0xd3, 0x0, 0x38, 0x0, 0xa1, 0x2c,
    0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0,
    0x0, 0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0, 0x0,
    0x0, 0x48, 0x0, 0xa1, 0x0, 0x5d, 0xd3, 0x0,

    /* U+44 "D" */
    0x2f, 0xdd, 0x50, 0x2, 0xc0, 0x8, 0x30, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0,
    0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0, 0xc,
    0x22, 0xc0, 0x8, 0x40, 0x2f, 0xdd, 0x50, 0x0,

    /* U+45 "E" */
    0x2f, 0xdd, 0xdd, 0x12, 0xc0, 0x0, 0x0, 0x2c,
    0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x2f, 0xdd,
    0xd3, 0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0, 0x0,
    0x2, 0xc0, 0x0, 0x0, 0x2f, 0xdd, 0xdd, 0x10,

    /* U+46 "F" */
    0x2f, 0xdd, 0xdd, 0x12, 0xc0, 0x0, 0x0, 0x2c,
    0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x2f, 0xdd,
    0xd3, 0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0, 0x0,
    0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0, 0x0, 0x0,

    /* U+47 "G" */
    0x0, 0x5d, 0xd3, 0x0, 0x38, 0x0, 0xa1, 0x2c,
    0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0,
    0x0, 0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0, 0x8e,
    0x10, 0x48, 0x0, 0xc2, 0x0, 0x5d, 0xdf, 0x20,

    /* U+48 "H" */
    0x2c, 0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2f, 0xdd,
    0xdf, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0, 0xc,
    0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0, 0xc, 0x20,

    /* U+49 "I" */
    0x3d, 0xee, 0xd1, 0x0, 0x86, 0x0, 0x0, 0x86,
    0x0, 0x0, 0x86, 0x0, 0x0, 0x86, 0x0, 0x0,
    0x86, 0x0, 0x0, 0x86, 0x0, 0x0, 0x86, 0x0,
    0x3d, 0xee, 0xd1,

    /* U+4A "J" */
    0x0, 0x5d, 0xdf, 0x20, 0x0, 0x0, 0xc2, 0x0,
    0x0, 0xc, 0x20, 0x0, 0x0, 0xc2, 0x0, 0x0,
    0xc, 0x20, 0x0, 0x0, 0xc2, 0x0, 0x0, 0xc,
    0x21, 0xa0, 0x8, 0x40, 0x3, 0xdd, 0x50, 0x0,

    /* U+4B "K" */
    0x2c, 0x0, 0xa, 0x12, 0xc0, 0x8, 0x30, 0x2c,
    0x6, 0x50, 0x2, 0xc5, 0x60, 0x0, 0x2f, 0xa0,
    0x0, 0x2, 0xe7, 0x0, 0x0, 0x2c, 0x4d, 0x50,
    0x2, 0xc0, 0x8, 0x30, 0x2c, 0x0, 0xa, 0x10,

    /* U+4C "L" */
    0x2c, 0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x2c,
    0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0,
    0x0, 0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0, 0x0,
    0x2, 0xc0, 0x0, 0x0, 0x2f, 0xdd, 0xdd, 0x10,

    /* U+4D "M" */
    0x2c, 0x0, 0x0, 0xe2, 0xc0, 0x0, 0xe, 0x2e,
    0x70, 0x8, 0xf2, 0xfa, 0x0, 0xcf, 0x2c, 0x68,
    0xa4, 0xe2, 0xc1, 0x87, 0x1e, 0x2c, 0x8, 0x60,
    0xe2, 0xc0, 0x0, 0xe, 0x2c, 0x0, 0x0, 0xe0,

    /* U+4E "N" */
    0x2b, 0x0, 0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2f,
    0x80, 0xc, 0x22, 0xb6, 0x80, 0xc2, 0x2b, 0x45,
    0xc, 0x22, 0xb0, 0x85, 0xc2, 0x2b, 0x0, 0x9f,
    0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0, 0xc, 0x20,

    /* U+4F "O" */
    0x0, 0x5d, 0x50, 0x0, 0x38, 0x8, 0x30, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0,
    0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0, 0xc,
    0x20, 0x48, 0x8, 0x40, 0x0, 0x5d, 0x50, 0x0,

    /* U+50 "P" */
    0x2f, 0xdd, 0xd3, 0x2, 0xc0, 0x0, 0xc2, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0,
    0x5, 0x2, 0xfe, 0xee, 0x30, 0x2c, 0x0, 0x0,
    0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0, 0x0, 0x0,

    /* U+51 "Q" */
    0x0, 0x5d, 0x50, 0x0, 0x57, 0x6, 0x50, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xb2, 0x2c, 0x0,
    0xb, 0x22, 0xc0, 0x0, 0xb2, 0x1b, 0x6, 0x5a,
    0x10, 0x38, 0x8, 0x30, 0x0, 0x5d, 0x5a, 0x10,

    /* U+52 "R" */
    0x2f, 0xdd, 0xd3, 0x2, 0xc0, 0x0, 0xc2, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0,
    0x5, 0x2, 0xfe, 0xee, 0x30, 0x2c, 0x6, 0xb2,
    0x2, 0xc0, 0x9, 0x40, 0x2c, 0x0, 0xa, 0x10,

    /* U+53 "S" */
    0x3, 0xdd, 0xd3, 0x1, 0x70, 0x0, 0x61, 0x2c,
    0x0, 0x3, 0x2, 0xc0, 0x0, 0x0, 0x1, 0x66,
    0x67, 0x10, 0x0, 0x0, 0xc2, 0x0, 0x0, 0xc,
    0x21, 0xa0, 0x0, 0xb1, 0x3, 0xdd, 0xd3, 0x0,

    /* U+54 "T" */
    0x1d, 0xde, 0xed, 0xd0, 0x0, 0x86, 0x0, 0x0,
    0x8, 0x60, 0x0, 0x0, 0x86, 0x0, 0x0, 0x8,
    0x60, 0x0, 0x0, 0x86, 0x0, 0x0, 0x8, 0x60,
    0x0, 0x0, 0x86, 0x0, 0x0, 0x8, 0x60, 0x0,

    /* U+55 "U" */
    0x2c, 0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0,
    0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0, 0xc,
    0x21, 0x80, 0x0, 0x81, 0x3, 0xdd, 0xd3, 0x0,

    /* U+56 "V" */
    0x2c, 0x0, 0x0, 0xe2, 0xc0, 0x0, 0xe, 0x16,
    0x0, 0x0, 0x80, 0x4a, 0x0, 0xc2, 0x4, 0xa0,
    0xc, 0x20, 0x29, 0x45, 0x91, 0x0, 0x68, 0xa4,
    0x0, 0x6, 0x8a, 0x40, 0x0, 0x6, 0x50, 0x0,

    /* U+57 "W" */
    0x2c, 0x0, 0x0, 0xe2, 0xc0, 0x0, 0xe, 0x2c,
    0x0, 0x0, 0xe2, 0xc0, 0x0, 0xe, 0x2c, 0x8,
    0x60, 0xe2, 0xc5, 0x89, 0x3e, 0x1a, 0x57, 0x83,
    0xc0, 0x27, 0x0, 0x81, 0x4, 0xa0, 0xc, 0x20,

    /* U+58 "X" */
    0x2b, 0x0, 0xc, 0x21, 0x80, 0x0, 0x81, 0x2,
    0x70, 0x72, 0x0, 0x4a, 0xa, 0x40, 0x0, 0x6f,
    0x50, 0x0, 0x38, 0x49, 0x20, 0x4, 0xa0, 0xa4,
    0x1, 0x80, 0x0, 0x81, 0x2b, 0x0, 0xc, 0x20,

    /* U+59 "Y" */
    0x2c, 0x0, 0x0, 0xe1, 0x80, 0x0, 0x9, 0x4,
    0xa0, 0xc, 0x20, 0x27, 0x0, 0x81, 0x0, 0x37,
    0x82, 0x0, 0x0, 0x86, 0x0, 0x0, 0x8, 0x60,
    0x0, 0x0, 0x86, 0x0, 0x0, 0x8, 0x60, 0x0,

    /* U+5A "Z" */
    0x1d, 0xdd, 0xdf, 0x20, 0x0, 0x0, 0xc2, 0x0,
    0x0, 0xc, 0x20, 0x0, 0x8, 0x30, 0x0, 0x6,
    0x50, 0x0, 0x4d, 0x70, 0x0, 0x2c, 0x0, 0x0,
    0x2, 0xc0, 0x0, 0x0, 0x2f, 0xdd, 0xdd, 0x10,

    /* U+5B "[" */
    0x3e, 0xee, 0x34, 0xa0, 0x0, 0x4a, 0x0, 0x4,
    0xa0, 0x0, 0x4a, 0x0, 0x4, 0xa0, 0x0, 0x4a,
    0x0, 0x4, 0xa0, 0x0, 0x4a, 0x0, 0x4, 0xa0,
    0x0, 0x3e, 0xdd, 0x30,

    /* U+5C "\\" */
    0x18, 0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x2,
    0x60, 0x0, 0x0, 0x4a, 0x0, 0x0, 0x0, 0x68,
    0x0, 0x0, 0x4, 0x50, 0x0, 0x0, 0x8, 0x60,
    0x0, 0x0, 0x28, 0x30, 0x0, 0x0, 0xa4, 0x0,
    0x0, 0x0, 0x81, 0x0, 0x0, 0xc, 0x20,

    /* U+5D "]" */
    0x3e, 0xee, 0x30, 0x0, 0xa4, 0x0, 0xa, 0x40,
    0x0, 0xa4, 0x0, 0xa, 0x40, 0x0, 0xa4, 0x0,
    0xa, 0x40, 0x0, 0xa4, 0x0, 0xa, 0x40, 0x0,
    0xa4, 0x3d, 0xde, 0x30,

    /* U+5E "^" */
    0x0, 0x6, 0x50, 0x0, 0x5, 0x68, 0x30, 0x3,
    0x80, 0xa, 0x11, 0xa0, 0x0, 0xb,

    /* U+5F "_" */
    0xdd, 0xdd, 0xdd, 0xd0,

    /* U+60 "`" */
    0x45, 0x6, 0x80, 0x5, 0x40, 0x86,

    /* U+61 "a" */
    0x0, 0x5d, 0xd3, 0x0, 0x38, 0x0, 0xb1, 0x3,
    0xdd, 0xdf, 0x21, 0x90, 0x0, 0xc2, 0x1a, 0x0,
    0x8f, 0x20, 0x3d, 0xd4, 0xc2,

    /* U+62 "b" */
    0x2b, 0x0, 0x0, 0x2, 0xb0, 0x0, 0x0, 0x2b,
    0x0, 0x0, 0x2, 0xb5, 0xdd, 0x30, 0x2f, 0x80,
    0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0, 0xc,
    0x22, 0xb0, 0x0, 0xc2, 0x2f, 0xdd, 0xd5, 0x0,

    /* U+63 "c" */
    0x3, 0xdd, 0xd3, 0x1, 0xb0, 0x0, 0xa1, 0x2c,
    0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x1b, 0x0,
    0xa, 0x10, 0x3d, 0xdd, 0x30,

    /* U+64 "d" */
    0x0, 0x0, 0xc, 0x20, 0x0, 0x0, 0xc2, 0x0,
    0x0, 0xc, 0x20, 0x7d, 0xdd, 0xf2, 0x2b, 0x0,
    0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0, 0xc,
    0x21, 0xa0, 0x8, 0xf2, 0x3, 0xdd, 0x4c, 0x20,

    /* U+65 "e" */
    0x3, 0xdd, 0xd3, 0x2, 0xc0, 0x0, 0xc2, 0x2f,
    0xdd, 0xde, 0x12, 0xc0, 0x0, 0x0, 0x2c, 0x0,
    0x0, 0x0, 0x5d, 0xdd, 0x30,

    /* U+66 "f" */
    0x0, 0x6, 0xdd, 0x10, 0x4, 0x50, 0x0, 0x0,
    0x68, 0x0, 0x1, 0xde, 0xed, 0x30, 0x0, 0x68,
    0x0, 0x0, 0x6, 0x80, 0x0, 0x0, 0x68, 0x0,
    0x0, 0x6, 0x80, 0x0, 0x0, 0x68, 0x0, 0x0,

    /* U+67 "g" */
    0x3, 0xdd, 0x4c, 0x21, 0xb0, 0x8, 0xf2, 0x2b,
    0x0, 0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0,
    0xc, 0x21, 0xed, 0xdd, 0xf2, 0x0, 0x0, 0xc,
    0x21, 0x90, 0x0, 0xb1, 0x3, 0xdd, 0xd3, 0x0,

    /* U+68 "h" */
    0x2b, 0x0, 0x0, 0x2, 0xb0, 0x0, 0x0, 0x2b,
    0x0, 0x0, 0x2, 0xc6, 0xdd, 0x50, 0x2e, 0x70,
    0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0, 0xc,
    0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0, 0xc, 0x20,

    /* U+69 "i" */
    0x0, 0x75, 0x0, 0x0, 0x0, 0x0, 0x3d, 0xe6,
    0x0, 0x0, 0x86, 0x0, 0x0, 0x86, 0x0, 0x0,
    0x86, 0x0, 0x0, 0x86, 0x0, 0x3d, 0xee, 0xd1,

    /* U+6A "j" */
    0x0, 0x0, 0x83, 0x3, 0xdd, 0xf4, 0x0, 0x0,
    0xa4, 0x0, 0x0, 0xa4, 0x0, 0x0, 0xa4, 0x0,
    0x0, 0xa4, 0x0, 0x0, 0xa4, 0x0, 0x0, 0xa4,
    0x19, 0x0, 0x93, 0x3, 0xdd, 0x50,

    /* U+6B "k" */
    0x2c, 0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x2c,
    0x0, 0x0, 0x2, 0xc0, 0x0, 0xa1, 0x2c, 0x0,
    0x83, 0x2, 0xfd, 0xd5, 0x0, 0x2c, 0x6, 0x50,
    0x2, 0xc0, 0x8, 0x30, 0x2c, 0x0, 0xa, 0x10,

    /* U+6C "l" */
    0x3e, 0x80, 0x0, 0x6, 0x80, 0x0, 0x6, 0x80,
    0x0, 0x6, 0x80, 0x0, 0x6, 0x80, 0x0, 0x6,
    0x80, 0x0, 0x6, 0x80, 0x0, 0x6, 0x80, 0x0,
    0x0, 0x8d, 0xd1,

    /* U+6D "m" */
    0x2f, 0xd8, 0x9d, 0x32, 0xc0, 0x86, 0xe, 0x2c,
    0x8, 0x60, 0xe2, 0xc0, 0x86, 0xe, 0x2c, 0x8,
    0x60, 0xe2, 0xc0, 0x86, 0xe,

    /* U+6E "n" */
    0x2c, 0x6d, 0xd5, 0x2, 0xe7, 0x0, 0xc2, 0x2b,
    0x0, 0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0,
    0xc, 0x22, 0xb0, 0x0, 0xc2,

    /* U+6F "o" */
    0x3, 0xdd, 0xd3, 0x2, 0xc0, 0x0, 0xc2, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x2c, 0x0,
    0xc, 0x20, 0x5d, 0xdd, 0x50,

    /* U+70 "p" */
    0x2b, 0x5d, 0xd3, 0x2, 0xf8, 0x0, 0xc2, 0x2b,
    0x0, 0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0,
    0xc, 0x22, 0xfd, 0xdd, 0x50, 0x2b, 0x0, 0x0,
    0x2, 0xb0, 0x0, 0x0, 0x2b, 0x0, 0x0, 0x0,

    /* U+71 "q" */
    0x3, 0xdd, 0xdf, 0x20, 0x40, 0x0, 0xc2, 0x2b,
    0x0, 0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0,
    0xc, 0x21, 0xed, 0xdd, 0xf2, 0x0, 0x0, 0xc,
    0x20, 0x0, 0x0, 0xc2, 0x0, 0x0, 0xc, 0x20,

    /* U+72 "r" */
    0x2c, 0x5d, 0xd3, 0x2, 0xf8, 0x0, 0xa1, 0x2c,
    0x0, 0x0, 0x2, 0xc0, 0x0, 0x0, 0x2c, 0x0,
    0x0, 0x2, 0xc0, 0x0, 0x0,

    /* U+73 "s" */
    0x3, 0xdd, 0xd3, 0x2, 0xc0, 0x0, 0xa1, 0x4,
    0xdd, 0xd3, 0x0, 0x0, 0x0, 0x91, 0x1a, 0x0,
    0xb, 0x10, 0x3d, 0xdd, 0x30,

    /* U+74 "t" */
    0x0, 0x68, 0x0, 0x0, 0x6, 0x80, 0x0, 0x0,
    0x68, 0x0, 0x1, 0xde, 0xed, 0xd1, 0x0, 0x68,
    0x0, 0x0, 0x6, 0x80, 0x0, 0x0, 0x68, 0x0,
    0x0, 0x4, 0x50, 0x0, 0x0, 0x6, 0xdd, 0x10,

    /* U+75 "u" */
    0x2b, 0x0, 0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2b,
    0x0, 0xc, 0x22, 0xb0, 0x0, 0xc2, 0x2b, 0x0,
    0x7e, 0x20, 0x5d, 0xd6, 0xc2,

    /* U+76 "v" */
    0x2c, 0x0, 0xc, 0x22, 0xc0, 0x0, 0xc2, 0x19,
    0x50, 0x59, 0x10, 0x3a, 0x9, 0x40, 0x3, 0xa0,
    0x94, 0x0, 0x5, 0xd5, 0x0,

    /* U+77 "w" */
    0x2c, 0x0, 0x0, 0xe2, 0xc0, 0x0, 0xe, 0x2c,
    0x8, 0x60, 0xe2, 0xc3, 0x88, 0x2e, 0x6, 0x82,
    0x29, 0x50, 0x4a, 0x0, 0xc2,

    /* U+78 "x" */
    0x2c, 0x0, 0xc, 0x21, 0x80, 0x0, 0x81, 0x3,
    0xdd, 0xd3, 0x0, 0x38, 0x8, 0x30, 0x18, 0x0,
    0x8, 0x12, 0xc0, 0x0, 0xc2,

    /* U+79 "y" */
    0x2b, 0x0, 0xc, 0x21, 0x80, 0x0, 0xc2, 0x2,
    0x70, 0x17, 0x10, 0x4a, 0xa, 0x40, 0x0, 0x68,
    0xa4, 0x0, 0x2, 0x87, 0x10, 0x0, 0x8, 0x50,
    0x0, 0x0, 0x85, 0x0, 0x3, 0xd6, 0x0, 0x0,

    /* U+7A "z" */
    0x1d, 0xdd, 0xdf, 0x20, 0x0, 0x0, 0x81, 0x0,
    0x6, 0xd3, 0x0, 0x5, 0x60, 0x0, 0x2, 0x70,
    0x0, 0x1, 0xef, 0xdd, 0xd1,

    /* U+7B "{" */
    0x0, 0x6, 0xd3, 0x0, 0x68, 0x0, 0x0, 0x68,
    0x0, 0x0, 0x68, 0x0, 0x0, 0x68, 0x0, 0x1d,
    0xa2, 0x0, 0x0, 0x68, 0x0, 0x0, 0x68, 0x0,
    0x0, 0x68, 0x0, 0x0, 0x68, 0x0, 0x0, 0x7,
    0xe3,

    /* U+7C "|" */
    0x68, 0x68, 0x68, 0x68, 0x68, 0x68, 0x68, 0x68,
    0x68, 0x68, 0x68, 0x68,

    /* U+7D "}" */
    0x3d, 0x60, 0x0, 0x0, 0x86, 0x0, 0x0, 0x86,
    0x0, 0x0, 0x86, 0x0, 0x0, 0x86, 0x0, 0x0,
    0x2a, 0xd1, 0x0, 0x86, 0x0, 0x0, 0x86, 0x0,
    0x0, 0x86, 0x0, 0x0, 0x86, 0x0, 0x3e, 0x70,
    0x0,

    /* U+7E "~" */
    0x3, 0xd6, 0x9, 0x11, 0xa0, 0x7e, 0x30,

    /* U+A0 " " */

    /* U+A1 "¡" */
    0x86, 0x54, 0x0, 0x86, 0x86, 0x86, 0x86, 0x86,
    0x86,

    /* U+A2 "¢" */
    0x0, 0x3, 0x20, 0x0, 0x0, 0x76, 0x0, 0x3,
    0xee, 0xe3, 0x1, 0xb0, 0x76, 0xa1, 0x1c, 0x7,
    0x60, 0x1, 0xc0, 0x76, 0x0, 0x1b, 0x7, 0x6a,
    0x10, 0x3e, 0xee, 0x30, 0x0, 0x7, 0x60, 0x0,
    0x0, 0x76, 0x0,

    /* U+A3 "£" */
    0x0, 0x6, 0xdd, 0x10, 0x0, 0x56, 0x0, 0xb0,
    0x4, 0xa0, 0x0, 0x0, 0x4, 0xa0, 0x0, 0x0,
    0x1e, 0xfd, 0xdd, 0x10, 0x4, 0xa0, 0x0, 0x0,
    0x4, 0xa0, 0x0, 0x0, 0x4, 0xa0, 0x0, 0x0,
    0x1e, 0xfd, 0xdd, 0xd0,

    /* U+A4 "¤" */
    0x1a, 0x0, 0xa, 0x10, 0x3e, 0xde, 0x30, 0x3,
    0x70, 0x73, 0x2, 0xc0, 0x0, 0xc2, 0x18, 0x0,
    0x8, 0x10, 0x37, 0x7, 0x30, 0x1d, 0xed, 0xed,
    0x10,

    /* U+A5 "¥" */
    0x2c, 0x0, 0x0, 0xe1, 0x80, 0x0, 0x9, 0x4,
    0xa0, 0xc, 0x20, 0x27, 0x0, 0x81, 0x0, 0x37,
    0x82, 0x0, 0x3d, 0xee, 0xd1, 0x0, 0x8, 0x60,
    0x0, 0x3d, 0xee, 0xd1, 0x0, 0x8, 0x60, 0x0,

    /* U+A7 "§" */
    0x3, 0xdd, 0xd3, 0x1, 0x80, 0x0, 0x81, 0x2c,
    0x0, 0x2, 0x0, 0x3d, 0x60, 0x0, 0x2c, 0x6,
    0xdd, 0x10, 0x4d, 0x60, 0xb2, 0x0, 0x6, 0xd3,
    0x0, 0x0, 0x0, 0x81, 0x1a, 0x0, 0xc, 0x20,
    0x3d, 0xdd, 0x30,

    /* U+A9 "©" */
    0x0, 0x5e, 0xe3, 0x0, 0x38, 0x0, 0xa1, 0x1b,
    0x5e, 0xd3, 0xc2, 0xc6, 0x80, 0xe, 0x1b, 0x5e,
    0xd3, 0xc0, 0x38, 0x0, 0xa1, 0x0, 0x5d, 0xd3,
    0x0,

    /* U+AD "­" */
    0x1d, 0xdd, 0xd3,

    /* U+AE "®" */
    0x0, 0x5e, 0xe3, 0x0, 0x38, 0x0, 0xa1, 0x1b,
    0x5e, 0xe3, 0xc2, 0xc6, 0xec, 0x3e, 0x1b, 0x57,
    0x83, 0xc0, 0x38, 0x0, 0xa1, 0x0, 0x5d, 0xd3,
    0x0,

    /* U+B0 "°" */
    0x5, 0xd5, 0x2, 0x70, 0x72, 0x4a, 0xa, 0x40,
    0x4d, 0x40,

    /* U+B4 "´" */
    0x5, 0x40, 0x86, 0x45, 0x6, 0x80,

    /* U+B5 "µ" */
    0x2c, 0x0, 0xc, 0x20, 0x2c, 0x0, 0xc, 0x20,
    0x2c, 0x0, 0xc, 0x20, 0x2c, 0x0, 0xc, 0x20,
    0x2c, 0x0, 0xc, 0x20, 0x2f, 0xdd, 0xde, 0xd0,
    0x2c, 0x0, 0x0, 0x0, 0x2c, 0x0, 0x0, 0x0,
    0x2c, 0x0, 0x0, 0x0,

    /* U+DF "ß" */
    0x3, 0xdd, 0xd3, 0x1, 0x80, 0x0, 0x81, 0x2c,
    0x0, 0xc, 0x22, 0xc0, 0x8, 0x30, 0x2c, 0x8,
    0x60, 0x2, 0xc0, 0x8, 0x30, 0x2c, 0x0, 0x8,
    0x12, 0xc0, 0x0, 0xc2, 0x2c, 0x5d, 0xd3, 0x0,

    /* U+E1 "á" */
    0x0, 0x0, 0x83, 0x0, 0x0, 0x75, 0x0, 0x0,
    0x46, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x5d,
    0xd3, 0x0, 0x38, 0x0, 0xb1, 0x3, 0xdd, 0xdf,
    0x21, 0x90, 0x0, 0xc2, 0x1a, 0x0, 0x8f, 0x20,
    0x3d, 0xd4, 0xc2
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 112, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 0, .adv_w = 112, .box_w = 2, .box_h = 9, .ofs_x = 3, .ofs_y = 0},
    {.bitmap_index = 9, .adv_w = 112, .box_w = 7, .box_h = 4, .ofs_x = 0, .ofs_y = 6},
    {.bitmap_index = 23, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 55, .adv_w = 112, .box_w = 7, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 94, .adv_w = 112, .box_w = 8, .box_h = 10, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 134, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 166, .adv_w = 112, .box_w = 3, .box_h = 4, .ofs_x = 2, .ofs_y = 6},
    {.bitmap_index = 172, .adv_w = 112, .box_w = 4, .box_h = 11, .ofs_x = 2, .ofs_y = -1},
    {.bitmap_index = 194, .adv_w = 112, .box_w = 4, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 216, .adv_w = 112, .box_w = 6, .box_h = 5, .ofs_x = 1, .ofs_y = 3},
    {.bitmap_index = 231, .adv_w = 112, .box_w = 7, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 256, .adv_w = 112, .box_w = 4, .box_h = 4, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 264, .adv_w = 112, .box_w = 7, .box_h = 1, .ofs_x = 0, .ofs_y = 3},
    {.bitmap_index = 268, .adv_w = 112, .box_w = 3, .box_h = 2, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 271, .adv_w = 112, .box_w = 7, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 310, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 342, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 369, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 401, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 433, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 465, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 497, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 529, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 561, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 593, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 625, .adv_w = 112, .box_w = 3, .box_h = 6, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 634, .adv_w = 112, .box_w = 4, .box_h = 8, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 650, .adv_w = 112, .box_w = 5, .box_h = 6, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 665, .adv_w = 112, .box_w = 7, .box_h = 3, .ofs_x = 0, .ofs_y = 3},
    {.bitmap_index = 676, .adv_w = 112, .box_w = 5, .box_h = 6, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 691, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 723, .adv_w = 112, .box_w = 7, .box_h = 10, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 758, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 790, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 822, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 854, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 886, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 918, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 950, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 982, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1014, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1041, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1073, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1105, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1137, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1169, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1201, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1233, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1265, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1297, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1329, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1361, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1393, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1425, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1457, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1489, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1521, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1553, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1585, .adv_w = 112, .box_w = 5, .box_h = 11, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1613, .adv_w = 112, .box_w = 7, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 1652, .adv_w = 112, .box_w = 5, .box_h = 11, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1680, .adv_w = 112, .box_w = 7, .box_h = 4, .ofs_x = 0, .ofs_y = 6},
    {.bitmap_index = 1694, .adv_w = 112, .box_w = 7, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1698, .adv_w = 112, .box_w = 3, .box_h = 4, .ofs_x = 2, .ofs_y = 7},
    {.bitmap_index = 1704, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1725, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1757, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1778, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1810, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1831, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1863, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 1895, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1927, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1951, .adv_w = 112, .box_w = 6, .box_h = 10, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 1981, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2013, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 2040, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2061, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2082, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2103, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 2135, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 2167, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2188, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2209, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2241, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2262, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2283, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2304, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2325, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 2357, .adv_w = 112, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2378, .adv_w = 112, .box_w = 6, .box_h = 11, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 2411, .adv_w = 112, .box_w = 2, .box_h = 12, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 2423, .adv_w = 112, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 2456, .adv_w = 112, .box_w = 7, .box_h = 2, .ofs_x = 0, .ofs_y = 3},
    {.bitmap_index = 2463, .adv_w = 112, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2463, .adv_w = 112, .box_w = 2, .box_h = 9, .ofs_x = 3, .ofs_y = -3},
    {.bitmap_index = 2472, .adv_w = 112, .box_w = 7, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2507, .adv_w = 112, .box_w = 8, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2543, .adv_w = 112, .box_w = 7, .box_h = 7, .ofs_x = 0, .ofs_y = 2},
    {.bitmap_index = 2568, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2600, .adv_w = 112, .box_w = 7, .box_h = 10, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 2635, .adv_w = 112, .box_w = 7, .box_h = 7, .ofs_x = 0, .ofs_y = 1},
    {.bitmap_index = 2660, .adv_w = 112, .box_w = 6, .box_h = 1, .ofs_x = 0, .ofs_y = 3},
    {.bitmap_index = 2663, .adv_w = 112, .box_w = 7, .box_h = 7, .ofs_x = 0, .ofs_y = 1},
    {.bitmap_index = 2688, .adv_w = 112, .box_w = 5, .box_h = 4, .ofs_x = 1, .ofs_y = 6},
    {.bitmap_index = 2698, .adv_w = 112, .box_w = 3, .box_h = 4, .ofs_x = 2, .ofs_y = 7},
    {.bitmap_index = 2704, .adv_w = 112, .box_w = 8, .box_h = 9, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 2740, .adv_w = 112, .box_w = 7, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2772, .adv_w = 112, .box_w = 7, .box_h = 10, .ofs_x = 0, .ofs_y = 0}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_1[] = {
    0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x7, 0x9,
    0xd, 0xe, 0x10, 0x14, 0x15, 0x3f, 0x41
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 95, .glyph_id_start = 1,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    },
    {
        .range_start = 160, .range_length = 66, .glyph_id_start = 96,
        .unicode_list = unicode_list_1, .glyph_id_ofs_list = NULL, .list_length = 15, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

/*Store all the custom data of the font*/
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = gylph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = NULL,
    .kern_scale = 0,
    .cmap_num = 2,
    .bpp = 4,
    .kern_classes = 0,
    .bitmap_format = 0
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
lv_font_t FreePixel14 = {
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 14,          /*The maximum line height required by the font*/
    .base_line = 3,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};

#endif /*#if FREEPIXEL14*/


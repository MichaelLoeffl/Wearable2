#include "lvgl/lvgl.h"

/*******************************************************************************
 * Size: 15 px
 * Bpp: 1
 * Opts: 
 ******************************************************************************/

#ifndef FREEPIXEL15
#define FREEPIXEL15 1
#endif

#if FREEPIXEL15

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t gylph_bitmap[] = {
    /* U+20 " " */
    0x0,

    /* U+21 "!" */
    0xfe, 0xc0,

    /* U+22 "\"" */
    0xcf, 0x3c, 0xf3,

    /* U+23 "#" */
    0x49, 0x2f, 0xd2, 0x49, 0x24, 0xbf, 0x49, 0x20,

    /* U+24 "$" */
    0x10, 0xf2, 0x5c, 0x89, 0xf, 0x5, 0x89, 0x13,
    0x25, 0xf0, 0x80,

    /* U+25 "%" */
    0x61, 0x22, 0x4b, 0x20, 0x82, 0x8, 0x26, 0x92,
    0x24, 0x30,

    /* U+26 "&" */
    0x31, 0x24, 0x8c, 0x62, 0x59, 0x62, 0x89, 0xd0,

    /* U+27 "'" */
    0xff,

    /* U+28 "(" */
    0x29, 0x49, 0x24, 0x91, 0x22,

    /* U+29 ")" */
    0x89, 0x12, 0x49, 0x25, 0x28,

    /* U+2A "*" */
    0x25, 0x5d, 0x52, 0x0,

    /* U+2B "+" */
    0x10, 0x20, 0x47, 0xf1, 0x2, 0x4, 0x0,

    /* U+2C "," */
    0x6d, 0xe0,

    /* U+2D "-" */
    0xfc,

    /* U+2E "." */
    0xf0,

    /* U+2F "/" */
    0x4, 0x10, 0x82, 0x10, 0x42, 0x8, 0x41, 0x8,
    0x20,

    /* U+30 "0" */
    0x7a, 0x18, 0x63, 0x96, 0x9c, 0x61, 0x85, 0xe0,

    /* U+31 "1" */
    0x23, 0x28, 0x42, 0x10, 0x84, 0x27, 0xc0,

    /* U+32 "2" */
    0x7a, 0x18, 0x41, 0x8, 0x42, 0x10, 0x83, 0xf0,

    /* U+33 "3" */
    0x7a, 0x10, 0x42, 0x30, 0x20, 0x41, 0x89, 0xc0,

    /* U+34 "4" */
    0x18, 0xa2, 0x92, 0x4a, 0x2f, 0xc2, 0x8, 0x20,

    /* U+35 "5" */
    0xfe, 0x8, 0x20, 0xf8, 0x10, 0x41, 0x85, 0xe0,

    /* U+36 "6" */
    0x39, 0x8, 0x20, 0xbb, 0x18, 0x61, 0x85, 0xe0,

    /* U+37 "7" */
    0xfe, 0x10, 0x42, 0x8, 0x41, 0x8, 0x20, 0x80,

    /* U+38 "8" */
    0x7a, 0x18, 0x52, 0x31, 0x28, 0x61, 0x85, 0xe0,

    /* U+39 "9" */
    0x7a, 0x18, 0x61, 0x8d, 0xd0, 0x41, 0x9, 0xc0,

    /* U+3A ":" */
    0xf0, 0x3c,

    /* U+3B ";" */
    0x6c, 0x0, 0xdb, 0xc0,

    /* U+3C "<" */
    0x12, 0x48, 0x42, 0x10,

    /* U+3D "=" */
    0xfc, 0xf, 0xc0,

    /* U+3E ">" */
    0x84, 0x21, 0x24, 0x80,

    /* U+3F "?" */
    0x7a, 0x18, 0x41, 0x8, 0x42, 0x8, 0x0, 0x80,

    /* U+40 "@" */
    0x38, 0x89, 0xc, 0xda, 0xb5, 0x6a, 0xca, 0x40,
    0x88, 0xe0,

    /* U+41 "A" */
    0x7a, 0x18, 0x61, 0x87, 0xf8, 0x61, 0x86, 0x10,

    /* U+42 "B" */
    0xfa, 0x18, 0x61, 0xfa, 0x18, 0x61, 0x87, 0xe0,

    /* U+43 "C" */
    0x39, 0x18, 0x20, 0x82, 0x8, 0x20, 0x44, 0xe0,

    /* U+44 "D" */
    0xf2, 0x28, 0x61, 0x86, 0x18, 0x61, 0x8b, 0xc0,

    /* U+45 "E" */
    0xfe, 0x8, 0x20, 0xfa, 0x8, 0x20, 0x83, 0xf0,

    /* U+46 "F" */
    0xfe, 0x8, 0x20, 0xfa, 0x8, 0x20, 0x82, 0x0,

    /* U+47 "G" */
    0x39, 0x18, 0x20, 0x82, 0x8, 0xe1, 0x44, 0xf0,

    /* U+48 "H" */
    0x86, 0x18, 0x61, 0xfe, 0x18, 0x61, 0x86, 0x10,

    /* U+49 "I" */
    0xf9, 0x8, 0x42, 0x10, 0x84, 0x27, 0xc0,

    /* U+4A "J" */
    0x3c, 0x10, 0x41, 0x4, 0x10, 0x41, 0x89, 0xc0,

    /* U+4B "K" */
    0x86, 0x29, 0x28, 0xc3, 0xa, 0x24, 0x8a, 0x10,

    /* U+4C "L" */
    0x82, 0x8, 0x20, 0x82, 0x8, 0x20, 0x83, 0xf0,

    /* U+4D "M" */
    0x83, 0x7, 0x1e, 0x3a, 0xb5, 0x64, 0xc9, 0x83,
    0x4,

    /* U+4E "N" */
    0x86, 0x1c, 0x69, 0xa6, 0x59, 0x63, 0x86, 0x10,

    /* U+4F "O" */
    0x31, 0x28, 0x61, 0x86, 0x18, 0x61, 0x48, 0xc0,

    /* U+50 "P" */
    0xfa, 0x18, 0x61, 0x87, 0xe8, 0x20, 0x82, 0x0,

    /* U+51 "Q" */
    0x31, 0x28, 0x61, 0x86, 0x18, 0x65, 0x48, 0xd0,

    /* U+52 "R" */
    0xfa, 0x18, 0x61, 0x87, 0xe9, 0x22, 0x8a, 0x10,

    /* U+53 "S" */
    0x7a, 0x18, 0x20, 0x78, 0x10, 0x41, 0x85, 0xe0,

    /* U+54 "T" */
    0xfe, 0x20, 0x40, 0x81, 0x2, 0x4, 0x8, 0x10,
    0x20,

    /* U+55 "U" */
    0x86, 0x18, 0x61, 0x86, 0x18, 0x61, 0x85, 0xe0,

    /* U+56 "V" */
    0x83, 0x6, 0xa, 0x24, 0x48, 0x8a, 0x14, 0x28,
    0x20,

    /* U+57 "W" */
    0x83, 0x6, 0xc, 0x19, 0x32, 0x6a, 0xd5, 0x44,
    0x88,

    /* U+58 "X" */
    0x86, 0x14, 0x92, 0x30, 0xc4, 0x92, 0x86, 0x10,

    /* U+59 "Y" */
    0x83, 0x5, 0x12, 0x22, 0x82, 0x4, 0x8, 0x10,
    0x20,

    /* U+5A "Z" */
    0xfc, 0x10, 0x42, 0x10, 0x84, 0x20, 0x83, 0xf0,

    /* U+5B "[" */
    0xf8, 0x88, 0x88, 0x88, 0x88, 0x88, 0xf0,

    /* U+5C "\\" */
    0x82, 0x4, 0x10, 0x20, 0x81, 0x4, 0x8, 0x20,
    0x41,

    /* U+5D "]" */
    0xf1, 0x11, 0x11, 0x11, 0x11, 0x11, 0xf0,

    /* U+5E "^" */
    0x10, 0x51, 0x14, 0x10,

    /* U+5F "_" */
    0xfe,

    /* U+60 "`" */
    0xa5,

    /* U+61 "a" */
    0x39, 0x10, 0x5f, 0x86, 0x37, 0x40,

    /* U+62 "b" */
    0x82, 0x8, 0x2e, 0xc6, 0x18, 0x61, 0x87, 0xe0,

    /* U+63 "c" */
    0x7a, 0x18, 0x20, 0x82, 0x17, 0x80,

    /* U+64 "d" */
    0x4, 0x10, 0x5f, 0x86, 0x18, 0x61, 0x8d, 0xd0,

    /* U+65 "e" */
    0x7a, 0x18, 0x7f, 0x82, 0x7, 0x80,

    /* U+66 "f" */
    0x1c, 0x82, 0x3e, 0x20, 0x82, 0x8, 0x20, 0x80,

    /* U+67 "g" */
    0x76, 0x38, 0x61, 0x86, 0x37, 0x41, 0x85, 0xe0,

    /* U+68 "h" */
    0x82, 0x8, 0x2e, 0xc6, 0x18, 0x61, 0x86, 0x10,

    /* U+69 "i" */
    0x20, 0xe2, 0x22, 0x22, 0xf0,

    /* U+6A "j" */
    0x8, 0x1e, 0x10, 0x84, 0x21, 0x8, 0x62, 0xe0,

    /* U+6B "k" */
    0x82, 0x8, 0x21, 0x8a, 0x4e, 0x24, 0x8a, 0x10,

    /* U+6C "l" */
    0xc2, 0x10, 0x84, 0x21, 0x8, 0x41, 0xc0,

    /* U+6D "m" */
    0xed, 0x26, 0x4c, 0x99, 0x32, 0x64, 0x80,

    /* U+6E "n" */
    0xbb, 0x18, 0x61, 0x86, 0x18, 0x40,

    /* U+6F "o" */
    0x7a, 0x18, 0x61, 0x86, 0x17, 0x80,

    /* U+70 "p" */
    0xbb, 0x18, 0x61, 0x86, 0x1f, 0xa0, 0x82, 0x0,

    /* U+71 "q" */
    0x7e, 0x18, 0x61, 0x86, 0x37, 0x41, 0x4, 0x10,

    /* U+72 "r" */
    0xbb, 0x18, 0x20, 0x82, 0x8, 0x0,

    /* U+73 "s" */
    0x7a, 0x18, 0x1e, 0x6, 0x17, 0x80,

    /* U+74 "t" */
    0x20, 0x82, 0x3f, 0x20, 0x82, 0x8, 0x20, 0x70,

    /* U+75 "u" */
    0x86, 0x18, 0x61, 0x86, 0x37, 0x40,

    /* U+76 "v" */
    0x86, 0x18, 0x52, 0x49, 0x23, 0x0,

    /* U+77 "w" */
    0x83, 0x6, 0x4c, 0x9a, 0xa8, 0x91, 0x0,

    /* U+78 "x" */
    0x86, 0x14, 0x8c, 0x4a, 0x18, 0x40,

    /* U+79 "y" */
    0x86, 0x14, 0x52, 0x28, 0xa1, 0x4, 0x11, 0x80,

    /* U+7A "z" */
    0xfc, 0x10, 0x84, 0x21, 0xf, 0xc0,

    /* U+7B "{" */
    0x19, 0x8, 0x42, 0x13, 0x4, 0x21, 0x8, 0x41,
    0x80,

    /* U+7C "|" */
    0xff, 0xfc,

    /* U+7D "}" */
    0xc1, 0x8, 0x42, 0x10, 0x64, 0x21, 0x8, 0x4c,
    0x0,

    /* U+7E "~" */
    0x66, 0x60,

    /* U+A0 " " */
    0x0,

    /* U+A1 "¡" */
    0xdf, 0xc0,

    /* U+A2 "¢" */
    0x10, 0x47, 0xa5, 0x92, 0x49, 0x25, 0x78, 0x41,
    0x0,

    /* U+A3 "£" */
    0x1c, 0x45, 0x2, 0xf, 0xc8, 0x10, 0x20, 0x41,
    0xfc,

    /* U+A4 "¤" */
    0x85, 0xe4, 0xa1, 0x85, 0x27, 0xa1,

    /* U+A5 "¥" */
    0x83, 0x5, 0x12, 0x22, 0x82, 0x1f, 0x8, 0x7c,
    0x20,

    /* U+A7 "§" */
    0x7a, 0x18, 0x18, 0x9a, 0x16, 0x46, 0x6, 0x17,
    0x80,

    /* U+A9 "©" */
    0x38, 0x8a, 0xed, 0x1a, 0x37, 0x51, 0x1c,

    /* U+AD "­" */
    0xf8,

    /* U+AE "®" */
    0x38, 0x8a, 0xed, 0x5b, 0x35, 0x51, 0x1c,

    /* U+B0 "°" */
    0x69, 0x96,

    /* U+B4 "´" */
    0x5a,

    /* U+B5 "µ" */
    0x85, 0xa, 0x14, 0x28, 0x59, 0xac, 0xc0, 0x81,
    0x0,

    /* U+DF "ß" */
    0x7a, 0x18, 0x62, 0x92, 0x48, 0xa1, 0x86, 0xe0,

    /* U+E1 "á" */
    0x8, 0x42, 0x0, 0x39, 0x10, 0x5f, 0x86, 0x37,
    0x40
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 120, .box_w = 1, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1, .adv_w = 120, .box_w = 1, .box_h = 10, .ofs_x = 4, .ofs_y = 0},
    {.bitmap_index = 3, .adv_w = 120, .box_w = 6, .box_h = 4, .ofs_x = 1, .ofs_y = 7},
    {.bitmap_index = 6, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 14, .adv_w = 120, .box_w = 7, .box_h = 12, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 25, .adv_w = 120, .box_w = 7, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 35, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 43, .adv_w = 120, .box_w = 2, .box_h = 4, .ofs_x = 3, .ofs_y = 7},
    {.bitmap_index = 44, .adv_w = 120, .box_w = 3, .box_h = 13, .ofs_x = 3, .ofs_y = -2},
    {.bitmap_index = 49, .adv_w = 120, .box_w = 3, .box_h = 13, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 54, .adv_w = 120, .box_w = 5, .box_h = 5, .ofs_x = 2, .ofs_y = 4},
    {.bitmap_index = 58, .adv_w = 120, .box_w = 7, .box_h = 7, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 65, .adv_w = 120, .box_w = 3, .box_h = 4, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 67, .adv_w = 120, .box_w = 6, .box_h = 1, .ofs_x = 1, .ofs_y = 4},
    {.bitmap_index = 68, .adv_w = 120, .box_w = 2, .box_h = 2, .ofs_x = 3, .ofs_y = 0},
    {.bitmap_index = 69, .adv_w = 120, .box_w = 6, .box_h = 12, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 78, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 86, .adv_w = 120, .box_w = 5, .box_h = 10, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 93, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 101, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 109, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 117, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 125, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 133, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 141, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 149, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 157, .adv_w = 120, .box_w = 2, .box_h = 7, .ofs_x = 3, .ofs_y = 0},
    {.bitmap_index = 159, .adv_w = 120, .box_w = 3, .box_h = 9, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 163, .adv_w = 120, .box_w = 4, .box_h = 7, .ofs_x = 2, .ofs_y = 1},
    {.bitmap_index = 167, .adv_w = 120, .box_w = 6, .box_h = 3, .ofs_x = 1, .ofs_y = 3},
    {.bitmap_index = 170, .adv_w = 120, .box_w = 4, .box_h = 7, .ofs_x = 2, .ofs_y = 1},
    {.bitmap_index = 174, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 182, .adv_w = 120, .box_w = 7, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 192, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 200, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 208, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 216, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 224, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 232, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 240, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 248, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 256, .adv_w = 120, .box_w = 5, .box_h = 10, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 263, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 271, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 279, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 287, .adv_w = 120, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 296, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 304, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 312, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 320, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 328, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 336, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 344, .adv_w = 120, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 353, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 361, .adv_w = 120, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 370, .adv_w = 120, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 379, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 387, .adv_w = 120, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 396, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 404, .adv_w = 120, .box_w = 4, .box_h = 13, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 411, .adv_w = 120, .box_w = 6, .box_h = 12, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 420, .adv_w = 120, .box_w = 4, .box_h = 13, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 427, .adv_w = 120, .box_w = 7, .box_h = 4, .ofs_x = 1, .ofs_y = 7},
    {.bitmap_index = 431, .adv_w = 120, .box_w = 7, .box_h = 1, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 432, .adv_w = 120, .box_w = 2, .box_h = 4, .ofs_x = 3, .ofs_y = 8},
    {.bitmap_index = 433, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 439, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 447, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 453, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 461, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 467, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 475, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 483, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 491, .adv_w = 120, .box_w = 4, .box_h = 9, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 496, .adv_w = 120, .box_w = 5, .box_h = 12, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 504, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 512, .adv_w = 120, .box_w = 5, .box_h = 10, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 519, .adv_w = 120, .box_w = 7, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 526, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 532, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 538, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 546, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 554, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 560, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 566, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 574, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 580, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 586, .adv_w = 120, .box_w = 7, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 593, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 599, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 607, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 613, .adv_w = 120, .box_w = 5, .box_h = 13, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 622, .adv_w = 120, .box_w = 1, .box_h = 14, .ofs_x = 3, .ofs_y = -2},
    {.bitmap_index = 624, .adv_w = 120, .box_w = 5, .box_h = 13, .ofs_x = 2, .ofs_y = -2},
    {.bitmap_index = 633, .adv_w = 120, .box_w = 6, .box_h = 2, .ofs_x = 1, .ofs_y = 4},
    {.bitmap_index = 635, .adv_w = 120, .box_w = 1, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 636, .adv_w = 120, .box_w = 1, .box_h = 10, .ofs_x = 4, .ofs_y = -3},
    {.bitmap_index = 638, .adv_w = 120, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 647, .adv_w = 120, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 656, .adv_w = 120, .box_w = 6, .box_h = 8, .ofs_x = 1, .ofs_y = 2},
    {.bitmap_index = 662, .adv_w = 120, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 671, .adv_w = 120, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 680, .adv_w = 120, .box_w = 7, .box_h = 8, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 687, .adv_w = 120, .box_w = 5, .box_h = 1, .ofs_x = 1, .ofs_y = 4},
    {.bitmap_index = 688, .adv_w = 120, .box_w = 7, .box_h = 8, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 695, .adv_w = 120, .box_w = 4, .box_h = 4, .ofs_x = 2, .ofs_y = 7},
    {.bitmap_index = 697, .adv_w = 120, .box_w = 2, .box_h = 4, .ofs_x = 3, .ofs_y = 8},
    {.bitmap_index = 698, .adv_w = 120, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 707, .adv_w = 120, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 715, .adv_w = 120, .box_w = 6, .box_h = 11, .ofs_x = 1, .ofs_y = 0}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_1[] = {
    0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x7, 0x9,
    0xd, 0xe, 0x10, 0x14, 0x15, 0x3f, 0x41
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 95, .glyph_id_start = 1,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    },
    {
        .range_start = 160, .range_length = 66, .glyph_id_start = 96,
        .unicode_list = unicode_list_1, .glyph_id_ofs_list = NULL, .list_length = 15, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

/*Store all the custom data of the font*/
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = gylph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = NULL,
    .kern_scale = 0,
    .cmap_num = 2,
    .bpp = 1,
    .kern_classes = 0,
    .bitmap_format = 0
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
lv_font_t FreePixel15 = {
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 15,          /*The maximum line height required by the font*/
    .base_line = 3,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};

#endif /*#if FREEPIXEL15*/


#include "Drivers/lv_conf.h"
#include "lvgl/src/lv_draw/lv_draw_img.h"

#ifndef LV_ATTRIBUTE_MEM_ALIGN
#define LV_ATTRIBUTE_MEM_ALIGN
#endif

#ifndef LV_ATTRIBUTE_LARGE_CONST
#define LV_ATTRIBUTE_LARGE_CONST
#endif


const LV_ATTRIBUTE_MEM_ALIGN LV_ATTRIBUTE_LARGE_CONST uint8_t PhoneAppIcon_map[] = {
  0x04, 0x02, 0x04, 0xff, 	/*Color of index 0*/
  0x2c, 0x82, 0x24, 0xff, 	/*Color of index 1*/
  0x94, 0xc2, 0x94, 0xff, 	/*Color of index 2*/
  0x4c, 0xd2, 0x3c, 0xff, 	/*Color of index 3*/
  0xcc, 0xe2, 0xcc, 0xff, 	/*Color of index 4*/
  0x5c, 0x9e, 0x54, 0xff, 	/*Color of index 5*/
  0xec, 0xf2, 0xec, 0xff, 	/*Color of index 6*/
  0xa4, 0xea, 0x94, 0xff, 	/*Color of index 7*/
  0x8c, 0xb6, 0x84, 0xff, 	/*Color of index 8*/
  0x54, 0x9a, 0x4c, 0xff, 	/*Color of index 9*/
  0x44, 0x8e, 0x3c, 0xff, 	/*Color of index 10*/
  0xac, 0xce, 0xac, 0xff, 	/*Color of index 11*/
  0xdc, 0xf6, 0xd4, 0xff, 	/*Color of index 12*/
  0x5c, 0xda, 0x4c, 0xff, 	/*Color of index 13*/
  0x74, 0xaa, 0x6c, 0xff, 	/*Color of index 14*/
  0xf4, 0xfa, 0xf4, 0xff, 	/*Color of index 15*/
  0xdc, 0xea, 0xdc, 0xff, 	/*Color of index 16*/
  0x3c, 0x8a, 0x2c, 0xff, 	/*Color of index 17*/
  0x54, 0xda, 0x3c, 0xff, 	/*Color of index 18*/
  0xc4, 0xde, 0xc4, 0xff, 	/*Color of index 19*/
  0xa4, 0xca, 0xa4, 0xff, 	/*Color of index 20*/
  0x7c, 0xe2, 0x6c, 0xff, 	/*Color of index 21*/
  0x54, 0x96, 0x4c, 0xff, 	/*Color of index 22*/
  0xfc, 0xfa, 0xfc, 0xff, 	/*Color of index 23*/
  0x24, 0x56, 0x1c, 0xff, 	/*Color of index 24*/
  0xa4, 0xca, 0x9c, 0xff, 	/*Color of index 25*/
  0x4c, 0x96, 0x44, 0xff, 	/*Color of index 26*/
  0xbc, 0xd6, 0xb4, 0xff, 	/*Color of index 27*/
  0x7c, 0xb2, 0x74, 0xff, 	/*Color of index 28*/
  0xbc, 0xf2, 0xb4, 0xff, 	/*Color of index 29*/
  0x34, 0x86, 0x2c, 0xff, 	/*Color of index 30*/
  0x9c, 0xc6, 0x9c, 0xff, 	/*Color of index 31*/
  0x4c, 0xd6, 0x3c, 0xff, 	/*Color of index 32*/
  0xdc, 0xea, 0xd4, 0xff, 	/*Color of index 33*/
  0x74, 0xaa, 0x64, 0xff, 	/*Color of index 34*/
  0x4c, 0x92, 0x44, 0xff, 	/*Color of index 35*/
  0x7c, 0xae, 0x74, 0xff, 	/*Color of index 36*/
  0xf4, 0xfe, 0xf4, 0xff, 	/*Color of index 37*/
  0xe4, 0xee, 0xe4, 0xff, 	/*Color of index 38*/
  0xd4, 0xe6, 0xd4, 0xff, 	/*Color of index 39*/
  0x64, 0xa2, 0x5c, 0xff, 	/*Color of index 40*/
  0xf4, 0xf6, 0xf4, 0xff, 	/*Color of index 41*/
  0x94, 0xc2, 0x8c, 0xff, 	/*Color of index 42*/
  0x44, 0x92, 0x3c, 0xff, 	/*Color of index 43*/
  0xe4, 0xf2, 0xe4, 0xff, 	/*Color of index 44*/
  0x6c, 0xde, 0x54, 0xff, 	/*Color of index 45*/
  0x74, 0xae, 0x6c, 0xff, 	/*Color of index 46*/
  0xdc, 0xee, 0xdc, 0xff, 	/*Color of index 47*/
  0x3c, 0x8a, 0x34, 0xff, 	/*Color of index 48*/
  0x54, 0xda, 0x44, 0xff, 	/*Color of index 49*/
  0x8c, 0xe6, 0x7c, 0xff, 	/*Color of index 50*/
  0x2c, 0x72, 0x1c, 0xff, 	/*Color of index 51*/
  0xbc, 0xda, 0xbc, 0xff, 	/*Color of index 52*/
  0xcc, 0xf2, 0xc4, 0xff, 	/*Color of index 53*/
  0x04, 0x0a, 0x04, 0xff, 	/*Color of index 54*/
  0x34, 0x82, 0x24, 0xff, 	/*Color of index 55*/
  0x9c, 0xc2, 0x94, 0xff, 	/*Color of index 56*/
  0x54, 0xd2, 0x3c, 0xff, 	/*Color of index 57*/
  0xd4, 0xe6, 0xcc, 0xff, 	/*Color of index 58*/
  0x64, 0x9e, 0x54, 0xff, 	/*Color of index 59*/
  0xec, 0xf6, 0xec, 0xff, 	/*Color of index 60*/
  0xb4, 0xee, 0xac, 0xff, 	/*Color of index 61*/
  0x8c, 0xba, 0x84, 0xff, 	/*Color of index 62*/
  0x44, 0xae, 0x34, 0xff, 	/*Color of index 63*/
  0xb4, 0xd2, 0xac, 0xff, 	/*Color of index 64*/
  0xdc, 0xfa, 0xdc, 0xff, 	/*Color of index 65*/
  0x64, 0xda, 0x4c, 0xff, 	/*Color of index 66*/
  0xfc, 0xfa, 0xf4, 0xff, 	/*Color of index 67*/
  0xcc, 0xde, 0xc4, 0xff, 	/*Color of index 68*/
  0x84, 0xe2, 0x6c, 0xff, 	/*Color of index 69*/
  0xfc, 0xfe, 0xfc, 0xff, 	/*Color of index 70*/
  0x3c, 0x86, 0x2c, 0xff, 	/*Color of index 71*/
  0xa4, 0xc6, 0x9c, 0xff, 	/*Color of index 72*/
  0x54, 0xd6, 0x3c, 0xff, 	/*Color of index 73*/
  0xfc, 0xfe, 0xf4, 0xff, 	/*Color of index 74*/
  0x4c, 0x92, 0x3c, 0xff, 	/*Color of index 75*/
  0x7c, 0xae, 0x6c, 0xff, 	/*Color of index 76*/
  0xe4, 0xee, 0xdc, 0xff, 	/*Color of index 77*/
  0x44, 0x8e, 0x34, 0xff, 	/*Color of index 78*/
  0x5c, 0xda, 0x44, 0xff, 	/*Color of index 79*/
  0x2c, 0x7e, 0x1c, 0xff, 	/*Color of index 80*/
  0xc4, 0xda, 0xbc, 0xff, 	/*Color of index 81*/
  0xcc, 0xf6, 0xc4, 0xff, 	/*Color of index 82*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 83*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 84*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 85*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 86*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 87*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 88*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 89*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 90*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 91*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 92*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 93*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 94*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 95*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 96*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 97*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 98*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 99*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 100*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 101*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 102*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 103*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 104*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 105*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 106*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 107*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 108*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 109*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 110*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 111*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 112*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 113*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 114*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 115*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 116*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 117*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 118*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 119*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 120*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 121*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 122*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 123*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 124*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 125*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 126*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 127*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 128*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 129*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 130*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 131*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 132*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 133*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 134*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 135*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 136*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 137*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 138*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 139*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 140*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 141*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 142*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 143*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 144*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 145*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 146*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 147*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 148*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 149*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 150*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 151*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 152*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 153*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 154*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 155*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 156*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 157*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 158*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 159*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 160*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 161*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 162*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 163*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 164*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 165*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 166*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 167*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 168*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 169*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 170*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 171*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 172*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 173*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 174*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 175*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 176*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 177*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 178*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 179*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 180*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 181*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 182*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 183*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 184*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 185*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 186*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 187*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 188*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 189*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 190*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 191*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 192*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 193*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 194*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 195*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 196*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 197*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 198*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 199*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 200*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 201*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 202*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 203*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 204*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 205*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 206*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 207*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 208*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 209*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 210*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 211*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 212*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 213*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 214*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 215*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 216*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 217*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 218*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 219*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 220*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 221*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 222*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 223*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 224*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 225*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 226*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 227*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 228*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 229*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 230*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 231*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 232*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 233*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 234*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 235*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 236*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 237*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 238*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 239*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 240*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 241*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 242*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 243*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 244*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 245*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 246*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 247*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 248*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 249*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 250*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 251*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 252*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 253*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 254*/
  0xff, 0xff, 0xff, 0xff, 	/*Color of index 255*/

  0x00, 0x36, 0x18, 0x3f, 0x39, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x39, 0x3f, 0x18, 0x36, 0x00,
  0x36, 0x33, 0x03, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x03, 0x33, 0x36,
  0x18, 0x03, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x03, 0x18,
  0x3f, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x3f,
  0x39, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x39,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x4f, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x0d, 0x4f, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x2d, 0x3d, 0x0c, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x0c, 0x3d, 0x2d, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x0d, 0x35, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x35, 0x0d, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x45, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x4a, 0x15, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x1f, 0x22, 0x02, 0x34, 0x4d, 0x0f, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x21, 0x4e, 0x50, 0x01, 0x1e, 0x0a, 0x22, 0x2c, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x2a, 0x01, 0x37, 0x37, 0x37, 0x01, 0x1e, 0x44, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x06, 0x16, 0x01, 0x37, 0x37, 0x37, 0x37, 0x11, 0x04, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x0b, 0x37, 0x37, 0x37, 0x37, 0x37, 0x37, 0x4e, 0x10, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x2e, 0x50, 0x37, 0x37, 0x37, 0x37, 0x01, 0x09, 0x3c, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x1b, 0x2b, 0x01, 0x37, 0x37, 0x37, 0x01, 0x0e, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x04, 0x05, 0x37, 0x37, 0x37, 0x37, 0x19, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x51, 0x47, 0x37, 0x37, 0x4e, 0x21, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x08, 0x37, 0x37, 0x01, 0x2e, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x34, 0x30, 0x37, 0x37, 0x11, 0x13, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x10, 0x16, 0x01, 0x37, 0x01, 0x24, 0x17, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x27, 0x44, 0x46, 0x46, 0x46, 0x46, 0x46, 0x2f, 0x3b, 0x01, 0x37, 0x01, 0x23, 0x21, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x0f, 0x51, 0x4c, 0x4e, 0x30, 0x0b, 0x46, 0x46, 0x46, 0x04, 0x05, 0x01, 0x37, 0x37, 0x47, 0x40, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x0f, 0x0b, 0x28, 0x1e, 0x01, 0x37, 0x01, 0x0a, 0x44, 0x29, 0x48, 0x0a, 0x01, 0x37, 0x37, 0x1e, 0x38, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x3a, 0x11, 0x01, 0x37, 0x37, 0x37, 0x37, 0x01, 0x1a, 0x05, 0x37, 0x01, 0x37, 0x01, 0x1e, 0x2a, 0x43, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x4d, 0x0a, 0x37, 0x37, 0x37, 0x37, 0x37, 0x37, 0x01, 0x01, 0x37, 0x37, 0x01, 0x4e, 0x14, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x0f, 0x28, 0x01, 0x37, 0x37, 0x37, 0x37, 0x37, 0x37, 0x37, 0x01, 0x37, 0x28, 0x44, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x3e, 0x01, 0x37, 0x37, 0x37, 0x37, 0x37, 0x01, 0x37, 0x09, 0x19, 0x3c, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x40, 0x37, 0x01, 0x01, 0x01, 0x1e, 0x4b, 0x22, 0x0b, 0x06, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x06, 0x3e, 0x1c, 0x3e, 0x48, 0x51, 0x26, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x20, 0x32, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x32, 0x20, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x49, 0x15, 0x25, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x25, 0x15, 0x49, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x4f, 0x1d, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x1d, 0x4f, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x42, 0x07, 0x35, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x52, 0x35, 0x07, 0x42, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12,
  0x39, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x39,
  0x3f, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x3f,
  0x18, 0x03, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x03, 0x18,
  0x36, 0x33, 0x03, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x03, 0x33, 0x36,
  0x00, 0x36, 0x18, 0x3f, 0x39, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x12, 0x39, 0x3f, 0x18, 0x36, 0x00,
};

lv_img_dsc_t PhoneAppIcon = {
  .header.always_zero = 0,
  .header.w = 50,
  .header.h = 50,
  .data_size = 3524,
  .header.cf = LV_IMG_CF_INDEXED_8BIT,
  .data = PhoneAppIcon_map,
};

#include "Drivers/lv_conf.h"
#include "lvgl/src/lv_draw/lv_draw_img.h"

#ifndef LV_ATTRIBUTE_MEM_ALIGN
#define LV_ATTRIBUTE_MEM_ALIGN
#endif

#ifndef LV_ATTRIBUTE_LARGE_CONST
#define LV_ATTRIBUTE_LARGE_CONST
#endif


const LV_ATTRIBUTE_MEM_ALIGN LV_ATTRIBUTE_LARGE_CONST uint8_t PowerSavingStatus_map[] = {
#if LV_COLOR_DEPTH == 1 || LV_COLOR_DEPTH == 8
  /*Pixel format: Alpha 8 bit, Red: 3 bit, Green: 3 bit, Blue: 2 bit*/
  0x1c, 0x63, 0x1c, 0xe7, 0x1c, 0xf8, 0x1c, 0xdf, 0x1c, 0x8b, 0x1c, 0x18, 0x1c, 0x00,
  0x1c, 0x0f, 0x1c, 0xa8, 0x1c, 0xfc, 0x1c, 0xf0, 0x1c, 0xfc, 0x1c, 0xa3, 0x1c, 0x0f,
  0x1c, 0x00, 0x1c, 0x7c, 0x1c, 0xf0, 0x1c, 0xb0, 0x1c, 0xff, 0x1c, 0xf3, 0x1c, 0x47,
  0x00, 0x00, 0x1c, 0x5f, 0x1c, 0xf8, 0x1c, 0xa8, 0x1c, 0xf4, 0x1c, 0xeb, 0x1c, 0x3f,
  0x00, 0x00, 0x1c, 0x10, 0x1c, 0x8c, 0x1c, 0xa0, 0x1c, 0xb4, 0x1c, 0xa7, 0x1c, 0x0f,
  0x00, 0x00, 0x1c, 0x00, 0x1c, 0x03, 0x1c, 0x17, 0x1c, 0x27, 0x1c, 0x98, 0x1c, 0x6f,
#endif
#if LV_COLOR_DEPTH == 16 && LV_COLOR_16_SWAP == 0
  /*Pixel format: Alpha 8 bit, Red: 5 bit, Green: 6 bit, Blue: 5 bit*/
  0xe0, 0x07, 0x63, 0xe0, 0x07, 0xe7, 0xe0, 0x07, 0xf8, 0xe0, 0x07, 0xdf, 0xe0, 0x07, 0x8b, 0xe0, 0x07, 0x18, 0xe0, 0x07, 0x00,
  0xe0, 0x07, 0x0f, 0xe0, 0x07, 0xa8, 0xe0, 0x07, 0xfc, 0xe0, 0x07, 0xf0, 0xe0, 0x07, 0xfc, 0xe0, 0x07, 0xa3, 0xe0, 0x07, 0x0f,
  0xe0, 0x07, 0x00, 0xe0, 0x07, 0x7c, 0xe0, 0x07, 0xf0, 0xe0, 0x07, 0xb0, 0xe0, 0x07, 0xff, 0xe0, 0x07, 0xf3, 0xe0, 0x07, 0x47,
  0x00, 0x00, 0x00, 0xe0, 0x07, 0x5f, 0xe0, 0x07, 0xf8, 0xe0, 0x07, 0xa8, 0xe0, 0x07, 0xf4, 0xe0, 0x07, 0xeb, 0xe0, 0x07, 0x3f,
  0x00, 0x00, 0x00, 0xe0, 0x07, 0x10, 0xe0, 0x07, 0x8c, 0xe0, 0x07, 0xa0, 0xe0, 0x07, 0xb4, 0xe0, 0x07, 0xa7, 0xe0, 0x07, 0x0f,
  0x00, 0x00, 0x00, 0xe0, 0x07, 0x00, 0xe0, 0x07, 0x03, 0xe0, 0x07, 0x17, 0xe0, 0x07, 0x27, 0xe0, 0x07, 0x98, 0xe0, 0x07, 0x6f,
#endif
#if LV_COLOR_DEPTH == 16 && LV_COLOR_16_SWAP != 0
  /*Pixel format: Alpha 8 bit, Red: 5 bit, Green: 6 bit, Blue: 5 bit  BUT the 2  color bytes are swapped*/
  0x07, 0xe0, 0x63, 0x07, 0xe0, 0xe7, 0x07, 0xe0, 0xf8, 0x07, 0xe0, 0xdf, 0x07, 0xe0, 0x8b, 0x07, 0xe0, 0x18, 0x07, 0xe0, 0x00,
  0x07, 0xe0, 0x0f, 0x07, 0xe0, 0xa8, 0x07, 0xe0, 0xfc, 0x07, 0xe0, 0xf0, 0x07, 0xe0, 0xfc, 0x07, 0xe0, 0xa3, 0x07, 0xe0, 0x0f,
  0x07, 0xe0, 0x00, 0x07, 0xe0, 0x7c, 0x07, 0xe0, 0xf0, 0x07, 0xe0, 0xb0, 0x07, 0xe0, 0xff, 0x07, 0xe0, 0xf3, 0x07, 0xe0, 0x47,
  0x00, 0x00, 0x00, 0x07, 0xe0, 0x5f, 0x07, 0xe0, 0xf8, 0x07, 0xe0, 0xa8, 0x07, 0xe0, 0xf4, 0x07, 0xe0, 0xeb, 0x07, 0xe0, 0x3f,
  0x00, 0x00, 0x00, 0x07, 0xe0, 0x10, 0x07, 0xe0, 0x8c, 0x07, 0xe0, 0xa0, 0x07, 0xe0, 0xb4, 0x07, 0xe0, 0xa7, 0x07, 0xe0, 0x0f,
  0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x07, 0xe0, 0x03, 0x07, 0xe0, 0x17, 0x07, 0xe0, 0x27, 0x07, 0xe0, 0x98, 0x07, 0xe0, 0x6f,
#endif
#if LV_COLOR_DEPTH == 32
  0x00, 0xff, 0x00, 0x63, 0x00, 0xff, 0x00, 0xe7, 0x00, 0xff, 0x00, 0xf8, 0x00, 0xff, 0x00, 0xdf, 0x00, 0xff, 0x00, 0x8b, 0x00, 0xff, 0x00, 0x18, 0x00, 0xff, 0x00, 0x00,
  0x00, 0xff, 0x00, 0x0f, 0x00, 0xff, 0x00, 0xa8, 0x00, 0xff, 0x00, 0xfc, 0x00, 0xff, 0x00, 0xf0, 0x00, 0xff, 0x00, 0xfc, 0x00, 0xff, 0x00, 0xa3, 0x00, 0xff, 0x00, 0x0f,
  0x00, 0xff, 0x00, 0x00, 0x00, 0xff, 0x00, 0x7c, 0x00, 0xff, 0x00, 0xf0, 0x00, 0xff, 0x00, 0xb0, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xf3, 0x00, 0xff, 0x00, 0x47,
  0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0x5f, 0x00, 0xff, 0x00, 0xf8, 0x00, 0xff, 0x00, 0xa8, 0x00, 0xff, 0x00, 0xf4, 0x00, 0xff, 0x00, 0xeb, 0x00, 0xff, 0x00, 0x3f,
  0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0x10, 0x00, 0xff, 0x00, 0x8c, 0x00, 0xff, 0x00, 0xa0, 0x00, 0xff, 0x00, 0xb4, 0x00, 0xff, 0x00, 0xa7, 0x00, 0xff, 0x00, 0x0f,
  0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00, 0xff, 0x00, 0x03, 0x00, 0xff, 0x00, 0x17, 0x00, 0xff, 0x00, 0x27, 0x00, 0xff, 0x00, 0x98, 0x00, 0xff, 0x00, 0x6f,
#endif
};

lv_img_dsc_t PowerSavingStatus = {
  .header.always_zero = 0,
  .header.w = 7,
  .header.h = 6,
  .data_size = 42 * LV_IMG_PX_SIZE_ALPHA_BYTE,
  .header.cf = LV_IMG_CF_TRUE_COLOR_ALPHA,
  .data = PowerSavingStatus_map,
};

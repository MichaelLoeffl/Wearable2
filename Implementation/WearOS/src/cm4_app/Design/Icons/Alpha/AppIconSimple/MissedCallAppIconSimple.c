#include "Drivers/lv_conf.h"
#include "lvgl/src/lv_draw/lv_draw_img.h"

#ifndef LV_ATTRIBUTE_MEM_ALIGN
#define LV_ATTRIBUTE_MEM_ALIGN
#endif

#ifndef LV_ATTRIBUTE_LARGE_CONST
#define LV_ATTRIBUTE_LARGE_CONST
#endif


const LV_ATTRIBUTE_MEM_ALIGN LV_ATTRIBUTE_LARGE_CONST uint8_t MissedCallAppIconSimple_map[] = {
#if LV_COLOR_DEPTH == 1 || LV_COLOR_DEPTH == 8
  /*Pixel format: Alpha 8 bit, Red: 3 bit, Green: 3 bit, Blue: 2 bit*/
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0xff, 0x03, 0xff, 0x03, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0xff, 0x00, 0xff, 0x03, 0xff, 0x0b, 0xff, 0x18, 0xff, 0x1b, 0xff, 0x1c, 0xff, 0x23, 0xff, 0x23, 0xff, 0x1c, 0xff, 0x1b, 0xff, 0x18, 0xff, 0x0b, 0xff, 0x03, 0xff, 0x00, 0x00, 0x00,
  0xff, 0x00, 0xff, 0x03, 0xff, 0x13, 0xff, 0x43, 0xff, 0x80, 0xff, 0x90, 0xff, 0x90, 0xff, 0x84, 0xff, 0x87, 0xff, 0x90, 0xff, 0x93, 0xff, 0x83, 0xff, 0x43, 0xff, 0x13, 0xff, 0x03, 0xff, 0x00,
  0xff, 0x03, 0xff, 0x4b, 0xff, 0x78, 0xff, 0x67, 0xff, 0x40, 0xff, 0x5f, 0xff, 0x83, 0xff, 0x97, 0xff, 0x97, 0xff, 0x83, 0xff, 0x5f, 0xff, 0x40, 0xff, 0x67, 0xff, 0x78, 0xff, 0x4b, 0xff, 0x03,
  0xff, 0x5f, 0xff, 0x63, 0xff, 0x4f, 0xff, 0x38, 0xff, 0x40, 0xff, 0x68, 0xff, 0x4c, 0xff, 0x3f, 0xff, 0x3f, 0xff, 0x4c, 0xff, 0x68, 0xff, 0x40, 0xff, 0x38, 0xff, 0x4f, 0xff, 0x63, 0xff, 0x5f,
  0xff, 0xa3, 0xff, 0x50, 0xff, 0x0f, 0xff, 0x14, 0xff, 0x50, 0xff, 0x77, 0xff, 0x24, 0xff, 0x00, 0xff, 0x00, 0xff, 0x24, 0xff, 0x77, 0xff, 0x50, 0xff, 0x14, 0xff, 0x0f, 0xff, 0x50, 0xff, 0xa3,
  0xff, 0x80, 0xff, 0x50, 0xff, 0x2b, 0xff, 0x34, 0xff, 0x57, 0xff, 0x54, 0xff, 0x13, 0xff, 0x00, 0xff, 0x00, 0xff, 0x13, 0xff, 0x54, 0xff, 0x57, 0xff, 0x34, 0xff, 0x2b, 0xff, 0x50, 0xff, 0x80,
  0xff, 0x20, 0xff, 0x7b, 0xff, 0x9c, 0xff, 0x8c, 0xff, 0x6f, 0xff, 0x1c, 0xff, 0x04, 0xff, 0x00, 0xff, 0x00, 0xff, 0x04, 0xff, 0x1f, 0xff, 0x6f, 0xff, 0x8c, 0xff, 0x9c, 0xff, 0x7b, 0xff, 0x20,
  0xff, 0x04, 0xff, 0x27, 0xff, 0x2c, 0xff, 0x1c, 0xff, 0x13, 0xff, 0x04, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x04, 0xff, 0x13, 0xff, 0x1f, 0xff, 0x2c, 0xff, 0x27, 0xff, 0x04,
  0xff, 0x00, 0xff, 0x04, 0xff, 0x03, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0xff, 0x03, 0xff, 0x04, 0xff, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
#endif
#if LV_COLOR_DEPTH == 16 && LV_COLOR_16_SWAP == 0
  /*Pixel format: Alpha 8 bit, Red: 5 bit, Green: 6 bit, Blue: 5 bit*/
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x03, 0xff, 0xff, 0x03, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x03, 0xff, 0xff, 0x0b, 0xff, 0xff, 0x18, 0xff, 0xff, 0x1b, 0xff, 0xff, 0x1c, 0xff, 0xff, 0x23, 0xff, 0xff, 0x23, 0xff, 0xff, 0x1c, 0xff, 0xff, 0x1b, 0xff, 0xff, 0x18, 0xff, 0xff, 0x0b, 0xff, 0xff, 0x03, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00,
  0xff, 0xff, 0x00, 0xff, 0xff, 0x03, 0xff, 0xff, 0x13, 0xff, 0xff, 0x43, 0xff, 0xff, 0x80, 0xff, 0xff, 0x90, 0xff, 0xff, 0x90, 0xff, 0xff, 0x84, 0xff, 0xff, 0x87, 0xff, 0xff, 0x90, 0xff, 0xff, 0x93, 0xff, 0xff, 0x83, 0xff, 0xff, 0x43, 0xff, 0xff, 0x13, 0xff, 0xff, 0x03, 0xff, 0xff, 0x00,
  0xff, 0xff, 0x03, 0xff, 0xff, 0x4b, 0xff, 0xff, 0x78, 0xff, 0xff, 0x67, 0xff, 0xff, 0x40, 0xff, 0xff, 0x5f, 0xff, 0xff, 0x83, 0xff, 0xff, 0x97, 0xff, 0xff, 0x97, 0xff, 0xff, 0x83, 0xff, 0xff, 0x5f, 0xff, 0xff, 0x40, 0xff, 0xff, 0x67, 0xff, 0xff, 0x78, 0xff, 0xff, 0x4b, 0xff, 0xff, 0x03,
  0xff, 0xff, 0x5f, 0xff, 0xff, 0x63, 0xff, 0xff, 0x4f, 0xff, 0xff, 0x38, 0xff, 0xff, 0x40, 0xff, 0xff, 0x68, 0xff, 0xff, 0x4c, 0xff, 0xff, 0x3f, 0xff, 0xff, 0x3f, 0xff, 0xff, 0x4c, 0xff, 0xff, 0x68, 0xff, 0xff, 0x40, 0xff, 0xff, 0x38, 0xff, 0xff, 0x4f, 0xff, 0xff, 0x63, 0xff, 0xff, 0x5f,
  0xff, 0xff, 0xa3, 0xff, 0xff, 0x50, 0xff, 0xff, 0x0f, 0xff, 0xff, 0x14, 0xff, 0xff, 0x50, 0xff, 0xff, 0x77, 0xff, 0xff, 0x24, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x24, 0xff, 0xff, 0x77, 0xff, 0xff, 0x50, 0xff, 0xff, 0x14, 0xff, 0xff, 0x0f, 0xff, 0xff, 0x50, 0xff, 0xff, 0xa3,
  0xff, 0xff, 0x80, 0xff, 0xff, 0x50, 0xff, 0xff, 0x2b, 0xff, 0xff, 0x34, 0xff, 0xff, 0x57, 0xff, 0xff, 0x54, 0xff, 0xff, 0x13, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x13, 0xff, 0xff, 0x54, 0xff, 0xff, 0x57, 0xff, 0xff, 0x34, 0xff, 0xff, 0x2b, 0xff, 0xff, 0x50, 0xff, 0xff, 0x80,
  0xff, 0xff, 0x20, 0xff, 0xff, 0x7b, 0xff, 0xff, 0x9c, 0xff, 0xff, 0x8c, 0xff, 0xff, 0x6f, 0xff, 0xff, 0x1c, 0xff, 0xff, 0x04, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x04, 0xff, 0xff, 0x1f, 0xff, 0xff, 0x6f, 0xff, 0xff, 0x8c, 0xff, 0xff, 0x9c, 0xff, 0xff, 0x7b, 0xff, 0xff, 0x20,
  0xff, 0xff, 0x04, 0xff, 0xff, 0x27, 0xff, 0xff, 0x2c, 0xff, 0xff, 0x1c, 0xff, 0xff, 0x13, 0xff, 0xff, 0x04, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x04, 0xff, 0xff, 0x13, 0xff, 0xff, 0x1f, 0xff, 0xff, 0x2c, 0xff, 0xff, 0x27, 0xff, 0xff, 0x04,
  0xff, 0xff, 0x00, 0xff, 0xff, 0x04, 0xff, 0xff, 0x03, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x03, 0xff, 0xff, 0x04, 0xff, 0xff, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
#endif
#if LV_COLOR_DEPTH == 16 && LV_COLOR_16_SWAP != 0
  /*Pixel format: Alpha 8 bit, Red: 5 bit, Green: 6 bit, Blue: 5 bit  BUT the 2  color bytes are swapped*/
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x03, 0xff, 0xff, 0x03, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x03, 0xff, 0xff, 0x0b, 0xff, 0xff, 0x18, 0xff, 0xff, 0x1b, 0xff, 0xff, 0x1c, 0xff, 0xff, 0x23, 0xff, 0xff, 0x23, 0xff, 0xff, 0x1c, 0xff, 0xff, 0x1b, 0xff, 0xff, 0x18, 0xff, 0xff, 0x0b, 0xff, 0xff, 0x03, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00,
  0xff, 0xff, 0x00, 0xff, 0xff, 0x03, 0xff, 0xff, 0x13, 0xff, 0xff, 0x43, 0xff, 0xff, 0x80, 0xff, 0xff, 0x90, 0xff, 0xff, 0x90, 0xff, 0xff, 0x84, 0xff, 0xff, 0x87, 0xff, 0xff, 0x90, 0xff, 0xff, 0x93, 0xff, 0xff, 0x83, 0xff, 0xff, 0x43, 0xff, 0xff, 0x13, 0xff, 0xff, 0x03, 0xff, 0xff, 0x00,
  0xff, 0xff, 0x03, 0xff, 0xff, 0x4b, 0xff, 0xff, 0x78, 0xff, 0xff, 0x67, 0xff, 0xff, 0x40, 0xff, 0xff, 0x5f, 0xff, 0xff, 0x83, 0xff, 0xff, 0x97, 0xff, 0xff, 0x97, 0xff, 0xff, 0x83, 0xff, 0xff, 0x5f, 0xff, 0xff, 0x40, 0xff, 0xff, 0x67, 0xff, 0xff, 0x78, 0xff, 0xff, 0x4b, 0xff, 0xff, 0x03,
  0xff, 0xff, 0x5f, 0xff, 0xff, 0x63, 0xff, 0xff, 0x4f, 0xff, 0xff, 0x38, 0xff, 0xff, 0x40, 0xff, 0xff, 0x68, 0xff, 0xff, 0x4c, 0xff, 0xff, 0x3f, 0xff, 0xff, 0x3f, 0xff, 0xff, 0x4c, 0xff, 0xff, 0x68, 0xff, 0xff, 0x40, 0xff, 0xff, 0x38, 0xff, 0xff, 0x4f, 0xff, 0xff, 0x63, 0xff, 0xff, 0x5f,
  0xff, 0xff, 0xa3, 0xff, 0xff, 0x50, 0xff, 0xff, 0x0f, 0xff, 0xff, 0x14, 0xff, 0xff, 0x50, 0xff, 0xff, 0x77, 0xff, 0xff, 0x24, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x24, 0xff, 0xff, 0x77, 0xff, 0xff, 0x50, 0xff, 0xff, 0x14, 0xff, 0xff, 0x0f, 0xff, 0xff, 0x50, 0xff, 0xff, 0xa3,
  0xff, 0xff, 0x80, 0xff, 0xff, 0x50, 0xff, 0xff, 0x2b, 0xff, 0xff, 0x34, 0xff, 0xff, 0x57, 0xff, 0xff, 0x54, 0xff, 0xff, 0x13, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x13, 0xff, 0xff, 0x54, 0xff, 0xff, 0x57, 0xff, 0xff, 0x34, 0xff, 0xff, 0x2b, 0xff, 0xff, 0x50, 0xff, 0xff, 0x80,
  0xff, 0xff, 0x20, 0xff, 0xff, 0x7b, 0xff, 0xff, 0x9c, 0xff, 0xff, 0x8c, 0xff, 0xff, 0x6f, 0xff, 0xff, 0x1c, 0xff, 0xff, 0x04, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x04, 0xff, 0xff, 0x1f, 0xff, 0xff, 0x6f, 0xff, 0xff, 0x8c, 0xff, 0xff, 0x9c, 0xff, 0xff, 0x7b, 0xff, 0xff, 0x20,
  0xff, 0xff, 0x04, 0xff, 0xff, 0x27, 0xff, 0xff, 0x2c, 0xff, 0xff, 0x1c, 0xff, 0xff, 0x13, 0xff, 0xff, 0x04, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x04, 0xff, 0xff, 0x13, 0xff, 0xff, 0x1f, 0xff, 0xff, 0x2c, 0xff, 0xff, 0x27, 0xff, 0xff, 0x04,
  0xff, 0xff, 0x00, 0xff, 0xff, 0x04, 0xff, 0xff, 0x03, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x03, 0xff, 0xff, 0x04, 0xff, 0xff, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
#endif
#if LV_COLOR_DEPTH == 32
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x03, 0xff, 0xff, 0xff, 0x03, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x03, 0xff, 0xff, 0xff, 0x0b, 0xff, 0xff, 0xff, 0x18, 0xff, 0xff, 0xff, 0x1b, 0xff, 0xff, 0xff, 0x1c, 0xff, 0xff, 0xff, 0x23, 0xff, 0xff, 0xff, 0x23, 0xff, 0xff, 0xff, 0x1c, 0xff, 0xff, 0xff, 0x1b, 0xff, 0xff, 0xff, 0x18, 0xff, 0xff, 0xff, 0x0b, 0xff, 0xff, 0xff, 0x03, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00,
  0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x03, 0xff, 0xff, 0xff, 0x13, 0xff, 0xff, 0xff, 0x43, 0xff, 0xff, 0xff, 0x80, 0xff, 0xff, 0xff, 0x90, 0xff, 0xff, 0xff, 0x90, 0xff, 0xff, 0xff, 0x84, 0xff, 0xff, 0xff, 0x87, 0xff, 0xff, 0xff, 0x90, 0xff, 0xff, 0xff, 0x93, 0xff, 0xff, 0xff, 0x83, 0xff, 0xff, 0xff, 0x43, 0xff, 0xff, 0xff, 0x13, 0xff, 0xff, 0xff, 0x03, 0xff, 0xff, 0xff, 0x00,
  0xff, 0xff, 0xff, 0x03, 0xff, 0xff, 0xff, 0x4b, 0xff, 0xff, 0xff, 0x78, 0xff, 0xff, 0xff, 0x67, 0xff, 0xff, 0xff, 0x40, 0xff, 0xff, 0xff, 0x5f, 0xff, 0xff, 0xff, 0x83, 0xff, 0xff, 0xff, 0x97, 0xff, 0xff, 0xff, 0x97, 0xff, 0xff, 0xff, 0x83, 0xff, 0xff, 0xff, 0x5f, 0xff, 0xff, 0xff, 0x40, 0xff, 0xff, 0xff, 0x67, 0xff, 0xff, 0xff, 0x78, 0xff, 0xff, 0xff, 0x4b, 0xff, 0xff, 0xff, 0x03,
  0xff, 0xff, 0xff, 0x5f, 0xff, 0xff, 0xff, 0x63, 0xff, 0xff, 0xff, 0x4f, 0xff, 0xff, 0xff, 0x38, 0xff, 0xff, 0xff, 0x40, 0xff, 0xff, 0xff, 0x68, 0xff, 0xff, 0xff, 0x4c, 0xff, 0xff, 0xff, 0x3f, 0xff, 0xff, 0xff, 0x3f, 0xff, 0xff, 0xff, 0x4c, 0xff, 0xff, 0xff, 0x68, 0xff, 0xff, 0xff, 0x40, 0xff, 0xff, 0xff, 0x38, 0xff, 0xff, 0xff, 0x4f, 0xff, 0xff, 0xff, 0x63, 0xff, 0xff, 0xff, 0x5f,
  0xff, 0xff, 0xff, 0xa3, 0xff, 0xff, 0xff, 0x50, 0xff, 0xff, 0xff, 0x0f, 0xff, 0xff, 0xff, 0x14, 0xff, 0xff, 0xff, 0x50, 0xff, 0xff, 0xff, 0x77, 0xff, 0xff, 0xff, 0x24, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x24, 0xff, 0xff, 0xff, 0x77, 0xff, 0xff, 0xff, 0x50, 0xff, 0xff, 0xff, 0x14, 0xff, 0xff, 0xff, 0x0f, 0xff, 0xff, 0xff, 0x50, 0xff, 0xff, 0xff, 0xa3,
  0xff, 0xff, 0xff, 0x80, 0xff, 0xff, 0xff, 0x50, 0xff, 0xff, 0xff, 0x2b, 0xff, 0xff, 0xff, 0x34, 0xff, 0xff, 0xff, 0x57, 0xff, 0xff, 0xff, 0x54, 0xff, 0xff, 0xff, 0x13, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x13, 0xff, 0xff, 0xff, 0x54, 0xff, 0xff, 0xff, 0x57, 0xff, 0xff, 0xff, 0x34, 0xff, 0xff, 0xff, 0x2b, 0xff, 0xff, 0xff, 0x50, 0xff, 0xff, 0xff, 0x80,
  0xff, 0xff, 0xff, 0x20, 0xff, 0xff, 0xff, 0x7b, 0xff, 0xff, 0xff, 0x9c, 0xff, 0xff, 0xff, 0x8c, 0xff, 0xff, 0xff, 0x6f, 0xff, 0xff, 0xff, 0x1c, 0xff, 0xff, 0xff, 0x04, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x04, 0xff, 0xff, 0xff, 0x1f, 0xff, 0xff, 0xff, 0x6f, 0xff, 0xff, 0xff, 0x8c, 0xff, 0xff, 0xff, 0x9c, 0xff, 0xff, 0xff, 0x7b, 0xff, 0xff, 0xff, 0x20,
  0xff, 0xff, 0xff, 0x04, 0xff, 0xff, 0xff, 0x27, 0xff, 0xff, 0xff, 0x2c, 0xff, 0xff, 0xff, 0x1c, 0xff, 0xff, 0xff, 0x13, 0xff, 0xff, 0xff, 0x04, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x04, 0xff, 0xff, 0xff, 0x13, 0xff, 0xff, 0xff, 0x1f, 0xff, 0xff, 0xff, 0x2c, 0xff, 0xff, 0xff, 0x27, 0xff, 0xff, 0xff, 0x04,
  0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x04, 0xff, 0xff, 0xff, 0x03, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x00, 0xff, 0xff, 0xff, 0x03, 0xff, 0xff, 0xff, 0x04, 0xff, 0xff, 0xff, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
#endif
};

lv_img_dsc_t MissedCallAppIconSimple = {
  .header.always_zero = 0,
  .header.w = 16,
  .header.h = 16,
  .data_size = 256 * LV_IMG_PX_SIZE_ALPHA_BYTE,
  .header.cf = LV_IMG_CF_TRUE_COLOR_ALPHA,
  .data = MissedCallAppIconSimple_map,
};

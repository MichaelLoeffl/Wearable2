#ifndef BUTTONLOGIC_H
#define BUTTONLOGIC_H

#include "Common/Common.h"

CFUNC bool ButtonPressed();
CFUNC bool ButtonReleased();
CFUNC bool ButtonClicked();
CFUNC bool ButtonLongPressed();

CFUNC void ButtonSetToSleep();
CFUNC void ButtonHandler(int ms);

#endif /* BUTTONLOGIC_H */

#ifndef BATTERYDRIVER_H
#define BATTERYDRIVER_H

#include "Common/Common.h"
#include <stdint.h>

// TBatteryState.ErrorCode:
#define BATT_OK      0x00
#define BATT_TIMEOUT 0xF0
#define BATT_CMD_ERR 0xFF
#define BATT_I2CERR  0xFE

typedef struct
{
    uint8_t Gauge;       // 0..100
    uint8_t ErrorCode;   // see above
    uint8_t Charging;    // !0 = charging
    uint8_t GaugeValid;   // !0 = Percentage is valid
    uint16_t ReceiveCnt; // count of data received
} TBatteryState;

CFUNC void BatteryInitialize (void);

CFUNC void BatteryDisable();

CFUNC void BatteryUpdate(int ms);

CFUNC bool BatteryIsCharging();

CFUNC uint16_t BatteryGetPercent();

CFUNC TBatteryState BatteryGetState();

#endif /* BATTERYDRIVER_H */

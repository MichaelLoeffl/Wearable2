#ifndef LITTLEFS_SPIFLASHDRIVER_H
#define LITTLEFS_SPIFLASHDRIVER_H

// adaption of littlefs to PSoC-6 with SPI flash, using SMIF
// for littlefs, see:
// https://github.com/ARMmbed/littlefs
//

#include "Common/Common.h"
#include "lfs.h"

extern lfs_t FileSystem;
extern const struct lfs_config fsConfig;

#ifdef LFS_NO_MALLOC
// use static file buffer
// use
//   lfs_file_opencfg(&FileSystem, &file, <name>, <flags>, &fileConfig0)
// instead of
//   lfs_file_open(&FileSystem, &file, <name>, <flags>)
extern const struct lfs_file_config fileConfig0;
#endif

CFUNC int SmifInit();
CFUNC bool SmifEnterMmioMode();
CFUNC bool SmifEnterXipMode();

CFUNC int LittleFS_Init();

CFUNC int littlefs_test_main();
CFUNC void dumpSpiFlash(uint32_t StartAddr, uint32_t EndAddr);
CFUNC int LittleFS_Format();

#endif /* LITTLEFS_SPIFLASHDRIVER_H */

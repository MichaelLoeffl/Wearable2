#include "Common/Common.h"

#include "cy_pdl.h"
#include "cyhal.h"
#include "cybsp.h"
#include "lfs.h"
#include "lfs_spi_flash_bd.h"

struct lfs_config lfs_cfg;
cy_rslt_t result;
lfs_t lfs;

static void increment_boot_count(lfs_t *lfs, struct lfs_config *lfs_cfg)
{
    uint32_t boot_count = 0;
    lfs_file_t file;

    /* Mount the filesystem */
    int err = lfs_mount(lfs, lfs_cfg);

    /* Reformat if we cannot mount the filesystem.
     * This should only happen when littlefs is set up on the storage device for
     * the first time.
     */
    if (err) {
        printf("\nError in mounting. This could be the first time littlefs is used on the storage device.\n");
        printf("Formatting the block device...\n\n");

        lfs_format(lfs, lfs_cfg);
        lfs_mount(lfs, lfs_cfg);
    }

    /* Read the current boot count. */
    lfs_file_open(lfs, &file, "boot_count", LFS_O_RDWR | LFS_O_CREAT);
    lfs_file_read(lfs, &file, &boot_count, sizeof(boot_count));

    /* Update the boot count. */
    boot_count += 1;
    lfs_file_rewind(lfs, &file);
    lfs_file_write(lfs, &file, &boot_count, sizeof(boot_count));

    /* The storage is not updated until the file_sd is closed successfully. */
    lfs_file_close(lfs, &file);

    /* Release any resources we were using. */
    lfs_unmount(lfs);

    /* Print the boot count. */
    printf("boot_count: %"PRIu32"\n\n", boot_count);
}

/*******************************************************************************
* Function Name: print_block_device_parameters
********************************************************************************
* Summary:
*   Prints the block device parameters such as the block count, block size, and
*   program (page) size to the UART terminal.
*
* Parameters:
*  lfs_cfg - pointer to the lfs_config structure.
*
*******************************************************************************/
static void print_block_device_parameters(struct lfs_config *lfs_cfg)
{
    printf("Number of blocks: %"PRIu32"\n", lfs_cfg->block_count);
    printf("Erase block size: %"PRIu32" bytes\n", lfs_cfg->block_size);
    printf("Prog size: %"PRIu32" bytes\n\n", lfs_cfg->prog_size);
}

CFUNC void LittleFS_Init(void)
{
    lfs_spi_flash_bd_config_t spi_flash_bd_cfg;
    lfs_spi_flash_bd_get_default_config(&spi_flash_bd_cfg);

    /* Initialize the pointers in lfs_cfg to NULL. */
    memset(&lfs_cfg, 0, sizeof(lfs_cfg));

    /* Create the SPI flash block device. */
    result = lfs_spi_flash_bd_create(&lfs_cfg, &spi_flash_bd_cfg);

    print_block_device_parameters(&lfs_cfg);
    increment_boot_count(&lfs, &lfs_cfg);
}

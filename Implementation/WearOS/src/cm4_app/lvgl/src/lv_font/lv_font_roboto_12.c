#include "lvgl/lvgl.h"

/*******************************************************************************
 * Size: 12 px
 * Bpp: 2
 * Opts: 
 * Range: 0x20-0x7E, 0x80-0xA5, 0xAD, 0xE1
 * Symbols: °®©§µ´ß
 ******************************************************************************/

#ifndef LV_FONT_ROBOTO_12
#define LV_FONT_ROBOTO_12 1
#endif

#if LV_FONT_ROBOTO_12

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t gylph_bitmap[] = {
    /* U+20 " " */

    /* U+21 "!" */
    0x8c, 0xcc, 0xcc, 0x80, 0xc0,

    /* U+22 "\"" */
    0x29, 0x28, 0x28,

    /* U+23 "#" */
    0x1, 0x10, 0x19, 0x83, 0xff, 0x42, 0x20, 0xc,
    0xc0, 0xff, 0xc0, 0x88, 0x6, 0x60, 0x15, 0x40,

    /* U+24 "$" */
    0x5, 0x0, 0xa0, 0x2a, 0xc3, 0xc, 0x34, 0x1,
    0xe0, 0x2, 0xc1, 0xc, 0x30, 0xc1, 0xf4, 0x5,
    0x0, 0x0,

    /* U+25 "%" */
    0x18, 0x0, 0xe4, 0x86, 0x29, 0xb, 0x60, 0x2,
    0x40, 0x9, 0xd0, 0x98, 0xc2, 0x22, 0x0, 0x78,
    0x0, 0x0,

    /* U+26 "&" */
    0x5, 0x0, 0xaa, 0x2, 0x48, 0xa, 0x90, 0xe,
    0x0, 0xec, 0xc6, 0x2e, 0xc, 0x34, 0x1f, 0xb0,
    0x0, 0x0,

    /* U+27 "'" */
    0x66, 0x50,

    /* U+28 "(" */
    0x1, 0x8, 0x18, 0x24, 0x30, 0x30, 0x30, 0x30,
    0x30, 0x34, 0x14, 0xc, 0x5,

    /* U+29 ")" */
    0x40, 0x82, 0xa, 0x1c, 0x30, 0xc3, 0x1c, 0x62,
    0x48, 0x40,

    /* U+2A "*" */
    0x4, 0x3, 0xa, 0xec, 0xb8, 0x27, 0x4, 0x40,

    /* U+2B "+" */
    0x4, 0x0, 0xa0, 0xa, 0xb, 0xfd, 0xa, 0x0,
    0xa0, 0x5, 0x0,

    /* U+2C "," */
    0x7, 0x69,

    /* U+2D "-" */
    0xb8,

    /* U+2E "." */
    0x0, 0xc0,

    /* U+2F "/" */
    0x1, 0x0, 0x80, 0x60, 0x20, 0x8, 0x6, 0x2,
    0x40, 0x80, 0x60, 0x24, 0x0,

    /* U+30 "0" */
    0xa, 0x2, 0xac, 0x30, 0xc7, 0xd, 0x60, 0x96,
    0x9, 0x30, 0xc3, 0x1c, 0x1f, 0x40, 0x0,

    /* U+31 "1" */
    0x1, 0x1f, 0x27, 0x7, 0x7, 0x7, 0x7, 0x7,
    0x7,

    /* U+32 "2" */
    0x9, 0x3, 0xac, 0x70, 0xc0, 0xc, 0x2, 0x80,
    0x60, 0xc, 0x2, 0x40, 0x7f, 0xe0,

    /* U+33 "3" */
    0x9, 0x3, 0xa8, 0x20, 0xc0, 0x1c, 0xf, 0x40,
    0x1c, 0x0, 0xc3, 0x1c, 0x2f, 0x40, 0x0,

    /* U+34 "4" */
    0x1, 0x40, 0x38, 0xb, 0x80, 0xd8, 0x25, 0x82,
    0x18, 0xbf, 0xe0, 0x18, 0x1, 0x80,

    /* U+35 "5" */
    0x6a, 0x2a, 0x89, 0x2, 0xf8, 0x47, 0x0, 0x94,
    0x27, 0xc, 0x7e, 0x0, 0x0,

    /* U+36 "6" */
    0x1, 0x0, 0xe4, 0x28, 0x3, 0xb4, 0x35, 0xc3,
    0xc, 0x30, 0x92, 0x4c, 0x1f, 0x80, 0x0,

    /* U+37 "7" */
    0x6a, 0x96, 0xad, 0x0, 0xc0, 0x28, 0x3, 0x0,
    0x70, 0xa, 0x0, 0xc0, 0x18, 0x0,

    /* U+38 "8" */
    0x9, 0x2, 0xa8, 0x30, 0xc3, 0x1c, 0x1f, 0x43,
    0x5c, 0x70, 0xc3, 0xc, 0x1f, 0x40, 0x0,

    /* U+39 "9" */
    0xa, 0x3, 0xb8, 0x30, 0xc6, 0xc, 0x31, 0xc2,
    0xec, 0x0, 0xc0, 0x68, 0x1e, 0x0,

    /* U+3A ":" */
    0x0, 0xc0, 0x0, 0x0, 0x3, 0x0,

    /* U+3B ";" */
    0x0, 0xc0, 0x0, 0x0, 0x7, 0x18, 0x90,

    /* U+3C "<" */
    0x0, 0x1, 0xd2, 0x91, 0xc0, 0x1e, 0x0, 0x60,

    /* U+3D "=" */
    0x3f, 0xc0, 0x3, 0xfc,

    /* U+3E ">" */
    0x0, 0x3, 0x80, 0xb, 0x40, 0x2c, 0x2e, 0x2,
    0x0,

    /* U+3F "?" */
    0x19, 0xe, 0xc1, 0x28, 0x9, 0x7, 0x3, 0x0,
    0x80, 0x0, 0xc, 0x0,

    /* U+40 "@" */
    0x2, 0xe8, 0x2, 0x82, 0x82, 0x8f, 0x20, 0x89,
    0x91, 0x23, 0x24, 0x98, 0x88, 0x22, 0x33, 0x24,
    0x8b, 0xbc, 0x30, 0x0, 0x3, 0x40, 0x0, 0x6e,
    0x40,

    /* U+41 "A" */
    0x2, 0x0, 0x1d, 0x0, 0xa8, 0x3, 0x30, 0x18,
    0xc0, 0xa2, 0x83, 0xff, 0xc, 0xc, 0xa0, 0x28,

    /* U+42 "B" */
    0x2a, 0x3, 0xad, 0x30, 0xa3, 0xa, 0x3f, 0xc3,
    0xa, 0x30, 0x33, 0xa, 0x3f, 0x80,

    /* U+43 "C" */
    0x6, 0x80, 0xba, 0x83, 0x43, 0xc, 0x0, 0x70,
    0x1, 0xc0, 0x3, 0x2, 0xa, 0x1c, 0xf, 0xd0,
    0x0, 0x0,

    /* U+44 "D" */
    0x29, 0x0, 0xeb, 0x43, 0x7, 0xc, 0xc, 0x30,
    0x30, 0xc0, 0xc3, 0x3, 0xc, 0x68, 0x3f, 0x80,

    /* U+45 "E" */
    0x2a, 0x93, 0xa9, 0x30, 0x3, 0x0, 0x3f, 0xc3,
    0x0, 0x30, 0x3, 0x0, 0x3f, 0xe0,

    /* U+46 "F" */
    0x2a, 0x83, 0xa8, 0x30, 0x3, 0x0, 0x3f, 0xc3,
    0x0, 0x30, 0x3, 0x0, 0x30, 0x0,

    /* U+47 "G" */
    0x6, 0x40, 0xba, 0x83, 0x3, 0xc, 0x0, 0x70,
    0x1, 0xc7, 0xd3, 0x3, 0x4e, 0xd, 0xf, 0xe0,
    0x0, 0x0,

    /* U+48 "H" */
    0x20, 0x14, 0xc0, 0xa3, 0x2, 0x8c, 0xa, 0x3f,
    0xf8, 0xc0, 0xa3, 0x2, 0x8c, 0xa, 0x30, 0x28,

    /* U+49 "I" */
    0x8c, 0xcc, 0xcc, 0xcc, 0xc0,

    /* U+4A "J" */
    0x0, 0x80, 0x30, 0xc, 0x3, 0x0, 0xc0, 0x30,
    0xd, 0x8a, 0x2e, 0x40,

    /* U+4B "K" */
    0x20, 0x20, 0xc2, 0x83, 0x1c, 0xc, 0xc0, 0x3e,
    0x0, 0xed, 0x3, 0x1c, 0xc, 0x28, 0x30, 0x30,

    /* U+4C "L" */
    0x20, 0x3, 0x0, 0x30, 0x3, 0x0, 0x30, 0x3,
    0x0, 0x30, 0x3, 0x0, 0x3f, 0xd0,

    /* U+4D "M" */
    0x24, 0x2, 0xe, 0x2, 0xc3, 0xc0, 0xf0, 0xe4,
    0x3c, 0x36, 0x2b, 0xc, 0xcc, 0xc3, 0x27, 0x30,
    0xc7, 0x8c, 0x30, 0xc3, 0x0,

    /* U+4E "N" */
    0x20, 0x14, 0xe0, 0xa3, 0xc2, 0x8d, 0x8a, 0x33,
    0x28, 0xc6, 0xa3, 0xe, 0x8c, 0x1e, 0x30, 0x38,

    /* U+4F "O" */
    0x6, 0x80, 0xbb, 0xc3, 0x3, 0xc, 0x9, 0x70,
    0x29, 0xc0, 0xa3, 0x2, 0x4e, 0x1c, 0xf, 0xd0,
    0x0, 0x0,

    /* U+50 "P" */
    0x2a, 0x40, 0xeb, 0x83, 0x3, 0xc, 0xc, 0x30,
    0x70, 0xfe, 0x43, 0x0, 0xc, 0x0, 0x30, 0x0,

    /* U+51 "Q" */
    0x6, 0x80, 0xbb, 0x83, 0x3, 0xc, 0x9, 0x60,
    0x25, 0x80, 0x93, 0x3, 0x4e, 0x1c, 0xf, 0xe0,
    0x1, 0xd0, 0x0, 0x0,

    /* U+52 "R" */
    0x29, 0x3, 0xad, 0x30, 0x63, 0x7, 0x30, 0xa3,
    0xfc, 0x30, 0xc3, 0x9, 0x30, 0x30,

    /* U+53 "S" */
    0x6, 0x2, 0x9d, 0x30, 0xa3, 0x40, 0x1e, 0x0,
    0x1d, 0x50, 0x63, 0xa, 0x2f, 0x80, 0x0,

    /* U+54 "T" */
    0x6a, 0xa6, 0xb9, 0x6, 0x0, 0x60, 0x6, 0x0,
    0x60, 0x6, 0x0, 0x60, 0x6, 0x0,

    /* U+55 "U" */
    0x10, 0x23, 0x3, 0x30, 0x33, 0x3, 0x30, 0x33,
    0x3, 0x30, 0x33, 0x46, 0x1f, 0x90, 0x0,

    /* U+56 "V" */
    0x50, 0x11, 0x80, 0xd3, 0x3, 0xd, 0x28, 0x28,
    0x90, 0x33, 0x0, 0xd8, 0x2, 0xd0, 0x7, 0x0,

    /* U+57 "W" */
    0x50, 0x40, 0x58, 0x34, 0x63, 0x1e, 0x28, 0xc5,
    0xc9, 0x26, 0x33, 0x9, 0xc9, 0xc1, 0xe1, 0xa0,
    0x38, 0x38, 0xd, 0xd, 0x0,

    /* U+58 "X" */
    0x10, 0x20, 0xc2, 0x82, 0x8d, 0x3, 0xa0, 0x7,
    0x0, 0x2d, 0x1, 0xdc, 0xa, 0x34, 0x70, 0x70,

    /* U+59 "Y" */
    0x50, 0x21, 0xc2, 0x83, 0xd, 0x6, 0x60, 0xf,
    0x40, 0x2c, 0x0, 0x60, 0x1, 0x80, 0x6, 0x0,

    /* U+5A "Z" */
    0x2a, 0x92, 0xae, 0x0, 0xc0, 0x24, 0x6, 0x0,
    0xd0, 0x28, 0x3, 0x0, 0x7f, 0xf0,

    /* U+5B "[" */
    0x38, 0xc3, 0xc, 0x30, 0xc3, 0xc, 0x30, 0xc3,
    0xe,

    /* U+5C "\\" */
    0x50, 0x18, 0x3, 0x0, 0x90, 0x18, 0x3, 0x0,
    0xc0, 0x24, 0x3, 0x0, 0xc0,

    /* U+5D "]" */
    0xf0, 0xc3, 0xc, 0x30, 0xc3, 0xc, 0x30, 0xc3,
    0x3c,

    /* U+5E "^" */
    0x4, 0x1c, 0x29, 0x36, 0x63,

    /* U+5F "_" */
    0xff, 0x80,

    /* U+60 "`" */
    0x70, 0xa0,

    /* U+61 "a" */
    0x5, 0x2, 0xb8, 0x10, 0xc1, 0xfc, 0x34, 0xc3,
    0x1c, 0x2e, 0xc0, 0x0,

    /* U+62 "b" */
    0x30, 0x3, 0x0, 0x35, 0x3, 0xbc, 0x30, 0xd3,
    0x9, 0x30, 0x93, 0xc, 0x3f, 0x40, 0x0,

    /* U+63 "c" */
    0x5, 0x2, 0xb8, 0x30, 0x86, 0x0, 0x60, 0x3,
    0xc, 0x1f, 0x40, 0x0,

    /* U+64 "d" */
    0x0, 0xc0, 0xc, 0x4, 0xc3, 0xbc, 0x30, 0xc6,
    0xc, 0x60, 0xc3, 0x1c, 0x2e, 0xc0, 0x0,

    /* U+65 "e" */
    0x4, 0x2, 0xb8, 0x30, 0xc7, 0xfc, 0x60, 0x3,
    0x4, 0x1f, 0x80, 0x0,

    /* U+66 "f" */
    0x1e, 0x28, 0x38, 0x79, 0x24, 0x24, 0x24, 0x24,
    0x24,

    /* U+67 "g" */
    0x4, 0x43, 0xbc, 0x30, 0xc6, 0xc, 0x60, 0xc3,
    0xc, 0x2e, 0xc0, 0xc, 0x3b, 0x80, 0x50,

    /* U+68 "h" */
    0x30, 0x3, 0x0, 0x35, 0x3, 0xb8, 0x30, 0xc3,
    0xc, 0x30, 0xc3, 0xc, 0x30, 0xc0,

    /* U+69 "i" */
    0x3, 0x13, 0x33, 0x33, 0x30,

    /* U+6A "j" */
    0x0, 0x30, 0x43, 0xc, 0x30, 0xc3, 0xc, 0x32,
    0x84,

    /* U+6B "k" */
    0x30, 0x3, 0x0, 0x30, 0x43, 0x24, 0x3a, 0x3,
    0xc0, 0x3a, 0x3, 0x34, 0x30, 0xc0,

    /* U+6C "l" */
    0x33, 0x33, 0x33, 0x33, 0x30,

    /* U+6D "m" */
    0x1, 0x4, 0xe, 0xfe, 0xc3, 0xd, 0x30, 0xc3,
    0x9, 0x30, 0xc2, 0x4c, 0x30, 0x93, 0xc, 0x24,

    /* U+6E "n" */
    0x5, 0x3, 0xb8, 0x30, 0xc3, 0xc, 0x30, 0xc3,
    0xc, 0x30, 0xc0,

    /* U+6F "o" */
    0x5, 0x2, 0xbc, 0x30, 0xc6, 0x9, 0x60, 0x93,
    0xc, 0x2f, 0x80, 0x0,

    /* U+70 "p" */
    0x5, 0x3, 0xbc, 0x30, 0xc3, 0x9, 0x30, 0x93,
    0xd, 0x3b, 0x43, 0x0, 0x30, 0x1, 0x0,

    /* U+71 "q" */
    0x4, 0x43, 0xbc, 0x30, 0xc6, 0xc, 0x60, 0xc3,
    0x1c, 0x2e, 0xc0, 0xc, 0x0, 0xc0, 0x4,

    /* U+72 "r" */
    0x0, 0x39, 0x30, 0x30, 0x30, 0x30, 0x30,

    /* U+73 "s" */
    0x4, 0xa, 0xd3, 0x4, 0xb4, 0x2, 0x88, 0x32,
    0xf4, 0x0,

    /* U+74 "t" */
    0x30, 0x74, 0xb8, 0x30, 0x30, 0x30, 0x30, 0x1c,
    0x0,

    /* U+75 "u" */
    0x0, 0x47, 0xc, 0x70, 0xc7, 0xc, 0x70, 0xc3,
    0xc, 0x2e, 0xc0, 0x0,

    /* U+76 "v" */
    0x40, 0x68, 0x63, 0x24, 0xcc, 0x2a, 0x7, 0x80,
    0xc0,

    /* U+77 "w" */
    0x40, 0x0, 0xa2, 0x89, 0x63, 0xcc, 0x36, 0xcc,
    0x29, 0xa8, 0x2c, 0x74, 0x1c, 0x30,

    /* U+78 "x" */
    0x0, 0x4c, 0xa2, 0xb0, 0x34, 0x1e, 0x9, 0xc6,
    0x28,

    /* U+79 "y" */
    0x40, 0x28, 0xa7, 0x24, 0xcc, 0x2a, 0x7, 0x40,
    0xc0, 0x30, 0x64, 0x4, 0x0,

    /* U+7A "z" */
    0x15, 0xa, 0xe0, 0x30, 0x24, 0x18, 0xc, 0x7,
    0xfc,

    /* U+7B "{" */
    0x9, 0x18, 0x18, 0x28, 0x24, 0xb0, 0x34, 0x28,
    0x28, 0x18, 0x9,

    /* U+7C "|" */
    0x7f, 0xff, 0xfc,

    /* U+7D "}" */
    0x90, 0x30, 0x34, 0x24, 0x28, 0xd, 0x28, 0x24,
    0x24, 0x30, 0x50,

    /* U+7E "~" */
    0x2e, 0x20, 0xcb, 0x80,

    /* U+A0 " " */

    /* U+A1 "¡" */
    0x12, 0x3, 0x33, 0x33, 0x30,

    /* U+A2 "¢" */
    0x5, 0x0, 0xa0, 0x2f, 0x83, 0x8, 0x60, 0x7,
    0x0, 0x30, 0xc1, 0xf4, 0xa, 0x0, 0x50,

    /* U+A3 "£" */
    0x5, 0x41, 0xec, 0x28, 0x52, 0x40, 0x7f, 0x2,
    0x40, 0x24, 0x2, 0x40, 0x7f, 0xe0,

    /* U+A4 "¤" */
    0x0, 0x0, 0x36, 0xea, 0x2d, 0x1d, 0x20, 0x9,
    0x30, 0x6, 0x30, 0x6, 0x2c, 0x1d, 0x3b, 0xea,
    0x0, 0x0,

    /* U+A5 "¥" */
    0x50, 0x47, 0xc, 0x32, 0x41, 0xa0, 0x7f, 0xc0,
    0x90, 0x7f, 0xc0, 0x90, 0x9, 0x0,

    /* U+A7 "§" */
    0x6, 0x2, 0x9c, 0x30, 0x63, 0x40, 0x2f, 0x87,
    0xe, 0x30, 0x72, 0xea, 0x6, 0xc1, 0xa, 0x34,
    0x91, 0xf8,

    /* U+A9 "©" */
    0x1, 0x90, 0x2, 0xab, 0x42, 0x6e, 0x34, 0x88,
    0x52, 0x55, 0x0, 0x94, 0x85, 0x23, 0x2e, 0x18,
    0x60, 0x18, 0x7, 0xf8, 0x0, 0x0, 0x0,

    /* U+AD "­" */
    0xb8,

    /* U+AE "®" */
    0x1, 0x90, 0x2, 0xab, 0x42, 0x7e, 0x34, 0x8c,
    0x92, 0x53, 0xf0, 0x94, 0xc9, 0x23, 0x31, 0x98,
    0x60, 0x18, 0x7, 0xf8, 0x0, 0x0, 0x0,

    /* U+B0 "°" */
    0x18, 0x25, 0x22, 0x2d,

    /* U+B4 "´" */
    0x1c, 0x30,

    /* U+B5 "µ" */
    0x10, 0x43, 0xc, 0x30, 0xc3, 0xc, 0x30, 0xc3,
    0xc, 0x3e, 0xc3, 0x0, 0x30, 0x1, 0x0,

    /* U+DF "ß" */
    0x0, 0x1, 0xf0, 0x35, 0x83, 0x18, 0x32, 0x43,
    0x30, 0x31, 0xc3, 0xa, 0x30, 0x63, 0x7d, 0x0,
    0x0,

    /* U+E1 "á" */
    0x0, 0x40, 0x28, 0x1, 0x0, 0x50, 0x2b, 0x81,
    0xc, 0x1f, 0xc3, 0x4c, 0x31, 0xc2, 0xec, 0x0,
    0x0
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_h = 0, .box_w = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 44, .box_h = 0, .box_w = 0, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 0, .adv_w = 47, .box_h = 9, .box_w = 2, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 5, .adv_w = 61, .box_h = 3, .box_w = 4, .ofs_x = 0, .ofs_y = 6},
    {.bitmap_index = 8, .adv_w = 105, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 24, .adv_w = 95, .box_h = 12, .box_w = 6, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 42, .adv_w = 122, .box_h = 10, .box_w = 7, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 60, .adv_w = 104, .box_h = 10, .box_w = 7, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 78, .adv_w = 33, .box_h = 3, .box_w = 2, .ofs_x = 0, .ofs_y = 6},
    {.bitmap_index = 80, .adv_w = 60, .box_h = 13, .box_w = 4, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 93, .adv_w = 61, .box_h = 13, .box_w = 3, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 103, .adv_w = 83, .box_h = 6, .box_w = 5, .ofs_x = 0, .ofs_y = 3},
    {.bitmap_index = 111, .adv_w = 95, .box_h = 7, .box_w = 6, .ofs_x = 0, .ofs_y = 1},
    {.bitmap_index = 122, .adv_w = 38, .box_h = 4, .box_w = 2, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 124, .adv_w = 48, .box_h = 1, .box_w = 3, .ofs_x = 0, .ofs_y = 3},
    {.bitmap_index = 125, .adv_w = 51, .box_h = 2, .box_w = 3, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 127, .adv_w = 71, .box_h = 10, .box_w = 5, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 140, .adv_w = 95, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 155, .adv_w = 95, .box_h = 9, .box_w = 4, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 164, .adv_w = 95, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 178, .adv_w = 95, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 193, .adv_w = 95, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 207, .adv_w = 95, .box_h = 10, .box_w = 5, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 220, .adv_w = 95, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 235, .adv_w = 95, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 249, .adv_w = 95, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 264, .adv_w = 95, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 278, .adv_w = 45, .box_h = 7, .box_w = 3, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 284, .adv_w = 39, .box_h = 9, .box_w = 3, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 291, .adv_w = 86, .box_h = 6, .box_w = 5, .ofs_x = 0, .ofs_y = 1},
    {.bitmap_index = 299, .adv_w = 92, .box_h = 3, .box_w = 5, .ofs_x = 0, .ofs_y = 2},
    {.bitmap_index = 303, .adv_w = 88, .box_h = 6, .box_w = 6, .ofs_x = 0, .ofs_y = 1},
    {.bitmap_index = 312, .adv_w = 81, .box_h = 9, .box_w = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 324, .adv_w = 148, .box_h = 11, .box_w = 9, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 349, .adv_w = 111, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 365, .adv_w = 105, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 379, .adv_w = 109, .box_h = 10, .box_w = 7, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 397, .adv_w = 110, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 413, .adv_w = 95, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 427, .adv_w = 92, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 441, .adv_w = 113, .box_h = 10, .box_w = 7, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 459, .adv_w = 119, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 475, .adv_w = 48, .box_h = 9, .box_w = 2, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 480, .adv_w = 93, .box_h = 9, .box_w = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 492, .adv_w = 105, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 508, .adv_w = 91, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 522, .adv_w = 145, .box_h = 9, .box_w = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 543, .adv_w = 119, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 559, .adv_w = 115, .box_h = 10, .box_w = 7, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 577, .adv_w = 106, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 593, .adv_w = 115, .box_h = 11, .box_w = 7, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 613, .adv_w = 102, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 627, .adv_w = 99, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 642, .adv_w = 100, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 656, .adv_w = 108, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 671, .adv_w = 108, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 687, .adv_w = 146, .box_h = 9, .box_w = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 708, .adv_w = 106, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 724, .adv_w = 101, .box_h = 9, .box_w = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 740, .adv_w = 100, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 754, .adv_w = 48, .box_h = 12, .box_w = 3, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 763, .adv_w = 71, .box_h = 10, .box_w = 5, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 776, .adv_w = 48, .box_h = 12, .box_w = 3, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 785, .adv_w = 71, .box_h = 5, .box_w = 4, .ofs_x = 0, .ofs_y = 4},
    {.bitmap_index = 790, .adv_w = 78, .box_h = 1, .box_w = 5, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 792, .adv_w = 59, .box_h = 2, .box_w = 3, .ofs_x = 0, .ofs_y = 7},
    {.bitmap_index = 794, .adv_w = 92, .box_h = 8, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 806, .adv_w = 95, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 821, .adv_w = 89, .box_h = 8, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 833, .adv_w = 95, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 848, .adv_w = 90, .box_h = 8, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 860, .adv_w = 61, .box_h = 9, .box_w = 4, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 869, .adv_w = 95, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 884, .adv_w = 93, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 898, .adv_w = 44, .box_h = 9, .box_w = 2, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 903, .adv_w = 43, .box_h = 12, .box_w = 3, .ofs_x = -1, .ofs_y = -3},
    {.bitmap_index = 912, .adv_w = 87, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 926, .adv_w = 44, .box_h = 9, .box_w = 2, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 931, .adv_w = 144, .box_h = 7, .box_w = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 947, .adv_w = 93, .box_h = 7, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 958, .adv_w = 96, .box_h = 8, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 970, .adv_w = 95, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 985, .adv_w = 96, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 1000, .adv_w = 58, .box_h = 7, .box_w = 4, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1007, .adv_w = 87, .box_h = 8, .box_w = 5, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 1017, .adv_w = 57, .box_h = 9, .box_w = 4, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 1026, .adv_w = 93, .box_h = 8, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 1038, .adv_w = 82, .box_h = 7, .box_w = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1047, .adv_w = 125, .box_h = 7, .box_w = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1061, .adv_w = 84, .box_h = 7, .box_w = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1070, .adv_w = 80, .box_h = 10, .box_w = 5, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 1083, .adv_w = 84, .box_h = 7, .box_w = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1092, .adv_w = 59, .box_h = 11, .box_w = 4, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1103, .adv_w = 47, .box_h = 11, .box_w = 1, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1106, .adv_w = 59, .box_h = 11, .box_w = 4, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1117, .adv_w = 113, .box_h = 2, .box_w = 7, .ofs_x = 0, .ofs_y = 2},
    {.bitmap_index = 1121, .adv_w = 44, .box_h = 0, .box_w = 0, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1121, .adv_w = 44, .box_h = 9, .box_w = 2, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1126, .adv_w = 92, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1141, .adv_w = 98, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1155, .adv_w = 137, .box_h = 9, .box_w = 8, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 1173, .adv_w = 89, .box_h = 9, .box_w = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1187, .adv_w = 103, .box_h = 12, .box_w = 6, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 1205, .adv_w = 151, .box_h = 10, .box_w = 9, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 1228, .adv_w = 48, .box_h = 1, .box_w = 3, .ofs_x = 0, .ofs_y = 3},
    {.bitmap_index = 1229, .adv_w = 151, .box_h = 10, .box_w = 9, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 1252, .adv_w = 72, .box_h = 4, .box_w = 4, .ofs_x = 0, .ofs_y = 5},
    {.bitmap_index = 1256, .adv_w = 60, .box_h = 2, .box_w = 4, .ofs_x = 0, .ofs_y = 7},
    {.bitmap_index = 1258, .adv_w = 96, .box_h = 10, .box_w = 6, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 1273, .adv_w = 100, .box_h = 11, .box_w = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 1290, .adv_w = 92, .box_h = 11, .box_w = 6, .ofs_x = 0, .ofs_y = -1}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_1[] = {
    0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x7, 0x9,
    0xd, 0xe, 0x10, 0x14, 0x15, 0x3f, 0x41
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 95, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY,
        .glyph_id_start = 1, .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0
    },
    {
        .range_start = 160, .range_length = 66, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY,
        .glyph_id_start = 96, .unicode_list = unicode_list_1, .glyph_id_ofs_list = NULL, .list_length = 15
    }
};

/*-----------------
 *    KERNING
 *----------------*/


/*Pair left and right glyphs for kerning*/
static const uint8_t kern_pair_glyph_ids[] =
{
    1, 53,
    3, 3,
    3, 8,
    3, 34,
    3, 66,
    3, 68,
    3, 69,
    3, 70,
    3, 72,
    3, 78,
    3, 79,
    3, 80,
    3, 81,
    3, 82,
    3, 84,
    3, 88,
    3, 110,
    8, 3,
    8, 8,
    8, 34,
    8, 66,
    8, 68,
    8, 69,
    8, 70,
    8, 72,
    8, 78,
    8, 79,
    8, 80,
    8, 81,
    8, 82,
    8, 84,
    8, 88,
    8, 110,
    9, 55,
    9, 56,
    9, 58,
    13, 3,
    13, 8,
    15, 3,
    15, 8,
    16, 16,
    34, 3,
    34, 8,
    34, 32,
    34, 36,
    34, 40,
    34, 48,
    34, 50,
    34, 53,
    34, 54,
    34, 55,
    34, 56,
    34, 58,
    34, 80,
    34, 85,
    34, 86,
    34, 87,
    34, 88,
    34, 90,
    34, 91,
    35, 53,
    35, 55,
    35, 58,
    36, 10,
    36, 53,
    36, 62,
    36, 94,
    37, 13,
    37, 15,
    37, 34,
    37, 53,
    37, 55,
    37, 57,
    37, 58,
    37, 59,
    38, 53,
    38, 68,
    38, 69,
    38, 70,
    38, 71,
    38, 72,
    38, 80,
    38, 82,
    38, 86,
    38, 87,
    38, 88,
    38, 90,
    39, 13,
    39, 15,
    39, 34,
    39, 43,
    39, 53,
    39, 66,
    39, 68,
    39, 69,
    39, 70,
    39, 72,
    39, 80,
    39, 82,
    39, 83,
    39, 86,
    39, 87,
    39, 90,
    39, 110,
    41, 34,
    41, 53,
    41, 57,
    41, 58,
    42, 34,
    42, 53,
    42, 57,
    42, 58,
    43, 34,
    44, 14,
    44, 36,
    44, 40,
    44, 48,
    44, 50,
    44, 68,
    44, 69,
    44, 70,
    44, 72,
    44, 78,
    44, 79,
    44, 80,
    44, 81,
    44, 82,
    44, 86,
    44, 87,
    44, 88,
    44, 90,
    44, 104,
    45, 3,
    45, 8,
    45, 34,
    45, 36,
    45, 40,
    45, 48,
    45, 50,
    45, 53,
    45, 54,
    45, 55,
    45, 56,
    45, 58,
    45, 86,
    45, 87,
    45, 88,
    45, 90,
    46, 34,
    46, 53,
    46, 57,
    46, 58,
    47, 34,
    47, 53,
    47, 57,
    47, 58,
    48, 13,
    48, 15,
    48, 34,
    48, 53,
    48, 55,
    48, 57,
    48, 58,
    48, 59,
    49, 13,
    49, 15,
    49, 34,
    49, 43,
    49, 57,
    49, 59,
    49, 66,
    49, 68,
    49, 69,
    49, 70,
    49, 72,
    49, 80,
    49, 82,
    49, 85,
    49, 87,
    49, 90,
    49, 110,
    50, 53,
    50, 55,
    50, 56,
    50, 58,
    51, 53,
    51, 55,
    51, 58,
    53, 1,
    53, 13,
    53, 14,
    53, 15,
    53, 34,
    53, 36,
    53, 40,
    53, 43,
    53, 48,
    53, 50,
    53, 52,
    53, 53,
    53, 55,
    53, 56,
    53, 58,
    53, 66,
    53, 68,
    53, 69,
    53, 70,
    53, 72,
    53, 78,
    53, 79,
    53, 80,
    53, 81,
    53, 82,
    53, 83,
    53, 84,
    53, 86,
    53, 87,
    53, 88,
    53, 89,
    53, 90,
    53, 91,
    53, 104,
    53, 110,
    54, 34,
    55, 10,
    55, 13,
    55, 14,
    55, 15,
    55, 34,
    55, 36,
    55, 40,
    55, 48,
    55, 50,
    55, 62,
    55, 66,
    55, 68,
    55, 69,
    55, 70,
    55, 72,
    55, 80,
    55, 82,
    55, 83,
    55, 86,
    55, 87,
    55, 90,
    55, 94,
    55, 104,
    55, 110,
    56, 10,
    56, 13,
    56, 14,
    56, 15,
    56, 34,
    56, 53,
    56, 62,
    56, 66,
    56, 68,
    56, 69,
    56, 70,
    56, 72,
    56, 80,
    56, 82,
    56, 83,
    56, 86,
    56, 94,
    56, 104,
    56, 110,
    57, 14,
    57, 36,
    57, 40,
    57, 48,
    57, 50,
    57, 55,
    57, 68,
    57, 69,
    57, 70,
    57, 72,
    57, 80,
    57, 82,
    57, 86,
    57, 87,
    57, 90,
    57, 104,
    58, 7,
    58, 10,
    58, 11,
    58, 13,
    58, 14,
    58, 15,
    58, 34,
    58, 36,
    58, 40,
    58, 43,
    58, 48,
    58, 50,
    58, 52,
    58, 53,
    58, 54,
    58, 55,
    58, 56,
    58, 57,
    58, 58,
    58, 62,
    58, 66,
    58, 68,
    58, 69,
    58, 70,
    58, 71,
    58, 72,
    58, 78,
    58, 79,
    58, 80,
    58, 81,
    58, 82,
    58, 83,
    58, 84,
    58, 85,
    58, 86,
    58, 87,
    58, 89,
    58, 90,
    58, 91,
    58, 94,
    58, 104,
    58, 110,
    59, 34,
    59, 36,
    59, 40,
    59, 48,
    59, 50,
    59, 68,
    59, 69,
    59, 70,
    59, 72,
    59, 80,
    59, 82,
    59, 86,
    59, 87,
    59, 88,
    59, 90,
    60, 43,
    60, 54,
    66, 3,
    66, 8,
    66, 87,
    66, 90,
    67, 3,
    67, 8,
    67, 87,
    67, 89,
    67, 90,
    67, 91,
    68, 3,
    68, 8,
    70, 3,
    70, 8,
    70, 87,
    70, 90,
    71, 3,
    71, 8,
    71, 10,
    71, 62,
    71, 68,
    71, 69,
    71, 70,
    71, 72,
    71, 82,
    71, 94,
    73, 3,
    73, 8,
    76, 68,
    76, 69,
    76, 70,
    76, 72,
    76, 82,
    78, 3,
    78, 8,
    79, 3,
    79, 8,
    80, 3,
    80, 8,
    80, 87,
    80, 89,
    80, 90,
    80, 91,
    81, 3,
    81, 8,
    81, 87,
    81, 89,
    81, 90,
    81, 91,
    83, 3,
    83, 8,
    83, 13,
    83, 15,
    83, 66,
    83, 68,
    83, 69,
    83, 70,
    83, 71,
    83, 72,
    83, 80,
    83, 82,
    83, 85,
    83, 87,
    83, 88,
    83, 90,
    83, 110,
    85, 80,
    87, 3,
    87, 8,
    87, 13,
    87, 15,
    87, 66,
    87, 68,
    87, 69,
    87, 70,
    87, 71,
    87, 72,
    87, 80,
    87, 82,
    87, 110,
    88, 13,
    88, 15,
    89, 68,
    89, 69,
    89, 70,
    89, 72,
    89, 80,
    89, 82,
    90, 3,
    90, 8,
    90, 13,
    90, 15,
    90, 66,
    90, 68,
    90, 69,
    90, 70,
    90, 71,
    90, 72,
    90, 80,
    90, 82,
    90, 110,
    91, 68,
    91, 69,
    91, 70,
    91, 72,
    91, 80,
    91, 82,
    92, 43,
    92, 54,
    110, 3,
    110, 8,
    110, 87,
    110, 90
};

/* Kerning between the respective left and right glyphs
 * 4.4 format which needs to scaled with `kern_scale`*/
static const int8_t kern_pair_values[] =
{
    -4, -10, -10, -11, -5, -6, -6, -6,
    -6, -2, -2, -6, -2, -6, -7, 1,
    -5, -10, -10, -11, -5, -6, -6, -6,
    -6, -2, -2, -6, -2, -6, -7, 1,
    -5, 2, 2, 2, -16, -16, -16, -16,
    -21, -11, -11, -6, -1, -1, -1, -1,
    -12, -2, -8, -6, -9, -1, -2, -1,
    -5, -3, -5, 1, -3, -2, -5, -2,
    -3, -1, -2, -10, -10, -2, -3, -2,
    -2, -4, -2, 2, -2, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -22,
    -22, -16, -25, 2, -3, -2, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -3,
    2, -3, 2, -3, 2, -3, 2, -3,
    -2, -6, -3, -3, -3, -3, -2, -2,
    -2, -2, -2, -2, -3, -2, -2, -2,
    -4, -6, -4, -6, -31, -31, 2, -6,
    -6, -6, -6, -26, -5, -16, -13, -22,
    -4, -12, -9, -12, 2, -3, 2, -3,
    2, -3, 2, -3, -10, -10, -2, -3,
    -2, -2, -4, -2, -30, -30, -13, -19,
    -3, -2, -1, -1, -1, -1, -1, -1,
    -1, 1, 1, 1, -1, -4, -3, -2,
    -3, -7, -2, -4, -4, -20, -22, -20,
    -7, -3, -3, -22, -3, -3, -1, 2,
    2, 1, 2, -11, -9, -9, -9, -9,
    -10, -10, -9, -10, -9, -7, -11, -9,
    -7, -5, -7, -7, -6, -22, -11, -2,
    2, -21, -3, -21, -7, -1, -1, -1,
    -1, 2, -4, -4, -4, -4, -4, -4,
    -4, -3, -3, -1, -1, 2, -3, -4,
    1, -12, -6, -12, -4, 1, 1, -3,
    -3, -3, -3, -3, -3, -3, -2, -2,
    1, -6, -3, -4, -2, -2, -2, -2,
    1, -2, -2, -2, -2, -2, -2, -2,
    -3, -3, -4, -3, 2, -5, -20, -5,
    -20, -9, -3, -3, -9, -3, -3, -1,
    2, -9, 2, 2, 1, 2, 2, -7,
    -6, -6, -6, -2, -6, -4, -4, -6,
    -4, -6, -4, -5, -2, -4, -2, -2,
    -2, -3, 2, -5, -7, 1, -2, -2,
    -2, -2, -2, -2, -2, -2, -2, -2,
    -2, -3, -3, -3, -2, -2, -6, -6,
    -1, -1, -3, -3, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, 2, 2,
    2, 2, -2, -2, -2, -2, -2, 2,
    -10, -10, -2, -2, -2, -2, -2, -10,
    -10, -10, -10, -13, -13, -1, -2, -1,
    -1, -3, -3, -1, -1, -1, -1, 2,
    2, -12, -12, -4, -2, -2, -2, 1,
    -2, -2, -2, 5, 2, 2, 2, -4,
    -2, 1, 1, -10, -10, -1, -1, -1,
    -1, 1, -1, -1, -1, -1, -12, -12,
    -2, -2, -2, -2, -2, -2, 1, 1,
    -10, -10, -1, -1, -1, -1, 1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -2, -2, -6, -6, -1, -1
};

/*Collect the kern pair's data in one place*/
static const lv_font_fmt_txt_kern_pair_t kern_pairs =
{
    .glyph_ids = kern_pair_glyph_ids,
    .values = kern_pair_values,
    .pair_cnt = 455,
    .glyph_ids_size = 0
};

/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

/*Store all the custom data of the font*/
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = gylph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .cmap_num = 2,
    .bpp = 2,

    .kern_scale = 16,
    .kern_dsc = &kern_pairs,
    .kern_classes = 0
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
lv_font_t lv_font_roboto_12 = {
    .dsc = &font_dsc,          /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .line_height = 13,          /*The maximum line height required by the font*/
    .base_line = 3,             /*Baseline measured from the bottom of the line*/
};

#endif /*#if LV_FONT_ROBOTO_12*/


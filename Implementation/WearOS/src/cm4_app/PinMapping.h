#ifndef PIN_MAPPING_H
#define PIN_MAPPING_H

#include "Drivers/Gpio.h"


#if BOARD_TYPE == BOARD_TYPE_CYPRESS
/***************************************************\
*                   CYPRESS BOARD                   *
\***************************************************/

// Development Board LEDs
#define CYPRESS_LED_RGB_RED     LOW_ACTIVE(0, 3)
#define CYPRESS_LED_RGB_GREEN   LOW_ACTIVE(1, 1)
#define CYPRESS_LED_RGB_BLUE    LOW_ACTIVE(11, 1)
#define CYPRESS_LED_USER1       LOW_ACTIVE(1, 5)
#define CYPRESS_LED_USER2       LOW_ACTIVE(13, 7)

// Debug UART
#undef  UART_ONE_WIRE
#define UART_ENABLED
#define UART_TX_PIN             PIN(5, 1)
#define UART_RX_PIN             PIN(5, 0)
#define UART_BAUDRATE           115200

// CapSense
#define SLIDER_CMOD             PIN(7, 7)
#define SLIDER_SHIELD           NO_PIN
#define SLIDER_PIN_0            PIN(8, 3)
#define SLIDER_PIN_1            PIN(8, 4)
#define SLIDER_PIN_2            PIN(8, 5)
#define SLIDER_PIN_3            PIN(8, 6)
#define SLIDER_PIN_4            PIN(8, 7)

// Display
#define DISPLAY_DISABLED
#define DISPLAY_COMMAND_PINS        HIGH_ACTIVE(12, 6)
#define DISPLAY_PINS                CYPRESS_LED_USER1
#define DISPLAY_RESET_PINS          NO_PIN
#define DISPLAY_TRANSMISSION_PINS   HIGH_ACTIVE(1, 2)

// GPIO pins
#define CPU_M0_ACTIVE_PINS      NO_PIN
#define CPU_ACTIVE_PINS         CYPRESS_LED_USER2,      HIGH_ACTIVE(12, 4)
#define BUSY_LOOP_PINS          CYPRESS_LED_RGB_RED,    HIGH_ACTIVE(12, 5)
#define MOTOR_PINS              NO_PIN,                 LOW_ACTIVE(10, 0)
#define HEARTRATE_LED_PINS      CYPRESS_LED_RGB_GREEN,  HIGH_ACTIVE(12, 7)
#define HOME_BUTTON_NOTIFY_PINS                         HIGH_ACTIVE(1, 3)
#define RENDERING_PINS                                  NO_PIN
#define RENDER_TRANSFER_PINS                            NO_PIN
#define PROCESSING_FAULT_PINS                           CYPRESS_LED_RGB_RED
#define ISR_DISABLE_PINS                                HIGH_ACTIVE(1, 4)
#define BLE_INDICATOR_PINS      CYPRESS_LED_RGB_BLUE



#elif BOARD_TYPE == BOARD_TYPE_QINNO
/***************************************************\
*                    QINNO BOARD                    *
\***************************************************/

// Development Board LEDs
#define CYPRESS_LED_RGB_RED     NO_PIN
#define CYPRESS_LED_RGB_GREEN   NO_PIN
#define CYPRESS_LED_RGB_BLUE    NO_PIN
#define CYPRESS_LED_USER1       NO_PIN
#define CYPRESS_LED_USER2       NO_PIN

// Debug UART
#define UART_DISABLED
#define UART_ONE_WIRE
#define UART_TX_PIN             PIN(1, 1)
#define UART_RX_PIN             PIN(1, 0)
#define UART_BAUDRATE           115200

// CapSense
#define SLIDER_CMOD             PIN(7, 1)
#define SLIDER_SHIELD           PIN(10, 6)
#define SLIDER_PIN_0            PIN(10, 1)
#define SLIDER_PIN_1            PIN(9, 4)
#define SLIDER_PIN_2            PIN(9, 5)
#define SLIDER_PIN_3            PIN(9, 6)
#define SLIDER_PIN_4            PIN(10, 5)

// Display
#define DISPLAY_ENABLED
#define DISPLAY_COMMAND_PINS        LOW_ACTIVE(11, 1)
#define DISPLAY_PINS                HIGH_ACTIVE(5, 0)
#define DISPLAY_RESET_PINS          LOW_ACTIVE(12, 4)
#define DISPLAY_TRANSMISSION_PINS   NO_PIN

// GPIO pins
#define CPU_M0_ACTIVE_PINS      NO_PIN
#define CPU_ACTIVE_PINS         NO_PIN
#define BUSY_LOOP_PINS          NO_PIN
#define MOTOR_PINS              HIGH_ACTIVE(7, 5)
#define HEARTRATE_LED_PINS      LOW_ACTIVE(1, 3)
#define HOME_BUTTON_NOTIFY_PINS NO_PIN
#define RENDERING_PINS          NO_PIN
#define RENDER_TRANSFER_PINS    NO_PIN
#define PROCESSING_FAULT_PINS   NO_PIN
#define ISR_DISABLE_PINS        NO_PIN
#define BLE_INDICATOR_PINS      NO_PIN

#else
#error "BOARD_TYPE not specified"
#endif


#ifdef DISPLAY_PINS
#define NotifyDisplayOn(DISPLAY_PINS)
#endif


#define INITIALIZE_ALL_DEBUG_PINS() INIT_PIN_ARRAY(CPU_ACTIVE_PINS)             \
                                    INIT_PIN_ARRAY(BUSY_LOOP_PINS)              \
                                    INIT_PIN_ARRAY(MOTOR_PINS)                  \
                                    INIT_PIN_ARRAY(HEARTRATE_LED_PINS)          \
                                    INIT_PIN_ARRAY(DISPLAY_PINS)                \
                                    INIT_PIN_ARRAY(DISPLAY_COMMAND_PINS)        \
                                    INIT_PIN_ARRAY(DISPLAY_RESET_PINS)          \
                                    INIT_PIN_ARRAY(DISPLAY_TRANSMISSION_PINS)   \
                                    INIT_PIN_ARRAY(HOME_BUTTON_NOTIFY_PINS)     \
                                    INIT_PIN_ARRAY(PROCESSING_FAULT_PINS)       \
                                    INIT_PIN_ARRAY(CPU_M0_ACTIVE_PINS)          \
                                    INIT_PIN_ARRAY(RENDERING_PINS)              \
                                    INIT_PIN_ARRAY(RENDER_TRANSFER_PINS)        \
                                    INIT_PIN_ARRAY(BLE_INDICATOR_PINS)          \
                                    INIT_PIN_ARRAY(ISR_DISABLE_PINS)


#define NOTIFY_CPU_ACTIVE()                 SET_PIN_ARRAY(CPU_ACTIVE_PINS)
#define NOTIFY_CPU_INACTIVE()               CLEAR_PIN_ARRAY(CPU_ACTIVE_PINS)

#define NOTIFY_BUSYLOOP_ACTIVE()            SET_PIN_ARRAY(BUSY_LOOP_PINS)
#define NOTIFY_BUSYLOOP_INACTIVE()          CLEAR_PIN_ARRAY(BUSY_LOOP_PINS)

#define NOTIFY_MOTOR_ACTIVE()               SET_PIN_ARRAY(MOTOR_PINS)
#define NOTIFY_MOTOR_INACTIVE()             CLEAR_PIN_ARRAY(MOTOR_PINS)

#define NOTIFY_HEARTRATE_ACTIVE()           SET_PIN_ARRAY(HEARTRATE_LED_PINS)
#define NOTIFY_HEARTRATE_INACTIVE()         CLEAR_PIN_ARRAY(HEARTRATE_LED_PINS)

#define ACTIVATE_DISPLAY()                  SET_PIN_ARRAY(DISPLAY_PINS)
#define DEACTIVATE_DISPLAY()                CLEAR_PIN_ARRAY(DISPLAY_PINS)

#define START_DISPLAY_COMMAND()             SET_PIN_ARRAY(DISPLAY_COMMAND_PINS)
#define END_DISPLAY_COMMAND()               CLEAR_PIN_ARRAY(DISPLAY_COMMAND_PINS)

#define DISPLAY_TRANSMISSION_START()        SET_PIN_ARRAY(DISPLAY_TRANSMISSION_PINS)
#define DISPLAY_TRANSMISSION_END()          CLEAR_PIN_ARRAY(DISPLAY_TRANSMISSION_PINS)

#define START_DISPLAY_RESET()               SET_PIN_ARRAY(DISPLAY_RESET_PINS)
#define END_DISPLAY_RESET()                 CLEAR_PIN_ARRAY(DISPLAY_RESET_PINS)

#define NOTIFY_BUTTON_PRESSED()             SET_PIN_ARRAY(HOME_BUTTON_NOTIFY_PINS)
#define NOTIFY_BUTTON_DEPRESSED()           CLEAR_PIN_ARRAY(HOME_BUTTON_NOTIFY_PINS)

#define NOTIFY_RENDERING_STARTED()          SET_PIN_ARRAY(RENDERING_PINS)
#define NOTIFY_RENDERING_FINISHED()         CLEAR_PIN_ARRAY(RENDERING_PINS)

#define NOTIFY_RENDER_TRANSFER_STARTED()    SET_PIN_ARRAY(RENDER_TRANSFER_PINS)
#define NOTIFY_RENDER_TRANSFER_FINISHED()   CLEAR_PIN_ARRAY(RENDER_TRANSFER_PINS)

#define NOTIFY_PROCESSING_FAULT()           SET_PIN_ARRAY(PROCESSING_FAULT_PINS)

#define NOTIFY_ISR_DISABLED()               SET_PIN_ARRAY(ISR_DISABLE_PINS)
#define NOTIFY_ISR_ENABLED()                CLEAR_PIN_ARRAY(ISR_DISABLE_PINS)

#define NOTIFY_BLE_EVENT_INDICATOR()        SET_PIN_ARRAY(BLE_INDICATOR_PINS)
#define CLEAR_BLE_EVENT_INDICATOR()         CLEAR_PIN_ARRAY(BLE_INDICATOR_PINS)


#endif // PIN_MAPPING_H

#ifndef NOTIFICATION_SERVICE_H
#define NOTIFICATION_SERVICE_H

#include "Common/App.hpp"
#include "Common/AppIds.hpp"
#include "Common/LinkedList.h"
#include "Notification.hpp"

#define MAX_NOTIFICATIONS (20)
#define MAX_NOTIFICATIONS_IN_APP (10)

class INotificationServiceUpdateHandler
{
    friend class NotificationService;

private:
    INotificationServiceUpdateHandler* next_ = nullptr;

public:
    virtual ~INotificationServiceUpdateHandler() {};

    virtual void OnNotificationSourceUpdate() = 0;
};

class NotificationService
{
private:
    App* notificationApp_ = nullptr;

    uint16_t uniqueIdCounter_ = 1;

    uint16_t notificationCursor_ = 0;
    LinkedList<Notification, MAX_NOTIFICATIONS> notifications_;

    INotificationServiceUpdateHandler* updateHandlerList_ = nullptr;

    void FireNotificationUpdate();

protected:
public:
    NotificationService();

    void AddNotificationServiceUpdateHandler(INotificationServiceUpdateHandler* handler);
    void RemoveNotificationServiceUpdateHandler(INotificationServiceUpdateHandler* handler);

    void SetNotificationApp(App* app) { notificationApp_ = app; }

    Notification* AddNotification(AppIds appId, const char* title, const char* message);
    Notification* AddNotification(AppIds appId, const char* title, uint16_t titleLength, const char* message, uint16_t messageLength);
    Notification* AddNotification(AppIds appId);

    void PresentLatestNotification(uint32_t identifier);

    uint16_t GetUnreadNotificationCount(AppIds appId);
    uint16_t GetNotificationCount(AppIds appId);

    void SendPositiveAction(uint32_t identifier);
    void SendNegativeAction(uint32_t identifier);

    Notification* GetNotification(uint32_t id);
    Notification* GetLatestNotification();
    uint16_t GetNotifications(AppIds appId, Notification** notificationArray, uint16_t arraySize);
    Notification* GetNotification(uint16_t index, AppIds appId = AppIds::Any);
    void RemoveNotification(uint32_t identifier);
    void RemoveAllNotifications();
    void MarkRead(uint32_t identifier);
    Notification* GetIncomplete();
};

#endif // NOTIFICATION_SERVICE_H

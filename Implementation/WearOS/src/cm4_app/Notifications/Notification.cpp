#include "Notification.hpp"
#include "AppSystemM4.hpp"
#include "Common/Utils.h"
#include "NotificationService.hpp"
#include "Services/VibrationService.hpp"

bool Notification::IsComplete()
{
    return Title[0] != '\0'
        && Message[0] != '\0'
        && AppIdentifier[0] != '\0'
        && Active == true;
}

void Notification::SetTitle(const char* title)
{
    bool wasComplete = IsComplete();
    Title[NOTIFICATION_TITLE_LENGTH - 1] = '\0';
    StringCopySafe(title, Title, NOTIFICATION_TITLE_LENGTH);
    PresentNotification(wasComplete);
}

void Notification::SetMessage(const char* message)
{
    bool wasComplete = IsComplete();
    Message[NOTIFICATION_MESSAGE_LENGTH - 1] = '\0';
    StringCopySafe(message, Message, NOTIFICATION_MESSAGE_LENGTH);
    PresentNotification(wasComplete);
}

void Notification::SetAppIdentifier(const char* appId)
{
    bool wasComplete = IsComplete();
    AppIdentifier[NOTIFICATION_APPID_LENGTH - 1] = '\0';
    StringCopySafe(appId, AppIdentifier, NOTIFICATION_APPID_LENGTH);
    PresentNotification(wasComplete);
}

void Notification::SetTitle(const char* title, const uint16_t titleLength)
{
    bool wasComplete = IsComplete();
    Title[NOTIFICATION_TITLE_LENGTH - 1] = '\0';
    StringCopySafe(title, Title, titleLength, NOTIFICATION_TITLE_LENGTH);
    PresentNotification(wasComplete);
}

void Notification::SetMessage(const char* message, const uint16_t sourceLength)
{
    bool wasComplete = IsComplete();
    Message[NOTIFICATION_MESSAGE_LENGTH - 1] = '\0';
    StringCopySafe(message, Message, sourceLength, NOTIFICATION_MESSAGE_LENGTH);
    PresentNotification(wasComplete);
}

void Notification::SetAppIdentifier(const char* appId, const uint16_t sourceLength)
{
    bool wasComplete = IsComplete();
    AppIdentifier[NOTIFICATION_APPID_LENGTH - 1] = '\0';
    StringCopySafe(appId, AppIdentifier, sourceLength, NOTIFICATION_APPID_LENGTH);
    PresentNotification(wasComplete);
}

void Notification::SetHasPositiveAction(bool value)
{
    HasPositiveAction = value;
}

void Notification::SetHasNegativeAction(bool value)
{
    HasNegativeAction = value;
}

void Notification::PresentNotification(bool wasComplete)
{
    if (!IsComplete() || wasComplete)
    {
        return;
    }

    LOG("Message received - Title: %s - Message: %s", Title, Message)
    VibrationService.Vibrate(VIBRATE_NOTIFICATION);
    auto notificationService = appSystem_.GetNotificationService();
    notificationService->PresentLatestNotification(Identifier);
}

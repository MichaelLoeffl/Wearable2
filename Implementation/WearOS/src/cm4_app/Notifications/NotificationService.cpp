#include "NotificationService.hpp"
#include "AppSystemM4.hpp"
#include "Drivers/BleCommunication.h"
#include "Common/Utils.h"

NotificationService notificationService_;

NotificationService::NotificationService()
{
}

void NotificationService::AddNotificationServiceUpdateHandler(INotificationServiceUpdateHandler* handler)
{
    RemoveNotificationServiceUpdateHandler(handler);
    handler->next_     = updateHandlerList_;
    updateHandlerList_ = handler;
}

void NotificationService::RemoveNotificationServiceUpdateHandler(INotificationServiceUpdateHandler* handler)
{
    INotificationServiceUpdateHandler** current = &updateHandlerList_;

    while (*current != nullptr)
    {
        if (*current == handler)
        {
            *current       = handler->next_;
            handler->next_ = nullptr;
            return;
        }

        current = &((*current)->next_);
    }

    return;
}

void NotificationService::FireNotificationUpdate()
{
    INotificationServiceUpdateHandler* current = updateHandlerList_;

    while (current != nullptr)
    {
        current->OnNotificationSourceUpdate();
        current = current->next_;
    }

    return;
}

Notification* NotificationService::AddNotification(AppIds appId)
{
    uniqueIdCounter_++;
    Notification notification;
    memset(notification.Title, '\0', NOTIFICATION_TITLE_LENGTH);
    memset(notification.Message, '\0', NOTIFICATION_MESSAGE_LENGTH);
    notification.Active     = true;
    notification.Timestamp  = 0;
    notification.AppId      = appId;
    notification.Identifier = uniqueIdCounter_;

    notifications_.Push(notification, true);

    if (appId == AppIds::IncomingCall)
    {
        notification.IsModal = true;
    }
    return &notifications_[0];
}

Notification* NotificationService::AddNotification(AppIds appId, const char* title, const char* message)
{
    auto notification = AddNotification(appId);

    if (notification == nullptr)
    {
        return 0;
    }

    notification->SetTitle(title);
    notification->SetMessage(message);
    notification->SetAppIdentifier("internal");

    return notification;
}

Notification* NotificationService::AddNotification(AppIds appId, const char* title, uint16_t titleLength, const char* message, uint16_t messageLength)
{
    auto notification = AddNotification(appId);

    if (notification == nullptr)
    {
        return 0;
    }

    notification->SetTitle(title, titleLength);
    notification->SetMessage(message, messageLength);
    notification->SetAppIdentifier("internal");

    return notification;
}

void NotificationService::PresentLatestNotification(uint32_t identifier)
{
    FireNotificationUpdate();

    auto notification = GetNotification(identifier);
    if (notification == nullptr || !notification->IsComplete())
    {
        return;
    }
    if (notificationApp_ == nullptr)
    {
        return;
    }

    appSystem_.StartApp(*notificationApp_);
}

uint16_t NotificationService::GetNotificationCount(AppIds appId)
{
    size_t countAll = notifications_.Count();
    size_t count    = 0;
    for (size_t i = 0; i < countAll; i++)
    {
        Notification* notification = &notifications_[i];
        if (notification->IsComplete())
        {
            if ((notification->AppId & appId) != AppIds::Unknown)
            {
                count++;
            }
        }
    }
    return count;
}

Notification* NotificationService::GetNotification(uint32_t id)
{
    for (size_t index = 0; index < notifications_.Count(); index++)
    {
        Notification* notification = &notifications_[index];
        if (notification->Identifier == id)
        {
            return notification;
        }
    }

    return nullptr;
}

Notification* NotificationService::GetLatestNotification()
{
    size_t countAll = notifications_.Count();
    for (size_t i = 0; i < countAll; i++)
    {
        Notification* notification = &notifications_[i];
        if (notification->IsComplete())
        {
            return notification;
        }
    }
    return nullptr;
}

uint16_t NotificationService::GetNotifications(AppIds appId, Notification** notificationArray, uint16_t arraySize)
{
    uint16_t countAll = notifications_.Count();
    uint16_t count    = 0;
    for (uint16_t i = 0; i < countAll && count < arraySize; i++)
    {
        Notification* notification = &notifications_[i];
        if (notification->IsComplete())
        {
            if ((notification->AppId & appId) != AppIds::Unknown)
            {
                notificationArray[count] = notification;
                count++;
            }
        }
    }
    return count;
}

Notification* NotificationService::GetNotification(uint16_t index, AppIds appId)
{
    uint16_t count = 0;
    for (uint16_t i = 0; i < notifications_.Count(); i++)
    {
        Notification* notification = &notifications_[i];
        if (notification->IsComplete() && (notification->AppId & appId) != AppIds::Unknown)
        {
            if (index == count)
            {
                return notification;
            }
            count++;
        }
    }
    return nullptr;
}

uint16_t NotificationService::GetUnreadNotificationCount(AppIds appId)
{
    size_t countAll = notifications_.Count();
    size_t count    = 0;
    for (size_t i = 0; i < countAll; i++)
    {
        Notification* notification = &notifications_[i];
        if (notification->IsNew && ((notification->AppId & appId) != AppIds::Unknown))
        {
            count++;
        }
    }
    return count;
}

Notification* NotificationService::GetIncomplete()
{
    size_t countAll = notifications_.Count();
    for (size_t i = 0; i < countAll; i++)
    {
        Notification* notification = &notifications_[i];
        if (!notification->IsComplete())
        {
            return notification;
        }
    }
    return nullptr;
}

void NotificationService::RemoveNotification(uint32_t identifier)
{
    size_t countAll = notifications_.Count();
    for (size_t i = 0; i < countAll; i++)
    {
        Notification* notification = &notifications_[i];
        if (notification->Identifier == identifier)
        {
            notifications_[i].Active = false;
            notifications_.Remove(i);
            FireNotificationUpdate();
            return;
        }
    }
}

void NotificationService::RemoveAllNotifications()
{
    size_t countAll = notifications_.Count();
    for (size_t i = 0; i < countAll; i++)
    {
        if (true)
        {
            notifications_[i].Active = false;
            notifications_.Remove(i);
            i--;
            countAll--;
        }
    }
    FireNotificationUpdate();
}

void NotificationService::MarkRead(uint32_t identifier)
{
    size_t countAll = notifications_.Count();
    for (size_t i = 0; i < countAll; i++)
    {
        Notification* notification = &notifications_[i];
        if (notification->Identifier == identifier)
        {
            notification->IsNew = false;
        }
    }
}

void NotificationService::SendPositiveAction(uint32_t identifier)
{
    SendPositiveNotificationAction(identifier);
}

void NotificationService::SendNegativeAction(uint32_t identifier)
{
    SendNegativeNotificationAction(identifier);
}

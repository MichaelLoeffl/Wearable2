#ifndef SOURCES_M4_NOTIFICATIONS_NOTIFICATION_H
#define SOURCES_M4_NOTIFICATIONS_NOTIFICATION_H

#include "Common/AppIds.hpp"
#include <stdint.h>

#define NOTIFICATION_TITLE_LENGTH (32)
#define NOTIFICATION_MESSAGE_LENGTH (128)
#define NOTIFICATION_APPID_LENGTH (64)

class Notification
{
public:
    uint32_t Identifier                           = 0;
    char Title[NOTIFICATION_TITLE_LENGTH]         = {0};
    char Message[NOTIFICATION_MESSAGE_LENGTH]     = {0};
    char AppIdentifier[NOTIFICATION_APPID_LENGTH] = {0};
    bool HasPositiveAction = false;
    bool HasNegativeAction = false;
    AppIds AppId;
    bool IsNew   = true;
    bool IsModal = false;
    uint64_t Timestamp;
    bool Active;

    bool IsComplete();

    void SetTitle(const char* title);
    void SetMessage(const char* message);
    void SetAppIdentifier(const char* appId);

    void SetTitle(const char* title, const uint16_t titleLength);
    void SetMessage(const char* message, const uint16_t sourceLength);
    void SetAppIdentifier(const char* appId, const uint16_t sourceLength);

    void SetHasPositiveAction(bool value);
    void SetHasNegativeAction(bool value);
private:
    void PresentNotification(bool wasComplete);
};

#endif /* SOURCES_M4_NOTIFICATIONS_NOTIFICATION_H */

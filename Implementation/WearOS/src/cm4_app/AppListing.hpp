
#ifdef APP_SYSTEM_DEFINE_APPS

#include "Apps/Communication/EmailApp.hpp"
#include "Apps/Communication/NotificationModalApp.hpp"
#include "Apps/Communication/NotificationsApp.hpp"
#include "Apps/Communication/PhoneApp.hpp"
#include "Apps/Health/FitnessApp.hpp"
#include "Apps/Health/HeartrateApp.hpp"
#include "Apps/Health/StepsApp.hpp"
#include "Apps/Launcher.hpp"
#include "Apps/Navigation/NavigationApp.hpp"
#include "Apps/Payment/PaymentApp.hpp"
#include "Apps/Productivity/CalendarApp.hpp"
#include "Apps/Productivity/ReminderApp.hpp"
#include "Apps/Settings/AboutApp.hpp"
#include "Apps/Settings/EnergyApp.hpp"
#include "Apps/Settings/FlightModeApp.hpp"
#include "Apps/Settings/PairingApp.hpp"
#include "Apps/Settings/SettingsApp.hpp"
#include "Apps/System/PowerMenuApp.hpp"
#include "Apps/TestingPlayground/PlaygroundApp.hpp"
#include "Apps/Tools/AltitudeApp.hpp"
#include "Apps/Tools/CameraApp.hpp"
#include "Apps/Watch/AlarmApp.hpp"
#include "Apps/Watch/DateApp.hpp"
#include "Apps/Watch/StopwatchApp.hpp"
#include "Apps/Watch/TimerApp.hpp"
#include "Apps/Watch/WatchApp.hpp"
#include "Apps/Watch/WatchLocalApp.hpp"
#include "Apps/Watch/WatchWorldApp.hpp"

#define DECLARE_APP(x) x x##_;

#elif defined(APP_SYSTEM_REGISTER_APPS)
#define DECLARE_APP(x) RegisterApp(x##_, #x);
#else
#define DECLARE_APP(x) \
    class x;           \
    extern x x##_;
#endif

DECLARE_APP(Launcher)

DECLARE_APP(NotificationModalApp)
DECLARE_APP(PowerMenuApp)

// Launcher Apps
DECLARE_APP(WatchApp)

DECLARE_APP(ReminderApp)
DECLARE_APP(PaymentApp)
#ifdef DEBUG
DECLARE_APP(PlaygroundApp)
#endif
DECLARE_APP(FitnessApp)
DECLARE_APP(NotificationsApp)
DECLARE_APP(EmailApp)
DECLARE_APP(PhoneApp)
DECLARE_APP(CalendarApp)
DECLARE_APP(NavigationApp)
DECLARE_APP(HeartrateApp)
DECLARE_APP(AltitudeApp)
DECLARE_APP(StepsApp)
DECLARE_APP(CameraApp)

// Watch Apps
DECLARE_APP(WatchLocalApp)
DECLARE_APP(WatchWorldApp)
DECLARE_APP(AlarmApp)
DECLARE_APP(StopwatchApp)
DECLARE_APP(TimerApp)
DECLARE_APP(DateApp)

// Settings Apps
DECLARE_APP(SettingsApp)
DECLARE_APP(EnergyApp)
DECLARE_APP(FlightModeApp)
DECLARE_APP(AboutApp)

// System Apps
DECLARE_APP(PairingApp)

// Unfinished apps
// DECLARE_APP(BarcodeApp)
// DECLARE_APP(WeatherApp)

#undef DECLARE_APP
#undef APP_SYSTEM_DEFINE_APPS

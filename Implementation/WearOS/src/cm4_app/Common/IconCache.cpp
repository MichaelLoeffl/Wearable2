#include "IconCache.hpp"
#include "Common/Common.h"

static uint8_t iconCache[ICON_CACHE_ICON_SIZE * ICON_CACHE_ICON_COUNT];

IconCache IconCacheInstance;

CFUNC const void* GetImageFromCache(const void* original)
{
    return (const void*)IconCacheInstance.GetIconFromCache((const lv_img_dsc_t*)original);
}

const lv_img_dsc_t* IconCache::GetIconFromCache(const lv_img_dsc_t* original)
{
    if (original == nullptr || original->data_size > ICON_CACHE_ICON_SIZE || cacheDisabled_)
    {
        return original;
    }

    uint16_t min               = 0xFFFF;
    uint16_t index             = 0;
    const lv_img_dsc_t* result = nullptr;
    for (int i = 0; i < ICON_CACHE_ICON_COUNT; i++)
    {
        if (cache[i].original == nullptr)
        {
            cache[i].Usage = 0;
        }
        else if (cache[i].original == original || original == &cache[i].icondesc)
        {
            if (cache[i].Usage < ICON_CACHE_ICON_COUNT * ICON_CACHE_ICON_COUNT)
            {
                cache[i].Usage += ICON_CACHE_ICON_COUNT;
            }
            result = &cache[i].icondesc;
        }
        else if (cache[i].Usage > 0)
        {
            cache[i].Usage--;
        }

        if (cache[i].Usage < min)
        {
            min   = cache[i].Usage;
            index = i;
        }
    }

    if (result == nullptr)
    {
        cache[index].Usage    = ICON_CACHE_ICON_COUNT;
        cache[index].original = original;
        cache[index].icondesc = *original;

        uint8_t* position = &iconCache[ICON_CACHE_ICON_SIZE * index];
        memcpy(position, original->data, original->data_size);
        cache[index].icondesc.data = position;
        result                     = &cache[index].icondesc;
    }

    return result;
}
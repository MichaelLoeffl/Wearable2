#include "Common/Common.h"

template <typename T, size_t BUFFER_SIZE>
class RingBuffer
{
private:
    T elements_[BUFFER_SIZE];
    T* read_;
    T* write_;
    size_t count_;

    size_t GetReadableChunkLength()
    {
        size_t count = count_;
        size_t position = read_ - elements_;
        if (position + count > BUFFER_SIZE)
        {
            return BUFFER_SIZE - position;
        }
        return count;
    }

    size_t GetWriteableChunkLength()
    {
        size_t count = BUFFER_SIZE - count_;
        size_t position = write_ - elements_;
        if (position + count > BUFFER_SIZE)
        {
            return BUFFER_SIZE - position;
        }
        return count;
    }

public:
    RingBuffer()
    {
        read_ = elements_;
        write_ = elements_;
        count_ = 0;
    }

    T Peek()
    {
        return *read_;
    }

    const T* PeekBuffer(size_t* length)
    {
        *length = GetReadableChunkLength();
        return read_;
    }

    T* Lock(size_t* length)
    {
        *length = GetWriteableChunkLength();
        return write_;
    }

    bool Unlock(size_t count)
    {
        if (count < 0)
        {
            return false;
        }

        if (count > Empty())
        {
            return false;
        }

        size_t position = write_ - elements_;
        position = (position + count) % BUFFER_SIZE;
        write_ = &elements_[position];
        count_ += count;
        return true;
    }

    bool TakeOne(T* output)
    {
        if (count_ == 0)
        {
            return false;
        }
         *output = Peek();
        return Drop(1);
    }

    bool Drop(size_t count)
    {
        if (count > count_)
        {
            return false;
        }

        count_ -= count;
        size_t position = read_ - elements_;
        position = (position + count) % BUFFER_SIZE;
        read_ = &elements_[position];
        return true;
    }

    bool Push(T element)
    {
        if (count_ >= BUFFER_SIZE)
        {
            return false;
        }

        *write_ = element;
        write_++;
        count_++;
        return true;
    }

    size_t Push(const T* elements, size_t count)
    {
        if (count <= 0)
        {
            return 0;
        }

        auto amount = count;
        if (amount > BUFFER_SIZE - count_)
        {
            amount = BUFFER_SIZE - count_;
        }

        for (size_t i = 0; i < amount; i++)
        {
            *write_ = elements[i];
            write_++;
            if (write_ == &elements_[BUFFER_SIZE])
            {
                write_ = elements_;
            }
        }

        count_ += amount;
        return amount;
    }

    size_t Count()
    {
        return count_;
    }

    size_t Empty()
    {
        return BUFFER_SIZE - count_;
    }
};

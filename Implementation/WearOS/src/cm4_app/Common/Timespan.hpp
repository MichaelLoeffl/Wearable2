#ifndef TIMESPAN_H_615B2F
#define TIMESPAN_H_615B2F

#include "DateTime.hpp"

class Timespan
{
    friend void FlattenTimespan(Timespan* timespan);
    friend DateTime TimespanToDate(const Timespan& source);

    friend Timespan operator+(const Timespan& a, const Timespan& b);
    friend Timespan operator-(const Timespan& a, const Timespan& b);
    friend Timespan operator-(const Timespan& a);

    friend bool operator>(const Timespan& a, const Timespan& b);
    friend bool operator>=(const Timespan& a, const Timespan& b);
    friend bool operator<(const Timespan& a, const Timespan& b);
    friend bool operator<=(const Timespan& a, const Timespan& b);
    friend bool operator==(const Timespan& a, const Timespan& b);
    friend bool operator!=(const Timespan& a, const Timespan& b);


private:
    int32_t milliseconds_;
    int32_t seconds_;
    int32_t minutes_;
    int32_t hours_;
    int32_t days_;

public:
    Timespan(int32_t milliseconds);
    Timespan(int32_t seconds, int32_t milliseconds);
    Timespan(int32_t minutes, int32_t seconds, int32_t milliseconds);
    Timespan(int32_t hours, int32_t minutes, int32_t seconds, int32_t milliseconds);
    Timespan(int32_t days, int32_t hours, int32_t minutes, int32_t seconds, int32_t milliseconds);

    int32_t TotalMilliseconds() const;
    int32_t TotalSeconds() const;
    int32_t TotalMinutes() const;
    int32_t TotalHours() const;
    int32_t TotalDays() const;
};

Timespan DateToTimespan(const DateTime& source);
DateTime TimespanToDate(const Timespan& source);

Timespan operator+(const Timespan& a, const Timespan& b);
Timespan operator-(const Timespan& a, const Timespan& b);
Timespan operator-(const Timespan& a);

DateTime operator+(const DateTime& date, const Timespan& timespan);
DateTime operator+=(const DateTime& date, const Timespan& timespan);
DateTime operator-(const DateTime& date, const Timespan& timespan);
DateTime operator-=(const DateTime& date, const Timespan& timespan);

Timespan operator-(const DateTime& dateA, const DateTime& dateB);


bool operator>(const Timespan& a, const Timespan& b);
bool operator>=(const Timespan& a, const Timespan& b);
bool operator<(const Timespan& a, const Timespan& b);
bool operator<=(const Timespan& a, const Timespan& b);
bool operator==(const Timespan& a, const Timespan& b);
bool operator!=(const Timespan& a, const Timespan& b);

#endif /* TIMESPAN_H_615B2F */
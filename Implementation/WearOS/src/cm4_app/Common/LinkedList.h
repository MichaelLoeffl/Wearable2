#ifndef LINKEDLIST_H_614E92
#define LINKEDLIST_H_614E92

#include <cstdint>

template<typename type, uint16_t size> class LinkedList
{
private:
    struct DoublyLinkedListElement
    {
        type Element;
        DoublyLinkedListElement* Last;
        DoublyLinkedListElement* Next;
    };

public:
    LinkedList()
    {
        for (uint16_t i = 0; i < size; i++)
        {
            elements_[i].Next = &elements_[(i + 1) % size];
            elements_[i].Last = &elements_[(i + size - 1) % size];
        }
        count_ = 0;

        first_ = elements_;
        last_  = elements_;
    }

    uint16_t Count() { return count_; }

    type& GetLast() { return last_->Element; }

    type& GetFirst() { return first_->Element; }

    bool Push(const type& element, bool force = false) { return PushFront(element, force); }

    bool PushFront(const type& element, bool force = false)
    {
        if (count_ == size)
        {
            if (force)
            {
                last_->Element = element;
                first_         = last_;
                last_          = first_->Last;
            }
            return false;
        }

        first_->Last->Element = element;
        first_                = first_->Last;

        if (count_ == 0)
        {
            last_ = first_;
        }
        count_++;

        return true;
    }

    bool PushBack(const type& element, bool force = false)
    {
        if (count_ == size)
        {
            if (force)
            {
                last_->Element = element;
            }
            return false;
        }

        last_->Next->Element = element;
        last_                = last_->Next;

        if (count_ == 0)
        {
            first_ = last_;
        }
        count_++;

        return true;
    }

    bool Pop() { return PopFront(); }

    bool PopFront()
    {
        if (count_ == 0)
        {
            return false;
        }

        first_ = first_->Next;
        count_--;
        return true;
    }

    bool PopBack()
    {
        if (count_ == 0)
        {
            return false;
        }

        last_ = last_->Last;
        count_--;
        return true;
    }

    bool Insert(uint16_t index, const type& item)
    {
        auto result = InsertOpenSpace(index);
        if (result == nullptr)
        {
            return false;
        }

        result->Element = item;

        return true;
    }

    bool Remove(uint16_t index)
    {
        if (index >= count_ || count_ == 0)
        {
            return false;
        }

        DoublyLinkedListElement* current = first_;
        for (uint16_t i = 0; i < index; i++)
        {
            current = current->Next;
        }

        current->Last->Next = current->Next;
        current->Next->Last = current->Last;

        if (current == last_)
        {
            last_ = current->Last;
        }

        if (current == first_)
        {
            first_ = current->Next;
        }

        current->Next = last_->Next;
        current->Last = last_;

        last_->Next         = current;
        current->Next->Last = current;
        count_--;

        if (count_ == 0)
        {
            last_ = first_;
        }

        return false;
    }

    type& operator[](uint16_t index)
    {
        if (index >= count_)
        {
            return last_->Element;
        }

        DoublyLinkedListElement* current = first_;
        for (uint16_t i = 0; i < index; i++)
        {
            current = current->Next;
        }
        return current->Element;
    }

private:
    DoublyLinkedListElement* InsertOpenSpace(uint16_t index)
    {
        if (count_ == size || index >= size)
        {
            return nullptr;
        }

        DoublyLinkedListElement* newSlot = first_->Last;
        if (index == 0)
        {
            first_ = newSlot;
            if (count_ == 0)
            {
                last_ = first_;
            }
            count_++;
            return newSlot;
        }

        newSlot       = last_->Next;
        auto previous = first_;
        for (int i = 1; i < index; i++)
        {
            previous = previous->Next;
        }

        newSlot->Next->Last = newSlot->Last;
        newSlot->Last->Next = newSlot->Next;

        newSlot->Next       = previous->Next;
        newSlot->Last       = previous;
        previous->Next      = newSlot;
        newSlot->Next->Last = newSlot;

        count_++;

        return newSlot;
    }

    DoublyLinkedListElement elements_[size];
    DoublyLinkedListElement* first_;
    DoublyLinkedListElement* last_;
    uint16_t count_;
};

#endif /* LINKEDLIST_H_614E92 */

#ifndef APP_H_1EE644
#define APP_H_1EE644

#include "AppFolders.hpp"
#include "AppIds.hpp"
#include "Common/Common.h"
#include "Tools.hpp"

#define APP_QUEUE_ALL_APPS (0)
#define APP_QUEUE_STACK (1)

#include "lvgl/src/lv_core/lv_obj.h"
#include "lvgl/src/lv_draw/lv_draw_img.h"

class Graphics;
class AppSystem;
class App;

template<int T> class LinkQueue;

template<int T> class AppSlot
{
public:
    AppSlot<T>* Next;
    AppSlot<T>* Last;
    LinkQueue<T>* Queue;

protected:
    AppSlot()
    {
        Next  = (AppSlot<T>*)nullptr;
        Last  = (AppSlot<T>*)nullptr;
        Queue = (LinkQueue<T>*)nullptr;
    }
};

class App : public AppSlot<APP_QUEUE_ALL_APPS>, public AppSlot<APP_QUEUE_STACK>
{
    friend class AppSystem;

private:
    AppSystem* system_;
    void* context_ = nullptr;
    const char* name_ = "";

    bool notificationActive_ = false;

    AppIds appId_         = AppIds::Unknown;
    lv_obj_t* badgeImage_ = nullptr;

    uint32_t lastRamUsage_;

public:
    virtual ~App() {}

    AppSystem* System() { return system_; }

    bool Active();

    virtual void Draw() {}
    virtual void SecondElapsed() {}
    virtual void BackPressed();
    virtual void BackLongPressed();

    virtual AppIds AppId() { return appId_; }
    virtual int GetPriority() { return 0; }
    virtual AppFolders AppFolder() { return AppFolders::Launcher; }
    virtual bool IgnoreRotation() { return false; }

    virtual void Invalidate();

    virtual int16_t GetScrollPosition();

    virtual void SetBadge(bool active);
    virtual void SetBadgeImage(lv_obj_t* notificationImage)
    {
        badgeImage_ = notificationImage;
        if (badgeImage_ != nullptr)
        {
            SetBadge(notificationActive_);
        }
    }

    virtual lv_img_dsc_t* GetAppImage() { return Tools::GetIcon(appId_); }

    void SetContext(void* context) { context_ = context; }
    void* GetContext() { return context_; }

protected:
    lv_obj_t* page_;

    App(AppIds appId)
    {
        appId_  = appId;
        system_ = (AppSystem*)nullptr;

        page_ = (lv_obj_t*)nullptr;
        lastRamUsage_ = 0;
    }

    void OnInitialize();
    void OnActivate();
    void OnDeactivate();

    void SetPageSize(int width);
    void EnableListLayout();

    void AdjustRotation(bool forceDefault = false);

    virtual void Restore() {}
    virtual void Activated() {}
    virtual void Deactivated() {}
    virtual void Shutdown() {}

    virtual bool KeepAlive() { return false; }

    virtual void Initialize() = 0;
};

#endif /* APP_H_1EE644 */

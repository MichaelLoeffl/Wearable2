#ifndef GRAPHICS_H_9BAF1D
#define GRAPHICS_H_9BAF1D

#include "Common/Common.h"
#include "Drivers/Display.h"

#define COLORRANGE(x) (x >> 2)
#define READ_R (COLORRANGE(r))
#define READ_G (COLORRANGE(g))
#define READ_B (COLORRANGE(b))

class Graphics
{
    void* displayBuffer_;

public:
    Graphics(void* displayBuffer);

    void Clear(uint8_t r, uint8_t g, uint8_t b)
    {
        /*uint8_t* pix = displayBuffer_;
        for (int i = DISPLAY_WIDTH * DISPLAY_HEIGHT; i > 0; i--)
        {
            *(pix++) = READ_R;
            *(pix++) = READ_G;
            *(pix++) = READ_B;
        }*/
    }

    void Clear(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
    {
        /*uint32_t c;
        uint8_t* pix = &displayBuffer_[DISPLAY_WIDTH * DISPLAY_HEIGHT * 3 - 1];
        uint8_t inva = 255 - a;

        for (; pix >= displayBuffer_; )
        {
            c = (((*pix) * inva) + READ_R * a) / 255;
            *(pix--) = (uint8_t)c;
            c = (((*pix) * inva) + READ_G * a) / 255;
            *(pix--) = (uint8_t)c;
            c = (((*pix) * inva) + READ_B * a) / 255;
            *(pix--) = (uint8_t)c;
        }*/
    }

    __inline void SetPixel(uint16_t x, uint16_t y, uint8_t r, uint8_t g, uint8_t b)
    {
        /*if (x >= DISPLAY_WIDTH)
        {
            x = 5;
        }

        if (y >= DISPLAY_HEIGHT)
        {
            y = 5;
        }
        uint8_t* pix = &(displayBuffer_[(x + DISPLAY_WIDTH * y) * 3]);

        *(pix++) = READ_R;
        *(pix++) = READ_G;
        *pix = READ_B;*/
    }
};

#endif /* GRAPHICS_H_9BAF1D */

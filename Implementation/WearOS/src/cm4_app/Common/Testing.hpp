#ifndef TESTFRAMEWORK_TEST_HPP
#define TESTFRAMEWORK_TEST_HPP

void TestWrite(const char* text);

namespace Testing
{
    class TestSuite;
}
extern Testing::TestSuite TestSuiteRoot;
extern Testing::TestSuite* suiteHandle_;

namespace Testing
{
    extern const char* TTF_CURRENT_NAME;

    struct TestResult {
        int line;
        bool success;
        const char *failedMessage;
        const char *failedCheck;
    };

    typedef void (*TTF_TestFunction)(TestResult &);

    class TestFunctionInfo
    {
        friend class TestSuite;
    private:
        TestFunctionInfo *sibling;

        void Run(const char* filter);
        void Print(bool printResults, int indentation, const char* filter);
    public:
        const char *name;
        const char *file;
        int line;
        TTF_TestFunction function;
        TestResult testResult;
    };

    struct SetupTeardownFunctionInfo
    {
        friend class TestSuite;
    private:
        void Run();
        void Print(bool printResults, int indentation);
    public:
        bool isSetup;
        const char *file;
        int line;
        TTF_TestFunction function;
        TestResult testResult;
    };

    typedef void (*InitializerFunc)();

    class Context {
    public:
        Context(const char *name) {
            TTF_CURRENT_NAME = name;
        }
    };

    class Internals_
    {
    public:
        static void Register(TestSuite* suite, TestFunctionInfo &testFunctionInfo);
        static void Register(TestSuite* suite, SetupTeardownFunctionInfo &functionInfo);
    };

    class TestSuite {
        friend class Internals_;
    private:
        const char *name = nullptr;

        bool success = false;

        TestSuite *parent_ = nullptr;
        TestSuite *sibling_ = nullptr;
        TestSuite *firstChild_ = nullptr;

        SetupTeardownFunctionInfo *setup_ = nullptr;
        TestFunctionInfo *firstTest_ = nullptr;
        SetupTeardownFunctionInfo *teardown_ = nullptr;

        void Register(TestSuite *child);
        void Print(bool printResults, int indentation, const char* filter);
        void RunInternal(const char *filter);

        void Register(TestFunctionInfo &testFunctionInfo);
        void Register(SetupTeardownFunctionInfo &functionInfo);

    public:

        const char *getName() {
            if (name == nullptr) {
                return "";
            }
            return name;
        }

        TestSuite() = default;
        TestSuite(InitializerFunc f);

        void Run(const char *filter);
        void Run();
    };
}

#define TTF_CONCAT_IMPL(s1,s2) s1##s2
#define TTF_CONCAT(s1,s2) TTF_CONCAT_IMPL(s1,s2)
#define TTF_ANONYMOUS(name) TTF_CONCAT(name,__COUNTER__)

/*******************************************************\
        TEST CASE
\*******************************************************/

#define TTF_TEST_CASE_IMPL(testCaseName, funcName, fileName, caseLine)          \
    static Testing::TestFunctionInfo funcName;              \
    funcName.name = testCaseName;                           \
    funcName.file = fileName;                               \
    funcName.line = caseLine;                               \
    Testing::Internals_::Register(suiteHandle_, funcName); \
    funcName.function = [](Testing::TestResult& testContext_)

#define TEST_CASE(name) TTF_TEST_CASE_IMPL(#name, name, __FILE__, __LINE__)

/*******************************************************\
        SETUP
\*******************************************************/

#define TTF_SETUP_IMPL(fileName, caseLine)                                      \
    static Testing::SetupTeardownFunctionInfo setupFunction;                    \
    setupFunction.file = fileName;                                              \
    setupFunction.line = caseLine;                                              \
    setupFunction.isSetup = true;                                               \
    Testing::Internals_::Register(suiteHandle_, setupFunction);                \
    setupFunction.function = [](Testing::TestResult& testContext_)

#define SETUP() TTF_SETUP_IMPL(__FILE__, __LINE__)

/*******************************************************\
        TEARDOWN
\*******************************************************/

#define TTF_TEARDOWN_IMPL(fileName, caseLine)                                       \
    static Testing::SetupTeardownFunctionInfo teardownFunction;                     \
    teardownFunction.file = fileName;                                               \
    teardownFunction.line = caseLine;                                               \
    teardownFunction.isSetup = false;                                               \
    Testing::Internals_::Register(suiteHandle_, teardownFunction);                 \
    teardownFunction.function = [](Testing::TestResult& testContext_)

#define TEARDOWN() TTF_TEARDOWN_IMPL(__FILE__, __LINE__)

/*******************************************************\
        TEST SUITE
\*******************************************************/

#define TTF_TEST_SUITE_IMPL(testSuiteName, suiteTypeName, file, line)                   \
    static Testing::Context TTF_ANONYMOUS(anonymous) = testSuiteName;                   \
    static Testing::TestSuite suiteTypeName = (Testing::InitializerFunc)[]()

#define TEST_SUITE(name) TTF_TEST_SUITE_IMPL(#name, name, __FILE__, __LINE__)

#define EXPECT(check, message) if ((check) == false && testContext_.success) {          \
                testContext_.line = __LINE__;                                           \
                testContext_.failedMessage = message;                                   \
                testContext_.failedCheck = #check;                                      \
                testContext_.success = false;                                           \
                return; }

#define END_WITH_SUCCESS()                                                              \
                return;


#endif //TESTFRAMEWORK_TESTING_HPP

#include "AppIds.hpp"

AppIds operator|(const AppIds left, const AppIds right)
{
    return static_cast<AppIds>(static_cast<int>(left) | static_cast<int>(right));
}

AppIds operator&(const AppIds left, const AppIds right)
{
    return static_cast<AppIds>(static_cast<int>(left) & static_cast<int>(right));
}
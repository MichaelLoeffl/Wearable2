#include "DateTime.hpp"
#include "Common/Debugging.h"
#include "Drivers/Core.h"
#include "Drivers/RtcDriver.h"
#include "Timespan.hpp"

// For debug output only
#include "Drivers/Bluetooth.h"

static const uint8_t daysPerMonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

static cy_stc_rtc_config_t rtcTime_;
static uint32_t oldSeconds_;
static uint32_t milliseconds_ = 0;
static uint32_t runs_         = 0;

bool DateTime::UpdateRtcTime(uint32_t deltaT)
{
    HAL_RTC_GetDateAndTime(&rtcTime_);
    if (rtcTime_.sec != oldSeconds_)
    {
        if (rtcTime_.sec == 0)
        {
            LOG("Clock: %02d.%03d (%d %d)", (int)rtcTime_.sec, (int)milliseconds_, (int)GetTickCount(), (int)runs_);
        }
        oldSeconds_   = rtcTime_.sec;
        milliseconds_ = 0;
        runs_         = 1;
        return true;
    }
    runs_++;
    milliseconds_ += deltaT;
    if (milliseconds_ > 999)
    {
        milliseconds_ = 999;
    }
    return false;
}

DateTime::DateTime()
{
    day_         = rtcTime_.date;
    hour_        = rtcTime_.hour;
    minute_      = rtcTime_.min;
    month_       = rtcTime_.month;
    second_      = rtcTime_.sec;
    year_        = rtcTime_.year + 2000;
    millisecond_ = milliseconds_;
}

bool DateTime::IsLeapYear(uint16_t year)
{
    return ((year & 3) == 0 && year % 100 != 0) || (year % 400 == 0);
}

bool DateTime::IsLeapYear() const
{
    return DateTime::IsLeapYear(year_);
}

DateTime::DateTime(int seconds)
{
    day_         = 0;
    hour_        = 0;
    minute_      = 0;
    month_       = 0;
    second_      = 0;
    year_        = 0;
    millisecond_ = 0;

    if (seconds != 0)
    {
        *this += Timespan((uint32_t)seconds, 0);
    }
}

DateTime::DateTime(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second, uint16_t millisecond)
{
    day_         = day;
    hour_        = hour;
    minute_      = minute;
    month_       = month;
    second_      = second;
    year_        = year;
    millisecond_ = millisecond;
}

DateTime::DateTime(uint16_t year, uint16_t day, uint8_t hour, uint8_t minute, uint8_t second, uint16_t millisecond)
{
    hour_        = hour;
    minute_      = minute;
    second_      = second;
    year_        = year;
    millisecond_ = millisecond;

    // January
    if (day <= 31)
    {
        day_   = (uint8_t)day;
        month_ = 1;
        return;
    }
    day -= 31;

    // February
    if (day <= 28)
    {
        day_   = day;
        month_ = 2;
        return;
    }
    if (IsLeapYear())
    {
        if (day <= 29)
        {
            day_   = day;
            month_ = 2;
            return;
        }

        day--;
    }
    day -= 28;

    // Get month
    for (int i = 2; i < 12; i++)
    {
        if (day <= daysPerMonth[i])
        {
            day_   = day;
            month_ = i + 1;
            return;
        }
        day -= daysPerMonth[i];
    }

    // More days than days in a year
    month_ = 0;
}

bool DateTime::Valid() const
{
    return year_ != 0;
}

uint16_t DateTime::GetDayOfYear() const
{
    if (month_ > 2)
    {
        uint16_t day;
        switch (month_)
        {
            case 3:
                day = 31 + 28;
                break;
            case 4:
                day = 31 + 28 + 31;
                break;
            case 5:
                day = 31 + 28 + 31 + 30;
                break;
            case 6:
                day = 31 + 28 + 31 + 30 + 31;
                break;
            case 7:
                day = 31 + 28 + 31 + 30 + 31 + 30;
                break;
            case 8:
                day = 31 + 28 + 31 + 30 + 31 + 30 + 31;
                break;
            case 9:
                day = 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31;
                break;
            case 10:
                day = 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30;
                break;
            case 11:
                day = 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31;
                break;
            case 12:
                day = 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30;
                break;
            default:
                return 1;
        }

        day += day_;

        return IsLeapYear() ? day + 1 : day;
    }
    // February
    else if (month_ == 2)
    {
        return day_ + 31;
    }
    // January
    else
    {
        return day_;
    }
}

uint8_t ZellersCongruence(uint8_t day, uint8_t month, uint16_t year)
{
    if (month == 1)
    {
        month = 13;
        year--;
    }
    if (month == 2)
    {
        month = 14;
        year--;
    }

    int q = day;
    int m = month;
    int k = year % 100;
    int j = year / 100;
    int h = q + 13 * (m + 1) / 5 + k + k / 4 + j / 4 + 5 * j;
    h     = h % 7;

    return (uint8_t)h;
}

uint8_t DateTime::GetDayOfWeek() const
{
    return (ZellersCongruence(day_, month_, year_) + 5) % 7 + 1;
}

uint8_t DateTime::GetDayOfWeek(uint8_t day, uint8_t month, uint16_t year)
{
    return (ZellersCongruence(day, month, year) + 5) % 7 + 1;
}

uint8_t DateTime::GetTotalWeeksInYear(uint16_t year)
{
    bool isLongYear =
        // year starting on Thursday
        GetDayOfWeek(01, 01, year) == 4
        // leap year starting on Wednesday
        || (IsLeapYear(year) && GetDayOfWeek(01, 01, year) == 3)
        // year ending on Thursday
        || GetDayOfWeek(31, 12, year) == 4
        // leap year starting on Friday
        || (IsLeapYear(year) && GetDayOfWeek(31, 12, year) == 5);

    return isLongYear ? 53 : 52;
}

uint8_t DateTime::GetCalendarWeek(bool iso8601) const
{
    uint8_t dayOfYear = GetDayOfYear();
    uint8_t dayOfWeek = GetDayOfWeek();
    if (iso8601)
    {
        // https://en.wikipedia.org/wiki/ISO_week_date
        uint8_t totalWeeksThisYear = GetTotalWeeksInYear(year_);
        uint8_t totalWeeksLastYear = GetTotalWeeksInYear(year_ - 1);

        uint8_t preliminaryWeek = ((dayOfYear - dayOfWeek) + 10) / 7;
        uint8_t isoWeek;
        if (preliminaryWeek < 1)
        {
            isoWeek = totalWeeksLastYear;
        }
        else if (preliminaryWeek > totalWeeksThisYear)
        {
            isoWeek = 1;
        }
        else
        {
            isoWeek = preliminaryWeek;
        }

        return isoWeek;
    }
    else
    {
        return (dayOfYear + 7 - dayOfWeek) / 7 + 1;
    }
}

DateTime DateTime::GetStartOfDay()
{
    return DateTime(year_, month_, day_, 0, 0, 0, 0);
}

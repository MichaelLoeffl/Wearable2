#include "Common/Testing.hpp"

using namespace Testing;

TestSuite TestSuiteRoot;
Testing::TestSuite* suiteHandle_ = &TestSuiteRoot;

const char* Testing::TTF_CURRENT_NAME;

TestSuite::TestSuite(InitializerFunc f)
{
    name = TTF_CURRENT_NAME;
    parent_ = suiteHandle_;

    if (parent_ != nullptr) {
        parent_->Register(this);
    }

    suiteHandle_ = this;
    f();
    suiteHandle_ = parent_;
}

void TestSuite::Register(TestSuite* child)
{
    if (firstChild_ != nullptr)
    {
        auto c = firstChild_;
        while (c->sibling_ != nullptr)
        {
            c = c->sibling_;
        }
        c->sibling_ = child;
    }
    else
    {
        firstChild_ = child;
    }
}

void TestSuite::Register(TestFunctionInfo& testFunctionInfo)
{
    auto testHandle = &firstTest_;
    while (*testHandle != nullptr)
    {
        testHandle = &((*testHandle)->sibling);
    }
    *testHandle = &testFunctionInfo;
}

void Internals_::Register(TestSuite* suite, TestFunctionInfo &testFunctionInfo)
{
    suite->Register(testFunctionInfo);
}

void Internals_::Register(TestSuite* suite, SetupTeardownFunctionInfo &functionInfo)
{
    suite->Register(functionInfo);
}

void TestSuite::Register(SetupTeardownFunctionInfo& functionInfo)
{
    if (functionInfo.isSetup) {
        setup_ = &functionInfo;
    }
    else
    {
        teardown_ = &functionInfo;
    }
}

bool TestFilter(const char* name, const char* filter, const char*& nextPosition, bool leaf = false)
{
    nextPosition = filter;
    if (name == nullptr || filter == nullptr)
    {
        return false;
    }

    int i = 0;
    for (; name[i] != '\0' && filter[i] != '\0'; i++)
    {
        if (name[i] != filter[i])
        {
            return false;
        }
    }

    if (name[i] == '\0')
    {
        if (filter[i] == '.')
        {
            if (leaf)
            {
                return false;
            }
            nextPosition = &filter[i+1];
            return true;
        }
        else if (filter[i] == '\0')
        {
            nextPosition = &filter[i];
            return true;
        }
    }
    if (filter[0] == '\0')
    {
        return true;
    }
    return false;
}

void TestSuite::Run()
{
    Run("");
}

void TestSuite::Run(const char* filter)
{
    if (filter == nullptr)
    {
        RunInternal("");
    }
    else
    {
        RunInternal(filter);
    }
    Print(true, 0, filter);
}

void TestSuite::RunInternal(const char* filter)
{
    const char* childFilter = filter;
    if (this == &TestSuiteRoot || TestFilter(name, filter, childFilter))
    {
        success = true;
        if (setup_ != nullptr)
        {
            setup_->Run();
            success = success && setup_->testResult.success;
        }

        if (success) {
            auto childTest = firstTest_;
            while (childTest != nullptr) {
                childTest->Run(childFilter);
                success = success && childTest->testResult.success;
                childTest = childTest->sibling;
            }

            auto childSuite = firstChild_;
            while (childSuite != nullptr) {
                childSuite->RunInternal(childFilter);
                success = success && childSuite->success;
                childSuite = childSuite->sibling_;
            }
        }

        if (teardown_ != nullptr)
        {
            teardown_->Run();
            success = success && teardown_->testResult.success;
        }
    }
}

void PrintIndent(int indentation)
{
    for (int i = 0; i < indentation; i++)
    {
        TestWrite("  ");
    }
}

void PrintSuccess(bool success, int style)
{
    if (success)
    {
        switch (style) {
            case 1:
                TestWrite("{ OK } ");
                break;
            case 2:
                TestWrite("> OK > ");
                break;
            case 3:
                TestWrite("< OK < ");
                break;
            default:
                TestWrite("[ OK ] ");
                break;
        }
    }
    else
    {
        switch (style) {
            case 1:
                TestWrite("{FAIL} ");
                break;
            case 2:
                TestWrite(">FAIL> ");
                break;
            case 3:
                TestWrite("<FAIL< ");
                break;
            default:
                TestWrite("[FAIL] ");
                break;
        }
    }
}

void TestSuite::Print(bool printResults, int indentation, const char* filter)
{
    const char* childFilter = filter;
    if (this == &TestSuiteRoot || TestFilter(name, filter, childFilter))
    {
        int indent = indentation;
        if (this != &TestSuiteRoot)
        {
            PrintIndent(indent);
            if (printResults)
            {
                PrintSuccess(success, 1);
            }
            TestWrite(getName());
            TestWrite("\n");
            indent += 2;
        }

        if (setup_ != nullptr)
        {
            setup_->Print(printResults, indent);
        }

        auto childTest = firstTest_;
        while (childTest != nullptr) {
            childTest->Print(printResults, indent, childFilter);
            childTest = childTest->sibling;
        }

        auto childSuite = firstChild_;
        while (childSuite != nullptr) {
            childSuite->Print(printResults, indent, childFilter);
            childSuite = childSuite->sibling_;
        }

        if (teardown_ != nullptr)
        {
            teardown_->Print(printResults, indent);
            success = success && teardown_->testResult.success;
        }
    }
}

void SetupTeardownFunctionInfo::Run()
{
    testResult = {0};
    testResult.success = true;
    testResult.failedCheck = "none";
    testResult.failedMessage = "none";
    testResult.line = 0;
    function(testResult);
}

void SetupTeardownFunctionInfo::Print(bool printSuccess, int indent)
{
    PrintIndent(indent);

    if (isSetup)
    {
        if (printSuccess)
        {
            PrintSuccess(testResult.success, 2);
        }
        TestWrite("Setup\n");
    }
    else
    {
        if (printSuccess)
        {
            PrintSuccess(testResult.success, 3);
        }
        TestWrite("Teardown\n");
    }
}

void uitoa(unsigned int n, char s[], int bufferSize)
{
    int remainder = n;
    int length = 1;
    while (remainder >= 10)
    {
        remainder /= 10;
        length++;
    }
    remainder = n;

    if (length >= bufferSize)
    {
        if (bufferSize >= 2)
        {
            s[0] = 'X';
            s[1] = '\0';
        }
        return;
    }

    s[length] = '\0';
    for (int i = length - 1; i >= 0; i--)
    {
        int digit = remainder % 10;
        remainder = remainder / 10;
        s[i] = '0' + digit;
    }
}

void TestFunctionInfo::Print(bool printSuccess, int indent, const char* filter)
{
    const char *childFilter;

    if (TestFilter(name, filter, childFilter, true))
    {
        PrintIndent(indent);
        if (printSuccess) {
            PrintSuccess(testResult.success, 0);
        }
        TestWrite(name);
        TestWrite("\n");

        if (printSuccess && !testResult.success)
        {
            PrintIndent(indent);
            TestWrite("       ");
            TestWrite(testResult.failedMessage);
            TestWrite(" (");
            TestWrite(testResult.failedCheck);
            TestWrite(") @: ");
            TestWrite(file);
            TestWrite(":");
            char buffer[12];
            uitoa( testResult.line, buffer, sizeof(buffer) / sizeof(char));
            TestWrite(buffer);
            TestWrite("\n");
        }
    }
}

void TestFunctionInfo::Run(const char* filter) {
    const char *childFilter;

    if (TestFilter(name, filter, childFilter, true))
    {
        testResult = {0};
        testResult.success = true;
        testResult.failedCheck = "none";
        testResult.failedMessage = "none";
        testResult.line = 0;
        function(testResult);
    }
}


#ifndef COMMON_TOOLS_H
#define COMMON_TOOLS_H

#include "../lvgl/src/lv_objx/lv_img.h"
#include "../Apps/Icons/Icons.h"
#include "AppIds.hpp"
#include "FitnessActivities.hpp"

class Tools
{
public:
    static lv_img_dsc_t* GetSmallIcon(AppIds appId, lv_img_dsc_t* defaultIcon = &MessagesAppIconSimple)
    {
        switch (appId)
        {
            case AppIds::Alarm:
                return &AlarmAppIconSimple;
            case AppIds::Altitude:
                return &AltitudeAppIconSimple;
            case AppIds::Calendar:
                return &CalendarAppIconSimple;
            case AppIds::Date:
                return &DateAppIconSimple;
            case AppIds::Email:
                return &MailAppIconSimple;
            case AppIds::Heartrate:
                return &HeartrateAppIconSimple;
            case AppIds::Navigation:
                return &NavigationAppIconSimple;
            case AppIds::Notifications:
                return &MessagesAppIconSimple;
            case AppIds::Phone:
                return &PhoneAppIconSimple;
            case AppIds::IncomingCall:
                return &PhoneAppIconSimple;
            case AppIds::Reminder:
                return &ReminderAppIconSimple;
            case AppIds::Settings:
                return &SettingsAppIconSimple;
            case AppIds::Steps:
                return &PedometerAppIconSimple;
            case AppIds::Stopwatch:
                return &StopwatchAppIconSimple;
            case AppIds::Timer:
                return &TimerAppIconSimple;
            case AppIds::WatchLocal:
                return &WatchLocalAppIconSimple;
            case AppIds::WatchWorld:
                return &WatchWorldAppIconSimple;
            case AppIds::Pairing:
                return &SettingsAppIconSimple;
            case AppIds::Energy:
                return &EnergyAppIconSimple;
            case AppIds::FlightMode:
                return &FlightModeAppIconSimple;
            case AppIds::Payment:
                return &PaymentAppIconSimple;
            case AppIds::Camera:
                return &CameraAppIconSimple;
            default:
                return defaultIcon;
        }
    }

    static lv_img_dsc_t* GetIcon(AppIds appId, lv_img_dsc_t* defaultIcon = &SettingsAppIcon)
    {
        switch (appId)
        {
            case AppIds::Alarm:
                return &AlarmAppIcon;
            case AppIds::Altitude:
                return &AltitudeAppIcon;
            case AppIds::Calendar:
                return &CalendarAppIcon;
            case AppIds::Date:
                return &DateAppIcon;
            case AppIds::Email:
                return &MailAppIcon;
            case AppIds::Heartrate:
                return &HeartrateAppIcon;
            case AppIds::Navigation:
                return &NavigationAppIcon;
            case AppIds::Notifications:
                return &MessagesAppIcon;
            case AppIds::Phone:
                return &PhoneAppIcon;
            case AppIds::Reminder:
                return &ReminderAppIcon;
            case AppIds::Settings:
                return &SettingsAppIcon;
            case AppIds::Steps:
                return &PedometerAppIcon;
            case AppIds::Stopwatch:
                return &StopwatchAppIcon;
            case AppIds::Timer:
                return &TimerAppIcon;
            case AppIds::WatchApps:
                return &WatchLocalAppIcon;
            case AppIds::WatchLocal:
                return &WatchLocalAppIcon;
            case AppIds::WatchWorld:
                return &WatchWorldAppIcon;
            case AppIds::Pairing:
                return &SettingsAppIcon;
            case AppIds::Energy:
                return &EnergyAppIcon;
            case AppIds::FlightMode:
                return &FlightModeAppIcon;
            case AppIds::Payment:
                return &PaymentAppIcon;
            case AppIds::About:
                return &AboutAppIcon;
            case AppIds::Fitness:
                return &YogaAppIcon;
            case AppIds::Camera:
                return &CameraAppIcon;
            default:
                return defaultIcon;
        }
    }

    static lv_img_dsc_t* GetFitnessActivityIcon(FitnessActivities activity)
    {
        switch (activity)
        {
            case FitnessActivities::Running:
                return &RunningAppIcon;
            case FitnessActivities::Walking:
                return &WalkingAppIcon;
            case FitnessActivities::Cycling:
                return &CyclingAppIcon;
            case FitnessActivities::Strength:
                return &StrengthAppIcon;
            case FitnessActivities::Yoga:
                return &YogaAppIcon;
            case FitnessActivities::Other:
                return &OtherActivityAppIcon;
            case FitnessActivities::Recent:
                return &RecentActivityAppIcon;
            default:
                return &OtherActivityAppIcon;
        }
    }

    static lv_img_dsc_t* GetFitnessActivitySmallIcon(FitnessActivities activity)
    {
        switch (activity)
        {
            case FitnessActivities::Running:
                return &RunningAppIconSimple;
            case FitnessActivities::Walking:
                return &WalkingAppIconSimple;
            case FitnessActivities::Cycling:
                return &CyclingAppIconSimple;
            case FitnessActivities::Strength:
                return &StrengthAppIconSimple;
            case FitnessActivities::Yoga:
                return &YogaAppIconSimple;
            case FitnessActivities::Other:
                return &OtherActivityAppIconSimple;
            default:
                return &OtherActivityAppIconSimple;
        }
    }

    static lv_img_dsc_t* GetFitnessActivityLargeIcon(FitnessActivities activity)
    {
        switch (activity)
        {
            case FitnessActivities::Running:
                return &RunningActivity;
            case FitnessActivities::Walking:
                return &WalkingActivity;
            case FitnessActivities::Cycling:
                return &CyclingActivity;
            case FitnessActivities::Strength:
                return &StrengthActivity;
            case FitnessActivities::Yoga:
                return &YogaActivity;
            case FitnessActivities::Other:
                return &OtherActivityActivity;
            default:
                return &OtherActivityActivity;
        }
    }
};

#endif /* COMMON_TOOLS_H */

#ifndef APPQUEUES_H_6F69A1
#define APPQUEUES_H_6F69A1

#include "Common/App.hpp"
#include "Common/Common.h"
#include <cassert>

class App;
class AppSystem;

template<int T> class LinkQueue
{
private:
    AppSlot<T>* first_;
    AppSlot<T>* last_;

public:
    App* First(AppSystem* system)
    {
        AppSlot<T>* current = first_;
        while (current != NULL && static_cast<App*>(current)->System() != system)
        {
            current = current->Next;
        }
        return static_cast<App*>(current);
    }
    App* Last(AppSystem* system)
    {
        AppSlot<T>* current = last_;
        while (current != NULL && static_cast<App*>(current)->System() != system)
        {
            current = current->Last;
        }
        return static_cast<App*>(current);
    }

    LinkQueue()
    {
        first_ = (AppSlot<T>*)NULL;
        last_  = (AppSlot<T>*)NULL;
    }

    void InsertFront(AppSlot<T>* slot)
    {
        assert(slot->Queue == NULL);
        InsertAfter(slot, (AppSlot<T>*)NULL);
    }

    void InsertBack(AppSlot<T>* slot) { InsertAfter(slot, last_); }

    __inline void InsertAfter(AppSlot<T>* slot, AppSlot<T>* prevSlot)
    {
        assert(slot->Queue == NULL && (prevSlot == NULL || prevSlot->Queue == this));
        slot->Queue = this;

        if (prevSlot == NULL)
        {
            // slot->Last = NULL;

            if (first_ == NULL)
            {
                last_ = slot;
            }
            else
            {
                first_->Last = slot;
                slot->Next   = first_;
            }

            first_ = slot;
        }
        else
        {
            slot->Last = prevSlot;

            if (prevSlot->Next == NULL)
            {
                last_ = slot;
            }
            else
            {
                slot->Next           = prevSlot->Next;
                prevSlot->Next->Last = slot;
            }

            prevSlot->Next = slot;
        }
    }

    void Remove(AppSlot<T>* slot)
    {
        assert(slot->Queue == this);
        slot->Queue = (LinkQueue<T>*)NULL;

        if (slot->Next == NULL)
        {
            last_ = slot->Last;
        }
        else
        {
            slot->Next->Last = slot->Last;
        }

        if (slot->Last == NULL)
        {
            first_ = slot->Next;
        }
        else
        {
            slot->Last->Next = slot->Next;
        }

        slot->Next = (AppSlot<T>*)NULL;
        slot->Last = (AppSlot<T>*)NULL;
    }

    void ToFront(AppSlot<T>* slot)
    {
        if (slot->Queue != NULL)
        {
            Remove(slot);
        }

        InsertFront(slot);
    }

    void ToBack(AppSlot<T>* slot)
    {
        if (slot->Queue != NULL)
        {
            Remove(slot);
        }

        InsertBack(slot);
    }
};

#endif /* APPQUEUES_H_6F69A1 */

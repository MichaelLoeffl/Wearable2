#ifndef DEBUGGING_H_5E50BB
#define DEBUGGING_H_5E50BB

#include "Common/Common.h"
#include <stdio.h>

CFUNC void UpdateDebugInterface(uint32_t dT);

CFUNC void UartSendData(const char* channel, const uint8_t* data, int dataLength);
CFUNC void UartStartTextBlock(const char* channel);
CFUNC void UartStartDataBlock(const char* channel);
CFUNC void UartFinishBlock();
CFUNC void UartFinishCoalition();
CFUNC int printf_(const char* format, ...);

#ifdef SIMULATOR

#include <assert.h>
#include <stdio.h>

#define LOG(...)         \
    printf_(__VA_ARGS__); \
    printf_("\n")
#define LOG_INLINE(...) printf(__VA_ARGS__)
#define LOG_FUNC LOG(__FUNCTION__);

#else

#ifdef DEBUG


#define LOG_INLINE(...) printf_(__VA_ARGS__)
#define LOG(...)                   \
    UartStartTextBlock("Text");    \
    printf_(__VA_ARGS__);          \
    UartFinishBlock();
#else // RELEASE
CFUNC void FlushOutput();
#define LOG_INLINE(...)
#define LOG(...)
#endif

#define LOG_FUNC

#endif

#endif /* DEBUGGING_H_5E50BB */

#ifndef APPSYSTEM_H_94D946
#define APPSYSTEM_H_94D946

#include "AppQueues.hpp"
#include "LinkQueueIterator.h"

typedef void (*ActionFunc)(void* data);
typedef void (*ActionFuncNoParam)();



struct InstrumentedFunction
{
    InstrumentedFunction(const char* functionName) {}

    ~InstrumentedFunction() {}
};

#define INSTRUMENTED_FUNCTION InstrumentedFunction functionInstrumentation_(__FUNCTION__)

struct ActionInfo
{
    ActionFunc Action;
    void* Data;
};

class App;

class AppSystem
{
    friend void CallPrepare(void* system);

private:
    bool initialized_;
    void PreInitialize();
    void Prepare();

public:
    virtual void OnIdle();

    virtual ~AppSystem() {}
    void Startup();
    void DoEvents();

    void RegisterApp(App& app, const char* appName);

    bool StartApp(App& app, void* context = NULL, bool setRootApp = true);
    bool StartApp(AppIds appId, bool setRootApp = true);
    void CloseApp(App& app);
    void CloseApp(AppIds appId);
    void CloseCurrentApp();
    void CloseAllApps();
    void BackToLauncher();
    void SetLauncher(App& app);

    App* GetCurrentApp();
    bool LauncherActive();
    int16_t GetScrollPosition();

    template<int T> inline LinkQueue<T>& GetQueue()
    {
        static LinkQueue<T> queue;
        return queue;
    }

    template<int T> inline LinkQueueIterator<T> IterateQueue()
    {
        LinkQueueIterator<T> iterator(GetQueue<T>(), this);
        return iterator;
    }

    void Dispatch(ActionFunc, void* data) const;
    void Dispatch(ActionFuncNoParam func) const;

protected:
    AppSystem();
    virtual App& Initialize() = 0;
    virtual bool HasGui()     = 0;
    virtual void BeforeMainLoop() {};

    virtual void MainLoop(uint32_t delta) = 0;
};

#endif /* APPSYSTEM_H_94D946 */

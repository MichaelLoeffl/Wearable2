#ifndef FITNESSINTENSITIES_HPP
#define FITNESSINTENSITIES_HPP

enum class FitnessIntensities
{
    Normal = 0,
    VeryLow,
    Low,
    High,
    VeryHigh
};

#endif /* FITNESSINTENSITIES_HPP */

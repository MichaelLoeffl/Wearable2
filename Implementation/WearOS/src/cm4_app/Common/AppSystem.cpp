#include "Common/AppSystem.hpp"
#include "Common/App.hpp"
#include "Common/Common.h"
#include "Drivers/Wakelock.h"
#include "Drivers/Core.h"
#include "Services/Settings.hpp"
#include "Services/VibrationService.hpp"
#include <assert.h>

static AppSystem* processorAppSystem_;

void AppSystemIdleHook(void* data)
{
    ((AppSystem*)data)->OnIdle();
}

CFUNC void vApplicationIdleHook()
{
}

void MainTask(void* queue)
{
}

void AppSystem::OnIdle()
{
}

void AppSystem::PreInitialize()
{
}

void AppSystem::Prepare()
{
    App& launcher = Initialize();

    processorAppSystem_ = this;

    LinkQueueIterator<APP_QUEUE_ALL_APPS> iterator = IterateQueue<APP_QUEUE_ALL_APPS>();
    while (iterator.Current() != NULL)
    {
        iterator.GetCurrentApp()->OnInitialize();
        iterator.Next();
    }

    initialized_ = true;
    SetLauncher(launcher);
}

extern uint32_t tickCounter;

void AppSystem::Startup()
{
    static uint8_t recCounter = 0;

    recCounter++;

    PreInitialize();

    if (recCounter == 1)
    {
        Prepare();
        BeforeMainLoop();
        while (true)
        {
            DoEvents();
        }
    }

    recCounter--;
}

void AppSystem::DoEvents()
{
    static uint16_t recCounter = 0;
    static uint32_t lastTick = 0;
    static Wakelock applicationRecursionWakelock_;

    recCounter++;
    if (recCounter > 1)
    {
        UpdateWakelock(&applicationRecursionWakelock_, POWER_STATE_BACKGROUND_PASSIVE);
    }

    uint16_t coalitionId = GetCoalitionId();
    SetCoalitionId(0);

    uint32_t tempTick = GetTickCount();
    uint32_t delta    = tempTick - lastTick;
    MainLoop(delta);
    lastTick = tempTick;

    SetCoalitionId(coalitionId);

    recCounter--;
    if (recCounter <= 1)
    {
        ReleaseWakelock(&applicationRecursionWakelock_);
    }
}

void AppSystem::RegisterApp(App& app, const char* appName)
{
    assert(!initialized_);
    assert(app.system_ == NULL);
    app.system_ = this;
    app.name_ = appName;

    GetQueue<APP_QUEUE_ALL_APPS>().InsertBack(&app);
}

bool AppSystem::StartApp(App& app, void* context, bool setRootApp)
{
    if (!HasGui())
    {
        return false;
    }

    LOG("App activated: %s", app.name_);

    if (GetCurrentApp() == &app)
    {
        return false;
    }

    if (setRootApp)
    {
        LinkQueue<APP_QUEUE_STACK>& queue = GetQueue<APP_QUEUE_STACK>();
        queue.First(this)->OnDeactivate();
        while (!LauncherActive())
        {
            queue.Remove(queue.First(this));
        }
    }

    GetCurrentApp()->OnDeactivate();

    VibrationService.Vibrate(VIBRATE_CLICK);
    LinkQueue<APP_QUEUE_STACK>& queue = GetQueue<APP_QUEUE_STACK>();
    queue.InsertFront(&app);
    app.SetContext(context);
    queue.First(this)->OnActivate();

    return true;
}

bool AppSystem::StartApp(AppIds appId, bool setRootApp)
{
    App* app;
    LinkQueueIterator<APP_QUEUE_ALL_APPS> iterator = IterateQueue<APP_QUEUE_ALL_APPS>();
    while ((app = iterator.GetCurrentApp()) != NULL)
    {
        if (app->AppId() == appId)
        {
            return StartApp(*app, nullptr);
        }
        iterator.Next();
    }

    return false;
}

void AppSystem::CloseApp(App& app)
{
    if (GetCurrentApp() == &app)
    {
        app.BackPressed();
    }
}

void AppSystem::CloseApp(AppIds appId)
{
    App* app;
    LinkQueueIterator<APP_QUEUE_ALL_APPS> iterator = IterateQueue<APP_QUEUE_ALL_APPS>();
    while ((app = iterator.GetCurrentApp()) != nullptr)
    {
        if (app->AppId() == appId)
        {
            CloseApp(*app);
            return;
        }
        iterator.Next();
    }
}

void AppSystem::CloseCurrentApp()
{
    if (!HasGui())
    {
        return;
    }
    if (LauncherActive())
    {
        return;
    }

    LinkQueue<APP_QUEUE_STACK>& queue = GetQueue<APP_QUEUE_STACK>();
    queue.First(this)->OnDeactivate();
    queue.Remove(queue.First(this));
    queue.First(this)->OnActivate();
}

void AppSystem::BackToLauncher()
{
    GetCurrentApp()->BackLongPressed();
}

void AppSystem::CloseAllApps()
{
    if (!HasGui())
    {
        return;
    }

    if (LauncherActive())
    {
        return;
    }

    LinkQueue<APP_QUEUE_STACK>& queue = GetQueue<APP_QUEUE_STACK>();
    queue.First(this)->OnDeactivate();
    do
    {
        queue.Remove(queue.First(this));
    } while (!LauncherActive());

    queue.First(this)->OnActivate();
}

void AppSystem::SetLauncher(App& app)
{
    if (!HasGui())
    {
        return;
    }

    LinkQueue<APP_QUEUE_STACK>& queue = GetQueue<APP_QUEUE_STACK>();
    queue.InsertBack(&app);

    if (LauncherActive())
    {
        GetCurrentApp()->OnActivate();
    }
}

App* AppSystem::GetCurrentApp()
{
    if (!HasGui())
    {
        return NULL;
    }
    LinkQueue<APP_QUEUE_STACK>& queue = GetQueue<APP_QUEUE_STACK>();
    return queue.First(this);
}

bool AppSystem::LauncherActive()
{
    if (!HasGui())
    {
        return true;
    }

    LinkQueue<APP_QUEUE_STACK>& queue = GetQueue<APP_QUEUE_STACK>();
    return GetCurrentApp() == queue.Last(this);
}

int16_t AppSystem::GetScrollPosition()
{
    auto app = GetCurrentApp();
    if (app == nullptr)
    {
        return 0;
    }

    return app->GetScrollPosition();
}

void AppSystem::Dispatch(ActionFunc func, void* data) const
{
    ActionInfo info;
    info.Action = func;
    info.Data   = data;
    (void)info;
}

void Redirector(void* func)
{
    ((ActionFuncNoParam)func)();
}

void AppSystem::Dispatch(ActionFuncNoParam func) const
{
    ActionInfo info;
    info.Action = Redirector;
    info.Data   = (void*)func;
    (void)info;
}

AppSystem::AppSystem()
{
    initialized_ = false;
}

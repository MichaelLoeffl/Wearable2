#ifndef APP_IDS_H
#define APP_IDS_H

enum class AppIds
{
    Unknown           = 0,
    System            = (1 << 0),
    Launcher          = (1 << 1),
    NotificationModal = (1 << 2),
    PowerMenu         = (1 << 3),
    Notifications     = (1 << 4),
    Email             = (1 << 5),
    WatchApps         = (1 << 6),
    WatchLocal        = (1 << 7),
    WatchWorld        = (1 << 8),
    Steps             = (1 << 9),
    Alarm             = (1 << 10),
    Altitude          = (1 << 11),
    BleConnection     = (1 << 12),
    Calendar          = (1 << 13),
    Fitness           = (1 << 14),
    Heartrate         = (1 << 15),
    Navigation        = (1 << 16),
    Payment           = (1 << 17),
    Phone             = (1 << 18),
    IncomingCall      = (1 << 19),
    About             = (1 << 20),
    Settings          = (1 << 21),
    Stopwatch         = (1 << 22),
    Reminder          = (1 << 23),
    Test              = (1 << 24),
    Timer             = (1 << 25),
    Camera            = (1 << 26),
    Pairing           = (1 << 27),
    Energy            = (1 << 28),
    FlightMode        = (1 << 29),
    Date              = (1 << 30),
    // Barcode           = (1 << 14),

    // Top is the last flag + 1. To get a mask over all flags we subtract 2 but we would miss the topmost flag then. Therefore we shift by one and include the
    // lowest bit
    Top,
    Max = Top - 1,
    Any = ((Max - 1) << 1) | 1,

};

AppIds operator|(const AppIds left, const AppIds right);
AppIds operator&(const AppIds left, const AppIds right);

#endif // APP_IDS_H
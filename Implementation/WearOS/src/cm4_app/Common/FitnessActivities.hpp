#ifndef FITNESSACTIVITIES_HPP
#define FITNESSACTIVITIES_HPP

enum class FitnessActivities
{
    None = 0,
    Running,
    Walking,
    Cycling,
    Strength,
    Yoga,
    Other,

    Count,

    Recent
};

#endif /* FITNESSACTIVITIES_HPP */

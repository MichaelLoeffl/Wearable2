#include "Common/App.hpp"
#include "Apps/Icons/Icons.h"
#include "Common/AppSystem.hpp"
#include "Drivers/Display.h"

#include "../lvgl/src/lv_core/lv_disp.h"
#include "../lvgl/src/lv_objx/lv_page.h"
#include "Services/Settings/SettingsStorage.hpp"

extern "C"
{
    extern lv_img_dsc_t SettingsAppIcon;
}

bool App::Active()
{
    return system_->GetQueue<APP_QUEUE_STACK>().First(system_) == this;
}

void App::BackPressed()
{
    if (System()->GetCurrentApp() == this)
    {
        System()->CloseCurrentApp();
    }
}

void App::BackLongPressed()
{
    if (System()->GetCurrentApp() == this)
    {
        System()->CloseAllApps();
    }
}

void App::Invalidate()
{
    if (page_ != nullptr)
    {
        lv_obj_invalidate(page_);
    }
}

int16_t App::GetScrollPosition()
{
    if (page_ != nullptr)
    {
        lv_area_t area;
        lv_obj_t* scrollable = lv_page_get_scrl(page_);
        lv_obj_get_coords(scrollable, &area);
        return area.x1;
    }
    return 0;
}

void App::SetBadge(bool active)
{
    notificationActive_ = active;

    if (!badgeImage_)
    {
        return;
    }

    lv_obj_set_hidden(badgeImage_, !active);
}

void App::AdjustRotation(bool forceDefault)
{
    if (forceDefault)
    {
        if (DisplayIsRotated())
        {
            DisplayRotate(false);
        }
        return;
    }

    bool hasToBeRotated = settingsStorage_.HandSide.Value && !IgnoreRotation();
    if (hasToBeRotated != DisplayIsRotated())
    {
        DisplayRotate(hasToBeRotated);
    }
}

void App::OnInitialize()
{
    Initialize();
}

void App::OnActivate()
{
    lv_mem_monitor_t mon;
    lv_mem_monitor(&mon);
    lastRamUsage_ = mon.used_pct;

    AdjustRotation();

    page_ = lv_page_create(lv_scr_act(), NULL);
    lv_obj_set_size(page_, SCREEN_WIDTH, SCREEN_HEIGHT);
    lv_obj_set_pos(page_, 0, 0);
    lv_page_set_scrl_height(page_, LV_VER_RES);

    SetPageSize(SCREEN_WIDTH);

    lv_page_set_sb_mode(page_, LV_SB_MODE_OFF);
    lv_page_set_scrl_layout(page_, LV_LAYOUT_OFF);

    Activated();
}

void App::OnDeactivate()
{
    Deactivated();

    if (page_ != nullptr)
    {
        lv_obj_del(page_);
        page_ = (lv_obj_t*)nullptr;
    }

    lv_mem_monitor_t mon;
    lv_mem_monitor(&mon);
    if (mon.used_pct > lastRamUsage_)
    {
        // TODO: Add assert or set breakpoint here to detect memory loss. It is not really working. App should be updated on following tick
        page_ = nullptr;
    }
}

void App::SetPageSize(int width)
{
    lv_page_set_scrl_width(page_, width);
}

void App::EnableListLayout()
{
    lv_page_set_scrl_layout(page_, LV_LAYOUT_ROW_M); /*Arrange elements automatically*/
}

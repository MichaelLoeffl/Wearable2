#ifndef ICONCACHE_HPP
#define ICONCACHE_HPP

#include "../lvgl/src/lv_objx/lv_img.h"

#define ICON_CACHE_ICON_COUNT (4)
#define ICON_CACHE_ICON_SIZE (3524)

class CachedIcon
{
public:
    const lv_img_dsc_t* original;
    uint16_t Usage;
    lv_img_dsc_t icondesc;
};

class IconCache
{
private:
    bool cacheDisabled_ = false;
    CachedIcon cache[ICON_CACHE_ICON_COUNT];

public:
    void Disable(bool disable) { cacheDisabled_ = disable; }
    const lv_img_dsc_t* GetIconFromCache(const lv_img_dsc_t* original);
};

extern IconCache IconCacheInstance;

#endif /* ICONCACHE_HPP */

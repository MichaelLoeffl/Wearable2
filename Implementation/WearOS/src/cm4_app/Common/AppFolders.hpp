#ifndef APPFOLDERS_H_1F0CD7
#define APPFOLDERS_H_1F0CD7

enum class AppFolders
{
    Home = 0,
    System,
    Launcher,
    WatchApps,
    SettingsApps
};

#endif /* APPFOLDERS_H_1F0CD7 */

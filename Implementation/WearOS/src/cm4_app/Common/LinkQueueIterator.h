#ifndef LINKQUEUEITERATOR_H_67DD2C
#define LINKQUEUEITERATOR_H_67DD2C

#include "Common/App.hpp"
#include <cassert>

class AppSystem;

template<int T> class LinkQueueIterator
{
    AppSlot<T>* current_;
    AppSystem* system_;

public:
    LinkQueueIterator(LinkQueue<T> queue, AppSystem* system)
    {
        system_  = system;
        current_ = queue.First(system);
        while (current_ != NULL && (GetCurrentApp()->System() != system_))
        {
            current_ = current_->Next;
        }
    }

    AppSlot<T>* Current() const { return static_cast<App*>(current_); }

    void Next()
    {
        if (current_ == NULL)
        {
            return;
        }

        assert(GetCurrentApp()->System() == system_);

        do
        {
            current_ = current_->Next;
        } while (current_ != NULL && (GetCurrentApp()->System() != system_));
    }

    App* GetCurrentApp() const { return static_cast<App*>(current_); }
};

#endif /* LINKQUEUEITERATOR_H_67DD2C */

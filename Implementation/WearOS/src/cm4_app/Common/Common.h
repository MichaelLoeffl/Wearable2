#ifndef POINT_TEC_COMMON_H
#define POINT_TEC_COMMON_H

#ifdef __cplusplus
#define CFUNC extern "C"
extern "C"
{
#else
#define CFUNC
#endif

#include "Common/Debugging.h"
#include "PointTypes.h"

#define BOARD_TYPE_CYPRESS 1
#define BOARD_TYPE_QINNO 2

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define CLAMP(value, min, max) (((value) > (max)) ? (max) : (((value) < (min)) ? (min) : (value)))


#if BOARD_TYPE == BOARD_TYPE_CYPRESS
#elif BOARD_TYPE == BOARD_TYPE_QINNO
#else
#error "BOARD_TYPE not specified"
#endif

#ifdef __cplusplus
}
#endif

CFUNC int printfDummy(const char* format, ...);
#define printf printfDummy

#endif // POINT_TEC_COMMON_H

#ifndef UTILS_H_B66DD1
#define UTILS_H_B66DD1

#include "Common.h"

void StringCopySafe(const char* source, char* destination, unsigned int sourceLength, unsigned int destinationLength);
void StringCopySafe(const char* source, char* destination, unsigned int destinationLength);
bool ToString(int number, uint8_t* destination, unsigned int destinationLength);

#endif /* UTILS_H_B66DD1 */
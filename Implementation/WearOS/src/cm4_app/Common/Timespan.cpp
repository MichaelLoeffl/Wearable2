#include "Timespan.hpp"
#include "DateTime.hpp"

void FlattenTimespan(Timespan* timespan)
{
    timespan->seconds_ += timespan->milliseconds_ / 1000;
    timespan->milliseconds_ %= 1000;
    if (timespan->milliseconds_ < 0)
    {
        timespan->seconds_--;
        timespan->milliseconds_ += 1000;
    }

    timespan->minutes_ += timespan->seconds_ / 60;
    timespan->seconds_ %= 60;
    if (timespan->seconds_ < 0)
    {
        timespan->minutes_--;
        timespan->seconds_ += 60;
    }

    timespan->hours_ += timespan->minutes_ / 60;
    timespan->minutes_ %= 60;
    if (timespan->minutes_ < 0)
    {
        timespan->hours_--;
        timespan->minutes_ += 60;
    }

    timespan->days_ += timespan->hours_ / 24;
    timespan->hours_ %= 24;
    if (timespan->hours_ < 0)
    {
        timespan->days_--;
        timespan->hours_ += 24;
    }

    return;
}

Timespan::Timespan(int32_t milliseconds)
{
    days_         = 0;
    hours_        = 0;
    minutes_      = 0;
    seconds_      = 0;
    milliseconds_ = milliseconds;

    FlattenTimespan(this);
}

Timespan::Timespan(int32_t seconds, int32_t milliseconds)
{
    days_         = 0;
    hours_        = 0;
    minutes_      = 0;
    seconds_      = seconds;
    milliseconds_ = milliseconds;

    FlattenTimespan(this);
}

Timespan::Timespan(int32_t minutes, int32_t seconds, int32_t milliseconds)
{
    days_         = 0;
    hours_        = 0;
    minutes_      = minutes;
    seconds_      = seconds;
    milliseconds_ = milliseconds;

    FlattenTimespan(this);
}

Timespan::Timespan(int32_t hours, int32_t minutes, int32_t seconds, int32_t milliseconds)
{
    days_         = 0;
    hours_        = hours;
    minutes_      = minutes;
    seconds_      = seconds;
    milliseconds_ = milliseconds;

    FlattenTimespan(this);
}

Timespan::Timespan(int32_t days, int32_t hours, int32_t minutes, int32_t seconds, int32_t milliseconds)
{
    days_         = days;
    hours_        = hours;
    minutes_      = minutes;
    seconds_      = seconds;
    milliseconds_ = milliseconds;

    FlattenTimespan(this);
}

int32_t Timespan::TotalMilliseconds() const
{
    int32_t result = days_;
    result         = result * 24 + hours_;
    result         = result * 60 + minutes_;
    result         = result * 60 + seconds_;
    result         = result * 1000 + milliseconds_;
    return result;
}

int32_t Timespan::TotalSeconds() const
{
    int32_t result = days_;
    result         = result * 24 + hours_;
    result         = result * 60 + minutes_;
    result         = result * 60 + seconds_;

    if (result < 0 && milliseconds_ != 0)
    {
        result++;
    }
    return result;
}

int32_t Timespan::TotalMinutes() const
{
    int32_t result = days_;
    result         = result * 24 + hours_;
    result         = result * 60 + minutes_;

    if (result < 0 && (milliseconds_ | seconds_) != 0)
    {
        result++;
    }
    return result;
}

int32_t Timespan::TotalHours() const
{
    int32_t result = days_;
    result         = result * 24 + hours_;

    if (result < 0 && (milliseconds_ | seconds_ | minutes_) != 0)
    {
        result++;
    }
    return result;
}

int32_t Timespan::TotalDays() const
{
    int32_t result = days_;

    if (result < 0 && (milliseconds_ | seconds_ | minutes_ | hours_) != 0)
    {
        result++;
    }
    return result;
}

Timespan DateToTimespan(const DateTime& source)
{
    uint32_t year = source.GetYear();

    if (year == 0)
    {
        return Timespan(0);
    }

    uint32_t day = year * 365;

    // Get leap days only for all previous years (complete years, year 0 is an invalid date)
    year--;
    day += year / 4;
    day -= year / 100;
    day += year / 400;
    day += 1; // Year 0 is a leap year!

    // Leap day in current year is included
    day += source.GetDayOfYear();

    return Timespan(day, source.GetHour(), source.GetMinute(), source.GetSecond(), source.GetMillisecond());
}

DateTime TimespanToDate(const Timespan& source)
{
    if (source.days_ <= 366)
    {
        return DateTime::Zero();
    }

    uint16_t year = (uint16_t)(source.days_ / 366);
    if (DateTime::IsLeapYear(year) && year > 0)
    {
        year--;
    }
    uint32_t leaps = (year / 4) - (year / 100) + (year / 400) + 1;
    uint32_t days  = source.days_ - year * 365 - leaps;

    while (days > 365)
    {
        if (DateTime::IsLeapYear(year))
        {
            if (days == 366)
            {
                break;
            }

            days -= 366;
            year++;
        }
        else
        {
            days -= 365;
            year++;
        }
    }

    return DateTime(year, days, source.hours_, source.minutes_, source.seconds_, source.milliseconds_);
}

Timespan operator+(const Timespan& a, const Timespan& b)
{
    return Timespan(a.days_ + b.days_, a.hours_ + b.hours_, a.minutes_ + b.minutes_, a.seconds_ + b.seconds_, a.milliseconds_ + b.milliseconds_);
}

Timespan operator-(const Timespan& a, const Timespan& b)
{
    return Timespan(a.days_ - b.days_, a.hours_ - b.hours_, a.minutes_ - b.minutes_, a.seconds_ - b.seconds_, a.milliseconds_ - b.milliseconds_);
}

DateTime operator+(const DateTime& date, const Timespan& timespan)
{
    return TimespanToDate(DateToTimespan(date) + timespan);
}

DateTime operator+=(const DateTime& date, const Timespan& timespan)
{
    return date + timespan;
}

DateTime operator-(const DateTime& date, const Timespan& timespan)
{
    return TimespanToDate(DateToTimespan(date) - timespan);
}

DateTime operator-=(const DateTime& date, const Timespan& timespan)
{
    return date - timespan;
}

Timespan operator-(const DateTime& dateA, const DateTime& dateB)
{
    return DateToTimespan(dateA) - DateToTimespan(dateB);
}

bool operator>(const Timespan& a, const Timespan& b)
{
    if (a.days_ == b.days_)
    {
        if (a.hours_ == b.hours_)
        {
            if (a.minutes_ == b.minutes_)
            {
                if (a.seconds_ == b.seconds_)
                {
                    return a.milliseconds_ > b.milliseconds_;
                }
                return a.seconds_ > b.seconds_;
            }
            return a.minutes_ > b.minutes_;
        }
        return a.hours_ > b.hours_;
    }
    return a.days_ > b.days_;
}

bool operator>=(const Timespan& a, const Timespan& b)
{
    if (a.days_ == b.days_)
    {
        if (a.hours_ == b.hours_)
        {
            if (a.minutes_ == b.minutes_)
            {
                if (a.seconds_ == b.seconds_)
                {
                    return a.milliseconds_ >= b.milliseconds_;
                }
                return a.seconds_ > b.seconds_;
            }
            return a.minutes_ > b.minutes_;
        }
        return a.hours_ > b.hours_;
    }
    return a.days_ > b.days_;
}

bool operator<(const Timespan& a, const Timespan& b)
{
    return !(a >= b);
}

bool operator<=(const Timespan& a, const Timespan& b)
{
    return !(a > b);
}

bool operator==(const Timespan& a, const Timespan& b)
{
    return a.days_ == b.days_ && a.hours_ == b.hours_ && a.minutes_ == b.minutes_ && a.seconds_ == b.seconds_ && a.milliseconds_ == b.milliseconds_;
}

bool operator!=(const Timespan& a, const Timespan& b)
{
    return a.days_ != b.days_ || a.hours_ != b.hours_ || a.minutes_ != b.minutes_ || a.seconds_ != b.seconds_ || a.milliseconds_ != b.milliseconds_;
}
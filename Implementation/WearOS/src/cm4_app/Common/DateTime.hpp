#ifndef DATETIME_H_7B532C
#define DATETIME_H_7B532C

#include "inttypes.h"

bool IsLeapYear(uint16_t year);

class DateTime
{
private:
    uint16_t millisecond_;
    uint8_t second_;
    uint8_t minute_;
    uint8_t hour_;
    uint8_t day_;
    uint8_t month_;
    uint16_t year_;

public:
    static bool UpdateRtcTime(uint32_t deltaT);

    __inline static DateTime Now() { return DateTime(); }
    __inline static DateTime Zero() { return DateTime(0); }
    __inline static DateTime StartOfDay() { return DateTime().GetStartOfDay(); }

    DateTime();
    DateTime(int seconds);
    DateTime(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second, uint16_t millisecond);
    DateTime(uint16_t year, uint16_t day, uint8_t hour, uint8_t minute, uint8_t second, uint16_t millisecond);

    uint16_t GetYear() const { return year_; }
    uint8_t GetMonth() const { return month_; }
    uint8_t GetDay() const { return day_; }
    uint8_t GetHour() const { return hour_; }
    uint8_t GetMinute() const { return minute_; }
    uint8_t GetSecond() const { return second_; }
    uint16_t GetMillisecond() const { return millisecond_; }

    uint8_t GetCalendarWeek(bool iso8601 = true) const;
    uint8_t GetDayOfWeek() const;
    static uint8_t GetDayOfWeek(uint8_t day, uint8_t month, uint16_t year);
    uint16_t GetDayOfYear() const;
    static uint8_t GetTotalWeeksInYear(uint16_t year);
    bool IsLeapYear() const;
    static bool IsLeapYear(uint16_t year);
    bool Valid() const;

    DateTime GetStartOfDay();

    bool operator>(const DateTime& other) const { return !operator<=(other); }
    bool operator>=(const DateTime& other) const { return !operator<(other); }

    bool operator==(const DateTime& other) const
    {
        return year_ == other.year_ && month_ == other.month_ && day_ == other.day_ && hour_ == other.hour_ && minute_ == other.minute_ &&
               second_ == other.second_ && millisecond_ == other.millisecond_;
    }

    bool operator!=(const DateTime& other) const { return !(*this == other); }

    bool operator<(const DateTime& other) const
    {
        if (year_ < other.year_)
        {
            return true;
        }
        if (month_ < other.month_)
        {
            return true;
        }
        if (day_ < other.day_)
        {
            return true;
        }
        if (hour_ < other.hour_)
        {
            return true;
        }
        if (minute_ < other.minute_)
        {
            return true;
        }
        if (second_ < other.second_)
        {
            return true;
        }
        if (millisecond_ < other.second_)
        {
            return true;
        }

        return false;
    }

    bool operator<=(const DateTime& other) const
    {
        if (year_ > other.year_)
        {
            return false;
        }
        if (month_ > other.month_)
        {
            return false;
        }
        if (day_ > other.day_)
        {
            return false;
        }
        if (hour_ > other.hour_)
        {
            return false;
        }
        if (minute_ > other.minute_)
        {
            return false;
        }
        if (second_ > other.second_)
        {
            return false;
        }
        if (millisecond_ > other.second_)
        {
            return false;
        }

        return true;
    }
};

#endif /* DATETIME_H_7B532C */

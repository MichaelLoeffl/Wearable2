#include "Common/Utils.h"
#include "Drivers/Core.h"
#include <string.h>

void StringCopySafe(const char* source, char* destination, unsigned int sourceLength, unsigned int destinationLength)
{
    unsigned int len = destinationLength - 1;
    destination[len] = '\0';
    len              = sourceLength < len ? sourceLength : len;

    memcpy(destination, source, len);
    destination[len] = '\0';
}

void StringCopySafe(const char* source, char* destination, unsigned int destinationLength)
{
    unsigned int len = destinationLength - 1;
    destination[len] = '\0';

    unsigned int i = 0;
    for (; i < len; i++)
    {
        if (source[i] == '\0')
        {
            break;
        }
        destination[i] = source[i];
    }

    destination[i] = '\0';
}

bool ToString(int number, uint8_t* destination, unsigned int destinationLength)
{
    // Reserve space for '\0'
    unsigned int requiredLength = 1;
    int tmp = number;

    // If negative invert - reserve space for '-'
    if (number < 0)
    {
        requiredLength++;
        tmp = -tmp;
    }

    // Probe for number of digits
    while (tmp > 0)
    {
        requiredLength++;
        tmp /= 10;
    }

    if (number == 0)
    {
        requiredLength = 2;
    }

    if (destinationLength < requiredLength)
    {
        if (destinationLength >= 1)
        {
            destination[0] = '\0';
        }
        if (destinationLength == 0)
        {
            HaltDevice(0x1A);
        }
        return false;
    }

    tmp = number >= 0 ? number : -number;
    for (unsigned int i = requiredLength - 1; i > 0; i--)
    {
        auto digit = tmp % 10;
        destination[i - 1] = (uint8_t)('0' + digit);
        tmp /= 10;
    }
    if (number < 0)
    {
        destination[0] = (uint8_t)'-';
    }
    destination[requiredLength - 1] = '\0';
    return true;
}
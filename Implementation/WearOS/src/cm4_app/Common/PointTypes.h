#ifndef POINT_TEC_TYPES_H
#define POINT_TEC_TYPES_H

#include <stdbool.h>
#include <stdint.h>

#ifndef WIN32
typedef uint64_t DWORD;
#endif

#ifndef NULL
#define NULL (void*)0
#endif

#define SAFE_FREE(ptr) \
    if (ptr != NULL)   \
    {                  \
        free(ptr);     \
        ptr = NULL;    \
    }
#define SAFE_DELETE(ptr) \
    if (ptr != NULL)     \
    {                    \
        delete (ptr);    \
        ptr = NULL;      \
    }
#define SAFE_DELETE_ARRAY(ptr) \
    if (ptr != NULL)           \
    {                          \
        delete[](ptr);         \
        ptr = NULL;            \
    }

#endif // POINT_TEC_TYPES_H

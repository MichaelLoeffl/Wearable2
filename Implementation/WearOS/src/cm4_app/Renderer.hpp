#ifndef RENDERER_H_4A8662
#define RENDERER_H_4A8662

#define FRAMES_PER_SECOND (50)
#define MS_PER_FRAME (1000 / FRAMES_PER_SECOND)

#include "Common/Common.h"

class Renderer
{
public:
    void Initialize();
};

#endif /* RENDERER_H_4A8662 */

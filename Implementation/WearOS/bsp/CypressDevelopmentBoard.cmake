set(BSP_DIR ${PROJECT_SOURCE_DIR}/src/cm4_app/TARGET_CypressDevelopmentBoard)

# Set target MPN
psoc6_set_device(CY8C6347BZI-BLD53)

# Set target CPU core
psoc6_set_core(CM4)

# Set OpenOCD script name
set(OPENOCD_CFG ${CMAKE_SOURCE_DIR}/CY8C6xx7.tcl)

set(BSP_SOURCES
  ${BSP_DIR}/cybsp.h
  ${BSP_DIR}/cybsp.c
  ${BSP_DIR}/cybsp_types.h
)
set(BSP_LINK_LIBRARIES
  mtb-pdl-cat1
)
include_directories(
        "${BSP_DIR}/COMPONENT_BSP_DESIGN_MODUS/GeneratedSource"
)

# Set device die-specific definitions
psoc6_add_component(CAT1)
psoc6_add_component(CAT1A)
psoc6_add_component(PSOC6_01)
psoc6_add_bsp_startup(
  startup_psoc6_01_cm4
  cy8c6xx7_cm4_dual
  startup_psoc6_01_cm0plus
  cy8c6xx7_cm0plus
)

# Set CM4-specific definitions
if(NOT ${CORE} STREQUAL CM0P)
  add_definitions(-DCY_USING_HAL)
  psoc6_add_component(CM0P_SLEEP)
  psoc6_add_component(BSP_DESIGN_MODUS)
  psoc6_add_component(PSOC6HAL)
  psoc6_add_component(MXCRYPTO)
  psoc6_add_component(MXCRYPTO_01)
  psoc6_add_component(BLE)
  psoc6_add_component(CORDIO)
  psoc6_add_component(BLESS)
endif()

# Define BSP library
add_library(bsp STATIC EXCLUDE_FROM_ALL ${BSP_SOURCES})
target_link_libraries(bsp PUBLIC ${BSP_LINK_LIBRARIES})

#set(BSP_DIR /bsp/CypressDevelopmentBoard)

# Define custom recipes for the BSP generated sources
psoc6_add_bsp_design_modus(${BSP_DIR}/COMPONENT_BSP_DESIGN_MODUS/design.modus)
psoc6_add_bsp_design_capsense(${BSP_DIR}/COMPONENT_BSP_DESIGN_MODUS/design.cycapsense)
psoc6_add_bsp_design_qspi(${BSP_DIR}/COMPONENT_BSP_DESIGN_MODUS/design.cyqspi)

if(${CORE} STREQUAL CM0P)
  return()
endif()

set(PORT_DIR ${CMAKE_SOURCE_DIR}/os/freertos)

# FreeRTOSConfig.h is included globally
include_directories(${PORT_DIR})

# Load library definitions
include(lib/freertos.cmake)
include(lib/clib-support.cmake)
include(lib/abstraction-rtos.cmake)

# Always link against clib-support and abstraction-rtos
target_link_libraries(bsp PRIVATE freertos abstraction-rtos clib-support)
target_link_libraries(freertos PRIVATE abstraction-rtos)
target_include_directories(mtb-hal-cat1 PUBLIC
  ${FREERTOS_INCLUDE_DIRS}
  ${CLIB_SUPPORT_INCLUDE_DIRS}
  ${ABSTRACTION_RTOS_INCLUDE_DIRS}
)

# Enable RTOS awareness in the HAL and BSP libraries
add_definitions(-DCY_RTOS_AWARE)

# Provide standard RTOS application hooks
target_sources(freertos PRIVATE ${PORT_DIR}/hooks.c)

# Load application definitions
include(app/ble-battery-level-freertos.cmake)
include(app/ble-throughput-freertos.cmake)
include(app/bmi160-motion-sensor-freertos.cmake)
include(app/capsense-buttons-slider-freertos.cmake)
include(app/emwin-eink-freertos.cmake)
include(app/emwin-oled-freertos.cmake)
include(app/emwin-tft-freertos.cmake)
include(app/ml-gesture-classification.cmake)
include(app/ml-profiler.cmake)
include(app/filesystem-emfile-freertos.cmake)
include(app/filesystem-littlefs-freertos.cmake)
include(app/low-power-capsense-freertos.cmake)
include(app/usb-audio-device-freertos.cmake)
include(app/usb-msc-filesystem-freertos.cmake)

# Load AnyCloud definitions for WiFi-enabled boards
include(anycloud.cmake)

# Load projects from CypressAcademy_MTB101_Files
include(app/mtb101.cmake)

# Load projects from Community-Code-Examples
include(app/community.cmake)
